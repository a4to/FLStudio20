FLhd   0  ` FLdt  �20.6.1.1513 ��  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              A                        �  �  �    �HQV ��z  ﻿[General]
GlWindowMode=1
LayerCount=11
FPS=1
AspectRatio=16:9
LayerOrder=0,1,2,3,4,5,6,7,8,9,10

[AppSet0]
App=HUD\HUD Mesh
FParamValues=0,0.3148,0.9695,0,0.2877,0.5243,0.3148,1,0.05,0.15,0.5,0.5,0.5,0.5109,0.5,1
ParamValues=0,314,969,0,287,524,314,1,50,150,500,500,500,510,500,1
Enabled=1

[AppSet1]
App=Postprocess\ParameterShake
FParamValues=1,0,2,0,0,0,0,0,3,0,0,0,5,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=1000,0,2,0,0,0,0,0,3,0,0,0,5,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet2]
App=Misc\ParamLinkCache
FParamValues=0.4789,0,0,0,0,0.0044,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=478,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesMisc\ParamLink=0,0,5,0,6,11,1000,1000,715,500,0,1000,515,0,1000,1000,11,45,252
Enabled=1

[AppSet3]
App=Misc\ParamLink
FParamValues=1,2,0,0,6,11,0.25,0.97,0.6552,0.5,0,1,0.5717,0,0,1,0.0191,0.95,0.3
ParamValues=1,2,0,0,6,11,250,970,655,500,0,1000,571,0,0,1,19,950,300
Enabled=1

[AppSet4]
App=Misc\ParamLink
FParamValues=1,2,0,0,4,11,0.25,0.97,0.6443,0.5,0,1,0.4845,0,0,1,0.0191,0.5,0.3
ParamValues=1,2,0,0,4,11,250,970,644,500,0,1000,484,0,0,1,19,500,300
Enabled=1

[AppSet5]
App=Misc\ParamLink
FParamValues=1,2,0,0,5,14,0.5,0.0681,0.5245,0.5245,0,1,0.8747,0,1,1,0.018,0.05,0.3
ParamValues=1,2,0,0,5,14,500,68,524,524,0,1000,874,0,1,1,18,50,300
Enabled=1

[AppSet6]
App=Misc\ParamLink
FParamValues=1,0,6,0,1,0,0.5,0.5,0.5,0.5,0,1,0.55,0,1,1,0.2331,0.9467,0.3
ParamValues=1,0,6,0,1,0,500,500,500,500,0,1000,550,0,1,1,233,946,300
Enabled=1

[AppSet7]
App=HUD\HUD Text
FParamValues=0,0.5,0,0,0.0339,0.0591,0.5,0,0,0,0,0,0.191,0,0,3,1,0.5,0.5,0,1,0,0,0,1
ParamValues=0,500,0,0,33,59,500,0,0,0,0,0,191,0,0,3,1000,500,500,0,1000,0,0,0,1
Enabled=1
LayerPrivateData=78018B4F494D4B2C2DD12B294963188900003F650455

[AppSet8]
App=HUD\HUD Text
FParamValues=0,0.5,0,0,0.0339,0.3751,0.5,0.6645,0,0,0,0,0.191,0,0,3,1,0.5,0.5,0,1,0,0,0,1
ParamValues=0,500,0,0,33,375,500,664,0,0,0,0,191,0,0,3,1000,500,500,0,1000,0,0,0,1
Enabled=1
LayerPrivateData=78018B4F494D4B2C2DD12B294963188900003F650455

[AppSet9]
App=HUD\HUD Text
FParamValues=0,0.5,0,0,0.0323,0.6898,0.5,0.3404,0,0,0,0,0.191,0,0,3,1,0.5,0.5,0,1,0,0,0,1
ParamValues=0,500,0,0,32,689,500,340,0,0,0,0,191,0,0,3,1000,500,500,0,1000,0,0,0,1
Enabled=1
LayerPrivateData=78018B4F494D4B2C2DD12B294963188900003F650455

[AppSet10]
App=HUD\HUD Text
FParamValues=0,0.5,0,0,0.212,0.0621,0.5,0.9259,0,0,0,0,0.191,0,0,3,1,0.5,0.5,0,1,0,0,0,1
ParamValues=0,500,0,0,212,62,500,925,0,0,0,0,191,0,0,3,1000,500,500,0,1000,0,0,0,1
Enabled=1
LayerPrivateData=78018B4F494D4B2C2DD12B294963188900003F650455

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="Vertical position","Horizontal position",Size,Color
Html="<p align=""center"">[textline]</p>"
VideoUseSync=0
Filtering=0

[Detached]
Top=84
Left=1231
Width=689
Height=577

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

