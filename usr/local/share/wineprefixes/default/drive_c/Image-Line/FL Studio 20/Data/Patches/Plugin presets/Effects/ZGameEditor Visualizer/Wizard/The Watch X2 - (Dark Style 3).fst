FLhd   0 * ` FLdt�  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��2D  ﻿[General]
GlWindowMode=1
LayerCount=21
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=4,7,16,6,5,0,1,2,11,19,17,18,3,10,9,8,12,14,13,15,20
WizardParams=995

[AppSet4]
App=Image effects\Image
FParamValues=0,0,0,0.72,0,0.7,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,720,0,700,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=Watch - Buttons

[AppSet7]
App=Image effects\Image
FParamValues=0,0,0,0.904,0.862,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,904,862,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=2
Name=Watch - Strap

[AppSet16]
App=Image effects\Image
FParamValues=0.588,0,0,1,0.554,0.5,0.5,0,0,0.38,0,0,0,0
ParamValues=588,0,0,1000,554,500,500,0,0,380,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=5
Name=Watch - Ray

[AppSet6]
App=Image effects\Image
FParamValues=0.812,0,0,0,0.548,0.5,0.5,0,0,0,1,0,0,0
ParamValues=812,0,0,0,548,500,500,0,0,0,1,0,0,0
Enabled=1
Collapsed=1
ImageIndex=3
Name=Watch - Glass

[AppSet5]
App=Image effects\Image
FParamValues=0,0,0,0.836,0.568,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,836,568,500,500,0,0,0,1,0,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=Watch - Border

[AppSet0]
App=Canvas effects\Digital Brain
FParamValues=0,0,0,0,1,0.5,0.5,0.6,0.716,0.3,0.1,0.25,1,0.5,1,0
ParamValues=0,0,0,0,1000,500,500,600,716,300,100,250,1000,500,1,0
ParamValuesBackground\SolidColor=0,616,1000,224
Enabled=1
Collapsed=1
Name=Effects BG 1

[AppSet1]
App=Postprocess\FrameBlur
FParamValues=0.184,0.48,0,0.412,0.386
ParamValues=184,480,0,412,386
ParamValuesCanvas effects\TaffyPulls=576,604,1000,0,0,500,500,100,500,500,0,0,0,500
ParamValuesCanvas effects\ShimeringCage=0,0,0,0,72,500,500,0,716,0,84,212,0,200
Enabled=1
UseBufferOutput=1
BufferRenderQuality=6
Collapsed=1
Name=Effects BG 2

[AppSet2]
App=Background\SolidColor
FParamValues=0,0,0,0.244
ParamValues=0,0,0,244
Enabled=1
Collapsed=1
Name=Background

[AppSet11]
App=HUD\HUD Prefab
FParamValues=25,0.888,0.5,0,1,0.5,0.5,0.634,1,1,4,0,0.5,0,0.364,0,0.892,1
ParamValues=25,888,500,0,1000,500,500,634,1000,1000,4,0,500,0,364,0,892,1
ParamValuesBackground\SolidColor=0,0,0,336
ParamValuesHUD\HUD Grid=908,500,0,1000,500,500,1000,1000,1000,444,500,1000,500,428,0,812,1000,176,500,0,0,1000
Enabled=1
Collapsed=1
Name=Grid
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C2CF43273CA18863D00000F220AA3

[AppSet19]
App=HUD\HUD Graph Polar
FParamValues=0,0,0,1,0.5,0.5,0.634,4,0,0,1,0.416,0.752,0,0.384,0.992,0.328,0.384,0.304,2,0.144,1
ParamValues=0,0,0,1000,500,500,634,4,0,0,1,416,752,0,384,992,328,384,304,2,144,1
Enabled=1
Collapsed=1
Name=EQ OMG 1

[AppSet17]
App=HUD\HUD Graph Polar
FParamValues=0,0,0,1,0.5,0.5,0.618,4,0,0,1,0.416,0.752,0,0.384,0.992,0.156,0.348,0.304,2,0.144,1
ParamValues=0,0,0,1000,500,500,618,4,0,0,1,416,752,0,384,992,156,348,304,2,144,1
ParamValuesHUD\HUD Meter Linear=0,500,0,0,0,0,602,0,500,500,300,100,0,188,0,0,250,0,0,1000,500,1000
ParamValuesHUD\HUD Graph Linear=0,500,0,0,500,500,1000,1000,718,444,500,1000,0,1000,0,500,200,0,0,0,500,1000
ParamValuesBackground\SolidColor=0,0,0,336
ParamValuesHUD\HUD Grid=0,500,0,0,500,500,1000,1000,1000,444,500,1000,500,100,0,500,1000,0,676,0,0,1000
Enabled=1
Collapsed=1
Name=EQ OMG 2

[AppSet18]
App=Image effects\Image
FParamValues=0,0,0,0,0.55,0.435,0.5,0,0,0,0,1,0,1
ParamValues=0,0,0,0,550,435,500,0,0,0,0,1,0,1000
Enabled=1
Collapsed=1
ImageIndex=6
Name=Watch - Effects 1

[AppSet3]
App=Image effects\Image
FParamValues=0,0,0,0,0.55,0.5,0.5,0,0,0,0,1,0,1
ParamValues=0,0,0,0,550,500,500,0,0,0,0,1,0,1000
Enabled=1
Collapsed=1
ImageIndex=6
Name=Watch - Effects 2

[AppSet10]
App=Image effects\Image
FParamValues=0,0,0,0,0.55,0.565,0.5,0,0,0,0,1,0,1
ParamValues=0,0,0,0,550,565,500,0,0,0,0,1,0,1000
Enabled=1
Collapsed=1
ImageIndex=6
Name=Watch - Effects 3

[AppSet9]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.437,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,437,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=7
Name=Main Watch 1

[AppSet8]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=7
Name=Main Watch 2

[AppSet12]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.563,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,563,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=7
Name=Main Watch 3

[AppSet14]
App=Text\TextTrueType
FParamValues=0,0,0,0,0,0.5,0.5,0,0,0,0.5
ParamValues=0,0,0,0,0,500,500,0,0,0,500
Enabled=1
Collapsed=1
Name=Main Text

[AppSet13]
App=Background\FourCornerGradient
FParamValues=13,0.192,0.496,0.696,1,0.67,0.68,1,0.76,0.656,1,0.842,0.8,0.816
ParamValues=13,192,496,696,1000,670,680,1000,760,656,1000,842,800,816
Enabled=1
Collapsed=1
Name=Filter Color

[AppSet15]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fide in-out

[AppSet20]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.62,0.5
ParamValues=500,500,500,500,620,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text=Xibalba
Html="<position x=""4"" y=""5""><p><font face=""American-Captain"" size=""5"" color=""#0D0D0D"">[author]</font></p></position>","<position x=""4"" y=""9""><p><font face=""Chosence-Bold"" size=""3"" color=""#0D0D0D"">[title]</font></p></position>","<position x=""4"" y=""16""><p> <font face=""Chosence-Bold"" size=""3"" color=""#3d3d3d"">[comment]</font></p></position>"
Images="[presetpath]Wizard\ColoveContent\Others\COLOVE Watch\The Watch X2 by COLOVE - Border.svg","[presetpath]Wizard\ColoveContent\Others\COLOVE Watch\The Watch X2 by COLOVE - Buttons.svg","[presetpath]Wizard\ColoveContent\Others\COLOVE Watch\The Watch X2 by COLOVE - Strap.svg","[presetpath]Wizard\ColoveContent\Others\COLOVE Watch\The Watch X2 by COLOVE - Glass.svg","[presetpath]Wizard\ColoveContent\Others\COLOVE Watch\The Watch X2 by COLOVE - Ray 1.svg","[presetpath]Wizard\ColoveContent\Others\COLOVE Watch\The Watch X2 by COLOVE - Ray 2.svg"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

