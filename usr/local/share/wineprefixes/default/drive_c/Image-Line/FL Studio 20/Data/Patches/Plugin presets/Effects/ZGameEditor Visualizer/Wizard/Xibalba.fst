FLhd   0  0 FLdt  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   է�  ﻿[General]
GlWindowMode=1
LayerCount=14
FPS=2
MidiPort=-1
Aspect=1
LayerOrder=0,7,2,5,6,3,1,8,10,4,9,11,12,13
WizardParams=659

[AppSet0]
App=HUD\HUD Grid
ParamValues=207,187,191,0,500,500,1000,899,1000,4,500,1000,797,291,405,441,237,70,604,139,907,0
ParamValuesBackground\FourCornerGradient=867,150,0,1000,1000,250,1000,1000,500,1000,1000,750,1000,1000
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet7]
App=HUD\HUD Graph Polar
ParamValues=125,631,1000,0,533,587,686,4,76,74,1,165,850,343,346,500,0,147,74,0,0,0
Enabled=1
Collapsed=1

[AppSet2]
App=HUD\HUD Prefab
ParamValues=247,0,631,1000,0,533,587,593,1000,1000,4,0,500,1,368,0,1000,1
ParamValuesBackground\FogMachine=500,65,689,0,285,500,500,500,500,500,0,0
Enabled=1
Collapsed=1
LayerPrivateData=78DA2BCE4DCCC95148CD49CD4DCD2B298E49CE2C4ACE498552BA0606967A9939650CC318000091BD0D72

[AppSet5]
App=HUD\HUD Prefab
ParamValues=33,0,631,1000,0,217,290,324,700,1000,4,0,500,1,472,0,1000,0
Enabled=1
Collapsed=1
LayerPrivateData=78DA4B4ECCC9C92F2D51C8C9CC4B8D498670740D0C2CF43273CA1846000000C7340A4B

[AppSet6]
App=HUD\HUD Text
ParamValues=0,631,1000,0,81,282,500,0,0,0,0,31,289,0,0,3,1000,500,500,0,1000,257,387,2,1
Enabled=1
Collapsed=1
LayerPrivateData=78DAF3CB2F4BF4CDCFCB6718A1000022B3032E

[AppSet3]
App=Postprocess\Youlean Bloom
ParamValues=334,0,112,641,1000,0,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet1]
App=HUD\HUD 3D
ParamValues=0,1,518,546,0,530,535,52,475,450,508,529,1000,238,96,500,500,1000,500,500,729,1000,1000,0,1,0,0,1000,1000,1
Enabled=1

[AppSet8]
App=Postprocess\AudioShake
ParamValues=63,0,1,572,0,0
Enabled=1

[AppSet10]
App=Background\FourCornerGradient
ParamValues=6,387,0,1000,654,292,1000,662,194,1000,635,611,989,605
Enabled=1
Collapsed=1

[AppSet4]
App=HUD\HUD Image
ParamValues=0,1,500,500,1000,1000,500,4,500,0,0,1000,1000,1,0
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet9]
App=Postprocess\ParameterShake
ParamValues=201,487,0,18,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesPostprocess\TransitionEffects=333,0,1000,664,0,500,500,500,500,500,0,0,0,1000
ParamValuesPostprocess\Vignette=0,0,63,1000,724,447
ParamValuesPostprocess\ScanLines=800,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet11]
App=Text\TextTrueType
ParamValues=432,0,0,1000,0,491,498,0,0,0,500
Enabled=1

[AppSet12]
App=Text\TextTrueType
ParamValues=0,0,0,0,0,491,500,0,0,0,500
Enabled=1

[AppSet13]
App=Postprocess\Youlean Color Correction
ParamValues=500,500,500,500,500,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0

[UserContent]
Text=Xibalba
Html=,"<position y=""5""><p align=""right""><font face=""American-Captain"" size=""7"" color=""#E5E5E5"">[author]</font></p></position>","<position y=""11""><p align=""right""><font face=""Chosence-Bold"" size=""5"" color=""#E5E5E5"">[title]</font></p></position>","<position x=""4"" y=""76""><p align=""left""><font face=""Chosence-Bold"" size=""2"" color=""#E5E5E5"">[extra1]</font></p></position>","<position x=""4"" y=""79""><p align=""left""><font face=""Chosence-Bold"" size=""3"" color=""#E5E5E5"">[comment]</font></p></position>",,,,"<position x=""4"" y=""86""><p align=""left""><p uppercase=""yes""><font face=""Antaris"" size=""4"" color=""#E5E5E5"">[extra6]</font></p></position>","<position x=""4"" y=""91""><p align=""left""><p uppercase=""yes""><font face=""Antaris"" size=""3"" color=""#E5E5E5"">[extra7]</font></p></position>",,"  "
VideoUseSync=0
EnableMipmap=1

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

