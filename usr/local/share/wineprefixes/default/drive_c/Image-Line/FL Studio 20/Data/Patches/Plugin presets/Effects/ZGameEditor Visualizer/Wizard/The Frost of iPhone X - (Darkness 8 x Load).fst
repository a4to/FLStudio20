FLhd   0 * ` FLdt�7  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV Վn
7  ﻿[General]
GlWindowMode=1
LayerCount=24
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=9,21,15,6,11,5,19,16,24,10,12,22,14,13,7,0,20,1,4,3,2,8,17,18

[AppSet9]
App=Canvas effects\DarkSpark
FParamValues=0.5,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0,0
ParamValues=500,0,500,500,500,500,500,500,500,500,0,0
ParamValuesBackground\ItsFullOfStars=0,700,0,0,500,500,500,0,0,0,684
ParamValuesPostprocess\RGB Shift=504,656,0,600,700,200
ParamValuesFeedback\WormHoleDarkn=0,0,0,1000,884,500,500,880,500,500,500,500
ParamValuesScenes\RhodiumLiquidCarbon=0,41,1000,0,0,500,500,1000,0,1000
ParamValuesHUD\HUD Grid=0,500,0,0,500,500,1000,1000,916,444,500,1000,500,520,172,500,1000,40,684,184,0,1000
ParamValuesFeedback\SphericalProjection=60,0,0,0,1000,425,500,590,500,500,0,500,0,1000,1000,1000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,570,500,500,500,500,500
ParamValuesCanvas effects\Electric=0,144,1000,620,732,492,504,188,100,1000,500
ParamValuesTunnel\TorusJourney=0,0,0,0,0,500,0,0,768,76,0,40,390,1000,1000,1000,800,0,0,136
Enabled=1
Collapsed=1
ImageIndex=7
Name=Background FX

[AppSet21]
App=Object Arrays\Pentaskelion
FParamValues=0,0,1,0,0,0.5,0.5,0.768,0.5,1,0.736,0.78,0.772,0.308
ParamValues=0,0,1000,0,0,500,500,768,500,1000,736,780,772,308
ParamValuesFeedback\WarpBack=0,0,0,0,500,500,1000
ParamValuesImage effects\ImageSphereWarp=0,548,718,1000,106,0,1000,500,500,500
ParamValuesFeedback\WormHoleDarkn=1000,0,0,0,500,620,312,300,500,732,652,500
ParamValuesObject Arrays\Filaments=0,0,0,0,204,500,500,500,500,500,0,0,500,0
ParamValuesObject Arrays\CrystalCube=800,100,500,500,322,500,500,500
ParamValuesObject Arrays\CubicMatrix=0,0,0,0,840,500,492,0,500,548,0,932,848
ParamValuesFeedback\70sKaleido=0,0,348,692,504,1000
ParamValuesImage effects\ImageSphinkter=0,0,0,0,1000,500,500,1000,1000,500,708,568,500,0,144
ParamValuesImage effects\Image=288,0,0,1000,1000,500,500,140,656,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,233,500,500,500,500,500
ParamValuesFeedback\BoxedIn=0,0,0,660,624,0,0,1000,380,784
Enabled=1
Collapsed=1
ImageIndex=7
Name=FIRE

[AppSet15]
App=Object Arrays\BallZ
FParamValues=0,0,0,1,0.046,0.5,0.5,2,0.068,0.08,0.712,0.972,0.64
ParamValues=0,0,0,1000,46,500,500,2,68,80,712,972,640
ParamValuesImage effects\ImageMasked=0,0,0,1000,456,500,500,200,200,0,500
ParamValuesHUD\HUD Prefab=216,0,500,0,1000,500,500,656,1000,1000,444,0,500,1000,1000,0,940,1000
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesTunnel\ForwardFall=840,584,1000,0,1000,460,500,116,1000,600,672,0
ParamValuesCanvas effects\ShimeringCage=0,0,0,1000,0,500,500,368,376,300,324,0,0,0
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesBackground\ItsFullOfStars=0,0,0,0,500,500,500,500,0,216,1000
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,248,192,500,36,492,948,500,500
ParamValuesFeedback\SphericalProjection=60,0,0,0,0,425,500,54,284,472,1000,500,0,552,200,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesImage effects\Image=0,0,0,1000,764,500,500,0,1000,0,0
ParamValuesFeedback\FeedMeFract=0,0,380,1000,504,0
ParamValuesPostprocess\Youlean Blur=56,0,0
ParamValuesPostprocess\Blur=440
ParamValuesPostprocess\Youlean Motion Blur=668,0,0,0
Enabled=1
Collapsed=1
Name=Killer

[AppSet6]
App=Background\ItsFullOfStars
FParamValues=0,1,1,0,0.196,0.5,0.5,0.136,1,0.412,0.856
ParamValues=0,1000,1000,0,196,500,500,136,1000,412,856
ParamValuesHUD\HUD Text=0,0,0,0,660,686,500,552,1000,1000,324,41,240,0,0,750,1000,536,504,1000
ParamValuesObject Arrays\BallZ=0,0,0,1000,914,500,500,182,68,80,712,972,640
ParamValuesPeak Effects\Stripe Peeks=528,200,988,0,928,0,54,736,520,0,434,144,296,1000,368,466,460,72,62,0,1000,680,0
Enabled=1
Collapsed=1
ImageIndex=7
Name=System OFF

[AppSet11]
App=Background\ItsFullOfStars
FParamValues=0.532,0.852,1,0,0.04,0.5,0.5,0.62,0.352,0.008,1
ParamValues=532,852,1000,0,40,500,500,620,352,8,1000
ParamValuesBackground\FourCornerGradient=933,1000,224,1000,1000,736,1000,1000,452,1000,1000,400,1000,1000
ParamValuesPeak Effects\JoyDividers=0,312,616,0,204,0,628,44,724,252,604,722,0
ParamValuesPeak Effects\PeekMe=0,1000,1000,0,204,528,384,0,0,216,60,180
ParamValuesHUD\HUD Graph Linear=0,0,1000,0,796,363,1000,1000,258,444,500,1000,404,224,0,864,0,280,0,333,1000,1000
ParamValuesHUD\HUD Free Line=0,500,0,0,656,552,168,304,182,92,182,168,1000,312,0,280,500
ParamValuesPeak Effects\Stripe Peeks=0,0,808,1000,0,0,74,736,520,0,786,196,296,0,276,518,108,308,224,1000,1000,1000,0
Enabled=1
Collapsed=1
ImageIndex=7
Name=EG BG 1

[AppSet5]
App=Background\FourCornerGradient
FParamValues=0,0.128,0.656,1,1,0.778,1,1,0.736,0.564,1,0.482,1,1
ParamValues=0,128,656,1000,1000,778,1000,1000,736,564,1000,482,1000,1000
ParamValuesImage effects\Image=0,0,0,0,950,500,500,0,0,0,0
ParamValuesPostprocess\Youlean Bloom=388,252,542,486
Enabled=1
Collapsed=1
Name=Color HUE

[AppSet19]
App=Postprocess\Ascii
FParamValues=1,0,0
ParamValues=1,0,0
ParamValuesPostprocess\Youlean Motion Blur=124,0,312,1000
ParamValuesFeedback\FeedMeFract=0,0,0,1000,664,0
ParamValuesBackground\FourCornerGradient=933,1000,0,1000,1000,0,1000,1000,0,1000,1000,0,1000,1000
ParamValuesPostprocess\Youlean Blur=424,0,0
Enabled=1
Collapsed=1
Name=AW Blur

[AppSet16]
App=Feedback\BoxedIn
FParamValues=0,0,0,1,0.7,0,0.252,0.5,0.032,0.676
ParamValues=0,0,0,1000,700,0,252,500,32,676
ParamValuesHUD\HUD Prefab=31,0,500,0,852,500,856,1000,1000,1000,444,0,1000,1000,200,0,1000,1000
ParamValuesParticles\ColorBlobs=0,0,0,0,376,500,500,350,1000,148
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesHUD\HUD Text=0,500,0,0,168,344,500,0,0,0,0,0,100,0,0,750,1000,500,500,1000
ParamValuesPostprocess\ScanLines=0,0,0,0,0,0
ParamValuesPeak Effects\Polar=0,0,0,0,188,36,572,228,1000,856,596,448,0,500,0,500,0,1000,1000,1000,320,480,1000,1000
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesParticles\fLuids=0,0,0,0,500,0,500,0,0,0,500,500,500,0,500,250,500,0,0,500,500,500,0,0,500,0,0,250,500,0,0,0
ParamValuesParticles\PlasmaFlys=500,500,500,0,1000,500,500,500,0,500,500,500,500,500
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesHUD\HUD Meter Linear=0,208,884,1000,0,0,0,824,128,753,224,68,0,11,500,0,584,560,404,364,568,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesFeedback\70sKaleido=0,0,212,1000,208,500
ParamValuesPeak Effects\Stripe Peeks=0,200,1000,1000,0,0,86,264,500,0,250,500,612,0,500,250,200,0,150,1000,1000,300,0
ParamValuesPostprocess\Point Cloud High=544,1000,520,500,184,168,467,503,62,777,140,1000,1000,0,667
ParamValuesFeedback\SphericalProjection=60,0,0,0,1000,0,0,0,0,0,0,333,500,1000,1000,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesPostprocess\Youlean Motion Blur=500,0,776,0
ParamValuesHUD\HUD 3D=0,0,500,500,500,500,500,580,500,500,500,1000,1000,500,1000,1000,0,0,1000,1000,500
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,746,500,500,500,500,500
ParamValuesScenes\Musicball=708,0,376,1000,500,500,500,0,896,500,148,492,0
ParamValuesFeedback\FeedMeFract=0,0,0,0,0,0
ParamValuesPostprocess\Point Cloud Low=0,330,330,476,500,456,504,503,306,625,156,0,0,0,0
Enabled=1
Collapsed=1
Name=WoW

[AppSet24]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.5,0.26,0.15,0.45,1,0,0,0
ParamValues=500,260,150,450,1000,0,0,0
ParamValuesPostprocess\Vignette=0,0,0,680,1000,276
ParamValuesFeedback\BoxedIn=0,0,0,560,800,0,0,1000,264,308
ParamValuesPeak Effects\Stripe Peeks=0,0,704,1000,0,0,74,736,520,0,18,448,432,1000,524,518,344,216,536,1000,1000,1000,0
ParamValuesPostprocess\Point Cloud High=0,330,330,500,500,448,444,503,1000,625,32,0,0,0,0
ParamValuesPostprocess\Point Cloud Low=0,330,330,500,500,448,444,743,888,625,28,0,0,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=EG BG 2

[AppSet10]
App=HUD\HUD Prefab
FParamValues=0,0,0.14,0,0,0.502,0.746,0.25,1,1,4,0,0.5,1,0.368,0.098,1,1
ParamValues=0,0,140,0,0,502,746,250,1000,1000,4,0,500,1,368,98,1000,1
ParamValuesHUD\HUD Graph Radial=0,500,0,0,200,500,250,444,500,0,1000,0,1000,0,250,500,200,0,0,0,500,1000
ParamValuesHUD\HUD Meter Radial=0,248,1000,0,0,0,0,992,492,708,156,34,1000,0,374,0,1000,1000,500,1000
ParamValuesHUD\HUD Free Line=0,500,0,0,656,552,168,304,182,92,182,168,1000,312,0,280,500
ParamValuesHUD\HUD Graph Linear=0,500,0,0,816,468,1000,1000,250,444,500,1000,212,1000,0,500,200,0,0,0,500,1000
ParamValuesHUD\HUD Meter Linear=0,500,0,0,0,0,750,0,800,664,300,100,0,125,500,1,238,0,0,1000,612,1000
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=LOGO
LayerPrivateData=780173C8CBCF4BD52B2E4B671899000060F9036F

[AppSet12]
App=Background\SolidColor
FParamValues=0,0.492,0.14,0.924
ParamValues=0,492,140,924
Enabled=1
Collapsed=1
Name=Background Main

[AppSet22]
App=HUD\HUD Prefab
FParamValues=9,0.62,0.58,0.244,0.316,0.5,0.5,0.29,1,1,4,0,0.5,0,0.144,0,1,1
ParamValues=9,620,580,244,316,500,500,290,1000,1000,4,0,500,0,144,0,1000,1
ParamValuesBackground\SolidColor=0,492,140,584
Enabled=1
Collapsed=1
Name=BG Grid
LayerPrivateData=78014B4A4CCE4E2FCA2FCD4B89490232750D0C2CF53273CA18460A0000F7BC084D

[AppSet14]
App=HUD\HUD Grid
AppVersion=1
FParamValues=0,0.636,0.072,0,0.18,0.48,1,0.988,0.308,4,0.5,1,0.5,0.452,0.924,0.5,1,0,0.44,0,1,1
ParamValues=0,636,72,0,180,480,1000,988,308,4,500,1000,500,452,924,500,1000,0,440,0,1000,1
ParamValuesHUD\HUD Prefab=31,0,500,0,852,500,148,1000,1000,1000,444,0,500,1000,200,0,1000,1000
ParamValuesBackground\SolidColor=0,792,760,120
Enabled=1
Collapsed=1
Name=Glow BG

[AppSet13]
App=Image effects\Image
FParamValues=0,0,0,0,0.709,0.5,0.501,0.042,0,0.25,0,0,0,0
ParamValues=0,0,0,0,709,500,501,42,0,250,0,0,0,0
ParamValuesImage effects\ImageWall=0,0,0,0,0,0,0
ParamValuesHUD\HUD Image=0,0,500,500,1000,1000,500,444,500,0,0,1000,1000,500,1000
ParamValuesBackground\SolidColor=0,892,1000,124
ParamValuesImage effects\ImageSlices=0,0,0,0,500,496,488,0,0,0,500,269,1000,500,0,0,0
Enabled=1
Collapsed=1
ImageIndex=6
Name=LCD - Mask

[AppSet7]
App=Text\TextTrueType
FParamValues=0.012,0,0,0,0,0.495,0.5,0,0,0,0.5
ParamValues=12,0,0,0,0,495,500,0,0,0,500
Enabled=1
Collapsed=1
Name=Main Text

[AppSet0]
App=Image effects\Image
FParamValues=0.54,0,0,1,1,0.5,0.501,0,0,0,0,0,0,0
ParamValues=540,0,0,1000,1000,500,501,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=5

[AppSet20]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=5

[AppSet1]
App=Image effects\Image
FParamValues=0,0,0,0.864,0.947,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,864,947,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=2
Name=iPhone - Buttons

[AppSet4]
App=Image effects\Image
FParamValues=0.764,0,0,0,0.952,0.501,0.5,0,0.004,0,0,0,0,0
ParamValues=764,0,0,0,952,501,500,0,4,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=4
Name=iPhone - Glass

[AppSet3]
App=Image effects\Image
FParamValues=0,0,0,0.94,0.972,0.5,0.514,0,0,0,0,0,0,0
ParamValues=0,0,0,940,972,500,514,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=iPhone - Border 2

[AppSet2]
App=Image effects\Image
FParamValues=0,0,0,0.872,0.947,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,872,947,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
Name=iPhone - Border 1

[AppSet8]
App=Image effects\Image
FParamValues=0,0,0,0,0.947,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,947,500,500,0,0,0,0,0,0,0
ParamValuesHUD\HUD Free Line=904,0,0,0,660,492,388,304,0,92,0,220,1000,312,0,272,500
ParamValuesHUD\HUD Text=44,500,0,0,652,520,500,552,1000,1000,324,1,240,0,0,750,1000,536,504,1000
Enabled=1
Collapsed=1
ImageIndex=3
Name=iPhone - Face ID

[AppSet17]
App=Background\FourCornerGradient
FParamValues=5,0.464,0.084,0.316,0.856,0.084,0.316,0.856,0.084,0.316,0.856,0.084,0.316,0.856
ParamValues=5,464,84,316,856,84,316,856,84,316,856,84,316,856
ParamValuesPostprocess\ColorCyclePalette=0,0,0,0,0,264,560,288,396,500,336
ParamValuesHUD\HUD Meter Linear=0,208,884,1000,0,0,0,824,128,753,224,68,0,11,500,0,584,560,404,364,568,1000
Enabled=1
Collapsed=1
Name=Filter Color

[AppSet18]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fade-in and out

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="<position y=""86""><p align=""center""><font face=""Chosence-Bold"" size=""2"" color=""#fff"">Image-Line Software</font></p></position>","<position y=""88  ""><p align=""center""><font face=""Chosence-regular"" size=""2"" color=""#CCCCC1"">Exclusive Sounds</font></p></position>"
Html="<position x=""3"" y=""4""><p align=""left""><font face=""American-Captain"" size=""8"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""3"" y=""12""><p align=""left""><b><font face=""Chosence-Bold"" size=""6"" color=""#FFFFFF"">[title]</font></b></p></position>","<position y=""87.1""><p align=""right""><b><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[comment]</font></b></p></position>",,,," ",,," ",,,
Images=[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Border-1.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Border-2.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Buttons.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Face-id.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Glass.svg
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

