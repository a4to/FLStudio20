FLhd   0  0 FLdt!  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   ՠA�   ﻿[General]
GlWindowMode=1
LayerCount=26
FPS=2
MidiPort=-1
Aspect=1
LayerOrder=14,20,15,30,29,1,2,3,4,5,6,9,10,7,8,11,28,13,0,22,23,21,16,12,17,18
WizardParams=1376,1424,1429

[AppSet14]
App=Canvas effects\OverlySatisfying
ParamValues=884,740,484,780
ParamValuesBackground\ItsFullOfStars=0,0,0,0,0,500,500,500,1000,0,0
ParamValuesPeak Effects\Stripe Peeks=392,0,0,1000,1000,0,191,500,500,500,158,1000,0,0,512,90,0,0,0,1000,1000,112,0
ParamValuesCanvas effects\Flaring=0,1000,1000,1000,0,500,500,181,0,0,184
ParamValuesPeak Effects\Reactive Sphere=584,260,0,0,821,500,500,252,60,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesHUD\HUD Graph Polar=0,500,0,0,500,500,254,444,500,0,0,0,1000,0,250,500,36,0,0,0,500,1000
ParamValuesCanvas effects\N-gonFigure=300,654,900,200,0,500,500,500,500,400,0
ParamValuesCanvas effects\TaffyPulls=692,0,0,0,0,500,500,0,500,500,0,0,0,500
Enabled=1
Collapsed=1

[AppSet20]
App=Peak Effects\Linear
ParamValues=296,964,648,1000,254,500,496,388,0,500,572,174,0,1,1,0,0,500,500,500,500,828,1000,268,388,0,364,212,330,1000,100
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesPeak Effects\Polar=0,0,0,1000,364,500,500,232,1000,500,500,500,1000,368,0,1000,24,1000,1000,0,1000,0,0,1000
ParamValuesPeak Effects\Reactive Sphere=584,260,0,0,821,500,500,252,472,1000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesMisc\PentUp=0,0,500,500,20,500,500,500,0,0,516,140
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPostprocess\Notebook_Drawings_RC_V5=0,200,0,250,250,0,200,1000,0
ParamValuesPeak Effects\Stripe Peeks=392,0,0,1000,1000,0,191,500,500,500,158,1000,0,0,512,90,0,0,0,1000,1000,112,0
ParamValuesImage effects\ImageSphinkter=0,0,0,0,600,500,500,500,500,500,500,500,500,0,0
ParamValuesMisc\FruityIndustry=700,0,1000,1000,500,500,500,500,500,0,500,500,500
ParamValuesHUD\HUD Graph Polar=0,500,0,1000,500,500,458,444,500,0,0,0,1000,0,250,284,36,0,0,0,500,1000
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,891,500,480,500,644,0
Enabled=1
Collapsed=1
ImageIndex=4

[AppSet15]
App=Peak Effects\Linear
ParamValues=296,964,648,1000,254,502,496,388,500,500,572,174,1000,1,1,0,0,500,500,500,500,828,1000,268,388,0,364,212,330,1000,100
Enabled=1
Collapsed=1
ImageIndex=4

[AppSet30]
App=HUD\HUD Image
ParamValues=320,0,500,500,1000,1000,174,4,500,0,0,1000,1000,1,1
ParamValuesBackground\FogMachine=0,0,1000,0,0,500,500,100,0,444,0,0
ParamValuesCanvas effects\DarkSpark=500,0,500,500,136,500,500,0,0,500,0,0
ParamValuesCanvas effects\Electric=0,740,484,780,1000,500,500,0,0,1000,500
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet29]
App=Postprocess\Youlean Color Correction
ParamValues=500,500,500,500,0,500
ParamValuesPeak Effects\Reactive Sphere=512,260,0,0,821,500,500,252,1000,1000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet1]
App=HUD\HUD Prefab
ParamValues=18,152,500,0,1000,500,500,126,1000,1000,4,0,120,1,1000,316,344,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C2CF43273CA18863D00000F220AA3

[AppSet2]
App=HUD\HUD Prefab
ParamValues=18,152,500,0,1000,500,500,36,1000,1000,4,0,920,1,1000,316,344,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C2CF43273CA18863D00000F220AA3

[AppSet3]
App=HUD\HUD Prefab
ParamValues=18,152,500,0,1000,500,500,92,1000,1000,4,0,976,1,1000,316,344,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C2CF43273CA18863D00000F220AA3

[AppSet4]
App=HUD\HUD Prefab
ParamValues=18,152,500,0,1000,500,500,73,1000,1000,4,0,272,1,664,316,344,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C2CF43273CA18863D00000F220AA3

[AppSet5]
App=HUD\HUD Prefab
ParamValues=18,152,500,0,1000,500,500,61,1000,1000,4,0,680,1,692,316,344,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C2CF43273CA18863D00000F220AA3

[AppSet6]
App=HUD\HUD Prefab
ParamValues=18,152,500,0,1000,500,500,45,1000,1000,4,0,504,1,1000,316,344,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C2CF43273CA18863D00000F220AA3

[AppSet9]
App=HUD\HUD Prefab
ParamValues=18,152,500,0,1000,500,500,27,1000,1000,4,0,600,1,1000,316,344,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C2CF43273CA18863D00000F220AA3

[AppSet10]
App=HUD\HUD Prefab
ParamValues=18,152,500,0,1000,500,500,16,1000,1000,4,0,79,1,1000,316,344,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C2CF43273CA18863D00000F220AA3

[AppSet7]
App=Misc\Automator
ParamValues=1,2,13,3,1000,10,250,1,3,13,3,1000,35,250,1,4,13,3,1000,23,250,1,5,13,3,1000,31,250
Enabled=1
Collapsed=1

[AppSet8]
App=Misc\Automator
ParamValues=1,6,13,3,1000,15,250,1,7,13,3,1000,17,250,1,10,13,3,1000,50,250,1,11,13,3,1000,90,250
Enabled=1
Collapsed=1

[AppSet11]
App=HUD\HUD Prefab
ParamValues=12,932,500,0,1000,500,500,130,1000,1000,4,0,120,1,1000,0,996,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C8CF43273CA18863D000009B80A9D

[AppSet28]
App=Postprocess\Youlean Color Correction
ParamValues=500,500,500,500,500,500
Enabled=1
Collapsed=1

[AppSet13]
App=Misc\Automator
ParamValues=1,12,13,3,1000,10,250,0,7,13,3,1000,17,250,0,10,13,3,1000,50,250,0,11,13,3,1000,90,250
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet0]
App=Image effects\Image
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet22]
App=Postprocess\Youlean Blur
ParamValues=0,0,0,0,0,0
ParamValuesPostprocess\FrameBlur=0,0,0,0,816,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesPostprocess\Youlean Motion Blur=500,0,0,476
ParamValuesPostprocess\Youlean Bloom=64,0,150,450
Enabled=1
Collapsed=1

[AppSet23]
App=Postprocess\ParameterShake
ParamValues=328,0,22,0,0,0,0,0,0,0,0,0,2,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet21]
App=Postprocess\AudioShake
ParamValues=4,0,0,500,100,1000
Enabled=1
Collapsed=1

[AppSet16]
App=HUD\HUD 3D
ParamValues=0,0,500,500,500,500,500,500,500,500,500,500,500,500,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
Collapsed=1
ImageIndex=3

[AppSet12]
App=HUD\HUD 3D
ParamValues=0,0,500,500,500,500,500,500,500,500,500,500,500,384,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
Collapsed=1
ImageIndex=2

[AppSet17]
App=Text\TextTrueType
ParamValues=456,0,0,1000,0,489,498,0,0,0,500
Enabled=1
Collapsed=1

[AppSet18]
App=Text\TextTrueType
ParamValues=0,0,0,0,0,489,500,0,0,0,500
Enabled=1
Collapsed=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0

[UserContent]
Text="This is the default text."
Html="<position x=""5""><position y=""5""><p align=""left""><font face=""American-Captain"" size=""9"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""5""><position y=""15""><p align=""left""><font face=""Chosence-Bold"" size=""6"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""5""><position y=""84""><p align=""right""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[extra1]</font></p></position>","<position x=""5""><position y=""90""><p align=""right""><font face=""American-Captain"" size=""4"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""5""><position y=""76""><p align=""center""><font face=""Chosence-Bold"" size=""4"" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""5""><position y=""80""><p align=""center""><font face=""Chosence-regular"" size=""3"" color=""#FFFFF"">[extra3]</font></p></position>"
Images=[plugpath]Content\Bitmaps\template1.jpg,"[presetpath]Wizard\Assets\Sacco\Circle - White.svg"
VideoUseSync=0
EnableMipmap=1

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

