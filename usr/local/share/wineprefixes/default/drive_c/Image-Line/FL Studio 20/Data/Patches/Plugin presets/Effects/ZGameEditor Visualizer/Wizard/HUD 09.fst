FLhd   0  0 FLdte  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   ��5�  ﻿[General]
GlWindowMode=1
LayerCount=29
FPS=2
MidiPort=-1
Aspect=1
LayerOrder=13,14,15,17,18,7,9,10,8,4,0,2,1,3,12,5,11,16,6,21,19,20,22,23,24,25,27,28,26
WizardParams=1137,1138,1185,1186,947

[AppSet13]
App=HUD\HUD Prefab
ParamValues=16,884,556,0,1000,529,468,190,1000,1000,4,0,123,1,0,0,48,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0CCCF43273CA18863D00000D540AA1

[AppSet14]
App=HUD\HUD Prefab
ParamValues=16,796,556,0,1000,529,468,190,1000,1000,4,0,306,1,0,48,128,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0CCCF43273CA18863D00000D540AA1

[AppSet15]
App=HUD\HUD Prefab
ParamValues=16,939,556,0,1000,529,468,192,1000,1000,4,0,612,1,368,128,170,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0CCCF43273CA18863D00000D540AA1

[AppSet17]
App=HUD\HUD Prefab
ParamValues=17,916,556,0,1000,529,468,321,1000,1000,4,0,62,1,368,179,530,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0CCCF53273CA18863D00000E3B0AA2

[AppSet18]
App=HUD\HUD Prefab
ParamValues=17,728,556,0,1000,529,468,384,1000,1000,4,0,110,1,368,156,181,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0CCCF53273CA18863D00000E3B0AA2

[AppSet7]
App=HUD\HUD Prefab
ParamValues=14,892,348,272,1000,529,468,122,1000,1000,4,0,123,1,368,479,976,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C4CF43273CA18863D00000B860A9F

[AppSet9]
App=HUD\HUD Prefab
ParamValues=14,0,772,312,1000,529,468,66,1000,1000,4,0,346,1,368,915,987,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C4CF43273CA18863D00000B860A9F

[AppSet10]
App=HUD\HUD Prefab
ParamValues=14,596,0,324,1000,529,468,128,1000,1000,4,0,134,1,0,813,916,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C4CF43273CA18863D00000B860A9F

[AppSet8]
App=HUD\HUD Prefab
ParamValues=14,420,500,747,1000,529,468,110,1000,1000,4,0,367,1,0,184,591,1
Enabled=1
UseBufferOutput=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C4CF43273CA18863D00000B860A9F

[AppSet4]
App=Background\SolidColor
ParamValues=400,0,0,0
Enabled=1
Collapsed=1

[AppSet0]
App=HUD\HUD Mesh
ParamValues=524,268,0,0,270,527,526,2,752,602,836,0,636,572,243,1
Enabled=1
Collapsed=1
MeshIndex=2

[AppSet2]
App=Postprocess\Youlean Bloom
ParamValues=184,0,178,450,1000,0,0,0
Enabled=1
Collapsed=1

[AppSet1]
App=Postprocess\Youlean Motion Blur
ParamValues=588,1,0,324,0,0,0
Enabled=1
Collapsed=1

[AppSet3]
App=HUD\HUD Mesh
ParamValues=892,500,0,1000,246,527,526,5,8,150,500,500,636,572,680,1
Enabled=1
Collapsed=1
MeshIndex=2

[AppSet12]
App=HUD\HUD Mesh
ParamValues=892,500,0,1000,246,527,526,2,0,150,500,500,636,572,680,1
Enabled=1
Collapsed=1
MeshIndex=2

[AppSet5]
App=Misc\Automator
ParamValues=1,2,4,3,1000,38,250,1,8,13,3,1000,9,250,1,10,13,3,1000,22,250,1,11,13,2,208,170,562
Enabled=1
Collapsed=1

[AppSet11]
App=Misc\Automator
ParamValues=1,9,13,3,1000,6,250,1,9,8,2,103,353,507,1,14,13,3,1000,2,250,1,15,13,3,1000,5,250
Enabled=1
Collapsed=1

[AppSet16]
App=Misc\Automator
ParamValues=1,16,13,3,1000,10,250,1,19,13,2,103,353,507,1,8,13,3,1000,2,250,1,18,13,3,1000,1,250
Enabled=1
Collapsed=1

[AppSet6]
App=HUD\HUD 3D
ParamValues=0,0,500,500,500,500,500,500,404,600,500,500,500,1000,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
Collapsed=1
ImageIndex=7
MeshIndex=2

[AppSet21]
App=Postprocess\Youlean Handheld
ParamValues=208,0,924,968,84,0
ParamValuesPostprocess\Youlean Bloom=85,412,269,450
Enabled=1
Collapsed=1

[AppSet19]
App=Postprocess\Youlean Color Correction
ParamValues=660,500,772,500,122,500
Enabled=1
Collapsed=1

[AppSet20]
App=Misc\Automator
ParamValues=1,20,5,2,436,166,662,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1
Collapsed=1

[AppSet22]
App=Postprocess\ParameterShake
ParamValues=544,88,8,14,544,88,10,14,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet23]
App=Postprocess\ParameterShake
ParamValues=544,88,13,14,544,88,14,14,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet24]
App=Postprocess\ParameterShake
ParamValues=104,0,12,8,544,88,14,14,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet25]
App=Text\TextTrueType
ParamValues=200,0,0,1000,0,493,498,0,0,0,500
Enabled=1
Collapsed=1

[AppSet27]
App=Peak Effects\Linear
ParamValues=0,524,0,0,182,243,726,388,0,1000,260,470,480,0,1,0,0,500,500,500,500,828,500,152,200,0,32,212,330,250,100
ParamValuesPeak Effects\SplinePeaks=988,20,950,0,604,500,1000,0,0,0
Enabled=1
Collapsed=1
ImageIndex=6

[AppSet28]
App=Peak Effects\Linear
ParamValues=0,524,648,0,182,243,726,388,0,500,260,350,480,0,1,0,0,500,500,500,500,828,500,152,200,0,32,212,330,250,100
Enabled=1
Collapsed=1
ImageIndex=6

[AppSet26]
App=Text\TextTrueType
ParamValues=0,0,0,0,0,493,500,0,0,0,500
Enabled=1
Collapsed=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0

[UserContent]
Text="This is the default text."
Html="<position x=""4""><position y=""5""><p align=""left""><font face=""American-Captain"" size=""13"" color=""#5e5e5e"">[author]</font></p></position>","<position x=""4""><position y=""28""><p align=""left""><font face=""Chosence-Bold"" size=""10"" color=""#5e5e5e"">[title]</font></p></position>","<position x=""0""><position y=""5""><p align=""right""><font face=""Chosence-Bold"" size=""4"" color=""#5e5e5e"">[extra1]</font></p></position>","<position x=""59""><position y=""91""><p align=""right""><font face=""Chosence-Bold"" size=""4"" color=""#5e5e5e"">[comment]</font></p></position>",,"<position x=""81""><position y=""74""><p align=""center""><font face=""Chosence-Bold"" size=""4"" color=""#5e5e5e"">[extra2]</font></p></position>","<position x=""81""><position y=""79""><p align=""center""><font face=""Chosence-regular"" size=""4"" color=""#5e5e5e"">[extra3]</font></p></position>"
Meshes=[plugpath]Content\Meshes\Sphere(Ico).zgeMesh,[plugpath]Content\Meshes\Sphere.zgeMesh
Images="[plugpath]Effects\HUD\prefabs\big circles\big-circle-004.ilv","[presetpath]Wizard\Assets\Sacco\Panel 01 stroke.svg","[presetpath]Wizard\Assets\Sacco\Panel 01.svg","[presetpath]Wizard\Assets\Sacco\Panel 01b black.svg","[presetpath]Wizard\Assets\Sacco\Panel 01b stroke.svg","[presetpath]Wizard\Assets\Sacco\Panel 01b.svg","[presetpath]Wizard\Assets\Sacco\Panel 02.svg"
VideoUseSync=0
EnableMipmap=1

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

