FLhd   0  0 FLdt�  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   ՚  ﻿[General]
GlWindowMode=1
LayerCount=17
FPS=1
MidiPort=-1
Aspect=1
LayerOrder=11,12,0,1,2,3,4,15,5,6,7,8,9,10,13,14,16
WizardParams=803,805

[AppSet11]
App=Peak Effects\VectorScope
ParamValues=516,0,0,0,808,0,2,0
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet12]
App=Image effects\Image
ParamValues=0,0,0,0,600,0,428,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=2

[AppSet0]
App=Physics\Heightfield
ParamValues=200,300,0,372,504,520,1000,1000,0,550,500,500,256,704
Enabled=1
Collapsed=1

[AppSet1]
App=Misc\Automator
ParamValues=0,6,7,2,452,200,516,0,1,11,5,272,378,486,0,4,5,5,672,468,506,0,0,0,0,476,486,470
Enabled=1
Collapsed=1

[AppSet2]
App=Postprocess\ColorCyclePalette
ParamValues=1,2,3,0,9,1000,0,500,0,0,0
Enabled=1
Collapsed=1
ImageIndex=3

[AppSet3]
App=Peak Effects\Polar
ParamValues=453,852,651,585,540,500,500,232,1000,500,395,712,0,500,0,500,0,1000,1000,0,0,964,0,1
Enabled=1
Collapsed=1
ImageIndex=3

[AppSet4]
App=Postprocess\Edge Detect
ParamValues=1000,0,0,1000,0,0
Enabled=1
Collapsed=1

[AppSet15]
App=Text\TextTrueType
ParamValues=0,0,0,904,0,494,497,0,0,0,500
Enabled=1
Collapsed=1

[AppSet5]
App=Text\TextTrueType
ParamValues=0,0,0,0,0,494,500,0,0,0,500
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet6]
App=Canvas effects\Stack Trace
ParamValues=328,0,0,744,0,184,604,0
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet7]
App=Blend\BufferBlender
ParamValues=0,1,9,1000,0,0,0,0,500,500,750,0
Enabled=1
Collapsed=1

[AppSet8]
App=Peak Effects\StereoWaveForm
ParamValues=748,0,0,1000,0,0,1000,0,0,1000,195,136,203,348,500,500,500,504,50,384,1000,1,16,1
Enabled=1
Collapsed=1

[AppSet9]
App=Peak Effects\Linear
ParamValues=744,0,0,40,738,868,396,260,252,500,140,452,0,1,1,1,0,500,500,1000,350,0,1000,1,252,1000,32,212,330,250,100
Enabled=1
Collapsed=1
ImageIndex=3

[AppSet10]
App=Background\ItsFullOfStars
ParamValues=840,0,0,0,788,500,500,564,0,0,0
Enabled=1
Collapsed=1
ImageIndex=3

[AppSet13]
App=Background\FourCornerGradient
ParamValues=6,1000,272,472,104,0,0,28,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet14]
App=Background\Grid
ParamValues=878,0,0,1000,224,224,1000
Enabled=1
Collapsed=1

[AppSet16]
App=Postprocess\Youlean Color Correction
ParamValues=500,500,500,500,500,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0

[UserContent]
Text="This is the default text."
Html="<position x=""3""><position y=""5""><p align=""left""><font face=""American-Captain"" size=""10"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""3""><position y=""13""><p align=""left""><font face=""Chosence-Bold"" size=""6"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""55""><position y=""90""><p align=""right""><font face=""Chosence-Bold"" size=""2"" color=""#FFFFFF"">[extra1]</font></p></position>","<position x=""67""><position y=""94""><p align=""right""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""2""><position y=""92""><p align=""center""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""2""><position y=""95""><p align=""center""><font face=""Chosence-regular"" size=""2"" color=""#FFFFF"">[extra3]</font></p></position>"
VideoUseSync=0
EnableMipmap=1

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

