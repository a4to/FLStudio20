FLhd   0  0 FLdt0  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   ռ+�  ﻿[General]
GlWindowMode=1
LayerCount=20
FPS=1
MidiPort=-1
Aspect=1
LayerOrder=2,0,4,3,6,8,11,12,5,7,9,13,14,15,16,1,17,10,18,19
WizardParams=947,949

[AppSet2]
App=Image effects\ImageWall
ParamValues=0,0,0,0,0,200,200
Enabled=1
Collapsed=1
ImageIndex=12
Name=Background Image

[AppSet0]
App=Image effects\ImageSphereWarp
ParamValues=0,449,524,765,250,487,0,500,500
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=Warb BG

[AppSet4]
App=Image effects\Image
ParamValues=43,0,0,0,1000,637,504,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=2
Name=Right Speaker

[AppSet3]
App=Image effects\Image
ParamValues=43,0,0,0,1000,363,500,0,0,0,0,0,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1
ImageIndex=1
Name=Left Speaker

[AppSet6]
App=Peak Effects\Polar
ParamValues=729,35,1000,0,500,500,500,1000,1000,500,378,535,0,244,1,857,0,1000,1000,1,0,498,0,1
Enabled=1
UseBufferOutput=1
Collapsed=1
ImageIndex=12
Name=Left Woofer

[AppSet8]
App=Peak Effects\Polar
ParamValues=729,35,1000,0,500,461,500,1000,1000,500,378,535,0,749,1,857,0,1000,1000,1,0,498,0,1
Enabled=1
UseBufferOutput=1
Collapsed=1
ImageIndex=12
Name=Right Woofer

[AppSet11]
App=Peak Effects\Polar
ParamValues=439,47,1000,0,43,360,501,249,0,500,249,406,0,500,0,500,0,1000,1000,1,0,0,0,1
Enabled=1
UseBufferOutput=1
Collapsed=1
ImageIndex=12
Name=Left Tweeter

[AppSet12]
App=Peak Effects\Polar
ParamValues=439,47,1000,0,43,339,501,249,0,500,249,406,0,947,0,500,0,1000,1000,0,0,0,0,1
Enabled=1
UseBufferOutput=1
Collapsed=1
ImageIndex=12
Name=Right Tweeter

[AppSet5]
App=Image effects\Image
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=5

[AppSet7]
App=Image effects\Image
ParamValues=0,0,0,0,871,239,544,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=6

[AppSet9]
App=Image effects\Image
ParamValues=0,0,0,0,871,768,552,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=7

[AppSet13]
App=Image effects\Image
ParamValues=0,0,0,0,1000,391,470,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=8

[AppSet14]
App=Image effects\Image
ParamValues=0,0,0,0,1000,669,474,0,0,0,0,0,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1
ImageIndex=9

[AppSet15]
App=Image effects\Image
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=3
Name=Background

[AppSet16]
App=Postprocess\AudioShake
ParamValues=38,0,0,500,100,900
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet1]
App=Image effects\Image
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=Speaker Group

[AppSet17]
App=Blend\BufferBlender
ParamValues=0,1,10,1000,0,0,0,0,500,500,746,0
Enabled=1
Collapsed=1
ImageIndex=10

[AppSet10]
App=Text\TextTrueType
ParamValues=0,0,0,1000,0,500,497,0,0,0,500
Enabled=1
Collapsed=1
Name=Project Info

[AppSet18]
App=Text\TextTrueType
ParamValues=0,0,0,0,0,500,500,0,0,0,500
Enabled=1
Collapsed=1

[AppSet19]
App=Postprocess\Youlean Color Correction
ParamValues=500,500,500,500,500,596
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0

[UserContent]
Text="Soft tantalizing petals                             Of the perfect flower                               Giving peace and calmness                           Over life's ongoing agony                           Hope brings a mind's eye                            Visions of love's heart                             Talks of simple past                                Greater than perfection                             Living an unpredictable life                        Not knowing where you'll be                         Showing unpredictable feelings                      Hoping that you'll soon see                         The moon's penetrating rays                         Lighting the cool night air                         Filling the heart's emptiness                       With something that cares                           Love is a needful thing                             Holding fast and true                               Forever will it sing                                A tune greater than blue...                         Blue Flowers","Russell Sivey",Blue,Flowers,
Html="<position x=""3""><position y=""5""><p align=""center""><font face=""American-Captain"" size=""10"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""3""><position y=""13""><p align=""center""><font face=""Chosence-Bold"" size=""6"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""55""><position y=""90""><p align=""right""><font face=""Chosence-Bold"" size=""2"" color=""#FFFFFF"">[extra1]</font></p></position>","<position x=""67""><position y=""94""><p align=""right""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""2""><position y=""92""><p align=""center""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""2""><position y=""95""><p align=""center""><font face=""Chosence-regular"" size=""2"" color=""#FFFFF"">[extra3]</font></p></position>"
Images=[presetpath]Wizard\Assets\ZGameTemplate_BlurryFLStudio.jpg,[presetpath]Wizard\Assets\Speaker-L.png,[presetpath]Wizard\Assets\Speaker-R.png
VideoUseSync=0
EnableMipmap=1

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

