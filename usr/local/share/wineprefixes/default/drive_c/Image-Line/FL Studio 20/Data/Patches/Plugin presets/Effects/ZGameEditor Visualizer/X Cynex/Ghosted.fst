FLhd   0  ` FLdt�  �	12.2.0.3 �.Z G a m e E d i t o r   V i s u a l i z e r   �4               A                  �  �   �  ~  �    �HQV ձ<-  [General]
GlWindowMode=1
LayerCount=6
FPS=2
DmxOutput=0
DmxDevice=0
MidiPort=-1
Aspect=1
LayerOrder=0,1,2,4,3,5

[AppSet0]
App=Background\SolidColor
ParamValues=645,702,814,946
ParamValuesBackground\FogMachine=0,690,717,0,882,401,273,334,440,1000,1000,314
ParamValuesCanvas effects\DarkSpark=548,0,500,500,500,500,500,500,500,500,0,0
ParamValuesCanvas effects\Flaring=87,489,187,1000,1000,503,500,63,0,400,500
ParamValuesMisc\Automator=1000,185,273,400,608,161,498,1000,148,182,400,576,112,538,1000,111,61,400,627,112,512,1000,185,303,400,378,129,342,0,0,0,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesTerrain\CubesAndSpheres=0,0,0,0,106,445,0,352,265,799,1000,242,1000,1000,97,939,1000,127
ParamValuesTerrain\GoopFlow=150,0,0,347,782,1000,395,0,26,500,0,500,1000,478,374,698
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet1]
App=Terrain\GoopFlow
ParamValues=0,531,360,860,0,500,1000,61,757,506,0,487,1000,716,0,193
ParamValuesBackground\FogMachine=0,166,866,0,1000,586,500,500,500,500,1000,387
ParamValuesBackground\FourCornerGradient=0,1000,0,558,552,538,1000,354,650,994,434,961,1000,603
ParamValuesFeedback\70sKaleido=0,0,0,1000,0,62
ParamValuesFeedback\BoxedIn=0,0,0,1000,500,0,0,500,0,0
ParamValuesFeedback\FeedMe=0,0,0,1000,1000,1000,358
ParamValuesFeedback\SphericalProjection=60,0,0,0,364,425,686,718,775,1000,0,500,500,731,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesObject Arrays\8x8x8_Eggs=0,448,381,0,154,500,500,357,574,644,0,221,221
ParamValuesObject Arrays\Filaments=0,719,140,526,500,500,500,719,596,35,1000,105,316,0
ParamValuesParticles\fLuids=439,930,965,754,754,737,211,1000,754,1000,632,1000,632,877,386,175,158,0,1000,0,825,263,281,684,500,88,70,1000,1000,4,175,211
ParamValuesParticles\ReactiveFlow=0,684,1000,719,719,0,491,140,88,930,316,500,1000,263,368,0,175,1000,105,702,53,140,1000,1000,0
Input=0
Enabled=1
UseBufferOutput=1
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet2]
App=Background\FourCornerGradient
ParamValues=0,1000,557,876,497,701,440,677,570,1000,578,449,565,139
ParamValuesBackground\FogMachine=0,0,0,880,500,500,222,500,500,0,0,0
ParamValuesBlend\BufferBlender=0,111,867,862,250,111,1000,0,500,500,750
ParamValuesImage effects\ImageWarp=0,0,0,297,0,669,803,424
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,860,500,500,500,500,500
ParamValuesParticles\fLuids=0,0,0,0,305,500,500,0,0,0,500,500,500,0,500,250,500,0,0,500,500,500,0,0,500,0,0,250,500,0,0,0
ParamValuesParticles\ReactiveMob=1000,464,1000,0,1000,500,474,401,1000,1000,182,415,550,0,614,1000,690,1000,301,502,0,383,498,1000,0,549,1000,398,0,353
ParamValuesTerrain\GoopFlow=0,0,0,0,596,500,500,0,500,497,241,628,500,1000,420,462
Input=0
Enabled=1
UseBufferOutput=1
BufferRenderQuality=0
ImageIndex=0
MeshIndex=0

[AppSet3]
App=Blend\BufferBlender
ParamValues=0,1,13,1000,1,2,1000,0,500,500,776
ParamValuesBackground\FogMachine=470,0,0,0,0,500,500,500,500,500,0,0
ParamValuesBackground\FourCornerGradient=133,1000,542,1000,1000,504,237,502,901,127,1000,827,1000,686
ParamValuesBackground\ItsFullOfStars=595,0,0,0,536,845,292,350,230,0,0
ParamValuesPostprocess\Blooming=0,253,1000,377,1000,738,1000,623,208
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=1
MeshIndex=0

[AppSet4]
App=Canvas effects\Flaring
ParamValues=0,1000,1000,1000,1000,500,500,152,0,2
ParamValuesBlend\BufferBlender=0,222,800,1000,250,222,0,0,500,500,750
ParamValuesMisc\Automator=1000,111,182,400,448,23,586,1000,111,212,400,531,26,548,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0
ParamValuesPostprocess\Blooming=0,163,688,1000,465,960,199,372,0
Input=0
Enabled=1
UseBufferOutput=1
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet5]
App=Postprocess\Blooming
ParamValues=0,0,0,1000,500,781,802,821,0
ParamValuesCanvas effects\N-gonFigure=0,654,666,0,478,500,500,389,0,1000,0
ParamValuesMisc\Automator=1000,37,182,400,729,93,680,1000,37,212,400,534,87,369,1000,37,152,400,665,231,539,0,74,152,400,550,96,327,0,0,0,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet6]
App=(none)
ParamValues=
ParamValuesFeedback\70sKaleido=525,163,858,1000,1000,263
ParamValuesFeedback\WarpBack=500,0,0,0,500,500,66
ParamValuesMisc\Automator=1000,222,242,400,518,128,436,1000,296,242,400,576,8,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet7]
App=(none)
ParamValues=
ParamValuesCanvas effects\N-gonFigure=0,654,900,0,859,500,500,594,0,1000,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet8]
App=(none)
ParamValues=
ParamValuesCanvas effects\N-gonFigure=0,654,580,0,904,500,500,653,0,699,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet9]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet10]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet11]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet12]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet13]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet14]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet15]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet16]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet17]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet18]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet19]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet20]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet21]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet22]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet23]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet24]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[Video export]
VideoH=480
VideoW=640
VideoRenderFps=30
SampleRate=0
VideoCodec=-1
AudioCodec=-1
AudioCodecFormat=-1
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=0
Uncompressed=0

[UserContent]
Text="You can","change this text","in settings."
Html=
VideoCues=
Meshes=
MeshAutoScale=0
MeshWithColors=0
Images=[plugpath]Content\Bitmaps\Particles\noiseFlare.png
VideoUseSync=0

[Detached]
Top=394
Left=139
Width=689
Height=638

