FLhd   0  ` FLdt�  �20.8.0.2091 �+  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4               A                  :   d   �  B  �    �HQV ��9�  ﻿[General]
GlWindowMode=1
LayerCount=16
FPS=1
AspectRatio=16:9
LayerOrder=2,8,11,0,1,3,4,5,13,10,12,9,7,6,14,15

[AppSet2]
App=Physics\Ragdoll
FParamValues=0,0.994,0,0.765,0.5,0.959,0.087,0
ParamValues=0,994,0,765,500,959,87,0
ParamValuesHUD\HUD Prefab=268,0,0,1000,0,500,500,542,1000,1000,444,0,468,1000,368,0,1000,1000
ParamValuesHUD\HUD Grid=0,234,534,0,500,500,564,1000,372,444,500,395,500,700,461,288,1000,100,500,1000,1000,1000
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet8]
App=Postprocess\Dot Matrix
FParamValues=1,0.64,0.661,0,0,0,1,1
ParamValues=1000,640,661,0,0,0,1000,1000
ParamValuesPostprocess\Youlean Blur=0,0,0
Enabled=1
Collapsed=1

[AppSet11]
App=HUD\HUD Image
FParamValues=0,1,0.5,0.514,1,1,0.233,4,0.5,0,0,1,1,0,1
ParamValues=0,1,500,514,1000,1000,233,4,500,0,0,1000,1000,0,1
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet0]
App=HUD\HUD Prefab
FParamValues=281,0,0.5,0,0,0.5,0.5,0.43,1,1,4,0,0.5,1,0.368,0,1,1
ParamValues=281,0,500,0,0,500,500,430,1000,1000,4,0,500,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78012BCE4DCCC95148CD49CD4DCD2B298E49CE2C4ACE498552BA06C6667A9939650CC318000091C00D72

[AppSet1]
App=HUD\HUD Prefab
FParamValues=281,0,0,1,0,0.5,0.5,0.43,1,1,4,0,0.5,1,0.368,0,0.6218,1
ParamValues=281,0,0,1000,0,500,500,430,1000,1000,4,0,500,1,368,0,621,1
Enabled=1
LayerPrivateData=78012BCE4DCCC95148CD49CD4DCD2B298E49CE2C4ACE498552BA06C6667A9939650CC318000091C00D72

[AppSet3]
App=HUD\HUD Prefab
FParamValues=300,0,0,0,0,0.5,0.5,0.528,1,1,4,0,0,1,0.381,0,1,1
ParamValues=300,0,0,0,0,500,500,528,1000,1000,4,0,0,1,381,0,1000,1
Enabled=1
LayerPrivateData=78012BCE4DCCC95148CD49CD4DCD2B298E49CE2C4ACE498552BA06A6A67A9939650CC318000092A30D73

[AppSet4]
App=HUD\HUD Prefab
FParamValues=269,0,0,1,0,0.5,0.5,0.612,1,1,4,0,0.962,0,0.656,0,1,1
ParamValues=269,0,0,1000,0,500,500,612,1000,1000,4,0,962,0,656,0,1000,1
Enabled=1
LayerPrivateData=78012BCE4DCCC95148CD49CD4DCD2B298E49CE2C4ACE498552BA0646267A9939650CC31800008F1C0D6F

[AppSet5]
App=HUD\HUD Prefab
FParamValues=251,0,0,0,0.77,0.5,0.507,0.787,1,1,4,0,0.5,1,0.368,0,1,1
ParamValues=251,0,0,0,770,500,507,787,1000,1000,4,0,500,1,368,0,1000,1
Enabled=1
LayerPrivateData=78012BCE4DCCC95148CD49CD4DCD2B298E49CE2C4ACE498552BA0606667A9939650CC31800008F1A0D6F

[AppSet13]
App=HUD\HUD Prefab
FParamValues=251,0,0.5,0.099,0,0.497,0.497,0.787,1,1,4,0,0.5,1,0.368,0,1,1
ParamValues=251,0,500,99,0,497,497,787,1000,1000,4,0,500,1,368,0,1000,1
Enabled=1
LayerPrivateData=78012BCE4DCCC95148CD49CD4DCD2B298E49CE2C4ACE498552BA0606667A9939650CC31800008F1A0D6F

[AppSet10]
App=Postprocess\AudioShake
FParamValues=0.196,0,1,0.277,0,0,0
ParamValues=196,0,1,277,0,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet12]
App=Scenes\Cloud Ten
FParamValues=0,0,0,0,1,0,0.123
ParamValues=0,0,0,0,1000,0,123
ParamValuesPostprocess\Blur=0
ParamValuesPostprocess\Youlean Bloom=725,330,327,589
Enabled=1

[AppSet9]
App=Image effects\ImageSphereWarp
FParamValues=0.896,0.5,0.75,1,0.25,1,0,0.5,0.5
ParamValues=896,500,750,1000,250,1000,0,500,500
ParamValuesTerrain\CubesAndSpheres=400,0,0,0,0,500,500,645,763,370,1000,1000,1000,1000,1000,1000,0,0
ParamValuesParticles\ColorBlobs=0,0,0,57,762,500,500,217,257,0
ParamValuesScenes\Cloud Ten=0,0,0,0,0,1000,134,0,0,0,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesBackground\FogMachine=500,0,466,500,802,500,500,647,669,60,872,703
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,892,500,500,500,500,500
ParamValuesCanvas effects\Rain=520,500,688,704,699,500,500,742,702,520,506,906
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesTunnel\TorusJourney=765,0,0,0,0,500,500,600,800,1000,500,40,390,1000,500,60,800,1000,500,1000
Enabled=1
Collapsed=1

[AppSet7]
App=HUD\HUD 3D
FParamValues=1,1,0.486,0.467,0.5,0.5,0.5,0.5,0.437,0.446,0.5,0.5,0.5,0.5,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=1000,1,486,467,500,500,500,500,437,446,500,500,500,500,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
ParamValuesPostprocess\ScanLines=286,286,1000,995,0,0
ParamValuesPostprocess\FrameBlur=60,0,0,106,94,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet6]
App=Misc\Automator
FParamValues=1,2,17,2,0.485,0.123,0.751,1,4,13,5,0,0,0.428,1,5,13,3,0,0.01,0.75,1,5,15,6,0.656,0,0.544
ParamValues=1,2,17,2,485,123,751,1,4,13,5,0,0,428,1,5,13,3,0,10,750,1,5,15,6,656,0,544
ParamValuesPostprocess\AudioShake=0,0,667,1000,125,118
Enabled=1
Collapsed=1

[AppSet14]
App=Text\TextTrueType
FParamValues=0.4347,0,0,1,0,0.493,0.498,0,0,0,0.5
ParamValues=434,0,0,1000,0,493,498,0,0,0,500
Enabled=1

[AppSet15]
App=Text\TextTrueType
FParamValues=0,0,0,0,0,0.493,0.5,0,0,0,0.5
ParamValues=0,0,0,0,0,493,500,0,0,0,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=256000
Uncompressed=0
Supersample=0

[UserContent]
Text="Soft tantalizing petals                             Of the perfect flower                               Giving peace and calmness                           Over life's ongoing agony                           Hope brings a mind's eye                            Visions of love's heart                             Talks of simple past                                Greater than perfection                             Living an unpredictable life                        Not knowing where you'll be                         Showing unpredictable feelings                      Hoping that you'll soon see                         The moon's penetrating rays                         Lighting the cool night air                         Filling the heart's emptiness                       With something that cares                           Love is a needful thing                             Holding fast and true                               Forever will it sing                                A tune greater than blue...                         Blue Flowers","Russell Sivey",Blue,Flowers,
Html="<position x=""4""><position y=""5""><p align=""left""><font face=""American-Captain"" size=""10"" color=""#000000"">[author]</font></p></position>","<position x=""4""><position y=""13""><p align=""left""><font face=""Chosence-Bold"" size=""6"" color=""#000000"">[title]</font></p></position>","<position x=""60""><position y=""93""><p align=""right""><font face=""Chosence-Bold"" size=""2"" color=""#FFFFFF"">[extra1]</font></p></position>","<position x=""67""><position y=""96""><p align=""right""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""2""><position y=""94""><p align=""center""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""2""><position y=""97""><p align=""center""><font face=""Chosence-regular"" size=""2"" color=""#FFFFF"">[extra3]</font></p></position>"
VideoUseSync=0
Filtering=0

[Detached]
Top=204
Left=1226
Width=540
Height=414

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

