FLhd   0 * ` FLdt  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��2w  ﻿[General]
GlWindowMode=1
LayerCount=19
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,9,10,11,6,12,15,14,13,8,7,16,1,4,3,2,5,17,18
WizardParams=517,518,519,520,521,705,769

[AppSet0]
App=Background\SolidColor
FParamValues=0,1,0.84,0.092
ParamValues=0,1000,840,92
Enabled=1
Collapsed=1
Name=Background FX

[AppSet9]
App=HUD\HUD Grid
AppVersion=1
FParamValues=0,0,0,1,0.504,0.536,1,1,0.828,4,0.5,1,0.5,0.66,0.544,0.176,0.844,0.048,0.4,0.216,0.496,1
ParamValues=0,0,0,1000,504,536,1000,1000,828,4,500,1000,500,660,544,176,844,48,400,216,496,1
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=Grid FX

[AppSet10]
App=HUD\HUD Prefab
FParamValues=0,0.08,0.5,0,0.928,0.502,0.658,0.349,1,1,4,0,0.5,1,0.368,0.096,1,1
ParamValues=0,80,500,0,928,502,658,349,1000,1000,4,0,500,1,368,96,1000,1
ParamValuesHUD\HUD Graph Radial=0,500,0,0,200,500,250,444,500,0,1000,0,1000,0,250,500,200,0,0,0,500,1000
ParamValuesHUD\HUD Meter Radial=0,248,1000,0,0,0,0,992,492,708,156,34,1000,0,374,0,1000,1000,500,1000
ParamValuesHUD\HUD Free Line=0,500,0,0,656,552,168,304,182,92,182,168,1000,312,0,280,500
ParamValuesHUD\HUD Graph Linear=0,500,0,0,816,468,1000,1000,250,444,500,1000,212,1000,0,500,200,0,0,0,500,1000
ParamValuesHUD\HUD Meter Linear=0,500,0,0,0,0,750,0,800,664,300,100,0,125,500,1,238,0,0,1000,612,1000
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=LOGO
LayerPrivateData=780173C8CBCF4BD52B2E4B671899000060F9036F

[AppSet11]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0.044,0.004,0,0,0.5,0.221,1,0.556,1,4,0.5,1,0.34,0.524,0,0.624,0.076,0.32,0,1,0,1
ParamValues=44,4,0,0,500,221,1000,556,1000,4,500,1,340,524,0,624,76,320,0,1,0,1
ParamValuesHUD\HUD Free Line=0,500,0,0,656,552,168,304,182,92,182,168,1000,312,0,280,500
Enabled=1
Collapsed=1
Name=EQ UP

[AppSet6]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0.044,0,0,0,0.5,0.777,1,0.556,1,4,1,1,0.34,0.524,0,0.564,0.076,0.32,0,1,0,1
ParamValues=44,0,0,0,500,777,1000,556,1000,4,1000,1,340,524,0,564,76,320,0,1,0,1
ParamValuesHUD\HUD Text=60,500,0,1000,660,686,500,552,1000,1000,324,1,240,0,0,750,1000,536,504,1000
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=EQ DOWN

[AppSet12]
App=Background\SolidColor
FParamValues=0,0.524,0,0
ParamValues=0,524,0,0
Enabled=1
Collapsed=1
Name=Background Main

[AppSet15]
App=HUD\HUD Prefab
FParamValues=9,0.3,0.5,0,0.488,0.5,0.5,0.3,1,1,4,0,0.5,0,0.332,0,1,1
ParamValues=9,300,500,0,488,500,500,300,1000,1000,4,0,500,0,332,0,1000,1
Enabled=1
Collapsed=1
Name=Border BG
LayerPrivateData=78014B4A4CCE4E2FCA2FCD4B89490232750D0C2CF53273CA18460A0000F7BC084D

[AppSet14]
App=Image effects\Image
FParamValues=0,0,1,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,1000,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=5

[AppSet13]
App=Image effects\Image
FParamValues=0,0,0,0,0.711,0.5,0.501,0.042,0,0.25,0,0,0,0
ParamValues=0,0,0,0,711,500,501,42,0,250,0,0,0,0
ParamValuesImage effects\ImageWall=0,0,0,0,0,0,0
ParamValuesHUD\HUD Image=0,0,500,500,1000,1000,500,444,500,0,0,1000,1000,500,1000
ParamValuesBackground\SolidColor=0,892,1000,124
ParamValuesImage effects\ImageSlices=0,0,0,0,500,496,488,0,0,0,500,269,1000,500,0,0,0
Enabled=1
Collapsed=1
ImageIndex=6
Name=LCD - Mask

[AppSet8]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=7

[AppSet7]
App=Text\TextTrueType
FParamValues=0.456,0,0,1,0,0.493,0.498,0,0,0,0.5
ParamValues=456,0,0,1000,0,493,498,0,0,0,500
Enabled=1
Collapsed=1
Name=Main Text

[AppSet16]
App=Text\TextTrueType
FParamValues=0,0,0,0,0,0.493,0.5,0,0,0,0.5
ParamValues=0,0,0,0,0,493,500,0,0,0,500
Enabled=1
Collapsed=1

[AppSet1]
App=Image effects\Image
FParamValues=0,0,0,0.864,0.947,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,864,947,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=2
Name=iPhone - Buttons

[AppSet4]
App=Image effects\Image
FParamValues=0.708,0,0,0,0.952,0.501,0.5,0,0.004,0,0,0,0,0
ParamValues=708,0,0,0,952,501,500,0,4,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=4
Name=iPhone - Glass

[AppSet3]
App=Image effects\Image
FParamValues=0,0,0,0.936,0.972,0.5,0.514,0,0,0,0,0,0,0
ParamValues=0,0,0,936,972,500,514,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=iPhone - Border 2

[AppSet2]
App=Image effects\Image
FParamValues=0,0.02,0.888,0.124,0.947,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,20,888,124,947,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
Name=iPhone - Border 1

[AppSet5]
App=Image effects\Image
FParamValues=0,0,0,0,0.95,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,950,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=3
Name=iPhone - FaceID

[AppSet17]
App=Background\FourCornerGradient
FParamValues=12,0.112,0.924,0.68,1,0.638,0.668,1,0.784,1,1,0.794,0.468,1
ParamValues=12,112,924,680,1000,638,668,1000,784,1000,1000,794,468,1000
ParamValuesHUD\HUD Meter Linear=0,208,884,1000,0,0,0,824,128,753,224,68,0,11,500,0,584,560,404,364,568,1000
Enabled=0
Collapsed=1
Name=Filter Color

[AppSet18]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fade-in and out

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""3"" y=""2""><p align=""left""><font face=""American-Captain"" size=""8"" color=""#333333"">[author]</font></p></position>","<position x=""3"" y=""10""><p align=""left""><b><font face=""Chosence-Bold"" size=""6"" color=""#333333"">[title]</font></b></p></position>","<position y=""92""><p align=""right""><b><font face=""Chosence-Bold"" size=""3"" color=""#333333"">[comment]</font></b></p></position>",," ",,," ",,,
Images=[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Border-1.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Border-2.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Buttons.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Face-id.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Glass.svg
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

