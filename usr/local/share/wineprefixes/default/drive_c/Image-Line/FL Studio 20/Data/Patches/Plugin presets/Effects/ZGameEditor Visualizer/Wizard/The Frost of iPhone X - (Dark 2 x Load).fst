FLhd   0 * ` FLdt�%  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��JT%  ﻿[General]
GlWindowMode=1
LayerCount=19
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,16,8,9,11,6,12,15,13,18,7,14,10,1,4,3,2,5,17
WizardParams=33,517,518,519,520,521,657

[AppSet0]
App=Background\SolidColor
FParamValues=0,0.564,0.84,0.904
ParamValues=0,564,840,904
Enabled=1
Collapsed=1
Name=Background FX

[AppSet16]
App=Peak Effects\WaveSimple
FParamValues=0,0.56,1,0,0.424,0.5,0.524,1,0
ParamValues=0,560,1000,0,424,500,524,1000,0
ParamValuesHUD\HUD Prefab=31,0,500,0,852,500,856,1000,1000,1000,444,0,1000,1000,200,0,1000,1000
ParamValuesPeak Effects\StereoWaveForm=0,556,1000,1000,0,1000,1000,1000,1000,1000,1000,344,272,498,500,588,500,424,1000,664,604,1000,0,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPeak Effects\PeekMe=0,0,0,1000,500,500,500,230,0,0,0,0
ParamValuesHUD\HUD Text=0,500,0,0,168,344,500,0,0,0,0,0,100,0,0,750,1000,500,500,1000
ParamValuesPeak Effects\Polar=0,0,0,0,188,36,572,228,1000,856,596,448,0,500,0,500,0,1000,1000,1000,320,480,1000,1000
ParamValuesHUD\HUD Grid=148,144,0,0,500,500,1000,1000,1000,444,500,1000,500,888,232,500,1000,0,784,0,392,1000
ParamValuesParticles\fLuids=0,0,0,0,500,0,500,0,0,0,500,500,500,0,500,250,500,0,0,500,500,500,0,0,500,0,0,250,500,0,0,0
ParamValuesParticles\PlasmaFlys=500,500,500,0,1000,500,500,500,0,500,500,500,500,500
ParamValuesFeedback\BoxedIn=0,0,0,1000,500,48,228,500,0,500
ParamValuesPeak Effects\VectorScope=0,333,1000,1000,1000,225,667,0
ParamValuesPostprocess\ScanLines=0,0,0,0,0,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesHUD\HUD Meter Linear=0,208,884,1000,0,0,0,824,128,753,224,68,0,11,500,0,584,560,404,364,568,1000
ParamValuesObject Arrays\Rings=0,0,0,1000,768,500,500,568,500,0,0,500,0,0,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesFeedback\70sKaleido=0,0,212,1000,208,500
ParamValuesPeak Effects\JoyDividers=1000,624,1000,0,700,500,500,0,500,100,600,650,510
ParamValuesPeak Effects\Stripe Peeks=0,200,1000,1000,0,0,86,264,500,0,250,500,612,0,500,250,200,0,150,1000,1000,300,0
ParamValuesObject Arrays\DiamondBit=500,500,500,0,734,500,500,1000,0,500,1000,0,232
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,158,500,500,500,500,500
Enabled=1
Collapsed=1
ImageIndex=7
Name=Grider

[AppSet8]
App=Postprocess\Youlean Motion Blur
FParamValues=0.232,1,0.636,0.956,0,0,0
ParamValues=232,1,636,956,0,0,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesHUD\HUD Text=44,500,0,0,652,520,500,552,1000,1000,324,1,240,0,0,750,1000,536,504,1000
ParamValuesHUD\HUD Grid=148,144,0,0,500,500,1000,1000,1000,444,500,1000,500,888,232,500,1000,0,784,0,392,1000
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPostprocess\Youlean Bloom=548,708,326,282
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesObject Arrays\Filaments=1000,0,0,1000,500,500,500,500,500,500,0,0,500,0
ParamValuesObject Arrays\CubicMatrix=0,0,0,1000,932,500,500,500,500,0,0,0,0
ParamValuesObject Arrays\CubesGrasping=0,0,1000,1000,500,500,500,500,500,500,500
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,15,500,500,500,500,500
ParamValuesPostprocess\Blur=1000
ParamValuesPostprocess\Youlean Blur=500,1000,0
ParamValuesHUD\HUD Free Line=0,500,0,964,660,492,388,304,182,92,182,220,1000,312,0,272,500
ParamValuesPostprocess\Youlean Color Correction=520,392,792,500,1000,136
Enabled=1
Collapsed=1
ImageIndex=7
Name=Grid FX

[AppSet9]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.644,0,0.414,0.067,1,0,0,0
ParamValues=644,0,414,67,1000,0,0,0
ParamValuesPostprocess\Youlean Motion Blur=232,1000,636,956
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=Bloom Day

[AppSet11]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0.044,0.54,0,0,0.5,0.221,1,0.556,1,4,0.5,1,0.34,0.524,0,0.624,0.076,0.32,0,1,0,1
ParamValues=44,540,0,0,500,221,1000,556,1000,4,500,1,340,524,0,624,76,320,0,1,0,1
ParamValuesHUD\HUD Free Line=0,500,0,0,656,552,168,304,182,92,182,168,1000,312,0,280,500
Enabled=1
Collapsed=1
Name=EQ UP

[AppSet6]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0.044,0.54,0,0,0.5,0.777,1,0.556,1,4,1,1,0.34,0.524,0,0.564,0.076,0.32,0,1,0,1
ParamValues=44,540,0,0,500,777,1000,556,1000,4,1000,1,340,524,0,564,76,320,0,1,0,1
ParamValuesHUD\HUD Text=60,500,0,1000,660,686,500,552,1000,1000,324,1,240,0,0,750,1000,536,504,1000
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=EQ DOWN

[AppSet12]
App=Background\SolidColor
FParamValues=0,0.524,0,0.948
ParamValues=0,524,0,948
Enabled=1
Collapsed=1
Name=Background Main

[AppSet15]
App=HUD\HUD Prefab
FParamValues=23,0.736,0.5,0,0.488,0.5,0.5,0.28,1,1,4,0,0.5,0,0.292,0,1,1
ParamValues=23,736,500,0,488,500,500,280,1000,1000,4,0,500,0,292,0,1000,1
Enabled=1
Collapsed=1
Name=Border BG
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0CCCF43273CA18863D00000D540AA1

[AppSet13]
App=Image effects\Image
FParamValues=0,0.544,1,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,544,1000,0,1000,500,500,0,0,0,0,0,0,0
ParamValuesImage effects\ImageWall=0,0,0,0,0,0,0
ParamValuesHUD\HUD Image=0,0,500,500,1000,1000,500,444,500,0,0,1000,1000,500,1000
ParamValuesBackground\SolidColor=0,892,1000,124
ParamValuesImage effects\ImageSlices=0,0,0,0,500,496,488,0,0,0,500,269,1000,500,0,0,0
Enabled=1
Collapsed=1
ImageIndex=5
Name=LCD - Mask

[AppSet18]
App=Image effects\Image
FParamValues=0,0,0,0,0.711,0.5,0.501,0.042,0,0.25,0,0,0,0
ParamValues=0,0,0,0,711,500,501,42,0,250,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=6

[AppSet7]
App=Text\TextTrueType
FParamValues=0.492,0,0,1,0,0.493,0.498,0,0,0,0.5
ParamValues=492,0,0,1000,0,493,498,0,0,0,500
Enabled=1
Collapsed=1
Name=Main Text

[AppSet14]
App=Text\TextTrueType
FParamValues=0,0,0,0,0,0.493,0.5,0,0,0,0.5
ParamValues=0,0,0,0,0,493,500,0,0,0,500
Enabled=1
Collapsed=1

[AppSet10]
App=HUD\HUD Prefab
FParamValues=0,0.092,0.52,0,0.192,0.502,0.66,0.25,1,1,4,0,0.5,1,0.368,0.104,1,1
ParamValues=0,92,520,0,192,502,660,250,1000,1000,4,0,500,1,368,104,1000,1
ParamValuesHUD\HUD Graph Radial=0,500,0,0,200,500,250,444,500,0,1000,0,1000,0,250,500,200,0,0,0,500,1000
ParamValuesHUD\HUD Meter Radial=0,248,1000,0,0,0,0,992,492,708,156,34,1000,0,374,0,1000,1000,500,1000
ParamValuesHUD\HUD Free Line=0,500,0,0,656,552,168,304,182,92,182,168,1000,312,0,280,500
ParamValuesHUD\HUD Graph Linear=0,500,0,0,816,468,1000,1000,250,444,500,1000,212,1000,0,500,200,0,0,0,500,1000
ParamValuesHUD\HUD Meter Linear=0,500,0,0,0,0,750,0,800,664,300,100,0,125,500,1,238,0,0,1000,612,1000
Enabled=1
Name=LOGO
LayerPrivateData=780173C8CBCF4BD52B2E4B671899000060F9036F

[AppSet1]
App=Image effects\Image
FParamValues=0,0,0,0.64,0.947,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,640,947,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=2
Name=iPhone - Buttons

[AppSet4]
App=Image effects\Image
FParamValues=0.708,0,0,0,0.952,0.501,0.5,0,0.004,0,0,0,0,0
ParamValues=708,0,0,0,952,501,500,0,4,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=4
Name=iPhone - Glass

[AppSet3]
App=Image effects\Image
FParamValues=0,0,0,0.936,0.972,0.5,0.514,0,0,0,0,0,0,0
ParamValues=0,0,0,936,972,500,514,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=iPhone - Border 2

[AppSet2]
App=Image effects\Image
FParamValues=0,0.544,0.888,0.124,0.947,0.5,0.501,0,0,0,0,0,0,0
ParamValues=0,544,888,124,947,500,501,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
Name=iPhone - Border 1

[AppSet5]
App=Image effects\Image
FParamValues=0,0,0,0,0.95,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,950,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=3
Name=iPhone - FaceID

[AppSet17]
App=Background\FourCornerGradient
FParamValues=7,0,0,0.424,0.712,0.792,0.48,0.752,0.656,0.836,1,0.614,0.468,0.804
ParamValues=7,0,0,424,712,792,480,752,656,836,1000,614,468,804
ParamValuesHUD\HUD Meter Linear=0,208,884,1000,0,0,0,824,128,753,224,68,0,11,500,0,584,560,404,364,568,1000
Enabled=1
Collapsed=1
Name=Filter Color

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""3"" y=""2""><p align=""left""><font face=""American-Captain"" size=""8"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""3"" y=""10""><p align=""left""><b><font face=""Chosence-Bold"" size=""6"" color=""#FFFFFF"">[title]</font></b></p></position>","<position y=""87.1""><p align=""right""><b><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[comment]</font></b></p></position>",,,,," ",,," ",,,
Images=[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Border-1.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Border-2.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Buttons.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Face-id.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Glass.svg
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

