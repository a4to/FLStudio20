FLhd   0 * ` FLdt�  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV մ20  ﻿[General]
GlWindowMode=1
LayerCount=20
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=4,7,16,6,5,0,1,2,11,17,18,3,10,9,8,12,14,13,15,19
WizardParams=947

[AppSet4]
App=Image effects\Image
FParamValues=0,0,0,0.496,0,0.7,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,496,0,700,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=Watch - Buttons

[AppSet7]
App=Image effects\Image
FParamValues=0,0.632,0.54,0.628,0.862,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,632,540,628,862,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=2
Name=Watch - Strap

[AppSet16]
App=Image effects\Image
FParamValues=0.068,0.632,0.76,0.396,0.554,0.5,0.5,0,0,0.352,0,0,0,0
ParamValues=68,632,760,396,554,500,500,0,0,352,0,0,0,0
Enabled=0
Collapsed=1
ImageIndex=5
Name=Watch - Ray

[AppSet6]
App=Image effects\Image
FParamValues=0.812,0,0,0.848,0.548,0.5,0.5,0,0,0,1,0,0,0
ParamValues=812,0,0,848,548,500,500,0,0,0,1,0,0,0
Enabled=1
Collapsed=1
ImageIndex=3
Name=Watch - Glass

[AppSet5]
App=Image effects\Image
FParamValues=0,0.604,0.668,0.736,0.568,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,604,668,736,568,500,500,0,0,0,1,0,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=Watch - Border

[AppSet0]
App=Canvas effects\Stack Trace
FParamValues=0,0.556,1,0.5,0.13,0.5,0.5,0
ParamValues=0,556,1000,500,130,500,500,0
ParamValuesCanvas effects\OverlySatisfying=0,740,484,560,600,500,500,100,100,1000,500
ParamValuesBackground\SolidColor=0,616,1000,224
ParamValuesCanvas effects\Digital Brain=0,0,0,0,1000,500,500,600,716,300,100,250,1000,500,1000,0
Enabled=1
Collapsed=1
Name=Effects BG 1

[AppSet1]
App=Postprocess\FrameBlur
FParamValues=0.824,0.48,0,0.412,0.386
ParamValues=824,480,0,412,386
ParamValuesCanvas effects\TaffyPulls=576,604,1000,0,0,500,500,100,500,500,0,0,0,500
ParamValuesCanvas effects\ShimeringCage=0,0,0,0,72,500,500,0,716,0,84,212,0,200
Enabled=1
UseBufferOutput=1
BufferRenderQuality=6
Collapsed=1
Name=Effects BG 2

[AppSet2]
App=Background\SolidColor
FParamValues=0,0.648,0.376,0.664
ParamValues=0,648,376,664
Enabled=1
Collapsed=1
Name=Background

[AppSet11]
App=HUD\HUD Prefab
FParamValues=200,0.376,0.5,0,1,0.5,0.5,0.594,1,1,4,0,0.5,1,0.616,0.484,1,1
ParamValues=200,376,500,0,1000,500,500,594,1000,1000,4,0,500,1,616,484,1000,1
ParamValuesBackground\SolidColor=0,0,0,336
ParamValuesHUD\HUD Grid=908,500,0,1000,500,500,1000,1000,1000,444,500,1000,500,428,0,812,1000,176,500,0,0,1000
Enabled=1
Collapsed=1
Name=Grid
LayerPrivateData=78012B48CC4BCD298E290051BA0606E67A9939650C23080000EDB3072D

[AppSet17]
App=HUD\HUD Graph Polar
FParamValues=0,0.5,0,1,0.5,0.5,0.322,4,0.596,0,1,0.232,1,0.208,0.858,1,0.66,0.28,0.264,2,0.144,1
ParamValues=0,500,0,1000,500,500,322,4,596,0,1,232,1000,208,858,1000,660,280,264,2,144,1
ParamValuesBackground\SolidColor=0,0,0,336
ParamValuesHUD\HUD Graph Linear=0,500,0,0,500,500,1000,1000,718,444,500,1000,0,1000,0,500,200,0,0,0,500,1000
ParamValuesHUD\HUD Meter Linear=0,500,0,0,0,0,602,0,500,500,300,100,0,188,0,0,250,0,0,1000,500,1000
ParamValuesHUD\HUD Grid=0,500,0,0,500,500,1000,1000,1000,444,500,1000,500,100,0,500,1000,0,676,0,0,1000
Enabled=1
Collapsed=1
Name=EQ OMG

[AppSet18]
App=Image effects\Image
FParamValues=0,0,0,0,0.55,0.435,0.5,0,0,0,0,1,0,1
ParamValues=0,0,0,0,550,435,500,0,0,0,0,1,0,1000
Enabled=1
Collapsed=1
ImageIndex=6
Name=Watch - Effects 1

[AppSet3]
App=Image effects\Image
FParamValues=0,0,0,0,0.55,0.5,0.5,0,0,0,0,1,0,1
ParamValues=0,0,0,0,550,500,500,0,0,0,0,1,0,1000
Enabled=1
Collapsed=1
ImageIndex=6
Name=Watch - Effects 2

[AppSet10]
App=Image effects\Image
FParamValues=0,0,0,0,0.55,0.565,0.5,0,0,0,0,1,0,1
ParamValues=0,0,0,0,550,565,500,0,0,0,0,1,0,1000
Enabled=1
Collapsed=1
ImageIndex=6
Name=Watch - Effects 3

[AppSet9]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.437,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,437,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=7
Name=Main Watch 1

[AppSet8]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=7
Name=Main Watch 2

[AppSet12]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.563,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,563,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=7
Name=Main Watch 3

[AppSet14]
App=Text\TextTrueType
FParamValues=0,0,0,0,0,0.5,0.5,0,0,0,0.5
ParamValues=0,0,0,0,0,500,500,0,0,0,500
Enabled=1
Collapsed=1
Name=Main Text

[AppSet13]
App=Background\FourCornerGradient
FParamValues=13,0.192,0.496,0.696,1,0.67,0.68,1,0.76,0.656,1,0.842,0.8,0.816
ParamValues=13,192,496,696,1000,670,680,1000,760,656,1000,842,800,816
Enabled=1
Collapsed=1
Name=Filter Color

[AppSet15]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fide in-out

[AppSet19]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position y=""5""><p align=""center""><font face=""American-Captain"" size=""5.5"" color=""#E5E5E5"">[author]</font></p></position>","<position y=""9""><p align=""center""><font face=""Chosence-Bold"" size=""3.2"" color=""#E5E5E5"">[title]</font></p></position>","<position y=""16""><p align=""center""><font face=""Chosence-Bold"" size=""3"" color=""#E5E5E5"">[comment]</font></p></position>","  "
Images="[presetpath]Wizard\ColoveContent\Others\COLOVE Watch\The Watch X2 by COLOVE - Border.svg","[presetpath]Wizard\ColoveContent\Others\COLOVE Watch\The Watch X2 by COLOVE - Buttons.svg","[presetpath]Wizard\ColoveContent\Others\COLOVE Watch\The Watch X2 by COLOVE - Strap.svg","[presetpath]Wizard\ColoveContent\Others\COLOVE Watch\The Watch X2 by COLOVE - Glass.svg","[presetpath]Wizard\ColoveContent\Others\COLOVE Watch\The Watch X2 by COLOVE - Ray 1.svg","[presetpath]Wizard\ColoveContent\Others\COLOVE Watch\The Watch X2 by COLOVE - Ray 2.svg"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

