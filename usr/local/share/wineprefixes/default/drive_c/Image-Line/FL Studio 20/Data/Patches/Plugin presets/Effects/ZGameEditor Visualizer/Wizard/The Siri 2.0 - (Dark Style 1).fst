FLhd   0 * ` FLdtG#  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV սE�"  ﻿[General]
GlWindowMode=1
LayerCount=21
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=17,8,6,2,7,1,3,5,4,15,14,16,0,11,9,10,13,12,18,19,20
WizardParams=853,854,855,856,857,995

[AppSet17]
App=HUD\HUD Prefab
FParamValues=0,0,0.5,0,0,0.812,0.263,0.51,1,1,4,0,0.5,1,0.368,0.101,1,1
ParamValues=0,0,500,0,0,812,263,510,1000,1000,4,0,500,1,368,101,1000,1
Enabled=1
UseBufferOutput=1
Name=LOGO
LayerPrivateData=780173C8CBCF4BD52B2E4B671899000060F9036F

[AppSet8]
App=Peak Effects\Linear
FParamValues=0,0,1,0,0.646,0.5,0.468,0.052,0,0.512,0,0.084,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,0.62,0.18,0.224,0.992,0.348,0.54,0.244,0.336,0.2
ParamValues=0,0,1000,0,646,500,468,52,0,512,0,84,500,1,0,1,0,500,500,500,500,0,620,180,224,992,348,540,244,336,200
Enabled=1
ImageIndex=2
Name=EQ LINE 1

[AppSet6]
App=Peak Effects\Linear
FParamValues=0.508,1,1,0.652,0.646,0.5,0.468,0.108,0.5,0.512,0,0.084,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,0.564,0.16,0.16,1,0.164,0.54,0.244,0.336,0.2
ParamValues=508,1000,1000,652,646,500,468,108,500,512,0,84,500,1,0,1,0,500,500,500,500,0,564,160,160,1000,164,540,244,336,200
Enabled=1
Collapsed=1
ImageIndex=2
Name=EQ LINE 2

[AppSet2]
App=Peak Effects\Linear
FParamValues=0.848,0.496,0.612,0,0.646,0.5,0.468,0.136,0,0.512,0,0.084,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,0.604,0.18,0.224,1,0.304,0.54,0.244,0.336,0.2
ParamValues=848,496,612,0,646,500,468,136,0,512,0,84,500,1,0,1,0,500,500,500,500,0,604,180,224,1000,304,540,244,336,200
Enabled=1
Collapsed=1
ImageIndex=2
Name=EQ LINE 3

[AppSet7]
App=Peak Effects\Linear
FParamValues=0.66,0.688,1,0,0.646,0.5,0.468,0.056,0,0.512,0,0.076,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,0.564,0.16,0.16,1,0.304,0.54,0.244,0.336,0.2
ParamValues=660,688,1000,0,646,500,468,56,0,512,0,76,500,1,0,1,0,500,500,500,500,0,564,160,160,1000,304,540,244,336,200
Enabled=1
Collapsed=1
ImageIndex=2
Name=EQ LINE 4

[AppSet1]
App=Peak Effects\Linear
FParamValues=0.476,0.356,0.623,0.193,0.646,0.5,0.468,0.128,0.5,0.512,0,0.048,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,1,0.18,0.224,0.472,0.304,0.54,0.244,0.336,0.2
ParamValues=476,356,623,193,646,500,468,128,500,512,0,48,500,1,0,1,0,500,500,500,500,0,1000,180,224,472,304,540,244,336,200
ParamValuesPostprocess\AudioShake=0,0,0,500,100,900
Enabled=1
Collapsed=1
ImageIndex=2
Name=EQ LINE 5

[AppSet3]
App=Peak Effects\Linear
FParamValues=0.636,0.648,0.636,0,0.646,0.5,0.468,0,0.5,0.512,0,0.312,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,0.5,0.18,0.224,0.064,0,0.54,0.244,0.336,0.2
ParamValues=636,648,636,0,646,500,468,0,500,512,0,312,500,1,0,1,0,500,500,500,500,0,500,180,224,64,0,540,244,336,200
Enabled=1
Collapsed=1
ImageIndex=2
Name=EQ LINE 6

[AppSet5]
App=Peak Effects\Linear
FParamValues=0.656,0.408,0.56,0.452,0.646,0.5,0.468,0.056,0,0.512,0,0.168,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0.324,0.564,0,0.16,1,0.104,0.54,0.244,0.336,0.2
ParamValues=656,408,560,452,646,500,468,56,0,512,0,168,500,1,0,1,0,500,500,500,500,324,564,0,160,1000,104,540,244,336,200
Enabled=1
Collapsed=1
ImageIndex=2
Name=EQ LINE 7

[AppSet4]
App=Peak Effects\Linear
FParamValues=0.5,0.488,1,0,0.646,0.5,0.468,0.16,0.5,0.512,0,0.052,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,0.564,0.064,0.132,0.908,0.22,0.54,0.244,0.336,0.2
ParamValues=500,488,1000,0,646,500,468,160,500,512,0,52,500,1,0,1,0,500,500,500,500,0,564,64,132,908,220,540,244,336,200
Enabled=1
Collapsed=1
ImageIndex=2
Name=EQ LINE 8

[AppSet15]
App=Peak Effects\Linear
FParamValues=0.512,1,0,0.496,0.678,0.5,0.468,0.24,0,0.512,0,0.104,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,0.564,0.192,0,0.892,0,0.54,0.244,0.336,0.2
ParamValues=512,1000,0,496,678,500,468,240,0,512,0,104,500,1,0,1,0,500,500,500,500,0,564,192,0,892,0,540,244,336,200
Enabled=1
Collapsed=1
ImageIndex=2
Name=EQ LINE 9

[AppSet14]
App=Postprocess\Blooming
FParamValues=0,0,0,1,0,1,0.284,0.536,0
ParamValues=0,0,0,1000,0,1000,284,536,0
ParamValuesPeak Effects\Linear=872,780,0,0,646,500,468,168,500,512,0,100,500,1000,0,1000,0,500,500,500,500,0,564,160,160,1000,304,540,244,336,200
ParamValuesFeedback\70sKaleido=0,0,0,1000,0,164
ParamValuesFeedback\FeedMe=0,0,0,640,0,636,412
Enabled=1
Collapsed=1
Name=EFX IN

[AppSet16]
App=Postprocess\Youlean Motion Blur
FParamValues=0.12,1,0.516,0.372,0,0,0
ParamValues=120,1,516,372,0,0,0
ParamValuesPostprocess\Blooming=0,0,0,1000,0,1000,284,536,0
Enabled=1
UseBufferOutput=1
Collapsed=1
ImageIndex=2
Name=EFX OUT

[AppSet0]
App=Background\SolidColor
FParamValues=0,0.62,0.736,0.872
ParamValues=0,620,736,872
ParamValuesCanvas effects\Digital Brain=0,0,0,0,1000,500,500,600,100,300,100,582,1000,484,1000,0
Enabled=1
Collapsed=1
Name=Siri BG

[AppSet11]
App=Postprocess\ScanLines
FParamValues=0,1,0.308,0.308,0
ParamValues=0,1,308,308,0
ParamValuesBackground\FourCornerGradient=467,0,620,1000,484,634,704,656,268,1000,1000,690,1000,1000
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPostprocess\Blooming=0,0,0,1000,500,800,4,328,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesPostprocess\ColorCyclePalette=0,0,0,0,467,0,296,756,0,0,0
ParamValuesPostprocess\Edge Detect=424,0,1000,472,588,604
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPostprocess\Point Cloud Default=0,742,434,500,500,448,408,487,330,625,156,0,0,0,0
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPostprocess\Luminosity=544
ParamValuesFeedback\FeedMe=0,0,428,448,500,608,460
ParamValuesFeedback\70sKaleido=0,0,568,352,244,0
ParamValuesPeak Effects\SplinePeaks=0,20,950,1000,812,432,1000,0,0,0
ParamValuesPeak Effects\ReflectedPeeks=0,500,500,1000,500,500,500,500,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,423,500,500,500,500,500
ParamValuesPostprocess\Blur=1000
ParamValuesPostprocess\FrameBlur=764,0,804,372,374,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesFeedback\FeedMeFract=0,0,0,1000,0,8
ParamValuesPostprocess\ParameterShake=512,784,20,31,332,0,20,94,0
Enabled=1
Collapsed=1
Name=Grid

[AppSet9]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.501,0,0,0,1,1,0,0
ParamValues=0,0,0,0,1000,500,501,0,0,0,1,1,0,0
ParamValuesPeak Effects\Linear=940,492,1000,0,538,500,468,60,500,512,0,110,500,1000,0,1000,0,500,500,500,500,0,1000,180,224,992,348,540,244,336,200
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,0,0,0,500,500,500,500
ParamValuesPostprocess\Youlean Pixelate=588,667,0,0
ParamValuesPostprocess\Youlean Blur=104,0,508
ParamValuesPostprocess\Youlean Bloom=500,120,698,1000
ParamValuesPostprocess\Youlean Color Correction=392,504,472,500,600,540
ParamValuesFeedback\FeedMe=0,0,0,944,0,692,732
Enabled=1
Name=Siri EQ Out

[AppSet10]
App=Background\FourCornerGradient
FParamValues=7,0,0.016,0.732,0.484,0.634,0.704,0.656,0.02,0.312,0,0.69,0,0
ParamValues=7,0,16,732,484,634,704,656,20,312,0,690,0,0
Enabled=1
Collapsed=1
Name=Filter Color HUE

[AppSet13]
App=Text\TextTrueType
FParamValues=0.076,0,0,0,0,0.5,0.5,0,0,0,0.5
ParamValues=76,0,0,0,0,500,500,0,0,0,500
Enabled=1
Collapsed=1
Name=Main Text

[AppSet12]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
ParamValuesPostprocess\Vignette=0,0,360,600,700,664
ParamValuesPostprocess\Youlean Motion Blur=372,1000,96,300
ParamValuesPostprocess\Youlean Bloom=328,764,238,224
ParamValuesBackground\FourCornerGradient=467,0,620,1000,484,634,704,656,268,1000,1000,690,1000,1000
ParamValuesPostprocess\TransitionEffects=500,500,500,500,500,500,500,500,500,500,0,0,0,1000
Enabled=1
Collapsed=1
Name=Fade in-out

[AppSet18]
App=Image effects\Image
FParamValues=0,0,0,1,1,0.5,0.501,0,0,0,0,0,0,0
ParamValues=0,0,0,1000,1000,500,501,0,0,0,0,0,0,0
Enabled=1
ImageIndex=1

[AppSet19]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=1

[AppSet20]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""4"" y=""5""><p><font face=""American-Captain"" size=""4"" color=""#fff"">[author]</font></p></position>","<position x=""4"" y=""8""><p><font face=""Chosence-Bold"" size=""3"" color=""#f4f4f4"">[title]</font></p></position>","<position x=""4"" y=""14""><p> <font face=""Chosence-Bold"" size=""3"" color=""#fff"">[comment]</font></p></position>"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

