FLhd   0  0 FLdtw  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   Ճ*�  ﻿[General]
GlWindowMode=1
LayerCount=7
FPS=1
MidiPort=-1
Aspect=1
LayerOrder=3,0,1,5,4,6,2
WizardParams=131,132

[AppSet3]
App=Peak Effects\SplinePeaks
ParamValues=520,833,0,496,500,500,1000
ParamValuesTerrain\GoopFlow=0,0,0,844,500,500,500,0,500,500,500,500,500,500,500,500
ParamValuesText\MeshText=0,0,0,616,502,500,500,500,500,500,500,500,500,500,500,500,100,0,0,0
ParamValuesScenes\Postcard=0,1000,484,1000,500,500,0,0,0,0,0
ParamValuesPostprocess\ColorCyclePalette=500,0,812,0,800,1000,0,500,40,500,0
ParamValuesPostprocess\ScanLines=0,800,0,0,0,0
Enabled=1

[AppSet0]
App=Background\ItsFullOfStars
ParamValues=0,0,0,500,496,500,500,500,0,0,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPeak Effects\VectorScope=0,0,0,0,1000,165,667,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesPeak Effects\Polar=912,508,1000,0,800,500,500,280,1000,344,192,632,0,500,1000
ParamValuesParticles\fLuids=0,0,0,0,876,500,500,0,0,0,500,500,500,0,500,250,500,0,0,500,500,500,0,0,500,0,0,250,500,0,0,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPeak Effects\Linear=0,0,1000,1000,750,500,84,1000,0,500,0,308,0,1000,0,1000,0,0,1000,1000,0,0,1000,1000,1000
ParamValuesCanvas effects\Flaring=652,0,0,1000,1000,1000,0,64,0,200,500
ParamValuesImage effects\Image=0,0,0,0,0,500,500,0,0,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,557,500,500,500,500,500
ParamValuesBackground\FourCornerGradient=0,1000,1000,752,1000,0,1000,1000,524,984,1000,750,1000,1000
ParamValuesPostprocess\Blur=0
Enabled=1

[AppSet1]
App=Particles\ColorBlobs
ParamValues=0,0,28,0,200,500,500,1000,0,80
ParamValuesParticles\BugTails=0,0,640,0,852,500,500,1000,1000,1000,1000,1000
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,41,500,500,500,500,500
ParamValuesMisc\CoreDump=0,0,0,648,1000,500,500,0,0,236,0,0,0,0,44,584,1000
ParamValuesObject Arrays\CubesGrasping=0,492,1000,0,500,500,500,760,1000,0,0
Enabled=1

[AppSet5]
App=Postprocess\ColorCyclePalette
ParamValues=1,1,6,0,13,1000,0,0,152,0,0
ParamValuesImage effects\Image=0,0,0,0,1000,500,500,0,0,0,0
ParamValuesPeak Effects\Polar=0,476,904,712,636,500,500,500,1000,156,336,480,0,500,0
Enabled=1

[AppSet4]
App=Text\TextTrueType
ParamValues=460,0,0,1000,0,493,497,0,0,0,500
ParamValuesParticles\PlasmaFlys=500,836,500,692,500,204,560,192,80,100,0,0,0,0
ParamValuesImage effects\Image=1000,0,1000,1000,1000,0,500,0,0,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,799,500,500,500,500,500
ParamValuesPeak Effects\JoyDividers=1000,348,180,664,0,0,0,1000,436,100,696,650,510
ParamValuesParticles\ReactiveMob=0,0,424,52,500,64,0,692,0,1000,0,64,1000,284,472,0,0,1000,36,0,0,284,1000,0,0,100,0,0,0,0
Enabled=1

[AppSet6]
App=Text\TextTrueType
ParamValues=0,0,0,0,0,493,500,0,0,0,500
Enabled=1

[AppSet2]
App=Postprocess\Youlean Color Correction
ParamValues=500,500,500,500,604,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0

[UserContent]
Text="Soft tantalizing petals                             Of the perfect flower                               Giving peace and calmness                           Over life's ongoing agony                           Hope brings a mind's eye                            Visions of love's heart                             Talks of simple past                                Greater than perfection                             Living an unpredictable life                        Not knowing where you'll be                         Showing unpredictable feelings                      Hoping that you'll soon see                         The moon's penetrating rays                         Lighting the cool night air                         Filling the heart's emptiness                       With something that cares                           Love is a needful thing                             Holding fast and true                               Forever will it sing                                A tune greater than blue...                         Blue Flowers","Russell Sivey",Blue,Flowers,
Html=,"<position x=""4"" y=""5""><p align=""left""><font face=""American-Captain"" size=""7"" color=""#E5E5E5"">[author]</font></p></position>","<position x=""4"" y=""11""><p align=""left""><font face=""Chosence-Bold"" size=""5"" color=""#E5E5E5"">[title]</font></p></position>","<position x=""4"" y=""76""><p align=""right""><font face=""Chosence-Bold"" size=""2"" color=""#E5E5E5"">[extra1]</font></p></position>","<position x=""4"" y=""79""><p align=""right""><font face=""Chosence-Bold"" size=""3"" color=""#E5E5E5"">[comment]</font></p></position>",,,,"<position x=""4"" y=""86""><p align=""right""><p uppercase=""yes""><font face=""Antaris"" size=""4"" color=""#E5E5E5"">[extra6]</font></p></position>","<position x=""4"" y=""91""><p align=""right""><p uppercase=""yes""><font face=""Antaris"" size=""3"" color=""#E5E5E5"">[extra7]</font></p></position>",,"  "
VideoUseSync=0
EnableMipmap=1

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

