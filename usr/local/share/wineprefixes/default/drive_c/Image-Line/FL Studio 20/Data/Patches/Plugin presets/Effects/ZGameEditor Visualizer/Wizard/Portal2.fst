FLhd   0  0 FLdt�  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   Ջ�  ﻿[General]
GlWindowMode=1
LayerCount=12
FPS=1
MidiPort=-1
Aspect=1
LayerOrder=5,2,3,0,1,7,4,6,9,8,10,11
WizardParams=563

[AppSet5]
App=Image effects\Image
ParamValues=662,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet2]
App=Peak Effects\Linear
ParamValues=0,525,1000,0,326,335,500,370,0,500,604,500,500,1,0,0,0,500,500,1000,350,0,117,1,290,0,32,212,330,250,100
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet3]
App=Peak Effects\Linear
ParamValues=0,94,1000,0,326,665,500,370,500,500,604,500,500,1,0,0,0,500,500,1000,350,0,117,1,290,0,32,212,330,250,100
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet0]
App=Peak Effects\Polar
ParamValues=0,566,1000,888,655,500,500,1000,500,0,324,865,0,500,0,792,1000,500,1000,0,522,607,0,1
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet1]
App=Peak Effects\Polar
ParamValues=0,94,1000,888,655,500,500,1000,500,0,324,865,0,1000,0,792,1000,500,1000,0,522,607,0,1
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet7]
App=Postprocess\AudioShake
ParamValues=114,0,2,719,27,260
Enabled=1
Collapsed=1

[AppSet4]
App=Postprocess\ColorCyclePalette
ParamValues=1,1,13,1,14,540,214,291,930,0,0
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet6]
App=Postprocess\AudioShake
ParamValues=183,0,1,383,100,900
Enabled=1

[AppSet9]
App=Postprocess\Vignette
ParamValues=0,0,3,0,768,455
Enabled=1
Collapsed=1

[AppSet8]
App=Text\TextTrueType
ParamValues=504,0,0,1000,0,494,498,0,0,0,500
Enabled=1

[AppSet10]
App=Text\TextTrueType
ParamValues=0,0,0,0,0,494,500,0,0,0,500
Enabled=1

[AppSet11]
App=Postprocess\Youlean Color Correction
ParamValues=500,500,500,500,500,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0

[UserContent]
Html="<position x=""3""><position y=""5""><p align=""left""><font face=""American-Captain"" size=""10"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""3""><position y=""13""><p align=""left""><font face=""Chosence-Bold"" size=""6"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""55""><position y=""90""><p align=""right""><font face=""Chosence-Bold"" size=""2"" color=""#FFFFFF"">[extra1]</font></p></position>","<position x=""67""><position y=""94""><p align=""right""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""2""><position y=""72""><p align=""center""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""2""><position y=""76""><p align=""center""><font face=""Chosence-regular"" size=""2"" color=""#FFFFF"">[extra3]</font></p></position>"
Images=[presetpath]Wizard\Assets\tiledwall.jpg
VideoUseSync=0
EnableMipmap=1

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

