FLhd   0  ` FLdt  �20.7.3.1981 ��  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4               A                        o  �  �    �HQV Ս�  ﻿[General]
GlWindowMode=1
LayerCount=12
FPS=1
AspectRatio=16:9
LayerOrder=0,3,2,5,4,6,10,11,7,8,1,9

[AppSet0]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.532,0.08,0.104,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,532,80,104,0,0,0,0,0
ParamValuesTunnel\Voxel Corridor=0,596,1000,0,600
Enabled=1
UseBufferOutput=1
Name=Section0

[AppSet3]
App=Background\SolidColor
FParamValues=0,0,0,0
ParamValues=0,0,0,0
Enabled=1

[AppSet2]
App=Postprocess\Vignette
FParamValues=0,0,0,1,0.212,0.6818
ParamValues=0,0,0,1000,212,681
Enabled=1
UseBufferOutput=1

[AppSet5]
App=HUD\HUD Image
FParamValues=0,1,0.5,0.5,1,1,0.5,4,0.5,0,0,1,1,1,1
ParamValues=0,1,500,500,1000,1000,500,4,500,0,0,1000,1000,1,1
Enabled=1
UseBufferOutput=1
ImageIndex=3

[AppSet4]
App=Misc\Automator
FParamValues=1,3,6,2,0.744,0.076,0.54,0,1,8,2,0.124,0.07,0.502,0,1,9,2,0.132,0.07,0.502,0,0,0,0,0.492,0.07,0.466
ParamValues=1,3,6,2,744,76,540,0,1,8,2,124,70,502,0,1,9,2,132,70,502,0,0,0,0,492,70,466
Enabled=1

[AppSet6]
App=Canvas effects\Electric
FParamValues=0.964,0.74,0.484,0.78,1,0.388,0.192,0.01,0
ParamValues=964,740,484,780,1000,388,192,10,0
ParamValuesCanvas effects\Bioorganic Wall=0,600,500,500,0,1000,1000,832
ParamValuesCanvas effects\Cellular Tiling=800,0,0,0,488,4
ParamValuesCanvas effects\Bumped Sinusoidal Warp=824,0,0,300,548,1000,824
ParamValuesCanvas effects\Lava=0,0,0,720,0,500,500,500,500,516,0
ParamValuesCanvas effects\Alloy Plated Voronoi=0,482,0,0,1,500,0
Enabled=1

[AppSet10]
App=Canvas effects\Electric
FParamValues=0.96,0.74,0.484,0.78,1,0.36,0.16,0.011,0
ParamValues=960,740,484,780,1000,360,160,11,0
Enabled=1

[AppSet11]
App=Canvas effects\Electric
FParamValues=0.96,0.74,0.484,0.78,1,0.356,0.192,0.009,0
ParamValues=960,740,484,780,1000,356,192,9,0
Enabled=1

[AppSet7]
App=Postprocess\Vignette
FParamValues=0,0,0,0.6,0.248,0
ParamValues=0,0,0,600,248,0
Enabled=1
UseBufferOutput=1

[AppSet8]
App=HUD\HUD Image
FParamValues=0,1,0.5,0.5,1,1,0.496,4,0.5,0.304,0.112,1,1,1,0
ParamValues=0,1,500,500,1000,1000,496,4,500,304,112,1000,1000,1,0
Enabled=1
UseBufferOutput=1
ImageIndex=5

[AppSet1]
App=Blend\BufferBlender
FParamValues=9,0,0,1,1,2,0.14,0,0.5,0.5,0.778,0
ParamValues=9,0,0,1000,1,2,140,0,500,500,778,0
Enabled=1
UseBufferOutput=1
ImageIndex=1

[AppSet9]
App=Blend\BufferBlender
FParamValues=9,0,0,1,1,4,1,0,0.5,0.5,0.75,0
ParamValues=9,0,0,1000,1,4,1000,0,500,500,750,0
Enabled=1
ImageIndex=2

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=256000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="This is the default text."
Images=@Stream:https://images.pexels.com/photos/2559941/pexels-photo-2559941.jpeg
VideoUseSync=0
Filtering=0

[Detached]
Top=-864
Left=0
Width=1536
Height=864

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

[Wizard]
Section0Cb=Voxel Corridor
Section1Cb=None
Section2Cb=Waveform Circle 04
Section3Cb=Pixelart
Macro0=Song Title
Macro1=Author
Macro2=Comment

