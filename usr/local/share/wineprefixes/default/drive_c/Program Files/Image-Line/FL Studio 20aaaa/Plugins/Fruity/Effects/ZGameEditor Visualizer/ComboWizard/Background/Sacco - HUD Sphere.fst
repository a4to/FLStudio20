FLhd   0 * ` FLdt�$  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��HU$  ﻿[General]
GlWindowMode=1
LayerCount=27
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,6,1,16,17,18,9,2,29,3,4,7,11,10,22,26,23,27,28,24,5,8,15,12,14,25,30
WizardParams=129,1425,1475,1476,177,225,465

[AppSet0]
App=Background\SolidColor
FParamValues=0,0,0,0.868
ParamValues=0,0,0,868
ParamValuesF=0,0,0,0.868
Enabled=1

[AppSet6]
App=HUD\HUD Grid
AppVersion=1
FParamValues=0.128,0.5,0,0,0.5,0.5,1,1,1,4,0.5,1,1,0.748,0,0.5,1,0,0.412,0,0,0
ParamValues=128,500,0,0,500,500,1000,1000,1000,4,500,1000,1000,748,0,500,1000,0,412,0,0,0
ParamValuesF=0.128,0.5,0,0,0.5,0.5,1,1,1,4,0.5,1,1,0.748,0,0.5,1,0,0.412,0,0,1
Enabled=1

[AppSet1]
App=HUD\HUD Prefab
FParamValues=73,0,0.5,0,0,0.5,0.5,0.09,1,1,4,0,0.5,1,0.368,0,1,0
ParamValues=73,0,500,0,0,500,500,90,1000,1000,4,0,500,1,368,0,1000,0
ParamValuesCanvas effects\SkyOcean=472,328,500,0,0,500,500,0,500,260,0,0,0
ParamValuesCanvas effects\ImageTileSprite=5,5,0,0,0,0,4,250,250,500,500,0,492,0,0,0,0
ParamValuesF=66,0,0.5,0,0,0.5,0.5,0.09,1,1,4,0,0.5,1,0.368,0,1,1
ParamValuesCanvas effects\FreqRing=0,500,1000,500,500,500,500,1000,0,0,0,4,500,16
Enabled=1
LayerPrivateData=78014B2FCA4C89490712BA0606867A9939650C230B0000B42405E0

[AppSet16]
App=HUD\HUD Prefab
FParamValues=73,0,0.5,0,0,0.288,0.5,0.09,1,1,4,0,0.5,1,0.368,0,1,0
ParamValues=73,0,500,0,0,288,500,90,1000,1000,4,0,500,1,368,0,1000,0
ParamValuesF=66,0,0.5,0,0,0.288,0.5,0.09,1,1,4,0,0.5,1,0.368,0,1,1
Enabled=1
LayerPrivateData=78014B2FCA4C89490712BA0606867A9939650C230B0000B42405E0

[AppSet17]
App=HUD\HUD Prefab
FParamValues=73,0,0.5,0,0,0.288,0.865,0.09,1,1,4,0,0.5,1,0.368,0,1,0
ParamValues=73,0,500,0,0,288,865,90,1000,1000,4,0,500,1,368,0,1000,0
ParamValuesF=66,0,0.5,0,0,0.288,0.865,0.09,1,1,4,0,0.5,1,0.368,0,1,1
Enabled=1
LayerPrivateData=78014B2FCA4C89490712BA0606867A9939650C230B0000B42405E0

[AppSet18]
App=HUD\HUD Prefab
FParamValues=73,0,0.5,0,0,0.5,0.865,0.09,1,1,4,0,0.5,1,0.368,0,1,0
ParamValues=73,0,500,0,0,500,865,90,1000,1000,4,0,500,1,368,0,1000,0
ParamValuesF=66,0,0.5,0,0,0.5,0.865,0.09,1,1,4,0,0.5,1,0.368,0,1,1
Enabled=1
LayerPrivateData=78014B2FCA4C89490712BA0606867A9939650C230B0000B42405E0

[AppSet9]
App=Canvas effects\SkyOcean
FParamValues=0,0.84,0.5,0,0,0.5,0.5,0,0.5,0.26,0,0,0,1
ParamValues=0,840,500,0,0,500,500,0,500,260,0,0,0,1000
ParamValuesF=0,0.84,0.5,0,0,0.5,0.5,0,0.5,0.26,0,0,0,1
Enabled=1

[AppSet2]
App=Canvas effects\SkyOcean
FParamValues=0,0.5,0.5,0,0,0.5,0.552,0.013,0.5,0.26,0,0,0,0
ParamValues=0,500,500,0,0,500,552,13,500,260,0,0,0,0
ParamValuesF=0,0.5,0.5,0,0,0.5,0.552,0.013,0.5,0.26,0,0,0,0
Enabled=1

[AppSet29]
App=Canvas effects\SkyOcean
FParamValues=0,0.5,0.5,0,0,0.5,0.552,0.191,0.5,0.26,0,0,0,0
ParamValues=0,500,500,0,0,500,552,191,500,260,0,0,0,0
ParamValuesF=0,0.5,0.5,0,0,0.5,0.552,0.191,0.5,0.26,0,0,0,0
Enabled=1

[AppSet3]
App=Canvas effects\SkyOcean
FParamValues=0,0.84,0.5,0,0,0.5,0.584,0.031,0.5,0.26,0,0,0,1
ParamValues=0,840,500,0,0,500,584,31,500,260,0,0,0,1000
ParamValuesF=0,0.84,0.5,0,0,0.5,0.584,0.031,0.5,0.26,0,0,0,1
Enabled=1

[AppSet4]
App=Canvas effects\SkyOcean
FParamValues=0,0.5,0.5,0,0,0.5,0.708,0.046,0.5,0.26,0,0,0,0
ParamValues=0,500,500,0,0,500,708,46,500,260,0,0,0,0
ParamValuesF=0,0.5,0.5,0,0,0.5,0.708,0.046,0.5,0.26,0,0,0,0
Enabled=1

[AppSet7]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.712,0,0.15,0.03,1,0,0,0
ParamValues=712,0,150,30,1000,0,0,0
ParamValuesF=0.712,0,0.15,0.03,1,0,0,0
ParamValuesPostprocess\FrameBlur=0,0,0,0,862,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
UseBufferOutput=1

[AppSet11]
App=HUD\HUD Prefab
FParamValues=0,0,0.5,0,0,0.5,0.484,0.5731,1,1,4,0,0.5,1,0.368,0.043,1,1
ParamValues=0,0,500,0,0,500,484,573,1000,1000,4,0,500,1,368,43,1000,1
ParamValuesF=0,0,0.5,0,0,0.5,0.484,0.434,1,1,4,0,0.5,1,0.368,0.043,1,1
Enabled=0
LayerPrivateData=780173C8CBCF4BD52B2E4B671899000060F9036F

[AppSet10]
App=Postprocess\Blooming
FParamValues=0,0,0,1,0.5,1,0.396,0.616,0
ParamValues=0,0,0,1000,500,1000,396,616,0
ParamValuesF=0,0,0,1,0.5,1,0.396,0.616,0
Enabled=1
UseBufferOutput=1

[AppSet22]
App=HUD\HUD Prefab
FParamValues=70,0.747,0.5,0,0,0.199,0.102,0.455,1,1,4,0,0.5,1,0.546,0,1,1
ParamValues=70,747,500,0,0,199,102,455,1000,1000,4,0,500,1,546,0,1000,1
ParamValuesF=63,0.747,0.5,0,0,0.199,0.102,0.455,1,1,4,0,0.5,1,0.546,0,1,1
Enabled=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D8C2CF43273CA18460000002F290AB6

[AppSet26]
App=HUD\HUD Prefab
FParamValues=70,0.747,0.5,1,0,0.199,0.102,0.455,1,1,4,0,0.5,1,0.546,0.9862,0.965,1
ParamValues=70,747,500,1000,0,199,102,455,1000,1000,4,0,500,1,546,986,965,1
ParamValuesF=63,0.747,0.5,1,0,0.199,0.102,0.455,1,1,4,0,0.5,1,0.546,0.928,0.0523,1
Enabled=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D8C2CF43273CA18460000002F290AB6

[AppSet23]
App=HUD\HUD Prefab
FParamValues=60,0.786,0.5,0,0,0.812,0.897,0.455,1,1,4,0,0.5,1,0.546,0,1,1
ParamValues=60,786,500,0,0,812,897,455,1000,1000,4,0,500,1,546,0,1000,1
ParamValuesF=53,0.786,0.5,0,0,0.812,0.897,0.455,1,1,4,0,0.5,1,0.546,0,1,1
Enabled=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D0C2DF43273CA18460000002E3F0AB5

[AppSet27]
App=HUD\HUD Prefab
FParamValues=60,0.786,0.84,1,0,0.812,0.897,0.455,1,1,4,0,0.5,1,0.546,0.9147,0.1958,1
ParamValues=60,786,840,1000,0,812,897,455,1000,1000,4,0,500,1,546,914,195,1
ParamValuesF=53,0.786,0.84,1,0,0.812,0.897,0.455,1,1,4,0,0.5,1,0.546,0.0005,0.0016,1
Enabled=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D0C2DF43273CA18460000002E3F0AB5

[AppSet28]
App=Misc\Automator
FParamValues=1,27,16,2,0.5,0.786,0.25,1,27,17,2,0.5,0.362,0.25,1,28,16,2,0.5,0.426,0.25,1,28,17,2,0.5,0.678,0.25
ParamValues=1,27,16,2,500,786,250,1,27,17,2,500,362,250,1,28,16,2,500,426,250,1,28,17,2,500,678,250
ParamValuesF=1,27,16,2,0.5,0.786,0.25,1,27,17,2,0.5,0.362,0.25,1,28,16,2,0.5,0.426,0.25,1,28,17,2,0.5,0.678,0.25
Enabled=1

[AppSet24]
App=Postprocess\Blooming
FParamValues=0,0,0,1,0.5,0.8,0.5,0.133,0
ParamValues=0,0,0,1000,500,800,500,133,0
ParamValuesF=0,0,0,1,0.5,0.8,0.5,0.133,0
ParamValuesPostprocess\Youlean Motion Blur=852,0,0,0
Enabled=1
UseBufferOutput=1

[AppSet5]
App=Image effects\ImageSphereWarp
FParamValues=0,0.5731,0.451,1,0,0.4,0,0.5,0.5
ParamValues=0,573,451,1000,0,400,0,500,500
ParamValuesF=0,0.434,0.451,1,0,0.4,0,0.5,0.5
Enabled=1
ImageIndex=1

[AppSet8]
App=Postprocess\ParameterShake
FParamValues=0.42,0.292,5,1,0.42,0.292,14,7,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=420,292,5,1,420,292,14,7,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=0.42,0.292,5,1,0.42,0.292,14,7,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet15]
App=Postprocess\ParameterShake
FParamValues=0.42,0.292,19,7,0.42,0.292,11,7,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=420,292,19,7,420,292,11,7,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=0.42,0.292,19,7,0.42,0.292,11,7,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet12]
App=Image effects\Image
FParamValues=0.92,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=920,0,0,0,1000,500,500,0,0,0,0,0,0,0
ParamValuesF=0.92,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
Enabled=1
ImageIndex=2

[AppSet14]
App=HUD\HUD Prefab
FParamValues=0,0.156,0.5,0,1,0.5,0.493,0.5731,1,1,4,0,0.5,1,0.368,0.111,1,1
ParamValues=0,156,500,0,1000,500,493,573,1000,1000,4,0,500,1,368,111,1000,1
ParamValuesF=0,0.156,0.5,0,1,0.5,0.493,0.434,1,1,4,0,0.5,1,0.368,0.111,1,1
Enabled=0
LayerPrivateData=780173C8CBCF4BD52B2E4B671899000060F9036F

[AppSet25]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
ParamValuesF=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
Enabled=1
ImageIndex=3

[AppSet30]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.544,0.5,0.5,1,0.5
ParamValues=500,544,500,500,1000,500
ParamValuesF=0.5,0.544,0.5,0.5,1,0.5
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position y=""10""><p align=""center""><font face=""American-Captain"" size=""9"" color=""#FFFFFF"">[author]</font></p></position>","<position y=""20""><p align=""center""><font face=""Chosence-Bold"" size=""7"" color=""#FFFFFF"">[title]</font></p></position>","<position y=""53""><p align=""center""><font face=""Chosence-Bold"" size=""5"" color=""#5e5e5e"">[extra1]</font></p></position>","<position y=""92""><p align=""center""><font face=""Chosence-Bold"" size=""4"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position y=""71""><p align=""center""><font face=""Chosence-Bold"" size=""4"" color=""#5e5e5e"">[extra2]</font></p></position>","<position y=""76""><p align=""center""><font face=""Chosence-regular"" size=""4"" color=""#5e5e5e"">[extra3]</font></p></position>",
Images="[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-007.ilv"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

