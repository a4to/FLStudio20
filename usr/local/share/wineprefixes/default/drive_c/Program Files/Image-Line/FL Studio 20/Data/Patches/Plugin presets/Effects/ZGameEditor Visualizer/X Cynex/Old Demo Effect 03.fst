FLhd   0  ` FLdt�  �	12.2.0.3 �.Z G a m e E d i t o r   V i s u a l i z e r   �4               A                  �  �   �  ~  �    �HQV ��<B  [General]
GlWindowMode=1
LayerCount=11
FPS=2
DmxOutput=0
DmxDevice=0
MidiPort=0
Aspect=0
LayerOrder=0,7,1,2,3,4,5,6,8,10,9

[AppSet0]
App=Misc\Automator
ParamValues=1,5,9,2,499,19,197,1,4,6,2,0,80,42,1,3,2,2,723,52,1000,1,3,10,2,594,0,582
ParamValuesBackground\FogMachine=0,696,1000,551,882,401,273,334,440,1000,0,314
ParamValuesCanvas effects\DarkSpark=548,0,500,500,500,500,500,500,500,500,0,0
ParamValuesCanvas effects\Flaring=87,489,187,1000,1000,503,775,63,0,400,500
ParamValuesCanvas effects\Flow Noise=317,675,1000,704,0,1000,1000,97,100,1000,500
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesTerrain\CubesAndSpheres=0,0,0,0,106,445,0,352,265,799,1000,242,1000,1000,97,939,1000,127
ParamValuesTerrain\GoopFlow=150,0,0,347,741,927,702,0,26,500,0,500,1000,478,374,698
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet1]
App=Background\FogMachine
ParamValues=0,0,0,324,444,500,500,500,500,322,301,512
ParamValuesBackground\FourCornerGradient=0,1000,0,558,613,0,546,600,0,526,594,961,1000,603
ParamValuesFeedback\70sKaleido=0,0,0,1000,0,62
ParamValuesFeedback\BoxedIn=0,0,0,1000,500,0,0,500,0,0
ParamValuesFeedback\FeedMe=0,0,0,1000,1000,1000,358
ParamValuesFeedback\SphericalProjection=60,0,0,0,364,425,686,718,775,1000,0,500,500,731,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesObject Arrays\8x8x8_Eggs=0,448,381,0,154,500,500,357,574,644,0,221,221
ParamValuesParticles\fLuids=439,930,965,754,754,737,211,1000,754,1000,632,1000,632,877,386,175,158,0,1000,0,825,263,281,684,500,88,70,1000,1000,4,175,211
Input=0
Enabled=1
UseBufferOutput=1
BufferRenderQuality=0
ImageIndex=1
MeshIndex=0

[AppSet2]
App=Background\FourCornerGradient
ParamValues=12,1000,0,1000,325,263,1000,118,135,465,578,805,1000,198
ParamValuesBackground\FogMachine=0,314,728,160,1000,500,500,500,500,138,0,0
ParamValuesBlend\BufferBlender=0,111,867,862,250,111,1000,0,500,500,750
ParamValuesImage effects\ImageWarp=0,0,0,297,0,669,803,424
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,860,500,500,500,500,500
ParamValuesParticles\fLuids=0,0,0,0,305,500,500,0,0,0,500,500,500,0,500,250,500,0,0,500,500,500,0,0,500,0,0,250,500,0,0,0
ParamValuesTerrain\GoopFlow=0,0,0,0,596,500,500,0,500,497,241,628,500,1000,420,462
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet3]
App=Background\ItsFullOfStars
ParamValues=0,170,1000,528,675,0,292,526,124,0,0
ParamValuesBackground\FogMachine=470,0,0,0,0,500,500,500,500,500,0,0
ParamValuesBackground\FourCornerGradient=133,1000,542,1000,1000,504,237,502,901,127,1000,827,1000,686
ParamValuesBlend\BufferBlender=0,111,800,1000,250,111,430,0,500,500,750
ParamValuesPostprocess\Blooming=0,253,1000,377,1000,738,1000,623,208
Input=0
Enabled=1
UseBufferOutput=1
BufferRenderQuality=0
ImageIndex=0
MeshIndex=0

[AppSet4]
App=Blend\BufferBlender
ParamValues=4,1,12,1000,1,0,226,0,574,191,0
ParamValuesMisc\Automator=1000,111,182,400,448,23,586,1000,111,212,400,531,26,548,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0
ParamValuesPostprocess\Blooming=0,163,688,1000,465,960,199,372,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=2
MeshIndex=0

[AppSet5]
App=Postprocess\Blooming
ParamValues=0,170,141,1000,500,1000,1000,219,0
ParamValuesCanvas effects\N-gonFigure=0,782,1000,0,804,500,500,670,0,1000,557
ParamValuesMisc\Automator=1000,37,182,400,729,93,680,1000,37,212,400,534,87,369,1000,37,152,400,665,231,539,0,74,152,400,550,96,327,0,0,0,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet6]
App=Feedback\WarpBack
ParamValues=174,0,0,0,500,500,192
ParamValuesFeedback\70sKaleido=525,163,858,1000,1000,263
ParamValuesMisc\Automator=1000,222,242,400,518,128,436,1000,296,242,400,608,8,250,1000,333,242,400,288,74,487,1000,333,152,400,800,409,84,0,0,0,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=3
MeshIndex=0

[AppSet7]
App=Misc\Automator
ParamValues=1,1,26,2,499,705,197,0,4,6,3,349,112,532,0,3,2,2,723,52,470,1,5,10,2,352,54,323
ParamValuesCanvas effects\N-gonFigure=0,654,900,0,859,500,500,663,0,1000,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet8]
App=Feedback\SphericalProjection
ParamValues=0,0,0,0,391,1000,0,244,480,473,1,1,1,965,500,500
ParamValuesCanvas effects\N-gonFigure=0,721,1000,592,816,500,499,493,0,1000,0
ParamValuesFeedback\FeedMe=0,0,0,1000,1000,0,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet9]
App=Text\TextDraw
ParamValues=52,557,0,0,130,500,430,0,0,842,773,125,0,0,0
ParamValuesBackground\ItsFullOfStars=0,454,0,326,0,500,500,442,442,201,560
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet10]
App=Image effects\Image
ParamValues=486,675,1000,1000,467,500,772,1000,0
ParamValuesBackground\ItsFullOfStars=0,454,0,326,0,500,500,644,442,447,560
ParamValuesImage effects\ImageBox=67,0,0,0,0,500,500,500,500,500,0,0,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=3
MeshIndex=0

[AppSet11]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet12]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet13]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet14]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet15]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet16]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet17]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet18]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet19]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet20]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet21]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet22]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet23]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet24]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[Video export]
VideoH=480
VideoW=640
VideoRenderFps=30
SampleRate=0
VideoCodec=-1
AudioCodec=-1
AudioCodecFormat=-1
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=0
Uncompressed=0

[UserContent]
Text="Add a title"
Html=
VideoCues=
Meshes=
MeshAutoScale=0
MeshWithColors=0
Images=[plugpath]Content\Bitmaps\Particles\flare.png,[plugpath]Content\Bitmaps\Particles\furball.png
VideoUseSync=0

[Detached]
Top=394
Left=139
Width=689
Height=638

