WTI wavetable file description
==============================

1. Introduction
===============
WTI files describe wavetable information for the Piano/ePiano plugins.
This information is ini-file format.

Wave data can be raw (just the samples) or riff (a regular wavefile). It should be 
in mono format, although stereo riff files can be loaded (it's just a lot slower).



2. Sections
===========
- Info
- VelocityLayers
- Parameters
- Sample0 - SampleN (N = # samples - 1)



3. Info section
===============
This section contains general information about the wavetable.

- name 			: the name of the wavetable (not used yet)
- shortname		: a short name to display to the user (not used yet)
- samples		: the number of samples in the wavetable
- samplefile		: the name of a single wavetable file (optional)
- filetype		: the type of the wave files, raw wave data or a riff wave file
			  possible values are :	raw & riff
- path			: the (relative) path to add to the filenames
- author		: who created this wavetable
- samplerate		: the samplerate of the waves
- velocityfactor	: a factor by which to multiply the voice envelope at the start of a note (usually not necessary)
- releasehighnotes	: set to "true" to also release the high notes (95-127)
- isstandard		: if 1, this is a wavetable that's included with FL Keys. Don't use this for your own wavetables!
- canloop		: if true (or absent), FL Keys will loop the samples. Otherwise, voices stop when the sample's end is reached



4. VelocityLayers section
=========================
A wavetable can contain multiple velocity layers. This means that a single note
is represented by more than one sample, depending on the velocity of the note.

For example, if there are three velocity layers, a note with velocity 20 can use
one sample, a note with velocity 60 can use another sample and a note with velocity
120 is played by a third sample.

In the wavetable data, samples for different velocity layers of the same group of 
notes must follow eachother.

- count 	: the number of velocity layers
- 0 .. count-1	: the lowest velocity for this layer, the highest possible velocity is 127



5. Parameters section
=====================
In this section you can specify initial values for all parameters. These are set when
the wavetable is loaded. 

Parameter values must be in the range 0..1. So valid parameter values are 0.1, 0.453, ...

Each parameter is identified by a number:

 0 : Decay (set to zero for no decay/sustain)
 1 : Release
 2 : Hardness
 3 : Velocity > Hardness
 4 : Muffle
 5 : Muffle > Hardness
 6 : Velocity sensitivity
 7 : Stereo
 8 : Tune
 9 : Detune
10 : Stretch
11 : Treble
12 : Pan/Tremolo
13 : LFO Rate
14 : Overdrive



6. Sample sections
==================
There are as many sample sections as there are samples in the wavetable (see the Info section).
They are named Sample0, Sample1, ...
These sections describe some properties of each sample, especially the root note and the 
high note.
You have to specify all samples for all velocity layers. The samples for the velocity layers for 
a given root and high have to follow eachother.

- name		: if the samples are all in separate files, the filename of each sample is specified here.
          	  You can leave the name of the sample out if it's the same one as for the previous one.
- root 		: the lowest note that this sample will be used for
- high		: the high note, usually somewhere in the middle between two root notes
  		  For the last sample, this has to be more than 127 (999 might be a good idea)

- pos		: position of this sample when all samples are in one raw data file 
- end		: position of the end of this sample when all samples are in one raw data file 
- loop		: the length of the loop, used to determine the looppoint for raw wavefiles (not for riffs)
- canloop	: if true (or absent), FL Keys will loop this sample. Otherwise, voices stop when the sample's end is reached. If "canloop" is set to false in the Info section, this value has no effect.

