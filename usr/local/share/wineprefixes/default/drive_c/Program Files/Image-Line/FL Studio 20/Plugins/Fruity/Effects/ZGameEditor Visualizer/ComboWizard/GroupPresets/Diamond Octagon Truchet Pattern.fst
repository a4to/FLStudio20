FLhd   0  0 FLdt<  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   ��%�  ﻿[General]
GlWindowMode=1
LayerCount=8
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=2,6,5,4,3,0,1,7

[AppSet2]
App=Peak Effects\Linear
FParamValues=0,0.964,0,0,0.258,0.5,0.248,0.388,0,0.5,0.568,0.35,1,0,1,0,0,0.5,0.5,0.5,0.468,0,0.5,0.152,0.2,0,0.032,0.212,0.33,0.25,0.1
ParamValues=0,964,0,0,258,500,248,388,0,500,568,350,1000,0,1,0,0,500,500,500,468,0,500,152,200,0,32,212,330,250,100
ParamValuesPeak Effects\Youlean Peak Shapes=0,0,0,0,74,500,368,500,490,104,564,500,120,144,500,0,0,200,200,360,358,0,520,5,0,1000,0,500,500,0,0,0
ParamValuesPeak Effects\Stripe Peeks=0,0,0,0,0,0,74,500,220,0,250,500,700,0,500,150,300,200,200,300,1000,0,0
ParamValuesPeak Effects\Youlean Waveform=0,0,0,0,186,859,500,250,372,250,500,210,500,0,100,0,750,1000,16,88,0,500,100,100,0,0,282,500,500,1000,0,1000
ParamValuesPeak Effects\Polar=0,0,0,0,172,500,158,300,556,500,128,420,0,631,1000,500,1000,1000,968,1000,1000,0,0,1000
ParamValuesPeak Effects\SplinePeaks=940,0,0,0,900,500,360,0,0,0
ParamValuesMisc\FruityIndustry=0,0,0,0,500,500,0,500,500,0,500,500,500
ParamValuesImage effects\Image=0,0,0,0,1000,500,500,0,0,0,1000,0,0,0
ParamValuesHUD\HUD Graph Polar=0,500,0,0,500,856,106,444,784,0,1000,0,1000,0,0,500,0,348,0,0,500,0
Enabled=1
ImageIndex=2
Name=Section2

[AppSet6]
App=HUD\HUD 3D
FParamValues=0,0,0.4985,0.4628,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.3633,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,0
ParamValues=0,0,498,462,500,500,500,500,500,500,500,500,500,363,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,0
Enabled=1
ImageIndex=2
Name=ForegroundMod

[AppSet5]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
Name=TextFst

[AppSet4]
App=Text\TextTrueType
FParamValues=0,0.8333,0,1,0,0.5,0.5,0,0,0,0.5
ParamValues=0,833,0,1000,0,500,500,0,0,0,500
Enabled=0
Name=Text

[AppSet3]
App=Postprocess\Youlean Drop Shadow
FParamValues=0.5,0.2,0,1,0.02,0.875,0
ParamValues=500,200,0,1000,20,875,0
Enabled=1
UseBufferOutput=1

[AppSet0]
App=Youlean new shaders\Patterns\Diamond Octagon Truchet Pattern
FParamValues=0,0,0.2,0,0.548,3,0.5
ParamValues=0,0,200,0,548,3,500
ParamValuesScenes\Alps=0,71,362,0,250,348,304,0,0
Enabled=1
Name=Section0

[AppSet1]
App=Postprocess\Youlean Color Correction
FParamValues=0.4064,0.5,0.2603,0.5,0.5,0.5
ParamValues=406,500,260,500,500,500
ParamValuesPostprocess\Vignette=0,0,0,600,1000,0
ParamValuesPostprocess\Ascii=0,232,0,600,700,200
ParamValuesYoulean new shaders\Postprocess\Extruded Video Image=208,297,0
Enabled=1
Name=Section1

[AppSet7]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
ImageIndex=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="This is the default text."
Images="@EmbeddedFst:Name=Playground.fst,Data=7801C594CD6EDB300CC7EF01F20E45CE5927C9F2D7501570E4A401966E5DB3B687AC07CDE61A01FE822277CDB3EDB047DA2B8CB2136F3D6C40B1C310382129F22FFA17893FBE7DDF5C40054615F7E3D14571A7ABBCFE7A59E720E878B4527B30B26E2B2BBCF16871B576C14B9DEBABDA58F10A9D64D74066AF95D5B5A0C19BF850F3DEE4600499D2291B8FC6A34DD2346BB004B7404B2C6FD24FF89C7C8427EB649551E5AD2A5AD8610939F5A7F88DBF3C8ED13CE534FC15C405EA399744CCD9530F9FAEA42B43BBFB6067CF557DD22FA0E6D4091E7D174541943BAA11CC74AB7D0675EEA039AFD4E702F201CD95D18FCA42AAAC12614468E8F1844996C8844B9ECA3993324E4392F284A76C21531E3399B264C613390B421A794E9EB03026248AB0E781147D1929ECB583E21307CC3D1E7578DC8707CE8DB9A3FA625C1D08BF87F14C12058F7AFF026CE1C9398FA4E4B394FE19938F7C10D35C262408F0200E98D8CB301D0E54D0C3422C11777FFB4089F9743806078E3DB53EE7AF87CA893A5ACF2451F0BF52BAD539D427F0D4E07545549DBB149444E4E0DC091AB3A3730D15DEDA45B3131E86D6AA6C0AC09B0DE29820712C64EF540902EF719BEBFAF74027FEA15585B67B5771A99EDEC27E81571BD68DCA74F580B2041716BA80AA1799696BDC0634E434621C173BD921CCA2AEE2A6CAEAB231B0DBE1DDC3A475DB80D975FD39178FC3CDCECDA9CA42E5DED3CD15B1B1DA16703FDDA8D66E6B83068A947DC2D29685989C3527D8ED432526930CE3602693F38DC5DA4257707FF6BA399F1C30A1FC7A5F656E33EC1E33DDDBF43BA76055B685DC6D5B3702EF058E40F86205F57018DEE9DC6E4510A1B904FDB0B5C20FC3AE63D7ADA98B020C565E43BE7A2C04F3039CC106A01ABC194EC5C159B5E5608F473F0178F28290"
VideoUseSync=0
Filtering=0

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

[Wizard]
Section0Cb=Diamond Octagon Truchet Pattern
Section1Cb=Brightness-Gamma-Contrast
Section2Cb=Linear Horiz Down polarity
Section3Cb=Playground
Macro0=Song Title
Macro1=Song Author
Macro2=Song made with love and time

