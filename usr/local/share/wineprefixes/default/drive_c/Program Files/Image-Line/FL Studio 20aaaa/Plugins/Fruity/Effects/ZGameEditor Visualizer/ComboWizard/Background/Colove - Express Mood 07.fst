FLhd   0 * ` FLdt`%  �20.6.9.1657 �y  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��I�$  ﻿[General]
GlWindowMode=1
LayerCount=16
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,1,11,10,9,4,2,7,8,12,13,14,15,16,17,3

[AppSet0]
App=Background\SolidColor
FParamValues=0,0,0,0.92
ParamValues=0,0,0,920
Enabled=1
Collapsed=1
Name=Background

[AppSet1]
App=Peak Effects\Reactive Sphere
FParamValues=0.9,0.596,1,0.104,1,0.5,0.5,0.364,0.48,1
ParamValues=900,596,1000,104,1000,500,500,364,480,1
ParamValuesPeak Effects\Linear=0,580,0,0,754,500,500,268,0,500,272,844,504,1000,336,144,0,500,500,1000,350,0,572,1000,220
ParamValuesImage effects\Image=0,0,0,0,0,500,500,0,0,0,0
ParamValuesPeak Effects\Polar=0,1000,0,0,788,498,506,416,516,1000,552,448,0,628,0
Enabled=1
Collapsed=1
Name=EQ or FX

[AppSet11]
App=Postprocess\Ascii
FParamValues=0,0.336,0.42
ParamValues=0,336,420
ParamValuesFeedback\WarpBack=500,0,0,0,500,500,0
ParamValuesParticles\ColorBlobs=0,0,0,0,700,500,500,0,1000,148
ParamValuesPostprocess\RGB Shift=348,236,0,600,700,200
ParamValuesPeak Effects\PeekMe=0,0,1000,0,756,500,500,230,1000,0,0,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesPostprocess\Blooming=0,0,0,1000,852,800,864,856,392
ParamValuesScenes\OnOffSpikes=0,1000,900,440,0,500,500,1000,600,600,800,356,1000,0,206,500,0,800,1000,1000,1000,1000,658
ParamValuesObject Arrays\8x8x8_Eggs=0,0,0,0,0,500,500,200,500,500,380,228,508
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPostprocess\Point Cloud Default=0,490,330,500,500,448,444,503,554,625,156,0,0,0,0
ParamValuesPostprocess\AudioShake=356,0,0,600,700,200
ParamValuesPeak Effects\JoyDividers=0,0,1000,228,852,500,500,0,500,100,600,650,510
ParamValuesFeedback\SphericalProjection=60,0,0,0,18,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesObject Arrays\DiamondBit=500,500,500,816,830,500,500,0,500,500,456,500,144
ParamValuesImage effects\Image=0,0,0,0,0,500,500,0,0,0,0
ParamValuesObject Arrays\BallZ=0,736,0,0,962,500,500,182,20,604,440,796,0
ParamValuesScenes\Musicball=0,0,0,1000,528,500,500,0,0,500,0,500,500
ParamValuesParticles\ReactiveMob=0,0,1000,0,960,500,500,1000,500,500,500,612,764,500,500,416,200,50,125,0,500,500,1000,1000,52,100,500,244,564,0
ParamValuesPeak Effects\StereoWaveForm=0,0,0,1000,0,0,1000,1000,1000,1000,1000,1000,1000,1000,500,500,500,500,160,500,500,1000,0,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPostprocess\ScanLines=800,800,0,0,0,0
ParamValuesParticles\fLuids=0,740,1000,668,756,500,500,328,0,0,500,500,500,276,500,446,912,0,0,500,500,500,0,0,504,336,0,670,696,168,168,56
ParamValuesPeak Effects\Reactive Sphere=464,712,0,1000,632,500,500,604,316,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesCanvas effects\Rain=616,500,1000,624,1000,364,500,836,72,500,0,848
ParamValuesObject Arrays\WaclawGasket=0,0,0,1000,508,500,500,500,500,75,100,500,500,500,500
ParamValuesScenes\Postcard=540,740,484,1000,532,500,0,0,0,0,0
ParamValuesCanvas effects\N-gonFigure=0,654,900,200,500,500,500,500,500,400,0
ParamValuesObject Arrays\Rings=0,0,0,0,848,500,500,500,396,0,272,100,644,0,0
ParamValuesPhysics\Columns=200,300,0,824,500,600,1000,1000,0,450,500,500,500,500
ParamValuesCanvas effects\Lava=480,0,1000,764,1000,532,512,440,512,460,500
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,604,500,436,500,500,740,500,724
ParamValuesScenes\Cloud Ten=684,424,168,128,256,0,300,100,100,1000,500
ParamValuesObject Arrays\CubicMatrix=0,0,0,0,808,500,500,500,500,0,0,0,0
ParamValuesCanvas effects\Flaring=0,1000,1000,1000,1000,500,500,329,0,0,500
ParamValuesCanvas effects\Stack Trace=444,572,1000,1000,0,1000,1000
Enabled=1
Collapsed=1
Name=IMPULSE

[AppSet10]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.84,0.5,0.5,0.5,0,0,0
ParamValues=0,0,0,0,840,500,500,500,0,0,0
ParamValuesFeedback\BoxedIn=0,0,1000,0,868,0,0,1000,596,324
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,500,408,500,500,500,0,284,500
ParamValuesBackground\Grid=666,0,0,980,40,68,1000,0,0,0
ParamValuesImage effects\Image=0,0,0,1000,1000,500,500,596,764,124,0
ParamValuesFeedback\FeedMe=0,756,1000,640,120,884,296
Enabled=1
Collapsed=1
ImageIndex=2
Name=GFX 1

[AppSet9]
App=Feedback\WormHoleEclipse
FParamValues=0.024,1,0,0,0,0.028,0.892,0.42,0.556,0.5,0.5
ParamValues=24,1000,0,0,0,28,892,420,556,500,500
ParamValuesCanvas effects\ShimeringCage=672,76,0,0,228,500,500,0,0,0,268,0,0,316
Enabled=1
Collapsed=1
ImageIndex=2
Name=GFX 2

[AppSet4]
App=Background\FourCornerGradient
FParamValues=13,1,0,1,0,0.25,1,0,0.5,1,0.956,0.75,1,0.968
ParamValues=13,1000,0,1000,0,250,1000,0,500,1000,956,750,1000,968
ParamValuesBackground\Grid=526,0,0,980,40,68,1000,0,0,0
ParamValuesImage effects\Image=0,0,0,1000,1000,500,500,0,0,0,0
Enabled=1
Collapsed=1
Name=FILTER for GFX

[AppSet2]
App=Canvas effects\ShimeringCage
FParamValues=0,0,1,0,0.272,0.5,0.5,0.264,0.212,0.108,0.268,0,0,0.316
ParamValues=0,0,1000,0,272,500,500,264,212,108,268,0,0,316
ParamValuesImage effects\Image=0,0,0,1000,1000,500,500,596,764,124,0
Enabled=1
Collapsed=1
ImageIndex=2
Name=PLFX 1

[AppSet7]
App=Canvas effects\ShimeringCage
FParamValues=0,0.076,1,0,0.272,0.5,0.5,0,0,0,0.268,0,0,0.316
ParamValues=0,76,1000,0,272,500,500,0,0,0,268,0,0,316
Enabled=1
Collapsed=1
ImageIndex=2
Name=PLFX 2

[AppSet8]
App=Canvas effects\ShimeringCage
FParamValues=0.672,0.076,0,0,0.272,0.5,0.5,0,0,0,0.268,0,0,0.316
ParamValues=672,76,0,0,272,500,500,0,0,0,268,0,0,316
Enabled=1
UseBufferOutput=1
BufferRenderQuality=6
Collapsed=1
ImageIndex=2
Name=PLFX 3

[AppSet12]
App=Background\Youlean Retro Road
FParamValues=0.912,0.084,1,0,0,0,0,0.156,0.278,0,0,0.652,1,0.284,1,0.956,0.872,0.324,0.564,0.152,0.118
ParamValues=912,84,1000,0,0,0,0,156,278,0,0,652,1000,284,1000,956,872,324,564,152,118
Enabled=1
UseBufferOutput=1
Name=ROAD

[AppSet13]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,1
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,1000
Enabled=1
Collapsed=1
Name=BG OUT

[AppSet14]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.58,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,580,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=SYNTH ROAD 1

[AppSet15]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.388,0,0,0.5,0,0,0,0
ParamValues=0,0,0,0,1000,500,388,0,0,500,0,0,0,0
Enabled=1
ImageIndex=1
Name=SYNTH ROAD 2

[AppSet16]
App=Postprocess\Youlean Kaleidoscope
FParamValues=0.5,0.5,0,0.6,0.756,0,0.5,0.5,0.5,0,0
ParamValues=500,500,0,600,756,0,500,500,500,0,0
ParamValuesImage effects\Image=0,0,0,0,1000,500,388,0,0,500,0,0,0,0
ParamValuesHUD\HUD 3D=0,0,500,500,500,500,500,500,500,500,500,500,500,500,0,500,500,500,500,500,500,500,500,1000,1000,0,0,1000,1000,0
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,0,236,500,500,500,500,500,500
ParamValuesFeedback\FeedMeFract=0,0,0,1000,524,0
Enabled=1
Collapsed=1
Name=Effector OUT

[AppSet17]
App=Background\FourCornerGradient
FParamValues=7,1,0.052,1,1,0.628,0.768,1,0.2,1,1,0.964,1,1
ParamValues=7,1000,52,1000,1000,628,768,1000,200,1000,1000,964,1000,1000
ParamValuesFeedback\70sKaleido=0,436,972,52,628,656
ParamValuesFeedback\WormHoleDarkn=380,0,0,1000,500,500,500,500,500,500,500,500
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,0,0,0,0,500,500,500
ParamValuesFeedback\SphericalProjection=0,0,0,0,976,377,592,430,312,404,0,333,530,0,916,492,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesCanvas effects\SkyOcean=0,0,1000,0,732,500,500,648,928,500,240,1000,668
ParamValuesCanvas effects\Rain=856,148,1000,888,268,500,500,500,500,476,772,1000
ParamValuesImage effects\Image=0,0,0,0,0,500,500,0,0,0,0
ParamValuesCanvas effects\ShimeringCage=652,916,0,1000,88,500,500,208,0,0,592,0,0,292
ParamValuesFeedback\FeedMe=0,0,600,656,788,640,1000
Enabled=1
Collapsed=1
Name=FILTER COLOR 1

[AppSet3]
App=Background\FourCornerGradient
FParamValues=10,0.076,0.64,1,0.672,0.634,0.744,1,0.62,1,1,0.556,1,1
ParamValues=10,76,640,1000,672,634,744,1000,620,1000,1000,556,1000,1000
ParamValuesFeedback\WormHoleDarkn=380,0,0,1000,500,500,500,500,500,500,500,500
ParamValuesFeedback\70sKaleido=0,436,972,52,628,656
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,0,0,0,0,500,500,500
ParamValuesFeedback\SphericalProjection=0,0,0,0,976,377,592,430,312,404,0,333,530,0,916,492,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesCanvas effects\SkyOcean=0,0,1000,0,732,500,500,648,928,500,240,1000,668
ParamValuesCanvas effects\Rain=856,148,1000,888,268,500,500,500,500,476,772,1000
ParamValuesImage effects\Image=0,0,0,0,0,500,500,0,0,0,0
ParamValuesCanvas effects\ShimeringCage=652,916,0,1000,88,500,500,208,0,0,592,0,0,292
ParamValuesFeedback\FeedMe=0,0,600,656,788,640,1000
Enabled=1
Collapsed=1
Name=FILTER COLOR 2

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="You can","change this text","in settings."
Html="This is the default text."
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

[Wizard]
Section0Cb=Colove - Drone 01
Section1Cb=None
Section2Cb=HUD Meter 02
Section3Cb=Arcade cabinet

