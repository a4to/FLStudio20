'Add your own mapping formula presets here. The format is [name]:[formula]. Use ' for comments.

Sine curve:Sin(Input*Pi*0.5)'Half a sine curve
Double curve:1-Cos(Input*Pi)*0.5-0.5

Switch 1 of 4:1-Min(Round(Input*2),1)
Switch 2 of 4:ifg(input,0.25)+ifl(input,0.5)-1
Switch 3 of 4:ifg(input,0.5)+ifl(input,0.75)-1
Switch 4 of 4:Max(Round(Input*2),1)-1

Inverted first half:1-Input*0.5-0.5
Inverted last half:1-Input*0.5

First half input:Input*2
Last half input:Input*2-1
Up and down:1-(Max(Input,0.5)- Min(Input,0.5))*2
Up and down smooth:Sin(Input*Pi)
