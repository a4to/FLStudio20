FLhd   0 * ` FLdt�  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��0O  ﻿[General]
GlWindowMode=1
LayerCount=27
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=19,28,18,6,5,14,26,27,2,20,11,12,15,23,24,10,25,13,21,0,9,8,16,17,7,22,29

[AppSet19]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.2708,0.5,0.5,0.5,0,0,1
ParamValues=0,0,0,0,270,500,500,500,0,0,1000
ParamValuesCanvas effects\Rain=500,500,500,500,0,500,500,500,500,500,500,500
ParamValuesCanvas effects\FreqRing=500,500,500,500,1000,500,500,500,629,312,123,500,500,500
ParamValuesCanvas effects\Lava=0,0,500,720,1000,500,500,500,500,0,0
ParamValuesCanvas effects\SkyOcean=0,0,500,0,920,500,500,460,500,500,0,0,0
Enabled=1
ImageIndex=7

[AppSet28]
App=Misc\Automator
FParamValues=1,20,5,6,0.333,0.25,0.471,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,20,5,6,333,250,471,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1

[AppSet18]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1

[AppSet6]
App=Background\SolidColor
FParamValues=0,0,0,0
ParamValues=0,0,0,0
Enabled=1

[AppSet5]
App=Postprocess\Vignette
FParamValues=0,0,0,0.6,0.135,0.55
ParamValues=0,0,0,600,135,550
Enabled=1
UseBufferOutput=1

[AppSet14]
App=HUD\HUD Prefab
FParamValues=28,0,0.5,0,0,0.5,0.5,0.254,1,1,4,0,0.5593,1,0.286,0,1,0
ParamValues=28,0,500,0,0,500,500,254,1000,1000,4,0,559,1,286,0,1000,0
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0DF53273CA18863D000009B90A9D

[AppSet26]
App=Peak Effects\Polar
FParamValues=0,0,0,0,0.241,0.5,0.5,0.5,0,0.5,0.177,0.401,0,0.5,0,0.5,0,1,1,0,0,0,0,0
ParamValues=0,0,0,0,241,500,500,500,0,500,177,401,0,500,0,500,0,1000,1000,0,0,0,0,0
Enabled=1
ImageIndex=7

[AppSet27]
App=HUD\HUD Prefab
FParamValues=28,0,0.5,0,0,0.5,0.5,0.143,1,1,4,0,0.5,1,0.427,0,1,0
ParamValues=28,0,500,0,0,500,500,143,1000,1000,4,0,500,1,427,0,1000,0
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0DF53273CA18863D000009B90A9D

[AppSet2]
App=HUD\HUD Prefab
FParamValues=251,0,0.483,1,0,0.5,0.5,0.246,1,1,4,0,0.5,1,0.1547,0,1,0
ParamValues=251,0,483,1000,0,500,500,246,1000,1000,4,0,500,1,154,0,1000,0
Enabled=1
LayerPrivateData=78012BCE4DCCC95148CD49CD4DCD2B298E49CE2C4ACE498552BA0606667A9939650CC31800008F1A0D6F

[AppSet20]
App=HUD\HUD Prefab
FParamValues=253,0,0.518,1,0,0.5,0.5,0.1349,1,1,4,0,0.5,1,0.5213,0,1,0
ParamValues=253,0,518,1000,0,500,500,134,1000,1000,4,0,500,1,521,0,1000,0
Enabled=1
LayerPrivateData=78012BCE4DCCC95148CD49CD4DCD2B298E49CE2C4ACE498552BA0606167A9939650CC318000090DC0D71

[AppSet11]
App=HUD\HUD 3D
FParamValues=0,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.146,0.5,0.5,0.5,0.5,0.5,0.5107,0.5,0.5,1,1,0,0,1,1,0
ParamValues=0,0,500,500,500,500,500,500,500,500,500,500,500,500,146,500,500,500,500,500,510,500,500,1,1,0,0,1000,1000,0
Enabled=1
ImageIndex=7

[AppSet12]
App=Misc\Automator
FParamValues=1,12,21,6,0.5,0.25,0.505,1,3,8,6,0.231,0.25,0.507,1,15,13,6,0.45,0.25,0.551,0,0,0,0,0,0.25,0.25
ParamValues=1,12,21,6,500,250,505,1,3,8,6,231,250,507,1,15,13,6,450,250,551,0,0,0,0,0,250,250
Enabled=1

[AppSet15]
App=Misc\Automator
FParamValues=1,3,15,6,0.129,0.25,0.512,1,28,15,6,0.427,0,0.518,1,21,15,6,0.15,0.668,0.531,1,21,8,6,0.079,0.54,0.505
ParamValues=1,3,15,6,129,250,512,1,28,15,6,427,0,518,1,21,15,6,150,668,531,1,21,8,6,79,540,505
Enabled=1

[AppSet23]
App=Postprocess\Vignette
FParamValues=0,0,0,0.6,0.4,0.12
ParamValues=0,0,0,600,400,120
Enabled=1

[AppSet24]
App=Background\FourCornerGradient
FParamValues=3,0.751,0,1,1,0.25,1,1,0.5,1,1,0.75,1,1
ParamValues=3,751,0,1000,1000,250,1000,1000,500,1000,1000,750,1000,1000
Enabled=1

[AppSet10]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1

[AppSet25]
App=Blend\BufferBlender
FParamValues=0,0,0,1,3,2,0.245,0,0.5,0.5,0.75,0
ParamValues=0,0,0,1000,3,2,245,0,500,500,750,0
Enabled=1
ImageIndex=3

[AppSet13]
App=Postprocess\Youlean Bloom
AppVersion=2
FParamValues=0.1987,0,0.15,0.4545,1,0,0,1
ParamValues=198,0,150,454,1000,0,0,1
Enabled=1

[AppSet21]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.803,0.5
ParamValues=500,500,500,500,803,500
Enabled=1
UseBufferOutput=1

[AppSet0]
App=Image effects\Image
FParamValues=0,0,0,0,0.951,0.544,0.489,0.167,0.167,0,0,0,0,0
ParamValues=0,0,0,0,951,544,489,167,167,0,0,0,0,0
Enabled=1

[AppSet9]
App=Image effects\Image
FParamValues=0.409,0,0,0,0.946,0.5,0.5,0,0,0,0,0,0,0
ParamValues=409,0,0,0,946,500,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=6

[AppSet8]
App=Image effects\Image
FParamValues=0,0,0,0,0.951,0.544,0.489,0.167,0.167,0,0,0,0,0
ParamValues=0,0,0,0,951,544,489,167,167,0,0,0,0,0
Enabled=1
ImageIndex=1

[AppSet16]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1

[AppSet17]
App=Blend\BufferBlender
FParamValues=0,0,0,1,1,1,0.56,0,0.5,0.5,0.75,0
ParamValues=0,0,0,1000,1,1,560,0,500,500,750,0
Enabled=1
ImageIndex=4

[AppSet7]
App=Postprocess\Youlean Blur
FParamValues=0.162,0,0,1,0,1
ParamValues=162,0,0,1,0,1
Enabled=1
ImageIndex=2

[AppSet22]
App=Postprocess\Youlean Handheld
FParamValues=0.2,0.5,0.85,1,0.053,2
ParamValues=200,500,850,1000,53,2
Enabled=1

[AppSet29]
App=Postprocess\AudioShake
FParamValues=0.014,0,0,0.5,0.1,0.9
ParamValues=14,0,0,500,100,900
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<p align=""center"">[textline]</p>"
Images="[plugpath]ComboWizard\Assets Revisualizer\original eye of magic.jpg","[plugpath]ComboWizard\Assets Revisualizer\overlay eye of magic.png"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

