FLhd   0  0 FLdt�  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   ծ,*  ﻿[General]
GlWindowMode=1
LayerCount=8
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=2,6,5,4,3,0,1,7

[AppSet2]
App=Peak Effects\Youlean Peak Shapes
FParamValues=0,0,0,0,0.106,0.5,0.357,0.5,0.49,0.104,0.564,0.5,0.12,0.144,0.5,0,0,0.2,0.2,0.36,0.358,0,0.52,3,0,1,0,0.5,0.648,0,0,0
ParamValues=0,0,0,0,106,500,357,500,490,104,564,500,120,144,500,0,0,200,200,360,358,0,520,3,0,1,0,500,648,0,0,0
ParamValuesHUD\HUD Graph Polar=0,500,0,0,500,856,106,444,784,0,1000,0,1000,0,0,500,0,348,0,0,500,0
ParamValuesImage effects\Image=0,0,0,0,1000,500,500,0,0,0,1000,0,0,0
ParamValuesMisc\FruityIndustry=0,0,0,0,500,500,0,500,500,0,500,500,500
ParamValuesPeak Effects\Linear=0,964,0,0,178,752,508,112,250,500,196,350,1000,1000,500,0,0,500,500,500,468,0,500,152,200,0,32,212,330,250,100
ParamValuesPeak Effects\Polar=0,0,0,0,172,500,158,300,556,500,128,420,0,631,1000,500,1000,1000,968,1000,1000,0,0,1000
ParamValuesPeak Effects\SplinePeaks=940,0,0,0,900,500,360,0,0,0
ParamValuesPeak Effects\Stripe Peeks=0,0,0,0,0,0,74,500,220,0,250,500,700,0,500,150,300,200,200,300,1000,0,0
ParamValuesPeak Effects\Youlean Waveform=0,0,0,0,186,859,500,250,372,250,500,210,500,0,100,0,750,1000,16,88,0,500,100,100,0,0,282,500,500,1000,0,1000
Enabled=1
ImageIndex=2
Name=Section2

[AppSet6]
App=HUD\HUD 3D
FParamValues=0,0,0.4985,0.4757,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,0
ParamValues=0,0,498,475,500,500,500,500,500,500,500,500,500,500,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,0
Enabled=1
ImageIndex=2
Name=ForegroundMod

[AppSet5]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
Name=TextFst

[AppSet4]
App=Text\TextTrueType
FParamValues=0,0.8333,0,1,0,0.5,0.5,0,0,0,0.5
ParamValues=0,833,0,1000,0,500,500,0,0,0,500
Enabled=0
Name=Text

[AppSet3]
App=Postprocess\Youlean Drop Shadow
FParamValues=0.5,0.2,0,1,0.02,0.875,0
ParamValues=500,200,0,1000,20,875,0
Enabled=1
UseBufferOutput=1

[AppSet0]
App=Youlean new shaders\Patterns\Japanese Wave Pattern
FParamValues=0,0,1,0,0.6
ParamValues=0,0,1000,0,600
ParamValuesCanvas effects\Electric=0,333,611,842,120,212,500,73,0,1000,500
ParamValuesImage effects\Image=0,0,0,0,1000,500,500,0,0,0,1000,0,0,0
ParamValuesObject Arrays\Filaments=0,0,0,0,500,500,716,500,292,296,212,0,500,0
ParamValuesPeak Effects\Fluidity=0,488,1000,0,472,508,512
ParamValuesScenes\Alien Thorns=0,500,542,724,4,500,500,0
ParamValuesScenes\Alps=0,71,362,0,250,348,304,0,0
ParamValuesScenes\Cloud Ten=0,0,0,0,272,1000,531,0,0,0,0
ParamValuesScenes\Frozen Wasteland=0,0,0,0,364,1,0,0,0,0,0
Enabled=1
Name=Section0

[AppSet1]
App=Postprocess\Youlean Color Correction
FParamValues=0.3303,0.5,0.5866,0.5,0.5,0.5
ParamValues=330,500,586,500,500,500
ParamValuesPostprocess\Ascii=0,232,0,600,700,200
ParamValuesPostprocess\Dot Matrix=1000,500,500,0,368,500,1000,1000
ParamValuesPostprocess\FrameBlur=1000,0,0,0,1000,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesPostprocess\RGB Shift=246,300,0,600,700,200
ParamValuesPostprocess\Vignette=0,0,0,576,1000,316
ParamValuesPostprocess\Youlean Motion Blur=14,1000,0,500,0,0,1000
ParamValuesYoulean new shaders\Postprocess\Extruded Video Image=208,297,0
Enabled=1
Name=Section1

[AppSet7]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
ImageIndex=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Images="@EmbeddedFst:""Name=Candy caramel With border.fst"",Data=7801C555CD6EDB300CBE1BF03B14397B9B24CB7F4355C0B1930658BA75CDDA1EB21E349B6B0438B6A1285DF36C3BEC91F60AA3EC244D0F05966DC060182229F223F581A27E7EFF313F871AB4ACEE5CE7BCBA5575D97CBB684A10D475A672033A6BD6B511DC75C697336BBC50A5BA6CB411AF5049572D14E64A1AD5081ABE4DB6311F74095A10CFF7A8C75CC775E669DBCEC0104C8292985CE79FF13FF9048FC6024B2D9737B25AC30A83C8EBC02318882BA176E184265B2BEEDABDC85A63DEFBD9245DCC2EAEF7719DE7B001E942092E88EB2128AE3DDE161311BBC408D87B59CF7D50EF8A671ED5F24B05E59EA04BAD1EA4815C1A29A298D0C8676996F22C1BB338A024E62336CA5296592D221C253ECA8374C8D36C184634F66D36324CC290C46474C096FFFB6C61712FB3155BB678F2076CBD4055EC21DCBFA68A244F44E5F4899EB0A387F93C2561E01FD0438FA3872709B32C4534DCB68B4FD1D07DDC36941FC5B883CD745443110F813D44ED7AE5192402EEF0FEA69FC67E36E27196F1614E49CE539EB37196F38465397BA229486C17D94E23617848133B8E26EC784B46C842EC177BA35EC7DC1EC37E1D4DCC4FF0BE1D4F93BD4A88DAD1F40C12017778FF85A61B554273028F2D0E35E4AA5327022F2ED92AB782266CA75C418DB36DDCAE848FA6995CB615E0FC03B173C8707816EFE51204CEBA75A99A434307FE712D2B653636E2423EBE83CD18C71FCC5A59A8FA1E61096E8C5505750F325446DB0434E234661C373BD8BD99C55DC4755D34CB56C36A85B3099D66EB16F4AAABCFAA3883AF57769AD7066A7B4E3B7BC5DC2853C19D37976BB368340A08B2EC1D26665989C1697B82D5DED7623028D00E7A30389B1B8CAD540D77A76FDAB3C19626849F6DEAC226C3EAD1D39EA6CF9C8391C5024A9BB669859FE0733285AF46501F9F8C5B559A8508631427A0EE17460451D4556CABD54D5581C6C82B28A70F956041882F9506A8F7DA105F8EBD325D2FF7B2EBFC02E379C682"
VideoUseSync=0
Filtering=0

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

[Wizard]
Section0Cb=Japanese Wave Pattern
Section1Cb=Brightness-Gamma-Contrast
Section2Cb=Peak Shape arc down perspective
Section3Cb=Candy caramel With border
Macro0=Song Title
Macro1=Song Author
Macro2=Song made with love and time

