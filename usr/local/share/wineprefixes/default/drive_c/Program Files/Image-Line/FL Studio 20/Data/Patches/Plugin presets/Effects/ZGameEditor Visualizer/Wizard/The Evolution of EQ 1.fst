FLhd   0 * ` FLdt   �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��r  ﻿[General]
GlWindowMode=1
LayerCount=9
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,4,1,3,6,7,2,5,8
WizardParams=419

[AppSet0]
App=Background\SolidColor
FParamValues=0,0.624,0.756,0.908
ParamValues=0,624,756,908
Enabled=1
Collapsed=1
Name=Background

[AppSet4]
App=HUD\HUD Meter Radial
AppVersion=1
FParamValues=0.18,0.952,0.88,0.008,0.14,0.116,0,0,0.5,0.5,0.268,0.242,4,0.162,3,0,0,0.1,0.508,1
ParamValues=180,952,880,8,140,116,0,0,500,500,268,242,4,162,3,0,0,100,508,1
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,648,500,0,500,500,500,500
ParamValuesBackground\SolidColor=0,624,756,908
ParamValuesHUD\HUD Graph Polar=0,500,0,0,500,436,286,444,500,0,1000,0,1000,0,250,500,200,0,0,0,500,1000
Enabled=1
Collapsed=1
Name=LOAD
LayerPrivateData=7801734C4ECD2BD62B2949631899000058F00367

[AppSet1]
App=Peak Effects\Polar
FParamValues=0.724,0,0,0,0.5,0.5,0.5,0.548,0.5,0.632,0.468,0.5,0.028,0.456,0,0.708,1,1,1,1,0.356,0.32,0,1
ParamValues=724,0,0,0,500,500,500,548,500,632,468,500,28,456,0,708,1000,1000,1000,1,356,320,0,1
Enabled=1
Collapsed=1
Name=EQ 1

[AppSet3]
App=Peak Effects\Polar
FParamValues=0.888,0,0,0,0.5,0.5,0.5,0.568,0.5,0.632,0.468,0.5,0.028,0,0,0.708,0.956,1,1,1,0.356,0.32,0,1
ParamValues=888,0,0,0,500,500,500,568,500,632,468,500,28,0,0,708,956,1000,1000,1,356,320,0,1
Enabled=1
Collapsed=1
Name=EQ 2

[AppSet6]
App=Peak Effects\Polar
FParamValues=0.82,0,1,0,0.5,0.5,0.5,0.568,0.5,0.58,0.468,0.5,0.016,0.604,0,0.708,0.956,1,1,1,0.356,0.32,0,1
ParamValues=820,0,1000,0,500,500,500,568,500,580,468,500,16,604,0,708,956,1000,1000,1,356,320,0,1
Enabled=1
Collapsed=1
Name=EQ 3

[AppSet7]
App=Peak Effects\Polar
FParamValues=0.548,0.928,1,0,0.5,0.5,0.5,0.568,0.5,0.568,0.468,0.5,0.016,0.156,0,0.708,0.956,1,1,1,0.356,0.32,0,1
ParamValues=548,928,1000,0,500,500,500,568,500,568,468,500,16,156,0,708,956,1000,1000,1,356,320,0,1
Enabled=1
Collapsed=1
Name=EQ 4

[AppSet2]
App=Text\TextTrueType
FParamValues=0.084,0,0,0,0,0.5,0.5,0,0,0,0.5
ParamValues=84,0,0,0,0,500,500,0,0,0,500
Enabled=1
Collapsed=1
Name=Main Text

[AppSet5]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fade in-out

[AppSet8]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""4"" y=""5""><p><font face=""American-Captain"" size=""5"" color=""#F9F9F9"">[author]</font></p></position>","<position x=""4"" y=""9""><p><font face=""Chosence-Bold"" size=""3"" color=""#F9F9F9"">[title]</font></p></position>","<position x=""4"" y=""15.5""><p> <font face=""Chosence-Bold"" size=""3"" color=""#F9F9F9"">[comment]</font></p></position>"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

