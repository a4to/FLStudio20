FLhd   0 * ` FLdt�  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��*D  ﻿[General]
GlWindowMode=1
LayerCount=15
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=12,1,9,6,0,5,2,3,4,8,10,7,11,13,14
WizardParams=563,613,614,615,616,617

[AppSet12]
App=HUD\HUD Prefab
FParamValues=0,0,0.5,0,0,0.5,0.391,0.25,1,1,4,0,0.5,1,0.368,0.101,1,1
ParamValues=0,0,500,0,0,500,391,250,1000,1000,4,0,500,1,368,101,1000,1
Enabled=1
UseBufferOutput=1
Name=LOGO
LayerPrivateData=780173C8CBCF4BD52B2E4B671899000060F9036F

[AppSet1]
App=Background\SolidColor
FParamValues=0,0.644,0.688,0.676
ParamValues=0,644,688,676
Enabled=1
Collapsed=1
Name=Siri 1.0 BG

[AppSet9]
App=Postprocess\ScanLines
FParamValues=0,1,1,1,0
ParamValues=0,1,1000,1000,0
ParamValuesBackground\FourCornerGradient=0,100,840,1000,1000,610,1000,1000,528,1000,1000,750,1000,1000
ParamValuesPostprocess\Youlean Blur=1000,1000,1000
ParamValuesPostprocess\FrameBlur=60,0,0,0,1000,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
Name=Grid

[AppSet6]
App=Peak Effects\Linear
FParamValues=0.856,0.676,0.368,0.208,0.726,0.5,0.464,0.096,0,0.5,0,0.104,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0.472,1,0.152,0.2,1,0.072,0.016,0.654,0.47,0.244
ParamValues=856,676,368,208,726,500,464,96,0,500,0,104,500,1,0,1,0,500,500,500,500,472,1000,152,200,1000,72,16,654,470,244
Enabled=1
Collapsed=1
ImageIndex=1
Name=SIRI EQ 1

[AppSet0]
App=Peak Effects\Linear
FParamValues=0.332,0,0.696,0,0.669,0.5,0.464,0.104,0,0.5,0,0.044,0.501,1,0,1,0,0.5,0.5,0.5,0.5,0,1,0.024,0.136,0.724,0.196,0.432,0.71,0.25,0.596
ParamValues=332,0,696,0,669,500,464,104,0,500,0,44,501,1,0,1,0,500,500,500,500,0,1000,24,136,724,196,432,710,250,596
Enabled=1
Collapsed=1
ImageIndex=1
Name=SIRI EQ 2

[AppSet5]
App=Peak Effects\Linear
FParamValues=0.592,0.448,0.576,0,0.69,0.5,0.464,0.188,0,0.5,0,0.028,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,1,0.152,0.244,0.848,0.332,0.412,0.434,0.25,0.1
ParamValues=592,448,576,0,690,500,464,188,0,500,0,28,500,1,0,1,0,500,500,500,500,0,1000,152,244,848,332,412,434,250,100
Enabled=1
Collapsed=1
ImageIndex=1
Name=SIRI EQ 3

[AppSet2]
App=Peak Effects\Linear
FParamValues=0.704,0.628,0.608,0,0.669,0.5,0.464,0.004,0.5,0.5,0,0.056,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,1,0.024,0.212,0.976,0.112,0.616,0.498,0.43,0.268
ParamValues=704,628,608,0,669,500,464,4,500,500,0,56,500,1,0,1,0,500,500,500,500,0,1000,24,212,976,112,616,498,430,268
Enabled=1
Collapsed=1
ImageIndex=1
Name=SIRI EQ 4

[AppSet3]
App=Peak Effects\Linear
FParamValues=0.788,0.456,0.736,0,0.73,0.5,0.464,0.204,0.5,0.5,0,0.051,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,1,0.152,0.2,0.504,0.016,0.412,0.434,0.25,0.1
ParamValues=788,456,736,0,730,500,464,204,500,500,0,51,500,1,0,1,0,500,500,500,500,0,1000,152,200,504,16,412,434,250,100
Enabled=1
Collapsed=1
ImageIndex=1
Name=SIRI EQ 5

[AppSet4]
App=Peak Effects\Linear
FParamValues=0.004,0.748,0,0,0.702,0.5,0.464,0.016,0.5,0.5,0,0.029,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,0.448,0.152,0.2,0,0.056,0.412,0.434,0.226,0.74
ParamValues=4,748,0,0,702,500,464,16,500,500,0,29,500,1,0,1,0,500,500,500,500,0,448,152,200,0,56,412,434,226,740
Enabled=1
Collapsed=1
ImageIndex=1
Name=SIRI EQ 6

[AppSet8]
App=Text\TextTrueType
FParamValues=0.488,0,0,1,0,0.5,0.497,0,0,0,0.5
ParamValues=488,0,0,1000,0,500,497,0,0,0,500
ParamValuesPostprocess\Youlean Blur=76,0,0
ParamValuesPostprocess\Youlean Bloom=600,0,578,18
ParamValuesBackground\FourCornerGradient=467,628,0,492,1000,210,492,1000,528,492,1000,750,492,1000
ParamValuesPostprocess\Youlean Color Correction=500,500,500,500,500,424
ParamValuesBackground\SolidColor=0,644,688,808
Enabled=1
Name=Main Text

[AppSet10]
App=Text\TextTrueType
FParamValues=0,0,0,0,0,0.5,0.5,0,0,0,0.5
ParamValues=0,0,0,0,0,500,500,0,0,0,500
Enabled=1
BufferRenderQuality=6

[AppSet7]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fade in-out

[AppSet11]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1

[AppSet13]
App=Image effects\Image
FParamValues=0.415,0,0,0.954,1,0.5,0.501,0,0,0,0,0,0,0
ParamValues=415,0,0,954,1000,500,501,0,0,0,0,0,0,0
Enabled=1

[AppSet14]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="<position y=""86""><p align=""center""><font face=""Chosence-Bold"" size=""2"" color=""#fff"">Image-Line Software</font></p></position>","<position y=""88  ""><p align=""center""><font face=""Chosence-regular"" size=""2"" color=""#CCCCC1"">Exclusive Sounds</font></p></position>"
Html="<position y=""9""><p align=""center""><font face=""American-Captain"" size=""6"" color=""#E1E1E1"">[author]</font></p></position>","<position y=""14""><p align=""center""><font face=""Chosence-Bold"" size=""4"" color=""#E1E1E1"">[title]</font></p></position>","<position y=""23""><p align=""center""><font face=""Chosence-Bold"" size=""3"" color=""#E1E1E1"">[comment]</font></p></position>"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

