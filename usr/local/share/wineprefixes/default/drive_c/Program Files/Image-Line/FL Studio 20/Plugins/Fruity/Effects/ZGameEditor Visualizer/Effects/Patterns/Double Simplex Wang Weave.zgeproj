<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" FileVersion="2">
  <OnLoaded>
    <ZLibrary>
      <Source>
<![CDATA[inline float angle(float X)
{
  if(X >= 0 && X < 360)return X;
  if(X > 360)return X-floor(X/360)* 360;
  if(X <   0)return X+floor(X/360)*-360;
}

vec3 hsv(float H, float S, float V)
{
  vec3 Color;
  float R,G,B,I,F,P,Q,T;

  H = angle(H);
  S = clamp(S,0,100);
  V = clamp(V,0,100);

  H /= 60;
  S /= 100;
  V /= 100;

  if(S == 0)
  {
    Color[0] = V;
    Color[1] = V;
    Color[2] = V;
  }
  else
  {
    I = floor(H);
    F = H-I;

    P = V*(1-S);
    Q = V*(1-S*F);
    T = V*(1-S*(1-F));

    if(I == 0){R = V; G = T; B = P;}
    if(I == 1){R = Q; G = V; B = P;}
    if(I == 2){R = P; G = V; B = T;}
    if(I == 3){R = P; G = Q; B = V;}
    if(I == 4){R = T; G = P; B = V;}
    if(I == 5){R = V; G = P; B = Q;}

    Color[0] = R;
    Color[1] = G;
    Color[2] = B;
  }
  return Color;
}]]>
      </Source>
    </ZLibrary>
  </OnLoaded>
  <OnUpdate>
    <ZExpression>
      <Expression>
<![CDATA[uResolution=vector2(app.ViewportWidth,app.ViewportHeight);
uViewport=vector2(app.ViewportX,app.ViewportY);
uAlpha =1.0-Parameters[0];

float speed=(Parameters[4]-0.5)*4.0;
float delta=app.DeltaTime*Speed; //comment to use other time options
uTime+=delta;

vec3 col = hsv(Parameters[1]*360-180.0,Parameters[2]*100,(1-Parameters[3])*100);
uColor = vector3(1.0-col[0],1.0-col[1],1.0-col[2]);]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <OnRender>
    <UseMaterial Material="mCanvas"/>
    <RenderSprite/>
  </OnRender>
  <Content>
    <Shader Name="MainShader">
      <VertexShaderSource>
<![CDATA[#version 120

void main(){
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[#version 120

uniform vec2 iResolution,iViewport;
uniform float iTime;
uniform float iAlpha;
uniform vec3 iColor;
uniform float iScale,iScroll,iRotation;

/*
float Truncate(float val) { return clamp(val,0.0,1.0); }

vec3 TransformHSV(vec3 c, float H, float S, float V) {
  float M_PI = 3.1415926;
	float VSU = V * S * cos(H * M_PI / 180.0);
	float VSW = V * S * sin(H * M_PI / 180.0);
  vec3 ret;
	ret.r = Truncate((0.299 * V + 0.701 * VSU + 0.168 * VSW) * c.r
		+ (0.587 * V - 0.587*VSU + 0.330 * VSW) * c.g
		+ (0.114 * V - 0.114*VSU - 0.497*VSW) * c.b);
	ret.g = Truncate((0.299 * V - .299 * VSU - 0.328 * VSW) * c.r
		+ (0.587 * V + 0.413 * VSU + 0.035 * VSW) * c.g
		+ (0.114 * V - 0.114 * VSU + 0.292 * VSW) * c.b);
	ret.b = Truncate((0.299 * V - 0.3 * VSU + 1.25 * VSW) * c.r
		+ (0.587 * V - 0.588 * VSU - 1.05 * VSW) * c.g
		+ (0.114 * V + 0.886 * VSU - 0.203 * VSW) * c.b);
	return ret;
} // col = TransformHSV(clamp(col, 0., 1.), iColorHSV[0], iColorHSV[1], iColorHSV[2]);
*/

/*


	Double Simplex Wang Weave
	-------------------------

	I don't know if I'd strictly refer to this as a Wang tile example, rather than
	applying Wang tile concepts to BigWIngs's double simplex weave notion. Either way,
	the resultant pattern is pretty unique.

	In an overly general sense, modern Truchet tiling relies on rotational symmetry
    (the original right triangle pattern did not), and as such require that any edge of
	one tile matches or connects with all edges of another. Wang tiles, on the other
	hand, are edge specific (some edges don't match others), which allows for more
	interesting variations.

	I'm sure most have heard of Wang tiles before. If not, they're well worth looking
	into, since it's possible to do so many cool things with them. Almost all examples
	you see involve quads rather than triangles. I'm not sure why, but I guess triangles
	are a little more difficult to work with, plus there'd be fewer edge combinations.

	However, if you make use of BigWIngs's double edge concept, you can increase the
	number of triangle edge combinations to create a visually rich weave pattern, but
	with haphazard sparse regions that are common to Wang tiling.

	If you take a look at a double simplex Truchet weave (uncomment the NO_EDGE_MATCHING
    define below), you'll see that every triangular tile is filled in and all contain
	the same number of Bezier cords, resulting in a tight, dense pattern. However, when
	edge matching is applied, the number of Bezier cords vary from three to zero, thus
    allowing regions of variable density and gaps to form, which breaks up the space.

	Ultimately, applying the principles in this example to a 3D grid block would create
	a pretty crazy structure, and I'd definitly like to try that at some stage, but
	this 2D pattern	will have to suffice for now.

	Anyway, this is just one of so many different variations. The quad version would be
	easier to code up and, in my opinion, would look better -- I only went the simplex
	route because I wanted to provide code for a novel example that wouldn't be readily
	accessible. I also have some more traditional Wang related patterns that I intend
	to post at some stage, which should be much easier to digest.



	Adapted from:

	Double Simplex Truchet Weave
	https://www.shadertoy.com/view/3lB3Rc - Shane

	Which was based on the following:

	Double Triangle Truchet Doodle - BigWIngs
	https://www.shadertoy.com/view/Ml2yzD

	Links:

	// I searched high and low for a triangle-based Wang tile image, but couldn't find
    // one, until Fabrice linked to something he did many years ago. Fabrice Neyret's
    // name often pops up on papers when I'm looking for imagery. :)
    //
	// https://hal.inria.fr/inria-00537511


*/


// Show the Wang edge connected lines. Not that exciting, but it's does illustrate the
// most basic simplex Wang pattern.
#define SHOW_WANG_EDGE_CONNECTIONS


// Displaying the two color Wang regions for the simplex grid: Note that triangles on
// neighboring edges share the same regional edge color. By the way, there are tri color,
// quad color, etc, versions as well.
//#define SHOW_WANG_REGIONS


// Just the normal Truchet variation. In fact, if you compared it to my previous
// Truchet version, the pattern should look exactly the same.
//#define NO_EDGE_MATCHING




// Standard 2D rotation formula.
mat2 rot2(in float a){ float c = cos(a), s = sin(a); return mat2(c, -s, s, c); }


// Standard vec2 to float hash - Based on IQ's original.
float hash21(vec2 p){ return fract(sin(dot(p, vec2(141.213, 289.197)))*43758.5453); }


// Unsigned distance to the segment joining "a" and "b".
float distLine(vec2 a, vec2 b){

	b = a - b;
	float h = clamp(dot(a, b)/dot(b, b), 0., 1.);
    return length(a - b*h);
}


// IQ's signed distance to a 2D triangle.
float sdTriangle( in vec2 p0, in vec2 p1, in vec2 p2, in vec2 p ){

	vec2 e0 = p1 - p0;
	vec2 e1 = p2 - p1;
	vec2 e2 = p0 - p2;

	vec2 v0 = p - p0;
	vec2 v1 = p - p1;
	vec2 v2 = p - p2;

	vec2 pq0 = v0 - e0*clamp( dot(v0,e0)/dot(e0,e0), 0.0, 1.0 );
	vec2 pq1 = v1 - e1*clamp( dot(v1,e1)/dot(e1,e1), 0.0, 1.0 );
	vec2 pq2 = v2 - e2*clamp( dot(v2,e2)/dot(e2,e2), 0.0, 1.0 );

    float s = sign( e0.x*e2.y - e0.y*e2.x );
    vec2 d = min( min( vec2( dot( pq0, pq0 ), s*(v0.x*e0.y-v0.y*e0.x) ),
                       vec2( dot( pq1, pq1 ), s*(v1.x*e1.y-v1.y*e1.x) )),
                       vec2( dot( pq2, pq2 ), s*(v2.x*e2.y-v2.y*e2.x) ));

	return -sqrt(d.x)*sign(d.y);
}


// Cheap and nasty 2D smooth noise function with inbuilt hash function -- based on IQ's
// original. Very trimmed down. In fact, I probably went a little overboard. I think it
// might also degrade with large time values.
float n2D(vec2 p){

    // Setup.
    // Any random integers will work, but this particular
    // combination works well.
    const vec2 s = vec2(1.0, 113.0);
    // Unique cell ID and local coordinates.
    vec2 ip = floor(p); p -= ip;
    // Vertex IDs.
    vec4 h = vec4(0., s.x, s.y, s.x + s.y) + dot(ip, s);

    // Smoothing.
    p = p*p*(3. - 2.*p);
    //p *= p*p*(p*(p*6. - 15.) + 10.); // Smoother.

    // Random values for the square vertices.
    h = fract(sin(h)*43758.5453);

    // Interpolation.
    h.xy = mix(h.xy, h.zw, p.y);
    return mix(h.x, h.y, p.x); // Output: Range: [0, 1].
}

// Each triangle shares a unique edge midpoint with its neighbor, which means the
// generated random number will be the same for that edge in either triangle. This
// is important when connecting edge patterns in Wang tiles, etc.
vec3 edges(vec2 id0, vec2 id1, vec2 id2){

    // Return a unique random number for the midpoint of each triangle edge.
    return vec3(hash21(mix(id0, id1, .5)), hash21(mix(id1, id2, .5)), hash21(mix(id2, id0, .5)));

}

// IQ's signed distance to a quadratic Bezier. Like all of IQ's code, it's quick
// and reliable. :) Having said that, I've cut it down to suit this particular
// example, so if you need the proper function, you should look at the original.
//
// Quadratic Bezier - 2D Distance - IQ
// https://www.shadertoy.com/view/MlKcDD
//
float sdBezier(vec2 pos, vec2 A, vec2 B, vec2 C){

    // p(t)    = (1 - t)^2*p0 + 2(1 - t)t*p1 + t^2*p2
    // p'(t)   = 2*t*(p0 - 2*p1 + p2) + 2*(p1 - p0)
    // p'(0)   = 2*(p1 - p0)
    // p'(1)   = 2*(p2 - p1)
    // p'(1/2) = 2*(p2 - p0)

    vec2 a = B - A;
    vec2 b = A - 2.*B + C;
    vec2 c = a * 2.;
    vec2 d = A - pos;

    // If I were to make one change to IQ's function, it'd be to cap off the value
    // below, since I've noticed that the function will fail with straight lines.
    float kk = 1./max(dot(b,b), 1e-6); // 1./dot(b,b);
    float kx = kk*dot(a,b);
    float ky = kk*(2.*dot(a,a) + dot(d,b))/3.;
    float kz = kk*dot(d,a);

    float res = 0.;

    float p = ky - kx*kx;
    float p3 = p*p*p;
    float q = kx*(2.*kx*kx - 3.*ky) + kz;
    float h = q*q + 4.*p3;

    h = max(h, 0.);

    //if(h >= 0.0){

        h = sqrt(h);
        vec2 x = (vec2(h, -h) - q) / 2.;
        vec2 uv = sign(x)*pow(abs(x), vec2(1./3.));
        float t = uv.x + uv.y - kx;
        t = clamp( t, 0., 1.);
        //tm = t;

        // 1 root
        vec2 qos = d + (c + b*t)*t;
        res = length(qos);

    /*}
    else {

        float z = sqrt(-p);
        float v = acos( q/(p*z*2.0) ) / 3.0;
        float m = cos(v);
        float n = sin(v)*1.732050808;
        vec3 t = vec3(m + m, -n - m, n - m) * z - kx;
        t = clamp( t, 0.0, 1.0 );


        // 3 roots
        vec2 qos = d + (c + b*t.x)*t.x;
        float dis = dot(qos,qos);

        res = dis;

        qos = d + (c + b*t.y)*t.y;
        dis = dot(qos,qos);
        res = min(res,dis);

        qos = d + (c + b*t.z)*t.z;
        dis = dot(qos,qos);
        res = min(res,dis);

        //tm = res;//min(min(t.x, t.y), t.z);

        res = sqrt( res );

        tm = res;
    }
    */
    return res;
}



// Rendering the smooth Bezier segment. The idea is to calculate the midpoint
// between "a.xy" and "b.xy," then offset it by the average of the combined normals
// at "a" and "b" multiplied by a factor based on the length between "a" and "b."
// At that stage, render a Bezier from "a" to the midpoint, then from the midpoint
// to "b." I hacked away to come up with this, which means there'd have to be a more
// robust method out there, so if anyone is familiar with one, I'd love to know.
float doSeg(vec2 p, vec4 a, vec4 b, float r){

    // Mid way point.
    vec2 mid = (a.xy + b.xy)/2.; // mix(a.xy, b.xy, .5);

    // The length between "a.xy" and "b.xy," multiplied by... a number that seemed
    // to work... Worst coding ever. :D
    float l = length(b.xy - a.xy)*1.732/6.; // (1.4142 - 1.)/1.4142;

    // Points on the same edge each have the same normal, and segments between them
    // require a larger arc. There was no science behind the decision. It's just
    // something I noticed and hacked a solution for. Comment the line out, and you'll
    // see why it's necessary. By the way, replacing this with a standard semicircular
    // arc would be even better, but this is easier.
    if(abs(length(b.zw - a.zw))<.01) l = r;

    // Offsetting the midpoint between the exit points "a" and "b"
    // by the average of their normals and the line length factor.
    mid += (a.zw + b.zw)/2.*l;

    // Piece together two quadratic Beziers to form the smooth Bezier curve from the
    // entry and exit points. The only reliable part of this method is the quadratic
    // Bezier function, since IQ wrote it. :D
    float b1 = sdBezier(p, a.xy, a.xy + a.zw*l, mid);
    float b2 = sdBezier(p, mid, b.xy + b.zw*l, b.xy);


    // Return the minimum distance to the smooth Bezier arc.
    return min(b1, b2);
}


// vec4 swap.
void swap(inout vec4 a, inout vec4 b){ vec4 tmp = a; a = b; b = tmp; }

// A swap without the extra declaration -- It works fine on my machine, but I'm
// not game  enough to use it, yet. :)
//void swap(inout vec4 a, inout vec4 b){ a = a + b; b = a - b; a = a - b; }


// To be more succinct, it's a double simplex edge-matched weave pattern.
vec3 pattern(vec2 p){


    // Scaling constant.
    const float gSc = 4.;
    p *= gSc;

	// Sampling and mixing one the textures, just for something to do.
    //
    // By the way, I wouldn't quote me on this, but I believe textures are stored in sRGB
    // format -- I've never found this particularly helpful, but it is what it is. Anyway,
    // this means the texels need to be converted to linear space (squaring the values is a
    // rough appoximation) before working with them. The original texture values are restored
    // to sRGB during gamma correction at the end (roughly taking the square root) before
    // presenting to the screen.
    //
    vec3 tx = iColor;//texture(iChannel0, p/gSc).xyz; tx *= tx;
    vec3 tx2 = iColor;//texture(iChannel0, p.yx/gSc*2.).xyz; tx2 *= tx2;
    tx = mix(tx, tx2, .5);

    // Resolution based smoothing factor.
    float sf = 1.5/iResolution.y*gSc;


    // SIMPLEX GRID SETUP


    vec2 s = floor(p + (p.x + p.y)*.36602540378); // Skew the current point.

    p -= s - (s.x + s.y)*.211324865; // Use it to attain the vector to the base vertex (from p).

    // Determine which triangle we're in. Much easier to visualize than the 3D version.
    float i = p.x < p.y? 1. : 0.; // Apparently, faster than: i = step(p.y, p.x);
    vec2 ioffs = vec2(1. - i, i);

    float dir = i<.5? -1.: 1.;

    // Vectors to the other two triangle vertices.
    vec2 ip0 = vec2(0), ip1 = ioffs - .2113248654, ip2 = vec2(.577350269);
    vec2 id0 = s, id1 = s + ioffs, id2 = s + 1.;

    // Make the vertices match up.
    if(i>.5) {
        vec2 tmp = ip0; ip0 = ip2; ip2 = tmp;
        tmp = id0; id0 = id2; id2 = tmp;
    }


    // Centralize everything, so that vec2(0) is in the center of the triangle.
    vec2 ctr = (ip0 + ip1 + ip2)/3.; // Centroid.
    //
    ip0 -= ctr; ip1 -= ctr; ip2 -= ctr; p -= ctr;




    // Displaying the 2D simplex grid. Basically, we're rendering lines between
    // each of the three triangular cell vertices to show the outline of the
    // cell edges.
    float tri = min(min(distLine(p - ip0, p - ip1), distLine(p - ip1, p - ip2)),
                  distLine(p - ip2, p - ip0));

    /*
    float div = 1e5;
    div = min(div, distLine(p, p - mix(ip0, ip1, .5)));
    div = min(div, distLine(p, p - mix(ip1, ip2, .5)));
    div = min(div, distLine(p, p - mix(ip2, ip0, .5)));
    */

    // The unique cell ID.
    //vec2 uID = (s*3. + ioffs + 1.)/3.;


    // Obtaining a random number for each cell edge. It's important to to pass the
    // vertex IDs to the "edges" function and not the vertex points like I did. :)
    vec3 edge = edges(id0, id1, id2);



    #ifdef NO_EDGE_MATCHING
    // A negative threshold means all curves and points are rendered.
    const float tRnd = -1.;
    #else
    // This means there's a 30% chance that an edge will be empty, and thus,
    // not used.
    const float tRnd = .3;
    #endif


    // The cell edges ID.
    float id = 0.;

    // This is a standard way to encode binary information into decimal form.
    // If the random edge value is over a specific threshold, encode the correct
    // binary digit.
    if(edge.x>tRnd) id += 1.;
    if(edge.y>tRnd) id += 2.;
    if(edge.z>tRnd) id += 4.;

    // Decode each binary digit.
    vec3 bits = mod(floor(id/vec3(1, 2, 4)), 2.);


    // Connecting points around the triangles. Two for each side. I should probably
    // use a bit of trigonometry and hard code these, but I was feeling lazy. :)
    const float offs = .204124; // Approx: length(ip0 - ip1)/4., or sqrt(1./24.);
    vec2 m01s = mix(ip0, ip1, .5 + offs);
    vec2 m01t = mix(ip0, ip1, .5 - offs);
    vec2 m12s = mix(ip1, ip2, .5 + offs);
    vec2 m12t = mix(ip1, ip2, .5 - offs);
    vec2 m20s = mix(ip2, ip0, .5 + offs);
    vec2 m20t = mix(ip2, ip0, .5 - offs);

    // The boundary normals for each point. I should probably hardcode these as well.
    vec2 n01 = -normalize(mix(ip0, ip1, .5));
    vec2 n12 = -normalize(mix(ip1, ip2, .5));
    vec2 n20 = -normalize(mix(ip2, ip0, .5));



    // Line variable. One for each edge.
    vec3 rLn = vec3(1e5);


    // Array index. It increases by two for every extra Bezier segment and pair of
    // end points needed. See below.
    int index = 0;

    // The actual points: The idea is to only add the necessary points to the array,
    // depending on how many edges line up, which would be 0, 1, 2, or all 3.
    // However, just in case my logic isn't up to scratch, I'll zero them all out,
    // just to be on the safe side.
    //
    // Each edge corresponds to two entry and exit points. These have to be shuffled
    // to produce the random weave you see, so it's necessary to keep track of the
    // the maximum array count. Variable array counts are possible in WebGL 2.0, but
    // I'd like to try to keep things backward compatible, etc.
    vec4[6] pnt = vec4[6](vec4(0), vec4(0), vec4(0), vec4(0), vec4(0), vec4(0));

    // If the first binary digit is flagged (one, not zero), fill in the point
    // information for the first two array spots, construct an edge, then increase the
    // index by two.
    if(bits.x>.5){

        // Adding the point and normal information for this particular edge, to be
        // shuffled first, then passed to the Bezier cord construction formula.
        pnt[index] = vec4(m01s, n01);
        pnt[index + 1] = vec4(m01t, n01);
        index += 2;

        // If the digit is a one, construct an edge from the cells center to
        // the corresponding edge midpoint.
        rLn.x = min(rLn.x, distLine(p, p - mix(ip0, ip1, .5)));

    }

    // Do the same for the second binary digit.
    if(bits.y>.5){

        pnt[index] = vec4(m12s, n12);
        pnt[index + 1] = vec4(m12t, n12);
        index += 2;

        rLn.y = min(rLn.y, distLine(p, p - mix(ip1, ip2, .5)));

    }

    // Third binary digit.
    if(bits.z>.5){

        pnt[index] = vec4(m20s, n20);
        pnt[index + 1] = vec4(m20t, n20);
        index += 2;

        rLn.z = min(rLn.z, distLine(p, p - mix(ip2, ip0, .5)));

    }

    // Combine all edges, and give it a thickness.
    float ln = min(min(rLn.x, rLn.y), rLn.z) - .02;


    // If lines need to be rendered, render a circle in the middle of the cell,
    // for decorative purposes.
    if(index>0) ln = min(ln, abs(length(p) - .05) - .02);

    // Shuffling the variable array of points and normals -- Six is the maximum. I think this
    // is the Fisher?Yates method, but don't quote me on it. It's been a while since I've used
    // a shuffling algorithm, so if there are inconsistancies, etc, feel free to let me know.
    //
    // For various combinatorial reasons, some non overlapping tiles will probably be
    // rendered more often, but generally speaking, the following should suffice.
    //
    for(int i = 5; i>0; i--){

        // For less than three edges, skip the upper array positions.
        if(i >= index) continue;

        // Using the cell ID and shuffle number to generate a unique random number.
        float fi = float(i);

        // Random number for each triangle: The figure "s*3 + ioffs + 1" is unique for
        // each triangle... I can't remember why I felt it necessary to divide by 3,
        // but I'll leave it in there. :)
        float rs = hash21((s*3. + ioffs + 1.)/3. + fi/float(index));

        // Other array point we're swapping with.
        //int j = int(floor(mod(rs*float(index)*1e6, fi + 1.)));
        // I think this does something similar to the line above, but if not, let us know.
        int j = int(floor(rs*(fi + .9999)));
        swap(pnt[i], pnt[j]);

    }

    // Constructing the Bezier cords for the cell, and the exit and entry points
    // for each cord.
    vec3 d = vec3(1e5), pnts = vec3(1e5);

    for(int i = 0; i<3; i++){

        // Skip when the index isn't high enough. In other words, if there are
        // only two points for the cell (one cord), break after the first
        // iteration, etc.
        if(index<(i*2+1)) break;

        // Marking the exit and entry points with some dots, for illustrative
        // and decorative purposes.
        pnts[i] = min(pnts[i], length(p - pnt[i*2].xy));
        pnts[i] = min(pnts[i], length(p - pnt[i*2 + 1].xy));

        // Contruct the Bezier segment between the two random edge points.
        d[i] =  doSeg(p,  pnt[i*2], pnt[i*2 + 1], offs);

    }

    // Give the end points and Bezier segments some width.
    pnts -= .045;
    d -= .1;



    // Initiate the scene color to the Shadertoy texture.
    vec3 col = tx;

    //col = mix(col, vec3(0), (1. - smoothstep(0., sf, tri - .0025)));

    // Concentric triangular pattern, for decorative purposes.
    // The following are interesting too: min(tri, div), max(tri, div).
    float pat = sin(tri*6.2831*24. + 3.14159*.75) + .8;


    // Blurred connecting cell lines for fake shadows. Uncomment to see what it does.
    col *= smoothstep(0., sf*12., ln - .15)*.25 + .75;

    // Using the connecting lines to fade the concentric triangular pattern -- The step is
    // not important, and just something I made up on the spot. A few days from now, even
    // I wouldn't know that it does. :D
    pat = max(pat, smoothstep(0., sf*12., ln - .15)*.75);
    //
    // Blending in the pattern. I think it's shorthand for the line below:
    // col = mix(col, vec3(0), 1. - pat);
    col *= pat;



    // Bezier line color.
    vec3 lCol = tx*4.;

    // Bezier stripe color.
    vec3 stCol = mix(lCol, vec3(1.3, 1, .7), .75);

    // Saving the background color for later usage.
    vec3 tmp = col;


	#ifdef SHOW_WANG_REGIONS

    d += 1e5; // A hacky way to take away the weave pattern.
    pnts += 1e5; // Same for the Bezier curve end points.

    //vec3 eRnd = vec3(hash21(mix(id0, id1, .5)), hash21(mix(id1, id2, .5)), hash21(mix(id2, id0, .5)));
    //vec3 edgeDot = vec3(length(p - mix(ip0, ip1, .5)), length(p - mix(ip1, ip2, .5)),
    //                    length(p - mix(ip2, ip0, .5)) );
    //edgeDot -= .06;


    // Using IQ's 2D triangle formula to constract the triangular edge region.
    vec3 wTri = vec3(sdTriangle(vec2(0), ip0, ip1, p), sdTriangle(vec2(0), ip1, ip2, p),
                       sdTriangle(vec2(0), ip2, ip0, p));
    // Shrinking the triangle a bit.
    wTri += .005;


    for(int i = 0; i<3; i++){

        // Random colored dots. Not used here.
        //vec3 dCol = 1.-vec3(1, 1. - eRnd[i], eRnd[i]*.25).zyx;

        // Brown regions represent empty regions, and green regions
        // represent regions where connections are rendered.
        vec3 fCol = mix(tmp, vec3(1, .8, 0), .75);
        if(bits[i]>.5) fCol = mix(tmp, vec3(.5, 1, .1), .75);//


        // Render the colored Wang region for this particular edge.
        col = mix(col, vec3(0), (1. - smoothstep(0., sf*3., wTri[i] - .02))*.5);
        col = mix(col, vec3(0), (1. - smoothstep(0., sf, wTri[i] - .02)));
        col = mix(col, fCol*fCol*1.8, (1. - smoothstep(0., sf, wTri[i])));

        /*
        // Edge dots: Not used here.
        col = mix(col, vec3(0), (1. - smoothstep(0., sf*6., edgeDot[i] - .02))*.5);
        col = mix(col, vec3(0), (1. - smoothstep(0., sf, edgeDot[i] - .02)));
        col = mix(col, fCol, (1. - smoothstep(0., sf, edgeDot[i])));
        */
    }



    // Render the white triangular cell edges.
    tri -= .02;

    float vrts = min(min(length(p - ip0), length(p - ip1)), length(p - ip2)); // Vertices.
    tri = min(tri, abs(vrts - .06) - .02);
    col = mix(col, vec3(0), (1. - smoothstep(0., sf*6., tri - .02))*.5);
    col = mix(col, vec3(0), (1. - smoothstep(0., sf, tri - .02)));
    col = mix(col, mix(tmp*2., vec3(1.25), .75), (1. - smoothstep(0., sf, tri + .0025)));
    col = mix(col, vec3(0), (1. - smoothstep(0., sf, tri + .02)));

    #endif


    #ifdef SHOW_WANG_EDGE_CONNECTIONS
    // Lay down the edge connections. I made them blue to contrast with the brown.
    vec3 cCol = mix(tmp*2., vec3(.3, .8, 1.25), .75);
    //cCol = cCol.xzy; // Green, etc.
    col = mix(col, vec3(0), (1. - smoothstep(0., sf*6., ln - .02))*.5);
    col = mix(col, vec3(0), (1. - smoothstep(0., sf, ln - .02)));
    col = mix(col, cCol, (1. - smoothstep(0., sf, ln + .0025)));
    col = mix(col, vec3(0), (1. - smoothstep(0., sf, ln + .02)));
    #endif

    #ifndef SHOW_WANG_REGIONS
    for(int i = 0; i<3; i++){

        // Only render as many edge connecting points, Beziers, etc, as needed. It's not
        // absolutely necessary, but I guess it'd save a few cycles... Although, on GPUs,
        // it'd difficult to know for sure. :)
        if(i>=index/2) break;

        // Save the underlying color.
        tmp = col;

        // Shading. Not used.
        //float sh = max(-(d[i]+.02)*64., 0.);

        // Render the Bezier shadows, edges, etx.
        col = mix(col, vec3(0), (1. - smoothstep(0., sf*6., d[i] - .02))*.5);
        col = mix(col, vec3(0), 1. - smoothstep(0., sf, d[i]));
        col = mix(col, lCol, 1. - smoothstep(0., sf, d[i]+.02));

        // The white stripe down the center.
        float divLn = d[i] + .06; // max(d[i] - .01, tri - .03);
        col = mix(col, vec3(0), (1. - smoothstep(0., sf*4., divLn))*.5);
        col = mix(col, vec3(0), 1. - smoothstep(0., sf, divLn));
        col = mix(col, mix(tmp, stCol*1.5, .75), (1. - smoothstep(0., sf, divLn + .02)));

        // Render some rivits at the cell boundary end points of the Beziers, for
        // illustrative and decorative purposes.
        col = mix(col, vec3(0), (1. - smoothstep(0., sf*2., pnts[i] - .01))*.35);
        col = mix(col, vec3(0), 1. - smoothstep(0., sf, pnts[i]));
        col = mix(col, mix(lCol, vec3(1), .65), 1. - smoothstep(0., sf, pnts[i] + .02));
        col = mix(col, vec3(0), 1. - smoothstep(0., sf, abs(pnts[i] + .035) - .005));

    }
    #endif


    // Return the pattern color.
    return col;

}



void mainImage(out vec4 fragColor, in vec2 fragCoord){

    // Aspect correct screen coordinates. Setting a minumum resolution on the
    // fullscreen setting in an attempt to keep things relatively crisp.
    float res = iResolution.y;//min(iResolution.y, 700.);
	vec2 uv = (fragCoord - iResolution.xy*.5)/res;

    // Rotation and translation of the scene.
    vec2 mv; //motion vector

    if(iScroll==0.)mv=vec2(0., 0.)*iTime;
    if(iScroll==1.)mv=vec2(-1., 0.)*iTime;
    if(iScroll==2.)mv=vec2(0., -1.)*iTime;
    if(iScroll==3.)mv=vec2(-1., -1.)*iTime;
    if(iScroll==4.)mv=vec2(1., -1.)*iTime;

    vec2 p=rot2(3.14159/iRotation)*uv*iScale  + mv;

    //vec2 p = rot2(3.14159/12.)*uv + vec2(1, .5)*iTime/10.;

    // The double simplex Wang weave pattern.
    vec3 col = pattern(p);

    // A bit of subtle noise-based color to break up the brown a little.
    col = mix(col, col.xzy, (n2D(p*4.)*.67 + n2D(p*8.)*.33)*.5);

    // Subtle vignette.
    uv = fragCoord/iResolution.xy;
    col *= pow(16.*uv.x*uv.y*(1. - uv.x)*(1. - uv.y) , .0625);
    // Colored variation.
    //col = mix(col.xzyw, col, pow(16.*uv.x*uv.y*(1. - uv.x)*(1. - uv.y) , .0625));


    // Rough gamma correction, then output to the screen.
    fragColor = vec4(sqrt(max(col, 0.)), iAlpha);
}

void main(){
    //ZGE does not use mainImage( out vec4 fragColor, in vec2 fragCoord )
    //Rededefined fragCoord as gl_FragCoord.xy-iViewport(dynamic window)
    mainImage(gl_FragColor,gl_FragCoord.xy-iViewport);
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="iResolution" VariableRef="uResolution"/>
        <ShaderVariable VariableName="iViewport" VariableRef="uViewport"/>
        <ShaderVariable VariableName="iTime" VariableRef="uTime"/>
        <ShaderVariable VariableName="iAlpha" VariableRef="uAlpha"/>
        <ShaderVariable VariableName="iColor" VariableRef="uColor"/>
        <ShaderVariable VariableName="iScroll" ValuePropRef="round(Parameters[5]*1000)"/>
        <ShaderVariable VariableName="iScale" ValuePropRef="clamp(parameters[6]*2,.1,16);"/>
        <ShaderVariable VariableName="iRotation" ValuePropRef="(Parameters[7])*.5+.5"/>
      </UniformVariables>
    </Shader>
    <Group Comment="Default ShaderToy Uniform Variable Inputs">
      <Children>
        <Variable Name="uResolution" Type="6"/>
        <Variable Name="uTime"/>
        <Variable Name="uViewport" Type="6"/>
      </Children>
    </Group>
    <Group Comment="FL Studio Variables">
      <Children>
        <Array Name="Parameters" SizeDim1="8" Persistent="255">
          <Values>
<![CDATA[789C63608080B3677CECCE9E39633B6BA6A4FDB2D92ED640217B9038008E660850]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Alpha
Hue
Saturation
Lightness
Speed
Scroll @list1000: None,Horizontal,Vertical,Diagnol,"Diagnol N"
Scale
Rotation]]>
          </StringValue>
        </Constant>
        <Constant Name="AuthorInfo" Type="2">
          <StringValue>
<![CDATA[Shane (converted by Youlean & StevenM)
https://www.shadertoy.com/view/tl2GWz
]]>
          </StringValue>
        </Constant>
      </Children>
    </Group>
    <Group Comment="Unique Uniform Variable Inputs">
      <Children>
        <Variable Name="uAlpha"/>
        <Variable Name="uColor" Type="7"/>
      </Children>
    </Group>
    <Group Comment="Materials and Textures">
      <Children>
        <Material Name="mCanvas" Blend="1" Shader="MainShader"/>
      </Children>
    </Group>
  </Content>
</ZApplication>
