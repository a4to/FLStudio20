<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" FileVersion="2">
  <OnLoaded>
    <ZLibrary>
      <Source>
<![CDATA[inline float angle(float X)
{
  if(X >= 0 && X < 360)return X;
  if(X > 360)return X-floor(X/360)* 360;
  if(X <   0)return X+floor(X/360)*-360;
}

vec3 hsv(float H, float S, float V)
{
  vec3 Color;
  float R,G,B,I,F,P,Q,T;

  H = angle(H);
  S = clamp(S,0,100);
  V = clamp(V,0,100);

  H /= 60;
  S /= 100;
  V /= 100;

  if(S == 0)
  {
    Color[0] = V;
    Color[1] = V;
    Color[2] = V;
  }
  else
  {
    I = floor(H);
    F = H-I;

    P = V*(1-S);
    Q = V*(1-S*F);
    T = V*(1-S*(1-F));

    if(I == 0){R = V; G = T; B = P;}
    if(I == 1){R = Q; G = V; B = P;}
    if(I == 2){R = P; G = V; B = T;}
    if(I == 3){R = P; G = Q; B = V;}
    if(I == 4){R = T; G = P; B = V;}
    if(I == 5){R = V; G = P; B = Q;}

    Color[0] = R;
    Color[1] = G;
    Color[2] = B;
  }
  return Color;
}]]>
      </Source>
    </ZLibrary>
  </OnLoaded>
  <OnUpdate>
    <ZExpression>
      <Expression>
<![CDATA[uResolution=vector2(app.ViewportWidth,app.ViewportHeight);
uViewport=vector2(app.ViewportX,app.ViewportY);
uAlpha =1.0-Parameters[0];

float speed=(Parameters[4]-0.5)*4.0;
float delta=app.DeltaTime*Speed; //comment to use other time options
uTime+=delta;


uColorHSV = vector3(Parameters[1]*360,Parameters[2],1-Parameters[3]);]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <OnRender>
    <UseMaterial Material="mCanvas"/>
    <RenderSprite/>
  </OnRender>
  <Content>
    <Shader Name="MainShader">
      <VertexShaderSource>
<![CDATA[#version 120

void main(){
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[#version 120

uniform vec2 iResolution,iViewport;
uniform float iTime;
uniform float iAlpha;
uniform vec3 iColorHSV;
uniform float iScale,iScroll,iRotation;

float Truncate(float val) { return clamp(val,0.0,1.0); }

vec3 TransformHSV(vec3 c, float H, float S, float V) {
  float M_PI = 3.1415926;
	float VSU = V * S * cos(H * M_PI / 180.0);
	float VSW = V * S * sin(H * M_PI / 180.0);
  vec3 ret;
	ret.r = Truncate((0.299 * V + 0.701 * VSU + 0.168 * VSW) * c.r
		+ (0.587 * V - 0.587*VSU + 0.330 * VSW) * c.g
		+ (0.114 * V - 0.114*VSU - 0.497*VSW) * c.b);
	ret.g = Truncate((0.299 * V - .299 * VSU - 0.328 * VSW) * c.r
		+ (0.587 * V + 0.413 * VSU + 0.035 * VSW) * c.g
		+ (0.114 * V - 0.114 * VSU + 0.292 * VSW) * c.b);
	ret.b = Truncate((0.299 * V - 0.3 * VSU + 1.25 * VSW) * c.r
		+ (0.587 * V - 0.588 * VSU - 1.05 * VSW) * c.g
		+ (0.114 * V + 0.886 * VSU - 0.203 * VSW) * c.b);
	return ret;
} // col = TransformHSV(clamp(col, 0., 1.), iColorHSV[0], iColorHSV[1], iColorHSV[2]);


/*

	3 Tap 2nd Order Voronoi
	-----------------------

	Not really an entry per se, but it wouldn't be Shadertoy without a cliche Voronoi example, so I
	coded one up. :)

	This particular one is a bump mapped, hashless, 3-Tap 2nd order cellular pattern. OK, it isn't
	Voronoi in the strictest sense, but it's produced with a custom algorithm designed to give the
	same feel.

	The standard Voronoi algorithm requires a little too much information. Loops, cell positions,
	distance checks, and some kind of workable two dimensional hash function. Someone could probably
	fit it into two tweets, but I don't imagine there'd be much room left for anything else.

	This pattern was produced using a different method, which involves plotting repeat points on warped,
	rotational layers, then finding the desired first and second order distances. The resultant pattern
	isn't perfect - that's for sure, but the algorithm takes up considerably less space, which leaves
	room for some very cheap bump mapping, coloring, etc.

	I'd originally raymarched this, but there wasn't enough room left to pretty it up, so instead, I
	opted to perform some very cheap two-sample bump mapping and coloring. By the way, if someone could
	remove some characters and apply some environment mapping, that'd be great. :D

*/


// Declaring some floats globally for global access - Basically, the opposite of what you're supposed
// to with shader code. It wouldn't make Ollj happy. :D

// I tried to give them intuitive one letter names. In order: Closest distance, second closest distance,
// current distance, storage for the function value (y-x), and an out-of-place looking global loop counter...
// Bad programming practice at its finest. :)
float x, y, d, f, i;

// Self contained, hashless, 3 Tap, 2nd Order Voronoi function. Not written for speed due to character
// restrictions, but at 3 taps, it's not going to be slow anyway.
void v(vec2 p){

    // The trick here is to plot a wrapped cell point, rotate space, plot another, repeat the process for
    // the final point, then determine the closest and second closest with some max\min trickery. Simple,
    // but quite effective. I've left a few details out, but that's the general idea.
    x = y = i = 2.;
    while(i++<5.) d = length(fract(p *= mat2(7, -5, 5, 7)*.1) - .5)/.6, y = max(x, min(y, d)), x = min(x, d);

}

// Standard 2D rotation formula.
mat2 rot2(in float a){ float c = cos(a), s = sin(a); return mat2(c, -s, s, c); }

void mainImage(out vec4 o, vec2 u){

    // Moving screen coordinates.

    u = u/iResolution.y*5.;

    vec2 mv;

    if(iScroll==0.)mv=vec2(0., 0.)*iTime;
    if(iScroll==1.)mv=vec2(-1., 0.)*iTime;
    if(iScroll==2.)mv=vec2(0., -1.)*iTime;
    if(iScroll==3.)mv=vec2(-1., -1.)*iTime;
    if(iScroll==4.)mv=vec2(1., -1.)*iTime;

    u=rot2(3.14159/iRotation)*u*iScale + mv;


    // Call the function. "v" for Voronoi.
    v(u);
    // Storing the "Y - X" distance (similar to the cool beveled looking one, Y*Y - X*X). To make it
    // slighty more interesting, I've added an additional sinusoidal term to give the illusion that the
    // pattern is lit via some kind of grated mechanism which causes a subtle interference pattern. I
    // used the second order distance only for that - to match the cell structure.
    f = y - x - sin(y*80.)/80.;

    // Take a second nearby sample.
    v(u-.05);

    // Calculating the sample difference, which in effect provides a gradient bump value. It's a cheap way
    // to bump things and is based, in part, on directional derivative lighting. IQ wrote an article about
    // it, which should be easy enough to find. I've stored the value in "i" to save characters.
    i = max(y - x - f, 0.);

    // Apply a colored gradient (ramped up for a bit of shine) to the Voronoi (with interfernce) value.
    o = f + vec4(10, 6, 2, 1)*i*i*i*20. + .03; // Very subtle global ambience on the end.

    // Backing off the green with a duller gradient to give a sunrise hue, or the lamest SSS effect ever. :D
    o.y -= i;

    o.rgb = TransformHSV(clamp(o.rgb, 0., 1.), iColorHSV[0], iColorHSV[1], iColorHSV[2]);
    o.a = iAlpha;
}

/*
// Red and blue highlights, but I couldn't find a way to make it fit. :)
void mainImage(out vec4 o, vec2 u){

    // Moving screen coordinates.
    //u = u/iResolution.y*5. + iTime;

    // Call the function. "v" for Voronoi.
    //v(u*iTest*2000.0);
    // Storing the "Y - X" distance (similar to the cool beveled looking one, Y*Y - X*X). To make it
    // slighty more interesting, I've added an additional sinusoidal term to give the illusion that the
    // pattern is lit via some kind of grated mechanism which causes a subtle interference pattern. I
    // used the second order distance only for that - to match the cell structure.
    //f = y - x + sin(y*80.)/1e2;

    // Take a second nearby sample.
    //v(u-.05);

    // Calculating the sample difference, which in effect provides a gradient bump value. It's a cheap way
    // to bump things and is based, in part, on directional derivative lighting. IQ wrote an article about
    // it, which should be easy enough to find. I've stored the value in "i" to save characters.
    //i = max(y - x - f, 0.);
    //x = max(f - y + x, 0.);

    //o = vec4(9, 6, 2, 1);
    // Apply a colored gradient (ramped up for a bit of shine) to the Voronoi (with interfernce) value.
    //o = f + (o*i*i*i + o.zyxw*x*x*x)*20. + .03; // Very subtle global ambience on the end.

    // Backing off the green with a duller gradient to give a sunrise hue, or the lamest SSS effect ever. :D
    //o.y -= i;

}
*/

void main(){
    //ZGE does not use mainImage( out vec4 fragColor, in vec2 fragCoord )
    //Rededefined fragCoord as gl_FragCoord.xy-iViewport(dynamic window)
    mainImage(gl_FragColor,gl_FragCoord.xy-iViewport);
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="iResolution" VariableRef="uResolution"/>
        <ShaderVariable VariableName="iViewport" VariableRef="uViewport"/>
        <ShaderVariable VariableName="iTime" VariableRef="uTime"/>
        <ShaderVariable VariableName="iAlpha" VariableRef="uAlpha"/>
        <ShaderVariable VariableName="iColorHSV" VariableRef="uColorHSV"/>
        <ShaderVariable VariableName="iScroll" ValuePropRef="round(Parameters[5]*1000)"/>
        <ShaderVariable VariableName="iScale" ValuePropRef="clamp(parameters[6]*2,.1,16);"/>
        <ShaderVariable VariableName="iRotation" ValuePropRef="(Parameters[7])*1+.01"/>
      </UniformVariables>
    </Shader>
    <Group Comment="Default ShaderToy Uniform Variable Inputs">
      <Children>
        <Variable Name="uResolution" Type="6"/>
        <Variable Name="uTime"/>
        <Variable Name="uViewport" Type="6"/>
      </Children>
    </Group>
    <Group Comment="FL Studio Variables">
      <Children>
        <Array Name="Parameters" SizeDim1="8" Persistent="255">
          <Values>
<![CDATA[789C63608081067B10396BA6A4FDB2D92ED60C0C0E603E003D5A048A]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Alpha
Hue
Saturation
Lightness
Speed
Scroll @list1000: None,Horizontal,Vertical,Diagnal,"Diagnal N"
Scale
iRotation
]]>
          </StringValue>
        </Constant>
        <Constant Name="AuthorInfo" Type="2">
          <StringValue>
<![CDATA[Shane (converted by Youlean & StevenM)
https://www.shadertoy.com/view/MsSfWh
]]>
          </StringValue>
        </Constant>
      </Children>
    </Group>
    <Group Comment="Unique Uniform Variable Inputs">
      <Children>
        <Variable Name="uAlpha"/>
        <Variable Name="uColorHSV" Type="7"/>
      </Children>
    </Group>
    <Group Comment="Materials and Textures">
      <Children>
        <Material Name="mCanvas" Blend="1" Shader="MainShader"/>
      </Children>
    </Group>
  </Content>
</ZApplication>
