FLhd   0 * ` FLdt�  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ի('  ﻿[General]
GlWindowMode=1
LayerCount=11
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,7,1,5,6,4,2,8,3,9,10

[AppSet0]
App=Canvas effects\Stack Trace
FParamValues=0.54,0.152,1,0.548,0.218,0.224,0.6,0
ParamValues=540,152,1000,548,218,224,600,0
ParamValuesCanvas effects\Flaring=0,1000,1000,1000,1000,500,500,381,0,200,500
ParamValuesCanvas effects\Digital Brain=0,0,0,0,336,500,500,224,0,640,60,310,408,520,1000
ParamValuesCanvas effects\Electric=0,1000,112,956,540,500,500,152,100,1000,500
ParamValuesCanvas effects\Lava=0,148,924,872,780,500,500,520,676,752,500
ParamValuesCanvas effects\Flow Noise=0,880,952,788,0,500,500,184,100,1000,500
ParamValuesCanvas effects\OverlySatisfying=672,740,484,636,600,500,500,100,100,1000,500
Enabled=1
Collapsed=1
Name=EFX 1

[AppSet7]
App=Feedback\WormHoleEclipse
FParamValues=0,0,0,1,0.12,1,0.5,0.5,0.5,0.5,0.5
ParamValues=0,0,0,1000,120,1000,500,500,500,500,500
ParamValuesFeedback\WarpBack=500,0,0,568,500,500,28
ParamValuesHUD\HUD Prefab=6,0,500,0,1000,500,500,734,1000,1000,444,0,500,1000,820,0,1000,1000
Enabled=1
Collapsed=1
Name=EFX 2

[AppSet1]
App=Postprocess\Edge Detect
FParamValues=0,0,0,0.664,0.384,0.572
ParamValues=0,0,0,664,384,572
ParamValuesBackground\FourCornerGradient=400,1000,752,1000,1000,728,1000,1000,48,1000,1000,568,1000,1000
ParamValuesPostprocess\Ascii=0,136,176,600,700,200
Enabled=1
Collapsed=1
Name=DARKHOLE

[AppSet5]
App=HUD\HUD Grid
AppVersion=1
FParamValues=0.352,0.5,0,1,0.5,0.5,1,1,0.844,4,0.5,1,0.5,0.82,1,1,0.648,0,0.888,0,1,1
ParamValues=352,500,0,1000,500,500,1000,1000,844,4,500,1000,500,820,1000,1000,648,0,888,0,1000,1
ParamValuesHUD\HUD Graph Radial=0,500,0,1000,500,500,202,444,500,0,1000,0,1000,0,250,728,200,0,0,0,500,1000
ParamValuesFeedback\BoxedIn=0,0,0,1000,1000,0,312,500,0,1000
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,900,708,336,28,784,500,500
ParamValuesHUD\HUD Callout Line=0,500,0,1000,500,500,700,400,0,200,0,200,100,450,100,1000
Enabled=1
Collapsed=1
Name=EFX 3

[AppSet6]
App=HUD\HUD Prefab
FParamValues=26,0.504,0.5,0,1,0.5,0.5,0.248,1,1,4,0,0.5,1,0.912,0,1,1
ParamValues=26,504,500,0,1000,500,500,248,1000,1000,4,0,500,1,912,0,1000,1
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,1000,708,336,28,784,500,500
ParamValuesHUD\HUD Grid=0,500,0,1000,500,500,1000,1000,1000,444,500,1000,500,100,0,500,1000,516,1000,300,0,1000
Enabled=1
Collapsed=1
Name=Modificator Center
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C2CF53273CA18863D000010090AA4

[AppSet4]
App=Feedback\FeedMe
FParamValues=0,0,0,1,0.212,1,0
ParamValues=0,0,0,1000,212,1000,0
ParamValuesTerrain\CubesAndSpheres=0,0,0,1000,0,500,500,650,540,400,1000,1000,1000,1000,1000,1000,0,1000
ParamValuesPostprocess\Youlean Motion Blur=484,1000,732,764
ParamValuesFeedback\BoxedIn=0,0,0,552,1000,0,260,500,0,820
ParamValuesPostprocess\Youlean Color Correction=308,500,512,500,836,904
Enabled=1
Collapsed=1
Name=Moon EFX

[AppSet2]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0,0.488,0.5
ParamValues=500,500,500,0,488,500
Enabled=1
Collapsed=1
Name=Color Correction

[AppSet8]
App=Feedback\70sKaleido
FParamValues=0,0,0,1,0.752,0.56
ParamValues=0,0,0,1000,752,560
ParamValuesHUD\HUD Prefab=375,0,500,0,980,500,500,392,1000,1000,444,0,500,1000,920,0,940,1000
ParamValuesPostprocess\RGB Shift=560,0,0,600,700,200
ParamValuesPostprocess\Point Cloud High=0,730,594,500,568,448,444,503,226,549,156,0,0,0,0
ParamValuesPostprocess\ScanLines=0,200,0,0,0,0
ParamValuesPostprocess\Youlean Blur=500,1000,336
ParamValuesPostprocess\Youlean Motion Blur=656,1000,0,0
ParamValuesBackground\FourCornerGradient=9,688,680,1000,1000,250,1000,1000,500,1000,1000,750,1000,1000
ParamValuesFeedback\FeedMe=0,0,0,1000,212,1000,0
Enabled=1
Collapsed=1
Name=To In EFX

[AppSet3]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.588,0,0.594,0.302,1,0,0,0
ParamValues=588,0,594,302,1000,0,0,0
Enabled=1
Collapsed=1
Name=To Out

[AppSet9]
App=Text\TextTrueType
FParamValues=0.12,0,0,0,0,0.5,0.5,0,0,0,0.5
ParamValues=120,0,0,0,0,500,500,0,0,0,500
Enabled=1
Collapsed=1
Name=Main Text

[AppSet10]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fade in-out

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text=Author,"Song Title"
Html="<position y=""42.7""><p align=""center""><font face=""American-Captain"" size=""5.5"" color=""#000000"">[author]</font></p></position>","<position y=""47""><p align=""center""><font face=""Chosence-Bold"" size=""3"" color=""#000000"">[title]</font></p></position>","<position y=""55""><p align=""center""><font face=""Chosence-Bold"" size=""3"" color=""#000000"">[comment]</font></p></position>",,," ",,,
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

