FLhd   0  0 FLdt  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   Փ3�  ﻿[General]
GlWindowMode=1
LayerCount=32
FPS=2
MidiPort=-1
Aspect=1
LayerOrder=15,19,16,23,1,24,22,3,8,7,6,5,2,0,17,18,9,10,11,12,13,25,14,21,20,4,31,28,30,29,26,27
WizardParams=129,273,321,369,417

[AppSet15]
App=HUD\HUD Grid
ParamValues=0,552,1000,0,500,500,1000,1000,500,4,500,1000,500,784,0,1000,276,100,772,0,0,1
Enabled=1
Collapsed=1

[AppSet19]
App=Postprocess\Youlean Color Correction
ParamValues=500,500,912,628,580,500
Enabled=1
Collapsed=1

[AppSet16]
App=Postprocess\Vignette
ParamValues=0,0,0,600,279,12
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet23]
App=Canvas effects\TaffyPulls
ParamValues=500,0,0,0,0,1000,500,0,500,500,0,0,0,1000
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet1]
App=Image effects\Image
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
ParamValuesBackground\SolidColor=0,0,0,280
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet24]
App=Image effects\Image
ParamValues=656,0,0,0,1000,720,500,1000,1000,516,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=3

[AppSet22]
App=Postprocess\AudioShake
ParamValues=48,0,1,500,100,1000
Enabled=1
Collapsed=1

[AppSet3]
App=Image effects\Image
ParamValues=0,0,508,472,1000,0,500,0,0,500,0,0,0,0
Enabled=1
Collapsed=1

[AppSet8]
App=Image effects\Image
ParamValues=0,122,508,472,1000,0,500,0,0,500,0,0,0,0
Enabled=1
Collapsed=1

[AppSet7]
App=Image effects\Image
ParamValues=0,230,508,472,1000,0,500,0,0,500,0,0,0,0
Enabled=1
Collapsed=1

[AppSet6]
App=Image effects\Image
ParamValues=0,393,508,472,1000,0,500,0,0,500,0,0,0,0
Enabled=1
Collapsed=1

[AppSet5]
App=Image effects\Image
ParamValues=0,475,508,472,1000,0,500,0,0,500,0,0,0,0
Enabled=1
Collapsed=1

[AppSet2]
App=Image effects\Image
ParamValues=0,531,508,472,1000,0,500,0,0,500,0,0,0,0
Enabled=1
Collapsed=1

[AppSet0]
App=Image effects\Image
ParamValues=0,552,580,560,1000,545,500,0,0,500,0,0,0,0
ParamValuesHUD\HUD Image=0,0,500,500,1000,1000,500,444,500,0,0,1000,1000,0,1000
Enabled=1
Collapsed=1

[AppSet17]
App=Image effects\Image
ParamValues=686,0,0,0,936,719,371,0,908,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=2

[AppSet18]
App=Image effects\Image
ParamValues=0,0,0,0,936,701,585,0,908,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=2

[AppSet9]
App=Postprocess\ParameterShake
ParamValues=131,435,3,5,85,447,8,5,1,1,0,0,3,3,0,0,0,0,0,0,0,0,1,1,0,0
ParamValuesMisc\Automator=1000,40,182,0,158,100,223,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0
Enabled=1
Collapsed=1

[AppSet10]
App=Postprocess\ParameterShake
ParamValues=59,451,7,5,41,451,6,5,1,1,0,0,3,3,0,0,0,0,0,0,0,0,1,1,0,0
Enabled=1
Collapsed=1

[AppSet11]
App=Postprocess\ParameterShake
ParamValues=31,450,5,5,14,453,2,5,1,1,0,0,3,3,0,0,0,0,0,0,0,0,1,1,0,0
Enabled=1
Collapsed=1

[AppSet12]
App=HUD\HUD Prefab
ParamValues=190,0,500,0,1000,896,382,226,1000,1000,4,0,500,1,408,0,1000,1
Enabled=0
Collapsed=1
LayerPrivateData=78DA2B48CC4BCD298E290051BA0606267A9939650C23080000EAE0072A

[AppSet13]
App=HUD\HUD Prefab
ParamValues=185,0,500,0,1000,868,863,197,1000,1000,4,0,500,1,444,0,1000,1
Enabled=0
Collapsed=1
LayerPrivateData=78DACBC9CC4B2D8E294E2D482C4A2CC92F4262EA1A185BEA65E694310C6B00005E1C0D2B

[AppSet25]
App=HUD\HUD Prefab
ParamValues=185,0,500,0,1000,873,720,183,1000,1000,4,0,500,1,444,0,1000,1
Enabled=0
Collapsed=1
LayerPrivateData=78DACBC9CC4B2D8E294E2D482C4A2CC92F4262EA1A185BEA65E694310C6B00005E1C0D2B

[AppSet14]
App=HUD\HUD Prefab
ParamValues=182,0,500,0,1000,880,623,143,1000,1000,4,0,500,1,485,0,1000,1
Enabled=0
Collapsed=1
LayerPrivateData=78DACBC9CC4B2D8E294E2D482C4A2CC92F4262EA1A189BE965E694310C6B00005B730D28

[AppSet21]
App=HUD\HUD Prefab
ParamValues=182,0,500,0,1000,903,157,140,782,1000,4,0,500,1,501,0,1000,1
Enabled=0
Collapsed=1
LayerPrivateData=78DACBC9CC4B2D8E294E2D482C4A2CC92F4262EA1A189BE965E694310C6B00005B730D28

[AppSet20]
App=Postprocess\FrameBlur
ParamValues=60,172,0,0,558
Enabled=1
Collapsed=1

[AppSet4]
App=Text\TextTrueType
ParamValues=820,0,0,1000,0,491,498,0,0,0,500
Enabled=1

[AppSet31]
App=Text\TextTrueType
ParamValues=0,0,0,0,0,491,500,0,0,0,500
Enabled=1

[AppSet28]
App=Peak Effects\Linear
ParamValues=0,145,648,0,290,748,576,388,250,500,260,350,532,0,1,0,0,500,500,500,500,828,500,152,200,0,32,212,330,250,100
Enabled=1
Collapsed=1
ImageIndex=4

[AppSet30]
App=Peak Effects\Linear
ParamValues=0,254,648,0,290,768,576,388,250,500,260,350,532,0,1,0,0,500,500,500,500,828,500,152,200,0,32,212,330,250,100
Enabled=1
Collapsed=1
ImageIndex=4

[AppSet29]
App=Peak Effects\Linear
ParamValues=0,405,648,0,290,790,576,388,250,500,260,350,532,0,1,0,0,500,500,500,500,828,500,152,200,0,32,212,330,250,100
Enabled=1
Collapsed=1
ImageIndex=4

[AppSet26]
App=Peak Effects\Linear
ParamValues=0,488,648,0,290,809,576,388,250,500,260,350,532,0,1,0,0,500,500,500,500,828,500,152,200,0,32,212,330,250,100
Enabled=1
Collapsed=1
ImageIndex=4

[AppSet27]
App=Peak Effects\Linear
ParamValues=0,528,648,0,290,826,576,388,250,500,260,350,532,0,1,0,0,500,500,500,500,828,500,152,200,0,32,212,330,250,100
Enabled=1
Collapsed=1
ImageIndex=4

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0

[UserContent]
Text="This is the default text."
Html="<position x=""5""><position y=""4""><p align=""left""><font face=""American-Captain"" size=""12"" color=""#000"">[author]</font></p></position>","<position x=""5""><position y=""14""><p align=""left""><font face=""Chosence-Bold"" size=""6"" color=""#000"">[title]</font></p></position>","<position x=""5""><position y=""36""><p align=""left""><font face=""Chosence-Bold"" size=""3.5"" color=""#000"">[extra1]</font></p></position>","<position x=""5""><position y=""90""><p align=""left""><font face=""Chosence-Bold"" size=""5"" color=""#444444"">[comment]</font></p></position>",,"<position y=""80""><position x=""0""><p align=""right""><font face=""Chosence-Bold"" size=""8"" color=""#000"">[extra2]</font></p></position>","<position y=""90""><position x=""0""><p align=""right""><font face=""Chosence-regular"" size=""6"" color=""#000"">[extra3]</font></p></position>",,," "
Images="[presetpath]Wizard\Assets\Sacco\Angled edges.svg",[plugpath]Content\Bitmaps\template1.jpg
VideoUseSync=0
EnableMipmap=1

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

