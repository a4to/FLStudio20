FLhd   0 * ` FLdt�  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��:L  ﻿[General]
GlWindowMode=1
LayerCount=19
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=8,6,2,7,1,3,5,4,15,16,14,17,0,9,10,11,12,13,18
WizardParams=659,853,854,855,856,857

[AppSet8]
App=Peak Effects\Linear
FParamValues=0,0,1,0,0.646,0.5,0.468,0.052,0,0.512,0,0.084,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,0.62,0.18,0.224,0.992,0.348,0.54,0.244,0.336,0.2
ParamValues=0,0,1000,0,646,500,468,52,0,512,0,84,500,1,0,1,0,500,500,500,500,0,620,180,224,992,348,540,244,336,200
Enabled=1
Collapsed=1
ImageIndex=2
Name=EQ LINE 1

[AppSet6]
App=Peak Effects\Linear
FParamValues=0.508,1,1,0.652,0.646,0.5,0.468,0.108,0.5,0.512,0,0.084,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,0.564,0.16,0.16,1,0.164,0.54,0.244,0.336,0.2
ParamValues=508,1000,1000,652,646,500,468,108,500,512,0,84,500,1,0,1,0,500,500,500,500,0,564,160,160,1000,164,540,244,336,200
Enabled=1
Collapsed=1
ImageIndex=2
Name=EQ LINE 2

[AppSet2]
App=Peak Effects\Linear
FParamValues=0.848,0.496,0.612,0,0.646,0.5,0.468,0.136,0,0.512,0,0.084,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,0.604,0.18,0.224,1,0.304,0.54,0.244,0.336,0.2
ParamValues=848,496,612,0,646,500,468,136,0,512,0,84,500,1,0,1,0,500,500,500,500,0,604,180,224,1000,304,540,244,336,200
Enabled=1
Collapsed=1
ImageIndex=2
Name=EQ LINE 3

[AppSet7]
App=Peak Effects\Linear
FParamValues=0.66,0.688,1,0,0.646,0.5,0.468,0.056,0,0.512,0,0.076,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,0.564,0.16,0.16,1,0.304,0.54,0.244,0.336,0.2
ParamValues=660,688,1000,0,646,500,468,56,0,512,0,76,500,1,0,1,0,500,500,500,500,0,564,160,160,1000,304,540,244,336,200
Enabled=1
Collapsed=1
ImageIndex=2
Name=EQ LINE 4

[AppSet1]
App=Peak Effects\Linear
FParamValues=0.476,0.356,0.623,0.193,0.646,0.5,0.468,0.128,0.5,0.512,0,0.048,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,1,0.18,0.224,0.472,0.304,0.54,0.244,0.336,0.2
ParamValues=476,356,623,193,646,500,468,128,500,512,0,48,500,1,0,1,0,500,500,500,500,0,1000,180,224,472,304,540,244,336,200
ParamValuesPostprocess\AudioShake=0,0,0,500,100,900
Enabled=1
Collapsed=1
ImageIndex=2
Name=EQ LINE 5

[AppSet3]
App=Peak Effects\Linear
FParamValues=0.636,0.648,0.636,0,0.646,0.5,0.468,0,0.5,0.512,0,0.312,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,0.5,0.18,0.224,0.064,0,0.54,0.244,0.336,0.2
ParamValues=636,648,636,0,646,500,468,0,500,512,0,312,500,1,0,1,0,500,500,500,500,0,500,180,224,64,0,540,244,336,200
Enabled=1
Collapsed=1
ImageIndex=2
Name=EQ LINE 6

[AppSet5]
App=Peak Effects\Linear
FParamValues=0.656,0.408,0.56,0.452,0.646,0.5,0.468,0.056,0,0.512,0,0.168,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0.324,0.564,0,0.16,1,0.104,0.54,0.244,0.336,0.2
ParamValues=656,408,560,452,646,500,468,56,0,512,0,168,500,1,0,1,0,500,500,500,500,324,564,0,160,1000,104,540,244,336,200
Enabled=1
Collapsed=1
ImageIndex=2
Name=EQ LINE 7

[AppSet4]
App=Peak Effects\Linear
FParamValues=0.5,0.488,1,0,0.646,0.5,0.468,0.16,0.5,0.512,0,0.052,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,0.564,0.064,0.132,0.908,0.22,0.54,0.244,0.336,0.2
ParamValues=500,488,1000,0,646,500,468,160,500,512,0,52,500,1,0,1,0,500,500,500,500,0,564,64,132,908,220,540,244,336,200
Enabled=1
Collapsed=1
ImageIndex=2
Name=EQ LINE 8

[AppSet15]
App=Peak Effects\Linear
FParamValues=0.512,1,0,0.496,0.678,0.5,0.468,0.24,0,0.512,0,0.104,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,0.564,0.192,0,0.892,0,0.54,0.244,0.336,0.2
ParamValues=512,1000,0,496,678,500,468,240,0,512,0,104,500,1,0,1,0,500,500,500,500,0,564,192,0,892,0,540,244,336,200
Enabled=1
Collapsed=1
ImageIndex=2
Name=EQ LINE 9

[AppSet16]
App=Postprocess\Youlean Motion Blur
FParamValues=1,1,0.844,0.396,0,0,0
ParamValues=1000,1,844,396,0,0,0
ParamValuesPostprocess\Blooming=0,0,0,1000,0,1000,284,536,0
Enabled=1
Collapsed=1
ImageIndex=2
Name=EFX OUT

[AppSet14]
App=Postprocess\Blooming
FParamValues=0,0,0,1,0,1,0.284,0.536,0
ParamValues=0,0,0,1000,0,1000,284,536,0
ParamValuesPeak Effects\Linear=872,780,0,0,646,500,468,168,500,512,0,100,500,1000,0,1000,0,500,500,500,500,0,564,160,160,1000,304,540,244,336,200
ParamValuesFeedback\70sKaleido=0,0,0,1000,0,164
ParamValuesFeedback\FeedMe=0,0,0,640,0,636,412
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=EFX IN

[AppSet17]
App=HUD\HUD Prefab
FParamValues=0,0,0.5,0,1,0.477,0.802,0.414,1,1,4,0,0.5,1,0.368,0.105,1,1
ParamValues=0,0,500,0,1000,477,802,414,1000,1000,4,0,500,1,368,105,1000,1
Enabled=1
UseBufferOutput=1
Name=LOGO
LayerPrivateData=780173C8CBCF4BD52B2E4B671899000060F9036F

[AppSet0]
App=Background\SolidColor
FParamValues=0,0,0,0
ParamValues=0,0,0,0
ParamValuesCanvas effects\Digital Brain=0,0,0,0,1000,500,500,600,100,300,100,582,1000,484,1000,0
Enabled=1
Name=Siri BG

[AppSet9]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.496,0,0,0,1,1,0,0
ParamValues=0,0,0,0,1000,500,496,0,0,0,1,1,0,0
ParamValuesPeak Effects\Linear=940,492,1000,0,538,500,468,60,500,512,0,110,500,1000,0,1000,0,500,500,500,500,0,1000,180,224,992,348,540,244,336,200
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,0,0,0,500,500,500,500
ParamValuesPostprocess\Youlean Pixelate=588,667,0,0
ParamValuesPostprocess\Youlean Blur=104,0,508
ParamValuesPostprocess\Youlean Bloom=500,120,698,1000
ParamValuesPostprocess\Youlean Color Correction=392,504,472,500,600,540
ParamValuesFeedback\FeedMe=0,0,0,944,0,692,732
Enabled=1
Collapsed=1
Name=Siri EQ Out

[AppSet10]
App=Background\FourCornerGradient
FParamValues=7,0,0.016,0.732,0.484,0.634,0.704,0.656,0.02,0.312,0,0.69,0,0
ParamValues=7,0,16,732,484,634,704,656,20,312,0,690,0,0
Enabled=1
Collapsed=1
Name=Filter Color HUE

[AppSet11]
App=Text\TextTrueType
FParamValues=0,0,0,0,0,0.488,0.5,0,0,0,0.5
ParamValues=0,0,0,0,0,488,500,0,0,0,500
Enabled=1

[AppSet12]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
ParamValuesPostprocess\Vignette=0,0,360,600,700,664
ParamValuesPostprocess\Youlean Motion Blur=372,1000,96,300
ParamValuesPostprocess\Youlean Bloom=328,764,238,224
ParamValuesBackground\FourCornerGradient=467,0,620,1000,484,634,704,656,268,1000,1000,690,1000,1000
ParamValuesPostprocess\TransitionEffects=500,500,500,500,500,500,500,500,500,500,0,0,0,1000
Enabled=1
Collapsed=1
Name=Fade in-out

[AppSet13]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1

[AppSet18]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="<position y=""86""><p align=""center""><font face=""Chosence-Bold"" size=""2"" color=""#fff"">Image-Line Software</font></p></position>","<position y=""88  ""><p align=""center""><font face=""Chosence-regular"" size=""2"" color=""#CCCCC1"">Exclusive Sounds</font></p></position>"
Html="<position x=""8"" y=""5""><p><font face=""American-Captain"" size=""7"" color=""#333333"">[author]</font></p></position>","<position x=""8"" y=""10""><p><font face=""Chosence-Bold"" size=""6"" color=""#333333"">[title]</font></p></position>","<position x=""8"" y=""19""><p> <font face=""Chosence-Bold"" size=""4"" color=""#333333"">[comment]</font></p></position>"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

