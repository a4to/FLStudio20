FLhd   0 * ` FLdt  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��<u  ﻿[General]
GlWindowMode=1
LayerCount=23
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=12,10,16,0,9,11,13,6,7,1,4,5,3,18,17,19,22,23,20,21,24,2,25
WizardParams=1235

[AppSet12]
App=Misc\Automator
FParamValues=1,11,12,2,0.5,0.101,0.25,0,14,6,2,0.5,0.101,0.488,0,14,8,2,0.3,0.101,0.46,0,14,2,2,1,0.101,0.378
ParamValues=1,11,12,2,500,101,250,0,14,6,2,500,101,488,0,14,8,2,300,101,460,0,14,2,2,1000,101,378
ParamValuesF=1,11,12,2,0.5,0.101,0.25,0,14,6,2,0.5,0.101,0.488,0,14,8,2,0.3,0.101,0.46,0,14,2,2,1,0.101,0.378
Enabled=1

[AppSet10]
App=HUD\HUD Grid
AppVersion=1
FParamValues=0,0.5,1,1,0.5,0.5,1,1,1,4,0.5,0.8682,1,0.744,0,0.5,1,0.139,0.5,0,0,0
ParamValues=0,500,1000,1000,500,500,1000,1000,1000,4,500,868,1000,744,0,500,1000,139,500,0,0,0
ParamValuesF=0,0.5,1,1,0.5,0.5,1,1,1,4,0.5,0.3504,1,0.744,0,0.5,1,0.139,0.5,0,0,1
Enabled=1
UseBufferOutput=1

[AppSet16]
App=Postprocess\ParameterShake
FParamValues=1,0,15,16,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=1000,0,15,16,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=1,0,15,16,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
UseBufferOutput=1

[AppSet0]
App=Background\SolidColor
FParamValues=0,0,0,0.9
ParamValues=0,0,0,900
ParamValuesF=0,0,0,0.66
Enabled=1

[AppSet9]
App=HUD\HUD 3D
FParamValues=0,0,0.812,0.5,0.5,0.5,0.5,0.5,0.5,0.326,0.5,0.5,0.5,0.844,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=0,0,812,500,500,500,500,500,500,326,500,500,500,844,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
ParamValuesF=0,0,0.812,0.5,0.5,0.5,0.5,0.5,0.5,0.326,0.5,0.5,0.5,0.844,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
Enabled=1

[AppSet11]
App=HUD\HUD 3D
FParamValues=0,0,0.196,0.5,0.5,0.5,0.5,0.5,0.5,0.672,0.5,0.5,0.5,0.844,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=0,0,196,500,500,500,500,500,500,672,500,500,500,844,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
ParamValuesF=0,0,0.196,0.5,0.5,0.5,0.5,0.5,0.5,0.672,0.5,0.5,0.5,0.844,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
Enabled=1

[AppSet13]
App=HUD\HUD Mesh
FParamValues=0,0.764,0.896,0,0.363,0.5,0.5,0,0.05,0.15,0.5,0.5,0.5,0.5,0.5,0
ParamValues=0,764,896,0,363,500,500,0,50,150,500,500,500,500,500,0
ParamValuesF=0,0.764,0.896,0,0.363,0.5,0.5,0,0.05,0.15,0.5,0.5,0.5,0.5,0.5,1
Enabled=1
MeshIndex=3

[AppSet6]
App=HUD\HUD Mesh
FParamValues=0,0.5,0,1,0.662,0.5,0.443,2,0,0.15,0.5,0.5,1,0.5,0.548,0
ParamValues=0,500,0,1000,662,500,443,2,0,150,500,500,1000,500,548,0
ParamValuesF=0,0.5,0,1,0.662,0.5,0.443,2,0,0.15,0.5,0.5,1,0.5,0.548,1
Enabled=1
MeshIndex=1

[AppSet7]
App=HUD\HUD Mesh
FParamValues=0,0.5,0,1,0.703,0.5,0.521,2,0,0.15,0.5,0.5,1,0.5,0.443,0
ParamValues=0,500,0,1000,703,500,521,2,0,150,500,500,1000,500,443,0
ParamValuesF=0,0.5,0,1,0.703,0.5,0.521,2,0,0.15,0.5,0.5,1,0.5,0.443,1
Enabled=1
MeshIndex=2

[AppSet1]
App=HUD\HUD Mesh
FParamValues=0.016,0.5,0,1,0.366,0.5,0.456,1,0,0.15,0.5,0.5,0.5,0.5,0.604,0
ParamValues=16,500,0,1000,366,500,456,1,0,150,500,500,500,500,604,0
ParamValuesF=0.016,0.5,0,1,0.366,0.5,0.456,1,0,0.15,0.5,0.5,0.5,0.5,0.604,0
Enabled=1

[AppSet4]
App=HUD\HUD Mesh
FParamValues=0,0.5,1,0,0.376,0.5,0.456,2,0,0.15,0.5,0.5,0.5,0.5,0.604,0
ParamValues=0,500,1000,0,376,500,456,2,0,150,500,500,500,500,604,0
ParamValuesF=0,0.5,1,0,0.376,0.5,0.456,2,0,0.15,0.5,0.5,0.5,0.5,0.604,0
Enabled=1

[AppSet5]
App=HUD\HUD Prefab
FParamValues=211,0,0.832,1,0,0.5,0.599,0.432,0.568,1,4,0,0.25,1,0.476,0,1,0
ParamValues=211,0,832,1000,0,500,599,432,568,1000,4,0,250,1,476,0,1000,0
ParamValuesF=204,0,0.832,1,0,0.5,0.599,0.432,0.568,1,4,0,0.25,1,0.476,0,1,1
Enabled=1
LayerPrivateData=78012B48CC4BCD298E290051BA0686167A9939650C23080000EF96072F

[AppSet3]
App=HUD\HUD Prefab
FParamValues=211,0,0.5,0,1,0.5,0.591,0.432,0.568,1,4,0,0.25,1,0.476,0,1,0
ParamValues=211,0,500,0,1000,500,591,432,568,1000,4,0,250,1,476,0,1000,0
ParamValuesF=204,0,0.5,0,1,0.5,0.591,0.432,0.568,1,4,0,0.25,1,0.476,0,1,0
Enabled=1
LayerPrivateData=78012B48CC4BCD298E290051BA0686167A9939650C23080000EF96072F

[AppSet18]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.644,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,644,500,0,0,0,0,0,0,0
ParamValuesF=0,0,0,0,1,0.644,0.5,0,0,0,0,0,0,0
Enabled=1
ImageIndex=1

[AppSet17]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
ParamValuesF=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
Enabled=1
ImageIndex=1

[AppSet19]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.633,0.492,0,0,0.75,0,0,0,0
ParamValues=0,0,0,0,1000,633,492,0,0,750,0,0,0,0
ParamValuesF=0,0,0,0,1,0.633,0.492,0,0,0.75,0,0,0,0
Enabled=1
ImageIndex=1

[AppSet22]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.625,0.5,0,0,0.75,0,0,0,0
ParamValues=0,0,0,0,1000,625,500,0,0,750,0,0,0,0
ParamValuesF=0,0,0,0,1,0.625,0.5,0,0,0.75,0,0,0,0
Enabled=1
ImageIndex=1

[AppSet23]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.606,0.507,0,0,0.75,0,0,0,0
ParamValues=0,0,0,0,1000,606,507,0,0,750,0,0,0,0
ParamValuesF=0,0,0,0,1,0.606,0.507,0,0,0.75,0,0,0,0
Enabled=1
ImageIndex=1

[AppSet20]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.365,0.348,0,0,0.25,0,0,0,0
ParamValues=0,0,0,0,1000,365,348,0,0,250,0,0,0,0
ParamValuesF=0,0,0,0,1,0.365,0.348,0,0,0.25,0,0,0,0
Enabled=1
ImageIndex=1

[AppSet21]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.372,0.356,0,0,0.25,0,0,0,0
ParamValues=0,0,0,0,1000,372,356,0,0,250,0,0,0,0
ParamValuesF=0,0,0,0,1,0.372,0.356,0,0,0.25,0,0,0,0
Enabled=1
ImageIndex=1

[AppSet24]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.393,0.363,0,0,0.25,0,0,0,0
ParamValues=0,0,0,0,1000,393,363,0,0,250,0,0,0,0
ParamValuesF=0,0,0,0,1,0.393,0.363,0,0,0.25,0,0,0,0
Enabled=1
ImageIndex=1

[AppSet2]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.5,0,0.086,0.45,1,0,0,0
ParamValues=500,0,86,450,1000,0,0,0
ParamValuesF=0.5,0,0.294,0.45,1,0,0,0
Enabled=1

[AppSet25]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
ParamValuesF=0.5,0.5,0.5,0.5,0.5,0.5
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""4""><position y=""5""><p align=""left""><font face=""American-Captain"" size=""13"" color=""#5e5e5e"">[author]</font></p></position>","<position x=""4""><position y=""28""><p align=""left""><font face=""Chosence-Bold"" size=""10"" color=""#5e5e5e"">[title]</font></p></position>","<position x=""0""><position y=""5""><p align=""right""><font face=""Chosence-Bold"" size=""4"" color=""#5e5e5e"">[extra1]</font></p></position>","<position x=""59""><position y=""91""><p align=""right""><font face=""Chosence-Bold"" size=""4"" color=""#5e5e5e"">[comment]</font></p></position>",,"<position x=""81""><position y=""74""><p align=""center""><font face=""Chosence-Bold"" size=""4"" color=""#5e5e5e"">[extra2]</font></p></position>","<position x=""81""><position y=""79""><p align=""center""><font face=""Chosence-regular"" size=""4"" color=""#5e5e5e"">[extra3]</font></p></position>"
Meshes=[plugpath]Content\Meshes\Heroine.zgeMesh,"[presetpath]Sacco\wizard presets copy\Assets\Sacco\Buildings solid sm 02.zgeMesh","[presetpath]Sacco\wizard presets copy\Assets\Sacco\Buildings tri solid sm 1.zgeMesh",[plugpath]Content\Meshes\Sphere.zgeMesh
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

