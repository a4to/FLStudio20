FLhd   0 * ` FLdt�0  �20.6.9.1657 �y  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��_�/  ﻿[General]
GlWindowMode=1
LayerCount=25
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=3,1,2,4,9,12,15,29,16,32,10,24,23,25,22,28,11,36,26,35,34,37,17,7,20

[AppSet3]
App=Canvas effects\SkyOcean
FParamValues=0,0.96,1,0,0,0.5,0.5,0.708,0.34,0.736,1,1,0.576,0.68
ParamValues=0,960,1000,0,0,500,500,708,340,736,1000,1000,576,680
Enabled=1
ImageIndex=1
Name=React 1

[AppSet1]
App=Background\FogMachine
FParamValues=0.672,0.588,1,0.064,0.456,0.5,0.5,0.5,0.5,0.636,0,0.516
ParamValues=672,588,1000,64,456,500,500,500,500,636,0,516
ParamValuesBackground\Youlean Background MDL=1000,1000,1000,420,884,424,198,320,648,558,376,696,800,664,1000,0,500,4,0,500
Enabled=1
Collapsed=1
ImageIndex=1
Name=Liner Color

[AppSet2]
App=Canvas effects\SkyOcean
FParamValues=0,0.856,1,1,0,0.5,0.5,0,0.404,0.656,1,0.568,0.048,0.488
ParamValues=0,856,1000,1000,0,500,500,0,404,656,1000,568,48,488
ParamValuesBlend\VideoAlphaKey=0,636,1000,0,1000,656,344,0,0,0,474,92,340
Enabled=1
Collapsed=1
ImageIndex=1
Name=React 2

[AppSet4]
App=Canvas effects\SkyOcean
FParamValues=0,0.592,1,1,0.232,0.5,0.5,0.708,0.34,0.568,0.864,0.924,0,0
ParamValues=0,592,1000,1000,232,500,500,708,340,568,864,924,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=React 3

[AppSet9]
App=Canvas effects\Rain
FParamValues=0.896,1,1,0.648,0.216,0.5,0.5,0.5,0.5,0,0.736,1
ParamValues=896,1000,1000,648,216,500,500,500,500,0,736,1000
ParamValuesCanvas effects\OverlySatisfying=0,0,0,0,600,500,500,100,100,1000,500
Enabled=1
Collapsed=1
ImageIndex=1
Name=Modliner X1

[AppSet12]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.924,0.108,0.304,0.608,0.472,0,0.268
ParamValues=0,0,0,0,924,108,304,608,472,0,268
Enabled=1
Collapsed=1
ImageIndex=1
Name=Start Riser 2

[AppSet15]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.932,0.328,0.304,0.496,0.472,0,0.72
ParamValues=0,0,0,0,932,328,304,496,472,0,720
Enabled=1
Collapsed=1
ImageIndex=1
Name=Start Riser 4

[AppSet29]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.932,0.52,0.704,0.5,0.472,0,0.72
ParamValues=0,0,0,0,932,520,704,500,472,0,720
Enabled=1
Collapsed=1
ImageIndex=1
Name=Start Riser 6

[AppSet16]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.92,0.712,0.296,0.496,0.472,0,0.672
ParamValues=0,0,0,0,920,712,296,496,472,0,672
Enabled=1
Collapsed=1
ImageIndex=1
Name=Start Riser 8

[AppSet32]
App=Background\ItsFullOfStars
FParamValues=0.048,0,0,0,0.876,0.936,0.504,0.496,0.472,0,0.672
ParamValues=48,0,0,0,876,936,504,496,472,0,672
Enabled=1
Collapsed=1
ImageIndex=1
Name=Start Riser 10

[AppSet10]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.92,0.904,0.268,0.496,0.472,0,0
ParamValues=0,0,0,0,920,904,268,496,472,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=Start Riser 12

[AppSet24]
App=Tunnel\Youlean Tunnel
FParamValues=0,0,0,1,1,0.5,0.5,0,0,0,0,0.13,0.472,0.25,0.5,0.5,0.5,0.5,0.5,0,0,0.338,1,0.5,0.5
ParamValues=0,0,0,1000,1000,500,500,0,0,0,0,130,472,250,500,500,500,500,500,0,0,338,1,500,500
ParamValuesHUD\HUD Prefab=27,0,500,0,0,500,500,158,1000,1000,444,0,500,1000,368,0,1000,1000
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPostprocess\Youlean Audio Shake=0,250,0,0,200,0,100,200,500,500,500,500,0,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesImage effects\ImageTileSprite=4,4,0,0,0,0,4,250,250,500,500,500,500,0,0,0,0
ParamValuesPostprocess\ColorCyclePalette=0,0,0,0,0,1000,0,500,0,0,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesHUD\HUD Image=0,0,500,500,1000,1000,500,444,500,0,0,1000,1000,0,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesBackground\Youlean Background MDL=1000,892,0,0,500,500,50,320,0,2,300,120,1000,1000,0,300,500,0,0,500
ParamValuesPostprocess\Youlean Pixelate=628,0,0,0
ParamValuesImage effects\Image=0,0,0,1000,672,500,500,0,0,0,0,0,1000,1000
ParamValuesObject Arrays\BallZ=0,0,1000,1000,392,500,500,818,500,500,500,500,500
ParamValuesPostprocess\Blur=1000
Enabled=1
Collapsed=1
ImageIndex=1
Name=Core Effect

[AppSet23]
App=Peak Effects\Stripe Peeks
FParamValues=0,4,0,0,0,0,0.142,0.5,0.5,0,0.294,0.5,0.612,0,0.5,0,0.356,0.108,0.188,1,1,0,0
ParamValues=0,4,0,0,0,0,142,500,500,0,294,500,612,0,500,0,356,108,188,1,1,0,0
ParamValuesFeedback\WarpBack=500,0,0,0,500,500,24
ParamValuesPeak Effects\Reactive Sphere=0,1000,973,0,773,500,500,252,108,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesPeak Effects\ReflectedPeeks=0,60,712,104,336,500,500,744,24
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,1000,500,500,500,500,500,500,500
ParamValuesHUD\HUD 3D=0,0,500,500,500,500,500,500,500,500,500,500,500,500,0,500,500,500,500,500,500,500,500,1000,1000,0,0,1000,1000,0
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,0,204,532,408,668,500,500
Enabled=1
Collapsed=1
Name=EQ
LayerPrivateData=780163608081067B10CB2F49004837D84BEBDFB505F1D50D39C07C109B18306BE64CBBB3677CE018A467D64C493B9038880D92FBF2F70A50FE8CDD0E3957FB45AEDBEC407609353BD8AD38761A6C17503DDC4E0039F0243D

[AppSet25]
App=Feedback\BoxedIn
FParamValues=0,0,0,1,1,0.252,0.472,0,0.116,0.972
ParamValues=0,0,0,1000,1000,252,472,0,116,972
ParamValuesHUD\HUD Prefab=18,0,500,0,1000,500,500,664,1000,1000,444,0,500,0,368,0,1000,1000
Enabled=1
Collapsed=1
Name=Cinema Effect

[AppSet22]
App=Postprocess\Youlean Audio Shake
FParamValues=0,0.206,0.176,0.052,0.348,0.032,0.064,0.196,0.5,0.5,0.484,0.42,0,0
ParamValues=0,206,176,52,348,32,64,196,500,500,484,420,0,0
ParamValuesFeedback\WarpBack=0,0,0,0,500,500,20
ParamValuesImage effects\ImageMasked=1000,0,412,20,820,500,500,200,200,1000,0
ParamValuesImage effects\ImageSphereWarp=0,500,0,1000,250,400,1000,204,260,500
ParamValuesPeak Effects\StereoWaveForm=0,0,0,1000,0,0,1000,1000,1000,1000,1000,500,500,500,500,500,500,500,180,500,500,1000,12,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesFeedback\WormHoleEclipse=0,0,0,0,0,0,500,400,436,500,500
ParamValuesImage effects\ImageTileSprite=4,4,0,0,0,0,4,250,250,500,500,500,500,0,0,0,0
ParamValuesHUD\HUD Grid=0,500,0,1000,500,500,1000,1000,500,444,500,1000,500,100,0,500,1000,100,500,300,0,1000
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesPostprocess\ColorCyclePalette=0,0,0,0,0,1000,0,500,0,0,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesHUD\HUD Image=0,0,500,500,1000,1000,500,444,500,0,0,1000,1000,0,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPostprocess\AudioShake=16,0,0,328,244,900
ParamValuesImage effects\ImageSphinkter=0,0,0,1000,1000,500,500,500,500,500,500,500,500,0,0
ParamValuesImage effects\Image=580,0,0,1000,1000,480,500,112,0,0,0,0,1000,1000
ParamValuesHUD\HUD 3D=0,0,500,500,500,500,500,500,500,500,500,500,500,500,0,500,500,500,500,500,500,500,500,1000,1000,0,0,1000,1000,0
Enabled=1
Collapsed=1
Name=Shake

[AppSet28]
App=Feedback\FeedMeFract
FParamValues=0,0,0,0.552,0.32,0.068
ParamValues=0,0,0,552,320,68
ParamValuesFeedback\BoxedIn=0,0,0,1000,1000,0,0,1000,0,500
Enabled=1
Collapsed=1
Name=Mirror

[AppSet11]
App=Feedback\BoxedIn
FParamValues=0,0,0,1,0.268,0,0,0.5,0.264,0.544
ParamValues=0,0,0,1000,268,0,0,500,264,544
ParamValuesFeedback\FeedMeFract=0,0,0,248,28,68
ParamValuesCanvas effects\ShimeringCage=0,0,0,0,160,500,500,0,0,0,0,0,0,76
ParamValuesCanvas effects\FreqRing=500,500,1000,0,312,500,500,92,140,312,315,500,500,320
Enabled=1
Collapsed=1
Name=Move to Stars

[AppSet36]
App=HUD\HUD Prefab
FParamValues=90,0,0,1,0,0.5,0.5,0.268,1,1,4,0,0.5,0,0.3684,0,1,1
ParamValues=90,0,0,1000,0,500,500,268,1000,1000,4,0,500,0,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78014B2FCA4C89490712BA0686167A9939650C230B0000BBC505E8

[AppSet26]
App=Postprocess\Point Cloud Low
FParamValues=0,0.818,0.658,0.512,0.564,0.364,0.496,0.499,0.646,1,0.1763,1,0.132,0,1
ParamValues=0,818,658,512,564,364,496,499,646,1000,176,1,132,0,1
ParamValuesFeedback\SphericalProjection=0,0,0,0,618,1000,0,590,500,880,0,333,530,1000,56,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesFeedback\BoxedIn=0,0,0,1000,208,0,252,464,64,824
ParamValuesFeedback\70sKaleido=0,0,0,1000,532,0
ParamValuesPostprocess\Point Cloud High=376,702,590,376,508,692,496,431,470,929,232,0,108,0,0
ParamValuesFeedback\FeedMe=0,0,0,1000,1000,1000,164
Enabled=1
Collapsed=1
ImageIndex=1
Name=Parter

[AppSet35]
App=Postprocess\Youlean Audio Shake
FParamValues=0,0.302,0,0,0.2,0,0.1,0.2,0.5,0.5,0.5,0.5,0,0
ParamValues=0,302,0,0,200,0,100,200,500,500,500,500,0,0
ParamValuesFeedback\WarpBack=500,0,0,0,496,500,328
ParamValuesFeedback\BoxedIn=0,0,0,1000,1000,0,0,500,880,500
ParamValuesFeedback\FeedMeFract=0,0,0,1000,0,800
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,0,500,500,500,556,340,0
ParamValuesPostprocess\Point Cloud Low=0,818,454,504,572,312,444,503,186,1000,156,1000,132,0,333
Enabled=1
Collapsed=1
Name=Shaker

[AppSet34]
App=Postprocess\Youlean Kaleidoscope
FParamValues=0.5,0.5,0,0.5,1,0,0.616,0.596,0.42,0.06,0
ParamValues=500,500,0,500,1000,0,616,596,420,60,0
ParamValuesPostprocess\Youlean Handheld=308,588,870,1000,180,1
ParamValuesFeedback\BoxedIn=0,0,0,1000,1000,0,0,500,504,500
ParamValuesFeedback\70sKaleido=0,0,0,324,0,440
ParamValuesHUD\HUD 3D=0,0,500,500,500,500,500,500,500,500,500,500,500,500,0,500,500,500,500,500,500,500,500,1000,1000,0,0,1000,1000,0
ParamValuesFeedback\WormHoleEclipse=0,0,736,1000,0,500,588,500,500,500,500
ParamValuesPostprocess\Point Cloud Low=0,818,454,504,572,312,444,503,186,1000,156,1000,132,0,333
Enabled=1
Collapsed=1
Name=Slow Mo

[AppSet37]
App=Scenes\Spherical Polyhedra
FParamValues=0.24,0,0,0.388,0,1,3,0.226,0.625,0.635,0.408,3
ParamValues=240,0,0,388,0,1,3,226,625,635,408,3
ParamValuesScenes\Boaty Goes Caving=732,250,1,1000
ParamValuesPostprocess\Youlean Pixelate=572,0,0,0
ParamValuesPostprocess\Youlean Motion Blur=400,1000,0,0,0,0,1000
ParamValuesScenes\Cloud Ten=988,0,0,0,0,0,1000,0,0,0,0
ParamValuesPostprocess\Youlean Kaleidoscope=500,500,0,732,1000,6,648,660,536,60,0
Enabled=1
Collapsed=1
Name=CORE

[AppSet17]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=1,0.12,0.414,0.5065,0.66,0,0,1
ParamValues=1000,120,414,506,660,0,0,1
ParamValuesScenes\Alien Thorns=0,500,250,500,2,500,500,0
ParamValuesTerrain\CubesAndSpheres=0,0,0,0,236,500,500,650,380,400,552,568,684,1000,1000,1000,0,1000
ParamValuesPostprocess\Blooming=0,0,0,1000,500,800,500,464,0
ParamValuesPostprocess\Youlean Audio Shake=0,250,0,0,200,0,100,200,500,500,500,500,0,0
ParamValuesPostprocess\ColorCyclePalette=0,0,0,0,0,1000,0,500,0,0,0
ParamValuesPostprocess\Point Cloud High=0,434,346,520,492,472,512,491,50,265,0,0,224,0,333
ParamValuesTunnel\Youlean Tunnel=0,0,0,1000,912,500,500,0,0,0,0,250,500,674,676,500,500,500,500,0,4,1000,1000,500,500
ParamValuesTerrain\GoopFlow=0,928,1000,0,280,500,500,0,500,500,500,424,1000,1000,596,1000
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=Bloom

[AppSet7]
App=Blend\Youlean From Buffer
FParamValues=0,1,0
ParamValues=0,1000,0
Enabled=1
Collapsed=1
Name=The Out

[AppSet20]
App=Background\FourCornerGradient
FParamValues=8,0.116,0,1,1,0.25,1,1,0.5,1,1,0.75,1,1
ParamValues=8,116,0,1000,1000,250,1000,1000,500,1000,1000,750,1000,1000
ParamValuesBackground\Youlean Background MDL=1000,892,0,0,500,500,230,320,0,242,300,120,1000,1000,0,300,500,0,0,500
ParamValuesBlend\VideoAlphaKey=0,888,1000,916,772,300,300,0,0,0,250,500,500
Enabled=1
Collapsed=1
Name=Filter-Color

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="You can","change this text","in settings."
Html="This is the default text."
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

[Wizard]
Section0Cb=Colove - Drone 01
Section1Cb=None
Section2Cb=HUD Meter 02
Section3Cb=Arcade cabinet

