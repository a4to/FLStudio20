FLhd   0 1 ` FLdt1  �20.5.1.1193 ��  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4               A                  @   �   �  B  �(C o l o r f u l   W a v e s i m p l e   �    �HQV ��.y  ﻿[General]
GlWindowMode=1
LayerCount=2
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,1

[AppSet0]
App=Canvas effects\TaffyPulls
ParamValues=500,302,424,24,0,500,500,0,500,500,0,0,0,500
ParamValuesBlend\BufferBlender=0,465,0,1000,0,0,0,0,500,500,750,0
ParamValuesBlend\VideoAlphaKey=0,176,1000,0,1000,300,300,0,0,1000,250,500,500
ParamValuesBlend\Youlean From Buffer=1000,417
ParamValuesBlend\Youlean Mask=711
ParamValuesCanvas effects\BitPadZ=0,495,0,0,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesCanvas effects\DarkSpark=500,821,500,500,500,500,500,500,500,500,0,0
ParamValuesCanvas effects\Digital Brain=0,349,0,0,1000,500,500,600,100,300,100,250,1000,500,1000,0
ParamValuesCanvas effects\Electric=0,771,484,780,1000,500,500,100,0,1000,500
ParamValuesCanvas effects\Flaring=0,553,1000,1000,1000,500,500,181,0,0,0
ParamValuesCanvas effects\Flow Noise=0,952,500,720,1000,500,500,100,0,0,0
ParamValuesCanvas effects\FreqRing=500,881,500,500,500,500,500,500,0,312,123,500,500,500
ParamValuesCanvas effects\ImageTileSprite=4,228,0,0,0,0,4,250,250,500,500,500,500,0,0,0,0
ParamValuesCanvas effects\Lava=0,676,500,720,1000,500,500,500,500,500,0
ParamValuesCanvas effects\N-gonFigure=300,452,900,200,500,500,500,500,500,400,0
ParamValuesCanvas effects\OverlySatisfying=0,312,484,780,600,500,500,100,100,1000,500
ParamValuesCanvas effects\Rain=500,95,500,500,500,500,500,500,500,500,500,500
ParamValuesCanvas effects\ShimeringCage=0,600,1000,0,0,500,500,0,0,0,0,0,0,0
ParamValuesCanvas effects\SkyOcean=0,87,500,0,500,500,500,0,500,500,0,0,0
ParamValuesCanvas effects\Stack Trace=0,53,1000,500,130,500,500,0
ParamValuesCanvas effects\StarTaser2=500,458,500,500,500,500,500,500,500,500,500,500,0
ParamValuesFeedback\70sKaleido=0,108,0,1000,0,500
ParamValuesPeak Effects\WaveSimple=0,389,1000,0,500,500,500,500,0
ParamValuesPeak Effects\Youlean Peak Shapes=0,517,0,0,250,500,500,500,250,200,700,500,20,200,500,0,0,200,200,200,150,0,0,1,500,1000,0,500,500,0,0,0
ParamValuesPhysics\Cage=600,966,300,0,800,500,500,500,500,500
ParamValuesPhysics\Columns=200,576,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPhysics\Heightfield=200,97,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPhysics\Ragdoll=0,910,500,500,500,600,500,1000
ParamValuesPostprocess\Ascii=0,864,0,600,700,200
ParamValuesPostprocess\AudioShake=500,963,0,500,100,900
ParamValuesPostprocess\Blooming=0,771,0,1000,500,800,500,500,0
ParamValuesPeak Effects\Fluidity=0,878,600,0,472,676,632
ParamValuesParticles\ReactiveFlow=0,769,500,250,500,0,500,100,500,500,500,500,500,0,0,500,200,500,125,500,500,500,1000,1000,100
ParamValuesPeak Effects\StereoWaveForm=0,722,0,1000,0,0,1000,1000,1000,1000,500,500,500,500,500,500,500,500,180,500,500,1000,12,0
ParamValuesPeak Effects\PeekMe=0,422,1000,0,500,500,500,230,0,0,0,0
ParamValuesPeak Effects\VectorScope=0,359,472,0,1000,1000,667,0
ParamValuesPeak Effects\Polar=0,287,0,0,500,500,500,500,500,500,500,500,0,500,0,500,0,1000,1000,0,0,0,0,1000
ParamValuesObject Arrays\8x8x8_Eggs=708,50,1000,0,0,500,500,200,500,454,0,0,0
ParamValuesPeak Effects\Reactive Sphere=0,71,893,0,773,500,500,252,252,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesObject Arrays\DeathClock8=0,159,0,0,500,500,500,1000,0,0,0,474
ParamValuesMisc\PentUp=0,920,1000,368,0,500,500,52,0,965,272,1000
ParamValuesPeak Effects\JoyDividers=0,737,0,0,700,500,500,0,500,100,600,650,510
ParamValuesPeak Effects\Linear=0,578,648,0,606,500,288,388,0,500,260,350,0,0,500,0,0,500,500,500,500,828,500,152,200,0,32,212,330,250,100
ParamValuesObject Arrays\CrystalCube=800,603,500,500,350,500,500,254
ParamValuesObject Arrays\CubicMatrix=0,963,1000,0,416,500,500,652,572,56,0,633,1000
ParamValuesObject Arrays\CubesGrasping=0,446,632,0,500,500,500,120,0,500,500
ParamValuesObject Arrays\DiamondBit=500,578,500,0,750,500,500,0,500,500,0,227,0
ParamValuesPeak Effects\SplinePeaks=0,950,1000,0,500,500,1000,0,0,0
ParamValuesObject Arrays\BallZ=0,883,1000,0,762,500,500,545,104,647,500,360,496
ParamValuesPeak Effects\ReflectedPeeks=500,972,500,0,500,500,500,500,0
ParamValuesParticles\StrangeAcid=0,85,0,0,500,500,500,50,500,500,0,200,300,0,0,0,400,766,600,567
ParamValuesPeak Effects\Stripe Peeks=0,290,1000,1000,0,0,250,500,500,0,250,500,700,1000,500,250,200,0,150,300,1000,0,0
ParamValuesParticles\ReactiveMob=0,229,500,500,500,500,500,1000,500,500,500,500,500,500,500,500,200,50,125,0,500,500,1000,1000,800,100,500,0,0,0
Enabled=1

[AppSet1]
App=Misc\Automator
ParamValues=1,1,2,3,1000,20,250,0,1,12,2,500,46,306,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""0""><position y=""4""><p align=""center""><font face=""04b03_COMP_MAT-Regular"" size=""9"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""0""><position y=""12""><p align=""center""><font face=""04b03_COMP_MAT-Regular"" size=""6"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""0""><position y=""33""><p align=""center""><font face=""04b03_COMP_MAT-Regular"" size=""3"" color=""#FFFFFF"">[extra1]</font></p></position>","<position x=""0""><position y=""78""><p align=""center""><font face=""04b03_COMP_MAT-Regular"" size=""4"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""0""><position y=""62""><p align=""center""><font face=""04b03_COMP_MAT-Regular"" size=""3"" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""0""><position y=""65""><p align=""center""><font face=""04b03_COMP_MAT-Regular"" size=""3"" color=""#FFFFF"">[extra3]</font></p></position>",
VideoUseSync=0
Filtering=0

[Detached]
Top=252
Left=760
Width=689
Height=577

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

