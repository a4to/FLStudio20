<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" FileVersion="2">
  <OnLoaded>
    <ZLibrary Comment="HSV Library">
      <Source>
<![CDATA[vec3 hsv(float h, float s, float v)
{
  s = clamp(s/100, 0, 1);
  v = clamp(v/100, 0, 1);

  if(!s)return vector3(v, v, v);

  h = h < 0 ? frac(1-abs(frac(h/360)))*6 : frac(h/360)*6;

  float c, f, p, q, t;

  c = floor(h);
  f = h-c;

  p = v*(1-s);
  q = v*(1-s*f);
  t = v*(1-s*(1-f));

  switch(c)
  {
    case 0: return vector3(v, t, p);
    case 1: return vector3(q, v, p);
    case 2: return vector3(p, v, t);
    case 3: return vector3(p, q, v);
    case 4: return vector3(t, p, v);
    case 5: return vector3(v, p, q);
  }
}]]>
      </Source>
    </ZLibrary>
  </OnLoaded>
  <OnUpdate>
    <ZExpression>
      <Expression>
<![CDATA[const int
  ALPHA = 0,
  HUE = 1,
  SATURATION = 2,
  LIGHTNESS = 3;

vec3 c=hsv(Parameters[HUE]*360,Parameters[SATURATION]*100,(1-Parameters[LIGHTNESS])*100);

ShaderColor.r=c.r;
ShaderColor.g=c.g;
ShaderColor.b=c.b;

uRes=vector2(app.ViewportWidth,app.ViewportHeight);
uViewport=vector2(app.ViewportX,app.ViewportY);
speed=1.0;
//uSpeed=Parameters[7]*6.0;

float delta=app.DeltaTime*speed;
uDeltaTimer+=delta;

float bandStep = SpecBandArray.SizeDim1/BandsCombined.SizeDim1;
for(int i=0; i<BandsCombined.SizeDim1; i++) {
  float f=BandsCombined[i];
  float damp=0.75;
  f=(f*damp) +
    (SpecBandArray[ clamp(bandStep*i,0,SpecBandArray.SizeDim1-1) ] * (1.0-damp));
  BandsCombined[i]=f;
//  BandsCombined[i]=SpecBandArray[ clamp(bandStep*i,0,SpecBandArray.SizeDim1-1) ];
}]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <OnRender>
    <UseMaterial Material="matShaderToy"/>
    <RenderSprite/>
  </OnRender>
  <Content>
    <Group Comment="Shader Variables">
      <Children>
        <Variable Name="uRes" Type="6"/>
        <Variable Name="uDeltaTimer"/>
        <Variable Name="uViewport" Type="6"/>
        <Array Name="BandsCombined" SizeDim1="16" Persistent="255">
          <Values>
<![CDATA[78DA636260606042C2C402985A000220000B]]>
          </Values>
        </Array>
        <Variable Name="ShaderColor" Type="7"/>
      </Children>
    </Group>
    <Shader Name="ShaderToy" UpdateVarsOnEachUse="255">
      <VertexShaderSource>
<![CDATA[//varying vec2 p;

uniform float iPositionY;
uniform float iPositionX;
uniform float iScale;

varying vec2 iUV;

void main(){
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;

  iUV.x=0.5 + vertex.x*0.5;
  iUV.y=0.5 + vertex.y*0.5;

  vertex.x*=iScale;
  vertex.y*=iScale;

  vertex.y+=iPositionY;
  vertex.x+=iPositionX;

  gl_Position = vertex;
  //p=vec2(vertex.x,vertex.y);
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[uniform vec2 iResolution,iViewport;
uniform float iGlobalTime;
uniform float iAlpha;
uniform sampler2D bands;

uniform vec3 iColor;

varying vec2 iUV;

const float bandCount = 16.0;

//Shader origin: https://www.shadertoy.com/view/MsXGDj
//by Dave Hoskins

// The functions use the following format:

// v0----v1--x--v2----v3

// Where 'x' is the fractional diff betweeen v1 and v2.

//--------------------------------------------------------------------------------
//  1 out, 1 in...
#define HASHSCALE .1031
float Hash(float p)
{
  vec3 p3  = fract(vec3(p) * HASHSCALE);
    p3 += dot(p3, p3.yzx + 19.19);
    return fract((p3.x + p3.y) * p3.z);
}


//--------------------------------------------------------------------------------
float Catmull_Rom(float x, float v0,float v1, float v2,float v3)
{
  float c2 = -.5 * v0 + 0.5*v2;
  float c3 = v0   + -2.5*v1 + 2.0*v2 + -.5*v3;
  float c4 = -.5 * v0 + 1.5*v1 + -1.5*v2 + 0.5*v3;
  return(((c4 * x + c3) * x + c2) * x + v1);

//  Or, the same result with...
//  float x2 = x  * x;
//  float x3 = x2 * x;
//  return 0.5 * ( ( 2.0 * v1) + (-v0 + v2) * x +
//                  (2.0 * v0 - 5.0 *v1 + 4.0 * v2 - v3) * x2 +
//                  (-v0 + 3.0*v1 - 3.0 *v2 + v3) * x3);


}

float getBand(float x) {
  return 0.5 + texture2D(bands,vec2(x/bandCount,0.0)).r*0.45;
}

//================================================================================
void main(void)
{
  vec2 uv = iUV; //(gl_FragCoord.xy-iViewport)/iResolution.xy;

  float pos = uv.x*bandCount;//(.5+uv.x) * 4.0;

  uv.x *= iResolution.x/iResolution.y;

  float x  = fract(pos);
  float v0 = getBand(floor(pos));
  float v1 = getBand(floor(pos)+1.0);
  float v2 = getBand(floor(pos)+2.0);
  float v3 = getBand(floor(pos)+3.0);
/*  float v0 = Hash(floor(pos));
  float v1 = Hash(floor(pos)+1.0);
  float v2 = Hash(floor(pos)+2.0);
  float v3 = Hash(floor(pos)+3.0);*/

  float f = Catmull_Rom(x, v0, v1, v2, v3);

  // Blobs...
  f = .02 / abs(f-uv.y);
/*  float d = .03/length((vec2(((uv.x)/9.0*.25), uv.y)-vec2(x+.03, v1)) * vec2(.25,1.0));
  f = max(f, d*d);
  d = .03/length((vec2(((uv.x)/9.0*.25), uv.y)-vec2(x-.97, v2)) * vec2(.25,1.0));
  f = max(f, d*d);*/

  //vec3(1.0,.2, .05)
  gl_FragColor = vec4(iColor * f, (f*2.0)*iAlpha);
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="iResolution" VariableRef="uRes"/>
        <ShaderVariable VariableName="iGlobalTime" VariableRef="uDeltaTimer"/>
        <ShaderVariable VariableName="iViewport" VariableRef="uViewport"/>
        <ShaderVariable VariableName="bands" ValueArrayRef="BandsCombined"/>
        <ShaderVariable VariableName="iPositionY" ValuePropRef="((1.0-Parameters[4])-0.5)*2"/>
        <ShaderVariable VariableName="iPositionX" ValuePropRef="(Parameters[5]-0.5)*2"/>
        <ShaderVariable VariableName="iAlpha" ValuePropRef="1-Parameters[0];"/>
        <ShaderVariable VariableName="iColor" VariableRef="ShaderColor"/>
        <ShaderVariable VariableName="iScale" ValuePropRef="Parameters[6]"/>
      </UniformVariables>
    </Shader>
    <Shader Name="ShaderToyModelView">
      <VertexShaderSource>
<![CDATA[varying vec2 p;

void main(){
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = gl_Position = gl_ModelViewProjectionMatrix * vertex;
  p=vec2(vertex.x,vertex.y);
}]]>
      </VertexShaderSource>
    </Shader>
    <Group Comment="FL Studio">
      <Children>
        <Array Name="SpecBandArray" SizeDim1="32"/>
        <Array Name="Parameters" SizeDim1="10" Persistent="255">
          <Values>
<![CDATA[78DA63606060E0BABED8C6D8B8D89E0102EC21B801C6070300710E0416]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Alpha
Hue
Saturation
Lightness
PositionY
PositionX
Scale]]>
          </StringValue>
        </Constant>
        <Constant Name="AuthorInfo" Type="2">
          <StringValue>
<![CDATA[David Hoskins
Adapted from https://www.shadertoy.com/view/MsXGDj]]>
          </StringValue>
        </Constant>
      </Children>
    </Group>
    <Group Comment="Parameter Variables">
      <Children>
        <Variable Name="speed"/>
      </Children>
    </Group>
    <Material Name="matShaderToy" Blend="1" Shader="ShaderToy"/>
  </Content>
</ZApplication>
