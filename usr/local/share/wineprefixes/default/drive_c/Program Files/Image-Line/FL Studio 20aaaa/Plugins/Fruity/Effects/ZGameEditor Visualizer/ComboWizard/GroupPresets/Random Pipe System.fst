FLhd   0  0 FLdta  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   ��)�  ﻿[General]
GlWindowMode=1
LayerCount=8
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=2,6,5,4,3,0,1,7

[AppSet2]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
ParamValuesPeak Effects\SplinePeaks=940,0,0,0,1000,500,1000,0,0,0
Enabled=1
Name=Section2

[AppSet6]
App=HUD\HUD 3D
FParamValues=0,1,0.8175,0.2098,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.3633,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,0
ParamValues=0,1,817,209,500,500,500,500,500,500,500,500,500,363,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,0
Enabled=1
ImageIndex=3
Name=ForegroundMod

[AppSet5]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
ImageIndex=1
Name=TextFst

[AppSet4]
App=Text\TextTrueType
FParamValues=0,0.8333,0,1,0,0.5,0.5,0,0,0,0.5
ParamValues=0,833,0,1000,0,500,500,0,0,0,500
Enabled=0
Name=Text

[AppSet3]
App=Postprocess\Youlean Drop Shadow
FParamValues=0.5,0.2,0,1,0.02,0.875,0
ParamValues=500,200,0,1000,20,875,0
Enabled=1
UseBufferOutput=1

[AppSet0]
App=Youlean new shaders\Patterns\Random Pipe System
FParamValues=0,0.5,0,0.5,0.6
ParamValues=0,500,0,500,600
Enabled=1
Name=Section0

[AppSet1]
App=Postprocess\Vignette
FParamValues=0,0,0,0.6,0.9079,0
ParamValues=0,0,0,600,907,0
ParamValuesPostprocess\Raindrops=404,0
ParamValuesPostprocess\RGB Shift=128,0,0,600,700,200
ParamValuesPostprocess\Youlean Color Correction=226,500,539,500,500,500
Enabled=1
Name=Section1

[AppSet7]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
ImageIndex=2

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Images="@EmbeddedFst:""Name=Song Position horizontal.fst"",Data=78018D544B6EDB3010DD1BF01D0CAF5597A4FE4518C0DF06A8D3BA713E28922C586B1213952581A2D2F86C5DF448BD426728DBB0D12C8A381467C8796FE671C83FBF7EDF7F84028CCA1FBB9D8FF99D2EB2F2E7659981E4DDCE5C6DC18CCBA6B0D2EF76668B25392F75A617A5B1F21D1AC3BA8295BD52569792471FD25DCC17938191CCE39EE876BA9DFB61552DC132A4C099BCD61BE821915E295B9AFAE15BD9E4A08A9EF38F94212A65D4E656E50DD408C306611478F46522212B8D9C25FC0829C81FC5E819F000770D429A0941234D190EEEAFDD89499F82B7D088DBA23A4CC62884400932448B0079C8707E84C6193B415B80FAD19B3E3DA124F5C3A2CC1569B0238F458B13269E4F7844E106240984A3F379EB21BEE31D070C8CE976A685FA9E434647715096FF8FB24BAB36D55BDAB6290E822845B952271A69464A8B00474A867476CAD28A53D23BAD1D2B6DF3661E01114C6B27DE1108554DEEA3125D752765B9B65B18FDA22C4C9455324E188FFDF1749C8859301D4F8251309D8471381E8E67411A719EC42922B3D1301AB260121F292376CA9C1CCD5C17F0669BA56D9B61A95C504351FF08E65A49B40DE83CB4E4B742ECA4D84BB3FFEE627948B1F88F23F3DD9CD3E8FBE426AC7FDA719F0225402A113D77E4CE8A98E7BB1E742782C07B35F7DFD643C4C2B511B20AE4F47D84C240D7B2A72D74AB33287BF05AE1A546B59C7921394BF0809D7127792AF6C6151478B767552D7D742DB1A572C0FB0F72BF618C8FC7EAB3DA80C4DBDE64BA3C7638BCAF8DCAB5DD52C4A57AFD04DB195E7658566AA58B6784A52E9FE91C8A1664A4AD21021E073C11012E3AD8835B242EE2A658959BCA405DE3E5C04DCBA60253BBFCC8C4BB7253D36B565828A8CE6B78B5B27FBDD6750F7F760DBD0C9E5493DB9EC59541BFDBB9B09B5CF6CFAA1EA6FB5CC87E7F859160FAFDF37BDA9263173D9EBDAFCE71ABAB0BF197DB62456C983EEEA4725AEA0958B55A4346BC6525FD34C097129EACE43EBE99773AB36B192538BD00FDBCB6328CDB26A6744D99E76030F20AB2F94B2E4518E1536D008A8335C287F260CC9BCD61DEEDFC0581988E8D","@EmbeddedFst:""Name=Handwritten mixer tag.fst"",Data=7801C5544B6EDB3010DD1BF01D02AFD594A4A85F110690252B06EAB469DC240B370BD69AC6046449A0A8343E5B173D52AFD0211DB9C9A680D14521D8E2FCDE0C9F66E6D78F9FAB0BA841CBEA7E3CBAA8EE545D36DF2F9B12041D8F1672073A6BFADA087F3C2AAE965679A94A75D56823DEA090762DACCDB534AA11347C973CC77CD42568413CEAB1F1683C5AA56DBB044330059EC4FC26FF82BF93CFF0642CACD4727B2BAB1E3A0C21A78187FFF84E288BF015F390FED1A225E2566409BE3CE6F998C4C5B8383CBB074B7B0D1B90BD01413D44F406D96A1110E10634829ED6BAF7A0563C60CE6AF9B582F2C0CD95568FD2402E8D14514C289966292FB21999F15996B124A724E729CF5991E53C6159CED2294FB36918D1985B6812446446A294BCA0891E495310589A12160D44F878C77DC9DC5A7C16C5C3E58EA22A083C447544BC8244C001EF5FC82AFC6CC6E32CE3D3BFD11424962636CB521286D885876E62C7D11447F6139F26916B2AEC316C2C4B8B7D1C4D8C85B6F38EEE2804F610D5D1F40A120107BCFF42D3AD2AA13981A7168715B972E25C501263B339E14ED0840DC235D438B345DB091F554BB96D2BC0B906313864B814D61FE416044E715FAAE6A5C2E17DEA65A5CCCE465CCAA7F7B02B70B061D9CAB5AA1F1096A0A15015D47B90A932DA26A011A731E36874B007358B5DC44DBD6EB6AD86AEC3C143A765DF82EE5C7D56C47EB8E9EC96AA0DD4F69E76AB889551A6827B6F257BB369341E1064BB77989B6D252667ED0956FB508BC9648D7AD093C9F9CA606CA56AB83F7BDB9E4F9E6942F8E5AE5EDB64583D7ADADBEC33E760E47A03A54DDBB4C24F382E40F86604F57115DEA9D26C4418E3710EEA6163441045AE625BAD6EAA0A34465E43B978AC040B42DCC01AA03E4853DC890761D16F0FE7F1E8373A7C827B"
VideoUseSync=0
Filtering=0

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

[Wizard]
Section0Cb=Random Pipe System
Section1Cb=Vignette
Section2Cb=Song Position horizontal
Section3Cb=Handwritten mixer tag
Macro0=Song Title
Macro1=Song Author
Macro2=Song made with love and time

