FLhd   0 * ` FLdtv  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��5�  ﻿[General]
GlWindowMode=1
LayerCount=14
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,7,1,6,5,12,13,11,4,2,8,3,9,10

[AppSet0]
App=Canvas effects\Flaring
FParamValues=0,0,0,0,0.428,0.5,0.5,0.041,0,3,0
ParamValues=0,0,0,0,428,500,500,41,0,3,0
ParamValuesCanvas effects\Digital Brain=0,212,796,592,728,500,500,156,572,0,228,442,1000,476,1000,56
ParamValuesCanvas effects\Lava=0,148,924,872,780,500,500,520,676,752,500
ParamValuesBackground\SolidColor=0,0,1000,836
ParamValuesCanvas effects\Electric=0,376,484,0,436,500,500,576,496,1000,500
ParamValuesCanvas effects\Stack Trace=540,152,1000,548,218,224,600,0
ParamValuesCanvas effects\Flow Noise=0,880,952,788,0,500,500,184,100,1000,500
ParamValuesCanvas effects\OverlySatisfying=672,740,484,636,600,500,500,100,100,1000,500
Enabled=1
Collapsed=1
Name=EFX 1

[AppSet7]
App=Postprocess\ScanLines
FParamValues=0,2,1,1,0
ParamValues=0,2,1000,1000,0
ParamValuesFeedback\WarpBack=500,0,0,568,500,500,28
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,120,1000,500,500,500,500,500
ParamValuesHUD\HUD Prefab=6,0,500,0,1000,500,500,734,1000,1000,444,0,500,1000,820,0,1000,1000
Enabled=1
Collapsed=1
Name=EFX 2

[AppSet1]
App=Postprocess\Edge Detect
FParamValues=0,0,0,0.664,0.384,0.572
ParamValues=0,0,0,664,384,572
ParamValuesBackground\FourCornerGradient=400,1000,752,1000,1000,728,1000,1000,48,1000,1000,568,1000,1000
ParamValuesPostprocess\Ascii=0,136,176,600,700,200
Enabled=1
Collapsed=1
Name=DARKHOLE

[AppSet6]
App=Postprocess\Youlean Motion Blur
FParamValues=1,1,1,0.216,0,0,0
ParamValues=1000,1,1000,216,0,0,0
ParamValuesHUD\HUD Prefab=73,0,500,0,1000,500,500,486,1000,1000,444,0,500,1000,368,212,1000,1000
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,1000,708,336,28,784,500,500
ParamValuesHUD\HUD Grid=0,500,0,1000,500,500,1000,1000,1000,444,500,1000,500,100,0,500,1000,516,1000,300,0,1000
ParamValuesImage effects\ImageSphinkter=0,0,0,1000,500,500,500,500,500,500,500,500,500,620,0
ParamValuesObject Arrays\8x8x8_Eggs=0,0,0,0,256,500,500,128,500,500,320,204,172
ParamValuesFeedback\BoxedIn=1000,0,1000,488,1000,1000,0,984,0,0
ParamValuesPostprocess\Blur=1000
ParamValuesPostprocess\Youlean Blur=500,0,504
ParamValuesObject Arrays\BallZ=620,1000,1000,0,550,500,500,818,584,936,0,1000,360
ParamValuesObject Arrays\Rings=0,0,0,1000,544,500,500,500,500,0,0,0,164,1000,0
Enabled=1
Collapsed=1
Name=Blured

[AppSet5]
App=HUD\HUD Prefab
FParamValues=21,0.728,1,1,0.004,0.5,0.5,0.152,1,1,4,0,0.5,0,0.444,0,0.916,1
ParamValues=21,728,1000,1000,4,500,500,152,1000,1000,4,0,500,0,444,0,916,1
ParamValuesHUD\HUD Callout Line=0,500,0,1000,500,500,700,400,0,200,0,200,100,450,100,1000
ParamValuesHUD\HUD Graph Radial=0,500,0,1000,500,500,202,444,500,0,1000,0,1000,0,250,728,200,0,0,0,500,1000
ParamValuesFeedback\BoxedIn=0,0,0,1000,1000,0,312,500,0,1000
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,900,708,336,28,784,500,500
ParamValuesHUD\HUD Grid=352,500,0,1000,500,500,1000,1000,844,444,500,1000,500,820,1000,1000,648,0,888,0,1000,1000
Enabled=1
Collapsed=1
Name=EFX 3
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C4CF43273CA18863D00000B860A9F

[AppSet12]
App=Canvas effects\ShimeringCage
FParamValues=0,0.072,1,0,0.212,0.5,0.5,0,0,0.112,0.016,0,0,0
ParamValues=0,72,1000,0,212,500,500,0,0,112,16,0,0,0
ParamValuesCanvas effects\Flaring=396,1000,1000,1000,1000,500,500,41,0,0,0
ParamValuesCanvas effects\FreqRing=0,508,1000,500,564,500,500,916,0,312,123,500,500,500
ParamValuesCanvas effects\N-gonFigure=300,654,900,200,60,500,500,0,1000,152,0
ParamValuesCanvas effects\Flow Noise=616,932,1000,720,1000,500,500,100,0,0,0
ParamValuesCanvas effects\OverlySatisfying=496,0,0,0,600,500,500,100,100,1000,500
Enabled=1
Collapsed=1
Name=Satisfying 1

[AppSet13]
App=Canvas effects\ShimeringCage
FParamValues=0,0.1,1,0,0.212,0.5,0.5,0,0.656,0.152,0.016,0,0,0
ParamValues=0,100,1000,0,212,500,500,0,656,152,16,0,0,0
Enabled=1
Collapsed=1
Name=Satisfying 2

[AppSet11]
App=Postprocess\AudioShake
FParamValues=0.4,0,1,0.5,0.1,0.9
ParamValues=400,0,1,500,100,900
ParamValuesHUD\HUD Prefab=67,0,500,0,1000,500,500,660,1000,1000,444,0,500,1000,1000,0,1000,1000
Enabled=1
Collapsed=1
Name=Shake

[AppSet4]
App=Feedback\FeedMe
FParamValues=0,0,0,1,0.212,1,0
ParamValues=0,0,0,1000,212,1000,0
ParamValuesTerrain\CubesAndSpheres=0,0,0,1000,0,500,500,650,540,400,1000,1000,1000,1000,1000,1000,0,1000
ParamValuesPostprocess\Youlean Motion Blur=484,1000,732,764
ParamValuesFeedback\BoxedIn=0,0,0,552,1000,0,260,500,0,820
ParamValuesPostprocess\Youlean Color Correction=308,500,512,500,836,904
Enabled=1
Collapsed=1
Name=Moon EFX

[AppSet2]
App=Postprocess\Youlean Color Correction
FParamValues=0.506,0.392,0.5,0.924,0.5,0.564
ParamValues=506,392,500,924,500,564
Enabled=1
Collapsed=1
Name=Color Correction

[AppSet8]
App=Feedback\70sKaleido
FParamValues=0,0,0,0.652,0.292,0.364
ParamValues=0,0,0,652,292,364
ParamValuesHUD\HUD Prefab=375,0,500,0,980,500,500,392,1000,1000,444,0,500,1000,920,0,940,1000
ParamValuesPostprocess\RGB Shift=560,0,0,600,700,200
ParamValuesPostprocess\Point Cloud High=0,730,594,500,568,448,444,503,226,549,156,0,0,0,0
ParamValuesPostprocess\ScanLines=0,200,0,0,0,0
ParamValuesPostprocess\Youlean Blur=500,1000,336
ParamValuesPostprocess\Youlean Motion Blur=656,1000,0,0
ParamValuesBackground\FourCornerGradient=9,688,680,1000,1000,250,1000,1000,500,1000,1000,750,1000,1000
ParamValuesFeedback\FeedMe=0,0,0,1000,212,1000,0
Enabled=1
Collapsed=1
Name=To In EFX

[AppSet3]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.588,0,0.594,0.302,1,0,0,0
ParamValues=588,0,594,302,1000,0,0,0
Enabled=1
Collapsed=1
Name=To Out

[AppSet9]
App=Text\TextTrueType
FParamValues=0.12,0,0,0,0,0.5,0.5,0,0,0,0.5
ParamValues=120,0,0,0,0,500,500,0,0,0,500
Enabled=1
Collapsed=1
Name=Main Text

[AppSet10]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fade in-out

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text=Author,"Song Title"
Html="<position y=""42.7""><p align=""center""><font face=""American-Captain"" size=""5.5"" color=""#000000"">[author]</font></p></position>","<position y=""47""><p align=""center""><font face=""Chosence-Bold"" size=""3"" color=""#000000"">[title]</font></p></position>","<position y=""55""><p align=""center""><font face=""Chosence-Bold"" size=""3"" color=""#000000"">[comment]</font></p></position>",,," ",,,
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

