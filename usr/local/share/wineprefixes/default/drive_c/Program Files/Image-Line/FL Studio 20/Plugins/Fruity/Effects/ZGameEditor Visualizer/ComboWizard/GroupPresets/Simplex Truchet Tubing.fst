FLhd   0  0 FLdt|  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   Ո   ﻿[General]
GlWindowMode=1
LayerCount=8
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=2,6,5,4,3,0,1,7

[AppSet2]
App=Peak Effects\Youlean Peak Shapes
FParamValues=0,0,0,0,0.066,0.5,0.37,0.72,0.49,0.104,0.564,0.5,0.12,0.328,0.5,0,0,0.2,0.2,0.36,0.358,0,0.52,1,0,1,0,0.5,0.5,0,0,0
ParamValues=0,0,0,0,66,500,370,720,490,104,564,500,120,328,500,0,0,200,200,360,358,0,520,1,0,1,0,500,500,0,0,0
ParamValuesImage effects\Image=0,0,0,0,1000,500,500,0,0,0,1000,0,0,0
ParamValuesPeak Effects\Polar=0,0,0,0,200,500,194,52,500,500,116,536,0,14,1000,500,0,509,1000,0,0,0,0,1000
ParamValuesPeak Effects\SplinePeaks=940,0,0,0,1000,500,1000,0,0,0
Enabled=1
ImageIndex=2
Name=Section2

[AppSet6]
App=HUD\HUD 3D
FParamValues=0,0,0.1627,0.5659,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.3438,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,0
ParamValues=0,0,162,565,500,500,500,500,500,500,500,500,500,343,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,0
Enabled=1
ImageIndex=2
Name=ForegroundMod

[AppSet5]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
Name=TextFst

[AppSet4]
App=Text\TextTrueType
FParamValues=0,0.8333,0,1,0,0.5,0.5,0,0,0,0.5
ParamValues=0,833,0,1000,0,500,500,0,0,0,500
Enabled=0
Name=Text

[AppSet3]
App=Postprocess\Youlean Drop Shadow
FParamValues=0.5,0.2,0,1,0.02,0.875,0
ParamValues=500,200,0,1000,20,875,0
Enabled=1
UseBufferOutput=1

[AppSet0]
App=Youlean new shaders\Tunnel\Simplex Truchet Tubing
FParamValues=0,0,0,0.5,0.6
ParamValues=0,0,0,500,600
ParamValuesImage effects\Image=0,0,0,0,1000,500,500,0,0,0,1000,0,0,0
Enabled=1
Name=Section0

[AppSet1]
App=Postprocess\Vignette
FParamValues=0,0,0,0.6,0.7738,0
ParamValues=0,0,0,600,773,0
ParamValuesPostprocess\Raindrops=404,0
ParamValuesPostprocess\RGB Shift=128,0,0,600,700,200
ParamValuesPostprocess\Youlean Color Correction=226,500,539,500,500,500
Enabled=1
Name=Section1

[AppSet7]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
ImageIndex=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Images="@EmbeddedFst:""Name=Tech showdown.fst"",Data=7801C554CD6EDB300CBE1BF03B14397B9D24FF6AA80A3876D2004BB7AE59DB43D68366738D00FF4191BBE6D976D823ED1546393F4B0F3BB43B0C862D92223FD21F48FEFAF17379010D6859DDBBCE4575A79AB2FD7ED99620A8EBCCE50674D6F68D11BEEB4CAF16D678A94A75D56A23DEA092AE3B28CCB534AA15347AC777311F75095A108F7ACC755C679976DD020CC1142889D94DFE05DF93CFF0642CACD4B2BE95550F6B0C21A7A1875F3C79C4513C4D18E77FAC7813C6A8324E029499E76392216688437978B0B4E7B021D95E20A88788DE5EB7560444B83D1A414F7BBBF5A056DD3E883969E4D70ACA0337575A3D4A03B93452C409A1539F4D83344B59C6789666D3DC8F437B063CA76CCC78C0239F2621475012C42424519C1E11445F4A506209E2D4DF1174EA53B62B96595658C82D21AF2029F11075A0E0192402EEF1FE85A6D8CF265982544DB23C1807933CFC3B4D8CA4131285D87F873E622FA6C992C143FB1D3A2A488E59A251645BEE752C21A86D94E4181201F778FF85A55B55427B024F1D4E295235A83341494276CA9DA09CED956B687058A7DD5AF8685AC8BAAB00071AC4DE21C36D507C9035081CDFBE54EDB16100FFD4CB4A998D8DB8944FEF6133C5898645270BD53C202CC18BA9AAA0D9828C95D136018D039AB0002F07D883992543C44D53B475A761BDC68943A745DF815E0FF55915DBE1666DD75363A0B1FF69D789581A652AB8F796B237AB56A38020F5D66166EA4A8CCEBA13ACF6A111A3518176D0A3D1F9D2606CA51AB83F7BDB9D8F763421FC62D3143619568F9EF66FB6997330B2584169D3B69DF079809B0FBE1981B3E83A77AA342B112528CE403DAC8C08E378A8D856ABDBAA028D91D750CE1F2BC1C20857AF06680EDA1897E14199F7F541769DDF115E8045"
VideoUseSync=0
Filtering=0

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

[Wizard]
Section0Cb=Simplex Truchet Tubing
Section1Cb=Vignette
Section2Cb=Peak Shape rotating full circle
Section3Cb=Tech showdown
Macro0=Song Title
Macro1=Song Author
Macro2=Song made with love and time

