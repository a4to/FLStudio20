FLhd   0 * ` FLdt�8  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��pQ8  ﻿[General]
GlWindowMode=1
LayerCount=45
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=4,3,5,2,11,9,8,7,14,13,12,10,19,46,25,28,27,43,41,42,39,40,37,1,38,48,47,56,0,15,20,44,45,22,26,32,29,36,31,33,30,6,54,55,58

[AppSet4]
App=Background\SolidColor
FParamValues=0,0,0,1
ParamValues=0,0,0,1000
ParamValuesF=0,0,0,1
Enabled=1

[AppSet3]
App=HUD\HUD Prefab
FParamValues=20,0.2141,0.5,0,0,0.48,0.5,0.149,1,1,4,0,0.518,1,0.368,0,1,0
ParamValues=20,214,500,0,0,480,500,149,1000,1000,4,0,518,1,368,0,1000,0
ParamValuesF=13,0.1177,0.5,0,0,0.48,0.5,0.149,1,1,4,0,0.33,1,0.368,0,1,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C8CF53273CA18863D00000A9F0A9E

[AppSet5]
App=Misc\Automator
FParamValues=1,4,13,3,1,0.036,0.25,1,4,2,2,0.5,0.036,0.25,0,4,10,3,0,0.036,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,4,13,3,1000,36,250,1,4,2,2,500,36,250,0,4,10,3,0,36,250,0,0,0,0,0,250,250
ParamValuesF=1,4,13,3,1,0.036,0.25,1,4,2,2,0.5,0.036,0.25,0,4,10,3,0,0.036,0.25,0,0,0,0,0,0.25,0.25
Enabled=1

[AppSet2]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.037,0,0.407,1,1,0,0,0
ParamValues=37,0,407,1000,1000,0,0,0
ParamValuesF=0.037,0,0.407,1,1,0,0,0
Enabled=1
UseBufferOutput=1

[AppSet11]
App=Background\SolidColor
FParamValues=0,0,0,1
ParamValues=0,0,0,1000
ParamValuesF=0,0,0,1
Enabled=1

[AppSet9]
App=HUD\HUD Prefab
FParamValues=225,0.4971,0.5,0,0,0.48,0.5,0.149,1,1,4,0,0.873,1,0.368,0,1,0
ParamValues=225,497,500,0,0,480,500,149,1000,1000,4,0,873,1,368,0,1000,0
ParamValuesF=218,0.9938,0.5,0,0,0.48,0.5,0.149,1,1,4,0,0.262,1,0.368,0,1,1
Enabled=1
LayerPrivateData=78012B28CA4F2F4A2D2E56484A2C2A8E0112BA0606867A9939650C23030000A1AD0906

[AppSet8]
App=Misc\Automator
FParamValues=1,10,13,3,0,0.032,0.75,1,10,2,2,0.5,0.069,0.25,0,4,10,3,0,0.036,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,10,13,3,0,32,750,1,10,2,2,500,69,250,0,4,10,3,0,36,250,0,0,0,0,0,250,250
ParamValuesF=1,10,13,3,0,0.032,0.75,1,10,2,2,0.5,0.069,0.25,0,4,10,3,0,0.036,0.25,0,0,0,0,0,0.25,0.25
Enabled=1

[AppSet7]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.037,0,0.407,1,1,0,0,0
ParamValues=37,0,407,1000,1000,0,0,0
ParamValuesF=0.037,0,0.407,1,1,0,0,0
Enabled=1
UseBufferOutput=1

[AppSet14]
App=Background\SolidColor
FParamValues=0,0,0,1
ParamValues=0,0,0,1000
ParamValuesF=0,0,0,1
Enabled=1

[AppSet13]
App=HUD\HUD Prefab
FParamValues=249,0.2141,0.5,0,0,0.48,0.5,1,1,1,4,0,0.436,1,0.368,0,1,0
ParamValues=249,214,500,0,0,480,500,1000,1000,1000,4,0,436,1,368,0,1000,0
ParamValuesF=242,0.1177,0.5,0,0,0.48,0.5,1,1,1,4,0,0.631,1,0.368,0,1,1
Enabled=1
LayerPrivateData=78012BCE4DCCC95148CD49CD4DCD2B298E49CE2C4ACE498552BA0606267A9939650CC31800008D580D6D

[AppSet12]
App=Misc\Automator
FParamValues=1,14,13,3,0,0.016,0.75,1,14,2,2,0.5,0.036,0.25,0,4,10,3,0,0.036,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,14,13,3,0,16,750,1,14,2,2,500,36,250,0,4,10,3,0,36,250,0,0,0,0,0,250,250
ParamValuesF=1,14,13,3,0,0.016,0.75,1,14,2,2,0.5,0.036,0.25,0,4,10,3,0,0.036,0.25,0,0,0,0,0,0.25,0.25
Enabled=1

[AppSet10]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.037,0,0.407,1,1,0,0,0
ParamValues=37,0,407,1000,1000,0,0,0
ParamValuesF=0.037,0,0.407,1,1,0,0,0
Enabled=1
UseBufferOutput=1

[AppSet19]
App=Image effects\ImageTileSprite
FParamValues=7,7,0,0,0,0,5,0.25,0.25,0.892,0.5,0.044,0.5,0,0,2,0.351
ParamValues=7,7,0,0,0,0,5,250,250,892,500,44,500,0,0,2,351
ParamValuesF=7,7,0,0,0,0,5,0.25,0.25,0.892,0.5,0.044,0.5,0,0,2,0.7267
Enabled=1

[AppSet46]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.672,0.5,0.5,0.5
ParamValues=500,500,672,500,500,500
ParamValuesF=0.5,0.5,0.672,0.5,0.5,0.5
Enabled=1
UseBufferOutput=1

[AppSet25]
App=Canvas effects\TaffyPulls
FParamValues=0.5,0,0,0,0,0.5,1,0,0.5,0.5,0,0,0,0.5
ParamValues=500,0,0,0,0,500,1000,0,500,500,0,0,0,500
ParamValuesF=0.5,0,0,0,0,0.5,1,0,0.5,0.5,0,0,0,0.5
Enabled=1
MeshIndex=21

[AppSet28]
App=Postprocess\Youlean Color Correction
FParamValues=1,0.5,0.5,0.5,0.5,0.5
ParamValues=1000,500,500,500,500,500
ParamValuesF=1,0.5,0.5,0.5,0.5,0.5
Enabled=1

[AppSet27]
App=Postprocess\Vignette
FParamValues=0,0,0,0,0.599,0
ParamValues=0,0,0,0,599,0
ParamValuesF=0,0,0,0,0.599,0
Enabled=1
UseBufferOutput=1

[AppSet43]
App=Background\SolidColor
FParamValues=0,0,0,0.848
ParamValues=0,0,0,848
ParamValuesF=0,0,0,0.848
Enabled=1

[AppSet41]
App=Misc\Automator
FParamValues=1,40,2,3,1,0.032,0.25,1,41,2,3,1,0.06,0.25,1,38,2,3,1,0.04,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,40,2,3,1000,32,250,1,41,2,3,1000,60,250,1,38,2,3,1000,40,250,0,0,0,0,0,250,250
ParamValuesF=1,40,2,3,1,0.032,0.25,1,41,2,3,1,0.06,0.25,1,38,2,3,1,0.04,0.25,0,0,0,0,0,0.25,0.25
Enabled=1

[AppSet42]
App=Misc\Automator
FParamValues=0,40,19,0,0,0.25,0.25,0,41,19,0,0,0.25,0.25,1,38,19,2,0.66,0.578,0.422,0,0,0,0,0,0.25,0.25
ParamValues=0,40,19,0,0,250,250,0,41,19,0,0,250,250,1,38,19,2,660,578,422,0,0,0,0,0,250,250
ParamValuesF=0,40,19,0,0,0.25,0.25,0,41,19,0,0,0.25,0.25,1,38,19,2,0.66,0.578,0.422,0,0,0,0,0,0.25,0.25
Enabled=1

[AppSet39]
App=HUD\HUD Grid
AppVersion=1
FParamValues=1,0.127,1,0,0.5,0.5,1,1,1,4,0.5,1,0.5,0.648,0,0.5,1,0.1,0.72,0.3,0,0
ParamValues=1000,127,1000,0,500,500,1000,1000,1000,4,500,1000,500,648,0,500,1000,100,720,300,0,0
ParamValuesF=1,0.738,1,0,0.5,0.5,1,1,1,4,0.5,1,0.5,0.648,0,0.5,1,0.1,0.72,0.3,0,1
Enabled=1

[AppSet40]
App=HUD\HUD Grid
AppVersion=1
FParamValues=0,0.864,1,0,0.5,0.5,1,1,1,4,0.5,1,0.5,0.816,0,0.5,1,0.1,0.5,0.3,0,0
ParamValues=0,864,1000,0,500,500,1000,1000,1000,4,500,1000,500,816,0,500,1000,100,500,300,0,0
ParamValuesF=0,0.884,1,0,0.5,0.5,1,1,1,4,0.5,1,0.5,0.816,0,0.5,1,0.1,0.5,0.3,0,1
Enabled=1

[AppSet37]
App=HUD\HUD Grid
AppVersion=1
FParamValues=0,0.909,1,0,0.5,0.5,1,1,1,4,0.5,1,0.5,1,0,0.5,1,0.1,0.6505,0.3,0,0
ParamValues=0,909,1000,0,500,500,1000,1000,1000,4,500,1000,500,1000,0,500,1000,100,650,300,0,0
ParamValuesF=0,0.923,1,0,0.5,0.5,1,1,1,4,0.5,1,0.5,1,0,0.5,1,0.1,0.7543,0.3,0,1
Enabled=1
UseBufferOutput=1

[AppSet1]
App=Background\SolidColor
FParamValues=0,0,0,0.912
ParamValues=0,0,0,912
ParamValuesF=0,0,0,0.912
Enabled=1

[AppSet38]
App=HUD\HUD 3D
FParamValues=0,0,0.5,0.472,0.5,0.5,0.657,0.388,0.304,0.5,0.5,0.801,0.715,0.468,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=0,0,500,472,500,500,657,388,304,500,500,801,715,468,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
ParamValuesF=0,0,0.5,0.472,0.5,0.5,0.657,0.388,0.304,0.5,0.5,0.801,0.715,0.468,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
Enabled=1
ImageIndex=5

[AppSet48]
App=HUD\HUD 3D
FParamValues=0.58,0,0.5,0.472,0.5,0.5,0.513,0,0.496,0.5,0.5,0.801,0.715,0.468,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=580,0,500,472,500,500,513,0,496,500,500,801,715,468,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
ParamValuesF=0.58,0,0.5,0.472,0.5,0.5,0.513,0,0.496,0.5,0.5,0.801,0.715,0.468,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
Enabled=1
ImageIndex=5

[AppSet47]
App=HUD\HUD 3D
FParamValues=0,0,0.5,0.228,0.5,0.5,0.513,0.388,0.26,0.5,0.5,0.801,0.715,0.468,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=0,0,500,228,500,500,513,388,260,500,500,801,715,468,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
ParamValuesF=0,0,0.5,0.228,0.5,0.5,0.513,0.388,0.26,0.5,0.5,0.801,0.715,0.468,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
Enabled=1
ImageIndex=5

[AppSet56]
App=Peak Effects\Polar
FParamValues=0.788,0,0,0,0.584,0.5,1,0.5,0.5,0.5,0.5,0.396,0,0.5,0,0.5,0,1,1,0,0,0,0,1
ParamValues=788,0,0,0,584,500,1000,500,500,500,500,396,0,500,0,500,0,1000,1000,0,0,0,0,1
ParamValuesF=0.788,0,0,0,0.584,0.5,1,0.5,0.5,0.5,0.5,0.396,0,0.5,0,0.5,0,1,1,0,0,0,0,1
Enabled=1
ImageIndex=5

[AppSet0]
App=Object Arrays\CrystalCube
FParamValues=0,0.196,0.868,0,0.646,0.5,0.696,0.144
ParamValues=0,196,868,0,646,500,696,144
ParamValuesF=0,0.196,0.868,0,0.646,0.5,0.696,0.144
ParamValuesObject Arrays\8x8x8_Eggs=0,0,0,772,0,500,500,200,500,500,0,0,0
ParamValuesMisc\PentUp=0,0,500,0,0,576,556,240,0,0,0,544
Enabled=1
ImageIndex=1
MeshIndex=21

[AppSet15]
App=Object Arrays\CrystalCube
FParamValues=0,0.468,0.88,0,0.538,0.5,0.736,0.144
ParamValues=0,468,880,0,538,500,736,144
ParamValuesF=0,0.468,0.88,0,0.538,0.5,0.736,0.144
Enabled=1
ImageIndex=3
MeshIndex=21

[AppSet20]
App=Postprocess\ParameterShake
FParamValues=1,0.108,18,16,1,0.112,19,16,4,3,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=1000,108,18,16,1000,112,19,16,4,3,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=1,0.108,18,16,1,0.112,19,16,4,3,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet44]
App=Background\FogMachine
FParamValues=0.852,0,0,0.5,0.5,0,1,0.5,0.5,0.5,0,0
ParamValues=852,0,0,500,500,0,1000,500,500,500,0,0
ParamValuesF=0.852,0,0,0.5,0.5,0,1,0.5,0.5,0.5,0,0
Enabled=1
ImageIndex=7

[AppSet45]
App=Background\FogMachine
FParamValues=0.828,0,0,0.5,0.5,1,1,0.5,0.5,0.5,0,0
ParamValues=828,0,0,500,500,1000,1000,500,500,500,0,0
ParamValuesF=0.828,0,0,0.5,0.5,1,1,0.5,0.5,0.5,0,0
Enabled=1
ImageIndex=7

[AppSet22]
App=Misc\Automator
FParamValues=1,19,2,3,1,0.022,0.25,1,22,2,3,1,0.03,0.25,1,35,2,3,1,0.037,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,19,2,3,1000,22,250,1,22,2,3,1000,30,250,1,35,2,3,1000,37,250,0,0,0,0,0,250,250
ParamValuesF=1,19,2,3,1,0.022,0.25,1,22,2,3,1,0.03,0.25,1,35,2,3,1,0.037,0.25,0,0,0,0,0,0.25,0.25
Enabled=1

[AppSet26]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.628,0.398,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,628,398,0,0,0,0,0,0,0
ParamValuesF=0,0,0,0,1,0.628,0.398,0,0,0,0,0,0,0
Enabled=1
ImageIndex=4

[AppSet32]
App=Image effects\Image
FParamValues=0.2421,0.188,1,0,1,0.628,0.398,0,0,0,0,0,0,0
ParamValues=242,188,1000,0,1000,628,398,0,0,0,0,0,0,0
ParamValuesF=0.6514,0.512,1,0,1,0.628,0.398,0,0,0,0,0,0,0
Enabled=1
ImageIndex=4

[AppSet29]
App=Image effects\Image
FParamValues=0,0.484,0,0,1,0.366,0.398,0,0,0,0,0,0,0
ParamValues=0,484,0,0,1000,366,398,0,0,0,0,0,0,0
ParamValuesF=0,0.484,0,0,1,0.366,0.398,0,0,0,0,0,0,0
Enabled=1
ImageIndex=4

[AppSet36]
App=Image effects\Image
FParamValues=0,0.484,0,0,1,0.501,0.398,0,0,0,0,0,0,0
ParamValues=0,484,0,0,1000,501,398,0,0,0,0,0,0,0
ParamValuesF=0,0.484,0,0,1,0.501,0.398,0,0,0,0,0,0,0
Enabled=1
ImageIndex=4

[AppSet31]
App=Image effects\Image
FParamValues=0.2421,0.036,1,0,1,0.366,0.398,0,0,0,0,0,0,0
ParamValues=242,36,1000,0,1000,366,398,0,0,0,0,0,0,0
ParamValuesF=0.6514,0.66,1,0,1,0.366,0.398,0,0,0,0,0,0,0
Enabled=1
ImageIndex=4

[AppSet33]
App=Misc\Automator
FParamValues=1,33,2,3,1,0.071,0.25,1,32,2,3,1,0.072,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,33,2,3,1000,71,250,1,32,2,3,1000,72,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
ParamValuesF=1,33,2,3,1,0.071,0.25,1,32,2,3,1,0.072,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
Enabled=1

[AppSet30]
App=Postprocess\ParameterShake
FParamValues=1,0,32,0,1,0,31,0,1,1,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=1000,0,32,0,1000,0,31,0,1,1,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=1,0,32,0,1,0,31,0,1,1,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet6]
App=Object Arrays\CrystalCube
FParamValues=0,0.752,0.88,0,0.87,0.5,0.56,0.144
ParamValues=0,752,880,0,870,500,560,144
ParamValuesF=0,0.752,0.88,0,0.87,0.5,0.56,0.144
Enabled=1
ImageIndex=2
MeshIndex=21

[AppSet54]
App=Postprocess\ParameterShake
FParamValues=1,0,53,16,1,0,52,16,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=1000,0,53,16,1000,0,52,16,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=1,0,53,16,1,0,52,16,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet55]
App=Postprocess\ParameterShake
FParamValues=1,0,53,2,1,0,52,2,3,3,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=1000,0,53,2,1000,0,52,2,3,3,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=1,0,53,2,1,0,52,2,3,3,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet58]
App=Postprocess\ParameterShake
FParamValues=1,0,57,2,1,0,49,2,3,3,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=1000,0,57,2,1000,0,49,2,3,3,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=1,0,57,2,1,0,49,2,3,3,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position y=""10""><p align=""center""><font face=""American-Captain"" size=""9"" color=""#FFFFFF"">[author]</font></p></position>","<position y=""20""><p align=""center""><font face=""Chosence-Bold"" size=""7"" color=""#FFFFFF"">[title]</font></p></position>","<position y=""53""><p align=""center""><font face=""Chosence-Bold"" size=""5"" color=""#5e5e5e"">[extra1]</font></p></position>","<position y=""92""><p align=""center""><font face=""Chosence-Bold"" size=""4"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position y=""71""><p align=""center""><font face=""Chosence-Bold"" size=""4"" color=""#5e5e5e"">[extra2]</font></p></position>","<position y=""76""><p align=""center""><font face=""Chosence-regular"" size=""4"" color=""#5e5e5e"">[extra3]</font></p></position>",
Meshes=[plugpath]Content\Meshes\Brain.zgeMesh,[plugpath]Content\Meshes\Car.zgeMesh,[plugpath]Content\Meshes\CircularPlane.zgeMesh,[plugpath]Content\Meshes\Cone(Textue).zgeMesh,[plugpath]Content\Meshes\Cone.zgeMesh,[plugpath]Content\Meshes\Cube.zgeMesh,[plugpath]Content\Meshes\Cylinder(Texture).zgeMesh,[plugpath]Content\Meshes\Cylinder.zgeMesh,[plugpath]Content\Meshes\Drone.zgeMesh,[plugpath]Content\Meshes\Hero.zgeMesh,[plugpath]Content\Meshes\Heroine.zgeMesh,[plugpath]Content\Meshes\Monkey.zgeMesh,[plugpath]Content\Meshes\Plane.zgeMesh,[plugpath]Content\Meshes\Skull.zgeMesh,[plugpath]Content\Meshes\Sphere(Ico).zgeMesh,[plugpath]Content\Meshes\Sphere(Texture).zgeMesh,[plugpath]Content\Meshes\Sphere.zgeMesh,[plugpath]Content\Meshes\Sphere.zgeMesh,"[plugpath]Content\Meshes\Spheres 1.zgeMesh",[plugpath]Content\Meshes\Torus.zgeMesh,"[plugpath]Content\Meshes\World map.zgeMesh"
Images=[presetpath]Wizard\Assets\Sacco\Dancer.png
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

