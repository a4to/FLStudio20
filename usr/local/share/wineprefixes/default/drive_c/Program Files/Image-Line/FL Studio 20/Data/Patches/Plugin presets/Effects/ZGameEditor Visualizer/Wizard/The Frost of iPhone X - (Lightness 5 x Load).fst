FLhd   0 * ` FLdt�  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��<o  ﻿[General]
GlWindowMode=1
LayerCount=20
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,9,8,17,10,12,15,11,6,1,7,16,19,14,4,2,3,5,18,13
WizardParams=659

[AppSet0]
App=Background\SolidColor
FParamValues=0,0.564,0.84,0.904
ParamValues=0,564,840,904
Enabled=1
Collapsed=1
Name=Background FX

[AppSet9]
App=Canvas effects\DarkSpark
FParamValues=0.5,0,1,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0,0
ParamValues=500,0,1000,500,500,500,500,500,500,500,0,0
ParamValuesBackground\SolidColor=0,564,840,904
Enabled=1
Collapsed=1
ImageIndex=7
Name=Nice Effect

[AppSet8]
App=Postprocess\Youlean Motion Blur
FParamValues=0.472,1,0.636,0.956,0,0,0
ParamValues=472,1,636,956,0,0,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesHUD\HUD Text=44,500,0,0,652,520,500,552,1000,1000,324,1,240,0,0,750,1000,536,504,1000
ParamValuesHUD\HUD Grid=148,144,0,0,500,500,1000,1000,1000,444,500,1000,500,888,232,500,1000,0,784,0,392,1000
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPostprocess\Youlean Bloom=548,708,326,282
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesObject Arrays\Filaments=1000,0,0,1000,500,500,500,500,500,500,0,0,500,0
ParamValuesObject Arrays\CubicMatrix=0,0,0,1000,932,500,500,500,500,0,0,0,0
ParamValuesObject Arrays\CubesGrasping=0,0,1000,1000,500,500,500,500,500,500,500
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,15,500,500,500,500,500
ParamValuesPostprocess\Blur=1000
ParamValuesPostprocess\Youlean Blur=500,1000,0
ParamValuesHUD\HUD Free Line=0,500,0,964,660,492,388,304,182,92,182,220,1000,312,0,272,500
ParamValuesPostprocess\Youlean Color Correction=520,392,792,500,1000,136
Enabled=1
Collapsed=1
ImageIndex=7
Name=Grid FX

[AppSet17]
App=Background\FourCornerGradient
FParamValues=7,0.504,0,1,1,0.856,1,1,0,1,1,0.852,1,1
ParamValues=7,504,0,1000,1000,856,1000,1000,0,1000,1000,852,1000,1000
ParamValuesHUD\HUD Meter Linear=0,208,884,1000,0,0,0,824,128,753,224,68,0,11,500,0,584,560,404,364,568,1000
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=Filter Color

[AppSet10]
App=HUD\HUD Prefab
FParamValues=0,0.092,0.5,0,0.036,0.502,0.65,0.286,1,1,4,0,0.5,1,0.368,0.1,1,1
ParamValues=0,92,500,0,36,502,650,286,1000,1000,4,0,500,1,368,100,1000,1
ParamValuesHUD\HUD Graph Radial=0,500,0,0,200,500,250,444,500,0,1000,0,1000,0,250,500,200,0,0,0,500,1000
ParamValuesHUD\HUD Meter Radial=0,248,1000,0,0,0,0,992,492,708,156,34,1000,0,374,0,1000,1000,500,1000
ParamValuesHUD\HUD Free Line=0,500,0,0,656,552,168,304,182,92,182,168,1000,312,0,280,500
ParamValuesHUD\HUD Graph Linear=0,500,0,0,816,468,1000,1000,250,444,500,1000,212,1000,0,500,200,0,0,0,500,1000
ParamValuesHUD\HUD Meter Linear=0,500,0,0,0,0,750,0,800,664,300,100,0,125,500,1,238,0,0,1000,612,1000
Enabled=1
UseBufferOutput=1
Name=LOGO
LayerPrivateData=780173C8CBCF4BD52B2E4B671899000060F9036F

[AppSet12]
App=Background\SolidColor
FParamValues=0,0.524,0,0
ParamValues=0,524,0,0
Enabled=1
Collapsed=1
Name=Background Main

[AppSet15]
App=HUD\HUD Prefab
FParamValues=7,0.756,0.5,0,0.484,0.5,0.5,0.3,1,1,4,0,0.5,0,0.444,0,1,1
ParamValues=7,756,500,0,484,500,500,300,1000,1000,4,0,500,0,444,0,1000,1
Enabled=1
Collapsed=1
Name=Border BG
LayerPrivateData=78014B4A4CCE4E2FCA2FCD4B89490232750D0CCCF53273CA18460A0000F5E0084B

[AppSet11]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0.14,0.056,1,1,0.5,0.357,1,0.284,1,4,0.5,1,0.284,0.644,0,0.784,0.076,0.244,0.312,2,0,1
ParamValues=140,56,1000,1000,500,357,1000,284,1000,4,500,1,284,644,0,784,76,244,312,2,0,1
ParamValuesHUD\HUD Free Line=0,500,0,0,656,552,168,304,182,92,182,168,1000,312,0,280,500
Enabled=1
Collapsed=1
Name=EQ UP

[AppSet6]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0.14,0.536,1,1,0.5,0.641,1,0.284,1,4,1,1,0.284,0.644,0,0.784,0.076,0.244,0.312,2,0,1
ParamValues=140,536,1000,1000,500,641,1000,284,1000,4,1000,1,284,644,0,784,76,244,312,2,0,1
ParamValuesHUD\HUD Text=60,500,0,1000,660,686,500,552,1000,1000,324,1,240,0,0,750,1000,536,504,1000
Enabled=1
Collapsed=1
Name=EQ DOWN

[AppSet1]
App=Image effects\Image
FParamValues=0,0,0,0,0.708,0.5,0.5,0.042,0,0.25,0,0,0,0
ParamValues=0,0,0,0,708,500,500,42,0,250,0,0,0,0
ParamValuesImage effects\ImageWall=0,0,0,0,0,0,0
ParamValuesHUD\HUD Image=0,0,500,500,1000,1000,500,444,500,0,0,1000,1000,500,1000
ParamValuesBackground\SolidColor=0,892,1000,124
ParamValuesImage effects\ImageSlices=0,0,0,0,500,496,488,0,0,0,500,269,1000,500,0,0,0
Enabled=1
Collapsed=1
ImageIndex=6
Name=LCD - Mask

[AppSet7]
App=Text\TextTrueType
FParamValues=0.08,0,0,0,0,0.493,0.5,0,0,0,0.5
ParamValues=80,0,0,0,0,493,500,0,0,0,500
Enabled=1
Name=Main Text

[AppSet16]
App=Image effects\Image
FParamValues=0.344,0,0,1,1,0.5,0.501,0,0,0,0,0,0,0
ParamValues=344,0,0,1000,1000,500,501,0,0,0,0,0,0,0
Enabled=1
ImageIndex=5

[AppSet19]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=5

[AppSet14]
App=Image effects\Image
FParamValues=0,0,0,0.484,0.946,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,484,946,500,500,0,0,0,0,0,0,0
ParamValuesHUD\HUD Grid=0,1000,1000,0,500,500,1000,1000,1000,444,500,1000,500,888,232,500,1000,0,784,0,392,1000
Enabled=1
ImageIndex=1
Name=iPhone - Buttons

[AppSet4]
App=Image effects\Image
FParamValues=0.8,0,0,0,0.952,0.5,0.5,0,0.004,0,0,0,0,0
ParamValues=800,0,0,0,952,500,500,0,4,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=3
Name=iPhone - Glass

[AppSet2]
App=Image effects\Image
FParamValues=0,0,0,0.92,0.944,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,920,944,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=4
Name=iPhone - Border 1

[AppSet3]
App=Image effects\Image
FParamValues=0,0,0,0.852,0.946,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,852,946,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
Name=iPhone - Border 2

[AppSet5]
App=Image effects\Image
FParamValues=0,0,0,0,0.946,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,946,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=2
Name=iPhone - FaceID

[AppSet18]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fade-in and out

[AppSet13]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""3"" y=""2""><p align=""left""><font face=""American-Captain"" size=""8"" color=""#333333"">[author]</font></p></position>","<position x=""3"" y=""10""><p align=""left""><b><font face=""Chosence-Bold"" size=""6"" color=""#333333"">[title]</font></b></p></position>","<position y=""92""><p align=""right""><b><font face=""Chosence-Bold"" size=""3"" color=""#333333"">[comment]</font></b></p></position>",,," ",,,
Images=[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Border-1.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Buttons.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Face-id.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Glass.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Border-6.svg
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

