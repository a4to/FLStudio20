FLhd   0 * ` FLdt2(  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ըO�'  ﻿[General]
GlWindowMode=1
LayerCount=24
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=9,5,0,15,16,20,11,10,12,14,13,22,23,7,1,4,3,2,8,6,19,17,18,21
WizardParams=1043

[AppSet9]
App=Canvas effects\Electric
FParamValues=0,0.74,0.484,0.268,0.032,0.52,0.644,0.148,0
ParamValues=0,740,484,268,32,520,644,148,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,570,500,500,500,500,500
ParamValuesHUD\HUD Grid=0,500,0,0,500,500,1000,1000,916,444,500,1000,500,520,172,500,1000,40,684,184,0,1000
Enabled=1
Collapsed=1
Name=Background FX

[AppSet5]
App=Background\FourCornerGradient
FParamValues=3,0.852,0.14,1,1,0.334,1,1,0.728,1,1,0.314,1,1
ParamValues=3,852,140,1000,1000,334,1000,1000,728,1000,1000,314,1000,1000
ParamValuesImage effects\Image=0,0,0,0,950,500,500,0,0,0,0
ParamValuesPostprocess\Youlean Bloom=388,252,542,486
Enabled=1
Collapsed=1
Name=Color HUE

[AppSet0]
App=HUD\HUD Prefab
FParamValues=223,0,0.5,0,1,0.5,0.5,0.656,1,1,4,0,0.5,1,1,0,0.94,1
ParamValues=223,0,500,0,1000,500,500,656,1000,1000,4,0,500,1,1000,0,940,1
ParamValuesBackground\SolidColor=0,0,1000,124
Enabled=1
Collapsed=1
Name=EFX
LayerPrivateData=78012B48CC4BCD298E290051BA06C6067A9939650C23080000E9F20729

[AppSet15]
App=Image effects\Image
FParamValues=0,0,0,1,0.764,0.5,0.5,0,1,0,0,0,0,0
ParamValues=0,0,0,1000,764,500,500,0,1000,0,0,0,0,0
ParamValuesImage effects\ImageMasked=0,0,0,1000,456,500,500,200,200,0,500
ParamValuesHUD\HUD Prefab=216,0,500,0,1000,500,500,656,1000,1000,444,0,500,1000,1000,0,940,1000
ParamValuesImage effects\ImageSlices=0,0,0,1000,760,500,500,0,0,0,500,333,0,500,0,0,0
Enabled=1
Collapsed=1
ImageIndex=8
Name=Killer

[AppSet16]
App=Postprocess\Point Cloud Low
FParamValues=0,0.33,0.33,0.5,0.5,0.448,0.444,0.503,0.33,0.625,0,0,0,0,0
ParamValues=0,330,330,500,500,448,444,503,330,625,0,0,0,0,0
ParamValuesHUD\HUD Prefab=31,0,500,0,852,500,856,1000,1000,1000,444,0,1000,1000,200,0,1000,1000
ParamValuesParticles\ColorBlobs=0,0,0,0,376,500,500,350,1000,148
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesHUD\HUD Text=0,500,0,0,168,344,500,0,0,0,0,0,100,0,0,750,1000,500,500,1000
ParamValuesPeak Effects\Polar=0,0,0,0,188,36,572,228,1000,856,596,448,0,500,0,500,0,1000,1000,1000,320,480,1000,1000
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesParticles\fLuids=0,0,0,0,500,0,500,0,0,0,500,500,500,0,500,250,500,0,0,500,500,500,0,0,500,0,0,250,500,0,0,0
ParamValuesParticles\PlasmaFlys=500,500,500,0,1000,500,500,500,0,500,500,500,500,500
ParamValuesFeedback\BoxedIn=0,0,0,1000,500,48,228,500,0,500
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPostprocess\ScanLines=0,0,0,0,0,0
ParamValuesHUD\HUD Meter Linear=0,208,884,1000,0,0,0,824,128,753,224,68,0,11,500,0,584,560,404,364,568,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesFeedback\70sKaleido=0,0,212,1000,208,500
ParamValuesPeak Effects\Stripe Peeks=0,200,1000,1000,0,0,86,264,500,0,250,500,612,0,500,250,200,0,150,1000,1000,300,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,746,500,500,500,500,500
ParamValuesHUD\HUD 3D=0,0,500,500,500,500,500,580,500,500,500,1000,1000,500,1000,1000,0,0,1000,1000,500
Enabled=1
Collapsed=1
ImageIndex=8

[AppSet20]
App=Feedback\70sKaleido
FParamValues=0,0,0.444,1,0.072,0.328
ParamValues=0,0,444,1000,72,328
ParamValuesPostprocess\Point Cloud High=0,678,786,596,484,656,476,491,486,836,156,0,1000,0,0
ParamValuesPostprocess\Youlean Bloom=456,252,0,454
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=Bloomer

[AppSet11]
App=Peak Effects\PeekMe
FParamValues=0.012,0.176,1,0,0.032,0.528,0.384,0,0,0.048,0.124,0.18
ParamValues=12,176,1000,0,32,528,384,0,0,48,124,180
ParamValuesHUD\HUD Graph Linear=0,0,1000,0,796,363,1000,1000,258,444,500,1000,404,224,0,864,0,280,0,333,1000,1000
ParamValuesHUD\HUD Free Line=0,500,0,0,656,552,168,304,182,92,182,168,1000,312,0,280,500
ParamValuesPeak Effects\JoyDividers=0,312,616,0,204,0,628,44,724,252,604,722,0
Enabled=1
UseBufferOutput=1
Collapsed=1
ImageIndex=8
Name=EQ

[AppSet10]
App=HUD\HUD Prefab
FParamValues=0,0,0.5,0,0,0.502,0.65,0.25,1,1,4,0,0.5,1,0.368,0.099,1,1
ParamValues=0,0,500,0,0,502,650,250,1000,1000,4,0,500,1,368,99,1000,1
ParamValuesHUD\HUD Graph Radial=0,500,0,0,200,500,250,444,500,0,1000,0,1000,0,250,500,200,0,0,0,500,1000
ParamValuesHUD\HUD Meter Radial=0,248,1000,0,0,0,0,992,492,708,156,34,1000,0,374,0,1000,1000,500,1000
ParamValuesHUD\HUD Free Line=0,500,0,0,656,552,168,304,182,92,182,168,1000,312,0,280,500
ParamValuesHUD\HUD Graph Linear=0,500,0,0,816,468,1000,1000,250,444,500,1000,212,1000,0,500,200,0,0,0,500,1000
ParamValuesHUD\HUD Meter Linear=0,500,0,0,0,0,750,0,800,664,300,100,0,125,500,1,238,0,0,1000,612,1000
Enabled=1
UseBufferOutput=1
Name=LOGO
LayerPrivateData=780173C8CBCF4BD52B2E4B671899000060F9036F

[AppSet12]
App=Background\SolidColor
FParamValues=0,0.728,0.14,0.94
ParamValues=0,728,140,940
Enabled=1
Collapsed=1
Name=Background Main

[AppSet14]
App=HUD\HUD Grid
AppVersion=1
FParamValues=0,0.5,0,0.7,0.18,0.48,1,0.988,0.308,4,0.5,1,0.5,0.452,0.24,0.5,1,0.08,0.636,0,0,1
ParamValues=0,500,0,700,180,480,1000,988,308,4,500,1000,500,452,240,500,1000,80,636,0,0,1
ParamValuesHUD\HUD Prefab=31,0,500,0,852,500,148,1000,1000,1000,444,0,500,1000,200,0,1000,1000
ParamValuesBackground\SolidColor=0,792,760,120
Enabled=1
Collapsed=1
Name=Glow BG

[AppSet13]
App=Image effects\Image
FParamValues=0,0,0,0,0.709,0.5,0.501,0.042,0,0.25,0,0,0,0
ParamValues=0,0,0,0,709,500,501,42,0,250,0,0,0,0
ParamValuesImage effects\ImageWall=0,0,0,0,0,0,0
ParamValuesHUD\HUD Image=0,0,500,500,1000,1000,500,444,500,0,0,1000,1000,500,1000
ParamValuesBackground\SolidColor=0,892,1000,124
ParamValuesImage effects\ImageSlices=0,0,0,0,500,496,488,0,0,0,500,269,1000,500,0,0,0
Enabled=1
Collapsed=1
ImageIndex=7
Name=LCD - Mask

[AppSet22]
App=Image effects\Image
FParamValues=0.496,0,0,1,1,0.5,0.501,0,0,0,0,0,0,0
ParamValues=496,0,0,1000,1000,500,501,0,0,0,0,0,0,0
Enabled=1
ImageIndex=5

[AppSet23]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=5

[AppSet7]
App=Text\TextTrueType
FParamValues=0.08,0,0,0,0,0.493,0.5,0,0,0,0.5
ParamValues=80,0,0,0,0,493,500,0,0,0,500
Enabled=1
Name=Main Text

[AppSet1]
App=Image effects\Image
FParamValues=0,0,0,0.864,0.947,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,864,947,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=2
Name=iPhone - Buttons

[AppSet4]
App=Image effects\Image
FParamValues=0.92,0,0,0,0.952,0.501,0.5,0,0.004,0,0,0,0,0
ParamValues=920,0,0,0,952,501,500,0,4,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=4
Name=iPhone - Glass

[AppSet3]
App=Image effects\Image
FParamValues=0,0,0,0.94,0.972,0.5,0.514,0,0,0,0,0,0,0
ParamValues=0,0,0,940,972,500,514,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=iPhone - Border 2

[AppSet2]
App=Image effects\Image
FParamValues=0,0,0,0.872,0.947,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,872,947,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
Name=iPhone - Border 1

[AppSet8]
App=Image effects\Image
FParamValues=0,0,0,0,0.947,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,947,500,500,0,0,0,0,0,0,0
ParamValuesHUD\HUD Free Line=904,0,0,0,660,492,388,304,0,92,0,220,1000,312,0,272,500
ParamValuesHUD\HUD Text=44,500,0,0,652,520,500,552,1000,1000,324,1,240,0,0,750,1000,536,504,1000
Enabled=1
Collapsed=1
ImageIndex=3
Name=iPhone - Face ID

[AppSet6]
App=HUD\HUD Text
FParamValues=0.96,0.5,0,0,0.66,0.686,0.5,0.552,1,1,0.324,4,0.24,0,0,3,1,0.536,0.504,0.001,1,0,0,0,1
ParamValues=960,500,0,0,660,686,500,552,1,1000,324,4,240,0,0,3,1000,536,504,1,1000,0,0,0,1
Enabled=1
Collapsed=1
Name=End Indicator
LayerPrivateData=7801734C4ECD2BD62B2949631899000058F00367

[AppSet19]
App=Image effects\Image
FParamValues=0,0,0,0,0.944,0.648,0.444,0,0,0,0,0,0,0
ParamValues=0,0,0,0,944,648,444,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=6
Name=OMG EQ

[AppSet17]
App=Background\FourCornerGradient
FParamValues=5,0.192,0.084,0.316,0.856,0.084,0.316,0.856,0.084,0.316,0.856,0.084,0.316,0.856
ParamValues=5,192,84,316,856,84,316,856,84,316,856,84,316,856
ParamValuesPostprocess\ColorCyclePalette=0,0,0,0,0,264,560,288,396,500,336
ParamValuesHUD\HUD Meter Linear=0,208,884,1000,0,0,0,824,128,753,224,68,0,11,500,0,584,560,404,364,568,1000
Enabled=1
Collapsed=1
Name=Filter Color

[AppSet18]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fade-in and out

[AppSet21]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""3"" y=""4""><p align=""left""><font face=""American-Captain"" size=""8"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""3"" y=""12""><p align=""left""><b><font face=""Chosence-Bold"" size=""6"" color=""#FFFFFF"">[title]</font></b></p></position>","<position y=""87.1""><p align=""right""><b><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[comment]</font></b></p></position>",,,," ",,," ",,,
Images=[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Border-1.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Border-2.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Buttons.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Face-id.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Glass.svg
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

