<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" NoSound="1" FileVersion="2">
  <OnLoaded>
    <SpawnModel Model="ScreenModel"/>
    <ZLibrary Comment="HSV Library">
      <Source>
<![CDATA[vec3 hsv(float h, float s, float v)
{
  s = clamp(s/100, 0, 1);
  v = clamp(v/100, 0, 1);

  if(!s)return vector3(v, v, v);

  h = h < 0 ? frac(1-abs(frac(h/360)))*6 : frac(h/360)*6;

  float c, f, p, q, t;

  c = floor(h);
  f = h-c;

  p = v*(1-s);
  q = v*(1-s*f);
  t = v*(1-s*(1-f));

  switch(c)
  {
    case 0: return vector3(v, t, p);
    case 1: return vector3(q, v, p);
    case 2: return vector3(p, v, t);
    case 3: return vector3(p, q, v);
    case 4: return vector3(t, p, v);
    case 5: return vector3(v, p, q);
  }
}]]>
      </Source>
    </ZLibrary>
    <ZLibrary Comment="ZgeViz interface">
      <Source>
<![CDATA[const int
  ALPHA = 0,
  HUE = 1,
  SATURATION = 2,
  LIGHTNESS = 3;]]>
      </Source>
    </ZLibrary>
  </OnLoaded>
  <OnUpdate>
    <ZExpression Expression="ShaderColor=hsv(Parameters[HUE]*360,Parameters[SATURATION]*100,(1-Parameters[LIGHTNESS])*100);"/>
  </OnUpdate>
  <Content>
    <Group>
      <Children>
        <Array Name="Parameters" SizeDim1="10" Persistent="255">
          <Values>
<![CDATA[78DA636000827C217B0686062066B067B8F1D3F6EC191F3B20DF0ECCAFC8B46358206D0B00A2330904]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Alpha
Hue
Saturation
Lightness
FogDensity
LanternRadius
RoomRadius
Gamma
PanIntensity
PanSpeed]]>
          </StringValue>
        </Constant>
        <Constant Name="AuthorInfo" Type="2" StringValue="srtuss"/>
      </Children>
    </Group>
    <Material Name="ScreenMaterial" Shading="1" Light="0" Blend="1" ZBuffer="0" Shader="ScreenShader">
      <Textures>
        <MaterialTexture Texture="WallBitmap" TextureWrapMode="1" TexCoords="1"/>
        <MaterialTexture Texture="LightMaskBitmap" TextureWrapMode="1" TexCoords="1"/>
      </Textures>
    </Material>
    <Shader Name="ScreenShader">
      <VertexShaderSource>
<![CDATA[varying vec2 position;

void main(){
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
  position=vec2(vertex.x,vertex.y);
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[//Lantern by "srtuss" and Philip

uniform float iGlobalTime;
uniform float resX;
uniform float resY;
uniform float viewportX;
uniform float viewportY;

uniform float Alpha;

vec2 iResolution = vec2(resX,resY);

uniform sampler2D tex1;
#define wallTexture tex1

uniform sampler2D tex2;
#define lightMaskTexture tex2

uniform vec3 sceneColor;

vec3 fogColor = vec3(0.0,0.25,0.25);
uniform float fogDensity;   // 0 .. 3

uniform float panIntensity; // 0 .. 1
uniform float panSpeed;     // 0 (off) .. ~40
float panOffset=0.0;    // -1.5 .. 1.5

float spinSpeed=0.0;    // 0 .. 3
float spiralMapping=0.0;    // 0 or 1

uniform float roomRadius;    // 5.0
uniform float lanternRadius; // 0.2

float lightMaskShift=0.0; // 0.0

uniform float gamma; // 2.2

struct ITSC
{
    vec3 p;
    float dist;
    vec3 n;
};

vec2 rotate(vec2 p, float a)
{
    return vec2(p.x * cos(a) - p.y * sin(a), p.x * sin(a) + p.y * cos(a));
}

ITSC raycyl_i(vec3 ro, vec3 rd, vec3 c, float r)
{
    ro = ro.xzy;
    rd = rd.xzy;

    ITSC i;
    i.dist = 1e38;
    vec3 e = ro - c;
    float a = dot(rd.xy, rd.xy);
    float b = 2.0 * dot(e.xy, rd.xy);
    float cc = dot(e.xy, e.xy) - r;
    float f = b * b - 4.0 * a * cc;
    if(f > 0.0)
    {
        f = sqrt(f);
        float t = (-b + f) / (2.0 * a);

        if(t > 0.001)
        {
            i.dist = t;
            i.p = e + rd * t;
            i.n = -vec3(normalize(i.p.xy), 0.0).xzy;
            i.p = i.p.xzy;
        }
    }
    return i;
}

ITSC raycyl(vec3 ro, vec3 rd, vec3 c, float r)
{
    ro = ro.xzy;
    rd = rd.xzy;

    ITSC i;
    i.dist = 1e38;
    vec3 e = ro - c;
    float a = dot(rd.xy, rd.xy);
    float b = 2.0 * dot(e.xy, rd.xy);
    float cc = dot(e.xy, e.xy) - r;
    float f = b * b - 4.0 * a * cc;
    if(f > 0.0)
    {
        f = sqrt(f);
        float t = (-b - f) / (2.0 * a);

        if(t > 0.001)
        {
            i.dist = t;
            i.p = e + rd * t;
            i.n = -vec3(normalize(i.p.xy), 0.0).xzy;
            i.p = i.p.xzy;
        }
    }
    return i;
}

void raypln(inout ITSC i, vec3 ro, vec3 rd, vec3 n, float d)
{
    float t = -(dot(ro, n) - d) / dot(rd, n);

    if(t > 0.0 && t < i.dist)
    {
        i.dist = t;
        i.p = ro + rd * t;
        i.n = n;
    }
}

float field(vec3 p, float bias)
{
    if (spinSpeed>0.01) p.xz = rotate(p.xz, spinSpeed*iGlobalTime); // spin
    p.y += panIntensity*(length(p.xz) - 0.44) * sin(iGlobalTime * panSpeed + panOffset);  // light angle

    //p.xy /= p.z;
    if (spiralMapping < 0.5) p.x = atan(p.z, p.x); // circular mapping
    else p.x = atan(p.z, p.x) + length(p); // spiral mapping

    //todo: figure out perfect mapping!
    float v = texture2D(lightMaskTexture, vec2(0.5*0.31415, 0.5)*(p.xy + vec2(0.0, -0.5 + lightMaskShift)), bias).y;
    //v = smoothstep(v, 0.8, 0.3);
    v = pow(v, 2.0) * 2.0;
    v *= exp(max(pow(p.y * p.y, 0.5) - 0.9, 0.0) * -20.0);

    return v;
}

float rayfield(vec3 p)
{
    return field(p, 0.0);
}

void main(void)
{
    vec2 uv = (gl_FragCoord.xy-vec2(viewportX,viewportY)) / iResolution.xy;

    uv = 2.0 * uv - 1.0;
    uv.x *= iResolution.x / iResolution.y;

    vec3 ro = vec3(0.0, 0.0, -2.0);
    vec3 rd = normalize(vec3(uv, 1.2));

    //ro.y += sin(iGlobalTime * 3.0) * 0.25; // bouncing cam

    ro.xz = rotate(ro.xz, iGlobalTime * 0.4);
    ro.xy = rotate(ro.xy, 0.2);
    rd.xz = rotate(rd.xz, iGlobalTime * 0.4);
    rd.xy = rotate(rd.xy, 0.2);

    vec3 col = vec3(1.0);

    ITSC itsc, itsc2;

    itsc = raycyl_i(ro, rd, vec3(0.0), roomRadius);

    itsc2 = raycyl(ro, rd, vec3(0.0), max(0.000001,lanternRadius));
    if(itsc2.dist < itsc.dist)
        itsc = itsc2;

    raypln(itsc, ro, rd, vec3(0.0, 1.0, 0.0), -1.0);

    vec2 prj = vec2(atan(itsc.p.x, itsc.p.z), itsc.p.y + length(itsc.p.xz));

    col = pow(texture2D(wallTexture, prj * vec2(0.1595,0.5)).z, 7.0) * vec3(3.0);
    col = vec3(col.z);

    float occ = length(vec2(length(itsc.p.xz) - 2.25, itsc.p.y + 1.0));
    occ = min(occ, length(vec2(length(itsc.p.xz) - 0.7, itsc.p.y + 1.0)));
    col *= 1.0 - exp(occ * -2.0) * 0.9;

    col += field(itsc.p, 0.0);

    #define STP 40

    float d = 0.0;
    float dI = itsc.dist / float(STP);
    float vli = 0.0;
    float rnd = gl_FragCoord.x + gl_FragCoord.y * 0.3333 + iGlobalTime;
    for(int i = 0; i < STP; i++)
    {
        rnd = fract(sin(rnd) * 23897.52351234);
        vli += rayfield(ro + rd * (d + rnd * dI));
        d += dI;
    }

    col += fogDensity * vli / float(STP);

    //vec3 cexp = mix(vec3(1.0, 0.7, 0.5), vec3(0.1, 0.7, 1.0), smoothstep(0.1, 0.5, col.x));
    vec3 cexp = mix(1.0-fogColor, 1.0-sceneColor, smoothstep(0.2, 0.1, col.x));
    col = pow(col, cexp * 2.0) * 4.0;
    col = pow(col, vec3(1.0 / gamma));

    gl_FragColor = vec4(col, Alpha);
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="iGlobalTime" ValuePropRef="App.Time"/>
        <ShaderVariable VariableName="resX" Value="1163" ValuePropRef="App.ViewportWidth"/>
        <ShaderVariable VariableName="resY" Value="432" ValuePropRef="App.ViewportHeight"/>
        <ShaderVariable VariableName="viewportX" Value="262" ValuePropRef="App.ViewportX"/>
        <ShaderVariable VariableName="viewportY" Value="262" ValuePropRef="App.ViewportY"/>
        <ShaderVariable VariableName="Alpha" ValuePropRef="1-Parameters[0];"/>
        <ShaderVariable VariableName="fogDensity" ValuePropRef="Parameters[4]*3;"/>
        <ShaderVariable VariableName="lanternRadius" ValuePropRef="0.01+Parameters[5];"/>
        <ShaderVariable VariableName="roomRadius" ValuePropRef="4+Parameters[6]*10;"/>
        <ShaderVariable VariableName="sceneColor" VariableRef="ShaderColor"/>
        <ShaderVariable VariableName="gamma" ValuePropRef="Parameters[7]*4;"/>
        <ShaderVariable VariableName="panIntensity" ValuePropRef="Parameters[8];"/>
        <ShaderVariable VariableName="panSpeed" ValuePropRef="Parameters[9]*40;"/>
      </UniformVariables>
    </Shader>
    <Model Name="ScreenModel">
      <OnRender>
        <UseMaterial Material="ScreenMaterial"/>
        <RenderSprite/>
      </OnRender>
    </Model>
    <Variable Name="ShaderColor" Type="7"/>
    <Bitmap Name="WallBitmap" Width="512" Height="256">
      <Producers>
        <BitmapNoise Octaves="4" Color="3" Tile="255"/>
      </Producers>
    </Bitmap>
    <Bitmap Name="LightMaskBitmap" Width="256" Height="256">
      <Producers>
        <BitmapNoise Offset="0.45" Persistence="0.67" ZHeight="42.06" Tile="255"/>
        <BitmapExpression>
          <Expression>
<![CDATA[//X,Y : current coordinate (0..1)
//Pixel : current color (rgb)
//Sample expression: Pixel.R=abs(sin(X*16));
if(Pixel.G>0.35)
  Pixel=vector4(0,0,0,0);
else
  Pixel=vector4(0,1,0,0);]]>
          </Expression>
        </BitmapExpression>
        <BitmapBlur Radius="14" Kind="2"/>
      </Producers>
    </Bitmap>
  </Content>
</ZApplication>
