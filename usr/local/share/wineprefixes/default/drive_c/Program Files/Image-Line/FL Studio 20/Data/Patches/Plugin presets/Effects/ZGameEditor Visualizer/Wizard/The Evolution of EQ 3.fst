FLhd   0 * ` FLdt�1  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��bT1  ﻿[General]
GlWindowMode=1
LayerCount=21
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,1,3,2,9,4,10,5,13,6,14,7,15,8,17,18,16,11,20,19,12

[AppSet0]
App=Background\SolidColor
FParamValues=0,0.596,0.068,0.936
ParamValues=0,596,68,936
Enabled=1
Collapsed=1
Name=Background

[AppSet1]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0,0,0,0,0.5,0.5,1,1,1,4,0.5,1,0.048,0.964,0,0.64,0.06,0,0,0,0,1
ParamValues=0,0,0,0,500,500,1000,1000,1000,4,500,1,48,964,0,640,60,0,0,0,0,1
ParamValuesCanvas effects\DarkSpark=500,24,500,500,500,500,500,500,500,500,0,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesCanvas effects\SkyOcean=0,0,500,0,500,604,500,0,500,1000,388,0,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPeak Effects\Linear=0,1000,1000,0,634,500,496,256,0,500,260,498,496,1000,0,1000,0,500,500,500,500,1000,1000,76,184,104,48,428,350,282,124
ParamValuesPostprocess\Youlean Pixelate=604,0,0,0
ParamValuesPostprocess\Youlean Motion Blur=748,1000,528,580
ParamValuesTerrain\GoopFlow=424,1000,1000,0,1000,500,304,0,500,496,324,496,1000,300,168,92
ParamValuesMisc\FruityIndustry=0,0,1000,0,0,500,500,500,500,0,500,500,500
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,838,500,500,500,500,500
ParamValuesMisc\Automator=0,74,30,429,0,282,466,1000,185,61,286,508,526,254,1000,111,91,571,0,250,250,0,0,0,0,0,250,250,0,0,0,0
Enabled=1
Collapsed=1
Name=EQ 1

[AppSet3]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0,0,0,0,0.5,0.5,1,1,0.88,4,0.5,1,0.048,0.964,0,0.64,0.024,0,0,0,0,1
ParamValues=0,0,0,0,500,500,1000,1000,880,4,500,1,48,964,0,640,24,0,0,0,0,1
ParamValuesTerrain\GoopFlow=104,628,1000,168,1000,500,304,0,500,496,104,460,1000,300,308,36
ParamValuesPeak Effects\Linear=0,248,68,788,634,500,496,256,0,500,260,470,496,1000,0,1000,0,500,500,500,500,1000,300,52,164,104,0,248,234,282,124
Enabled=1
Collapsed=1
Name=EQ 2

[AppSet2]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0,0.48,0,0,0.5,0.5,1,1,0.768,4,0.5,1,0.048,0.964,0,0.64,0.056,0,0,0,0,1
ParamValues=0,480,0,0,500,500,1000,1000,768,4,500,1,48,964,0,640,56,0,0,0,0,1
ParamValuesPostprocess\Vignette=0,0,0,576,944,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPeak Effects\Linear=0,0,0,0,634,500,496,256,0,500,260,370,496,1000,0,1000,0,500,500,500,500,1000,300,52,164,104,0,248,234,282,124
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesBackground\SolidColor=0,612,68,924
ParamValuesPostprocess\ScanLines=929,929,0,0,0,1000
ParamValuesTerrain\GoopFlow=512,1000,868,0,1000,500,304,0,500,496,324,460,1000,300,308,128
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPostprocess\Dot Matrix=1000,0,188,0,680,500,1000,1000
Enabled=1
Collapsed=1
Name=EQ 3

[AppSet9]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0,0,0,0,0.5,0.5,1,1,0.68,4,0.5,1,0.048,0.964,0,0.64,0.024,0,0,0,0,1
ParamValues=0,0,0,0,500,500,1000,1000,680,4,500,1,48,964,0,640,24,0,0,0,0,1
ParamValuesHUD\HUD Prefab=0,128,500,0,0,500,468,526,1000,1000,444,0,500,1000,368,132,1000,1000
ParamValuesBackground\ItsFullOfStars=0,756,1000,0,764,500,500,500,0,0,0
ParamValuesFeedback\70sKaleido=0,0,0,1000,1000,1000
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,468,364,500,500,588,0,0,536
ParamValuesFeedback\WormHoleEclipse=0,692,1000,0,1000,184,500,0,96,1000,140
ParamValuesHUD\HUD Grid=0,500,0,0,500,500,1000,1000,1000,444,500,1000,500,100,0,500,1000,112,1000,300,0,1000
ParamValuesFeedback\BoxedIn=0,0,620,1000,500,144,252,500,208,704
ParamValuesFeedback\FeedMe=0,0,588,1000,616,1000,340
Enabled=1
Collapsed=1
Name=EQ 4

[AppSet4]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0,0.48,0,0,0.5,0.5,1,1,0.588,4,0.5,1,0.048,0.964,0,0.64,0.056,0,0,0,0,1
ParamValues=0,480,0,0,500,500,1000,1000,588,4,500,1,48,964,0,640,56,0,0,0,0,1
ParamValuesTerrain\GoopFlow=476,248,1000,168,1000,500,304,0,496,508,748,460,516,548,308,36
ParamValuesHUD\HUD Grid=932,500,0,644,500,500,1000,1000,1000,444,500,1000,500,644,404,200,496,216,616,320,668,1000
Enabled=1
Collapsed=1
Name=EQ 5

[AppSet10]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0,0,0,0,0.5,0.5,1,1,0.504,4,0.5,1,0.048,0.964,0,0.64,0.024,0,0,0,0,1
ParamValues=0,0,0,0,500,500,1000,1000,504,4,500,1,48,964,0,640,24,0,0,0,0,1
ParamValuesPostprocess\Youlean Color Correction=508,568,504,508,508,484
Enabled=1
BufferRenderQuality=6
Collapsed=1
Name=EQ 6

[AppSet5]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0,0.48,0,0,0.5,0.5,1,1,0.432,4,0.5,1,0.048,0.964,0,0.64,0.056,0,0,0,0,1
ParamValues=0,480,0,0,500,500,1000,1000,432,4,500,1,48,964,0,640,56,0,0,0,0,1
ParamValuesTerrain\GoopFlow=516,92,1000,168,836,500,304,0,496,484,748,460,516,548,308,36
Enabled=1
Collapsed=1
Name=EQ 7

[AppSet13]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0,0,0,0,0.5,0.5,1,1,0.356,4,0.5,1,0.048,0.964,0,0.64,0.024,0,0,0,0,1
ParamValues=0,0,0,0,500,500,1000,1000,356,4,500,1,48,964,0,640,24,0,0,0,0,1
ParamValuesPostprocess\Blooming=0,0,0,1000,500,800,288,400,0
ParamValuesPostprocess\ColorCyclePalette=0,250,500,333,267,528,1000,1000,1000,250,168
ParamValuesPostprocess\ScanLines=0,214,1000,1000,0,272
ParamValuesPostprocess\Youlean Pixelate=632,0,0,0
ParamValuesImage effects\Image=732,0,0,1000,187,342,610,0,0,0,0,1,1000,1000
ParamValuesPostprocess\Youlean Bloom=548,0,134,112
ParamValuesPostprocess\Youlean Motion Blur=500,0,872,568
ParamValuesPostprocess\Blur=1000
ParamValuesBackground\FourCornerGradient=667,1000,944,712,1000,638,748,1000,96,1000,1000,0,1000,1000
Enabled=1
Collapsed=1
Name=EQ 8

[AppSet6]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0,0.48,0,0,0.5,0.5,1,1,0.284,4,0.5,1,0.048,0.964,0,0.64,0.056,0,0,0,0,1
ParamValues=0,480,0,0,500,500,1000,1000,284,4,500,1,48,964,0,640,56,0,0,0,0,1
ParamValuesPostprocess\Blooming=0,0,0,712,580,800,128,176,0
Enabled=1
Collapsed=1
Name=EQ 9

[AppSet14]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0,0,0,0,0.5,0.5,1,1,0.224,4,0.5,1,0.048,0.964,0,0.64,0.024,0,0,0,0,1
ParamValues=0,0,0,0,500,500,1000,1000,224,4,500,1,48,964,0,640,24,0,0,0,0,1
ParamValuesBackground\SolidColor=0,612,84,956
ParamValuesImage effects\Image=0,612,68,888,1000,500,584,484,0,8,1000,0,1000,736
Enabled=1
Collapsed=1
Name=EQ 10

[AppSet7]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0,0.48,0,0,0.5,0.5,1,1,0.172,4,0.5,1,0.048,0.964,0,0.64,0.056,0,0,0,0,1
ParamValues=0,480,0,0,500,500,1000,1000,172,4,500,1,48,964,0,640,56,0,0,0,0,1
ParamValuesPostprocess\Youlean Motion Blur=500,0,872,568
ParamValuesPostprocess\Blur=1000
ParamValuesBackground\FourCornerGradient=667,1000,944,712,1000,638,748,1000,96,1000,1000,0,1000,1000
ParamValuesPostprocess\ColorCyclePalette=0,250,500,333,267,528,1000,1000,1000,250,168
ParamValuesPostprocess\ScanLines=0,214,1000,1000,0,272
Enabled=1
Collapsed=1
Name=EQ 11

[AppSet15]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0,0,0,0,0.5,0.5,1,1,0.124,4,0.5,1,0.048,0.964,0,0.64,0.024,0,0,0,0,1
ParamValues=0,0,0,0,500,500,1000,1000,124,4,500,1,48,964,0,640,24,0,0,0,0,1
ParamValuesHUD\HUD Prefab=265,420,500,0,1000,500,500,980,1000,1000,444,0,500,1000,368,0,1000,1000
ParamValuesPeak Effects\StereoWaveForm=0,0,0,1000,0,0,1000,1000,1000,1000,500,500,500,500,500,500,500,500,0,500,500,1000,12,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPeak Effects\PeekMe=0,0,1000,0,352,500,272,230,0,380,0,0
ParamValuesPhysics\Cage=888,672,1000,0,800,500,500,864,72,1000
ParamValuesBackground\SolidColor=0,612,764,924
ParamValuesPeak Effects\SplinePeaks=0,756,950,0,500,500,1000,0,0,0
ParamValuesPeak Effects\Polar=860,580,1000,0,176,500,500,1000,1000,288,620,1000,552,284,0,0,0,1000,1000,1000,304,356,0,1000
ParamValuesImage effects\Image=0,0,0,164,914,534,514,0,0,0,0,1,0,0
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
Enabled=1
Collapsed=1
Name=EQ 12

[AppSet8]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0,0.48,0,0,0.5,0.5,1,1,0.08,4,0.5,1,0.048,0.964,0,0.64,0.056,0,0,0,0,1
ParamValues=0,480,0,0,500,500,1000,1000,80,4,500,1,48,964,0,640,56,0,0,0,0,1
ParamValuesPostprocess\Vignette=0,0,0,1000,676,596
ParamValuesPostprocess\RGB Shift=424,448,0,600,700,200
ParamValuesPostprocess\Youlean Bloom=356,0,58,106
ParamValuesPostprocess\Point Cloud High=0,1000,482,500,512,448,496,495,570,657,184,0,376,0,667
ParamValuesPostprocess\ScanLines=400,400,0,0,1000,1000
ParamValuesPostprocess\Point Cloud Low=0,1000,482,500,500,448,496,503,570,473,156,0,0,0,0
Enabled=1
Collapsed=1
Name=EQ 13

[AppSet17]
App=Image effects\Image
FParamValues=0.156,0.572,0,0.972,0.896,0.5,0.5,0,0,0,0,0,1,1
ParamValues=156,572,0,972,896,500,500,0,0,0,0,0,1,1000
Enabled=1
Collapsed=1
Name=Dot 1

[AppSet18]
App=Image effects\Image
FParamValues=0,0.224,1,1,0.756,0.5,0.5,0,0,0,0,0,1,1
ParamValues=0,224,1000,1000,756,500,500,0,0,0,0,0,1,1000
Enabled=1
Collapsed=1
Name=Dot 2

[AppSet16]
App=HUD\HUD Meter Radial
AppVersion=1
FParamValues=0,0,0,0,0.984,0,0,0.14,0.5,0.5,0.48,0.222,0,0,4,0,0,0,0,1
ParamValues=0,0,0,0,984,0,0,140,500,500,480,222,0,0,4,0,0,0,0,1
ParamValuesFeedback\WarpBack=0,580,1000,0,500,200,104
ParamValuesFeedback\WormHoleDarkn=0,0,0,0,0,500,500,52,0,500,500,500
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,1000,0,348,260,164,500,500
ParamValuesHUD\HUD Graph Linear=0,500,0,0,500,500,1000,1000,146,444,500,1000,0,1000,0,500,200,0,0,0,500,1000
ParamValuesHUD\HUD Callout Line=452,500,0,1000,496,628,680,800,0,200,182,460,1000,450,216,1000
ParamValuesFeedback\SphericalProjection=60,0,0,0,0,425,500,434,500,320,0,1,500,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesImage effects\Image=0,0,0,636,684,424,556,0,0,0,0,1,0,0
ParamValuesHUD\HUD Graph Polar=20,352,1000,0,500,500,194,444,728,0,1000,268,808,384,498,780,276,12,0,0,500,1000
ParamValuesBackground\FourCornerGradient=467,0,728,1000,1000,2,1000,1000,500,1000,1000,0,1000,1000
Enabled=1
Collapsed=1
Name=TimeLine
LayerPrivateData=78018B4F494D4B2C2DD12B294963188900003F650455

[AppSet11]
App=Text\TextTrueType
FParamValues=0.152,0,0,1,0,0.492,0.497,0,0,0,0.5
ParamValues=152,0,0,1000,0,492,497,0,0,0,500
Enabled=1
Name=Main Text

[AppSet20]
App=Text\TextTrueType
FParamValues=0.152,0,0,0,0,0.492,0.5,0,0,0,0.5
ParamValues=152,0,0,0,0,492,500,0,0,0,500
Enabled=1

[AppSet19]
App=Background\FourCornerGradient
FParamValues=6,0.36,0.576,0.136,1,0.576,0.136,1,0.576,0.136,1,0.576,0.136,1
ParamValues=6,360,576,136,1000,576,136,1000,576,136,1000,576,136,1000
ParamValuesPostprocess\Vignette=0,0,0,576,944,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPeak Effects\Linear=580,612,68,762,822,536,500,732,0,500,0,350,468,1000,0,1000,0,0,0,0,0,0,0,0,332,24,176,632,558,298,124
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesBackground\SolidColor=0,612,68,924
ParamValuesPostprocess\ScanLines=929,929,0,0,0,1000
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPostprocess\Dot Matrix=1000,0,188,0,680,500,1000,1000
Enabled=1
Collapsed=1
Name=Filter Color

[AppSet12]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fade in-out

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""6"" y=""6""><p uppercase=""yes""><b><font face=""American-captain"" size=""8"" color=""#fff"">[author]</font></b></p></position>","<position x=""6"" y=""12.3""><p uppercase=""yes""><b><font face=""Chosence-Bold"" size=""5"" color=""#fff"">[title]</font></b></p></position>","<position x=""6"" y=""8.3""><p align=""right"" uppercase=""yes""><b><font face=""Oswald"" size=""3"" color=""#fff"">[comment]</font></b></p></position>"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

