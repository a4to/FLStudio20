﻿This option is provided to allow you to more accurately hear the song as it will be rendered, and to help FL Studio to perform better in some questionable tests run by shady software vendors.

It's also |SLOW AS HELL| so don't expect to actually use this if your system specifications are not very high.
