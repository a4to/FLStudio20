FLhd   0  0 FLdtB  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   ��!�  ﻿[General]
GlWindowMode=1
LayerCount=8
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=2,6,5,4,3,0,1,7

[AppSet2]
App=HUD\HUD Graph Polar
FParamValues=0,0.5,0,0,0.5,0.856,0.074,4,0.784,0,1,0,1,0,0.25,0.5,0,0.46,0,0,0.5,0
ParamValues=0,500,0,0,500,856,74,4,784,0,1,0,1000,0,250,500,0,460,0,0,500,0
ParamValuesImage effects\Image=0,0,0,0,1000,500,500,0,0,0,1000,0,0,0
ParamValuesPeak Effects\Linear=0,964,0,0,178,752,508,112,250,500,196,350,1000,1000,500,0,0,500,500,500,468,0,500,152,200,0,32,212,330,250,100
ParamValuesPeak Effects\Polar=0,0,0,0,172,500,158,300,556,500,128,420,0,631,1000,500,1000,1000,968,1000,1000,0,0,1000
ParamValuesPeak Effects\SplinePeaks=940,0,0,0,900,500,360,0,0,0
ParamValuesPeak Effects\Stripe Peeks=0,0,0,0,0,0,74,500,220,0,250,500,700,0,500,150,300,200,200,300,1000,0,0
ParamValuesPeak Effects\Youlean Peak Shapes=0,0,0,0,74,500,368,500,490,104,564,500,120,144,500,0,0,200,200,360,358,0,520,5,0,1000,0,500,500,0,0,0
ParamValuesPeak Effects\Youlean Waveform=0,0,0,0,186,859,500,250,372,250,500,210,500,0,100,0,750,1000,16,88,0,500,100,100,0,0,282,500,500,1000,0,1000
Enabled=1
Name=Section2

[AppSet6]
App=HUD\HUD 3D
FParamValues=0,0,0.4985,0.4709,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,0
ParamValues=0,0,498,470,500,500,500,500,500,500,500,500,500,500,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,0
Enabled=1
ImageIndex=2
Name=ForegroundMod

[AppSet5]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
Name=TextFst

[AppSet4]
App=Text\TextTrueType
FParamValues=0,0.8333,0,0,0,0.5,0.5,0,0,0,0.5
ParamValues=0,833,0,0,0,500,500,0,0,0,500
Enabled=0
Name=Text

[AppSet3]
App=Postprocess\Youlean Drop Shadow
FParamValues=0.5,0.2,0,1,0.02,0.875,0
ParamValues=500,200,0,1000,20,875,0
Enabled=1
UseBufferOutput=1

[AppSet0]
App=Youlean new shaders\Clouds\Nebulous Tunnel
FParamValues=0,0,1,0,0.6
ParamValues=0,0,1000,0,600
Enabled=1
Name=Section0

[AppSet1]
ParamValuesPostprocess\Youlean Color Correction=234,500,501,500,500,500
Enabled=1
Name=Section1

[AppSet7]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
ImageIndex=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Images="@EmbeddedFst:""Name=Gracious Marker.fst"",Data=7801C5544B6EDB3010DD1BF01D02AF5597A4A85F1106D0C7AA81266D1A37C9C2CD8295A631015912682A8DCFD6458FD42B7428D94AB3C822E8A21024CDF771F83033BF7FFE5ABF871AB4ACEEA693F7D5ADAACBE6C7455382A0D3C9B9DC834E9BAE36C29D4EF2CB95355EA8525D36DA8837A8C4BB160A73258D6A04F5DF45879C4FBA042D88431D369D4C27EBB86D5760081E8192585E675FF13DF9028FC6C24A2DB737B2EA60872964EE39F81DFE734E58F864423343EFDC239E8F32755C7CFB843E09E5FEC1BA9E637A6470D83F223A47DD5A1110E18E680423AC7788A0561D3117B5FC5641391273A9D583349049234510121AB8E982E53C61198F7916D0380ED3346539CB0286F698A77C9179A4F7B23CCD78C4D28CC5098FD3C40F6898FB781C59C49C9324C23B8CBCD1D7F1C6A38859568223372E45BD7F78800E37E096A957B387B88E05B5DC3C8344C023DEBFF097237F1C29E349465F66C98B2C4B6C91C6C4F7B12B4796D8EB58B27DE4332CDCB6CE3CE4D8664F143137185BE21031E86333BCDC6016D452F40C1201FF2B4537AA84E6041E5B1C5CE4A957978292901C945B41237654AEA0C6F9CDDB9D70D1B492DBB6029C7110C780141744F1516E41E04477A56AFE36F4E09F3B5929B3B71917F2F103EC731C7258B5B250F53DC21274E4AA827A004994D1F6008ABD19328ECE1E7634B3B0CFB8AE8B66DB6AD8ED700E3168D5B5A0777D7D56C55EB8DED98D551BA8ED3DED86116BA34C0577CE5A7666D3681410643B042CCDB612B3D3F604ABBDAFC56C56A01DF46C76B636985BA91AEE4EDFB667B3034D08BFDAD7853D0CABC7487B9BE1E40C8C2C3650DA639B56B811C76508DF8DA02EAEC55B559A8DF0431497A0EE37467841D0576CABD54D5581C6CC2B28CF1F2AC13C1FB7B106A8472DC1FD382AE7DD7694A7933F6A918537"
VideoUseSync=0
Filtering=0

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

[Wizard]
Section0Cb=Nebulous Tunnel
Section1Cb=None
Section2Cb=Graph Polar 01
Section3Cb=Gracious Marker
Macro0=Song Title
Macro1=Song Author
Macro2=Song made with love and time

