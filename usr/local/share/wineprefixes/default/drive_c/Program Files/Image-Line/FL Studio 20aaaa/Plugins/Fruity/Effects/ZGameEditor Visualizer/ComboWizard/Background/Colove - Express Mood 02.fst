FLhd   0 * ` FLdt$  �20.6.9.1651 �s  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ՇG�#  ﻿[General]
GlWindowMode=1
LayerCount=21
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,5,3,1,2,4,9,6,12,13,15,14,16,10,8,22,23,17,7,11,20

[AppSet0]
App=Background\SolidColor
FParamValues=0,0.648,0.636,0.728
ParamValues=0,648,636,728
ParamValuesBackground\FourCornerGradient=0,1000,664,732,1000,898,736,1000,504,620,1000,150,812,1000
Enabled=1
Collapsed=1
Name=Background XSTry-Ray

[AppSet5]
App=Canvas effects\Flaring
FParamValues=0.524,1,1,0.092,0.952,0.5,0.5,0.653,0,2,0.112
ParamValues=524,1000,1000,92,952,500,500,653,0,2,112
Enabled=1
Collapsed=1
Name=Fatele Mod

[AppSet3]
App=Canvas effects\SkyOcean
FParamValues=0,0.96,1,0,0,0.5,0.5,0.708,0.34,0.736,1,1,0.576,0.68
ParamValues=0,960,1000,0,0,500,500,708,340,736,1000,1000,576,680
Enabled=1
Collapsed=1
ImageIndex=1
Name=React 1

[AppSet1]
App=Background\FogMachine
FParamValues=0.672,0.588,1,0.064,0.456,0.5,0.5,0.5,0.5,0.636,0,0.516
ParamValues=672,588,1000,64,456,500,500,500,500,636,0,516
ParamValuesBackground\Youlean Background MDL=1000,1000,1000,420,884,424,198,320,648,558,376,696,800,664,1000,0,500,4,0,500
Enabled=1
Collapsed=1
ImageIndex=1
Name=Liner Color

[AppSet2]
App=Canvas effects\SkyOcean
FParamValues=0,0.856,1,1,0,0.5,0.5,0,0.404,0.656,1,0.568,0.048,0.488
ParamValues=0,856,1000,1000,0,500,500,0,404,656,1000,568,48,488
ParamValuesBlend\VideoAlphaKey=0,636,1000,0,1000,656,344,0,0,0,474,92,340
Enabled=1
Collapsed=1
ImageIndex=1
Name=React 2

[AppSet4]
App=Canvas effects\SkyOcean
FParamValues=0,0.592,1,1,0.232,0.5,0.5,0.708,0.34,0.568,0.864,0.924,0,0
ParamValues=0,592,1000,1000,232,500,500,708,340,568,864,924,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=React 3

[AppSet9]
App=Canvas effects\Rain
FParamValues=0.896,1,1,0.648,0.216,0.5,0.5,0.5,0.5,0,0.736,1
ParamValues=896,1000,1000,648,216,500,500,500,500,0,736,1000
ParamValuesCanvas effects\OverlySatisfying=0,0,0,0,600,500,500,100,100,1000,500
Enabled=1
Collapsed=1
ImageIndex=1
Name=Modliner X1

[AppSet6]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.864,0.2,0.864,0.628,0.472,0,0
ParamValues=0,0,0,0,864,200,864,628,472,0,0
ParamValuesImage effects\Image=0,0,0,1000,884,500,500,0,0,0,0,0,1000,1000
Enabled=1
Collapsed=1
ImageIndex=1
Name=Start Riser 1

[AppSet12]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.924,0.184,0.304,0.608,0.472,0,0.268
ParamValues=0,0,0,0,924,184,304,608,472,0,268
Enabled=1
Collapsed=1
ImageIndex=1
Name=Start Riser 2

[AppSet13]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.932,0.496,0.312,0.496,0.472,0,0.72
ParamValues=0,0,0,0,932,496,312,496,472,0,720
Enabled=1
Collapsed=1
ImageIndex=1
Name=Start Riser 3

[AppSet15]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.932,0.344,0.312,0.496,0.472,0,0.72
ParamValues=0,0,0,0,932,344,312,496,472,0,720
Enabled=1
Collapsed=1
ImageIndex=1
Name=Start Riser 4

[AppSet14]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.932,0.416,0.632,0.496,0.472,0,0.72
ParamValues=0,0,0,0,932,416,632,496,472,0,720
Enabled=1
Collapsed=1
ImageIndex=1
Name=Start Riser 5

[AppSet16]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.9,0.668,0.488,0.496,0.472,0,0
ParamValues=0,0,0,0,900,668,488,496,472,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=Start Riser 6

[AppSet10]
App=Postprocess\Youlean Image Rotation
FParamValues=0.672,0.5,0,0.5,0.608,0.432,0,1
ParamValues=672,500,0,500,608,432,0,1
ParamValuesHUD\HUD Prefab=0,0,500,0,1000,500,500,186,1000,1000,444,0,500,1000,832,276,916,1000
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesImage effects\ImageTileSprite=4,4,0,0,0,0,4,250,250,500,500,500,500,0,0,0,0
ParamValuesHUD\HUD Grid=0,500,0,0,500,500,1000,1000,1000,444,500,1000,500,100,0,500,1000,100,792,0,0,1000
ParamValuesPostprocess\ColorCyclePalette=0,0,0,0,0,1000,0,500,0,0,0
ParamValuesFeedback\BoxedIn=0,0,0,1000,1000,312,80,500,544,1000
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesHUD\HUD Image=0,0,500,500,1000,1000,500,444,500,0,0,1000,1000,0,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesFeedback\FeedMe=0,0,0,0,1000,260,468
ParamValuesMisc\PentUp=0,488,0,0,544,500,500,500,472,724,580,0
ParamValuesPostprocess\AudioShake=24,0,0,500,100,900
ParamValuesPostprocess\Dot Matrix=424,500,500,520,0,500,1000,1000
ParamValuesFeedback\70sKaleido=0,0,520,368,248,412
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,0,0,500,500,500,500,500,500
ParamValuesPostprocess\Point Cloud High=0,434,346,520,492,472,512,491,50,265,0,0,224,0,333
ParamValuesCanvas effects\OverlySatisfying=0,0,0,0,600,500,500,100,100,1000,500
ParamValuesFeedback\SphericalProjection=60,0,0,0,0,425,500,498,156,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,636,500,500,500,500,500
ParamValuesHUD\HUD 3D=0,0,500,500,500,500,500,500,500,500,500,500,500,500,0,500,500,500,500,500,500,500,500,1000,1000,0,0,1000,1000,0
ParamValuesPostprocess\FrameBlur=0,0,524,300,926,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=Modliner X2

[AppSet8]
App=Peak Effects\Linear
FParamValues=0,0.964,0,1,0.486,0.388,0.548,0.268,0,0.5,0.26,0.79,0.484,1,0,0,0,0.5,0.5,0.5,0.5,0.828,0.5,0,0.02,0.192,0,0.7,0.206,0.398,0.66
ParamValues=0,964,0,1000,486,388,548,268,0,500,260,790,484,1,0,0,0,500,500,500,500,828,500,0,20,192,0,700,206,398,660
ParamValuesBackground\Youlean Background MDL=1000,72,1000,0,500,500,50,320,0,170,300,120,1000,1000,0,300,500,0,0,500
ParamValuesParticles\fLuids=0,0,0,0,884,492,476,0,0,0,392,628,500,780,708,766,500,608,468,728,500,500,168,208,500,0,0,250,248,0,0,0
ParamValuesCanvas effects\SkyOcean=0,592,1000,1000,232,500,500,708,340,568,864,924,0
ParamValuesParticles\ReactiveFlow=0,125,500,250,636,0,500,100,500,500,500,500,500,0,0,500,200,500,125,500,500,500,1000,1000,100
ParamValuesPeak Effects\Fluidity=912,488,1000,0,0,264,8
ParamValuesPeak Effects\JoyDividers=0,0,0,1000,700,500,500,0,500,100,600,650,510
Enabled=1
Collapsed=1
ImageIndex=1
Name=EQ

[AppSet22]
App=Peak Effects\Linear
FParamValues=0,0.592,0.776,0.496,0.486,0.388,0.548,0.268,0,0.5,0.76,0.538,0.484,1,0,0,0,0.5,0.5,0.5,0.5,0.828,0.5,0,0.02,0.192,0,0.7,0.206,0.398,0.66
ParamValues=0,592,776,496,486,388,548,268,0,500,760,538,484,1,0,0,0,500,500,500,500,828,500,0,20,192,0,700,206,398,660
Enabled=1
Collapsed=1
ImageIndex=1
Name=EQ MID

[AppSet23]
App=Peak Effects\Linear
FParamValues=0,0.564,1,1,0.99,0.388,0.548,1,0,0.068,0.608,0.674,1,1,0,1,0,0.472,0.5,0.5,0.5,0.968,0.5,0,0.02,0.192,0.032,0.7,0.206,0.398,0.66
ParamValues=0,564,1000,1000,990,388,548,1000,0,68,608,674,1000,1,0,1,0,472,500,500,500,968,500,0,20,192,32,700,206,398,660
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet17]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.5,0,0.178,0.371,0.364,0,1,1
ParamValues=500,0,178,371,364,0,1,1
ParamValuesScenes\Alien Thorns=0,500,250,500,2,500,500,0
ParamValuesTerrain\CubesAndSpheres=0,0,0,0,236,500,500,650,380,400,552,568,684,1000,1000,1000,0,1000
ParamValuesTerrain\GoopFlow=0,928,1000,0,280,500,500,0,500,500,500,424,1000,1000,596,1000
ParamValuesPostprocess\Blooming=0,0,0,1000,500,900,408,852,0
ParamValuesPostprocess\Point Cloud High=0,434,346,520,492,472,512,491,50,265,0,0,224,0,333
ParamValuesTunnel\Youlean Tunnel=0,0,0,1000,912,500,500,0,0,0,0,250,500,674,676,500,500,500,500,0,4,1000,1000,500,500
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=Bloom

[AppSet7]
App=Blend\Youlean From Buffer
FParamValues=0,1,0
ParamValues=0,1000,0
Enabled=1
Collapsed=1
Name=The Out

[AppSet11]
App=Feedback\WarpBack
FParamValues=0,0,0,0,0.5,0.5,0.044
ParamValues=0,0,0,0,500,500,44
ParamValuesHUD\HUD Prefab=15,0,500,0,1000,500,500,186,1000,1000,444,0,500,1000,832,276,916,1000
ParamValuesImage effects\ImageTileSprite=4,4,0,0,0,0,4,250,250,500,500,500,500,0,0,0,0
Enabled=1
Collapsed=1
Name=Modliner X3

[AppSet20]
App=Background\FourCornerGradient
FParamValues=4,1,0,0.716,1,0.734,0.908,1,0.128,1,1,0.498,1,1
ParamValues=4,1000,0,716,1000,734,908,1000,128,1000,1000,498,1000,1000
ParamValuesBackground\Youlean Background MDL=1000,892,0,0,500,500,230,320,0,242,300,120,1000,1000,0,300,500,0,0,500
ParamValuesBlend\VideoAlphaKey=0,888,1000,916,772,300,300,0,0,0,250,500,500
Enabled=1
Collapsed=1
Name=Filter-Color

[Video export]
VideoH=2160
VideoW=3840
VideoRenderFps=30
SampleRate=48000
VideoCodecName=
AudioCodecName=(default)
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=C:\Users\spyro\Desktop\LollieVox - Optimum Momentum_3.mp4
Bitrate=70000000
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="You can","change this text","in settings."
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

