FLhd   0  0 FLdt`3  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   ��e�2  ﻿[General]
GlWindowMode=1
LayerCount=63
FPS=2
MidiPort=-1
Aspect=1
LayerOrder=1,3,7,5,27,39,9,28,8,10,16,6,15,30,31,51,0,2,4,11,13,12,17,18,19,20,21,22,23,24,25,26,32,34,35,36,33,14,37,38,54,61,55,59,60,56,50,49,40,41,43,44,46,47,29,57,58,42,45,48,52,53,62
WizardParams=1808,1809,1810,1811,1813,1814,1815,1816,1817,2579,2580

[AppSet1]
App=Image effects\ImageBox
ParamValues=0,572,536,712,440,504,500,500,500,500,0,0,0,0,0,0,500,500,500
Enabled=1
UseBufferOutput=1
Collapsed=1
ImageIndex=9
MeshIndex=1

[AppSet3]
App=Image effects\ImageBox
ParamValues=0,56,784,384,440,504,500,500,500,500,0,0,0,0,0,0,500,500,500
Enabled=1
UseBufferOutput=1
Collapsed=1
ImageIndex=9
MeshIndex=1

[AppSet7]
App=HUD\HUD Mesh
ParamValues=0,488,464,1000,309,195,455,3,0,150,500,500,500,750,500,1
Enabled=1
Collapsed=1

[AppSet5]
App=HUD\HUD Mesh
ParamValues=0,488,468,820,309,195,455,1,0,150,500,784,500,750,500,1
Enabled=1
Collapsed=1

[AppSet27]
App=HUD\HUD Prefab
ParamValues=18,572,500,0,0,363,747,74,1000,1000,4,0,0,1,512,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C2CF43273CA18863D00000F220AA3

[AppSet39]
App=HUD\HUD Prefab
ParamValues=18,0,500,0,0,363,747,46,1000,1000,4,0,0,1,512,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C2CF43273CA18863D00000F220AA3

[AppSet9]
App=Misc\Automator
ParamValues=1,9,8,2,200,486,565,1,15,6,2,466,150,498,1,16,7,2,277,270,467,0,28,13,3,1000,74,250
Enabled=1
Collapsed=1

[AppSet28]
App=Postprocess\ParameterShake
ParamValues=1000,0,27,12,1000,0,39,12,3,3,0,0,4,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet8]
App=Postprocess\Blooming
ParamValues=0,0,0,1000,500,736,1000,95,0
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet10]
App=Image effects\Image
ParamValues=600,0,0,1000,1000,480,485,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=5

[AppSet16]
App=Image effects\Image
ParamValues=371,0,0,1000,776,459,638,398,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=5

[AppSet6]
App=Image effects\Image
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1
ImageIndex=5

[AppSet15]
App=HUD\HUD Free Line
ParamValues=0,500,0,0,500,500,308,100,0,100,0,100,1,0,0,118,500
ParamValuesHUD\HUD Prefab=94,0,500,0,1000,500,148,1000,396,1000,444,0,500,1000,368,260,720,1000
Enabled=1
UseBufferOutput=1
Collapsed=1
LayerPrivateData=78DA63606060F8B3F2A32D90B2676068D8CF0007A3EC5136F96C0016BE4022

[AppSet30]
App=HUD\HUD Free Line
ParamValues=0,0,1000,0,500,1000,1000,100,0,100,0,100,1,0,0,1000,250
Enabled=1
Collapsed=1
LayerPrivateData=78DA63600081067B20B603E2FD0C7030CA1E6593CF0600662A3E8D

[AppSet31]
App=Postprocess\Blooming
ParamValues=0,0,0,1000,500,1000,1000,876,0
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet51]
App=HUD\HUD Meter Radial
ParamValues=396,40,0,804,120,40,750,0,712,98,118,305,29,250,1,0,0,0,500,1
Enabled=1
UseBufferOutput=1
Collapsed=1
LayerPrivateData=78DAF3CDCF2B294E2D2A4A2CD10DC9C8CC6318690000C76205F0

[AppSet0]
App=Background\SolidColor
ParamValues=0,140,116,0
Enabled=1
Collapsed=1

[AppSet2]
App=Image effects\Image
ParamValues=0,0,0,0,484,500,1000,1000,476,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=2

[AppSet4]
App=Image effects\Image
ParamValues=0,0,0,0,484,500,483,1000,224,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=3

[AppSet11]
App=HUD\HUD Grid
ParamValues=0,500,0,1000,0,500,1000,1000,1000,4,500,1000,500,100,0,500,1000,100,0,0,0,1
Enabled=1
Collapsed=1

[AppSet13]
App=HUD\HUD Grid
ParamValues=0,500,0,1000,0,500,1000,1000,1000,4,500,1000,500,100,0,500,1000,100,0,0,0,1
Enabled=1
Collapsed=1

[AppSet12]
App=Postprocess\ParameterShake
ParamValues=1000,0,11,4,1000,548,13,4,4,4,0,0,6,6,0,1,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet17]
App=HUD\HUD 3D
ParamValues=276,0,623,409,500,412,500,500,500,500,500,500,500,1000,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
ParamValuesHUD\HUD Image=0,0,500,316,1000,1000,1000,444,500,0,0,1000,1000,500,1000
ParamValuesImage effects\Image=0,0,0,0,1000,500,500,0,0,0,0,1,0,0
Enabled=1
Collapsed=1
ImageIndex=6
MeshIndex=1

[AppSet18]
App=HUD\HUD 3D
ParamValues=276,0,259,430,500,412,500,500,500,500,500,500,500,1000,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
Collapsed=1
ImageIndex=6
MeshIndex=1

[AppSet19]
App=HUD\HUD 3D
ParamValues=276,0,540,442,500,412,500,500,500,500,500,500,500,1000,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
Collapsed=1
ImageIndex=6
MeshIndex=1

[AppSet20]
App=HUD\HUD 3D
ParamValues=276,0,985,461,500,412,500,500,500,500,500,500,500,1000,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
Collapsed=1
ImageIndex=6
MeshIndex=1

[AppSet21]
App=HUD\HUD 3D
ParamValues=276,0,628,461,500,412,531,500,500,500,500,500,500,1000,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
Collapsed=1
ImageIndex=6
MeshIndex=1

[AppSet22]
App=HUD\HUD 3D
ParamValues=276,0,632,461,500,412,553,500,500,500,500,500,500,1000,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
Collapsed=1
ImageIndex=6
MeshIndex=1

[AppSet23]
App=HUD\HUD 3D
ParamValues=276,0,443,486,500,412,553,500,500,500,500,500,500,1000,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
Collapsed=1
ImageIndex=6
MeshIndex=1

[AppSet24]
App=HUD\HUD 3D
ParamValues=276,0,261,515,500,412,553,500,500,500,500,500,500,1000,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
Collapsed=1
ImageIndex=6
MeshIndex=1

[AppSet25]
App=Misc\Automator
ParamValues=1,18,3,3,1000,68,250,1,19,3,3,1000,70,250,1,20,3,3,1000,30,250,1,21,3,3,1000,77,250
Enabled=1
Collapsed=1

[AppSet26]
App=Misc\Automator
ParamValues=1,22,3,3,1000,46,250,1,23,3,3,1000,24,250,1,24,3,3,1000,58,250,1,25,3,3,1000,59,250
Enabled=1
Collapsed=1

[AppSet32]
App=Image effects\Image
ParamValues=908,0,0,0,1000,475,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=7

[AppSet34]
App=Image effects\Image
ParamValues=908,0,0,0,1000,358,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=7

[AppSet35]
App=Image effects\Image
ParamValues=908,0,0,0,1000,510,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=7

[AppSet36]
App=Image effects\Image
ParamValues=908,0,0,0,1000,432,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=7

[AppSet33]
App=Misc\Automator
ParamValues=1,33,6,3,708,25,394,1,35,6,3,708,43,394,1,36,6,3,708,63,394,1,37,6,3,708,75,394
Enabled=1
Collapsed=1

[AppSet14]
App=Image effects\Image
ParamValues=0,0,0,0,1000,464,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=4

[AppSet37]
App=HUD\HUD Prefab
ParamValues=0,0,500,0,918,816,486,547,1000,1000,4,0,500,1,368,112,1000,1
ParamValuesImage effects\Image=0,0,0,896,720,837,486,0,0,0,0,0,0,0
Enabled=1
Name=LOGO
LayerPrivateData=78DA73C8CBCF4BD52B2E4B671899000060F9036F

[AppSet38]
App=HUD\HUD Prefab
ParamValues=31,0,500,0,764,344,158,479,1000,132,4,0,500,1,564,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4B4ECCC9C92F2D51C8C9CC4B8D498670740D0CCCF43273CA1846000000C5620A49

[AppSet54]
App=HUD\HUD Prefab
ParamValues=50,556,500,0,764,936,211,290,1000,616,4,0,500,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C4DF53273CA18460000002B840AB2

[AppSet61]
App=HUD\HUD Prefab
ParamValues=50,556,500,0,764,808,211,290,1000,616,4,0,500,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C4DF53273CA18460000002B840AB2

[AppSet55]
App=HUD\HUD Prefab
ParamValues=51,436,500,0,764,514,218,179,1000,616,4,0,500,1,424,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0CCDF43273CA18460000002C6D0AB3

[AppSet59]
App=HUD\HUD Prefab
ParamValues=51,436,500,0,764,586,218,179,1000,616,4,0,500,1,424,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0CCDF43273CA18460000002C6D0AB3

[AppSet60]
App=HUD\HUD Prefab
ParamValues=51,436,500,0,764,658,218,179,1000,616,4,0,500,1,424,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0CCDF43273CA18460000002C6D0AB3

[AppSet56]
App=HUD\HUD Prefab
ParamValues=136,0,500,0,764,19,158,187,224,1000,4,0,250,1,424,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DACBC9CC4B2D8EC98193BA0626967A9939650C230400007B4808DA

[AppSet50]
App=HUD\HUD Prefab
ParamValues=15,0,500,0,764,714,97,46,1000,1000,4,0,500,1,564,0,983,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C4CF53273CA18863D00000C6D0AA0

[AppSet49]
App=HUD\HUD Prefab
ParamValues=147,0,500,0,764,232,42,310,1000,124,4,0,500,1,564,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DACBC9CC4B2D8E294E2D482C4A2CC92F4262EA1A1818EA65E694310C6B000054580D20

[AppSet40]
App=HUD\HUD Prefab
ParamValues=38,552,500,0,764,536,83,250,1000,592,4,0,500,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C8CF53273CA184600000028C80AAF

[AppSet41]
App=HUD\HUD Prefab
ParamValues=38,0,500,0,764,536,83,250,1000,592,4,0,500,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C8CF53273CA184600000028C80AAF

[AppSet43]
App=HUD\HUD Prefab
ParamValues=39,552,500,0,764,884,83,250,1000,1000,4,0,500,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C4CF43273CA184600000029B10AB0

[AppSet44]
App=HUD\HUD Prefab
ParamValues=39,0,500,0,764,884,83,250,1000,1000,4,0,500,1,368,28,62,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C4CF43273CA184600000029B10AB0

[AppSet46]
App=HUD\HUD Prefab
ParamValues=219,0,40,1000,0,256,229,250,1000,352,4,0,500,1,368,0,0,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0606467A9939650C23030000A2990907

[AppSet47]
App=HUD\HUD Prefab
ParamValues=219,0,40,1000,0,256,249,250,1000,352,4,0,500,1,368,0,0,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0606467A9939650C23030000A2990907

[AppSet29]
App=HUD\HUD Prefab
ParamValues=219,0,40,1000,0,226,135,250,854,352,4,0,500,1,368,0,0,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0606467A9939650C23030000A2990907

[AppSet57]
App=HUD\HUD Prefab
ParamValues=219,0,40,1000,0,226,114,250,854,352,4,0,500,1,368,0,0,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0606467A9939650C23030000A2990907

[AppSet58]
App=Postprocess\ParameterShake
ParamValues=1000,0,29,16,1000,0,57,16,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet42]
App=Misc\Automator
ParamValues=0,42,2,6,692,428,250,0,42,17,2,632,440,250,1,45,16,2,500,514,250,1,45,17,2,500,250,250
Enabled=1
Collapsed=1

[AppSet45]
App=Postprocess\ParameterShake
ParamValues=788,272,41,1,1000,0,46,16,3,0,0,0,2,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet48]
App=Postprocess\ParameterShake
ParamValues=1000,4,51,16,1000,0,47,16,1,0,0,0,2,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet52]
App=Image effects\Image
ParamValues=0,0,0,0,1000,636,327,0,0,528,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=8

[AppSet53]
App=Postprocess\Youlean Color Correction
ParamValues=500,500,500,500,500,500
Enabled=1
Collapsed=1

[AppSet62]
App=Text\TextTrueType
ParamValues=0,0,0,0,0,491,500,0,0,0,500
Enabled=1
Collapsed=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0

[UserContent]
Text=" "
Html="<position x=""6""><position y=""5""><p align=""left""><font face=""American-Captain"" size=""7"" color=""#333333"">[author]</font></p></position>","<position x=""6""><position y=""15.5""><p align=""left""><font face=""Chosence-Bold"" size=""6"" color=""#333333"">[title]</font></p></position>","<position x=""14.2""><position y=""13.2""><p align=""center""><font face=""Chosence-Bold"" size=""3"" color=""#333333"">[extra1]</font></p></position>","<position x=""0""><position y=""94.5""><p align=""right""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""0""><position y=""75""><p align=""right""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""0""><position y=""78""><p align=""right""><font face=""Chosence-regular"" size=""2.5"" color=""#FFFFF"">[extra3]</font></p></position>","  "
Meshes=[plugpath]Content\Meshes\Car.zgeMesh
Images=[plugpath]Effects\HUD\prefabs\components\component-002.ilv,[plugpath]Effects\HUD\prefabs\iss.svg
VideoUseSync=0
EnableMipmap=1

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

