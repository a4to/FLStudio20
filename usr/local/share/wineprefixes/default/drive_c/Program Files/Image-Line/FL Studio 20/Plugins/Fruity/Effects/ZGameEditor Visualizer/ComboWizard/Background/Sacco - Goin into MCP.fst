FLhd   0 * ` FLdt�  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV գ8  ﻿[General]
GlWindowMode=1
LayerCount=14
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=4,3,8,7,0,1,6,9,2,12,27,20,24,23
WizardParams=1187,176,321,368,465,81

[AppSet4]
App=Misc\Automator
FParamValues=1,4,13,3,1,0.018,0.25,1,8,13,3,1,0.007,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,4,13,3,1000,18,250,1,8,13,3,1000,7,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
ParamValuesF=1,4,13,3,1,0.018,0.25,1,8,13,3,1,0.007,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
Enabled=1

[AppSet3]
App=HUD\HUD Prefab
FParamValues=72,0,0.308,0,0,0.5,0.5,0.25,1,1,4,0,0.153,1,0.368,0,1,0
ParamValues=72,0,308,0,0,500,500,250,1000,1000,4,0,153,1,368,0,1000,0
ParamValuesF=65,0,0.308,0,0,0.5,0.5,0.25,1,1,4,0,0.998,1,0.368,0,1,1
Enabled=1
UseBufferOutput=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D8C0DF43273CA184600000028CB0AAF

[AppSet8]
App=HUD\HUD Prefab
FParamValues=81,0,0.308,0,0,0.5,0.5,0.25,1,1,4,0,0.649,1,0.368,0,1,0
ParamValues=81,0,308,0,0,500,500,250,1000,1000,4,0,649,1,368,0,1000,0
ParamValuesF=74,0,0.308,0,0,0.5,0.5,0.25,1,1,4,0,0.649,1,0.368,0,1,1
Enabled=1
UseBufferOutput=1
LayerPrivateData=78014B2FCA4C89490712BA0606967A9939650C230B0000BBC405E8

[AppSet7]
App=HUD\HUD Prefab
FParamValues=22,0,0.308,0,0,0.5,0.5,0.25,1,1,4,0,0.949,1,0.368,0,1,0
ParamValues=22,0,308,0,0,500,500,250,1000,1000,4,0,949,1,368,0,1000,0
ParamValuesF=15,0,0.308,0,0,0.5,0.5,0.25,1,1,4,0,0.777,1,0.368,0,1,1
Enabled=1
UseBufferOutput=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C4CF53273CA18863D00000C6D0AA0

[AppSet0]
App=Background\SolidColor
FParamValues=0,0,0,1
ParamValues=0,0,0,1000
ParamValuesF=0,0,0,1
Enabled=1

[AppSet1]
App=Image effects\ImageSphinkter
FParamValues=0,0.529,0.84,0,1,0.5,0.5,0,0.58,0.495,0.909,0.5,0.51,0,0.748
ParamValues=0,529,840,0,1000,500,500,0,580,495,909,500,510,0,748
ParamValuesImage effects\ImageUnscaled=0,0,0,1000,1000
ParamValuesInternal controllers\Peak Band Controller=0,1000,0,1000,0,1000,0,1000,1000,1000,0,1000,0,1000,0,1000,0,0,0,0,0,0,0,0,0,0
ParamValuesF=0,0.529,0.84,0,1,0.5,0.5,0,0.58,0.495,0.909,0.5,0.51,0,0.748
ParamValuesImage effects\ImageTileSprite=5,5,0,0,0,0,4,250,250,500,500,136,500,0,0,0,0
Enabled=1
ImageIndex=31
MeshIndex=5

[AppSet6]
App=Image effects\ImageSphinkter
FParamValues=0,0.456,0.832,0,1,0.5,0.5,0,0.58,0.482,0.909,0.5,0.51,0,0.748
ParamValues=0,456,832,0,1000,500,500,0,580,482,909,500,510,0,748
ParamValuesF=0,0.456,0.832,0,1,0.5,0.5,0,0.58,0.482,0.909,0.5,0.51,0,0.748
Enabled=1
ImageIndex=33
MeshIndex=6

[AppSet9]
App=Image effects\ImageSphinkter
FParamValues=0,0,0.832,0,1,0.5,0.5,0,0.58,0.487,0.909,0.5,0.51,0,0.748
ParamValues=0,0,832,0,1000,500,500,0,580,487,909,500,510,0,748
ParamValuesF=0,0,0.832,0,1,0.5,0.5,0,0.58,0.487,0.909,0.5,0.51,0,0.748
Enabled=1
ImageIndex=32
MeshIndex=6

[AppSet2]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=1,0,0,0.455,1,0,0,0
ParamValues=1000,0,0,455,1000,0,0,0
ParamValuesF=1,0,0.15,0.455,1,0,0,0
Enabled=1

[AppSet12]
App=Background\SolidColor
FParamValues=0.073,0,0,1
ParamValues=73,0,0,1000
ParamValuesF=0.073,0,0,1
Enabled=1

[AppSet27]
App=Misc\Automator
FParamValues=1,27,13,3,1,0.004,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,27,13,3,1000,4,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
ParamValuesF=1,27,13,3,1,0.004,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
Enabled=1

[AppSet20]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.404,0,0.03,0.607,1,0,0,0
ParamValues=404,0,30,607,1000,0,0,0
ParamValuesF=0.404,0,0.03,0.607,1,0,0,0
Enabled=1

[AppSet24]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.524,0.5,0.5,0.628,0.5
ParamValues=500,524,500,500,628,500
ParamValuesF=0.5,0.524,0.5,0.5,0.628,0.5
Enabled=1

[AppSet23]
App=Postprocess\FrameBlur
FParamValues=0.06,0,0,0,0.446
ParamValues=60,0,0,0,446
ParamValuesF=0.06,0,0,0,0.446
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="This is the default text."
Meshes=[plugpath]Content\Meshes\Brain.zgeMesh,[plugpath]Content\Meshes\Car.zgeMesh,[plugpath]Content\Meshes\CircularPlane.zgeMesh,[plugpath]Content\Meshes\Cone(Textue).zgeMesh,[plugpath]Content\Meshes\Cone.zgeMesh,[plugpath]Content\Meshes\Cube.zgeMesh,[plugpath]Content\Meshes\Cylinder(Texture).zgeMesh,[plugpath]Content\Meshes\Cylinder.zgeMesh,[plugpath]Content\Meshes\Drone.zgeMesh,[plugpath]Content\Meshes\Hero.zgeMesh,[plugpath]Content\Meshes\Heroine.zgeMesh,[plugpath]Content\Meshes\Monkey.zgeMesh,[plugpath]Content\Meshes\Plane.zgeMesh,[plugpath]Content\Meshes\Skull.zgeMesh,[plugpath]Content\Meshes\Sphere(Ico).zgeMesh,[plugpath]Content\Meshes\Sphere(Texture).zgeMesh,[plugpath]Content\Meshes\Sphere.zgeMesh,[plugpath]Content\Meshes\Sphere.zgeMesh,"[plugpath]Content\Meshes\Spheres 1.zgeMesh",[plugpath]Content\Meshes\Torus.zgeMesh,"[plugpath]Content\Meshes\World map.zgeMesh"
Images=[plugpath]Effects\HUD\prefabs\components\component-001.ilv,[plugpath]Effects\HUD\prefabs\components\component-002.ilv,[plugpath]Effects\HUD\prefabs\components\component-003.ilv,[plugpath]Effects\HUD\prefabs\components\component-004.ilv,[plugpath]Effects\HUD\prefabs\components\component-005.ilv,[plugpath]Effects\HUD\prefabs\components\component-006.ilv,[plugpath]Effects\HUD\prefabs\components\component-007.ilv,[plugpath]Effects\HUD\prefabs\components\component-008.ilv,[plugpath]Effects\HUD\prefabs\components\component-009.ilv,[plugpath]Effects\HUD\prefabs\components\component-010.ilv,[plugpath]Effects\HUD\prefabs\components\component-011.ilv,[plugpath]Effects\HUD\prefabs\components\component-012.ilv,[plugpath]Effects\HUD\prefabs\components\component-013.ilv,[plugpath]Effects\HUD\prefabs\components\component-014.ilv,[plugpath]Effects\HUD\prefabs\components\component-015.ilv,[plugpath]Effects\HUD\prefabs\components\component-016.ilv,[plugpath]Effects\HUD\prefabs\components\component-017.ilv,[plugpath]Effects\HUD\prefabs\components\component-018.ilv,[plugpath]Effects\HUD\prefabs\components\component-019.ilv,[plugpath]Effects\HUD\prefabs\components\component-020.ilv,[plugpath]Effects\HUD\prefabs\components\component-021.ilv,[plugpath]Effects\HUD\prefabs\components\component-022.ilv,[plugpath]Effects\HUD\prefabs\components\component-023.ilv,[plugpath]Effects\HUD\prefabs\components\component-024.ilv,[plugpath]Effects\HUD\prefabs\components\component-025.ilv,[plugpath]Effects\HUD\prefabs\components\component-026.ilv,[plugpath]Effects\HUD\prefabs\components\component-027.ilv,[plugpath]Effects\HUD\prefabs\components\component-028.ilv,[plugpath]Effects\HUD\prefabs\components\component-029.ilv,[plugpath]Effects\HUD\prefabs\components\component-030.ilv,[plugpath]Content\Bitmaps\Particles\earth1.png
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

