<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Comment="PreAlphaMode: Source image is in premultiplied alpha format." ClearColor="0.051 0.051 0.051 1" CameraPosition="0 0 0.5" LightPosition="0 1 1" FOV="90" FileVersion="2">
  <OnLoaded>
    <ZExternalLibrary Comment="Vector graphics library" ModuleName="ZgeNano" CallingConvention="1">
      <Source>
<![CDATA[/*
  ZgeNano Library; vector graphics rendering library
  build on NanoVG: https://github.com/memononen/nanovg
  and NanoSVG: https://github.com/memononen/nanosvg

  Version: 1.4 (2018-09-10)

  Download Windows DLL and Android shared library from
  https://github.com/Rado-1/ZgeNano/releases

  Project home
  https://github.com/Rado-1/ZgeNano

  Copyright (c) 2016-2018 Radovan Cervenka
*/

// init flags
const int
  NVG_ANTIALIAS = 1<<0,
	NVG_STENCIL_STROKES = 1<<1,
	NVG_DEBUG = 1<<2;

// winding (direction of arcs)
const int
  NVG_CCW = 1, // counter-clockwise
  NVG_CW = 2; // clockwise

// solidity
const int
  NVG_SOLID = 1,
	NVG_HOLE = 2;

// line caps
const int
	NVG_BUTT = 0,
	NVG_ROUND = 1,
	NVG_SQUARE = 2,
	NVG_BEVEL = 3,
	NVG_MITER = 4;

// align
const int
	// Horizontal align
	NVG_ALIGN_LEFT 		= 1<<0,	// Default, align text horizontally to left.
	NVG_ALIGN_CENTER 	= 1<<1,	// Align text horizontally to center.
	NVG_ALIGN_RIGHT 	= 1<<2,	// Align text horizontally to right.
	// Vertical align
	NVG_ALIGN_TOP 		= 1<<3,	// Align text vertically to top.
	NVG_ALIGN_MIDDLE	= 1<<4,	// Align text vertically to middle.
	NVG_ALIGN_BOTTOM	= 1<<5,	// Align text vertically to bottom.
	NVG_ALIGN_BASELINE	= 1<<6; // Default, align text vertically to baseline.

// blend factor
const int
	NVG_ZERO = 1<<0,
	NVG_ONE = 1<<1,
	NVG_SRC_COLOR = 1<<2,
	NVG_ONE_MINUS_SRC_COLOR = 1<<3,
	NVG_DST_COLOR = 1<<4,
	NVG_ONE_MINUS_DST_COLOR = 1<<5,
	NVG_SRC_ALPHA = 1<<6,
	NVG_ONE_MINUS_SRC_ALPHA = 1<<7,
	NVG_DST_ALPHA = 1<<8,
	NVG_ONE_MINUS_DST_ALPHA = 1<<9,
	NVG_SRC_ALPHA_SATURATE = 1<<10;

// composite operation
const int
	NVG_SOURCE_OVER = 0,
	NVG_SOURCE_IN = 1,
	NVG_SOURCE_OUT = 2,
	NVG_ATOP = 3,
	NVG_DESTINATION_OVER = 4,
	NVG_DESTINATION_IN = 5,
	NVG_DESTINATION_OUT = 6,
	NVG_DESTINATION_ATOP = 7,
	NVG_LIGHTER = 8,
	NVG_COPY = 9,
	NVG_XOR = 10;

// image flags
const int
  NVG_IMAGE_GENERATE_MIPMAPS	= 1<<0,     // Generate mipmaps during creation of the image.
	NVG_IMAGE_REPEATX			= 1<<1,		// Repeat image in X direction.
	NVG_IMAGE_REPEATY			= 1<<2,		// Repeat image in Y direction.
	NVG_IMAGE_FLIPY				= 1<<3,		// Flips (inverses) image in Y direction when rendered.
	NVG_IMAGE_PREMULTIPLIED		= 1<<4;		// Image data has premultiplied alpha.

// Init
xptr nvg_Init(int flags) {} // Returns NanoVG context used in other functions.
void nvg_SetContext(xptr context) {}
void nvg_Finish(xptr context) {}
int nvg_SetViewport(xptr context) {} // Returns 1 if changed viewport, 0 otherwise.

// Drawing
void nvg_BeginFrame() {}
void nvg_CancelFrame() {}
void nvg_EndFrame() {}

// Global composite operation
void nvg_GlobalCompositeOperation(int op) {}
void nvg_GlobalCompositeBlendFunc(int sfactor, int dfactor) {}
void nvg_GlobalCompositeBlendFuncSeparate(int srcRGB, int dstRGB, int srcAlpha, int dstAlpha) {}

// State handling
void nvg_Save() {}
void nvg_Restore() {}
void nvg_Reset() {}

// Render styles
void nvg_StrokeColor(float r, float g, float b, float a) {}
void nvg_StrokePaint(xptr paint) {} // paint - result of Paints functions
void nvg_FillColor(float r, float g, float b, float a) {}
void nvg_FillPaint(xptr paint) {} // paint - result of Paints functions
void nvg_MiterLimit(float limit) {}
void nvg_StrokeWidth(float size) {}
void nvg_LineCap(int cap) {}
void nvg_LineJoin(int join) {}
void nvg_GlobalAlpha(float alpha) {}

// Transformations
void nvg_ResetTransform() {}
void nvg_Transform(float a, float b, float c, float d, float e, float f) {}
void nvg_Translate(float x, float y) {}
void nvg_Rotate(float angle) {}
void nvg_SkewX(float angle) {}
void nvg_SkewY(float angle) {}
void nvg_Scale(float x, float y) {}

// Images
int nvg_CreateImage(string filename, int imageFlags) {}
int nvg_CreateImageMem(int imageFlags, xptr data, int ndata) {}
int nvg_CreateImageRGBA(int w, int h, int imageFlags, xptr data) {}
void nvg_UpdateImage(int image, xptr data) {}
void nvg_ImageSize(int image, ref int w, ref int h) {}
void nvg_DeleteImage(int image) {}

// Paints
xptr nvg_LinearGradient(float sx, float sy, float ex, float ey,
	float ir, float ig, float ib, float ia,
	float or, float og, float ob, float oa) {}
xptr nvg_BoxGradient(float x, float y, float w, float h, float r, float f,
	float ir, float ig, float ib, float ia,
	float or, float og, float ob, float oa) {}
xptr nvg_RadialGradient(float cx, float cy, float inr, float outr,
	float ir, float ig, float ib, float ia,
	float or, float og, float ob, float oa) {}
xptr nvg_ImagePattern(float ox, float oy, float ex, float ey,
	float angle, int image, float alpha) {}
void nvg_FreePaint(xptr paint) {}

// Scissoring
void nvg_Scissor(float x, float y, float w, float h) {}
void nvg_IntersectScissor(float x, float y, float w, float h) {}
void nvg_ResetScissor() {}

// Paths
void nvg_BeginPath() {}
void nvg_MoveTo(float x, float y) {}
void nvg_LineTo(float x, float y) {}
void nvg_BezierTo(float c1x, float c1y, float c2x, float c2y, float x, float y) {}
void nvg_QuadTo(float cx, float cy, float x, float y) {}
void nvg_ArcTo(float x1, float y1, float x2, float y2, float radius) {}
void nvg_ClosePath() {}
void nvg_PathWinding(int dir) {}
void nvg_Arc(float cx, float cy, float r, float a0, float a1, int dir) {}
void nvg_Rect(float x, float y, float w, float h) {}
void nvg_RoundedRect(float x, float y, float w, float h, float r) {}
void nvg_RoundedRectVarying(float x, float y, float w, float h,
	float radTopLeft, float radTopRight, float radBottomRight, float radBottomLeft) {}
void nvg_Ellipse(float cx, float cy, float rx, float ry) {}
void nvg_Circle(float cx, float cy, float r) {}
void nvg_Fill() {}
void nvg_Stroke() {}
void nvg_StrokeNoScale() {}

// Text
int nvg_CreateFont(string name, string filename) {}
int nvg_CreateFontMem(string name, xptr data, int ndata, int freeData) {}
int nvg_FindFont(string name) {}
int nvg_AddFallbackFontId(int baseFont, int fallbackFont) {}
int nvg_AddFallbackFont(string baseFont, string fallbackFont) {}
void nvg_FontSize(float size) {}
void nvg_FontBlur(float blur) {}
void nvg_TextLetterSpacing(float spacing) {}
void nvg_TextLineHeight(float lineHeight) {}
void nvg_TextAlign(int align) {}
void nvg_FontFaceId(int font) {}
void nvg_FontFace(string font) {}
void nvg_Text(float x, float y, string str, string end) {}
void nvg_TextBox(float x, float y, float breakRowWidth, string str, string end) {}
float nvg_TextBounds(float x, float y, string str, string end, vec4 bounds) {}
void nvg_TextBoxBounds(float x, float y, float breakRowWidth, string str, string end, vec4 bounds) {}
void nvg_TextMetrics(ref float ascender, ref float descender, ref float lineh) {}

// SVG support
xptr nsvg_ParseFromFile(string filename, string units, float dpi) {}
xptr nsvg_ParseMem(string data, int ndata, string units, float dpi) {}
void nsvg_ImageSize(xptr image, ref float width, ref float height) {}
int nsvg_ImageShapeCount(xptr image, string shapeIdPrefix) {}
void nsvg_Draw(xptr image, string shapeIdPrefix,
  int strokeWidthScaling, float strokeWidthFactor, float buildUpFromFactor, float buildUpToFactor, xptr color) {}
void nsvg_Rasterize(xptr image, float tx, float ty, float scale, xptr dst, int w, int h) {}
void nsvg_Delete(xptr image) {}]]>
      </Source>
    </ZExternalLibrary>
    <ZLibrary Comment="HSV convertion by Kjell">
      <Source>
<![CDATA[float angle(float X)
{
  if(X >= 0 && X < 360)return X;
  if(X > 360)return X-floor(X/360)* 360;
  if(X <   0)return X+floor(X/360)*-360;
}
  //used for the color convertion from RGB to HSV
void hsv(float H, float S, float V)
{
  float R,G,B,I,F,P,Q,T;

  H = angle(H);
  S = clamp(S,0,100);
  V = clamp(V,0,100);

  H /= 60;
  S /= 100;
  V /= 100;

  if(S == 0)
  {
    Color[0] = V;
    Color[1] = V;
    Color[2] = V;
    return;
  }

  I = floor(H);
  F = H-I;

  P = V*(1-S);
  Q = V*(1-S*F);
  T = V*(1-S*(1-F));

  if(I == 0){R = V; G = T; B = P;}
  if(I == 1){R = Q; G = V; B = P;}
  if(I == 2){R = P; G = V; B = T;}
  if(I == 3){R = P; G = Q; B = V;}
  if(I == 4){R = T; G = P; B = V;}
  if(I == 5){R = V; G = P; B = Q;}

  Color[0] = R;
  Color[1] = G;
  Color[2] = B;
}]]>
      </Source>
    </ZLibrary>
    <SpawnModel Comment="spawn the main object" Model="o_object" SpawnStyle="1"/>
    <ZLibrary>
      <Source>
<![CDATA[void nvg_AncorRotate(float x, float y, float angle)
{
	nvg_Translate(x, y);
	nvg_Rotate(angle);
	nvg_Translate(x * -1.0, y * -1.0);
}

float Mix(float y0, float y1, float x)
{
	return(y0*(1 - x) + y1 * x);
}


void AddImage()
{
	for (int i = 0; i < ArrayVar.SizeDim1; i++)
	{
		if (ArrayVarOutOfBounds[i] == 1)
		{
			ArrayVar[i] = MinSizeVar;
      ArrayRotationVar[i] = AngleVar;
      TranslateVar[i, 0] = TranslateX;
      TranslateVar[i, 1] = TranslateY;

			ArrayVarOutOfBounds[i] = 0;
			return;
		}
	}
}

void AddInitImage(int index, int size)
{
	for (int i = 0; i < ArrayVar.SizeDim1; i++)
	{
		if (ArrayVarOutOfBounds[i] == 1)
		{
    	if (OrtoParam < 0.5) ArrayVar[i] = MinSizeVar * pow(SpeedVar,size - index);
      else ArrayVar[i] = 0 + SpeedVar * index;

      ArrayRotationVar[i] = AngleVar + RotationVar * (size - index);

  		if (ArrayVar[i] > MaxSizeVar) ArrayVarOutOfBounds[i] = 1;
			else ArrayVarOutOfBounds[i] = 0;

			return;
		}
	}
}

void Iterate(int iteration)
{
  float iter = iteration;

  if (OrtoParam < 0.5)
  {
   ArrayVar[iteration] *= SpeedVar;
  }
  else if (ArrayVar[iteration] > 0.0) ArrayVar[iteration] += SpeedVar;

  ArrayRotationVar[iteration] += RotationVar;

	if (ArrayVar[iteration] > MaxSizeVar)
	{
		ArrayVar[iteration] = 0.0;
    ArrayRotationVar[iteration] = AngleVar;
		ArrayVarOutOfBounds[iteration] = 1;
	}

	rtg.Scale.X = ScaleXVar;
	rtg.Scale.Y = ScaleYVar;

	rtg.Scale *= ArrayVar[iteration];
  rtg.Rotate.Z = ArrayRotationVar[iteration];

  rtg.Translate.X = TranslateVar[iteration, 0];
  rtg.Translate.Y = TranslateVar[iteration, 1];

  set_c.color.A = Mix(set_c.color.A, ArrayVar[iteration], FadeInVar);
}

void Reset()
{
	float stepIterator = 0.0;
	CounterVar = 0;
	int eps = 60.0 / ElementsCountVar;

	for (int i = 0; i < ArrayVar.SizeDim1; i++)
	{
		ArrayVar[i] = stepIterator;
		ArrayVarOutOfBounds[i] = 1;
    ArrayRotationVar[i] = AngleVar;
	}

	int sec = eps * 1024; // Precalculate. This will cover whole range.
	for (int j = 0; j < sec; j++)
	{
		if (CounterVar % eps == 0)
		{
			AddInitImage(j, sec);
		}
		CounterVar++;
	}
}

// callback on changed OpenGL context
void OnGLContextChange() {
  // reset NanoVG
  @CallComponent(Component: InitNanoVG);
  Reset();
}]]>
      </Source>
    </ZLibrary>
    <ZExpression Name="InitNanoVG">
      <Expression>
<![CDATA[// init NanoVG
NvgContext = nvg_Init(NVG_STENCIL_STROKES);]]>
      </Expression>
    </ZExpression>
  </OnLoaded>
  <OnUpdate>
    <ZExpression Comment="update from sliders">
      <Expression>
<![CDATA[float speed = 0;

if (OrtoParam > 0.5) speed = (Parameters[11] * Parameters[11]) * 0.125;
else speed = (Parameters[11] * Parameters[11]) * 0.125 + 1.001;

ElementsCountVar = Parameters[12] * Parameters[12] * 10 + 0.1;
MinSizeVar = Parameters[13] * Parameters[13] + 0.005;
MaxSizeVar = Parameters[14] * 10 + 0.01;
RotationVar = (Parameters[15] - 0.5) * 0.05;
MasterRotationVar = (Parameters[16] - 0.5) * 0.05;
AngleVar = Parameters[9];
TranslateX = (Parameters[17] - 0.5) * 2;
TranslateY = (Parameters[18] - 0.5) * 2;
FadeInVar = Parameters[19];
OrtoParam = Parameters[22];

if (speed != SpeedVar)
{
  SpeedVar = speed;
  Reset();
}


//call the color convertion script
hsv(Parameters[1]*360,Parameters[2]*100,(1-Parameters[3])*100);
uAlpha=Parameters[0];

//size can be done by moveing Camera
app.CameraPosition.z=4-Parameters[4]*3.5;
  //or changeing object scale
  //CurrentModel.Scale=Parameters[4]*2;

app.cameraPosition.x=(-0.5+Parameters[5])*-5;
app.cameraPosition.y=(-0.5+Parameters[6])*5;


if(Parameters[10])
  m_basic.Blend=6;  //Premultiplied alpha mode
else
  m_basic.Blend=1;  //Normal blend]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <OnClose>
    <ZExpression Name="FinishNanoVG" Expression="nvg_Finish(NvgContext);"/>
  </OnClose>
  <Content>
    <Group Comment="values and such">
      <Children>
        <Array Name="Color" SizeDim1="3"/>
        <Array Name="Parameters" SizeDim1="25" Persistent="255">
          <Values>
<![CDATA[78DA636040070DF6400286D1E5EC20E2301A1517331433F03270C1D431C0C40102B4066D]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Comment="Add new Parametere names here, each new line creates a new slider in the GUI." Type="2">
          <StringValue>
<![CDATA[Alpha
Hue
Saturation
Lightness
Size
Position X
Position Y
Width
Height
Angle
PreAlphaMode @checkbox
Speed
Elements
Min Size
Max Size
Rotation
Master Rotation
Start X
Start Y
Fade In
Source @list1000: "Image", "Uniform Triangle", "Uniform Rect", "Uniform Circle", "Uniform Hex"
Uni. Tickness
Orto Mode @checkbox
End X
End Y]]>
          </StringValue>
        </Constant>
        <Constant Name="AuthorInfo" Type="2" StringValue="Youlean"/>
      </Children>
    </Group>
    <Group Comment="assets">
      <Children>
        <Bitmap Name="b_texture" Comment="Anti-Aliased Polygon" Width="256" Height="256" Filter="2">
          <Producers>
            <BitmapExpression UseBlankSource="1">
              <Expression>
<![CDATA[//X,Y : current coordinate (0..1)
//Pixel : current color (rgb)

// bitmap variables

int width = 256;
float aawidth = 1;

// algo variables

int sprockets = 6;
int centeroffset = 0;

// default color

pixel.r = 1;
Pixel.g = 1;
pixel.b = 1;
Pixel.A = 0;

//

// other variables

float this_angle;
int this_sprocket;
float normalx, normaly;
float distance;

float aadist = aawidth / width;
float angle =( 2 * PI)/ sprockets;
float maxdist = cos( angle / 2.0 )* 0.48;
float center = 0.5 -( 1.0 * centeroffset / width );

//

// determine angle of current pixel to center
if( x - 0.5 >= 0 )
  this_angle = atan2(( x - 0.5 ),( y - center ));
else
  this_angle = PI + atan2(( 0.5 - x ),( center - y  ));

// determine sprocket of current pixel
this_sprocket = (( this_angle )/ angle );

// calculate normal of current sprocket
normalx = sin(( angle * this_sprocket )+( angle / 2.0 ));
normaly = cos(( angle * this_sprocket )+( angle / 2.0 ));

// calculate distance of current pixel by dot product with normal
distance =(( x - 0.5 )* normalx )+(( Y - center )* normaly );

// set alpha accordingly
if( distance < maxdist ) pixel.a = ( maxdist - distance )/ aadist;

Pixel.R *= Pixel.A;
Pixel.G *= Pixel.A;
Pixel.B *= Pixel.A;]]>
              </Expression>
            </BitmapExpression>
            <BitmapExpression UseBlankSource="1">
              <Expression>
<![CDATA[//X,Y : current coordinate (0..1)
//Pixel : current color (rgb)

// bitmap variables

int width = 256;
float aawidth = 1;

// algo variables

int sprockets = 6;
int centeroffset = 0;

// default color

pixel.r = 1;
Pixel.g = 1;
pixel.b = 1;
Pixel.A = 0;

//

// other variables

float this_angle;
int this_sprocket;
float normalx, normaly;
float distance;

float aadist = aawidth / width;
float angle =( 2 * PI)/ sprockets;
float maxdist = cos( angle / 2.0 )* 0.5;
float center = 0.5 -( 1.0 * centeroffset / width );

//

// determine angle of current pixel to center
if( x - 0.5 >= 0 )
  this_angle = atan2(( x - 0.5 ),( y - center ));
else
  this_angle = PI + atan2(( 0.5 - x ),( center - y  ));

// determine sprocket of current pixel
this_sprocket = (( this_angle )/ angle );

// calculate normal of current sprocket
normalx = sin(( angle * this_sprocket)+( angle / 2.0));
normaly = cos(( angle * this_sprocket)+( angle / 2.0));

// calculate distance of current pixel by dot product with normal
distance =(( x - 0.5 )* normalx )+(( Y - center )* normaly );

// set alpha accordingly
if( distance < maxdist ) pixel.a = ( maxdist - distance )/ aadist;

Pixel.R *= Pixel.A;
Pixel.G *= Pixel.A;
Pixel.B *= Pixel.A;]]>
              </Expression>
            </BitmapExpression>
            <BitmapCombine Method="1"/>
          </Producers>
        </Bitmap>
        <Material Name="m_basic" Shading="1" Blend="1">
          <Textures>
            <MaterialTexture Texture="b_texture" TexCoords="1"/>
          </Textures>
        </Material>
      </Children>
    </Group>
    <Model Name="o_object">
      <Definitions>
        <Group Name="DoRenderSprite">
          <Children>
            <RenderSetColor Name="set_c" Color="1 1 1 1"/>
            <RenderTransformGroup Name="rtg" Scale="0 0 0">
              <Children>
                <RenderSprite/>
              </Children>
            </RenderTransformGroup>
          </Children>
        </Group> <!-- DoRenderSprite -->

      </Definitions>
      <OnUpdate>
        <ZExpression>
          <Expression>
<![CDATA[@UseMaterial(Material: m_basic);

int TextureWidth = b_Texture.Width;
int TextureHeight = b_Texture.Height;

float r = TextureWidth * 1f / TextureHeight;

ScaleXVar=(1 + Parameters[7]*4) * r;
ScaleYVar=1 + Parameters[8]*4;

//if not using a shader color can be sent to a color component this sould be removed when using a shader.
set_c.color.R=Color[0];
set_c.color.G=Color[1];
set_c.color.B=Color[2];
set_c.color.A=(1-Parameters[0]);


app.CameraRotation.z += MasterRotationVar;

int eps = 60.0 / ElementsCountVar;

if (CounterVar % eps == 0)
{
   AddImage();
}

float fps = clamp(App.FpsCounter, 15, 60);
float fpsAjust = 60 / fps;

CounterVar++;]]>
          </Expression>
        </ZExpression>
      </OnUpdate>
      <OnRender>
        <UseMaterial Material="m_basic"/>
        <ZExpression>
          <Expression>
<![CDATA[const float PI = 3.14159265359;

float ScaleParam = Parameters[4];
float AngleParam = Parameters[9] * 3.14;
float TicknessParam = Parameters[21] * Parameters[21] + 0.002;

float EndXParam = (Parameters[23] - 0.5) * 5;
float EndYParam = (Parameters[24] - 0.5) * 5;

float ViewportWidth = App.ViewportWidth;
float ViewportHeight = App.ViewportHeight;
float AspectRatio = ViewportWidth / ViewportHeight;

int SourceParam = floor(Parameters[20] * 1000);

nvg_SetViewport(NvgContext);
nvg_SetContext(NvgContext);
nvg_BeginFrame();

for(int iteration=0; iteration<ArrayVar.SizeDim1; iteration++) {

  Iterate(iteration);

  nvg_Reset();

  // Init NanoVG scaling
  nvg_Scale(ViewportWidth / 2.0 / AspectRatio * ScaleParam,  ViewportHeight / 2.0 * ScaleParam);
  nvg_Translate(1 * AspectRatio / ScaleParam, 1 / ScaleParam);

  nvg_Translate(TranslateVar[iteration, 0], TranslateVar[iteration, 1]);
  nvg_Translate(app.cameraPosition.x * -2, app.cameraPosition.y * 2);

  nvg_Rotate(app.CameraRotation.z * PI * -4);

  nvg_Scale(ScaleXVar, ScaleYVar);

  nvg_GlobalAlpha(1.0 - uAlpha);

  nvg_StrokeColor(Color[0], Color[1], Color[2], set_c.color.A);
  nvg_LineJoin(NVG_MITER);

  float invScale = 1.0 / ScaleParam;

  switch(SourceParam) {
  // Triangle
    case 0 : {
      float scale = ArrayVar[iteration];
      rtg.Translate.X += scale * EndXParam;
      rtg.Translate.Y += scale * EndYParam * -1;
      @CallComponent(Component : DoRenderSprite);
      break;
  }
    case 1 :
  {
     float scale = ArrayVar[iteration] * 4.15 / 2;

     float p1x = 0;
     float p1y = 0;
     float p2x = scale;
     float p2y = 0;

     float s60 = 0.86602540378;//sin(60 * PI / 180.0);
     float c60 = 0.5;//cos(60 * PI / 180.0);

     float p3x = c60 * (p1x - p2x) - s60 * (p1y - p2y) + p2x;
     float p3y = s60 * (p1x - p2x) + c60 * (p1y - p2y) + p2y;

     nvg_StrokeWidth(0.1 * TicknessParam * invScale);

     nvg_Translate(scale * EndXParam, scale * EndYParam);
     nvg_Rotate(ArrayRotationVar[iteration] * PI * -2);
     nvg_Translate(scale / -2, scale / 3.5);


     nvg_BeginPath();

     nvg_MoveTo(p1x, p1y);
     nvg_LineTo(p2x, p2y);
     nvg_LineTo(p3x, p3y);
     nvg_LineTo(p1x, p1y);
     nvg_Stroke();
     break;
  }

  // Rect
    case 2 :
  {
      float scale = ArrayVar[iteration] * 4.15 / 2;

     float p1x = 0;
     float p1y = 0;
     float p2x = scale;
     float p2y = scale;

      nvg_Translate(scale * EndXParam, scale * EndYParam);
     nvg_Rotate(ArrayRotationVar[iteration] * PI * -2);
     nvg_Translate(scale / -2, scale / -2);

      nvg_StrokeWidth(0.1 * TicknessParam * invScale);
      nvg_BeginPath();
      nvg_Rect(p1x, p1y, p2x, p2y);
      nvg_Stroke();
      break;
  }

  // Circle
    case 3 :
  {
      float scale = ArrayVar[iteration] * 4.15 / 2;

     float p1x = 0;
     float p1y = 0;

      nvg_Translate(scale * EndXParam, scale * EndYParam);
     nvg_Rotate(ArrayRotationVar[iteration] * PI * -2);

      nvg_StrokeWidth(0.1 * TicknessParam * invScale);
      nvg_BeginPath();
      nvg_Circle(p1x, p1y, scale / 2);
      nvg_Stroke();
      break;
  }

  // Hex
    case 4 :
  {
      float scale = ArrayVar[iteration] * 4.15 / 4;

     float p1x = 0;
     float p1y = 0;

         nvg_Translate(scale * EndXParam, scale * EndYParam);
     nvg_Rotate(ArrayRotationVar[iteration] * PI * -2);
      nvg_StrokeWidth(0.1 * TicknessParam * invScale);

      nvg_Save();

      nvg_BeginPath();

      float height = Sqrt((scale * scale) - ((scale / 2) * (scale / 2)));
      for(int i = 0; i < 6; i++)
      {
        if (i == 0) nvg_MoveTo(p1x - scale / 2, p1y - height);
        else  nvg_LineTo(p1x - scale / 2, p1y - height);

        nvg_LineTo(p1x + scale / 2, p1y - height);
        nvg_Rotate(PI / 3);
      }

      nvg_Stroke();
      nvg_Restore();
      break;
  }
  }
}
nvg_EndFrame();]]>
          </Expression>
        </ZExpression>
      </OnRender>
    </Model>
    <Variable Name="SpeedVar"/>
    <Array Name="ArrayVar" SizeDim1="1024"/>
    <Array Name="ArrayRotationVar" SizeDim1="1024"/>
    <Array Name="ArrayVarOutOfBounds" Type="1" SizeDim1="1024"/>
    <Variable Name="MinSizeVar"/>
    <Variable Name="CounterVar" Type="1"/>
    <Variable Name="ElementsCountVar"/>
    <Variable Name="MaxSizeVar"/>
    <Variable Name="ScaleXVar"/>
    <Variable Name="ScaleYVar"/>
    <Variable Name="RotationVar"/>
    <Variable Name="MasterRotationVar"/>
    <Variable Name="AngleVar"/>
    <Array Name="TranslateVar" Dimensions="1" SizeDim1="1024" SizeDim2="2"/>
    <Variable Name="TranslateX"/>
    <Variable Name="TranslateY"/>
    <Variable Name="FadeInVar"/>
    <Variable Name="NvgContext" Type="9"/>
    <Variable Name="uAlpha"/>
    <Variable Name="OrtoParam"/>
  </Content>
</ZApplication>
