FLhd   0 * ` FLdt�  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ���  ﻿[General]
GlWindowMode=1
LayerCount=8
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,4,5,1,3,2,7,8

[AppSet0]
App=Canvas effects\Flaring
FParamValues=0,1,1,1,0.256,0.5,0.5,0.209,0,0,0
ParamValues=0,1000,1000,1000,256,500,500,209,0,0,0
ParamValuesCanvas effects\Digital Brain=0,0,0,0,620,500,500,412,520,300,180,310,904,500,1000,0
ParamValuesCanvas effects\Electric=0,740,0,928,260,500,500,56,0,1000,500
Enabled=1
Collapsed=1
Name=EFX 1

[AppSet4]
App=Postprocess\ColorCyclePalette
FParamValues=1,5,8,1,10,0.38,0.608,0.42,0.752,0,0
ParamValues=1,5,8,1,10,380,608,420,752,0,0
ParamValuesFeedback\WarpBack=0,0,0,0,500,500,24
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesHUD\HUD Grid=0,500,0,0,500,500,1000,1000,1000,444,500,1000,500,604,284,500,1000,40,644,0,1000,1000
ParamValuesFeedback\BoxedIn=0,0,0,1000,1000,332,232,500,0,500
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesFeedback\FeedMe=0,0,0,1000,500,1000,476
ParamValuesTerrain\CubesAndSpheres=0,0,0,1000,0,500,500,650,540,400,1000,1000,1000,1000,1000,1000,0,1000
ParamValuesFeedback\70sKaleido=0,0,0,1000,484,500
ParamValuesPostprocess\Youlean Color Correction=308,500,512,500,836,904
ParamValuesFeedback\FeedMeFract=0,0,0,1000,396,264
ParamValuesPostprocess\Youlean Motion Blur=484,1000,732,764
Enabled=1
Collapsed=1
Name=EFX 2

[AppSet5]
App=Feedback\WormHoleEclipse
FParamValues=0,0,0,0.656,0.84,0.824,0.296,0.196,0.436,0.58,0.5
ParamValues=0,0,0,656,840,824,296,196,436,580,500
ParamValuesFeedback\BoxedIn=0,0,0,1000,1000,0,312,500,0,1000
ParamValuesFeedback\FeedMeFract=0,0,0,1000,1000,72
Enabled=1
Collapsed=1
Name=EFX 3

[AppSet1]
App=Background\FourCornerGradient
FParamValues=5,0.568,0.308,0.768,0.74,0.414,0.724,0.74,0.278,0.892,0.74,0.278,0.82,0.756
ParamValues=5,568,308,768,740,414,724,740,278,892,740,278,820,756
Enabled=1
Collapsed=1
Name=Color FIXER

[AppSet3]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.472,0.472,0.384,0.35,1,0,0,0
ParamValues=472,472,384,350,1000,0,0,0
Enabled=1
Collapsed=1
Name=Master Out

[AppSet2]
App=Postprocess\ColorCyclePalette
FParamValues=1,4,4,0,0,1,0.252,0.5,0.3,0,0.16
ParamValues=1,4,4,0,0,1000,252,500,300,0,160
ParamValuesFeedback\WarpBack=500,0,0,0,36,500,1000
ParamValuesFeedback\WormHoleDarkn=0,0,0,1000,0,756,500,500,500,500,500,500
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,1000,908,500,836,556,500,500
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesFeedback\SphericalProjection=60,0,0,0,1000,425,0,590,500,500,0,500,530,1000,0,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesPostprocess\Youlean Color Correction=404,544,672,620,744,680
ParamValuesFeedback\FeedMe=0,0,0,1000,500,1000,504
Enabled=1
Collapsed=1
Name=Color Out

[AppSet7]
App=Text\TextTrueType
FParamValues=0.076,0,0,0,0,0.5,0.492,0,0,0,0.5
ParamValues=76,0,0,0,0,500,492,0,0,0,500
Enabled=1
Collapsed=1
Name=Main Text

[AppSet8]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fade in-out

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""4"" y=""5.2""><p><font face=""American-Captain"" size=""5.2"" color=""#000"">[author]</font></p></position>","<position x=""4"" y=""9""><p><font face=""Chosence-Bold"" size=""3.2"" color=""#000"">[title]</font></p></position>","<position x=""4"" y=""15.8""><p> <font face=""Chosence-Bold"" size=""2.7"" color=""#000"">[comment]</font></p></position>"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

