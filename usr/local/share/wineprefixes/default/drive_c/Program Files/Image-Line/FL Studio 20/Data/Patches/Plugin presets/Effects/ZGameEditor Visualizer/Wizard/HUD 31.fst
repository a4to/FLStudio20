FLhd   0  0 FLdt  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   Ր%�  ﻿[General]
GlWindowMode=1
LayerCount=22
FPS=2
MidiPort=-1
Aspect=1
LayerOrder=7,12,0,1,4,5,2,3,22,15,21,16,18,20,17,19,6,11,9,10,23,8
WizardParams=1139,1140,226,517,518,519,520,521,82

[AppSet7]
App=Text\TextTrueType
ParamValues=0,0,0,1000,0,500,492,0,0,0,500
Enabled=1
Collapsed=1

[AppSet12]
App=Postprocess\Youlean Blur
ParamValues=380,0,1000,0,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet0]
App=Background\SolidColor
ParamValues=0,0,0,840
Enabled=1
Collapsed=1

[AppSet1]
App=HUD\HUD Prefab
ParamValues=1,974,200,804,0,500,500,326,1000,1000,4,0,500,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4B4A4CCE4E2FCA2FCD4B89490232750D0C0CF53273CA18460A0000F04C0845

[AppSet4]
App=HUD\HUD Prefab
ParamValues=4,26,788,776,0,500,500,326,1000,1000,4,0,500,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4B4A4CCE4E2FCA2FCD4B89490232750D0C4CF43273CA18460A0000F3160848

[AppSet5]
App=Misc\Automator
ParamValues=1,2,2,2,500,29,250,1,5,2,2,500,29,750,0,3,5,2,500,71,487,0,0,0,0,0,250,250
Enabled=1
Collapsed=1

[AppSet2]
App=Postprocess\Point Cloud High
ParamValues=0,330,330,520,519,618,444,503,330,1000,0,0,0,0,2
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet3]
App=Misc\Automator
ParamValues=1,3,4,2,500,11,478,1,3,6,2,500,29,438,1,3,5,2,500,71,489,0,0,0,0,0,250,250
Enabled=1
Collapsed=1

[AppSet22]
App=HUD\HUD Prefab
ParamValues=219,440,500,0,1000,357,254,250,528,1000,4,1,500,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0606467A9939650C23030000A2990907

[AppSet15]
App=HUD\HUD Prefab
ParamValues=219,0,500,0,0,357,254,250,528,1000,4,1,500,1,368,0,0,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0606467A9939650C23030000A2990907

[AppSet21]
App=HUD\HUD Prefab
ParamValues=219,532,500,0,1000,641,254,250,528,1000,4,0,500,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0606467A9939650C23030000A2990907

[AppSet16]
App=HUD\HUD Prefab
ParamValues=219,0,500,0,0,641,254,250,528,1000,4,0,500,1,368,0,0,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0606467A9939650C23030000A2990907

[AppSet18]
App=HUD\HUD Prefab
ParamValues=23,596,500,0,0,500,257,19,1000,1000,4,0,0,1,828,0,0,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C8DF53273CA18863D00000B870A9F

[AppSet20]
App=Misc\Automator
ParamValues=1,19,17,2,500,170,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1
Collapsed=1

[AppSet17]
App=Postprocess\ParameterShake
ParamValues=1000,0,15,16,1000,0,16,16,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet19]
App=Postprocess\ParameterShake
ParamValues=500,500,18,16,1000,0,18,12,8,3,1,0,6,6,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet6]
App=Postprocess\Youlean Bloom
ParamValues=556,0,274,102,1000,0,0,0
Enabled=1
Collapsed=1

[AppSet11]
App=Image effects\Image
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet9]
App=Image effects\ImageBox
ParamValues=332,0,0,1000,32,500,593,500,500,500,0,0,0,0,0,0,500,500,500
ParamValuesHUD\HUD Prefab=0,0,500,0,1000,500,604,440,1000,1000,444,0,500,1000,368,0,1000,1000
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet10]
App=HUD\HUD Prefab
ParamValues=0,0,500,0,0,500,604,440,1000,1000,4,0,500,1,368,0,1000,1
Enabled=1
Name=LOGO
LayerPrivateData=78DA73C8CBCF4BD52B2E4B671899000060F9036F

[AppSet23]
App=Postprocess\Youlean Color Correction
ParamValues=500,500,500,500,500,500
Enabled=1
Collapsed=1

[AppSet8]
App=Text\TextTrueType
ParamValues=0,0,0,0,0,500,500,0,0,0,500
Enabled=1
Collapsed=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0

[UserContent]
Text="This is the default text."
Html="<position x=""0""><position y=""8""><p align=""center""><font face=""American-Captain"" size=""12"" color=""#ffffff"">[author]</font></p></position>","<position x=""0""><position y=""30""><p align=""center""><font face=""Chosence-regular"" size=""8"" color=""#ffffff"">[title]</font></p></position>","<position x=""0""><position y=""83""><p align=""center""><font face=""Chosence-regular"" size=""5"" color=""#ffffff"">[extra1]</font></p></position>",,"<position x=""0""><position y=""89""><p align=""center""><font face=""Chosence-Bold"" size=""4"" color=""#FFFFFF"">[comment]</font></p></position>",
VideoUseSync=0
EnableMipmap=1

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

