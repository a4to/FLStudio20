FLhd   0 * ` FLdto]  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ���\  ﻿[General]
GlWindowMode=1
LayerCount=23
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=6,16,8,9,0,20,14,15,1,10,11,18,4,5,7,19,21,3,2,12,13,17,22
WizardParams=225

[AppSet6]
App=Canvas effects\Lava
FParamValues=0,0.648,1,0.648,0,0.5,0.5,1,0.528,0.236,0.38
ParamValues=0,648,1000,648,0,500,500,1000,528,236,380
ParamValuesCanvas effects\Digital Brain=0,0,568,0,428,1000,508,4,892,28,0,264,340,292,1000,0
ParamValuesBackground\SolidColor=0,932,1000,0
Enabled=1
Collapsed=1
Name=EFX 1

[AppSet16]
App=Canvas effects\OverlySatisfying
FParamValues=0,0.74,0,0
ParamValues=0,740,0,0
Enabled=1
Collapsed=1
Name=EFX 2

[AppSet8]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.332,0.476,0.618,0.66,1,0,0,0
ParamValues=332,476,618,660,1000,0,0,0
ParamValuesBackground\FourCornerGradient=400,1000,700,1000,1000,726,1000,1000,8,972,1000,750,1000,984
ParamValuesPostprocess\Youlean Motion Blur=0,1000,420,776
ParamValuesPostprocess\Blur=644
Enabled=1
Collapsed=1
Name=Bloom

[AppSet9]
App=Feedback\WarpBack
FParamValues=0.248,0,0,0.052,0.468,0.5,0.2
ParamValues=248,0,0,52,468,500,200
ParamValuesHUD\HUD Graph Radial=0,500,0,712,500,500,82,444,500,0,1000,0,1000,64,250,500,200,312,12,0,500,1000
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,696,0,728,500,584,596,500,768
ParamValuesFeedback\70sKaleido=0,0,228,1000,860,324
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,1000,0,0,500,920,552,500
ParamValuesHUD\HUD Graph Linear=0,1000,1000,0,500,504,1000,584,810,444,500,1000,0,688,796,900,164,180,132,333,536,1000
ParamValuesPostprocess\Youlean Pixelate=392,333,0,0
ParamValuesFeedback\BoxedIn=0,0,0,0,1000,1000,748,500,0,0
ParamValuesFeedback\FeedMeFract=0,0,0,1000,564,0
ParamValuesPostprocess\Youlean Motion Blur=688,1000,408,672
ParamValuesFeedback\FeedMe=0,0,0,1000,500,680,312
Enabled=1
Collapsed=1
Name=OMG EFX

[AppSet0]
App=HUD\HUD Prefab
FParamValues=78,0,0.5,0,1,0.5,0.5,0.306,1,1,4,0,0.5,1,1,0,1,1
ParamValues=78,0,500,0,1000,500,500,306,1000,1000,4,0,500,1,1000,0,1000,1
ParamValuesBackground\FourCornerGradient=533,1000,0,1000,1000,434,1000,1000,500,1000,1000,750,1000,1000
ParamValuesBackground\SolidColor=0,932,1000,0
ParamValuesHUD\HUD Grid=0,500,0,1000,500,500,1000,1000,888,444,500,1000,500,428,0,500,1000,1000,648,0,0,1000
Enabled=1
Collapsed=1
Name=SWAP
LayerPrivateData=78014B2FCA4C89490712BA0606667A9939650C230B0000B8E805E5

[AppSet20]
App=Postprocess\Blooming
FParamValues=0,0,0,0.772,0.7,0.944,0.5,0.5,0
ParamValues=0,0,0,772,700,944,500,500,0
ParamValuesFeedback\WormHoleEclipse=0,180,1000,1000,1000,1000,0,976,764,500,500
Enabled=1
Collapsed=1
Name=Master LCD

[AppSet14]
App=Postprocess\Youlean Pixelate
FParamValues=0.432,0,0,0
ParamValues=432,0,0,0
ParamValuesHUD\HUD Prefab=6,0,500,0,1000,500,1000,514,1000,1000,444,0,660,0,368,372,736,1000
ParamValuesCanvas effects\Lava=0,664,1000,684,168,500,500,1000,528,500,224
ParamValuesFeedback\70sKaleido=0,0,320,180,908,152
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,1000,0,432,652,616,500,500
ParamValuesHUD\HUD Grid=0,500,0,1000,500,500,1000,1000,1000,444,500,1000,500,100,0,500,1000,800,904,300,0,1000
ParamValuesPostprocess\Youlean Motion Blur=336,1000,296,492
ParamValuesImage effects\Image=0,0,0,0,644,500,500,96,53,250,0,0,0,0
ParamValuesPostprocess\Youlean Bloom=1000,648,0,742
ParamValuesPostprocess\Youlean Blur=500,1000,600
ParamValuesPostprocess\Youlean Color Correction=500,488,500,972,800,620
Enabled=1
Collapsed=1
ImageIndex=3
Name=TOTAL Blur

[AppSet15]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=1,0,0.314,0.414,1,0,0,0
ParamValues=1000,0,314,414,1000,0,0,0
ParamValuesFeedback\FeedMeFract=0,0,0,0,296,0
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,708,348,544,500,260,500,500,560
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=Cool Bloom

[AppSet1]
App=HUD\HUD Prefab
FParamValues=0,0,0.5,0,0,0.24,0.176,0.342,1,1,4,0,0.5,0,0.68,0.101,1,1
ParamValues=0,0,500,0,0,240,176,342,1000,1000,4,0,500,0,680,101,1000,1
ParamValuesBackground\SolidColor=0,676,0,956
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=Grid BG 2
LayerPrivateData=780173C8CBCF4BD52B2E4B671899000060F9036F

[AppSet10]
App=Background\SolidColor
FParamValues=0,0,0,0.856
ParamValues=0,0,0,856
ParamValuesPostprocess\ScanLines=200,0,0,0,0,0
Enabled=1
Collapsed=1
Name=Background

[AppSet11]
App=HUD\HUD Prefab
FParamValues=90,0.652,0.5,0,0,0.5,0.5,0.274,1,1,4,0,0.5,0,0.68,0,1,1
ParamValues=90,652,500,0,0,500,500,274,1000,1000,4,0,500,0,680,0,1000,1
ParamValuesHUD\HUD Grid=0,500,0,1000,500,500,1000,1000,1000,444,500,1000,500,660,68,220,1000,0,704,328,0,1000
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,157,500,500,500,500,500
ParamValuesPostprocess\ScanLines=0,71,464,464,0,0
Enabled=1
Collapsed=1
Name=Grid BG 1
LayerPrivateData=78014B2FCA4C89490712BA0686167A9939650C230B0000BBC505E8

[AppSet18]
App=Image effects\Image
FParamValues=0,0.112,0.028,0.752,1,0.526,0.578,0.212,0.068,0.048,0,1,1,0.388
ParamValues=0,112,28,752,1000,526,578,212,68,48,0,1,1,388
ParamValuesCanvas effects\DarkSpark=500,0,500,500,252,500,500,500,500,500,0,0
ParamValuesPostprocess\RGB Shift=1000,0,0,600,700,200
ParamValuesPostprocess\Blooming=0,0,0,1000,1000,800,1000,1000,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesCanvas effects\SkyOcean=0,980,1000,0,248,500,500,772,836,500,464,936,732
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPostprocess\Point Cloud Default=0,1000,494,520,420,652,400,479,60,641,156,1000,184,0,333
ParamValuesPostprocess\AudioShake=360,0,333,500,100,900
ParamValuesPostprocess\Youlean Pixelate=568,0,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,1000,670,500,500,820,188,500,500,500,272
ParamValuesCanvas effects\StarTaser2=308,500,500,500,1000,1000,500,500,500,388,84,676,0
ParamValuesPostprocess\Ascii=0,0,572,600,700,200
ParamValuesHUD\HUD Prefab=83,652,500,0,0,500,500,274,1000,1000,444,0,500,0,680,0,1000,1000
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesCanvas effects\Digital Brain=0,0,0,0,1000,500,500,0,732,300,528,250,1000,0,1000,852
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,0,20,356,500,664,532,788
ParamValuesPostprocess\ColorCyclePalette=0,0,0,0,0,1000,320,280,844,0,528
ParamValuesHUD\HUD Grid=0,500,0,1000,500,500,1000,1000,704,444,500,1000,500,456,636,500,1000,552,1000,0,1000,1000
ParamValuesPostprocess\Edge Detect=728,768,676,820,660,592
ParamValuesCanvas effects\TaffyPulls=500,0,1000,0,0,500,500,584,500,500,656,560,0,500
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesFeedback\FeedMe=0,0,0,1000,500,1000,388
ParamValuesPostprocess\Dot Matrix=904,500,696,808,0,52,1000,1000
ParamValuesPostprocess\Vignette=0,0,0,600,808,184
ParamValuesCanvas effects\OverlySatisfying=0,740,0,0,600,500,500,100,100,1000,500
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,696,500,748,544,500,552,500,500
ParamValuesFeedback\70sKaleido=0,0,400,420,476,364
ParamValuesCanvas effects\Flaring=0,60,0,0,624,500,500,181,0,800,0
ParamValuesPostprocess\Blur=1000
ParamValuesPostprocess\Point Cloud Low=0,450,170,484,512,688,500,524,646,673,108,0,588,0,333
Enabled=1
Collapsed=1
ImageIndex=3
Name=Table

[AppSet4]
App=Peak Effects\Linear
FParamValues=0.1,0.02,0.888,0,0.306,0.312,0.468,0.256,0,0.64,0,0.478,0.504,1,0,0,0,0.5,0.5,0.5,0.5,0.236,0.476,0.132,0.108,0.056,0.184,0.38,0.434,0.83,0.56
ParamValues=100,20,888,0,306,312,468,256,0,640,0,478,504,1,0,0,0,500,500,500,500,236,476,132,108,56,184,380,434,830,560
ParamValuesFeedback\WarpBack=500,0,744,0,500,500,1000
ParamValuesCanvas effects\DarkSpark=500,0,500,500,188,500,500,500,500,500,0,0
ParamValuesParticles\ColorBlobs=808,1000,0,0,164,184,500,1000,68,624
ParamValuesPostprocess\RGB Shift=1000,372,0,600,700,200
ParamValuesPeak Effects\PeekMe=0,0,1000,0,500,500,500,1000,1000,1000,0,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesPostprocess\Blooming=0,0,0,1000,500,840,504,928,0
ParamValuesText\TextTrueType=144,0,0,0,0,500,500,0,0,0,500
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesMisc\PentUp=0,0,500,500,500,500,552,500,0,0,0,0
ParamValuesPostprocess\Point Cloud Default=0,602,290,496,504,448,504,503,1000,625,152,0,0,0,0
ParamValuesPostprocess\BufferBlender=0,0,267,756,750,0,0,0,500,500,750
ParamValuesPostprocess\AudioShake=36,0,0,600,700,200
ParamValuesPeak Effects\JoyDividers=0,0,0,0,940,500,500,0,100,0,440,606,462
ParamValuesObject Arrays\DiamondBit=500,500,500,0,750,0,464,0,500,1000,0,500,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,490,500,500,500,500,500
ParamValuesCanvas effects\Electric=264,12,1000,956,0,500,500,104,100,1000,500
ParamValuesBackground\FourCornerGradient=400,1000,0,1000,1000,250,1000,1000,500,1000,1000,750,1000,1000
ParamValuesPostprocess\Ascii=0,0,696,600,700,200
ParamValuesParticles\ReactiveMob=0,500,500,500,1000,500,500,1000,500,500,500,500,500,500,500,500,200,50,125,0,500,500,1000,1000,180,100,500,0,0,0
ParamValuesHUD\HUD Prefab=0,0,500,0,340,240,372,430,1000,1000,444,0,500,0,680,128,1000,1000
ParamValuesCanvas effects\Digital Brain=0,0,0,472,804,500,500,408,256,336,100,258,1000,196,1000,0
ParamValuesHUD\HUD Meter Bar=0,68,1000,1000,268,448,0,346,58,1000
ParamValuesParticles\ReactiveFlow=0,125,500,1000,504,0,844,100,692,184,356,208,592,372,196,956,200,564,669,580,556,400,652,656,340
ParamValuesHUD\HUD Graph Linear=0,500,0,0,240,552,1000,1000,250,444,500,1000,0,1000,0,500,200,0,0,0,500,1000
ParamValuesImage effects\ImageSphereWarp=0,0,750,1000,126,400,0,500,500,500
ParamValuesHUD\HUD Callout Line=0,500,0,0,84,500,700,400,0,200,0,200,100,450,100,1000
ParamValuesPeak Effects\StereoWaveForm=0,72,1000,1000,1000,0,0,648,816,1000,900,1000,500,528,500,500,500,328,0,48,1000,1000,0,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPeak Effects\Polar=40,0,184,0,372,500,612,1000,500,1000,296,708,28,648,0
ParamValuesPostprocess\ScanLines=400,0,0,0,0,0
ParamValuesHUD\HUD Meter Linear=0,196,1000,0,0,0,0,400,248,624,272,96,0,188,500,4,182,200,68,240,1000,492,1000
ParamValuesObject Arrays\Rings=0,0,0,1000,540,500,436,500,500,0,0,0,0,0,0
ParamValuesCanvas effects\OverlySatisfying=836,740,1000,0,600,500,500,100,100,1000,500
ParamValuesFeedback\FeedMe=0,0,832,492,304,432,692
ParamValuesPhysics\Columns=200,300,0,1000,676,0,164,1000,1000,450,604,312,500,500
ParamValuesPostprocess\Dot Matrix=520,420,500,0,0,500,1000,792
ParamValuesFeedback\70sKaleido=636,0,0,384,96,500
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,0,344,876,184,0,160,536,564
ParamValuesObject Arrays\CubesGrasping=0,0,1000,0,272,0,0,500,500,312,0
ParamValuesObject Arrays\CubicMatrix=0,0,0,0,500,0,500,500,500,0,0,0,0
ParamValuesCanvas effects\Flaring=0,1000,1000,1000,1000,672,396,181,0,200,500
ParamValuesTerrain\GoopFlow=0,588,1000,0,620,1000,652,0,496,500,180,464,472,1000,536,516
ParamValuesHUD\HUD Graph Polar=0,500,0,0,240,500,250,444,500,0,1000,0,1000,0,250,500,200,0,0,0,500,1000
ParamValuesPostprocess\Blur=256
ParamValuesCanvas effects\Flow Noise=908,732,1000,0,1000,492,500,100,100,1000,500
Enabled=1
Collapsed=1
ImageIndex=3
Name=EQ WOW 1

[AppSet5]
App=Peak Effects\Linear
FParamValues=0.848,0.08,0,0,0.306,0.312,0.468,0.432,0,0.48,0,0.642,0.504,1,0,0,0,0.5,0.5,0.5,0.5,0.56,0.476,0.132,0.108,0.244,0.196,0.38,0.434,0.83,0.56
ParamValues=848,80,0,0,306,312,468,432,0,480,0,642,504,1,0,0,0,500,500,500,500,560,476,132,108,244,196,380,434,830,560
ParamValuesCanvas effects\DarkSpark=500,0,500,1000,500,500,500,500,500,500,1000,1000
ParamValuesPostprocess\RGB Shift=200,380,0,600,700,200
ParamValuesPeak Effects\PeekMe=0,716,1000,0,528,556,248,0,0,1000,796,20
ParamValuesPeak Effects\VectorScope=452,733,1000,0,1000,1000,667,0
ParamValuesParticles\BugTails=0,0,0,0,948,500,500,0,0,0,0,0
ParamValuesText\TextTrueType=280,0,0,0,0,500,500,0,0,0,500
ParamValuesMisc\PentUp=0,0,0,0,500,500,500,500,0,708,344,0
ParamValuesTunnel\ForwardFall=0,1000,0,0,476,500,500,130,236,724,248,0
ParamValuesTunnel\Oblivion=0,212,1000,572,736,500,500,100,100,1000,500
ParamValuesObject Arrays\Pentaskelion=0,0,1000,0,500,500,500,0,500,500,500,584,392,680
ParamValuesObject Arrays\CrystalCube=440,608,832,940,968,500,500,612
ParamValuesImage effects\Image=0,0,0,0,1000,500,500,0,0,0,0
ParamValuesCanvas effects\Electric=0,0,0,976,148,500,500,0,100,1000,500
ParamValuesPeak Effects\StereoWaveForm=0,0,1000,1000,444,1000,1000,1000,1000,1000,892,1000,500,240,500,500,500,600,500,136,856,0,0,0
ParamValuesScenes\RhodiumLiquidCarbon=480,0,0,0,0,992,1000,1000,584,780
ParamValuesObject Arrays\WaclawGasket=0,856,1000,0,0,500,520,500,500,287,1000,88,500,668,660
ParamValuesObject Arrays\DeathClock8=0,0,0,0,1000,500,860,1000,52,536,0,640
ParamValuesScenes\Postcard=0,468,1000,0,500,500,0,0,0,0,0
ParamValuesCanvas effects\ShimeringCage=0,0,0,0,0,500,500,588,368,0,80,0,244,148
ParamValuesObject Arrays\Rings=0,0,0,1000,868,500,500,500,500,0,0,0,452,16,0
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesTunnel\TorusJourney=0,0,0,0,0,500,500,0,828,452,500,40,390,1000,500,60,800,1000,500,1000
ParamValuesCanvas effects\FreqRing=0,500,500,500,500,500,500,500,0,312,123,500,500,500
ParamValuesFeedback\70sKaleido=968,700,1000,1000,420,1000
ParamValuesPostprocess\Point Cloud High=0,922,966,412,724,708,444,503,898,465,436,0,0,0,0
ParamValuesObject Arrays\CubesGrasping=0,0,1000,1000,0,500,0,500,500,0,0
ParamValuesScenes\Cloud Ten=752,692,432,464,428,1000,300,100,100,1000,500
ParamValuesBackground\FogMachine=0,744,1000,1000,1000,492,452,480,480,448,0,0
ParamValuesParticles\ColorBlobs=0,924,1000,0,700,500,500,350,1000,1000
ParamValuesPostprocess\Blooming=0,964,1000,0,500,800,500,0,88
ParamValuesPhysics\Cage=0,700,1000,0,932,500,500,0,1000,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesBackground\ItsFullOfStars=0,560,1000,664,544,408,552,500,560,4,516
ParamValuesPeak Effects\JoyDividers=0,0,1000,1000,700,500,500,0,500,100,600,650,510
ParamValuesFeedback\SphericalProjection=60,532,296,288,424,425,172,538,236,0,0,500,530,584,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesCanvas effects\BitPadZ=0,0,440,336,972,500,0,360,0,740,584,0,784,520,240,336,0,424,240,528,1000
ParamValuesObject Arrays\BallZ=0,888,1000,868,894,500,500,364,568,400,436,200,672
ParamValuesBackground\FourCornerGradient=867,1000,28,1000,1000,806,1000,1000,472,1000,1000,686,1000,1000
ParamValuesParticles\StrangeAcid=0,712,1000,476,1000,500,500,146,500,500,912,564,0,116,0,0,400,766,600,567
ParamValuesPostprocess\Ascii=0,1000,0,600,700,200
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPeak Effects\Polar=284,16,1000,600,324,500,500,484,976,312,524,624,0,360,0
ParamValuesPostprocess\ColorCyclePalette=0,375,250,0,200,508,1000,500,192,500,1000
ParamValuesParticles\fLuids=0,692,1000,0,836,500,500,0,0,0,500,500,0,0,500,250,500,0,0,500,500,500,0,0,500,0,0,250,500,0,0,0
ParamValuesPostprocess\ScanLines=0,200,0,0,0,0
ParamValuesCanvas effects\N-gonFigure=0,846,900,252,500,500,500,500,0,400,0
ParamValuesScenes\Lantern=0,452,1000,500,0,200,250,500,228,998
ParamValuesPostprocess\Dot Matrix=500,500,500,332,1000,432,1000,652
ParamValuesFeedback\WormHoleDarkn=1000,680,840,372,348,544,40,500,404,448,500,500
ParamValuesObject Arrays\Filaments=764,1000,1000,0,500,500,500,500,500,500,0,0,500,0
ParamValuesScenes\Mandelbulb=1000,500,500,500,536,236,952,44,456,1000,1000,125,0,0,0,0,0,0
ParamValuesObject Arrays\CubicMatrix=0,776,1000,0,852,500,500,500,500,0,0,0,0
ParamValuesCanvas effects\Flaring=0,1000,1000,1000,668,424,560,181,0,400,500
ParamValuesPeak Effects\ReflectedPeeks=0,0,1000,0,676,500,500,416,172
ParamValuesPostprocess\Blur=1000
ParamValuesTerrain\GoopFlow=0,700,1000,92,0,864,504,0,500,1000,152,592,1000,1000,536,592
ParamValuesCanvas effects\Flow Noise=0,508,500,720,1000,500,500,100,100,1000,500
ParamValuesPostprocess\Point Cloud Low=0,490,330,500,500,448,444,503,158,625,156,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=3
Name=EQ WOW 2

[AppSet7]
App=Peak Effects\Linear
FParamValues=0.564,0.08,0,1,0.306,0.312,0.468,0.432,0,0.48,0,0.562,0.504,1,0,0,0,0.5,0.5,0.5,0.5,0,0.5,0.252,0.552,0.5,0.312,0.38,0.434,0.83,0.56
ParamValues=564,80,0,1000,306,312,468,432,0,480,0,562,504,1,0,0,0,500,500,500,500,0,500,252,552,500,312,380,434,830,560
ParamValuesCanvas effects\DarkSpark=500,0,500,1000,500,500,500,500,500,500,1000,1000
ParamValuesPostprocess\RGB Shift=200,380,0,600,700,200
ParamValuesPeak Effects\PeekMe=0,716,1000,0,528,556,248,0,0,1000,796,20
ParamValuesPeak Effects\VectorScope=452,733,1000,0,1000,1000,667,0
ParamValuesParticles\BugTails=0,0,0,0,948,500,500,0,0,0,0,0
ParamValuesText\TextTrueType=280,0,0,0,0,500,500,0,0,0,500
ParamValuesMisc\PentUp=0,0,0,0,500,500,500,500,0,708,344,0
ParamValuesTunnel\ForwardFall=0,1000,0,0,476,500,500,130,236,724,248,0
ParamValuesTunnel\Oblivion=0,212,1000,572,736,500,500,100,100,1000,500
ParamValuesObject Arrays\Pentaskelion=0,0,1000,0,500,500,500,0,500,500,500,584,392,680
ParamValuesObject Arrays\CrystalCube=440,608,832,940,968,500,500,612
ParamValuesImage effects\Image=0,0,0,0,1000,500,500,0,0,0,0
ParamValuesCanvas effects\Electric=0,0,0,976,148,500,500,0,100,1000,500
ParamValuesPeak Effects\StereoWaveForm=0,0,1000,1000,444,1000,1000,1000,1000,1000,892,1000,500,240,500,500,500,600,500,136,856,0,0,0
ParamValuesScenes\RhodiumLiquidCarbon=480,0,0,0,0,992,1000,1000,584,780
ParamValuesObject Arrays\WaclawGasket=0,856,1000,0,0,500,520,500,500,287,1000,88,500,668,660
ParamValuesObject Arrays\DeathClock8=0,0,0,0,1000,500,860,1000,52,536,0,640
ParamValuesScenes\Postcard=0,468,1000,0,500,500,0,0,0,0,0
ParamValuesCanvas effects\ShimeringCage=0,0,0,0,0,500,500,588,368,0,80,0,244,148
ParamValuesObject Arrays\Rings=0,0,0,1000,868,500,500,500,500,0,0,0,452,16,0
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesTunnel\TorusJourney=0,0,0,0,0,500,500,0,828,452,500,40,390,1000,500,60,800,1000,500,1000
ParamValuesCanvas effects\FreqRing=0,500,500,500,500,500,500,500,0,312,123,500,500,500
ParamValuesFeedback\70sKaleido=968,700,1000,1000,420,1000
ParamValuesPostprocess\Point Cloud High=0,922,966,412,724,708,444,503,898,465,436,0,0,0,0
ParamValuesObject Arrays\CubesGrasping=0,0,1000,1000,0,500,0,500,500,0,0
ParamValuesScenes\Cloud Ten=752,692,432,464,428,1000,300,100,100,1000,500
ParamValuesBackground\FogMachine=0,744,1000,1000,1000,492,452,480,480,448,0,0
ParamValuesParticles\ColorBlobs=0,924,1000,0,700,500,500,350,1000,1000
ParamValuesPostprocess\Blooming=0,964,1000,0,500,800,500,0,88
ParamValuesPhysics\Cage=0,700,1000,0,932,500,500,0,1000,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesBackground\ItsFullOfStars=0,560,1000,664,544,408,552,500,560,4,516
ParamValuesPeak Effects\JoyDividers=0,0,1000,1000,700,500,500,0,500,100,600,650,510
ParamValuesFeedback\SphericalProjection=60,532,296,288,424,425,172,538,236,0,0,500,530,584,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesCanvas effects\BitPadZ=0,0,440,336,972,500,0,360,0,740,584,0,784,520,240,336,0,424,240,528,1000
ParamValuesObject Arrays\BallZ=0,888,1000,868,894,500,500,364,568,400,436,200,672
ParamValuesBackground\FourCornerGradient=400,164,556,1000,1000,134,1000,1000,376,1000,1000,62,1000,1000
ParamValuesParticles\StrangeAcid=0,712,1000,476,1000,500,500,146,500,500,912,564,0,116,0,0,400,766,600,567
ParamValuesPostprocess\Ascii=0,1000,0,600,700,200
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPeak Effects\Polar=284,16,1000,600,324,500,500,484,976,312,524,624,0,360,0
ParamValuesPostprocess\ColorCyclePalette=0,375,250,0,200,508,1000,500,192,500,1000
ParamValuesParticles\fLuids=0,692,1000,0,836,500,500,0,0,0,500,500,0,0,500,250,500,0,0,500,500,500,0,0,500,0,0,250,500,0,0,0
ParamValuesPostprocess\ScanLines=0,200,0,0,0,0
ParamValuesCanvas effects\N-gonFigure=0,846,900,252,500,500,500,500,0,400,0
ParamValuesScenes\Lantern=0,452,1000,500,0,200,250,500,228,998
ParamValuesPostprocess\Dot Matrix=500,500,500,332,1000,432,1000,652
ParamValuesFeedback\WormHoleDarkn=1000,680,840,372,348,544,40,500,404,448,500,500
ParamValuesObject Arrays\Filaments=764,1000,1000,0,500,500,500,500,500,500,0,0,500,0
ParamValuesScenes\Mandelbulb=1000,500,500,500,536,236,952,44,456,1000,1000,125,0,0,0,0,0,0
ParamValuesObject Arrays\CubicMatrix=0,776,1000,0,852,500,500,500,500,0,0,0,0
ParamValuesCanvas effects\Flaring=0,1000,1000,1000,668,424,560,181,0,400,500
ParamValuesPeak Effects\ReflectedPeeks=0,0,1000,0,676,500,500,416,172
ParamValuesPostprocess\Blur=1000
ParamValuesTerrain\GoopFlow=0,700,1000,92,0,864,504,0,500,1000,152,592,1000,1000,536,592
ParamValuesCanvas effects\Flow Noise=0,508,500,720,1000,500,500,100,100,1000,500
ParamValuesPostprocess\Point Cloud Low=0,490,330,500,500,448,444,503,158,625,156,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=3
Name=EQ WOW 3

[AppSet19]
App=Image effects\Image
FParamValues=0.472,0,0,1,1,0.5,0.501,0,0,0,0,0,0,0
ParamValues=472,0,0,1000,1000,500,501,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet21]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet3]
App=HUD\HUD 3D
FParamValues=0,0,0.654,0.446,0.496,0.492,0.48,0.464,0.508,0.43,0.523,0.37,0.481,0.24,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=0,0,654,446,496,492,480,464,508,430,523,370,481,240,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
ParamValuesImage effects\Image=588,0,0,120,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=2
Name=LCD Module

[AppSet2]
App=Image effects\Image
FParamValues=0,0,0,0.704,0.944,0.608,0.492,0,0,0,0,0,0,0
ParamValues=0,0,0,704,944,608,492,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
Name=Object 1

[AppSet12]
App=Background\FourCornerGradient
FParamValues=12,0.264,0.596,0.456,1,0.578,0.632,0.764,0.548,1,0.196,0.618,1,0.276
ParamValues=12,264,596,456,1000,578,632,764,548,1000,196,618,1000,276
Enabled=1
Collapsed=1
Name=Master Filter Color

[AppSet13]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fader in-out

[AppSet17]
App=Text\TextTrueType
FParamValues=0.536,0,0,1,0,0.494,0.497,0,0,0,0.5
ParamValues=536,0,0,1000,0,494,497,0,0,0,500
Enabled=1
Collapsed=1
Name=Main Text

[AppSet22]
App=Text\TextTrueType
FParamValues=0,0,0,0,0,0.494,0.5,0,0,0,0.5
ParamValues=0,0,0,0,0,494,500,0,0,0,500
Enabled=1
Collapsed=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""3""><position y=""81""><p align=""left""><font face=""American-Captain"" size=""13"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""3""><position y=""92""><p align=""left""><font face=""Chosence-Bold"" size=""6"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""0""><position y=""91""><p align=""right""><font face=""Chosence-Bold"" size=""4"" color=""#FFFFFF"">[comment]</font></p></position>"
Images="[plugpath]Content\Bitmaps\Vector art\iMac.ilv"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

