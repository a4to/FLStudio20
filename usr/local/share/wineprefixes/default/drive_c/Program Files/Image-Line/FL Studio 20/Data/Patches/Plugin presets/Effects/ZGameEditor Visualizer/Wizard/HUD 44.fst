FLhd   0  0 FLdt�  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   ��0X  ﻿[General]
GlWindowMode=1
LayerCount=27
FPS=2
MidiPort=-1
Aspect=1
LayerOrder=4,3,8,2,5,19,23,21,9,10,20,0,17,18,11,7,6,1,14,15,12,22,13,26,16,25,24
WizardParams=1090,1091,1092,1141,1142,1143,1144,1145,130,178,274

[AppSet4]
App=Background\SolidColor
ParamValues=0,0,0,1000
Enabled=1
Collapsed=1

[AppSet3]
App=HUD\HUD Prefab
ParamValues=2,0,500,832,656,560,500,331,1000,1000,4,0,500,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4B4A4CCE4E2FCA2FCD4B89490232750D0C8CF43273CA18460A0000F13A0846

[AppSet8]
App=Postprocess\Youlean Bloom
ParamValues=500,0,150,455,1000,0,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet2]
App=HUD\HUD Prefab
ParamValues=1,468,768,952,0,500,500,305,1000,1000,4,0,500,1,0,0,1000,1
Enabled=1
UseBufferOutput=1
Collapsed=1
LayerPrivateData=78DA4B4A4CCE4E2FCA2FCD4B89490232750D0C0CF53273CA18460A0000F04C0845

[AppSet5]
App=HUD\HUD Prefab
ParamValues=5,200,292,592,0,580,500,406,1000,1000,4,0,500,1,0,0,1000,1
Enabled=1
UseBufferOutput=1
Collapsed=1
LayerPrivateData=78DA4B4A4CCE4E2FCA2FCD4B89490232750D0C4CF53273CA18460A0000F4040849

[AppSet19]
App=Background\Grid
ParamValues=924,0,0,1000,360,1000,771
Enabled=1
UseBufferOutput=1
Collapsed=1
MeshIndex=1

[AppSet23]
App=HUD\HUD Prefab
ParamValues=0,0,500,0,0,873,792,408,1000,1000,4,0,500,1,368,108,1000,1
Enabled=1
UseBufferOutput=1
Name=LOGO
LayerPrivateData=78DA73C8CBCF4BD52B2E4B671899000060F9036F

[AppSet21]
App=Background\FourCornerGradient
ParamValues=0,72,0,0,0,250,0,0,500,1000,1000,750,1000,1000
Enabled=1
Collapsed=1

[AppSet9]
App=Postprocess\ParameterShake
ParamValues=512,312,2,14,536,236,5,14,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet10]
App=Postprocess\ParameterShake
ParamValues=1000,0,0,12,536,236,5,14,0,1,0,1,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet20]
App=Image effects\Image
ParamValues=0,0,0,0,1000,480,524,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=3

[AppSet0]
App=Canvas effects\OverlySatisfying
ParamValues=696,740,484,1000
ParamValuesCanvas effects\Lava=0,0,500,720,735,500,500,500,500,500,805
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,669,500,500,500,500,500,500,814
ParamValuesCanvas effects\Digital Brain=312,0,0,0,582,500,500,752,100,300,100,494,864,48,0,0
ParamValuesBackground\Grid=250,0,0,1000,685,224,1000,0,0,793
ParamValuesCanvas effects\SkyOcean=0,0,500,0,643,500,500,0,500,500,0,0,62
ParamValuesCanvas effects\StarTaser2=500,500,500,500,776,500,500,500,500,500,500,500,765
ParamValuesCanvas effects\N-gonFigure=300,654,900,200,670,500,500,500,500,400,466
ParamValuesBlend\BufferBlender=0,111,0,1000,0,0,0,0,500,500,750,0
Enabled=1
Collapsed=1

[AppSet17]
App=Postprocess\Youlean Color Correction
ParamValues=500,500,500,387,500,500
Enabled=1
Collapsed=1

[AppSet18]
App=Canvas effects\TaffyPulls
ParamValues=992,618,468,0,0,500,500,0,516,500,0,0,0,1000
Enabled=1
Collapsed=1
MeshIndex=1

[AppSet11]
App=Misc\Automator
ParamValues=1,20,7,2,692,14,446,1,18,4,3,1000,14,250,1,19,2,3,1000,12,250,0,0,0,0,0,250,250
Enabled=1
Collapsed=1

[AppSet7]
App=Image effects\ImageBox
ParamValues=0,0,0,0,374,500,523,500,572,500,0,0,0,0,0,0,300,300,300
Enabled=1
ImageIndex=2

[AppSet6]
App=Image effects\ImageBox
ParamValues=0,0,0,0,417,500,523,500,594,500,0,0,0,0,0,0,300,300,300
Enabled=1

[AppSet1]
App=Image effects\ImageBox
ParamValues=0,0,0,0,457,500,523,500,532,500,0,0,0,0,0,0,300,300,300
ParamValuesHUD\HUD Mesh=0,500,0,1000,250,500,432,167,50,150,500,500,500,500,568,1000
Enabled=1
ImageIndex=1

[AppSet14]
App=HUD\HUD Free Line
ParamValues=0,500,0,0,20,196,100,100,0,100,1,1000,1,908,0,100,500
Enabled=1
Collapsed=1
LayerPrivateData=78DA63606060387BE68C2D90B2076286E6035E7607FEDB83D92071F34E45381BA2A6613F031C8CB247D9D8D9000EDE46DF

[AppSet15]
App=HUD\HUD Free Line
ParamValues=0,500,0,0,925,196,100,100,0,100,3,1000,1,1000,0,100,1000
Enabled=1
Collapsed=1
LayerPrivateData=78DA63606060387BE68C2D03143825A4DA1E6070B007B1DBC5226D4DE3A2C06C889A0620BB613F031C8CB247D9D8D900083C4438

[AppSet12]
App=Postprocess\Youlean Bloom
ParamValues=176,0,150,635,1000,0,0,0
Enabled=1
Collapsed=1

[AppSet22]
App=Postprocess\Youlean Color Correction
ParamValues=500,500,500,500,500,500
Enabled=1
Collapsed=1

[AppSet13]
App=Text\TextTrueType
ParamValues=512,0,0,1000,0,488,497,0,0,0,500
Enabled=1

[AppSet26]
App=Text\TextTrueType
ParamValues=0,0,0,0,0,488,500,0,0,0,500
Enabled=1

[AppSet16]
App=Image effects\Image
ParamValues=484,0,0,1000,1000,500,501,0,0,0,0,0,0,0
Enabled=1
ImageIndex=4

[AppSet25]
App=Image effects\Image
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=4

[AppSet24]
App=Peak Effects\Linear
ParamValues=0,252,416,0,98,796,736,72,0,500,260,350,1000,0,1,0,0,500,500,500,500,0,500,152,200,0,32,212,330,250,100
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0

[UserContent]
Text="This is the default text."
Html="<position x=""5""><position y=""2""><p align=""left""><font face=""American-Captain"" size=""10"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""5""><position y=""11""><p align=""left""><font face=""Chosence-Bold"" size=""6"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""0""><position y=""4""><p align=""right""><font face=""Chosence-Bold"" size=""5"" color=""#FFFFFF"">[extra1]</font></p></position>","<position x=""0""><position y=""11""><p align=""right""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""5""><position y=""86""><p align=""left""><font face=""Chosence-Bold"" size=""5"" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""5""><position y=""91""><p align=""left""><font face=""Chosence-regular"" size=""5"" color=""#FFFFF"">[extra3]</font></p></position>"
Meshes=[plugpath]Content\Meshes\Sphere(Texture).zgeMesh
VideoUseSync=0
EnableMipmap=1

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

