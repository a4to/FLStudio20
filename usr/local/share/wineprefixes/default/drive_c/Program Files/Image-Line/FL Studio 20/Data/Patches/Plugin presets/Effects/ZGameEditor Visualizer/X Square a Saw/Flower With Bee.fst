FLhd   0  ` FLdtg  �
12.3.0.72 �.Z G a m e E d i t o r   V i s u a l i z e r   �4              A                  �  W   �  ~  �    �HQV ��9�  [General]
GlWindowMode=1
LayerCount=5
FPS=1
DmxOutput=0
DmxDevice=0
MidiPort=0
Aspect=1
CameraMode=0
LayerOrder=0,1,3,2,4
Info=

[AppSet0]
App=Image effects\Image
ParamValues=0,0,0,0,1000,500,500,0,0
ParamValuesBackground\FogMachine=500,525,1000,0,350,500,500,186,167,1000,0,0
ParamValuesBackground\SolidColor=645,702,814,946
ParamValuesCanvas effects\DarkSpark=548,0,500,500,500,500,500,500,500,500,0,0
ParamValuesCanvas effects\Flaring=87,489,187,1000,1000,503,775,63,0,400,500
ParamValuesCanvas effects\Flow Noise=317,675,1000,704,0,1000,1000,97,100,1000,500
ParamValuesCanvas effects\N-gonFigure=0,599,654,0,632,494,500,157,0,170,0
ParamValuesCanvas effects\ShimeringCage=0,0,0,0,48,500,500,1000,473,0,0,0,0,0
ParamValuesMisc\Automator=1000,185,273,400,608,161,498,1000,148,182,400,576,112,538,1000,111,61,400,627,112,512,1000,185,303,400,378,129,342,0,0,0,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesTerrain\CubesAndSpheres=0,0,0,0,106,445,0,352,265,799,1000,242,1000,1000,97,939,1000,127
ParamValuesTerrain\GoopFlow=150,0,0,347,342,500,500,0,0,500,0,500,402,1000,1000,698
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=0
MeshIndex=0
Name=background

[AppSet1]
App=Postprocess\Vignette
ParamValues=0,0,0,540,665,447
ParamValuesBackground\FogMachine=0,580,1000,0,329,984,500,1000,500,1000,0,0
ParamValuesBackground\FourCornerGradient=0,1000,0,558,613,0,546,600,0,526,594,961,1000,603
ParamValuesBackground\ItsFullOfStars=0,0,0,0,90,500,500,654,0,0,0
ParamValuesBlend\BufferBlender=0,889,0,1000,250,111,786,0,494,500,750,0
ParamValuesCanvas effects\Electric=0,541,504,749,763,500,500,39,100,1000,500
ParamValuesCanvas effects\Flaring=0,1000,1000,1000,1000,500,500,181,0,600,500
ParamValuesCanvas effects\SkyOcean=0,0,500,0,323,500,500,80,756,500,0,0,0
ParamValuesFeedback\70sKaleido=0,0,0,1000,0,62
ParamValuesFeedback\BoxedIn=0,0,0,1000,500,0,0,500,0,0
ParamValuesFeedback\FeedMe=0,0,0,1000,1000,1000,358
ParamValuesFeedback\SphericalProjection=60,0,0,0,398,425,686,718,775,1000,0,500,500,731,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesObject Arrays\8x8x8_Eggs=0,448,381,0,154,500,500,357,574,644,0,221,221
ParamValuesObject Arrays\Filaments=0,719,140,526,500,500,500,719,596,35,1000,105,316,0
ParamValuesParticles\fLuids=439,930,965,754,754,737,211,1000,754,1000,632,1000,632,877,386,175,158,0,1000,0,825,263,281,684,500,88,70,1000,1000,4,175,211
ParamValuesParticles\ReactiveFlow=0,0,1000,1000,1000,0,574,722,1000,1000,0,439,614,263,491,1000,789,561,474,439,105,456,211,386,281
ParamValuesPeak Effects\Polar=0,763,777,0,547,793,461,670,1000,500,417,486,26,0,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesTerrain\GoopFlow=0,531,360,860,0,500,1000,61,757,506,0,487,1000,716,0,193
ParamValuesText\TextTrueType=0,0,0,0,0,492,207,0,0,0,500
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=1
MeshIndex=0
Name=vignette

[AppSet3]
App=Peak Effects\Linear
ParamValues=415,157,809,0,389,670,237,1000,0,500,0,424,500,1,0,607,0,500,500,1000,1000,350,581,0
ParamValuesBackground\FogMachine=470,0,0,0,0,500,500,500,500,500,0,0
ParamValuesBackground\FourCornerGradient=133,1000,1000,1000,1000,539,928,502,837,994,451,827,1000,686
ParamValuesBackground\ItsFullOfStars=595,0,0,0,536,845,292,350,230,0,0
ParamValuesBackground\SolidColor=0,613,782,826
ParamValuesBlend\BufferBlender=0,0,0,0,250,0,579,0,0,500,500,0
ParamValuesFeedback\SphericalProjection=60,0,0,0,1000,447,500,747,341,834,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesFeedback\WarpBack=913,0,0,0,500,206,72
ParamValuesFeedback\WormHoleEclipse=150,0,0,13,842,198,401,244,577,500,500
ParamValuesParticles\BugTails=0,0,180,0,743,500,500,0,0,0,0,0
ParamValuesPeak Effects\Polar=0,0,0,0,168,500,500,560,1000,500,500,500,0,500,0
ParamValuesPeak Effects\WaveSimple=0,0,0,0,621,500,0,500,0
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPostprocess\AudioShake=7,0,0,600,700,200
ParamValuesPostprocess\Blooming=0,253,1000,76,1000,658,1000,623,0
ParamValuesPostprocess\Vignette=0,0,0,554,954,200
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=1
MeshIndex=0
Name=spectrum

[AppSet2]
App=Text\TextTrueType
ParamValues=0,0,0,0,0,479,299,0,0,0,500
ParamValuesBackground\FogMachine=0,0,0,0,500,500,222,500,500,0,0,0
ParamValuesBackground\FourCornerGradient=0,1000,592,872,741,691,440,677,526,536,578,43,367,408
ParamValuesBackground\SolidColor=0,833,0,1000
ParamValuesBlend\BufferBlender=0,111,867,862,250,111,1000,0,500,500,750,1000
ParamValuesImage effects\ImageWarp=0,0,0,297,0,156,257,1000
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,860,500,500,500,500,500
ParamValuesParticles\fLuids=439,632,1000,0,491,676,526,684,1000,807,1000,754,1000,386,596,404,53,0,1000,500,1000,0,1000,509,474,965,1000,246,1000,140,333,0
ParamValuesParticles\ReactiveMob=1000,464,1000,0,1000,500,474,401,1000,1000,182,415,550,0,614,1000,690,1000,301,502,0,383,498,1000,0,549,1000,398,0,353
ParamValuesPeak Effects\Linear=0,275,1000,354,609,571,366,419,1,576,0,111,0,1000,1000,715,0,500,500,1000,1000,350,581,0
ParamValuesTerrain\GoopFlow=0,0,0,0,596,500,500,0,500,497,241,628,500,1000,420,462
ParamValuesText\MeshText=0,56,0,0,246,500,544,500,500,500,500,500,500,500,500,500,100,0,500,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=1
MeshIndex=0
Name=track info

[AppSet4]
App=Postprocess\AudioShake
ParamValues=9
ParamValuesBlend\BufferBlender=0,222,800,1000,250,0,0,0,500,500,750,1000
ParamValuesCanvas effects\Flaring=0,1000,1000,1000,1000,500,500,152,0,400,500
ParamValuesMisc\Automator=1000,111,182,400,448,525,586,1000,111,212,400,531,26,548,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0
ParamValuesPostprocess\Blooming=0,0,0,1000,654,697,0,292,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=1
MeshIndex=0
Name=shake effect

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=44100
VideoCodecName=Windows Media Video 9 Advanced Profile
AudioCodecName=Windows Media Audio 10 Professional
AudioCodecFormatName=440 kbps, 44 kHz, 2 channel 24 bit CBR
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=C:\Users\Martin Ruhland\Desktop\Square a Saw - visualizer templates\flower with bee.wmv
Bitrate=50000000
Uncompressed=0

[UserContent]
Text=
Html="<position x=""0"" y=""31""><p align=""right""><font face=""RobotoCondensed-Regular"" size=""9"">[author]</position>","<position x=""0"" y=""40""><p align=""right""><font face=""RobotoCondensed-Regular"" color=""#ffffff"" size=""8""><i>[title]</b></p></font></position>"
VideoCues=
Meshes=
MeshAutoScale=0
MeshWithColors=0
Images="[plugpath]Content\Bitmaps\ZGEVIZ flower with bee.jpg"
VideoUseSync=0

[Detached]
Top=394
Left=139
Width=689
Height=638

