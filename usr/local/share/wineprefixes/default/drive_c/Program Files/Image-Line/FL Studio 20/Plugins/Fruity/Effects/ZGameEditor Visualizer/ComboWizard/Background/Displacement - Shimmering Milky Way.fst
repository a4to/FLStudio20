FLhd   0  ` FLdt!	  �20.7.3.1981 ��  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4               A                        o  �  �    �HQV ՗�  ﻿[General]
GlWindowMode=1
LayerCount=6
FPS=1
AspectRatio=16:9
LayerOrder=0,5,2,3,4,1

[AppSet0]
App=HUD\HUD Image
FParamValues=0,0,0.5,0.5,1,1,0.54,4,0.5,0,0,1,1,1,0
ParamValues=0,0,500,500,1000,1000,540,4,500,0,0,1000,1000,1,0
Enabled=1
UseBufferOutput=1

[AppSet5]
App=Canvas effects\Lava
FParamValues=0.992,0,0.5,0.72,0,0.5,0.5,0.5,0.5,0.336,0
ParamValues=992,0,500,720,0,500,500,500,500,336,0
ParamValuesBackground\FogMachine=960,0,0,500,500,500,500,500,500,500,0,0
ParamValuesBackground\ItsFullOfStars=968,0,0,0,500,500,500,500,0,0,0
ParamValuesCanvas effects\Combustible Voronoi=872,0,1000,0,600,0
ParamValuesCanvas effects\Alloy Plated Voronoi=976,510,1000,0,1,500,0
Enabled=1

[AppSet2]
App=Postprocess\Vignette
FParamValues=0,0,0,0.6,1,0.132
ParamValues=0,0,0,600,1000,132
ParamValuesBackground\ItsFullOfStars=248,0,0,0,500,500,500,500,0,0,0
ParamValuesCanvas effects\Alloy Plated Voronoi=0,562,0,0,1,500,0
Enabled=1
UseBufferOutput=1

[AppSet3]
App=Blend\BufferBlender
FParamValues=0,0,0,1,1,1,1,0,0.5,0.5,0.75,0
ParamValues=0,0,0,1000,1,1,1000,0,500,500,750,0
Enabled=1
UseBufferOutput=1
ImageIndex=1

[AppSet4]
App=HUD\HUD Image
FParamValues=0,0,0.5,0.5,1,1,0.5,4,0.5,0,0,1,1,1,1
ParamValues=0,0,500,500,1000,1000,500,4,500,0,0,1000,1000,1,1
Enabled=1
ImageIndex=3

[AppSet1]
App=Postprocess\Youlean Color Correction
FParamValues=0.48,0.5,0.616,0.5,0.54,0.5
ParamValues=480,500,616,500,540,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=256000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="This is the default text."
Images=@Stream:https://images.pexels.com/photos/32237/pexels-photo.jpg
VideoUseSync=0
Filtering=0

[Detached]
Top=-864
Left=0
Width=1536
Height=864

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

[Wizard]
Section0Cb=Displacement - Mountain View
Section1Cb=None
Section2Cb=Waveform Horiz 02
Section3Cb=Handwritten sweet
Macro0=Song Title
Macro1=Author
Macro2=Comment

