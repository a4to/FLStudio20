FLhd   0 * ` FLdt5  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ի-�  ﻿[General]
GlWindowMode=1
LayerCount=25
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=19,20,18,3,1,10,26,13,23,7,16,5,22,17,21,0,2,9,4,15,14,12,6,11,8

[AppSet19]
App=Background\SolidColor
FParamValues=0,0,0,0
ParamValues=0,0,0,0
Enabled=1

[AppSet20]
App=Misc\Automator
FParamValues=1,19,5,6,0,0.25,0.52,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,19,5,6,0,250,520,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1

[AppSet18]
App=Postprocess\Vignette
FParamValues=0,0,0,0.6,0.2572,0.863
ParamValues=0,0,0,600,257,863
Enabled=1
UseBufferOutput=1

[AppSet3]
App=Image effects\Image
FParamValues=0,0,0,1,0.7716,0.5,0.5,0,0,0,0,0,1,1
ParamValues=0,0,0,1000,771,500,500,0,0,0,0,0,1,1000
Enabled=1

[AppSet1]
App=Postprocess\Youlean Blur
FParamValues=0.405,0,0,0,0,0
ParamValues=405,0,0,0,0,0
Enabled=1
ImageIndex=6

[AppSet10]
App=HUD\HUD Prefab
FParamValues=25,0,1,1,1,0.5,0.5,0.243,1,1,4,0,0.5,1,0.3344,0,1,0
ParamValues=25,0,1000,1000,1000,500,500,243,1000,1000,4,0,500,1,334,0,1000,0
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C2CF43273CA18863D00000F220AA3

[AppSet26]
App=HUD\HUD Prefab
FParamValues=22,0,0,0,0,0.5,0.5,0.235,1,1,4,0,0.5,1,0.312,0,1,0
ParamValues=22,0,0,0,0,500,500,235,1000,1000,4,0,500,1,312,0,1000,0
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C4CF53273CA18863D00000C6D0AA0

[AppSet13]
App=HUD\HUD 3D
FParamValues=0,0,0.5,0.5,0.5,0.5,0.5,0.5,0.501,0.501,0.501,0.5,0.5,0.488,0.141,0.501,0.501,0.501,0.501,0.501,0.5221,0.501,0.501,1,1,0,0,1,1,0
ParamValues=0,0,500,500,500,500,500,500,501,501,501,500,500,488,141,501,501,501,501,501,522,501,501,1,1,0,0,1000,1000,0
Enabled=1
ImageIndex=6

[AppSet23]
App=Misc\Automator
FParamValues=1,14,21,6,0.488,0.763,0.509,1,11,15,6,0,0.25,0.526,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,14,21,6,488,763,509,1,11,15,6,0,250,526,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1

[AppSet7]
App=Particles\fLuids
FParamValues=0,0,0,0,0.228,0.5,0.5,0,0,0,0.5,0.5,0.5,0,0.5,0.36,0.5,0,0,0.5,0.59,1,0,0,0.5,0,0,0.25,1,0,0,0
ParamValues=0,0,0,0,228,500,500,0,0,0,500,500,500,0,500,360,500,0,0,500,590,1000,0,0,500,0,0,250,1000,0,0,0
Enabled=1
ImageIndex=6

[AppSet16]
App=Misc\Automator
FParamValues=1,8,21,6,0.5,0.25,0.507,1,8,22,6,0.5,0.25,0.61,1,4,5,6,0,0.25,0.56,0,0,0,0,0,0.25,0.25
ParamValues=1,8,21,6,500,250,507,1,8,22,6,500,250,610,1,4,5,6,0,250,560,0,0,0,0,0,250,250
Enabled=1

[AppSet5]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1

[AppSet22]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1

[AppSet17]
App=Image effects\Image
FParamValues=0,0,0,0,0.804,0.5,0.529,0,0,0,0,0,0,0
ParamValues=0,0,0,0,804,500,529,0,0,0,0,0,0,0
Enabled=1
ImageIndex=2

[AppSet21]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1

[AppSet0]
App=Blend\BufferBlender
FParamValues=0,0,0,1,2,5,1,0,0.5,0.5,0.75,1
ParamValues=0,0,0,1000,2,5,1000,0,500,500,750,1
Enabled=1
ImageIndex=5

[AppSet2]
App=HUD\HUD 3D
FParamValues=0,0,0.5,0.458,0.5,0.5061,0.5079,0.5,0.5,0.5,0.4768,0.5,0.5,0.803,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,0
ParamValues=0,0,500,458,500,506,507,500,500,500,476,500,500,803,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,0
Enabled=1
ImageIndex=6

[AppSet9]
App=Misc\Automator
FParamValues=1,3,11,2,0.48,0.009,0.502,1,3,6,6,0.495,0.224,0.501,1,3,7,6,0.495,0.25,0.501,0,0,0,0,0,0.25,0.25
ParamValues=1,3,11,2,480,9,502,1,3,6,6,495,224,501,1,3,7,6,495,250,501,0,0,0,0,0,250,250
Enabled=1

[AppSet4]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1

[AppSet15]
App=Canvas effects\FreqRing
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0,0.312,0.123,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500,500,500,0,312,123,500,500,500
ParamValuesImage effects\Image=0,0,0,1000,917,500,500,0,0,0,0,0,1000,1000
Enabled=1
ImageIndex=6

[AppSet14]
App=Postprocess\Youlean Blur
FParamValues=0.5,0,0,0,0,0
ParamValues=500,0,0,0,0,0
Enabled=1
ImageIndex=6

[AppSet12]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1

[AppSet6]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
ParamValuesBlend\BufferBlender=0,0,0,1000,250,222,407,0,500,500,750,0
Enabled=1
ImageIndex=1

[AppSet11]
App=Background\FourCornerGradient
FParamValues=6,0.7619,0.027,1,1,0.288,0.325,1,0.152,0.62,1,0.118,1,0.346
ParamValues=6,761,27,1000,1000,288,325,1000,152,620,1000,118,1000,346
Enabled=1

[AppSet8]
App=Misc\Automator
FParamValues=1,12,2,6,0.479,0.25,0.522,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,12,2,6,479,250,522,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<p align=""center"">[textline]</p>"
Images="[plugpath]ComboWizard\Assets Revisualizer\eye-3221498.jpg"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

