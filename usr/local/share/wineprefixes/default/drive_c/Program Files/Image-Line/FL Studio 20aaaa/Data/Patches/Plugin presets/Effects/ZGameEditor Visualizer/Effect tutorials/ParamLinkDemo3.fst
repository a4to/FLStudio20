FLhd   0  ` FLdt�  �20.6.1.1513 ��  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4               A                        ?  �  �    �HQV ���
  ﻿[General]
GlWindowMode=1
LayerCount=10
FPS=2
AspectRatio=16:9
LayerOrder=0,2,1,3,4,5,6,7,8,9

[AppSet0]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=1

[AppSet2]
App=HUD\HUD Image
FParamValues=0,0,0.1865,0.6127,0.5642,1,0.2194,4,0.5,0.727,0.3514,0.0851,0.1513,1,0
ParamValues=0,0,186,612,564,1000,219,4,500,727,351,85,151,1,0
Enabled=1
ImageIndex=1

[AppSet1]
App=HUD\HUD Prefab
FParamValues=458,0,0,0,0,0.1481,0.275,0.3343,1,1,4,0,0.5,1,0.4418,0,1,0
ParamValues=458,0,0,0,0,148,275,334,1000,1000,4,0,500,1,441,0,1000,0
Enabled=1
LayerPrivateData=78012BCE4DCCC95148CD49CD4DCD2B298E292E2C2D4A05918945A9BA06A6C67A9939650CC319000075FA0D4E

[AppSet3]
App=HUD\HUD Callout Line
FParamValues=0,0.5,0,0,0.7652,0.4168,0.269,0.2293,4,0.878,0,0.2,0.1408,0.5073,0,1
ParamValues=0,500,0,0,765,416,269,229,4,878,0,200,140,507,0,1
Enabled=1

[AppSet4]
App=Misc\ParamLink
FParamValues=1,3,4,2,9,0,0.5,0.5,0.4809,0.5,0,1,0.55,0,0,1,0.0343,0.3203,0.2032
ParamValues=1,3,4,2,9,0,500,500,480,500,0,1000,550,0,0,1,34,320,203
Enabled=1
ImageIndex=2

[AppSet5]
App=Misc\ParamLink
FParamValues=1,3,5,2,10,0,0.5,0.5,0.4673,0.5,0,1,0.55,0,0,1,0.0343,0.0562,0.2032
ParamValues=1,3,5,2,10,0,500,500,467,500,0,1000,550,0,0,1,34,56,203
Enabled=1
ImageIndex=2

[AppSet6]
App=Misc\Automator
FParamValues=1,8,1,3,0,0.002,0.756,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,8,1,3,0,2,756,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1

[AppSet7]
App=Misc\ParamLinkCache
FParamValues=0.5529,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=552,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet8]
App=Misc\ParamLink
FParamValues=1,7,0,3,4,5,0.5,0.6253,0.7124,0.5,0,1,0.5173,0,1,1,0.1678,0.3203,0.2032
ParamValues=1,7,0,3,4,5,500,625,712,500,0,1000,517,0,1,1,167,320,203
Enabled=1
ImageIndex=2

[AppSet9]
App=Misc\ParamLink
FParamValues=1,7,0,3,5,5,0.8867,0.6253,0.5437,0.5,0,1,0.5255,0,1,1,0.1678,0.0562,0.2032
ParamValues=1,7,0,3,5,5,886,625,543,500,0,1000,525,0,1,1,167,56,203
Enabled=1
ImageIndex=2

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Html="<p align=""center"">[textline]</p>"
Images="[plugpath]Content\Bitmaps\ZGEVIZ flower with bee.jpg",[plugpath]Content\Bitmaps\Wizard\SeychellesBeachRock(HD).jpg
VideoUseSync=0
Filtering=0

[Detached]
Top=80
Left=1095
Width=825
Height=697

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

