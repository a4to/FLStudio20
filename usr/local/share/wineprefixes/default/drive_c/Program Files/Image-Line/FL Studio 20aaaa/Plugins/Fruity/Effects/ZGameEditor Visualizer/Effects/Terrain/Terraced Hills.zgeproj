<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" FileVersion="2">
  <OnLoaded>
    <ZLibrary>
      <Source>
<![CDATA[inline float angle(float X)
{
  if(X >= 0 && X < 360)return X;
  if(X > 360)return X-floor(X/360)* 360;
  if(X <   0)return X+floor(X/360)*-360;
}

vec3 hsv(float H, float S, float V)
{
  vec3 Color;
  float R,G,B,I,F,P,Q,T;

  H = angle(H);
  S = clamp(S,0,100);
  V = clamp(V,0,100);

  H /= 60;
  S /= 100;
  V /= 100;

  if(S == 0)
  {
    Color[0] = V;
    Color[1] = V;
    Color[2] = V;
  }
  else
  {
    I = floor(H);
    F = H-I;

    P = V*(1-S);
    Q = V*(1-S*F);
    T = V*(1-S*(1-F));

    if(I == 0){R = V; G = T; B = P;}
    if(I == 1){R = Q; G = V; B = P;}
    if(I == 2){R = P; G = V; B = T;}
    if(I == 3){R = P; G = Q; B = V;}
    if(I == 4){R = T; G = P; B = V;}
    if(I == 5){R = V; G = P; B = Q;}

    Color[0] = R;
    Color[1] = G;
    Color[2] = B;
  }
  return Color;
}]]>
      </Source>
    </ZLibrary>
  </OnLoaded>
  <OnUpdate>
    <ZExpression>
      <Expression>
<![CDATA[uResolution=vector2(app.ViewportWidth,app.ViewportHeight);
uViewport=vector2(app.ViewportX,app.ViewportY);
uAlpha =1.0-Parameters[0];

float speed=(Parameters[1]-0.5)*4.0;
float delta=app.DeltaTime*Speed; //comment to use other time options
uTime+=delta;
//uColorHSV = vector3(Parameters[1]*360,Parameters[2],1-Parameters[3]);]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <OnRender>
    <UseMaterial Material="mCanvas"/>
    <RenderSprite/>
  </OnRender>
  <Content>
    <Shader Name="MainShader">
      <VertexShaderSource>
<![CDATA[#version 120

void main(){
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[#version 120

uniform vec2 iResolution,iViewport;
uniform float iTime;
uniform float iAlpha;

/*

	Terraced Hills
	--------------

    This is an abstract representation of the terraced hills you see throughout various parts
	of the world. In essence, it's just a very basic terrain layout with some edging.

	I wanted to make something nice and simple. The code is reasonably compact, but without
	sacrificing too much in the way of efficiency or readability.

*/

// 2x2 matrix rotation. Angle vector, courtesy of Fabrice.
mat2 rot2( float th ){ vec2 a = sin(vec2(1.5707963, 0) + th); return mat2(a, -a.y, a.x); }

// The triangle function that Shadertoy user Nimitz has used in various triangle noise demonstrations.
// See Xyptonjtroz - Very cool.
float tri(in float x){return abs(x-floor(x)-.5);} // Triangle function.
vec2 tri(in vec2 x){return abs(x-floor(x)-.5);} // Triangle function.
//vec2 tri(in vec2 x){return cos(x*6.2831853)*0.25+0.25;} // Smooth version. Not used here.

// PF - phase variance. Varies between zero and 1. Zero is redundant, as it returns the triangle function.
//vec2 trap(in vec2 x, float pf){ return (tri(x - pf*.125) + tri(x + pf*.125))*.5; } // Trapezoid function.

// A simple noisey layer made up of a sawtooth combination.
float hLyr(vec2 p) { return dot(tri(p/1.5 + tri(p.yx/3. + .25)), vec2(1)); }
//float hLyr(vec2 p) { return dot(trap(p/1.5 + trap(p.yx/3. + .25, .5), .5), vec2(1)); }


// I've mentioned this before, but you can make some pretty interesting surfaces simply by
// combining mutations of a rudimentary functional layer. Take the base layer, then rotate, skew,
// change frequency, amplitude, etc, and combine it with the previous layer. Continue ad infinitum...
// or until your GPU makes you stop. :)
float hMap(vec2 p) {

    float ret = 0., a = 1.;

    // Combining three layers of the function above.
    for(int i=0; i<3; i++) {
        ret += abs(a)*hLyr(p/a); // Add a portion of the layer to the final result.
        //p = rot2(1.5707963/3.)*p;
        //p = mat2(.866025, .5, -.5, .866025)*p; // Rotate the layer only.
        p = mat2(.866025, .57735, -.57735, .866025)*p; // Rotate and skew the layer.
        a *= -0.3; // Multiplying the amplitude by a negative for an interesting variation.
    }

    // Squaring and smoothing the result for some peakier peaks. Not mandatory.
    ret = smoothstep(-.2, 1., ret*ret/1.39/1.39);


    // The last term adds some ridges. Basically, you take the result then blend in a
    // small periodic portion of it... The code explains it better.
    return ret*.975 + tri(ret*12.)*.05; // Range: [0, 1].. but I'd double check. :)
    //return ret*.99 + clamp(cos((ret)*6.283*24.)*.5+.5, 0., 1.)*.01; // Another way.


}

// Distance function. A flat plane perturbed by a height function of some kind.
float map(vec3 p) { return (p.y - hMap(p.xz)*.35)*.75; }


// Tetrahedral normal - courtesy of IQ. I'm in saving mode, so am saving a few map calls.
// I've added to the function to include a rough tetrahedral edge calculation.
vec3 normal(in vec3 p, inout float edge){

    // Edging thickness. I wanted the edges to be resolution independent... or to put it
    // another way, I wanted the lines to be a certain pixel width regardless of the
    // canvas size. If you don't, then the lines can look too fat in fullscreen.
    vec2 e = vec2(-1., 1.)*.5/iResolution.y;

    // The hit point value, and four nearby samples, spaced out in a tetrahedral fashion.
	float d1 = map(p + e.yxx), d2 = map(p + e.xxy);
	float d3 = map(p + e.xyx), d4 = map(p + e.yyy);
    float d = map(p);

    // Edge calculation. Taking for samples around the hit point and determining how
    // much they vary. Large variances tend to indicate an edge.
    edge = abs(d1 + d2 + d3 + d4 - d*4.);
    edge = smoothstep(0., 1., sqrt(edge/e.y*2.));

    // Recalculating for the normal. I didn't want the sample spacing to change from
    // one resolution to the next. Hence, the fixed number. Just for the record, I tend
    // to work within the 800 by 450 window.
    e = vec2(-1., 1.)*.001;
	d1 = map(p + e.yxx), d2 = map(p + e.xxy);
	d3 = map(p + e.xyx), d4 = map(p + e.yyy);

    // Normalizing.
	return normalize(e.yxx*d1 + e.xxy*d2 + e.xyx*d3 + e.yyy*d4 );
}



void mainImage( out vec4 fragColor, in vec2 fragCoord )	{


    // Unit direction ray.
	vec3 rd = normalize(vec3(fragCoord.xy - iResolution.y*.5, iResolution.y));

    // Orienting to face a particular direction.
    rd.yz = rot2(.35)*rd.yz;

    // Camera point - Placed above the plane and moving in the general XZ direction.
    vec3 ro = vec3(iTime*.4, .5, iTime*.2);

    // Basic raymarching.
	float t=0., d;
	for(int i=0; i<96; i++) {

		d = map(ro + rd*t); // Closest distance to current ray point.

        // Break condition - Surface hit, or too far.
        if(abs(d)<.001*(t*.125 + 1.) || t>20.) break;

        // Advancing the ray - Using a bit more accuracy nearer the camera.
        t += (step(1., t)*.3 + .7)*d;
	}

    // Hit point. Note that about a quarter of the screen hits the curved far plane (sky),
    // so a few cycles are wasted, but there's no nested code block, which looks a bit
    // neater... I wouldn't do this for more sophisticated examples, but it's OK here.
    vec3 sp = ro + rd*t;

    // Applying direct lighting. It's simpler, but it's more of an aesthetic choice for this
    // particular example.
    vec3 ld = vec3(-0.676, 0.408, 0.613); // Normalized, or pretty close.


    // Normal and edge value.
    float edge;
    vec3 n = normal(sp, edge);

    float dif = max(dot(ld, n), 0.); // Diffuse.
    float spe = pow(max(dot(reflect(rd, n), ld), 0.), 16.); // Specular.

    float sh = hMap(sp.xz); // Using the height map to enhance the peaks and troughs.

    // A bit of random, blocky sprinkling for the hills. Cheap, but it'll do.
    float rnd = fract(sin(dot(floor(sp.xz*512.), vec2(41.73, 289.67)))*43758.5453)*.5 + .5;

    // The fog. Since the foreground color is pretty bland, I've made it really colorful. I've gone
    // for the sunset cliche... or misty morning sunrise, if you prefer. :)
    vec3 fog = mix(vec3(.75,.77, .78), vec3(1.04, .95, .87), (pow(1.0 + dot(rd, ld), 3.))*.35);

    // Using the values above to produce the final color, then mixing in some fog according to distance.
    vec3 c = mix((vec3(1.1, 1.05, 1)*rnd*(dif + .1)*sh + fog*spe)*(1. - edge*.7), fog*fog, min(1., t*.3));

    // No gamma correction. If you wanted, you could think of it as postprocessing the final
    // color and gamma correction rolled into one. :)
    fragColor = vec4(c, iAlpha);
}


void main(){
    //ZGE does not use mainImage( out vec4 fragColor, in vec2 fragCoord )
    //Rededefined fragCoord as gl_FragCoord.xy-iViewport(dynamic window)
    mainImage(gl_FragColor,gl_FragCoord.xy-iViewport);
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="iResolution" VariableRef="uResolution"/>
        <ShaderVariable VariableName="iViewport" VariableRef="uViewport"/>
        <ShaderVariable VariableName="iTime" VariableRef="uTime"/>
        <ShaderVariable VariableName="iAlpha" VariableRef="uAlpha"/>
        <ShaderVariable VariableName="iColorHSV" VariableRef="uColorHSV"/>
      </UniformVariables>
    </Shader>
    <Group Comment="Default ShaderToy Uniform Variable Inputs">
      <Children>
        <Variable Name="uResolution" Type="6"/>
        <Variable Name="uTime"/>
        <Variable Name="uViewport" Type="6"/>
      </Children>
    </Group>
    <Group Comment="FL Studio Variables">
      <Children>
        <Array Name="Parameters" SizeDim1="5" Persistent="255">
          <Values>
<![CDATA[789C63606060983553D29E81A10188216C00231703D6]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Alpha
Speed]]>
          </StringValue>
        </Constant>
        <Constant Name="AuthorInfo" Type="2">
          <StringValue>
<![CDATA[Shane (converted by Youlean & StevenM)
Adapted from: https://www.shadertoy.com/view/MtdSRn]]>
          </StringValue>
        </Constant>
      </Children>
    </Group>
    <Group Comment="Unique Uniform Variable Inputs">
      <Children>
        <Variable Name="uAlpha"/>
        <Variable Name="uColorHSV" Type="7"/>
      </Children>
    </Group>
    <Group Comment="Materials and Textures">
      <Children>
        <Material Name="mCanvas" Blend="1" Shader="MainShader"/>
      </Children>
    </Group>
  </Content>
</ZApplication>
