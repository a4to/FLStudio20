FLhd   0  0 FLdt�  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   ն,2  ﻿[General]
GlWindowMode=1
LayerCount=27
FPS=2
MidiPort=-1
Aspect=1
LayerOrder=12,10,14,15,16,0,9,11,13,6,7,1,4,5,3,18,17,19,22,23,20,21,24,2,25,8,26
WizardParams=1235,1285,1286,1287,1288,1289

[AppSet12]
App=Misc\Automator
ParamValues=1,11,12,2,500,101,250,0,14,6,2,500,101,488,0,14,8,2,300,101,460,0,14,2,2,1000,101,378
Enabled=1
Collapsed=1

[AppSet10]
App=HUD\HUD Grid
ParamValues=0,500,1000,1000,500,500,1000,1000,1000,4,500,35,1000,744,0,500,1000,139,500,0,0,1
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet14]
App=HUD\HUD Prefab
ParamValues=226,628,832,0,1000,297,748,168,384,1000,4,0,250,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0606967A9939650C23030000A90D090E

[AppSet15]
App=HUD\HUD Prefab
ParamValues=226,0,832,1000,0,297,748,168,384,1000,4,0,250,1,527,0,0,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0606967A9939650C23030000A90D090E

[AppSet16]
App=Postprocess\ParameterShake
ParamValues=1000,0,15,16,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet0]
App=Background\SolidColor
ParamValues=0,0,0,660
Enabled=1
Collapsed=1

[AppSet9]
App=HUD\HUD 3D
ParamValues=0,0,812,500,500,500,500,500,500,326,500,500,500,844,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
Collapsed=1
MeshIndex=4

[AppSet11]
App=HUD\HUD 3D
ParamValues=0,0,196,500,500,500,500,500,500,672,500,500,500,844,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
Collapsed=1
MeshIndex=4

[AppSet13]
App=HUD\HUD Mesh
ParamValues=0,764,896,0,363,500,500,0,50,150,500,500,500,500,500,1
Enabled=1
Collapsed=1
MeshIndex=3

[AppSet6]
App=HUD\HUD Mesh
ParamValues=0,500,0,1000,662,500,443,2,0,150,500,500,1000,500,548,1
Enabled=1
Collapsed=1
MeshIndex=1

[AppSet7]
App=HUD\HUD Mesh
ParamValues=0,500,0,1000,703,500,521,2,0,150,500,500,1000,500,443,1
Enabled=1
Collapsed=1
MeshIndex=2

[AppSet1]
App=HUD\HUD Mesh
ParamValues=16,500,0,1000,366,500,456,1,0,150,500,500,500,500,604,0
Enabled=1
Collapsed=1

[AppSet4]
App=HUD\HUD Mesh
ParamValues=0,500,1000,0,376,500,456,2,0,150,500,500,500,500,604,0
Enabled=1
Collapsed=1

[AppSet5]
App=HUD\HUD Prefab
ParamValues=204,0,832,1000,0,500,599,432,568,1000,4,0,250,1,476,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B48CC4BCD298E290051BA0686167A9939650C23080000EF96072F

[AppSet3]
App=HUD\HUD Prefab
ParamValues=204,0,500,0,1000,500,591,432,568,1000,4,0,250,1,476,0,1000,0
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B48CC4BCD298E290051BA0686167A9939650C23080000EF96072F

[AppSet18]
App=Image effects\Image
ParamValues=0,0,0,0,1000,644,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet17]
App=Image effects\Image
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet19]
App=Image effects\Image
ParamValues=0,0,0,0,1000,633,492,0,0,750,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet22]
App=Image effects\Image
ParamValues=0,0,0,0,1000,625,500,0,0,750,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet23]
App=Image effects\Image
ParamValues=0,0,0,0,1000,606,507,0,0,750,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet20]
App=Image effects\Image
ParamValues=0,0,0,0,1000,365,348,0,0,250,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet21]
App=Image effects\Image
ParamValues=0,0,0,0,1000,372,356,0,0,250,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet24]
App=Image effects\Image
ParamValues=0,0,0,0,1000,393,363,0,0,250,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet2]
App=Postprocess\Youlean Bloom
ParamValues=500,0,294,450,1000,0,0,0
Enabled=1
Collapsed=1

[AppSet25]
App=Postprocess\Youlean Color Correction
ParamValues=500,500,500,500,500,500
Enabled=1
Collapsed=1

[AppSet8]
App=Text\TextTrueType
ParamValues=0,0,0,0,0,500,500,0,0,0,500
Enabled=1

[AppSet26]
App=HUD\HUD Prefab
ParamValues=0,0,500,0,828,659,344,122,1000,1000,4,0,500,1,368,108,1000,1
Enabled=1
Name=LOGO
LayerPrivateData=78DA73C8CBCF4BD52B2E4B671899000060F9036F

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0

[UserContent]
Text="This is the default text."
Html="<position x=""2""><position y=""5""><p align=""center""><font face=""04b03_COMP_MAT-Regular"" size=""5"" color=""#333333"">[author]</font></p></position>","<position x=""2""><position y=""10""><p align=""center""><font face=""04b03_COMP_MAT-Regular"" size=""4"" color=""#333333"">[title]</font></p></position>","<position x=""58""><position y=""26""><p align=""left""><font face=""Chosence-bold"" size=""3"" color=""#333333"">[extra1]</font></p></position>","<position x=""0""><position y=""19""><p align=""center""><font face=""Chosence-bold"" size=""2"" color=""#333333"">[comment]</font></p></position>",,"<position x=""30""><position y=""26""><p align=""left""><font face=""Chosence-bold"" size=""3"" color=""#333333"">[extra2]</font></p></position>","<position x=""30""><position y=""29""><p align=""left""><font face=""Chosence-bold"" size=""2.5"" color=""#333333"">[extra3]</font></p></position>","  ",
Meshes=[plugpath]Content\Meshes\Heroine.zgeMesh,"[presetpath]_Wizard\Assets\Sacco\Buildings solid sm 02.zgeMesh","[presetpath]_Wizard\Assets\Sacco\Buildings tri solid sm 1.zgeMesh",[plugpath]Content\Meshes\Sphere.zgeMesh
VideoUseSync=0
EnableMipmap=1

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

