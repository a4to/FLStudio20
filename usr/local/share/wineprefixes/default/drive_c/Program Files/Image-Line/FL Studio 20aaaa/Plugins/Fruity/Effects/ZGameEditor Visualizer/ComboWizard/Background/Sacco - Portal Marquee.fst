FLhd   0 * ` FLdt
f  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ���{e  ﻿[General]
GlWindowMode=1
LayerCount=48
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=47,49,48,45,1,40,42,0,4,41,3,5,31,32,28,35,34,29,25,26,22,23,20,19,17,16,39,44,43,7,6,2,9,10,11,12,13,14,46,18,21,24,27,30,33,36,8,15
WizardParams=2241,225,321,33,369

[AppSet47]
App=HUD\HUD Prefab
FParamValues=248,0.0007,0.844,0.896,0,0.5,0.5,0.65,1,1,4,0,0.325,1,0.368,0,1,0
ParamValues=248,0,844,896,0,500,500,650,1000,1000,4,0,325,1,368,0,1000,0
ParamValuesF=241,0.2286,0.844,0.896,0,0.5,0.5,0.65,1,1,4,0,0.804,1,0.368,0,1,1
Enabled=1
LayerPrivateData=78012BCE4DCCC95148CD49CD4DCD2B298E49CE2C4ACE498552BA0606C67A9939650CC31800008C770D6C

[AppSet49]
App=HUD\HUD Prefab
FParamValues=248,0,0.5,0.896,0,0.5,0.5,0.548,1,1,4,0,0.282,1,0.368,0,1,0
ParamValues=248,0,500,896,0,500,500,548,1000,1000,4,0,282,1,368,0,1000,0
ParamValuesF=241,0,0.5,0.896,0,0.5,0.5,0.548,1,1,4,0,0.487,1,0.368,0,1,1
Enabled=1
LayerPrivateData=78012BCE4DCCC95148CD49CD4DCD2B298E49CE2C4ACE498552BA0606C67A9939650CC31800008C770D6C

[AppSet48]
App=HUD\HUD Prefab
FParamValues=251,0.9993,0.864,0.908,0,0.5,0.5,0.938,1,1,4,0,0.521,1,0.368,0,1,0
ParamValues=251,999,864,908,0,500,500,938,1000,1000,4,0,521,1,368,0,1000,0
ParamValuesF=244,0.7714,0.864,0.908,0,0.5,0.5,0.938,1,1,4,0,0.658,1,0.368,0,1,1
Enabled=1
LayerPrivateData=78012BCE4DCCC95148CD49CD4DCD2B298E49CE2C4ACE498552BA0606667A9939650CC31800008F1A0D6F

[AppSet45]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=1,0,0.15,1,1,0,0,0
ParamValues=1000,0,150,1000,1000,0,0,0
ParamValuesF=1,0,0.15,1,1,0,0,0
Enabled=1
UseBufferOutput=1

[AppSet1]
App=Background\SolidColor
FParamValues=0,0,0,0.996
ParamValues=0,0,0,996
ParamValuesF=0,0,0,0.996
Enabled=1

[AppSet40]
App=Background\Grid
FParamValues=0.878,0,0,1,0.224,0.224,1
ParamValues=878,0,0,1000,224,224,1000
ParamValuesF=0.878,0,0,1,0.224,0.224,1
ParamValuesHUD\HUD Prefab=66,0,500,0,0,500,500,250,1000,1000,444,0,500,1000,368,0,1000,1000
Enabled=1

[AppSet42]
App=Image effects\Image
FParamValues=0.424,0,1,0.44,0.904,0.5,0.5,0,0,0,0,0,0,0
ParamValues=424,0,1000,440,904,500,500,0,0,0,0,0,0,0
ParamValuesF=0.424,0,1,0.44,0.904,0.5,0.5,0,0,0,0,0,0,0
Enabled=1
ImageIndex=163

[AppSet0]
App=Object Arrays\CrystalCube
FParamValues=0.519,0.596,0.776,0.5,0,0.5,0.5,0.231
ParamValues=519,596,776,500,0,500,500,231
ParamValuesF=0.519,0.596,0.776,0.5,0,0.5,0.5,0.231
ParamValuesMidi\Casio SK-5=0,0,0,750,875,800,500,500,250,372,300,132,0,1000
ParamValuesMidi\Midi Spiral=0,750,500,500,250,248,0,0,0,125,502,0
Enabled=1
ImageIndex=167

[AppSet4]
App=Object Arrays\CrystalCube
FParamValues=0.999,0.28,0.776,0.5,0,0.5,0.5,0.231
ParamValues=999,280,776,500,0,500,500,231
ParamValuesF=0.999,0.28,0.776,0.5,0,0.5,0.5,0.231
Enabled=1
ImageIndex=167

[AppSet41]
App=Misc\Automator
FParamValues=1,50,13,3,0,0.024,0.75,1,49,13,3,0,0.016,0.75,1,48,13,3,0,0.056,0.75,0,0,0,0,0,0.25,0.25
ParamValues=1,50,13,3,0,24,750,1,49,13,3,0,16,750,1,48,13,3,0,56,750,0,0,0,0,0,250,250
ParamValuesF=1,50,13,3,0,0.024,0.75,1,49,13,3,0,0.016,0.75,1,48,13,3,0,0.056,0.75,0,0,0,0,0,0.25,0.25
Enabled=1

[AppSet3]
App=Misc\Automator
FParamValues=1,48,2,2,0.5,0.25,0.75,1,49,2,2,0.5,0.25,0.25,1,8,1,2,0.5,0.25,0.75,1,7,1,2,0.5,0.25,0.25
ParamValues=1,48,2,2,500,250,750,1,49,2,2,500,250,250,1,8,1,2,500,250,750,1,7,1,2,500,250,250
ParamValuesF=1,48,2,2,0.5,0.25,0.75,1,49,2,2,0.5,0.25,0.25,1,8,1,2,0.5,0.25,0.75,1,7,1,2,0.5,0.25,0.25
Enabled=1
UseBufferOutput=1

[AppSet5]
App=Background\SolidColor
FParamValues=0,0,0,0.996
ParamValues=0,0,0,996
ParamValuesF=0,0,0,0.996
Enabled=1
UseBufferOutput=1

[AppSet31]
App=HUD\HUD Free Line
AppVersion=1
FParamValues=0,0,0,0,0.643,0.761,0.2988,0.1,0,0.1,0,0.1,1,0,0,0.423,0.5
ParamValues=0,0,0,0,643,761,298,100,0,100,0,100,1,0,0,423,500
ParamValuesF=0,0,0,0,0.643,0.761,0.3739,0.1,0,0.1,0,0.1,1,0,0,0.423,0.5
Enabled=1
LayerPrivateData=780163606060289D7CC30E48D9333034EC07D25030CA8604C4683890130E0071813F6D

[AppSet32]
App=HUD\HUD Free Line
AppVersion=1
FParamValues=0,0,0,0,0,0.761,0.2988,0.1,0,0.1,0,0.1,1,0,0,0.357,0.5
ParamValues=0,0,0,0,0,761,298,100,0,100,0,100,1,0,0,357,500
ParamValuesF=0,0,0,0,0,0.761,0.3739,0.1,0,0.1,0,0.1,1,0,0,0.357,0.5
Enabled=1
LayerPrivateData=780163606060D872629B1D90B2676068D80FA4A160940D0988D17020271C0030873FBF

[AppSet28]
App=HUD\HUD Free Line
AppVersion=1
FParamValues=0,0,0,0,0.684,0.667,0.2988,0.1,0,0.1,0,0.1,1,0,0,0.323,0.5
ParamValues=0,0,0,0,684,667,298,100,0,100,0,100,1,0,0,323,500
ParamValuesF=0,0,0,0,0.684,0.667,0.3739,0.1,0,0.1,0,0.1,1,0,0,0.323,0.5
Enabled=1
LayerPrivateData=780163606060704A586A07A4EC19181AF60369281865430262341CC80907000DD73ED4

[AppSet35]
App=HUD\HUD Free Line
AppVersion=1
FParamValues=0,0,0,0,0,0.913,0.2988,0.1,0,0.1,0,0.1,1,0,0,0.405,0.5
ParamValues=0,0,0,0,0,913,298,100,0,100,0,100,1,0,0,405,500
ParamValuesF=0,0,0,0,0,0.913,0.3739,0.1,0,0.1,0,0.1,1,0,0,0.405,0.5
Enabled=1
LayerPrivateData=780163606060D08C396F07A4EC19181AF60369281865430262341CC80907002BCB3EE1

[AppSet34]
App=HUD\HUD Free Line
AppVersion=1
FParamValues=0,0,0,0,0.591,0.913,0.2988,0.1,0,0.1,0,0.1,1,0,0,0.423,0.5
ParamValues=0,0,0,0,591,913,298,100,0,100,0,100,1,0,0,423,500
ParamValuesF=0,0,0,0,0.591,0.913,0.3739,0.1,0,0.1,0,0.1,1,0,0,0.423,0.5
Enabled=1
LayerPrivateData=780163606060289D7CC30E48D9333034EC07D25030CA8604C4683890130E0071813F6D

[AppSet29]
App=HUD\HUD Free Line
AppVersion=1
FParamValues=0,0,0,0,0,0.667,0.2988,0.1,0,0.1,0,0.1,1,0,0,0.316,0.5
ParamValues=0,0,0,0,0,667,298,100,0,100,0,100,1,0,0,316,500
ParamValuesF=0,0,0,0,0,0.667,0.3739,0.1,0,0.1,0,0.1,1,0,0,0.316,0.5
Enabled=1
LayerPrivateData=78016360606038786AA11D90B2676068D80FA4A160940D0988D17020271C0022B73FB9

[AppSet25]
App=HUD\HUD Free Line
AppVersion=1
FParamValues=0,0,0,0,0.698,0.608,0.2988,0.1,0,0.1,0,0.1,1,0,0,0.304,0.5
ParamValues=0,0,0,0,698,608,298,100,0,100,0,100,1,0,0,304,500
ParamValuesF=0,0,0,0,0.698,0.608,0.3739,0.1,0,0.1,0,0.1,1,0,0,0.304,0.5
Enabled=1
LayerPrivateData=78016360606078BC74B61D90B2676068D80FA4A160940D0988D17020271C000DF43FB0

[AppSet26]
App=HUD\HUD Free Line
AppVersion=1
FParamValues=0,0,0,0,0,0.608,0.2988,0.1,0,0.1,0,0.1,1,0,0,0.303,0.5
ParamValues=0,0,0,0,0,608,298,100,0,100,0,100,1,0,0,303,500
ParamValuesF=0,0,0,0,0,0.608,0.3739,0.1,0,0.1,0,0.1,1,0,0,0.303,0.5
Enabled=1
LayerPrivateData=780163606060B8A834DB0E48D9333034EC07D25030CA8604C4683890130E00B3753F1B

[AppSet22]
App=HUD\HUD Free Line
AppVersion=1
FParamValues=0,0,0,0,0.704,0.564,0.2988,0.1,0,0.1,0,0.1,1,0,0,0.304,0.5
ParamValues=0,0,0,0,704,564,298,100,0,100,0,100,1,0,0,304,500
ParamValuesF=0,0,0,0,0.704,0.564,0.3739,0.1,0,0.1,0,0.1,1,0,0,0.304,0.5
Enabled=1
LayerPrivateData=78016360606078BC74B61D90B2676068D80FA4A160940D0988D17020271C000DF43FB0

[AppSet23]
App=HUD\HUD Free Line
AppVersion=1
FParamValues=0,0,0,0,0,0.564,0.2988,0.1,0,0.1,0,0.1,1,0,0,0.296,0.5
ParamValues=0,0,0,0,0,564,298,100,0,100,0,100,1,0,0,296,500
ParamValuesF=0,0,0,0,0,0.564,0.3739,0.1,0,0.1,0,0.1,1,0,0,0.296,0.5
Enabled=1
LayerPrivateData=78016360606008E89D6E07A4EC19181AF60369281865430262341CC8090700768A3F01

[AppSet20]
App=HUD\HUD Free Line
AppVersion=1
FParamValues=0,0,0,0,0.706,0.53,0.2988,0.1,0,0.1,0,0.1,1,0,0,0.304,0.5
ParamValues=0,0,0,0,706,530,298,100,0,100,0,100,1,0,0,304,500
ParamValuesF=0,0,0,0,0.706,0.53,0.3739,0.1,0,0.1,0,0.1,1,0,0,0.304,0.5
Enabled=1
LayerPrivateData=78016360606078BC74B61D90B2676068D80FA4A160940D0988D17020271C000DF43FB0

[AppSet19]
App=HUD\HUD Free Line
AppVersion=1
FParamValues=0,0,0,0,0,0.53,0.2988,0.1,0,0.1,0,0.1,1,0,0,0.294,0.5
ParamValues=0,0,0,0,0,530,298,100,0,100,0,100,1,0,0,294,500
ParamValuesF=0,0,0,0,0,0.53,0.3739,0.1,0,0.1,0,0.1,1,0,0,0.294,0.5
Enabled=1
LayerPrivateData=780163606060D06E9F6607A4EC19181AF60369281865430262341CC809070010223ED5

[AppSet17]
App=HUD\HUD Free Line
AppVersion=1
FParamValues=0,0,0,0,0.707,0.507,0.2988,0.1,0,0.1,0,0.1,1,0,0,0.304,0.5
ParamValues=0,0,0,0,707,507,298,100,0,100,0,100,1,0,0,304,500
ParamValuesF=0,0,0,0,0.707,0.507,0.3739,0.1,0,0.1,0,0.1,1,0,0,0.304,0.5
Enabled=1
LayerPrivateData=78016360606078BC74B61D90B2676068D80FA4A160940D0988D17020271C000DF43FB0

[AppSet16]
App=HUD\HUD Free Line
AppVersion=1
FParamValues=0,0,0,0,0,0.507,0.2988,0.1,0,0.1,0,0.1,1,0,0,0.293,0.5
ParamValues=0,0,0,0,0,507,298,100,0,100,0,100,1,0,0,293,500
ParamValuesF=0,0,0,0,0,0.507,0.3739,0.1,0,0.1,0,0.1,1,0,0,0.293,0.5
Enabled=1
UseBufferOutput=1
LayerPrivateData=7801636060609064996607A4EC19181AF60369281865430262341CC8090700B5A33E40

[AppSet39]
App=Background\SolidColor
FParamValues=0,0,0,0.996
ParamValues=0,0,0,996
ParamValuesF=0,0,0,0.996
Enabled=1

[AppSet44]
App=Background\Grid
FParamValues=0.983,0,0,1,0.224,0.224,1
ParamValues=983,0,0,1000,224,224,1000
ParamValuesF=0.983,0,0,1,0.224,0.224,1
Enabled=1

[AppSet43]
App=Image effects\Image
FParamValues=0.424,0,0,1,0.904,0.5,0.5,0,0,0,0,0,0,0
ParamValues=424,0,0,1000,904,500,500,0,0,0,0,0,0,0
ParamValuesF=0.424,0,0,1,0.904,0.5,0.5,0,0,0,0,0,0,0
Enabled=1
ImageIndex=162

[AppSet7]
App=Object Arrays\CrystalCube
FParamValues=0.0007,0,0.776,0.5,0,0.5,0.5,0.231
ParamValues=0,0,776,500,0,500,500,231
ParamValuesF=0.2286,0,0.776,0.5,0,0.5,0.5,0.231
ParamValuesMidi\Casio SK-5=0,0,0,750,875,800,500,500,250,372,300,132,0,1000
ParamValuesMidi\Midi Spiral=0,750,500,500,250,248,0,0,0,125,502,0
Enabled=1
ImageIndex=38

[AppSet6]
App=Object Arrays\CrystalCube
FParamValues=0.9993,0.748,0.776,0.5,0,0.5,0.5,0.231
ParamValues=999,748,776,500,0,500,500,231
ParamValuesF=0.7714,0.748,0.776,0.5,0,0.5,0.5,0.231
Enabled=1
ImageIndex=40

[AppSet2]
App=Postprocess\TransitionEffects
FParamValues=4,0.6747,0.5,0.5,0.5
ParamValues=4,674,500,500,500
ParamValuesF=4,0.8794,0.5,0.5,0.5
Enabled=1
ImageIndex=164

[AppSet9]
App=Misc\Automator
FParamValues=1,3,2,2,0.5,0.086,0.75,0,5,1,2,0.5,0.25,0.25,0,8,1,2,0.5,0.25,0.75,0,7,1,2,0.5,0.25,0.25
ParamValues=1,3,2,2,500,86,750,0,5,1,2,500,250,250,0,8,1,2,500,250,750,0,7,1,2,500,250,250
ParamValuesF=1,3,2,2,0.5,0.086,0.75,0,5,1,2,0.5,0.25,0.25,0,8,1,2,0.5,0.25,0.75,0,7,1,2,0.5,0.25,0.25
Enabled=1

[AppSet10]
App=HUD\HUD Prefab
FParamValues=23,0,0.5,0,0,0.5,0.5,0.176,1,1,4,0,0.2109,1,0.22,0,0.047,0
ParamValues=23,0,500,0,0,500,500,176,1000,1000,4,0,210,1,220,0,47,0
ParamValuesF=16,0,0.5,0,0,0.5,0.5,0.176,1,1,4,0,0.6543,1,0.22,0,0.047,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0CCCF43273CA18863D00000D540AA1

[AppSet11]
App=HUD\HUD Prefab
FParamValues=23,0,0.5,0,0,0.5,0.5,0.173,1,1,4,0,0.1563,1,0.22,0.047,0.069,0
ParamValues=23,0,500,0,0,500,500,173,1000,1000,4,0,156,1,220,47,69,0
ParamValuesF=16,0,0.5,0,0,0.5,0.5,0.173,1,1,4,0,0.3829,1,0.22,0.047,0.069,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0CCCF43273CA18863D00000D540AA1

[AppSet12]
App=HUD\HUD Prefab
FParamValues=23,0,0.5,0,0,0.5,0.5,0.199,1,1,4,0,0.4219,1,0.22,0.07,0.126,0
ParamValues=23,0,500,0,0,500,500,199,1000,1000,4,0,421,1,220,70,126,0
ParamValuesF=16,0,0.5,0,0,0.5,0.5,0.199,1,1,4,0,0.3085,1,0.22,0.07,0.126,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0CCCF43273CA18863D00000D540AA1

[AppSet13]
App=Postprocess\ParameterShake
FParamValues=1,0,10,12,1,0,11,12,3,4,0,0,9,8,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=1000,0,10,12,1000,0,11,12,3,4,0,0,9,8,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=1,0,10,12,1,0,11,12,3,4,0,0,9,8,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet14]
App=Postprocess\ParameterShake
FParamValues=1,0,12,12,1,0,11,12,3,4,0,0,8,7,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=1000,0,12,12,1000,0,11,12,3,4,0,0,8,7,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=1,0,12,12,1,0,11,12,3,4,0,0,8,7,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet46]
App=Image effects\Image
FParamValues=0,0,0.728,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,728,0,1000,500,500,0,0,0,0,0,0,0
ParamValuesF=0,0,0.728,0,1,0.5,0.5,0,0,0,0,0,0,0
Enabled=1
ImageIndex=166

[AppSet18]
App=Postprocess\ParameterShake
FParamValues=0.704,0,16,6,0.704,0,17,6,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=704,0,16,6,704,0,17,6,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=0.704,0,16,6,0.704,0,17,6,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet21]
App=Postprocess\ParameterShake
FParamValues=0.704,0,19,6,0.704,0,20,6,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=704,0,19,6,704,0,20,6,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=0.704,0,19,6,0.704,0,20,6,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet24]
App=Postprocess\ParameterShake
FParamValues=0.704,0,23,6,0.704,0,22,6,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=704,0,23,6,704,0,22,6,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=0.704,0,23,6,0.704,0,22,6,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet27]
App=Postprocess\ParameterShake
FParamValues=0.704,0,26,6,0.704,0,25,6,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=704,0,26,6,704,0,25,6,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=0.704,0,26,6,0.704,0,25,6,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet30]
App=Postprocess\ParameterShake
FParamValues=0.704,0,29,6,0.704,0,28,6,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=704,0,29,6,704,0,28,6,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=0.704,0,29,6,0.704,0,28,6,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet33]
App=Postprocess\ParameterShake
FParamValues=0.704,0,32,6,0.704,0,31,6,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=704,0,32,6,704,0,31,6,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=0.704,0,32,6,0.704,0,31,6,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet36]
App=Postprocess\ParameterShake
FParamValues=0.704,0,35,6,0.704,0,34,6,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=704,0,35,6,704,0,34,6,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=0.704,0,35,6,0.704,0,34,6,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet8]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.5,0,0.15,0.45,1,0,0,0
ParamValues=500,0,150,450,1000,0,0,0
ParamValuesF=0.5,0,0.15,0.45,1,0,0,0
Enabled=1

[AppSet15]
App=Text\TextTrueType
FParamValues=0,0,0,0,0,0.5,0.5,0,0,0,0.5
ParamValues=0,0,0,0,0,500,500,0,0,0,500
ParamValuesF=0,0,0,0,0,0.5,0.5,0,0,0,0.5
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="    "
Images="[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-001.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-002.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-003.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-004.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-005.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-006.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-007.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-008.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-009.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-010.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-011.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-012.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-013.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-014.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-015.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-016.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-017.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-018.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-019.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-020.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-021.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-022.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-023.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-024.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-025.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-026.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-027.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-028.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-029.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-030.ilv","[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-031.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-001.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-002.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-003.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-004.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-005.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-006.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-007.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-008.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-009.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-010.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-011.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-012.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-013.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-014.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-015.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-016.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-017.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-018.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-019.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-020.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-021.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-022.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-023.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-024.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-025.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-026.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-027.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-028.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-029.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-030.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-031.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-032.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-033.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-034.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-035.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-036.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-037.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-038.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-039.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-040.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-041.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-042.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-043.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-044.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-045.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-046.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-047.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-048.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-049.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-050.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-051.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-052.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-053.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-054.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-055.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-056.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-057.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-058.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-059.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-060.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-061.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-062.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-063.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-064.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-065.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-066.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-067.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-068.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-069.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-070.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-071.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-072.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-073.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-074.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-075.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-076.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-077.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-078.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-079.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-080.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-081.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-082.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-083.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-084.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-085.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-086.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-087.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-088.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-089.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-090.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-091.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-092.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-093.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-094.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-095.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-096.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-097.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-098.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-099.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-100.ilv","[plugpath]Effects\HUD\prefabs\small elements\circle\circle-101.ilv",[plugpath]Effects\HUD\prefabs\components\component-001.ilv,[plugpath]Effects\HUD\prefabs\components\component-002.ilv,[plugpath]Effects\HUD\prefabs\components\component-003.ilv,[plugpath]Effects\HUD\prefabs\components\component-004.ilv,[plugpath]Effects\HUD\prefabs\components\component-005.ilv,[plugpath]Effects\HUD\prefabs\components\component-006.ilv,[plugpath]Effects\HUD\prefabs\components\component-007.ilv,[plugpath]Effects\HUD\prefabs\components\component-008.ilv,[plugpath]Effects\HUD\prefabs\components\component-009.ilv,[plugpath]Effects\HUD\prefabs\components\component-010.ilv,[plugpath]Effects\HUD\prefabs\components\component-011.ilv,[plugpath]Effects\HUD\prefabs\components\component-012.ilv,[plugpath]Effects\HUD\prefabs\components\component-013.ilv,[plugpath]Effects\HUD\prefabs\components\component-014.ilv,[plugpath]Effects\HUD\prefabs\components\component-015.ilv,[plugpath]Effects\HUD\prefabs\components\component-016.ilv,[plugpath]Effects\HUD\prefabs\components\component-017.ilv,[plugpath]Effects\HUD\prefabs\components\component-018.ilv,[plugpath]Effects\HUD\prefabs\components\component-019.ilv,[plugpath]Effects\HUD\prefabs\components\component-020.ilv,[plugpath]Effects\HUD\prefabs\components\component-021.ilv,[plugpath]Effects\HUD\prefabs\components\component-022.ilv,[plugpath]Effects\HUD\prefabs\components\component-023.ilv,[plugpath]Effects\HUD\prefabs\components\component-024.ilv,[plugpath]Effects\HUD\prefabs\components\component-025.ilv,[plugpath]Effects\HUD\prefabs\components\component-026.ilv,[plugpath]Effects\HUD\prefabs\components\component-027.ilv,[plugpath]Effects\HUD\prefabs\components\component-028.ilv,[plugpath]Effects\HUD\prefabs\components\component-029.ilv,[plugpath]Effects\HUD\prefabs\components\component-030.ilv,[plugpath]Content\Bitmaps\Particles\earth1.png,"[presetpath]Wizard\Assets\Sacco\Circle - Black.svg"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

