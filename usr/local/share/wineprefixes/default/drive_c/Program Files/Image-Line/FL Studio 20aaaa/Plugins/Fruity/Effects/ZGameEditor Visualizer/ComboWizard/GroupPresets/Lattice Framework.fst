FLhd   0  0 FLdtS  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   ��3�  ﻿[General]
GlWindowMode=1
LayerCount=8
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=2,6,5,4,3,0,1,7

[AppSet2]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
ParamValuesPeak Effects\Youlean Peak Shapes=0,0,0,0,136,500,368,500,490,104,564,500,120,144,500,0,0,200,200,360,358,0,520,0,0,1000,0,500,500,0,0,0
ParamValuesPeak Effects\Stripe Peeks=0,0,0,0,0,0,74,500,220,0,250,500,700,0,500,150,300,200,200,300,1000,0,0
ParamValuesPeak Effects\Linear=0,964,0,0,178,752,508,112,250,500,196,350,1000,1000,500,0,0,500,500,500,468,0,500,152,200,0,32,212,330,250,100
ParamValuesPeak Effects\Youlean Waveform=0,0,0,0,186,859,500,250,372,250,500,210,500,0,100,0,750,1000,16,88,0,500,100,100,0,0,282,500,500,1000,0,1000
ParamValuesPeak Effects\Polar=0,0,0,0,172,500,158,300,556,500,128,420,0,631,1000,500,1000,1000,968,1000,1000,0,0,1000
ParamValuesPeak Effects\SplinePeaks=940,0,0,0,900,500,360,0,0,0
ParamValuesMisc\FruityIndustry=0,0,0,0,500,500,0,500,500,0,500,500,500
ParamValuesHUD\HUD Graph Polar=0,500,0,0,500,856,106,444,784,0,1000,0,1000,0,0,500,0,348,0,0,500,0
Enabled=1
Name=Section2

[AppSet6]
App=HUD\HUD 3D
FParamValues=0,1,0.5,0.5032,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,0
ParamValues=0,1,500,503,500,500,500,500,500,500,500,500,500,500,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,0
Enabled=1
ImageIndex=3
Name=ForegroundMod

[AppSet5]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
ImageIndex=1
Name=TextFst

[AppSet4]
App=Text\TextTrueType
FParamValues=0,0.8333,0,0,0,0.5,0.5,0,0,0,0.5
ParamValues=0,833,0,0,0,500,500,0,0,0,500
Enabled=0
Name=Text

[AppSet3]
App=Postprocess\Youlean Drop Shadow
FParamValues=0.5,0.2,0,1,0.02,0.875,0
ParamValues=500,200,0,1000,20,875,0
Enabled=1
UseBufferOutput=1

[AppSet0]
App=Youlean new shaders\Tunnel\Lattice Framework
FParamValues=0,0.5,0.8,0,0.6
ParamValues=0,500,800,0,600
Enabled=1
Name=Section0

[AppSet1]
App=Postprocess\Youlean Color Correction
FParamValues=0.3844,0.5,0.6106,0.5,0.5,0.5
ParamValues=384,500,610,500,500,500
ParamValuesPostprocess\Vignette=0,0,0,576,1000,316
ParamValuesPostprocess\RGB Shift=246,300,0,600,700,200
ParamValuesYoulean new shaders\Postprocess\Extruded Video Image=208,297,0
ParamValuesPostprocess\Youlean Motion Blur=14,1000,0,500,0,0,1000
ParamValuesPostprocess\FrameBlur=1000,0,0,0,1000,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesPostprocess\Ascii=0,232,0,600,700,200
ParamValuesPostprocess\Dot Matrix=1000,500,500,0,368,500,1000,1000
Enabled=1
Name=Section1

[AppSet7]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
ImageIndex=2

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="This is the default text."
Images="@EmbeddedFst:""Name=Song Position horizontal.fst"",Data=78018D544B6EDB3010DD0BD01D04AF5D97A4FE4518C0DF06A8D3BA713E28922C58691213952541A2D3F86C5DF448BD426728DBB0D12C8A381467C8796FE671C83FBF7EDF7F84121A553CBACEC7E24E9779F5F3B2CA4172D799AB2D34E36A531AE9BBCE6CB124E7A5CEF5A26A8C7C87C6B0AD213357CAE84AF2E843BA8BF9D2E4D048D6E77DE13AAE733FACEB251886143893D77A0D1E12E94C99AA691FBE559B0254E959FF483544A51AB5BE55C5065A846183300AFAF46522212B8DAC25FC0829C81FC5E819F000770D429A0941234D190EF6AFDB89499F8277D088DBA15A4CC62884400932448B0079C8707E84C6193B415B80FAE14D9F9E5092F66151158A34D891C7A2C30993BE4F78446107240984A5F379E721BEE31D070C8C719D69A9BE1790D3511C94E5FFA3ECD2A875FD96B65D8A83204A51AED48A469AC5688900F5A6644867AB2CAD5825FBA7B563A55DDEAC4F4004D3D971FF0884AA26F75189B6BA93B26CDB2D1AFDA20C4C9451324E188FFDF1749C8859301D4F8251309D8471381E8E67411A719EC42922B3D1301AB260121F292376CA9C1CCD5C97F0669BA55D9B61A95C504351FF08665B49740D683DB4E47742ECA4D84BB3FFEE627948B1F88F23F3ED9CD3E8FBE426AC7FDA719F0225402A113DB7E4D68A58DFB73D684F0481F76AEEBF9D8788856D236415C8E9FB088581B6654F5BE856E75079F05AE3A546B5AC7921394BF080AD7127792AF6C6159478B767752B7D742DB1A50AC0FB0F72BF618C8F47F659AD41E26DDFE4BA3A7658BCAF1B5568B3A5884BF5FA09B633BCECB0AC55A6CB6784A52E9FE902CA0E64A44D43043C0E7822025CB4B007B7486CC44D9955EBBA81B6C5CB819B969B1A9AD6E64726DE959B965EB3D24049755EC3AB91BDEB956E3DFC991578393CA94D613C832B839EEB5C9875217B67B587E93E97B2D7CB30129A5EEFFC9EB614D8458F67EFEB73DC6AEB42FCE5B6CC880DD3C79D544E473D01A3B215E4C45BD5D24F037C29E1C948EEE39B79A773B3925182D30BD0CF2B23C3B86B624AB7A98A021A8CBC827CFE52481146F8543700E5C11AE1437930E69BF561EE3A7F017A538E8B","@EmbeddedFst:""Name=Handwritten celebrity signature.fst"",Data=7801C554CD6EDB300CBE07C83B1439679924CBB13D5405623B6E80B55BD7ACED21EB41B3B94680FF20CB5DF36C3BEC91F60AA3ECD86B6F2B76180CC724457E22BF90FCF5E3E7EE1C4AD032BF9F4ECEF33B5566D5F7CB2A0341A7930B79001D556D6984339D24575B6BBC5499BAAAB4116F50593535A4E65A1A5509BA7C171C633EEA0CB420733A67D3C974B25BD5F5160CC12B50129B9BF80BBE279FE1C95858A965712BF3161A0C210B778EBFFD77C109F3FF98D0EC04A872DF5FA24CE70EBE5D40178472F7605E2F315DD21FD82F22CE07DD5A1110E10634821EF6B4F7A0561D31D7A5FC9A43361273A5D5A334104B2385E7139A38911F2551C08328E2614C57215F45E1D2A3BE1F201061AB9012CEC93346E82B19218463BD1EE358BE2D79E150764C90619E0B87116728E5E861D9FC2B6210B52BFB0524020E78FF428DE744EBC867095F47310FF93A763D375A45090F62CA4286942D1DEABB3D4D64B5264B171B6E6C1CF65A9A6C57781E476E3A967C6EFF60FB742C31E27A4355AF6389CE11B563E90524020E78FF85A55B954175024F358E2552D5A91B41898FBDD6297782066C50AEA1C4E94CEA463868DACAA2CE012718C4E010E1F8A71F640102E7B5CD54F5DCD0E17D6A65AECCC1465CCAA7F770487084615BCB54950F084BF0205139943D48A88CB617508F539FD909E8604733F3BB889B32AD8A5A43D3E094A1D3B6AD41375D7E56C576B869EC3E2A0D94B64EBB3FC4CE2893C3FD7C275BB3AF340A0852F40E1B53E462765A9F60B60FA598CD52B4839ECDCE7606637355C2FDE9DBFA6C76A409E1B78732B59761F6E869ABE96F8EC1C8740F99BDB6AA8513705C75F0CD08EAE0D2BB5399D98BA58FE206D4C3DE08D7F3BA8C6DB6BACA73D018790DD9C5632E98BBC45DAB01CA510B71FB8DCA455B8CF274F21BD8A07C7E"
VideoUseSync=0
Filtering=0

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

[Wizard]
Section0Cb=Lattice Framework
Section1Cb=Brightness-Gamma-Contrast
Section2Cb=Song Position horizontal
Section3Cb=Handwritten celebrity signature
Macro0=Song Title
Macro1=Song Author
Macro2=Song made with love and time

