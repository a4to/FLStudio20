﻿














Copyright © 1998-2022 Image Line NV
All rights reserved




 




|Created by|
	Didier Dambrin (gol)




 
|Programming|
	Daniel Schaack (dan) §Developer§
	Eugene Kryukov §Developer§
	Frédéric Vanmol (reflex) §Lead Developer§
	Kyle Spratt §Developer§
	Mark Boyd (marko) §Developer§
	Maxx Claster §Developer§
	Maxim Zaev (maxklint) §Developer§
	Miroslav Krajcovic (miro) §Developer§
	Oliver Habersetzer (Parrik) §Developer§
	Paul Dunn (zxdunny) §Developer§
	Pierre M. (ShiniKnobz) §Developer§
	Viacheslav Slavinsky (svofski) §Developer§
	Ville Krumlinde (VilleK) §Developer§





|Other people at Image Line|
	Aish Agarwal §CEO§
	Angus F. Hewlett §CTO§
	Arlo Giunchi (nucleon) §Content & Training§
	Asier Recondo §Backoffice Developer§
	Christophe Dupire (Wiselabs) §Content§
	Deni Madzharov §Backoffice Developer§
	Frank Van Biesen (FVB) §Founder§
	Ief Goossens §Support & Website§ 
	Ivan Tsankov §Backoffice Developer§
	Jean-Marie Cannie (JMC) §Founder§
	Kim De Meyst §Distribution & Marketing§
	Leo König §Social Media§
	Milad Rezayi (Milinor) §Support§
	Mitko Georgiev §Backoffice Developer§
	Myriam Dupont §Accounts§
	Philipp Carstens (Phil.flp) §Support§
	Philip Tsvetkov §Backoffice Developer§	
	Saif Sameer §Content Developer§
	Scott Fisher §COO§
	Tom Apell (Evyi) §Support§

 



|Used technologies|
	§3DNow!§ is a trademark of Advanced Micro Devices
	
	§Acid§ is a trademark of Sonic Foundry, Inc
	
	§Anti-Grain§ Geometry by Maxim Shemanarev (McSeem)
	
	§ASIO§ Technology by Steinberg Media Technologies GmbH

	§Auftakt§ beat detection by zplane
	
	§BeatSlicer§ engine by Peter Segerdahl
	
	§Delphi FFmpeg headers§ (www.delphiffmpeg.com/headers)
	
	§DelphiZip§ (www.delphizip.org)
	
	§DirectX§ is a trademark of Microsoft
	
	§DrumSynth§ renderer by Paul Kellett
	
	§élastique Pro§ time stretching / pitch shifting by zplane
	
	This software uses libraries from the §FFmpeg project§ under the LGPLv2.1 (www.ffmpeg.org)
	
	§FLAC§ by Ogg Vorbis team at www.vorbis.com
	
	§FreeType§ font engine copyright © 2012 by The FreeType Project (www.freetype.org). All rights reserved
	
	§Integrated Performance Primitives§ by Intel

	§LAME§ MP3 encoder by Mark Taylor & Albert Faber (lame.sourceforge.io)

	§Metering§ peak detection by zplane

	§Ogg Vorbis§ by Ogg Vorbis team at www.vorbis.com

	§Python§ by the Python team at www.python.org

	§paxCompiler§ by Alexander Baranovsky
	
	§PDF Score Export§ by Jan Nieuwenhuizen (based on www.lilypond.org)

	§Reeverb§ engine by Ultrafunk

	§Restore§ noise removal by zplane
	
	§ReWire§ Interface Technology by Reason Studios, Sweden
	
	§REX§ is a trademark of Reason Studios, Sweden
	
	§Sample Format Translator§ by Chicken Systems

	§SimSynth§ 1 & 2 renderers by David Billen
	
	§SoftVoice§ speech synthesis by SoftVoice
	
	§SoundCloud®§ is a trademark of SoundCloud Limited
	
	§svgren§ SVG library by Ivan Gagis (github.com/igagis/svgren)
	
	§Symbiosis§ by Magnus Lidström (nuedge.net/article/5-symbiosis)
	
	§VST§ PlugIn Interface Technology by Steinberg Media Technologies GmbH
	
	§WavPack§ audio compressor by www.wavpack.com





|Image-Line Remote|
	Pierre M. (ShiniKnobz)





|FL-Chan and other anime artwork|
	uruido (uruido.web.fc2.com)

|Additional 3D / logo artwork|
	Ivan Tantsiura (ivangraphics)
	Daniil Kondratyev (andrakondra)





|Help / tutorials|
	Arlo Giunchi (nucleon)
	Leo M. König (SH-1)
	Pierre M. (ShiniKnobz)
	Scott Fisher [Editor]





| Songs / samples / presets|
	4one
	a:dump
	Adam Szabo
	Alex Hodge
	Alpha Strike
	Ammeris Gill
	Andre Lewis
	Andreas Zeug
	Andrew Greenwood
	Andrew Kelly
	Andy
	Arlo Giunchi (nucleon)
	Benoît Durand
	Blake Reary
	BSB
	Colin Gibbens
	Cometa
	Dan Brenner
	Dan Roberts
	Daven Hughes
	David Hazelden
	dg
	Didier Dambrin (gol)
	Dionysos Dajka
	DJ Deft (David Gradwell)
	Dominic Vallée
	ed chamberlain
	Eric Jones
	Eric Kaufman
	Erwin Fienieg
	Evildan 
	fboyz
	Frank Bongers
	Fuego
	Hugo  Leclercq (Madeon)
	Jason Cluts
	Jeff D. Hail (HailDamage)
	Joel Zimmerman (Deadmau5)
	Johan Vilborg
	John Pegg
	Joshua Hernandez
	Julijan Nikolic (Youlean)
	Jumpin' Johnathon Starr
	Kieron A. Gore
	Madeon
	Magnus 5
	Manu Bernard
	Manz-Trax
	Matias Monteagudo
	Matt Taylor
	Nick Kent
	Papelmedia (String ensemble soundfont)
	Pilchard
	Ranx
	Raquel J
	Reegz
	Robert Conde
	Robin Chard
	Sacco
	Sam Harrison
	Sam Scott
	Scott Fisher
	Shane Yates
	Stan Vasilev (mmlabs)
	Stephen McEniff
	Steve Duda
	Thomas Weber 
	Toby Emerson
	Tomas Duewiger
	Vassilatos Vangelis
	Vlad Ivanov
	Wiselabs
	ZeHoS 





|www.image-line.com website|
	Ief Goossens
	Ivan Tsankov
	Miroslav Krajcovic





|Plugins|
	Didier Dambrin:  
	§3xOSC, Autogun, BeepMap, Edison, EQUO, Fruit Kick, Fruity 7 Band EQ, Fruity Balance, Fruity Bass Boost, Fruity Big Clock, Fruity Center, Fruity Convolver, Fruity dB Meter, Fruity Delay, Fruity Delay 2, Fruity Delay Bank, Fruity Fast LP, Fruity Flangus, Fruity Free Filter, Fruity Granulizer, Fruity HTML Notebook, Fruity Keyboard Controller, Fruity Love Philter, Fruity Note Book, Fruity PanOMatic, Fruity Parametric EQ, Fruity Peak Controller, Fruity Phase Inverter, Fruity Send, Fruity Soft Clipper, Fruity Stereo Enhancer, Fruity Stereo Shaper, Fruity Vocoder, Fruity WaveShaper, Fruity X-Y Controller, Harmless, MIDI Out, Ogun, Soundgoodizer, Vocodex, Wave Candy§ 

	Maxx Claster:
	§BassDrum, Drumaxx, Drumpad, Effector, Groove Machine, Groove Machine Synth, Hardcore, MiniSynth, Morphine, Pitcher, Poizone, Sakura, Sawer, Toxic Biohazard§
	
	arguru, Frédéric Vanmol & Daniel Schaack:
	§DirectWave§  

	Paul Kellett (engine) & Frédéric Vanmol	(conversion), Didier Dambrin (GUI): 
	§Drumsynth Live§

	Paul Kellett (original code), Frédéric Vanmol
	(conversion), Didier Dambrin (GUI):
	§DX10§ 

	Didier Dambrin (Code). Character by uruido
	(uruido.web.fc2.com):
	§Fruity Dance§
	
	Maxx:
	§FL Studio Mobile§

	Robert Conde & Frédéric Vanmol:
	§FPC§

	David Billen:
	§Fruity blood overdrive§

	Didier Dambrin, Frédéric Vanmol:
	§Fruity Envelope Controller, Fruity Fast Distortion§

	Didier Dambrin (Plugin & GUI), Frédéric Vanmol (VST Port), Laurent de Soras (Saturation algorithm), Robert Bristow-Johnson (Filter algorithm):
	§Fruity Filter§

	Didier Dambrin, David Billen (algorithm): 
	§Fruity Filter§

	Didier Dambrin, Andrew Tumashinov (RapidEvaluator):
	§Fruity Formula Controller§

	Didier Dambrin, Chris Moulios (source code):
	§Fruity LSD§

	slim slow slider (code), Didier Dambrin (GUI):
	§Fruity Multiband Compressor§

	Robert Conde:
	§Fruity mute & Fruity scratcher§

	Frédéric Vanmol & Joel Zimmerman (Deadmau5):
	§Fruity Squeeze§

	Didier Dambrin, Robert Bristow-Johnson
	(EQ Cookbook): 
	§Fruity Parametric EQ2§

	Smart Electron:x:
	§Fruity phaser & flanger§

	Ultrafunk:
	§Fruity Reeverb§

	Ultrafunk (Algorithms), Didier Dambrin (GUI), Frédéric Vanmol (VST Translation):
	§Fruity Reeverb 2§

	Didier Dambrin (Slicer plugin), Peter Segerdahl (BeatSlicer engine):
	§Fruity Slicer§

	Didier Dambrin, FFT analysis.
	FFTW library (http://www.fftw.org/):
	§Fruity Spectroman§

	Didier Dambrin, Frédéric Vanmol:
	§Fruity Vibrator§

	Didier Dambrin (Code & GUI), Frédéric Vanmol (VST Port):
	§Gross Beat§

	Didier Dambrin (Code & GUI), Frédéric Vanmol (VST Port), Harmless Chan character uruido (uruido.web.fc2.com):
	§Harmless§

	Didier Dambrin (Code & GUI), Frédéric Vanmol (VST Port), Harmor Chan character uruido (uruido.web.fc2.com):
	§Harmor§

	zplane (Pitch & time engine), Frédéric Vanmol (Code & GUI), Didier Dambrin (Skin design): 
	§Newtone§

	Didier Dambrin (Code & GUI), Perry Cook (thanks for his work on physical modelling):
	§Plucked§

	David Billen (Engine), Frédéric Vanmol (Conversion), Didier Dambrin (GUI):
	§Simsynth Live§

	Didier Dambrin (Code & GUI), Script compiler (http://www.paxcompiler.com/):
	§Slicex§
	
	Miro (GUI), Benjamin 'BeRo' Rosseaux (Sobanth engine):
	§Soundfont Player§

	Didier Dambrin (Code & GUI), Bram (Oversampling algorithms), Antti, Robert Bristow-Johnson (Filter designs):
	§Sytrus§

	Richard Hoffmann:
	§TS404§
	
	Daniel Schaack (code) & Miroslav Krajcovic (GUI): 
	§Transient Shaper, Transistor Bass, Fruity Delay 3§
	
	Paul Dunn:
	§VFX Level Scaler§

	Richard Hoffmann (code), Didier Dambrin (GUI):
	§Wasp, Wasp XT§

	Ville Krumlinde:
	§ZGameEditor Visualizer§





|Many thanks to|
	Anders Melander (drag&drop components)
	Andrew V Tumashinov (RapidEvaluator component)
	Angus Johnson (components)
	arguru (interpolation stuff & misc assistance)
	Brad Stowers (components)
	Cyril Finot & Maxime Carbonnelle (technical assistance)
	David Churcher (MIDI components)
	David Fordham (mic 3D model)
	David Hoskins (shaders for ZGE Visualizer)
	Eugene Roshal (WinRAR)
	Filip "movAX13h" Sound (shaders for ZGE Visualizer)
	François Piette (components)
	Gordon Alex Cowie III (components)
	Hiroyuki Hori (components)
	Inigo Quilez - Shadertoy.com (shaders for ZGE Visualizer)
	Isa (xmas skin)
	Jean Lacoste (components)
	Jerome Schmitt (old equalizer plugin)
	Joiro Hatagaya (old font artwork)
	Jordan Russell (components)
	JPH Wacheski (shaders for ZGE Visualizer)
	Julien Barthelemy (LMHY bot wallpaper)
	Jurgen Faul (ID3v2 tag reading)
	Justin Mchale (web space)
	Kieron A Gore (DrumSynth GM preset)
	Kjell Zijlmans (shaders for ZGE Visualizer)
	kuroda Dycoon (DirectSound header translation)
	Laurent de Soras (FFT)
	Liming Xu (shaders for ZGE Visualizer)
	Marcel van Brakel (components)
	Marivie A Galeon and Lawrence Blue B Cuenca (ID3v1 tag reading)		
	Markus Oberhumer & Laszlo Molnar (UPX)
	Martijn van Engeland (B-splines)
	Martin Fay (OpenAsio)
	MAZ (web space & samples)
	Mercury (plugin database)
	Mike Lischke (tree component)
	nimitz (nimitz_stg@hotmail.com) (shaders for ZGE Visualizer)
	Olli Niemitalo (resonant filter formula & sound stuff)
	paq (wallpapers, FL robot)	
	Paul Rathey (kibou) (mirrors)
	Paulo Falcao (shaders for ZGE Visualizer)
	Perry R Cook (reverb models in STK98)
	Peter Thörnqvist (components)
	R Schultz (components)
	Rebirth 338 & HammerHead (inspiration)
	Robert Bristow-Johnson (equalizer info)
	Robin Abresch (Robiaster) (testing and support)
	Ryan J Mills (components)
	Sylvain R. (Sickness).
	Tord Jansson & Jukka Poikolainen (BladeEnc DLL)
	tric (MP3 stuff)


	
	The songwriting team for the cool demo tunes...
	
	
	...and all the betatesters (you!)





|FL Studio|
	The fastest way from your brain to your speakers

















  