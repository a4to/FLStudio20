FLhd   0 * ` FLdt�  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV մ:0  ﻿[General]
GlWindowMode=1
LayerCount=18
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=1,2,3,4,5,6,7,8,9,10,0,14,16,15,13,18,17,12

[AppSet1]
App=Terrain\GoopFlow
FParamValues=0.424,1,1,0,1,0.5,0.304,0,0.5,0.496,0.324,0.496,1,0.3,0.168,0.092
ParamValues=424,1000,1000,0,1000,500,304,0,500,496,324,496,1000,300,168,92
ParamValuesCanvas effects\DarkSpark=500,24,500,500,500,500,500,500,500,500,0,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesCanvas effects\SkyOcean=0,0,500,0,500,604,500,0,500,1000,388,0,0
ParamValuesMisc\FruityIndustry=0,0,1000,0,0,500,500,500,500,0,500,500,500
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,838,500,500,500,500,500
ParamValuesMisc\Automator=0,74,30,429,0,282,466,1000,185,61,286,508,526,254,1000,111,91,571,0,250,250,0,0,0,0,0,250,250,0,0,0,0
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
Enabled=1
Collapsed=1
ImageIndex=3
Name=Water 1

[AppSet2]
App=Terrain\GoopFlow
FParamValues=0.512,1,0.868,0,1,0.5,0.304,0,0.5,0.496,0.324,0.46,1,0.3,0.308,0.128
ParamValues=512,1000,868,0,1000,500,304,0,500,496,324,460,1000,300,308,128
Enabled=1
Collapsed=1
ImageIndex=3
Name=Water 2

[AppSet3]
App=Terrain\GoopFlow
FParamValues=0.104,0.628,1,0.168,1,0.5,0.304,0,0.5,0.496,0.104,0.46,1,0.3,0.308,0.036
ParamValues=104,628,1000,168,1000,500,304,0,500,496,104,460,1000,300,308,36
Enabled=1
Collapsed=1
ImageIndex=3
Name=Water 3

[AppSet4]
App=Terrain\GoopFlow
FParamValues=0.476,0.248,1,0.168,1,0.5,0.304,0,0.496,0.508,0.748,0.46,0.516,0.548,0.308,0.036
ParamValues=476,248,1000,168,1000,500,304,0,496,508,748,460,516,548,308,36
Enabled=1
Collapsed=1
ImageIndex=3
Name=Water 4

[AppSet5]
App=Terrain\GoopFlow
FParamValues=0.516,0.092,1,0.168,0.836,0.5,0.304,0,0.496,0.484,0.748,0.46,0.516,0.548,0.308,0.036
ParamValues=516,92,1000,168,836,500,304,0,496,484,748,460,516,548,308,36
Enabled=1
Collapsed=1
ImageIndex=3
Name=Water 5

[AppSet6]
App=Postprocess\Blooming
FParamValues=0,0,0,0.712,0.58,0.8,0.128,0.176,0
ParamValues=0,0,0,712,580,800,128,176,0
Enabled=1
Collapsed=1
Name=Taper Bloom

[AppSet7]
App=Postprocess\Youlean Motion Blur
FParamValues=0.5,0,0.872,0.568,0,0,0
ParamValues=500,0,872,568,0,0,0
ParamValuesPostprocess\Blur=1000
ParamValuesBackground\FourCornerGradient=667,1000,944,712,1000,638,748,1000,96,1000,1000,0,1000,1000
ParamValuesPostprocess\ColorCyclePalette=0,250,500,333,267,528,1000,1000,1000,250,168
ParamValuesPostprocess\ScanLines=0,214,1000,1000,0,272
Enabled=1
Collapsed=1
ImageIndex=3
Name=Reflector

[AppSet8]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.356,0,0.058,0.106,1,0,0,0
ParamValues=356,0,58,106,1000,0,0,0
ParamValuesPostprocess\Vignette=0,0,0,1000,676,596
ParamValuesPostprocess\RGB Shift=424,448,0,600,700,200
ParamValuesPostprocess\Point Cloud High=0,1000,482,500,512,448,496,495,570,657,184,0,376,0,667
ParamValuesPostprocess\ScanLines=400,400,0,0,1000,1000
ParamValuesPostprocess\Point Cloud Low=0,1000,482,500,500,448,496,503,570,473,156,0,0,0,0
Enabled=1
Collapsed=1
Name=Bloom

[AppSet9]
App=HUD\HUD Prefab
FParamValues=0,0.128,0.5,0,0,0.5,0.468,0.526,1,1,4,0,0.5,1,0.368,0.132,1,1
ParamValues=0,128,500,0,0,500,468,526,1000,1000,4,0,500,1,368,132,1000,1
ParamValuesBackground\ItsFullOfStars=0,756,1000,0,764,500,500,500,0,0,0
ParamValuesFeedback\70sKaleido=0,0,0,1000,1000,1000
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,468,364,500,500,588,0,0,536
ParamValuesFeedback\WormHoleEclipse=0,692,1000,0,1000,184,500,0,96,1000,140
ParamValuesHUD\HUD Grid=0,500,0,0,500,500,1000,1000,1000,444,500,1000,500,100,0,500,1000,112,1000,300,0,1000
ParamValuesFeedback\BoxedIn=0,0,620,1000,500,144,252,500,208,704
ParamValuesFeedback\FeedMe=0,0,588,1000,616,1000,340
Enabled=1
Collapsed=1
Name=Logo
LayerPrivateData=780173C8CBCF4BD52B2E4B671899000060F9036F

[AppSet10]
App=Postprocess\Youlean Color Correction
FParamValues=0.508,0.568,0.504,0.508,0.508,0.484
ParamValues=508,568,504,508,508,484
Enabled=1
UseBufferOutput=1
BufferRenderQuality=6
Collapsed=1
Name=Filter Color

[AppSet0]
App=Background\SolidColor
FParamValues=0,0.612,0.068,0.444
ParamValues=0,612,68,444
Enabled=1
Collapsed=1
Name=WORLD

[AppSet14]
App=Image effects\Image
FParamValues=0,0.612,0.068,0.356,1,0.5,0.588,0.484,0,0.02,0,0,1,0.772
ParamValues=0,612,68,356,1000,500,588,484,0,20,0,0,1,772
ParamValuesBackground\SolidColor=0,612,84,956
Enabled=1
Collapsed=1
ImageIndex=3
Name=Table

[AppSet16]
App=Image effects\Image
FParamValues=0,0,0,0,0.684,0.424,0.556,0,0,0,0,1,0,0
ParamValues=0,0,0,0,684,424,556,0,0,0,0,1,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=Object 1

[AppSet15]
App=Image effects\Image
FParamValues=0,0,0,0,0.914,0.534,0.514,0,0,0,0,1,0,0
ParamValues=0,0,0,0,914,534,514,0,0,0,0,1,0,0
ParamValuesHUD\HUD Prefab=265,420,500,0,1000,500,500,980,1000,1000,444,0,500,1000,368,0,1000,1000
ParamValuesPeak Effects\StereoWaveForm=0,0,0,1000,0,0,1000,1000,1000,1000,500,500,500,500,500,500,500,500,0,500,500,1000,12,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPeak Effects\PeekMe=0,0,1000,0,352,500,272,230,0,380,0,0
ParamValuesPhysics\Cage=888,672,1000,0,800,500,500,864,72,1000
ParamValuesBackground\SolidColor=0,612,764,924
ParamValuesPeak Effects\SplinePeaks=0,756,950,0,500,500,1000,0,0,0
ParamValuesPeak Effects\Polar=860,580,1000,0,176,500,500,1000,1000,288,620,1000,552,284,0,0,0,1000,1000,1000,304,356,0,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
Enabled=1
Collapsed=1
Name=Object 2

[AppSet13]
App=Image effects\Image
FParamValues=0.732,0,0,1,0.187,0.342,0.61,0,0,0,0,1,1,1
ParamValues=732,0,0,1000,187,342,610,0,0,0,0,1,1,1000
ParamValuesPostprocess\Youlean Motion Blur=500,0,872,568
ParamValuesPostprocess\Blur=1000
ParamValuesBackground\FourCornerGradient=667,1000,944,712,1000,638,748,1000,96,1000,1000,0,1000,1000
ParamValuesPostprocess\ColorCyclePalette=0,250,500,333,267,528,1000,1000,1000,250,168
ParamValuesPostprocess\ScanLines=0,214,1000,1000,0,272
Enabled=1
Collapsed=1
ImageIndex=3
Name=LCD Border 1

[AppSet18]
App=Image effects\Image
FParamValues=0.26,0,0,1,0.1,0.328,0.62,0,0,0,0,1,1,1
ParamValues=260,0,0,1000,100,328,620,0,0,0,0,1,1,1000
Enabled=1
Collapsed=1
ImageIndex=3
Name=LCD Border 2

[AppSet17]
App=Image effects\Image
FParamValues=0,0,0,0,0.036,0.317,0.627,0,0,0,0,1,0,1
ParamValues=0,0,0,0,36,317,627,0,0,0,0,1,0,1000
Enabled=1
Collapsed=1
ImageIndex=2
Name=LCD Out

[AppSet12]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fade in-out

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Images="[plugpath]Content\Bitmaps\Vector art\McIntosh 275.ilv","[plugpath]Content\Bitmaps\Vector art\Enhancer Marantz.ilv"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

