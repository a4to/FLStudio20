FLhd   0 * ` FLdt�-  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��Z@-  ﻿[General]
GlWindowMode=1
LayerCount=26
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=16,22,23,20,0,14,24,9,21,19,10,12,15,11,6,13,7,25,1,4,3,2,5,17,18,8
WizardParams=419,517,518,519,520,521

[AppSet16]
App=Terrain\GoopFlow
FParamValues=0,1,1,0,0,0.468,0.564,0.356,0.5,0,0.636,0.36,0.876,0.7,0.62,0.996
ParamValues=0,1000,1000,0,0,468,564,356,500,0,636,360,876,700,620,996
ParamValuesObject Arrays\Filaments=0,368,0,1000,704,500,500,500,376,236,72,456,32,420
ParamValuesScenes\RhodiumLiquidCarbon=0,41,1000,0,1000,148,500,912,772,1000
ParamValuesPostprocess\Youlean Bloom=0,524,486,174
Enabled=1
Collapsed=1
ImageIndex=7
Name=LOVE IS UP

[AppSet22]
App=Terrain\GoopFlow
FParamValues=0,1,1,0.668,0,1,0.564,0.156,0.428,0.304,0.592,0.424,1,1,0.436,0.868
ParamValues=0,1000,1000,668,0,1000,564,156,428,304,592,424,1000,1000,436,868
Enabled=1
Collapsed=1
ImageIndex=7
Name=LOVE IS LEFT

[AppSet23]
App=Terrain\GoopFlow
FParamValues=0,0,1,0,0,0.52,0.5,0.236,0.5,0.508,0.592,0.424,1,1,0.436,0.868
ParamValues=0,0,1000,0,0,520,500,236,500,508,592,424,1000,1000,436,868
ParamValuesCanvas effects\TaffyPulls=384,0,1000,780,0,500,500,300,660,436,280,0,0,164
ParamValuesHUD\HUD Graph Linear=0,0,0,1000,500,480,1000,240,1000,444,500,1000,0,1000,0,844,476,284,0,167,1000,1000
Enabled=1
Collapsed=1
ImageIndex=7
Name=LOVE IS DOWN

[AppSet20]
App=Terrain\GoopFlow
FParamValues=0,0,1,0,0,0.528,0.564,0.336,0.476,0.756,0.592,0.424,1,1,0.436,0.868
ParamValues=0,0,1000,0,0,528,564,336,476,756,592,424,1000,1000,436,868
Enabled=1
Collapsed=1
ImageIndex=7
Name=LOVE IS RIGHT

[AppSet0]
App=HUD\HUD Grid
AppVersion=1
FParamValues=0,0,0,1,0.5,0.5,1,1,0.5,4,0.5,1,0.5,0.816,1,1,0.86,0,0.7,0,1,1
ParamValues=0,0,0,1000,500,500,1000,1000,500,4,500,1000,500,816,1000,1000,860,0,700,0,1000,1
ParamValuesFeedback\WarpBack=0,0,0,24,500,356,0
ParamValuesBackground\ItsFullOfStars=0,0,0,0,732,500,500,500,0,0,0
ParamValuesFeedback\WormHoleDarkn=1000,0,0,24,500,684,884,500,316,936,500,500
ParamValuesBackground\SolidColor=0,0,0,0
ParamValuesFeedback\SphericalProjection=0,0,0,0,56,425,672,402,260,416,1000,500,500,1000,0,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesCanvas effects\Flaring=0,1000,1000,1000,1000,500,500,181,0,200,500
ParamValuesCanvas effects\Electric=0,0,1000,960,1000,500,500,68,100,1000,500
ParamValuesFeedback\FeedMeFract=0,0,940,516,896,0
ParamValuesCanvas effects\OverlySatisfying=0,740,484,1000,600,500,500,100,100,1000,500
Enabled=1
Collapsed=1
Name=DARK MOOD

[AppSet14]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.864,0.196,0.5,0.564,0.384,0,0.54
ParamValues=0,0,0,0,864,196,500,564,384,0,540
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,822,500,500,500,500,500
ParamValuesObject Arrays\Filaments=0,1000,1000,1000,800,500,500,500,376,172,0,0,0,420
ParamValuesObject Arrays\Rings=148,0,0,1000,32,500,500,500,500,0,0,0,344,452,412
ParamValuesHUD\HUD Grid=60,352,0,256,500,500,1000,1000,1000,444,500,1000,500,540,0,76,1000,232,732,0,0,1000
Enabled=1
Collapsed=1
ImageIndex=7
Name=STARS 2

[AppSet24]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.864,0.836,0.5,0.564,0.384,0,0.54
ParamValues=0,0,0,0,864,836,500,564,384,0,540
Enabled=1
Collapsed=1
ImageIndex=7
Name=STARS 2

[AppSet9]
App=Postprocess\Youlean Motion Blur
FParamValues=0.516,1,0.208,0.676,0,0,0
ParamValues=516,1,208,676,0,0,0
ParamValuesImage effects\ImageMasked=0,0,0,0,760,500,500,1000,200,1000,500
ParamValuesCanvas effects\Digital Brain=576,0,0,0,1000,500,500,600,100,300,100,250,1000,500,1000
ParamValuesObject Arrays\Filaments=0,1000,1000,0,552,500,500,0,392,268,900,476,0,432
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,1000,484,500,500,800,764,752
ParamValuesObject Arrays\CubicMatrix=0,468,1000,0,792,500,500,184,500,0,0,0,0
ParamValuesHUD\HUD Grid=0,0,0,1000,504,536,1000,1000,1000,444,500,1000,500,764,944,304,560,116,288,116,292,1000
ParamValuesImage effects\ImageSphinkter=0,833,0,1000,0,500,500,1000,500,608,500,416,704,0,0
ParamValuesHUD\HUD Meter Radial=0,500,0,0,0,0,750,0,500,500,740,242,1000,0,250,148,0,1000,500,1000
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,2,500,500,500,500,500
ParamValuesCanvas effects\TaffyPulls=1000,0,0,0,0,500,500,100,500,500,0,888,48,500
ParamValuesCanvas effects\OverlySatisfying=0,0,1000,940,600,500,500,100,100,1000,500
ParamValuesMisc\PentUp=0,360,1000,1000,500,500,500,500,876,0,0,0
Enabled=1
Collapsed=1
ImageIndex=7
Name=WoW FX 1

[AppSet21]
App=Feedback\WormHoleDarkn
FParamValues=0.74,0,0.212,0.468,1,0,0.156,0.936,0.708,0.628,0.5,0.5
ParamValues=740,0,212,468,1000,0,156,936,708,628,500,500
ParamValuesFeedback\FeedMe=0,0,376,1000,712,1000,496
ParamValuesFeedback\FeedMeFract=0,0,0,488,808,0
ParamValuesPostprocess\Blooming=0,0,0,408,252,432,0,768,0
ParamValuesFeedback\WormHoleEclipse=0,0,0,692,752,152,500,500,708,244,628
ParamValuesPostprocess\Point Cloud High=0,582,390,500,512,496,504,503,330,625,156,0,0,0,0
ParamValuesPostprocess\ColorCyclePalette=1000,0,0,0,0,384,0,500,0,0,432
Enabled=1
Collapsed=1
ImageIndex=7
Name=WoW FX 2

[AppSet19]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.396,0.388,0.754,0.182,1,0,0,0
ParamValues=396,388,754,182,1000,0,0,0
ParamValuesFeedback\WarpBack=500,0,0,0,0,0,88
ParamValuesParticles\ColorBlobs=0,0,0,1000,0,500,500,350,1000,148
ParamValuesScenes\Cloud Ten=1000,0,0,0,0,0,1000,0,0,0,0
ParamValuesHUD\HUD Grid=0,500,0,1000,500,500,1000,1000,1000,444,500,1000,500,0,288,1000,1000,596,500,720,0,1000
ParamValuesCanvas effects\Flaring=84,1000,1000,1000,1000,500,500,181,0,400,500
ParamValuesBackground\Grid=250,0,996,1000,224,224,1000,0,0,0
ParamValuesPostprocess\Youlean Color Correction=500,44,500,500,284,500
ParamValuesPostprocess\Youlean Motion Blur=1000,1000,336,636
ParamValuesFeedback\FeedMe=160,0,220,244,736,1000,640
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=WoW FX 3

[AppSet10]
App=HUD\HUD Prefab
FParamValues=0,0,0.5,0,0.172,0.502,0.648,0.318,1,1,4,0,0.5,1,0.368,0.1,1,1
ParamValues=0,0,500,0,172,502,648,318,1000,1000,4,0,500,1,368,100,1000,1
ParamValuesHUD\HUD Graph Radial=0,500,0,0,200,500,250,444,500,0,1000,0,1000,0,250,500,200,0,0,0,500,1000
ParamValuesHUD\HUD Meter Radial=0,248,1000,0,0,0,0,992,492,708,156,34,1000,0,374,0,1000,1000,500,1000
ParamValuesHUD\HUD Free Line=0,500,0,0,656,552,168,304,182,92,182,168,1000,312,0,280,500
ParamValuesHUD\HUD Graph Linear=0,500,0,0,816,468,1000,1000,250,444,500,1000,212,1000,0,500,200,0,0,0,500,1000
ParamValuesHUD\HUD Meter Linear=0,500,0,0,0,0,750,0,800,664,300,100,0,125,500,1,238,0,0,1000,612,1000
Enabled=1
UseBufferOutput=1
Name=LOGO
LayerPrivateData=780173C8CBCF4BD52B2E4B671899000060F9036F

[AppSet12]
App=Background\SolidColor
FParamValues=0,0.524,0,0.748
ParamValues=0,524,0,748
Enabled=1
Collapsed=1
Name=Background Main

[AppSet15]
App=HUD\HUD Prefab
FParamValues=3,0,0.5,0.568,0,0.5,0.5,0.956,1,1,4,0,0.5,1,0.368,0,1,1
ParamValues=3,0,500,568,0,500,500,956,1000,1000,4,0,500,1,368,0,1000,1
Enabled=1
Collapsed=1
Name=Border BG
LayerPrivateData=78014B4A4CCE4E2FCA2FCD4B89490232750D0C8CF53273CA18460A0000F2280847

[AppSet11]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0.044,0.004,1,0,0.5,0.221,1,0.556,1,4,0.5,1,0.34,0.524,0,0.624,0.076,0.32,0,1,0,1
ParamValues=44,4,1000,0,500,221,1000,556,1000,4,500,1,340,524,0,624,76,320,0,1,0,1
ParamValuesHUD\HUD Free Line=0,500,0,0,656,552,168,304,182,92,182,168,1000,312,0,280,500
Enabled=1
Collapsed=1
Name=EQ UP

[AppSet6]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0.044,0,1,0,0.5,0.777,1,0.556,1,4,1,1,0.34,0.524,0,0.564,0.076,0.32,0,1,0,1
ParamValues=44,0,1000,0,500,777,1000,556,1000,4,1000,1,340,524,0,564,76,320,0,1,0,1
ParamValuesHUD\HUD Text=60,500,0,1000,660,686,500,552,1000,1000,324,1,240,0,0,750,1000,536,504,1000
Enabled=1
Collapsed=1
Name=EQ DOWN

[AppSet13]
App=Image effects\Image
FParamValues=0,0,0,0,0.711,0.5,0.501,0.042,0,0.25,0,0,0,0
ParamValues=0,0,0,0,711,500,501,42,0,250,0,0,0,0
ParamValuesImage effects\ImageWall=0,0,0,0,0,0,0
ParamValuesHUD\HUD Image=0,0,500,500,1000,1000,500,444,500,0,0,1000,1000,500,1000
ParamValuesBackground\SolidColor=0,892,1000,124
ParamValuesImage effects\ImageSlices=0,0,0,0,500,496,488,0,0,0,500,269,1000,500,0,0,0
Enabled=1
Collapsed=1
ImageIndex=6
Name=LCD - Mask

[AppSet7]
App=Text\TextTrueType
FParamValues=0.08,0,0,0,0,0.494,0.5,0,0,0,0.5
ParamValues=80,0,0,0,0,494,500,0,0,0,500
Enabled=1
Name=Main Text

[AppSet25]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=5

[AppSet1]
App=Image effects\Image
FParamValues=0,0,0.612,0,0.947,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,612,0,947,500,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=2
Name=iPhone - Buttons

[AppSet4]
App=Image effects\Image
FParamValues=0.672,0,0.136,0,0.952,0.501,0.5,0,0.004,0,0,0,0,0
ParamValues=672,0,136,0,952,501,500,0,4,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=4
Name=iPhone - Glass

[AppSet3]
App=Image effects\Image
FParamValues=0,0,0,0.856,0.972,0.5,0.514,0,0,0,0,0,0,0
ParamValues=0,0,0,856,972,500,514,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=iPhone - Border 2

[AppSet2]
App=Image effects\Image
FParamValues=0,0.492,0.56,0.592,0.947,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,492,560,592,947,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
Name=iPhone - Border 1

[AppSet5]
App=Image effects\Image
FParamValues=0,0,0,0,0.95,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,950,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=3
Name=iPhone - FaceID

[AppSet17]
App=Background\FourCornerGradient
FParamValues=7,1,0.324,0.696,0.744,0.296,0.576,1,0.224,1,0.188,0.14,1,1
ParamValues=7,1000,324,696,744,296,576,1000,224,1000,188,140,1000,1000
ParamValuesHUD\HUD Meter Linear=0,208,884,1000,0,0,0,824,128,753,224,68,0,11,500,0,584,560,404,364,568,1000
Enabled=1
Collapsed=1
Name=Filter Color

[AppSet18]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fade-in and out

[AppSet8]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text=Author,"Song Title"
Html="<position x=""3"" y=""2""><p align=""left""><font face=""American-Captain"" size=""8"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""3"" y=""10""><p align=""left""><b><font face=""Chosence-Bold"" size=""6"" color=""#FFFFFF"">[title]</font></b></p></position>","<position y=""92""><p align=""right""><b><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[comment]</font></b></p></position>",," ",,," ",,,
Images=[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Border-1.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Border-2.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Buttons.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Face-id.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Glass.svg
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

