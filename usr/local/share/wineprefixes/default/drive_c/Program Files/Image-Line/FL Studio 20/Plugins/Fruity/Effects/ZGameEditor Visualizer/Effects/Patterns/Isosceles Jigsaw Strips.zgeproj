<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" FileVersion="2">
  <OnLoaded>
    <ZLibrary Comment="HSV Library">
      <Source>
<![CDATA[vec3 hsv(float h, float s, float v)
{
  s = clamp(s/100, 0, 1);
  v = clamp(v/100, 0, 1);

  if(!s)return vector3(v, v, v);

  h = h < 0 ? frac(1-abs(frac(h/360)))*6 : frac(h/360)*6;

  float c, f, p, q, t;

  c = floor(h);
  f = h-c;

  p = v*(1-s);
  q = v*(1-s*f);
  t = v*(1-s*(1-f));

  switch(c)
  {
    case 0: return vector3(v, t, p);
    case 1: return vector3(q, v, p);
    case 2: return vector3(p, v, t);
    case 3: return vector3(p, q, v);
    case 4: return vector3(t, p, v);
    case 5: return vector3(v, p, q);
  }
}]]>
      </Source>
    </ZLibrary>
  </OnLoaded>
  <OnUpdate>
    <ZExpression>
      <Expression>
<![CDATA[uResolution=vector2(app.ViewportWidth,app.ViewportHeight);
uViewport=vector2(app.ViewportX,app.ViewportY);
uAlpha =1.0-Parameters[0];

float speed=(Parameters[4]-0.5)*4.0;
float delta=app.DeltaTime*Speed; //comment to use other time options
uTime+=delta;

vec3 col = hsv(Parameters[1]*360,Parameters[2]*100,(1-Parameters[3])*100);
uColor = vector3(col[0],col[1],col[2]);]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <OnRender>
    <UseMaterial Material="mCanvas"/>
    <RenderSprite/>
  </OnRender>
  <Content>
    <Shader Name="MainShader">
      <VertexShaderSource>
<![CDATA[#version 120

void main(){
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[#version 120

uniform vec2 iResolution,iViewport;
uniform float iTime;

// Implement this
uniform float iAlpha; // Replace at fragColor
uniform vec3 iColor; // Replace iChannel0

float fMod(float x, float y)
{
  return x - floor(x/y) * y;
}

int iMod(int a, int b)
{
  int tmp = a/b;
  return a - (b*tmp);
}

/*

	Isosceles Jigsaw Strips
	-----------------------

	After looking at Fizzer's really cool and clever diamond grid based jigsaw
	pattern (the link's below), I made a comment regarding some hexagon and
	triangle jigsaw patterns I'd been meaning to post, then forgot about it.
	Anyway, it's been a while, but better late than never.

	Triangle jigsaw patterns aren't common. I haven't seen code for one, but I'm
	sure it's out there, and you can find pictures in equilateral form. Just to
    be different, I figured I'd put together an isosceles version. To mix things
	up some more, I decided to give the resultant pattern some pseudo depth by
    taking the nodules off one side of the individual triangle pieces, then
    applying an edge-shadow trick I've seen in various triangle grid examples,
    which gives it that weird sense of layered depth.

    By the way, there are a couple of defines below for anyone wants to see a
    regular triangle jigsaw pattern with connecting nodules on all three sides.

	The jigsaw logic itself wasn't that difficult, but coding the isosceles grid
	was a little frustrating, since I decided to code it from scratch. There's a
	common way to code up a basic equilateral triangle grid that involves scaling
    and skewing. However, I wanted to take a more direct approach that would
    bypass the skewing, eliminate grid rotation, and allow for any isosceles
    triangle grid to be rendered by simply changing the scaling coordinates.

    Later, I'll put up a simpler example that makes use of the isosceles grid,
    but for now, this is just a demonstration to show that it works. At some
    stage, I'd like to put together a 3D version.

	By the way, if you've never coded a jigsaw example before, it'd probably be
	better to start with a simple square jigsaw.



    Other Examples:

    // A regular square jigsaw pattern, but taking a clever diamond grid
    // approach to avoid neighbor calculations. I wish I'd thought of it. :)
    // On a side note, I was able to run with Fizzer's original geometric idea
    // and cut down on things even more, so I might put up the results later.
    Jigsaw Pattern - Fizzer
    https://www.shadertoy.com/view/3tlXR4


    // A jigsaw pattern on the 3D plane. Frustrating to make. :D
    Jigsaw - Shane
	https://www.shadertoy.com/view/XdGBDW

*/


// If you'd like to see a regular 3-sided triangle jigsaw pattern, then
// uncomment this. It probably looks more natural with the equilateral
// triangle setting turned on below.
//#define STANDARD_JIGSAW

// A regular equilateral scaling, which is just a special form of isosceles.
//#define EQUILATERAL



// Standard 2D rotation formula.
mat2 rot2(in float a){ float c = cos(a), s = sin(a); return mat2(c, -s, s, c); }


// IQ's vec2 to float hash.
float hash21(vec2 p){  return fract(sin(dot(p, vec2(27.619, 57.583)))*43758.5453); }


// IQ's signed distance to a 2D triangle.
float sdTri(in vec2 p, in vec2 p0, in vec2 p1, in vec2 p2){

	vec2 e0 = p1 - p0, e1 = p2 - p1, e2 = p0 - p2;

	vec2 v0 = p - p0, v1 = p - p1, v2 = p - p2;

	vec2 pq0 = v0 - e0*clamp( dot(v0, e0)/dot(e0, e0), 0., 1.);
	vec2 pq1 = v1 - e1*clamp( dot(v1, e1)/dot(e1, e1), 0., 1.);
	vec2 pq2 = v2 - e2*clamp( dot(v2, e2)/dot(e2, e2), 0., 1.);

    float s = sign( e0.x*e2.y - e0.y*e2.x);
    vec2 d = min( min( vec2(dot(pq0, pq0), s*(v0.x*e0.y - v0.y*e0.x)),
                       vec2(dot(pq1, pq1), s*(v1.x*e1.y - v1.y*e1.x))),
                       vec2(dot(pq2, pq2), s*(v2.x*e2.y - v2.y*e2.x)));

	return -sqrt(d.x)*sign(d.y);
}



// Triangle's incenter: The center of the inscribed circle, which in essence is
// the largest circle that you can fit into a triangle.
vec2 inCent(vec2 a, vec2 b, vec2 c){

    // Side lengths.
    float bc = length(b - c), ac = length(a - c), ab = length(a - b);
    return (bc*a + ac*b + ab*c)/(bc + ac + ab);
}

// The scaling vector. Basically, it determines the height to width ratio.
//
#ifdef EQUILATERAL
// An equilateral scaling, which is just a special kind of isosceles.
const vec2 s = vec2(1./.8660254, 1);
#else
// The default scale for this example. There's no reason behind it. I just liked
// the way it made things look.
const vec2 s = vec2(1, 1./.8660254);
#endif
// Similar size equilateral. Just a larger version of vec2(1./.8660254, 1).
//const vec2 s = vec2(4./3., 1./.8660254);
//
// One to one scaling -- which would effectively make the scaling redundant.
//const vec2 s = vec2(1);

vec3 triJigsaw(vec2 p){


    // The relative vertice positions. You could hardcode these into the formulae
    // below, but if you're performing various edge arithmetic, etc, they're handy
    // to keep around.
    const vec2 v0 = vec2(-.5), v1 = vec2(0, .5), v2 = vec2(.5, -.5);

    // Scaling the cordinates down, which makes them easier to work with. You scale
    // them back up, after the calculations are done.
    p /= s;

    // Triangles pack a grid nicely, but unfortunately, if you want vertices to
    // match up, each alternate row needs to be shifted along by half the base
    // width. The following vector will help effect that.
    float ys = mod(floor(p.y), 2.)*.5;
    vec4 ipY = vec4(ys, 0, ys + .5, 0);

    // Two triangles pack into each square cell, and each triangle uses the bottom
    // left point as it's unique identifier. The two points are stored here.
    vec4 ip4 = floor(p.xyxy + ipY) - ipY + .5;

    // The local coordinates of the two triangles are stored here.
    vec4 p4 = fract(p.xyxy - ipY) - .5;

    // Which isoso... I always struggle to spell it... isosceles triangle
    // are we in? Right way up, or upside down. By the way, if you're wondering
    // where the following arises from, "abs(x) + abs(y) - c" partitions
    // a square and "abs(x) + y - c" partitions a triangle.
    float itri = (abs(p4.x)*2. + p4.y<.5)? 1. : -1.;

    // Depending on which triangle we're in, return a vector containing the local
    // coordinates in the first two spots, and the unique position-based identifying
    // number in the latter two spots. These two positions would be all you'd need
    // to render a colored triangle grid. However, when combined with the triangle
    // orientation and vertices (above), you can render more interesting things.
    p4 = itri>0.? vec4(p4.xy*s, ip4.xy) : vec4(p4.zw*s, ip4.zw);

    // Making a copy of the triangle's local coordinates and ID. It's not particularly
    // necessary, but saves a bit of extra writing and confusion.
    p = p4.xy; // Local coordinates.
    vec2 ip = p4.zw; // Triangle ID.


    // The unscaled triangle vertices, which double as an ID: Note that the vertices of
    // alternate triangles are flipped in a such a way that edge neighboring edge vertices
    // match up.
    vec2[3] vID = vec2[3](v0*itri, v1*itri, v2*itri);

    // Edge IDs, based on the vertex IDs above. The mid points of neighboring triangles
    // occur in the same position, which means both triangles will generate the same
    // unique random number at that position. This is handy for all kinds of things,
    // including determining where and how to place a jigsaw nodule.
    vec2[3] eID = vec2[3](mix(vID[0], vID[1], .5), mix(vID[1], vID[2], .5), mix(vID[2], vID[0], .5));


    // Using the triangle vertices to calculate the incenter. If we were simply
    // rendering a triangle with the vertices, we wouldn't need this, but we're
    // going to perform some neighboring triangle comparisons.
    vec2 cnID = inCent(vID[0], vID[1], vID[2]);
    // This will work also, but with elongated triangles, and so on, the center won't look
    // very centralized. For an equilateral triangle, however, it will work perfectly.
    //vec2 cnID = (vID[0] + vID[1] + vID[2])/3.;



    // Scaled vertices -- It's not absolutely necessary to have these, but when doing more
    // complicated things, I like to swith between the unscaled IDs and the physical scaled
    // vertices themselves.
    vec2[3] vV = vec2[3](vID[0]*s, vID[1]*s, vID[2]*s);

    // Scaled mid-edge vertices.
    vec2[3] vE = vec2[3](eID[0]*s, eID[1]*s, eID[2]*s);


    // Producing the triangle, then rounding the edges slightly, for aesthetic purposes.
    //
    // The folowing line moves the vertices in a bit. Normzlizing the vectors is a choice.
    // I think you need to leave them unnormalized for border width consistancy, but I'm
    // not positive. Either way, it doesn't effect things too much.
    vec2 vn0 = vID[0] - cnID, vn1 = vID[1] - cnID, vn2 = vID[2] - cnID;//*s*s
    //
    // The triangle is rendered smaller than needed, then a scalar is added to the
    // final jigsaw piece (tri -= a;) prior to returning the values (See below).
    float tri = sdTri(p, vV[0] - vn0*.2, vV[1] - vn1*.2, vV[2] - vn2*.2);

    //tri = max(tri, -(length(p - cnID*s) - .055)); // Displaying the scaled incenter.
    //tri = max(tri, -(tri+ .27)); // Carving out the center.

    // Constructing the little jigsaw nodules, or whatever they're called. :)
    //
    // Neighboring triangles share boundaries from lines crossing from
    // incircle to incircle. With an equilateral triangle, the "cn" value
    // is zero, but for non-equilateral triangles -- like the isosceles,
    // the extra step is necessary... Well, I'm pretty sure it is, but I'd
    // double check. :)
    float rndC = hash21(ip - cnID);


    for(int i = 0; i<3; i++){

        #ifndef STANDARD_JIGSAW
        if(i == 1) continue;
        #endif

        //vec2 n = normalize(eID[i] - cnID)*.04;
        vec2 n = normalize(eID[i])*.04;

        // The normal running parallel to this specific edge.
        vec2 n2 = normalize(vID[i] - vID[iMod((i + 1), 3)]);

         // Triangles on either side of an edge have the mid point of the shared
        // edge in common. You can use this point to generate random values common
        // to each triangle. In this case, it will be used to offset the jigsaw
        // nodule by the normal running parallel to the edge.
        float rndI2 = (hash21(ip + eID[i] + .1) - .5)*.25;

        // The random value of the edge's neighboring triangle, which will be
        // compared to the value of the current triangle. In this case, if the
        // current triangle has a random value less than that of the triangle
        // on its neighboring edge, carve out a nodule, and update the ID. If
        // it's not less, then render a prodruding nodule.
        float rndI = hash21(ip + eID[i]*2. + cnID);

        // If the current random value is less than the edge neighbor, carve out
        // a circular gap, then add a smaller circle to represent the prodruding
        // nodule on the neighboring edge triangle.
        if(rndC<rndI){

            float nodule = length(p - vE[i] + (n - n2*rndI2)) - .09;

            // Carve out a la
            tri = max(tri, -(nodule - .12));

            //
            if(nodule<tri){
                ip += eID[i]*2.;
                // The neighboring triangle has an incircle in the oppposite
                // direction. I'd like to say I figured this obvious fact out
                // immediately, but that's never how it goes. :)
                cnID = -cnID;

                tri = min(tri, nodule);
            }

        }
        else {

            // If the current random value for the triangle is greater than
            // that of the triangle on its neighboring edge, add a circular
            // nodule. If you comment this out, you'll see that it doesn't
            // do much, but is still necessary. With larger edge gaps, it'd
            // be more visible. With no edge gaps, it wouldn't be.
            float nodule = length(p - vE[i] - (n - n2*rndI2)) - .09;
            tri = min(tri, nodule);

            // We should probably update the ID on the other side, but it
			// doesn't effect things, in this case, so I've left it out.
            //if(nodule<tri) {
                //ip += eID[i]*2.;
                //cnID = -cnID;
            //}

        }
    }


    // This gives the triangular jigsaw piece a bit of roundness. If you took
    // it away for a sharp look, you'd have to retweak the triangle and nodule
    // sizes a bit.
    tri -= .053;

    // Return the distance field value, and the cell ID. Note that the cell ID
    // is based on position, which means you could theoretically map it to
    // height map, etc. Multiplying the value by the scaling vector will keep
    // the aspect ratio intact.
    return vec3(tri, (ip + cnID)*s);
}

vec3 fPalette(float i){


    vec3 c = pow(min(vec3(1.4, 1, 1)*i, 1.), vec3(1, 2, 8));

    return mix(c, c.xzy, (1. - i)*.7);

}


void mainImage(out vec4 fragColor, in vec2 fragCoord){


    // Restricting the fullscreen resolution.
    float iRes = iResolution.y;//min(iResolution.y, 800.);
    // Aspect correct screen coordinates.
	vec2 uv = (fragCoord - iResolution.xy*.5)/iRes;


    // Scaling and translation.
    float gSc = 7.;
    // Depending on perspective; Moving the scene toward the bottom left,
    // or the camera in the north east (top right) direction.
    vec2 p = (uv*gSc - vec2(-1, -.5)*iTime);


    // The smoothing factor.
    float sf = 1./iRes*gSc;


    // The isosceles jigsaw grid object.
    vec3 d = triJigsaw(p);
    // Taking an extra sample for highlighting purpuses.
    vec3 dh = triJigsaw(p - vec2(-.02, .02)*s);


    #ifndef STANDARD_JIGSAW
    // Applying some simple trigonometry to produce a repeat rotated shadow.
    //
    // The shadow depends upon the scaling vector to produce the triangles.
    // If you look at the hight and half width of the isosceles, you'll see
    // where the following comes from.
	float a = atan(s.x/s.y/2.);
    // The base width of the triangle.
    float w = cos(a)*s.x;
    // Rotating by the base angle.
    vec2 q = rot2(a)*p;
    // Repeating the shadow object once every triangular base width.
    q.x = abs(mod(q.x, w) - w);
    // Giving the shadow a bit of length.
    float sh = q.x - .02*w;
    #endif


    // Setting the scene background to black.
    vec3 col = vec3(0);

    // Creating the textured overlay:
    //
    // Reading in the texture using the global coordinates, roughly converting
    // from sRGB to linear (tx *= tx), then tweaking the result to suit.
    vec3 tx =iColor; tx *= tx;
    tx = smoothstep(-.05, .35, tx); // Fading and lightening a bit.
    // Giving each triangle and indidual texture color based on the unique
    // positional ID. This gives things a little extra visual interest.
    vec3 txTri = iColor;  txTri *= txTri;
    txTri = smoothstep(0., .5, txTri);

    // Mixing the two textures above to give the individual triangle color.
    vec3 tCol = min(txTri*tx, 1.);

    // Using the ID do dermine which triangles are upside down. Sometimes, you'd
    // need to do this, so this is here to show that all that you need from a
    // triangle grid function is the local coordinates and positional ID.
    //if(fract(d.z/s.y)>.57) tCol *= vec3(3, 2, 1);

    // Using the distance values from the two field samples above to produce two
    // shade values. Their difference produces a pseudo hightlight. Essentially,
    // it's directional derivative based bump mapping.
    //
    float b = max(clamp(d.x/.1 + .5, 0., 1.) - clamp(dh.x/.1 + .5, 0., 1.), 0.)/(.03);

    // Adding the bumped highlight to the triangle color. There are other ways
    // to add layer highlights, but this will do.
    tCol = min(tCol*(.9 + b*b*.02), 1.);

    // Adding the dark edge: Since the background is black, it's not necessary,
    // but for other background colors, you'd need it.
    //col = mix(col, vec3(0), (1. - smoothstep(0., sf, d.x)));
    // Adding the colored triangle layer (slightly edged in) to the top.
    col = mix(col, tCol, (1. - smoothstep(0., sf, d.x + .02)));


    #ifndef STANDARD_JIGSAW
    col = mix(col, vec3(0), (1. - smoothstep(0., sf*16.*iRes/450., sh))*.7);
    #endif

    // Applying a subtle silhouette, for art's sake.
	uv = fragCoord/iResolution.xy;
    col *= pow(16.*(1. - uv.x)*(1. - uv.y)*uv.x*uv.y, 1./16.)*1.05;

    // Rough gamma correction, then output to the screen.
    fragColor = vec4(sqrt(max(col, 0.)), iAlpha);

}



void main(){
    //ZGE does not use mainImage( out vec4 fragColor, in vec2 fragCoord )
    //Rededefined fragCoord as gl_FragCoord.xy-iViewport(dynamic window)
    mainImage(gl_FragColor,gl_FragCoord.xy-iViewport);
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="iResolution" VariableRef="uResolution"/>
        <ShaderVariable VariableName="iViewport" VariableRef="uViewport"/>
        <ShaderVariable VariableName="iTime" VariableRef="uTime"/>
        <ShaderVariable VariableName="iAlpha" VariableRef="uAlpha"/>
        <ShaderVariable VariableName="iColor" VariableRef="uColor"/>
      </UniformVariables>
    </Shader>
    <Group Comment="Default ShaderToy Uniform Variable Inputs">
      <Children>
        <Variable Name="uResolution" Type="6"/>
        <Variable Name="uTime"/>
        <Variable Name="uViewport" Type="6"/>
      </Children>
    </Group>
    <Group Comment="FL Studio Variables">
      <Children>
        <Array Name="Parameters" SizeDim1="5" Persistent="255">
          <Values>
<![CDATA[789C63604001F6B3664ADA030005F301CB]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Alpha
Hue
Saturation
Lightness
Speed]]>
          </StringValue>
        </Constant>
        <Constant Name="AuthorInfo" Type="2">
          <StringValue>
<![CDATA[Shane (converted by Youlean & StevenM)
https://www.shadertoy.com/view/3sd3Rj
]]>
          </StringValue>
        </Constant>
      </Children>
    </Group>
    <Group Comment="Unique Uniform Variable Inputs">
      <Children>
        <Variable Name="uAlpha"/>
        <Variable Name="uColor" Type="7"/>
      </Children>
    </Group>
    <Group Comment="Materials and Textures">
      <Children>
        <Material Name="mCanvas" Blend="1" Shader="MainShader"/>
      </Children>
    </Group>
  </Content>
</ZApplication>
