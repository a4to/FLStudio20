FLhd   0 * ` FLdt�;  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��vP;  ﻿[General]
GlWindowMode=1
LayerCount=55
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=1,3,7,5,27,39,9,28,8,10,16,6,15,30,31,51,0,2,4,12,17,18,19,20,21,22,23,24,25,26,32,34,35,36,33,14,38,54,61,55,59,60,56,50,49,40,41,43,44,58,42,45,48,52,53
WizardParams=2579,2580

[AppSet1]
App=Image effects\ImageBox
FParamValues=0,0.572,0.536,0.712,0.44,0.504,0.5,0.5,0.5,0.5,0,0,0,0,0,0,0.5,0.5,0.5
ParamValues=0,572,536,712,440,504,500,500,500,500,0,0,0,0,0,0,500,500,500
Enabled=1
UseBufferOutput=1
ImageIndex=9
MeshIndex=1

[AppSet3]
App=Image effects\ImageBox
FParamValues=0,0.056,0.784,0.384,0.44,0.504,0.5,0.5,0.5,0.5,0,0,0,0,0,0,0.5,0.5,0.5
ParamValues=0,56,784,384,440,504,500,500,500,500,0,0,0,0,0,0,500,500,500
Enabled=1
UseBufferOutput=1
ImageIndex=9
MeshIndex=1

[AppSet7]
App=HUD\HUD Mesh
FParamValues=0,0.488,0.464,1,0.309,0.195,0.455,3,0,0.15,0.5,0.5,0.5,0.75,0.5,0
ParamValues=0,488,464,1000,309,195,455,3,0,150,500,500,500,750,500,0
Enabled=1

[AppSet5]
App=HUD\HUD Mesh
FParamValues=0,0.488,0.468,0.82,0.309,0.195,0.455,1,0,0.15,0.5,0.784,0.5,0.75,0.5,0
ParamValues=0,488,468,820,309,195,455,1,0,150,500,784,500,750,500,0
Enabled=1

[AppSet27]
App=HUD\HUD Prefab
FParamValues=25,0.572,0.5,0,0,0.363,0.747,0.074,1,1,4,0,0.9889,1,0.512,0,1,0
ParamValues=25,572,500,0,0,363,747,74,1000,1000,4,0,988,1,512,0,1000,0
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C2CF43273CA18863D00000F220AA3

[AppSet39]
App=HUD\HUD Prefab
FParamValues=25,0,0.5,0,0,0.363,0.747,0.046,1,1,4,0,0.9778,1,0.512,0,1,0
ParamValues=25,0,500,0,0,363,747,46,1000,1000,4,0,977,1,512,0,1000,0
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C2CF43273CA18863D00000F220AA3

[AppSet9]
App=Misc\Automator
FParamValues=1,9,8,2,0.2,0.486,0.565,1,15,6,2,0.466,0.15,0.498,1,16,7,2,0.277,0.27,0.467,0,28,13,3,1,0.074,0.25
ParamValues=1,9,8,2,200,486,565,1,15,6,2,466,150,498,1,16,7,2,277,270,467,0,28,13,3,1000,74,250
Enabled=1

[AppSet28]
App=Postprocess\ParameterShake
FParamValues=1,0,27,12,1,0,39,12,3,3,0,0,4,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=1000,0,27,12,1000,0,39,12,3,3,0,0,4,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet8]
App=Postprocess\Blooming
FParamValues=0,0,0,1,0.5,0.736,1,0.1673,0
ParamValues=0,0,0,1000,500,736,1000,167,0
Enabled=1
UseBufferOutput=1

[AppSet10]
App=Image effects\Image
FParamValues=0.6,0,0,1,1,0.48,0.485,0,0,0,0,0,0,0
ParamValues=600,0,0,1000,1000,480,485,0,0,0,0,0,0,0
Enabled=1
ImageIndex=5

[AppSet16]
App=Image effects\Image
FParamValues=0.371,0,0,1,0.776,0.459,0.638,0.398,0,0,0,0,0,0
ParamValues=371,0,0,1000,776,459,638,398,0,0,0,0,0,0
Enabled=1
ImageIndex=5

[AppSet6]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
UseBufferOutput=1
ImageIndex=5

[AppSet15]
App=HUD\HUD Free Line
AppVersion=1
FParamValues=0,0.5,0,0,0.5,0.5,0.2163,0.1,0,0.1,0,0.1,0,0,0,0.118,0.5
ParamValues=0,500,0,0,500,500,216,100,0,100,0,100,0,0,0,118,500
ParamValuesHUD\HUD Prefab=94,0,500,0,1000,500,148,1000,396,1000,444,0,500,1000,368,260,720,1000
Enabled=1
UseBufferOutput=1
LayerPrivateData=780163606060F8B3F2A32D90B2676068D80FA4A160940D0988D17020271C0016BE4022

[AppSet30]
App=HUD\HUD Free Line
AppVersion=1
FParamValues=0,0,1,0,0.5,1,1,0.1,0,0.1,0,0.1,1,0,0,1,0.25
ParamValues=0,0,1000,0,500,1000,1000,100,0,100,0,100,1,0,0,1000,250
Enabled=1
LayerPrivateData=780163600081067B20B603E2FD602E446C943D1A0E64A70100662A3E8D

[AppSet31]
App=Postprocess\Blooming
FParamValues=0,0,0,1,0.5,1,1,0.876,0
ParamValues=0,0,0,1000,500,1000,1000,876,0
Enabled=1
UseBufferOutput=1

[AppSet51]
App=HUD\HUD Meter Radial
AppVersion=1
FParamValues=0.396,0.04,0,0.804,0.12,0.04,0.75,0,0.712,0.098,0.118,0.305,40,0.25,1,0,0.1738,0,0.5,1
ParamValues=396,40,0,804,120,40,750,0,712,98,118,305,40,250,1,0,173,0,500,1
Enabled=1
UseBufferOutput=1
LayerPrivateData=7801F32C49CCC94CCCCBCBD70D4A4D2FCD492CD22B29496318390000C9F2081B

[AppSet0]
App=Background\SolidColor
FParamValues=0,0.14,0.116,0
ParamValues=0,140,116,0
Enabled=1

[AppSet2]
App=Image effects\Image
FParamValues=0,0,0,0,0.484,0.5,1,1,0.476,0,0,0,0,0
ParamValues=0,0,0,0,484,500,1000,1000,476,0,0,0,0,0
Enabled=1
ImageIndex=2

[AppSet4]
App=Image effects\Image
FParamValues=0,0,0,0,0.484,0.5,0.483,1,0.224,0,0,0,0,0
ParamValues=0,0,0,0,484,500,483,1000,224,0,0,0,0,0
Enabled=1
ImageIndex=3

[AppSet12]
App=Postprocess\ParameterShake
FParamValues=1,0,11,4,1,0.548,13,4,4,4,0,0,6,6,0,1,0,0,0,0,0,0,0,0,0,0
ParamValues=1000,0,11,4,1000,548,13,4,4,4,0,0,6,6,0,1,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet17]
App=HUD\HUD 3D
FParamValues=0.276,0,0.242,0.409,0.5,0.412,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=276,0,242,409,500,412,500,500,500,500,500,500,500,1000,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
ParamValuesHUD\HUD Image=0,0,500,316,1000,1000,1000,444,500,0,0,1000,1000,500,1000
ParamValuesImage effects\Image=0,0,0,0,1000,500,500,0,0,0,0,1,0,0
Enabled=1
ImageIndex=6

[AppSet18]
App=HUD\HUD 3D
FParamValues=0.276,0,0.838,0.43,0.5,0.412,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=276,0,838,430,500,412,500,500,500,500,500,500,500,1000,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
ImageIndex=6

[AppSet19]
App=HUD\HUD 3D
FParamValues=0.276,0,0.931,0.442,0.5,0.412,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=276,0,931,442,500,412,500,500,500,500,500,500,500,1000,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
ImageIndex=6

[AppSet20]
App=HUD\HUD 3D
FParamValues=0.276,0,0.922,0.461,0.5,0.412,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=276,0,922,461,500,412,500,500,500,500,500,500,500,1000,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
ImageIndex=6

[AppSet21]
App=HUD\HUD 3D
FParamValues=0.276,0,0.694,0.461,0.5,0.412,0.531,0.5,0.5,0.5,0.5,0.5,0.5,1,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=276,0,694,461,500,412,531,500,500,500,500,500,500,1000,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
ImageIndex=6

[AppSet22]
App=HUD\HUD 3D
FParamValues=0.276,0,0.145,0.461,0.5,0.412,0.553,0.5,0.5,0.5,0.5,0.5,0.5,1,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=276,0,145,461,500,412,553,500,500,500,500,500,500,1000,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
ImageIndex=6

[AppSet23]
App=HUD\HUD 3D
FParamValues=0.276,0,0.266,0.486,0.5,0.412,0.553,0.5,0.5,0.5,0.5,0.5,0.5,1,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=276,0,266,486,500,412,553,500,500,500,500,500,500,1000,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
ImageIndex=6

[AppSet24]
App=HUD\HUD 3D
FParamValues=0.276,0,0.564,0.515,0.5,0.412,0.553,0.5,0.5,0.5,0.5,0.5,0.5,1,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=276,0,564,515,500,412,553,500,500,500,500,500,500,1000,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
ImageIndex=6

[AppSet25]
App=Misc\Automator
FParamValues=1,18,3,3,1,0.068,0.25,1,19,3,3,1,0.07,0.25,1,20,3,3,1,0.03,0.25,1,21,3,3,1,0.077,0.25
ParamValues=1,18,3,3,1000,68,250,1,19,3,3,1000,70,250,1,20,3,3,1000,30,250,1,21,3,3,1000,77,250
Enabled=1

[AppSet26]
App=Misc\Automator
FParamValues=1,22,3,3,1,0.046,0.25,1,23,3,3,1,0.024,0.25,1,24,3,3,1,0.058,0.25,1,25,3,3,1,0.059,0.25
ParamValues=1,22,3,3,1000,46,250,1,23,3,3,1000,24,250,1,24,3,3,1000,58,250,1,25,3,3,1000,59,250
Enabled=1

[AppSet32]
App=Image effects\Image
FParamValues=0.908,0,0,0,1,0.4718,0.5,0,0,0,0,0,0,0
ParamValues=908,0,0,0,1000,471,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=7

[AppSet34]
App=Image effects\Image
FParamValues=0.908,0,0,0,1,0.6236,0.5,0,0,0,0,0,0,0
ParamValues=908,0,0,0,1000,623,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=7

[AppSet35]
App=Image effects\Image
FParamValues=0.908,0,0,0,1,0.6041,0.5,0,0,0,0,0,0,0
ParamValues=908,0,0,0,1000,604,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=7

[AppSet36]
App=Image effects\Image
FParamValues=0.908,0,0,0,1,0.4222,0.5,0,0,0,0,0,0,0
ParamValues=908,0,0,0,1000,422,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=7

[AppSet33]
App=Misc\Automator
FParamValues=1,33,6,3,0.708,0.025,0.394,1,35,6,3,0.708,0.043,0.394,1,36,6,3,0.708,0.063,0.394,1,37,6,3,0.708,0.075,0.394
ParamValues=1,33,6,3,708,25,394,1,35,6,3,708,43,394,1,36,6,3,708,63,394,1,37,6,3,708,75,394
Enabled=1

[AppSet14]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.4641,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,464,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=4

[AppSet38]
App=HUD\HUD Prefab
FParamValues=38,0,0.5,0,0.764,0.344,0.158,0.479,1,0.132,4,0,0.5,1,0.564,0,1,1
ParamValues=38,0,500,0,764,344,158,479,1000,132,4,0,500,1,564,0,1000,1
Enabled=1
LayerPrivateData=78014B4ECCC9C92F2D51C8C9CC4B8D498670740D0CCCF43273CA1846000000C5620A49

[AppSet54]
App=HUD\HUD Prefab
FParamValues=57,0.556,0.5,0,0.764,0.936,0.211,0.29,1,0.616,4,0,0.5,1,0.368,0,1,1
ParamValues=57,556,500,0,764,936,211,290,1000,616,4,0,500,1,368,0,1000,1
Enabled=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D0C4DF53273CA18460000002B840AB2

[AppSet61]
App=HUD\HUD Prefab
FParamValues=57,0.556,0.5,0,0.764,0.808,0.211,0.29,1,0.616,4,0,0.5,1,0.368,0,1,1
ParamValues=57,556,500,0,764,808,211,290,1000,616,4,0,500,1,368,0,1000,1
Enabled=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D0C4DF53273CA18460000002B840AB2

[AppSet55]
App=HUD\HUD Prefab
FParamValues=58,0.436,0.5,0,0.764,0.514,0.218,0.179,1,0.616,4,0,0.5,1,0.424,0,1,1
ParamValues=58,436,500,0,764,514,218,179,1000,616,4,0,500,1,424,0,1000,1
Enabled=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D0CCDF43273CA18460000002C6D0AB3

[AppSet59]
App=HUD\HUD Prefab
FParamValues=58,0.436,0.5,0,0.764,0.586,0.218,0.179,1,0.616,4,0,0.5,1,0.424,0,1,1
ParamValues=58,436,500,0,764,586,218,179,1000,616,4,0,500,1,424,0,1000,1
Enabled=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D0CCDF43273CA18460000002C6D0AB3

[AppSet60]
App=HUD\HUD Prefab
FParamValues=58,0.436,0.5,0,0.764,0.658,0.218,0.179,1,0.616,4,0,0.5,1,0.424,0,1,1
ParamValues=58,436,500,0,764,658,218,179,1000,616,4,0,500,1,424,0,1000,1
Enabled=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D0CCDF43273CA18460000002C6D0AB3

[AppSet56]
App=HUD\HUD Prefab
FParamValues=143,0,0.5,0,0.764,0.019,0.158,0.187,0.224,1,4,0,0.25,1,0.424,0,1,1
ParamValues=143,0,500,0,764,19,158,187,224,1000,4,0,250,1,424,0,1000,1
Enabled=1
LayerPrivateData=7801CBC9CC4B2D8EC98193BA0626967A9939650C230400007B4808DA

[AppSet50]
App=HUD\HUD Prefab
FParamValues=22,0,0.5,0,0.764,0.714,0.097,0.046,1,1,4,0,0.5,1,0.564,0,0.983,1
ParamValues=22,0,500,0,764,714,97,46,1000,1000,4,0,500,1,564,0,983,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C4CF53273CA18863D00000C6D0AA0

[AppSet49]
App=HUD\HUD Prefab
FParamValues=154,0,0.5,0,0.764,0.232,0.042,0.31,1,0.124,4,0,0.5,1,0.564,0,1,1
ParamValues=154,0,500,0,764,232,42,310,1000,124,4,0,500,1,564,0,1000,1
Enabled=1
LayerPrivateData=7801CBC9CC4B2D8E294E2D482C4A2CC92F4262EA1A1818EA65E694310C6B000054580D20

[AppSet40]
App=HUD\HUD Prefab
FParamValues=45,0.552,0.5,0,0.764,0.536,0.083,0.25,1,0.592,4,0,0.5,1,0.368,0,1,1
ParamValues=45,552,500,0,764,536,83,250,1000,592,4,0,500,1,368,0,1000,1
Enabled=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D0C8CF53273CA184600000028C80AAF

[AppSet41]
App=HUD\HUD Prefab
FParamValues=45,1,0.5,0,0.764,0.536,0.083,0.25,1,0.592,4,0,0.5,1,0.368,0,1,1
ParamValues=45,1000,500,0,764,536,83,250,1000,592,4,0,500,1,368,0,1000,1
Enabled=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D0C8CF53273CA184600000028C80AAF

[AppSet43]
App=HUD\HUD Prefab
FParamValues=46,0.552,0.5,0,0.764,0.884,0.083,0.25,1,1,4,0,0.5,1,0.368,0,1,1
ParamValues=46,552,500,0,764,884,83,250,1000,1000,4,0,500,1,368,0,1000,1
Enabled=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D0C4CF43273CA184600000029B10AB0

[AppSet44]
App=HUD\HUD Prefab
FParamValues=46,0,0.5,0,0.764,0.884,0.083,0.25,1,1,4,0,0.5,1,0.368,0.9389,0.1864,1
ParamValues=46,0,500,0,764,884,83,250,1000,1000,4,0,500,1,368,938,186,1
Enabled=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D0C4CF43273CA184600000029B10AB0

[AppSet58]
App=Postprocess\ParameterShake
FParamValues=1,0,29,16,1,0,57,16,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=1000,0,29,16,1000,0,57,16,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet42]
App=Misc\Automator
FParamValues=0,42,2,6,0.692,0.428,0.25,0,42,17,2,0.632,0.44,0.25,1,45,16,2,0.5,0.514,0.25,1,45,17,2,0.5,0.25,0.25
ParamValues=0,42,2,6,692,428,250,0,42,17,2,632,440,250,1,45,16,2,500,514,250,1,45,17,2,500,250,250
Enabled=1

[AppSet45]
App=Postprocess\ParameterShake
FParamValues=0.788,0.272,41,1,1,0,46,16,3,0,0,0,2,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=788,272,41,1,1000,0,46,16,3,0,0,0,2,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet48]
App=Postprocess\ParameterShake
FParamValues=1,0.004,51,16,1,0,47,16,1,0,0,0,2,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=1000,4,51,16,1000,0,47,16,1,0,0,0,2,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet52]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.636,0.327,0,0,0.528,0,0,0,0
ParamValues=0,0,0,0,1000,636,327,0,0,528,0,0,0,0
Enabled=1
ImageIndex=8

[AppSet53]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text=" "
Html="<position x=""6""><position y=""5""><p align=""left""><font face=""American-Captain"" size=""7"" color=""#333333"">[author]</font></p></position>","<position x=""6""><position y=""15.5""><p align=""left""><font face=""Chosence-Bold"" size=""6"" color=""#333333"">[title]</font></p></position>","<position x=""14.2""><position y=""13.2""><p align=""center""><font face=""Chosence-Bold"" size=""3"" color=""#333333"">[extra1]</font></p></position>","<position x=""0""><position y=""94.5""><p align=""right""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""0""><position y=""75""><p align=""right""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""0""><position y=""78""><p align=""right""><font face=""Chosence-regular"" size=""2.5"" color=""#FFFFF"">[extra3]</font></p></position>","  "
Meshes=[plugpath]Content\Meshes\Car.zgeMesh
Images=[plugpath]Effects\HUD\prefabs\components\component-002.ilv,[plugpath]Effects\HUD\prefabs\iss.svg
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

