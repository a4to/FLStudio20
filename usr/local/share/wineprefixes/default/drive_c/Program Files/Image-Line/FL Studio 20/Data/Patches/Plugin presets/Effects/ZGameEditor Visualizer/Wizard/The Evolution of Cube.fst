FLhd   0 * ` FLdtX  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ���  ﻿[General]
GlWindowMode=1
LayerCount=11
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,6,4,5,1,2,3,8,9,7,10
WizardParams=467

[AppSet0]
App=Canvas effects\Digital Brain
FParamValues=0,0,0,0,0.62,0.5,0.5,0.412,0.52,0.3,0.18,0.31,0.904,0.5,1,0
ParamValues=0,0,0,0,620,500,500,412,520,300,180,310,904,500,1,0
Enabled=1
Collapsed=1
Name=EFX 1

[AppSet6]
App=HUD\HUD Prefab
FParamValues=74,0,0.5,0,0,0.5,0.5,0.284,1,1,4,0,0.5,1,0.776,0,1,1
ParamValues=74,0,500,0,0,500,500,284,1000,1000,4,0,500,1,776,0,1000,1
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,1000,708,336,28,784,500,500
ParamValuesHUD\HUD Grid=0,500,0,1000,500,500,1000,1000,1000,444,500,1000,500,100,0,500,1000,516,1000,300,0,1000
Enabled=1
Collapsed=1
Name=EFX 2
LayerPrivateData=78014B2FCA4C89490712BA0606467A9939650C230B0000B51805E1

[AppSet4]
App=Feedback\BoxedIn
FParamValues=0,0,0,0.552,1,0,0.26,0.5,0,0.82
ParamValues=0,0,0,552,1000,0,260,500,0,820
ParamValuesTerrain\CubesAndSpheres=0,0,0,1000,0,500,500,650,540,400,1000,1000,1000,1000,1000,1000,0,1000
ParamValuesPostprocess\Youlean Color Correction=308,500,512,500,836,904
ParamValuesPostprocess\Youlean Motion Blur=484,1000,732,764
Enabled=1
Collapsed=1
Name=EFX 3

[AppSet5]
App=Feedback\WormHoleEclipse
FParamValues=0,0,0,1,1,0.708,0.336,0.028,0.784,0.5,0.5
ParamValues=0,0,0,1000,1000,708,336,28,784,500,500
ParamValuesFeedback\BoxedIn=0,0,0,1000,1000,0,312,500,0,1000
Enabled=1
Collapsed=1
Name=EFX 5

[AppSet1]
App=Background\FourCornerGradient
FParamValues=6,1,0.724,0.816,1,0.114,1,1,0.804,0.008,0.964,0.75,0.552,1
ParamValues=6,1000,724,816,1000,114,1000,1000,804,8,964,750,552,1000
Enabled=1
Collapsed=1
Name=Color FIXER

[AppSet2]
App=Postprocess\Youlean Color Correction
FParamValues=0.404,0.544,0.5,0.62,0.744,0.68
ParamValues=404,544,500,620,744,680
Enabled=1
Collapsed=1
Name=Color Out

[AppSet3]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.204,0.452,0.296,0.454,1,0,0,0
ParamValues=204,452,296,454,1000,0,0,0
Enabled=1
Collapsed=1
Name=Master Out

[AppSet8]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fade in-out

[AppSet9]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1

[AppSet7]
App=Text\TextTrueType
FParamValues=0.54,0,0,1,0,0.5,0.497,0,0,0,0.5
ParamValues=540,0,0,1000,0,500,497,0,0,0,500
Enabled=1
Name=Main Text

[AppSet10]
App=Text\TextTrueType
FParamValues=0.052,0,0,0,0,0.5,0.5,0,0,0,0.5
ParamValues=52,0,0,0,0,500,500,0,0,0,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""0""><position y=""5""><p align=""center""><font face=""American-Captain"" size=""10"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""0""><position y=""13""><p align=""center""><font face=""Chosence-Bold"" size=""6"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""0""><position y=""86""><p align=""center""><font face=""Chosence-Bold"" size=""4"" color=""#FFFFFF"">[comment]</font></p></position>"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

