<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="Mesh" ClearColor="0 0 0 1" AmbientLightColor="0.5 0.475 0.25 0" ScreenMode="0" CameraPosition="0 0 8" Camera="MeshCamera" LightPosition="0 0 10" ViewportRatio="3" MouseVisible="255" FileVersion="2">
  <Comment>
<![CDATA[HUD Mesh
3D mesh. The rendered mesh is given by the MESH parameter.

Mouse in ZGameEditor Visualizer - Preview:
RMB - displays the pivot point
LMB on pivot point + mouse move - selects and moves mesh

Parameters:
Alpha - transparency
Hue - color hue
Saturation - color saturation
Lightness - color lightness
Size - size of mesh
Position X - X position of mesh
Position Y - Y position of mesh
Material - selection of material
Line/point size - width of wireframe line or size of points 
Light Hue - color hue of light
Light Saturation - color saturation of light
Light Lightness - color lightness of light
Rotation X - rotation around axis X
Rotation Y - rotation around axis Y
Spinning - spinning around axis Y
Is selectable - allow to change position by LMB in Preview]]>
  </Comment>
  <OnLoaded>
    <ZExternalLibrary Comment="OpenGL 4.0 graphics" ModuleName="opengl32" DefinitionsFile="opengl.txt">
      <BeforeInitExp>
<![CDATA[if(ANDROID) {
  if(App.GLBase==0)
    this.ModuleName="libGLESv1_CM.so";
  else
    this.ModuleName="libGLESv2.so";
}]]>
      </BeforeInitExp>
    </ZExternalLibrary>
    <ZLibrary Comment="HSV conversion by Kjell">
      <Source>
<![CDATA[vec4 Color;

float angle(float X)
{
  if(X >= 0 && X < 360)return X;
  if(X > 360)return X-floor(X/360)* 360;
  if(X <   0)return X+floor(X/360)*-360;
}

void hsv(float H, float S, float V)
{
  float R,G,B,I,F,P,Q,T;
  
  H = angle(H);
  S = clamp(S,0,100);
  V = clamp(V,0,100);

  H /= 60;
  S /= 100;
  V /= 100;
  
  if(S == 0)
  {
    Color.R = V;
    Color.G = V;
    Color.B = V;
    return;
  }

  I = floor(H);
  F = H-I;

  P = V*(1-S);
  Q = V*(1-S*F);
  T = V*(1-S*(1-F));

  if(I == 0){R = V; G = T; B = P;}
  if(I == 1){R = Q; G = V; B = P;}
  if(I == 2){R = P; G = V; B = T;}
  if(I == 3){R = P; G = Q; B = V;}
  if(I == 4){R = T; G = P; B = V;}
  if(I == 5){R = V; G = P; B = Q;}
  
  Color.R = R;
  Color.G = G;
  Color.B = B;
}]]>
      </Source>
    </ZLibrary>
    <ZExternalLibrary ModuleName="ZGameEditor Visualizer">
      <Source>
<![CDATA[// ZGameEditor Visualizer built-in functions

void ParamsWriteValueForLayer(xptr Handle, int Layer,int Param, float NewValue) { }]]>
      </Source>
    </ZExternalLibrary>
    <ZLibrary Comment="ZgeViz interface">
      <Source>
<![CDATA[const string AuthorInfo = "Rado1";
const int FLNoDepthClear = 0;

// PARAMETERS

const string ParamHelpConst =
"Alpha\n" +
"Hue\n" +
"Saturation\n" +
"Lightness\n" +
"Size\n" +
"Position X\n" +
"Position Y\n"+
"Material @list: Flat, Smooth, Wireframe, \"Flat & wireframe\",Point, X-Ray\n" +
"Line/point size\n" +
"Light Hue\n" +
"Light Saturation\n" +
"Light Lightness\n" +
"Rotation X\n" +
"Rotation Y\n" +
"Spinning\n" +
"Is selectable @checkbox";

const int
  ALPHA = 0,
  HUE = 1,
  SATURATION = 2,
  LIGHTNESS = 3,
  SIZE = 4,
  POSITION_X = 5,
  POSITION_Y = 6,
  MATERIAL = 7,
  LINE_POINT_SIZE = 8,
  LIGHT_HUE = 9,
  LIGHT_SATURATION = 10,
  LIGHT_LIGHTNESS = 11,
  ROT_X = 12,
  ROT_Y = 13,
  SPINNING = 14,
  IS_SELECTABLE = 15,
  NUM_OF_PARAMS = 16;

const int
  NUM_OF_MATERIALS = 6;

// VARIABLES

xptr FLPluginHandle;
int LayerNr;

//float[32] SpecBandArray;
//float[0] AudioArray;
//float SongPositionInBeats;

//float[NUM_OF_PARAMS] Parameters;
float[NUM_OF_PARAMS] ParamOld;
int[NUM_OF_PARAMS] ParamChanged;]]>
      </Source>
    </ZLibrary>
    <ZLibrary Comment="Globals">
      <Source>
<![CDATA[// CONSTANTS

// determines processing mode flag for OnHostMessage
private const int FPD_ProcessMode = 1;

// boolean
const int FALSE = 0;
const int TRUE = 1;

// varia
const float SELECTION_RADIUS = 15.46875;
const float ZOOM_FACTOR = 4.0;

// VARIABLES

float ViewportWidth, ViewportHeight, AspectRatio, SelectionRadius;
int IsLMB, IsRMB, WasLMB, IsMoved, IsNotExported;
float RefX, RefY, OrigX, OrigY;
float CurrentAngle, PointSize;
int IsTwoMeshes;

// FUNCTIONS

inline float dist(float x1, float y1, float x2, float y2) {
  float dx = x1-x2, dy = y1-y2;
  return sqrt(dx*dx+dy*dy);
}

// Callback to handle external messages
void OnHostMessage(int id, int index, int value) {
  if(id == FPD_ProcessMode)
    IsNotExported = !(value & 16);
}]]>
      </Source>
    </ZLibrary>
    <ZExpression Comment="Init">
      <Expression>
<![CDATA[// init variables
CurrentAngle = 0;
LightColor.A = 1.0;
IsNotExported = TRUE;

// recompute all parameters
for(int i = 0; i < NUM_OF_PARAMS; ++i)
  ParamOld[i] = -1;

// init OpenGL
glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
//glDisable(GL_VERTEX_PROGRAM_POINT_SIZE);
glEnable(GL_POINT_SMOOTH); // makes points rounded
//glDisable(GL_POINT_SMOOTH);
glPointSize(1);]]>
      </Expression>
    </ZExpression>
  </OnLoaded>
  <OnUpdate>
    <Condition Expression="return App.ViewportWidth != 0;">
      <OnTrue>
        <ZExpression Comment="Reset input">
          <Expression>
<![CDATA[IsLMB = FALSE;
IsRMB = FALSE;]]>
          </Expression>
        </ZExpression>
        <KeyPress Comment="LMB" Keys="{">
          <OnPressed>
            <ZExpression Expression="IsLMB = TRUE;"/>
          </OnPressed>
        </KeyPress>
        <KeyPress Comment="RMB" Keys="}">
          <OnPressed>
            <ZExpression Expression="IsRMB = TRUE;"/>
          </OnPressed>
        </KeyPress>
        <ZExpression Comment="Handle parameter change">
          <Expression>
<![CDATA[int s = FALSE;

// changed viewport size?
if (App.ViewportWidth != ViewportWidth || App.ViewportHeight != ViewportHeight) {
  ViewportWidth = App.ViewportWidth;
  ViewportHeight = App.ViewportHeight;
  AspectRatio = ViewportWidth / ViewportHeight;
  SelectionRadius = SELECTION_RADIUS / ViewportHeight;
  SelectionTrans.Scale.X = SelectionRadius * ZOOM_FACTOR;
  SelectionTrans.Scale.Y = SelectionRadius * ZOOM_FACTOR;
  SelectionRadius *= 0.5;
  s = TRUE;
}

// update parameter change
for(int i = 0; i < NUM_OF_PARAMS; ++i) {
  ParamChanged[i] = Parameters[i] != ParamOld[i];
  if (ParamChanged[i])
    ParamOld[i] = Parameters[i];
}

// check changed parameters

// update color
if (ParamChanged[HUE] || ParamChanged[SATURATION] || ParamChanged[LIGHTNESS] ||
  ParamChanged[ALPHA]) {

  hsv(Parameters[HUE]*360,Parameters[SATURATION]*100,(1-Parameters[LIGHTNESS])*100);
  MeshColor.Color = Color;
  MeshColor.Color.A = 1.0 - Parameters[ALPHA];
  XRayStrength.Value = MeshColor.Color.A;

  hsv(Parameters[HUE]*360,Parameters[SATURATION]*100,(Parameters[LIGHTNESS])*100);
  MeshColor1.Color.R = Color.R;
  MeshColor1.Color.G = Color.G;
  MeshColor1.Color.B = Color.B;
  MeshColor1.Color.A = MeshColor.Color.A;
}

// update size
if (ParamChanged[SIZE])
  MeshTrans.Scale = pow(Parameters[SIZE] * 3.0, 3);

// update position X
if (ParamChanged[POSITION_X]) {
  SelectionTrans.Translate.X = (Parameters[POSITION_X] - 0.5) * ZOOM_FACTOR;
  MeshTrans.Translate.X = SelectionTrans.Translate.X * 3.2;
}

// update position Y
if (ParamChanged[POSITION_Y]) {
  SelectionTrans.Translate.Y = (Parameters[POSITION_Y] - 0.5) * ZOOM_FACTOR;
  MeshTrans.Translate.Y = SelectionTrans.Translate.Y * 3.2;
}

// update material
if (ParamChanged[MATERIAL]) {
  int m = round(Parameters[MATERIAL] * NUM_OF_MATERIALS);

  switch (m){

    case 0: // flat
      MeshMaterial.Material = FlatMaterial;
      IsTwoMeshes = FALSE;
      break;

    case 1: // smooth
      MeshMaterial.Material = SmoothMaterial;
      IsTwoMeshes = FALSE;
      break;

    case 2: // wireframe
      MeshMaterial.Material = WireframeMaterial;
      IsTwoMeshes = FALSE;
      break;

    case 3: // flat & wireframe
      MeshMaterial.Material = Flat2Material;
      IsTwoMeshes = TRUE;
      break;

    case 4: // points
      MeshMaterial.Material = PointMaterial;
      IsTwoMeshes = FALSE;
      break;

    case 5: // x-ray
      MeshMaterial.Material = XRayMaterial;
      IsTwoMeshes = FALSE;
      break;
  }
}

// update line width/point size
if (ParamChanged[LINE_POINT_SIZE] || s) {
  PointSize = 1.0 + Parameters[LINE_POINT_SIZE] * ViewportWidth * 0.05;
  WireframeMaterial.WireframeWidth = PointSize;
  Wireframe2Material.WireframeWidth = PointSize;
}

// update light color
if (ParamChanged[LIGHT_HUE] || ParamChanged[LIGHT_SATURATION] ||
  ParamChanged[LIGHT_LIGHTNESS]) {

  hsv(Parameters[LIGHT_HUE]*360, Parameters[LIGHT_SATURATION]*100,
    (1-Parameters[LIGHT_LIGHTNESS])*100);

  App.AmbientLightColor = Color;
  LightColor.R = Color.R;
  LightColor.G = Color.G;
  LightColor.B = Color.B;
}

// update rotation X
if (ParamChanged[ROT_X])
  MeshTrans.Rotate.X = 0.5 - Parameters[ROT_X];

// update rotation Y
if (ParamChanged[ROT_Y])
  MeshTrans.Rotate.Y = Parameters[ROT_Y] - 0.5;

// update spin angle
CurrentAngle += (Parameters[SPINNING] - 0.5) * App.DeltaTime;
SpinTrans.Rotate.Y = CurrentAngle;

// handle LMB

if (Parameters[IS_SELECTABLE] && IsLMB) {

  float mx = App.MousePosition.X * AspectRatio / ZOOM_FACTOR + 0.5;
  float my = App.MousePosition.Y / ZOOM_FACTOR + 0.5;

  if (IsMoved) {

    // move
    ParamsWriteValueForLayer(FLPluginHandle, LayerNr, POSITION_X, OrigX + mx - RefX);
    ParamsWriteValueForLayer(FLPluginHandle, LayerNr, POSITION_Y, OrigY + my - RefY);

  } else {

    if (!WasLMB && dist(
      Parameters[POSITION_X], Parameters[POSITION_Y], mx, my) < SelectionRadius) {

      // start moving
      RefX = mx;
      RefY = my;
      OrigX = Parameters[POSITION_X];
      OrigY = Parameters[POSITION_Y];
      IsMoved = TRUE;
    }
  }
} else // IsLMB
  IsMoved = FALSE;

WasLMB = IsLMB;]]>
          </Expression>
        </ZExpression>
      </OnTrue>
    </Condition>
  </OnUpdate>
  <OnRender>
    <ZExpression Comment="Use mesh camera">
      <Expression>
<![CDATA[App.Camera = MeshCamera;
glPointSize(PointSize);]]>
      </Expression>
    </ZExpression>
    <RenderTransformGroup Name="MeshTrans" Scale="0.4219 0.4219 0.4219">
      <Children>
        <RenderTransformGroup Name="SpinTrans">
          <Children>
            <UseMaterial Name="MeshMaterial" Material="FlatMaterial"/>
            <RenderSetColor Name="MeshColor" Color="1 1 1 1"/>
            <RenderMesh Mesh="TheMesh"/>
            <Condition Comment="Two meshes?" Expression="return IsTwoMeshes;">
              <OnTrue>
                <UseMaterial Material="Wireframe2Material"/>
                <RenderSetColor Name="MeshColor1" Color="0 0 0 1"/>
                <RenderMesh Mesh="TheMesh"/>
              </OnTrue>
            </Condition>
          </Children>
        </RenderTransformGroup>
      </Children>
    </RenderTransformGroup>
    <Condition Comment="Is selected?" Expression="return Parameters[IS_SELECTABLE] &amp;&amp; (IsRMB || IsMoved) &amp;&amp; IsNotExported;">
      <OnTrue>
        <ZExpression Comment="Use HUD camera" Expression="App.Camera = HudCamera;"/>
        <RenderTransformGroup Name="SelectionTrans" Scale="0.1663 0.1663 1">
          <Children>
            <UseMaterial Material="SelectionMaterial"/>
            <RenderSprite/>
          </Children>
        </RenderTransformGroup>
      </OnTrue>
    </Condition>
  </OnRender>
  <Content>
    <Array Name="Parameters" SizeDim1="16" Persistent="255">
      <Values>
<![CDATA[789C636000037B063868B083F2C16267CFF8D8CE9A29892C86841BEC01C0550722]]>
      </Values>
    </Array>
    <Variable Name="LightColor" Type="8"/>
    <Mesh Name="TheMesh">
      <Producers>
        <MeshBox/>
      </Producers>
    </Mesh>
    <Camera Name="MeshCamera" Position="0 0 8"/>
    <Camera Name="HudCamera" Kind="1" Position="0 0 8"/>
    <Group Comment="Materials">
      <Children>
        <Material Name="FlatMaterial" Shading="1" Blend="1"/>
        <Shader Name="SmoothShader">
          <VertexShaderSource>
<![CDATA[varying vec3 v;
varying vec3 n;
varying vec4 c;

void main() {
  // vertex, normal and color
  v = (gl_ModelViewMatrix * gl_Vertex).xyz;
  n = gl_NormalMatrix * gl_Normal;
  c = gl_Color;

  // position
  gl_Position = ftransform();
}]]>
          </VertexShaderSource>
          <FragmentShaderSource>
<![CDATA[varying vec3 v;
varying vec3 n;
varying vec4 c;

uniform vec4 lightColor;

void main() {
  // reflection
  float r = max(dot(normalize(n), normalize(-v)), 0.0);

  gl_FragColor = r * c * 0.8 + pow(r, 32.0) * lightColor;
  gl_FragColor.a = c.a;
}]]>
          </FragmentShaderSource>
          <UniformVariables>
            <ShaderVariable VariableName="lightColor" VariableRef="LightColor"/>
          </UniformVariables>
        </Shader>
        <Material Name="SmoothMaterial" Light="0" Blend="1" Shader="SmoothShader"/>
        <Material Name="WireframeMaterial" WireframeWidth="2.6525" Shading="2" Blend="1" DrawBackFace="255"/>
        <Material Name="Flat2Material" Shading="1" Blend="1"/>
        <Shader Name="Wireframe2Shader">
          <VertexShaderSource>
<![CDATA[void main() {
  vec4 v = gl_ModelViewMatrix * gl_Vertex;
  v.xyz *= 0.99;

  gl_Position = gl_ProjectionMatrix * v;
  gl_FrontColor = gl_Color;
}]]>
          </VertexShaderSource>
          <FragmentShaderSource>
<![CDATA[void main() {
  gl_FragColor = gl_Color;
}]]>
          </FragmentShaderSource>
        </Shader>
        <Material Name="Wireframe2Material" WireframeWidth="2.6525" Shading="2" Light="0" Blend="1" Shader="Wireframe2Shader"/>
        <Material Name="PointMaterial" Shading="3" Blend="1" DrawBackFace="255"/>
        <Shader Name="XRayShader">
          <VertexShaderSource>
<![CDATA[varying vec3 v;
varying vec3 n;
varying vec4 c;

void main() {
  // vertex, normal and color
  v = (gl_ModelViewMatrix * gl_Vertex).xyz;
  n = gl_NormalMatrix * gl_Normal;
  c = gl_Color;

  // position
  gl_Position = ftransform();
}]]>
          </VertexShaderSource>
          <FragmentShaderSource>
<![CDATA[varying vec3 v;
varying vec3 n;
varying vec4 c;

uniform float strength;

void main() {
  // opacity
  float op = 1.0 - pow(abs(dot(normalize(n), normalize(-v))), strength);

  // final color
  gl_FragColor = op * c;
  gl_FragColor.a = gl_FrontFacing ? op * 2.0: op;
}]]>
          </FragmentShaderSource>
          <UniformVariables>
            <ShaderVariable Name="XRayStrength" VariableName="strength" Value="1"/>
          </UniformVariables>
        </Shader>
        <Material Name="XRayMaterial" WireframeWidth="2" Shading="1" Color="0.502 1 1 1" Light="0" Blend="1" ZBuffer="0" DrawBackFace="255" Shader="XRayShader"/>
        <Bitmap Name="SelectionBitmap" Comment="NoCustom" Width="128" Height="128">
          <Producers>
            <BitmapExpression>
              <Expression>
<![CDATA[// Local variables

float B, S, X1, Y1;

// Distance from center ( per axis )

X1 = (0.5-X) * 1.01;
Y1 = (0.5-Y) * 1.01;

B = 128; // 128 = BitmapSize | 1 = Anti-Aliasing bias ( in pixels )

Pixel.A = B*(0.5-sqrt(X1*X1+Y1*Y1));

// Set RGB to white

Pixel.R = 1;
Pixel.G = 1;
Pixel.B = 1;]]>
              </Expression>
            </BitmapExpression>
          </Producers>
        </Bitmap>
        <Material Name="SelectionMaterial" Shading="1" Color="1 0.2 0.2 0.5" Light="0" Blend="1" ZBuffer="0">
          <Textures>
            <MaterialTexture Texture="SelectionBitmap" TexCoords="1"/>
          </Textures>
        </Material>
      </Children>
    </Group>
  </Content>
</ZApplication>
