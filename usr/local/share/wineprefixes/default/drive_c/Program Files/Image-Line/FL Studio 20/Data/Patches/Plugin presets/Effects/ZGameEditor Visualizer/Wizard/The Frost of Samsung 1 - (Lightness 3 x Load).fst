FLhd   0 * ` FLdt-#  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV գE�"  ﻿[General]
GlWindowMode=1
LayerCount=19
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=6,16,8,9,0,20,14,15,10,21,11,1,4,2,3,5,12,7,17
WizardParams=1041

[AppSet6]
App=Canvas effects\Lava
FParamValues=0,0.648,1,0.648,0,0.5,0.5,1,0.528,0.236,0.38
ParamValues=0,648,1000,648,0,500,500,1000,528,236,380
ParamValuesCanvas effects\Digital Brain=0,0,568,0,428,1000,508,4,892,28,0,264,340,292,1000,0
ParamValuesBackground\SolidColor=0,932,1000,0
Enabled=1
Name=EFX 1

[AppSet16]
App=Canvas effects\OverlySatisfying
FParamValues=0,0.74,0,0
ParamValues=0,740,0,0
Enabled=1
Name=EFX 2

[AppSet8]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.332,0.476,0.618,0.66,1,0,0,0
ParamValues=332,476,618,660,1000,0,0,0
ParamValuesBackground\FourCornerGradient=400,1000,700,1000,1000,726,1000,1000,8,972,1000,750,1000,984
ParamValuesPostprocess\Youlean Motion Blur=0,1000,420,776
ParamValuesPostprocess\Blur=644
Enabled=1
Name=Bloom

[AppSet9]
App=Feedback\WarpBack
FParamValues=0.248,0,0,0.052,0.468,0.5,0.2
ParamValues=248,0,0,52,468,500,200
ParamValuesHUD\HUD Graph Radial=0,500,0,712,500,500,82,444,500,0,1000,0,1000,64,250,500,200,312,12,0,500,1000
ParamValuesFeedback\70sKaleido=0,0,228,1000,860,324
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,696,0,728,500,584,596,500,768
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,1000,0,0,500,920,552,500
ParamValuesHUD\HUD Graph Linear=0,1000,1000,0,500,504,1000,584,810,444,500,1000,0,688,796,900,164,180,132,333,536,1000
ParamValuesPostprocess\Youlean Pixelate=392,333,0,0
ParamValuesFeedback\BoxedIn=0,0,0,0,1000,1000,748,500,0,0
ParamValuesFeedback\FeedMeFract=0,0,0,1000,564,0
ParamValuesPostprocess\Youlean Motion Blur=688,1000,408,672
ParamValuesFeedback\FeedMe=0,0,0,1000,500,680,312
Enabled=1
Name=OMG EFX

[AppSet0]
App=HUD\HUD Prefab
FParamValues=27,0,0.5,0,1,0.5,0.5,0.53,1,1,4,0,0.5,1,0.876,0,1,1
ParamValues=27,0,500,0,1000,500,500,530,1000,1000,4,0,500,1,876,0,1000,1
ParamValuesBackground\FourCornerGradient=533,1000,0,1000,1000,434,1000,1000,500,1000,1000,750,1000,1000
ParamValuesBackground\SolidColor=0,932,1000,0
ParamValuesHUD\HUD Grid=0,500,0,1000,500,500,1000,1000,888,444,500,1000,500,428,0,500,1000,1000,648,0,0,1000
Enabled=1
Name=SWAP
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0DF43273CA18863D000008D20A9C

[AppSet20]
App=Postprocess\Blooming
FParamValues=0,0,0,1,0.7,0.944,0.5,0.5,0
ParamValues=0,0,0,1000,700,944,500,500,0
ParamValuesFeedback\WormHoleEclipse=0,180,1000,1000,1000,1000,0,976,764,500,500
Enabled=1
Name=Master LCD

[AppSet14]
App=Postprocess\Youlean Motion Blur
FParamValues=0.448,1,0.456,0.16,0,0,0
ParamValues=448,1,456,160,0,0,0
ParamValuesHUD\HUD Prefab=6,0,500,0,1000,500,1000,514,1000,1000,444,0,660,0,368,372,736,1000
ParamValuesCanvas effects\Lava=0,664,1000,684,168,500,500,1000,528,500,224
ParamValuesFeedback\70sKaleido=0,0,320,180,908,152
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,1000,0,432,652,616,500,500
ParamValuesHUD\HUD Grid=0,500,0,1000,500,500,1000,1000,1000,444,500,1000,500,100,0,500,1000,800,904,300,0,1000
ParamValuesImage effects\Image=0,0,0,0,644,500,500,96,53,250,0,0,0,0
ParamValuesPostprocess\Youlean Bloom=1000,648,0,742
ParamValuesPostprocess\Youlean Blur=500,1000,600
ParamValuesPostprocess\Youlean Color Correction=500,488,500,972,800,620
Enabled=1
ImageIndex=6
Name=TOTAL Blur

[AppSet15]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.836,0.432,0.15,0.442,1,0,0,0
ParamValues=836,432,150,442,1000,0,0,0
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,708,348,544,500,260,500,500,560
ParamValuesFeedback\FeedMeFract=0,0,0,0,296,0
Enabled=1
UseBufferOutput=1
Name=Cool Bloom

[AppSet10]
App=Background\SolidColor
FParamValues=0,0,0,0.352
ParamValues=0,0,0,352
ParamValuesPostprocess\ScanLines=200,0,0,0,0,0
Enabled=1
Name=Background

[AppSet21]
App=Peak Effects\Linear
FParamValues=0,0.964,0.788,0.22,0.326,0.588,0.5,0.388,0.752,0.5,0.26,0.35,0,0,1,0,0,0.5,0.5,0.5,0.5,0,0.5,0.152,0.2,0,0.032,0.212,0.33,0.25,0.1
ParamValues=0,964,788,220,326,588,500,388,752,500,260,350,0,0,1,0,0,500,500,500,500,0,500,152,200,0,32,212,330,250,100
ParamValuesFeedback\WarpBack=0,0,0,0,0,500,144
ParamValuesParticles\ReactiveFlow=0,917,500,0,644,0,756,0,0,96,632,12,300,192,648,512,200,472,0,68,684,868,220,0,0
ParamValuesParticles\ColorBlobs=816,0,0,0,700,500,500,350,0,148
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,0,1000,992,264,592,500,1000
ParamValuesHUD\HUD Grid=0,500,0,0,500,500,1000,1000,1000,444,500,1000,500,100,0,500,1000,100,500,300,0,1000
ParamValuesParticles\fLuids=0,0,0,0,808,500,500,680,0,0,500,500,500,0,500,250,500,0,0,500,500,500,0,0,500,0,0,250,500,0,0,0
ParamValuesParticles\PlasmaFlys=500,500,500,0,1000,500,500,500,0,500,500,500,500,500
ParamValuesFeedback\BoxedIn=0,0,0,256,1000,0,228,500,16,500
ParamValuesFeedback\FeedMe=0,0,0,1000,500,1000,708
ParamValuesPeak Effects\JoyDividers=0,0,0,1000,604,552,1000,0,348,408,664,690,486
ParamValuesFeedback\70sKaleido=0,0,544,1000,724,196
ParamValuesFeedback\WormHoleDarkn=0,0,0,0,1000,500,1000,0,1000,680,500,500
ParamValuesFeedback\SphericalProjection=60,0,0,0,750,425,500,590,500,500,0,333,530,1000,316,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,914,500,500,500,500,500
ParamValuesFeedback\FeedMeFract=0,0,0,668,808,0
ParamValuesParticles\ReactiveMob=0,500,500,500,996,500,500,1000,500,500,500,500,500,500,500,500,200,50,125,0,500,500,1000,1000,800,100,500,0,0,0
Enabled=1
ImageIndex=6
Name=Grid BG 2

[AppSet11]
App=HUD\HUD Grid
AppVersion=1
FParamValues=0.468,0.5,0,0.664,0.5,0.5,0.96,0.672,1,4,0.548,1,0.5,0.828,0.472,0.144,0.488,0,1,0,0.368,1
ParamValues=468,500,0,664,500,500,960,672,1000,4,548,1000,500,828,472,144,488,0,1000,0,368,1
ParamValuesHUD\HUD Prefab=18,0,0,0,884,488,496,588,1000,1000,444,0,324,0,0,0,1000,1000
ParamValuesPostprocess\ScanLines=0,71,464,464,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,157,500,500,500,500,500
Enabled=1
Name=Grid BG 1

[AppSet1]
App=Image effects\ImageBox
FParamValues=0,0,0,0,0,0.5,0.5,0.5,0.5,0.5,0,0,0,0,0,0,0.5,0.5,0.5
ParamValues=0,0,0,0,0,500,500,500,500,500,0,0,0,0,0,0,500,500,500
ParamValuesImage effects\Image=0,0,0,0,997,500,500,0,0,0,0,0,0,0
Enabled=0
Name=SM Buttons

[AppSet4]
App=Image effects\Image
FParamValues=0.956,0,0,0.324,1,0.5,0.5,0.002,0.008,0,0,0,0,0
ParamValues=956,0,0,324,1000,500,500,2,8,0,0,0,0,0
Enabled=1
ImageIndex=2
Name=SM Glass

[AppSet2]
App=Image effects\Image
FParamValues=0,0,0,0.952,0.96,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,952,960,500,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=4
Name=SM Border

[AppSet3]
App=Image effects\Image
FParamValues=0.588,0,0,0.12,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=588,0,0,120,1000,500,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=1
Name=SM Camera

[AppSet5]
App=Image effects\Image
FParamValues=0.696,0,0,0,0.998,0.5,0.5,0.002,0.001,0,0,0,0,0
ParamValues=696,0,0,0,998,500,500,2,1,0,0,0,0,0
Enabled=1
ImageIndex=3
Name=SM Frame

[AppSet12]
App=Background\FourCornerGradient
FParamValues=8,0,0,1,1,0.25,1,1,0.5,1,1,0.75,1,1
ParamValues=8,0,0,1000,1000,250,1000,1000,500,1000,1000,750,1000,1000
Enabled=1
Name=Master Filter Color

[AppSet7]
App=Image effects\Image
FParamValues=0,0,0,0,0.644,0.5,0.5,0.102,0.053,0.25,0,0,0,0.148
ParamValues=0,0,0,0,644,500,500,102,53,250,0,0,0,148
Enabled=1
ImageIndex=5
Name=LCD Module

[AppSet17]
App=Text\TextTrueType
FParamValues=0.244,0,0,0,0,0.5,0.5,0,0,0,0.5
ParamValues=244,0,0,0,0,500,500,0,0,0,500
Enabled=1
Name=Main Text

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""4"" y=""5""><p><font face=""American-Captain"" size=""4"" color=""#000000"">[author]</font></p></position>","<position x=""4"" y=""8""><p><font face=""Chosence-Bold"" size=""3"" color=""#000000"">[title]</font></p></position>","<position x=""4"" y=""14""><p> <font face=""Chosence-Bold"" size=""3"" color=""#000000"">[comment]</font></p></position>"
Images=[presetpath]Wizard\ColoveContent\Devices\svg\Samsung\Samsung-S-8-Buttons.svg,[presetpath]Wizard\ColoveContent\Devices\svg\Samsung\Samsung-S-8-Camera.svg,[presetpath]Wizard\ColoveContent\Devices\svg\Samsung\Samsung-S-8-Glass.svg,[presetpath]Wizard\ColoveContent\Devices\png\Display\Samsung-S-8-Frame.png,[presetpath]Wizard\ColoveContent\Devices\svg\Samsung\Samsung-S-8-Border-RQ.svg
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

