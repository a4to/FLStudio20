<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="jph wacheski - 2011" ClearColor="0 0 0 1" ScreenMode="0" CameraPosition="0 0 0" CameraRotation="0 0 0.5" FileVersion="2">
  <Comment>
<![CDATA[Filaments - Script Unique Parameters

Direction - of filament tendancy.
Bounce - waviness of filaments flailing.
Openness -  wave size off of zero.
Warble - a random undulation motion added.
Thickness - individual filament vertical size.
Smoothness - number of polygons used to create filaments.]]>
  </Comment>
  <OnLoaded>
    <ZLibrary Comment="HSV convertion by Kjell">
      <Source>
<![CDATA[//

float angle(float X)
{
  if(X >= 0 && X < 360)return X;
  if(X > 360)return X-floor(X/360)* 360;
  if(X <   0)return X+floor(X/360)*-360;
}

//

void hsv(float H, float S, float V)
{
  float R,G,B,I,F,P,Q,T;
  
  H = angle(H);
  S = clamp(S,0,100);
  V = clamp(V,0,100);

  H /= 60;
  S /= 100;
  V /= 100;
  
  if(S == 0)
  {
    Color[0] = V;
    Color[1] = V;
    Color[2] = V;
    return;
  }

  I = floor(H);
  F = H-I;

  P = V*(1-S);
  Q = V*(1-S*F);
  T = V*(1-S*(1-F));

  if(I == 0){R = V; G = T; B = P;}
  if(I == 1){R = Q; G = V; B = P;}
  if(I == 2){R = P; G = V; B = T;}
  if(I == 3){R = P; G = Q; B = V;}
  if(I == 4){R = T; G = P; B = V;}
  if(I == 5){R = V; G = P; B = Q;}
  
  Color[0] = R;
  Color[1] = G;
  Color[2] = B;
}]]>
      </Source>
    </ZLibrary>
  </OnLoaded>
  <OnUpdate>
    <ZExpression Comment="CONTROLS">
      <Expression>
<![CDATA[fil_color.color.a=(1-Parameters[0]);

hsv(Parameters[1]*360,Parameters[2]*100,(1-Parameters[3])*100);

c[0]=Color[0];
c[1]=Color[1];
c[2]=Color[2];

app.CameraRotation.z=Parameters[7];

direction=-.5+Parameters[8];
bounce=Parameters[9];
open=(-.5+Parameters[10])*2;
warble=Parameters[11];
thickness=0.05+Parameters[12];
waves.XCount=64-ceil(Parameters[13]*64);

mfl_t.TextureX+=direction*-0.123;

app.cameraPosition.z=(1-Parameters[4])*21;
app.cameraPosition.x=(-0.5+Parameters[5])*8;
app.cameraPosition.y=(-0.5+Parameters[6])*-8;

/*
Alpha
Hue
Saturation
Lightness
Size
Position X
Position Y
Rotation
Direction
Bounce
Openness
Warble
Thickness
Smoothness]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <OnRender>
    <UseMaterial Material="m_fil"/>
    <Repeat Name="it" Count="16">
      <OnIteration>
        <RenderTransform Name="rise_fall" Comment="trans y up and down -.5 - .5 " Scale="1 0.9 1"/>
        <RenderTransformGroup Name="space_warp" Comment="trans y 5 to 5 " Scale="16 4 0">
          <Children>
            <ZExpression>
              <Expression>
<![CDATA[fil_color.color.r=it.iteration*(c[0]*.1)+.1;
fil_color.color.g=it.iteration*(c[1]*.1)+.1;
fil_color.color.b=it.iteration*(c[2]*.1)+.1;]]>
              </Expression>
            </ZExpression>
            <RenderSetColor Name="fil_color" Color="0.85 0.475 0.475 0.5"/>
            <RenderNet Name="waves" Comment="WAVE forms" XCount="63">
              <RenderVertexExpression>
<![CDATA[//Update each vertex.
//Vertex : current vertex
//TexCoord : current texture coordinate

this.vertex.y*=thickness;

this.vertex.y+=(noise2( this.vertex.X+app.time*direction, it.Iteration+app.time*bounce )
 *(it.Iteration*open))+(noise2(it.Iteration,this.vertex.X-app.time)*warble);]]>
              </RenderVertexExpression>
            </RenderNet>
          </Children>
        </RenderTransformGroup>
      </OnIteration>
    </Repeat>
  </OnRender>
  <Content>
    <Bitmap Name="beam_map" Width="16" Height="16" Filter="2">
      <Producers>
        <BitmapExpression>
          <Expression>
<![CDATA[//X,Y : current coordinate (0..1)
//Pixel : current color (rgb)

pixel.r=sin(y*pi)+rnd()*.1;
pixel.g=sin(y*pi);
pixel.b=sin(y*pi)+rnd()*.2;
pixel.a=8*sin(y*pi*1.2);]]>
          </Expression>
        </BitmapExpression>
      </Producers>
    </Bitmap>
    <Material Name="m_fil" WireframeWidth="1" Shading="1" Light="0" Blend="1" ZBuffer="0">
      <Textures>
        <MaterialTexture Name="mfl_t" Texture="beam_map" TextureScale="10 1 1" TextureX="-0.0615" TexCoords="1"/>
      </Textures>
    </Material>
    <Array Name="c" Comment="fil color" SizeDim1="3" Persistent="255">
      <Values>
<![CDATA[78DA636060B0676068B003610007B701BC]]>
      </Values>
    </Array>
    <Variable Name="direction"/>
    <Variable Name="bounce"/>
    <Variable Name="open"/>
    <Variable Name="warble"/>
    <Variable Name="thickness"/>
    <Array Name="Parameters" SizeDim1="14" Persistent="255">
      <Values>
<![CDATA[78DA6360C000F63830B23C0300295101BA]]>
      </Values>
    </Array>
    <Constant Name="ParamHelpConst" Type="2">
      <StringValue>
<![CDATA[Alpha
Hue
Saturation
Lightness
Size
Position X
Position Y
Rotation
Direction
Bounce
Openness
Warble
Thickness
Smoothness]]>
      </StringValue>
    </Constant>
    <Array Name="Color" SizeDim1="3"/>
    <Constant Name="AuthorInfo" Type="2">
      <StringValue>
<![CDATA[Jph Wacheski
http://jph_wacheski.itch.io/]]>
      </StringValue>
    </Constant>
  </Content>
</ZApplication>
