FLhd   0  ` FLdtC  �	12.5.1.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4    	          A                  �   =   �  �  �    �HQV ��!�  ﻿[General]
GlWindowMode=1
LayerCount=11
FPS=1
DmxOutput=0
DmxDevice=0
OutputControllers=0
MidiPort=0
Aspect=1
CameraMode=0
CameraZoom=0
CameraRotate=0
LayerOrder=2,4,9,10,3,8,5,0,6,7,1
Info=

[AppSet2]
App=Peak Effects\Linear
ParamValues=0,511,0,0,423,869,500,517,250,500,0,500,0,0,0,1000,0,500,500,1000,350,18,1000,1,0,500
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=1
MeshIndex=0

[AppSet4]
App=Peak Effects\Linear
ParamValues=0,511,0,0,423,130,500,517,250,500,0,500,1000,0,0,1000,0,500,500,1000,350,18,1000,1,0,500
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=1
MeshIndex=0

[AppSet9]
App=Peak Effects\Linear
ParamValues=651,511,565,0,121,447,500,1000,0,500,0,790,500,1,0,1000,0,500,500,1000,350,0,135,1,1000,500
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=1
MeshIndex=0

[AppSet10]
App=Peak Effects\Linear
ParamValues=651,511,565,0,121,553,500,1000,500,500,0,790,500,1,0,1000,0,500,500,1000,350,0,135,1,1000,500
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=1
MeshIndex=0

[AppSet3]
App=Postprocess\Edge Detect
ParamValues=1000,0,0,976,0,440
ParamValuesPostprocess\Blur=1000
ParamValuesPostprocess\Dot Matrix=1000,500,500,0,0,0,487,1000
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=1
MeshIndex=0

[AppSet8]
App=Text\TextTrueType
ParamValues=607,0,0,0,0,500,500,0,0,0,500
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=1
MeshIndex=0

[AppSet5]
App=Postprocess\Blur
ParamValues=267
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=1
MeshIndex=0

[AppSet0]
App=Peak Effects\Polar
ParamValues=946,214,730,0,0,250,500,1000,500,500,896,498,1000,200,1,1000,1000,1000,1000,0,337,1000,1
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=1
MeshIndex=0

[AppSet6]
App=Peak Effects\Polar
ParamValues=946,214,730,0,0,750,500,1000,500,500,896,498,1000,800,1,1000,1000,1000,1000,0,337,1000,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=1
MeshIndex=0

[AppSet7]
App=Canvas effects\Lava
ParamValues=802,0,0,896,1000,500,500,219,720,844
ParamValuesBackground\FourCornerGradient=0,338,0,1000,1000,250,1000,1000,500,1000,1000,750,1000,1000
ParamValuesBackground\ItsFullOfStars=724,0,0,0,588,500,500,595,0,0,0
ParamValuesCanvas effects\BitPadZ=0,0,0,0,1000,500,500,0,0,0,0,0,0,0,0,0,123,0,1000,0,0
ParamValuesCanvas effects\Electric=0,740,484,780,1000,500,500,23,100,1000,500
ParamValuesCanvas effects\Flaring=959,1000,1000,1000,1000,500,500,37,0,400,500
ParamValuesCanvas effects\OverlySatisfying=0,740,0,179,600,500,500,100,100,1000,500
ParamValuesCanvas effects\Rain=1000,500,0,965,327,500,500,651,519,0,0,1000
ParamValuesCanvas effects\SkyOcean=0,0,500,0,0,500,500,255,500,282,0,0,0
ParamValuesImage effects\Image=861,0,0,0,1000,500,500,0,0,0,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=1
MeshIndex=0

[AppSet1]
App=Postprocess\ColorCyclePalette
ParamValues=1,0,15,2,0,1000,231,549,982,1,0
ParamValuesInternal controllers\ZGE XY Controller=1000,1000,1000,500,500,800,0,0,0
ParamValuesPostprocess\Blur=0
ParamValuesTunnel\Oblivion=827,294,804,364,1000,500,500,100,100,1000,500
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=1
MeshIndex=0

[Video export]
VideoH=480
VideoW=640
VideoRenderFps=30
SampleRate=0
VideoCodecName=Windows Media Video 9 Advanced Profile
AudioCodecName=Windows Media Audio 10 Professional
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=0
AudioBitrate=128000
Uncompressed=0

[UserContent]
Text=
Html="<position y=""0"">","<p align=""center"">","<font face=""FrancoisOne"" size=""9""><b><i>[author]</i></b></font>",</p>,</position>,"<position y=""90"">","<p align=""center"">","<font face=""FrancoisOne"" size=""7""><b><i>[title]</i></b></font>",</p>,</position>
VideoCues=
Meshes=
MeshAutoScale=0
MeshWithColors=0
Images=[plugpath]Content\Bitmaps\template3.jpg
VideoUseSync=0

[Detached]
Top=180
Left=819
Width=1101
Height=748

[Controller]
RedBase=0
GreenBase=0
BlueBase=0
LumBase=0
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

