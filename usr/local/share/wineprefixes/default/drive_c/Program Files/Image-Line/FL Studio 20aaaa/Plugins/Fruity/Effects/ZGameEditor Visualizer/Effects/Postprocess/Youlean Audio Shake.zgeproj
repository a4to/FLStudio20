<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" FileVersion="2">
  <OnLoaded>
    <SpawnModel Model="Canvas" SpawnStyle="1"/>
    <ZLibrary Comment="Youlean Helper Library">
      <Source>
<![CDATA[/*

Copyright (C) 2018 and later, Youlean

V1.1


// MANUAL -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


Function: "ArraySlope" will change slope of the array.

Parameter: "slopeValue" above 1 will boost start of the array and values below 1 will decrease values at start.


void ArraySlope(float[] in, int arraySize, float slopeValue)

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Function: "ArraySlopeAdvanced" is similar to "ArraySlope" but it has some advanced features.

Parameter: "slopeStartPercentage" will control the start of slope. This can be useful if you want to boost only the bass in spectrogram array.
Parameter: "slopeValue" above 1 will boost start of the array and values below 1 will decrease values at start.
Parameter: "tension" can be used with spectrogram array to boost lows without affecting mids and highs that much...


void ArraySlopeAdvanced(float[] in, int arraySize, float slopeValue, float slopeStartPercentage, float tension)

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Function: "SetEnvSmoothProperties" Call to set smoothing coefficients. After setting the coefficients you can call "EnvSmooth" to get the smoothed value.

Parameter: "attackMS" Sets attack in milliseconds.
Parameter: "releaseMS" Sets release in milliseconds.
Parameter: "expectedFps" You should put FPS that you want to use in video render, or you can use "int fps = clamp(App.FpsCounter, 15, 60);" to get current ZGE FPS.


inline void SetEnvSmoothProperties(float attackMS, float releaseMS, float expectedFps)

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Function: "SetLPSmoothProperties" Call to set smoothing coefficients. After setting the coefficients you can call "LPSmooth" to get the smoothed value.

Parameter: "smoothMS" Sets smooth in milliseconds. This is same as setting "attackMS" and "releaseMS" to the same value, but it is more efficient.
Parameter: "expectedFps" You should put FPS that you want to use in video render, or you can use "int fps = clamp(App.FpsCounter, 15, 60);" to get current ZGE FPS.


inline void SetLPSmoothProperties(float smoothMS, float expectedFps)

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Function: "EnvSmooth" Uses coefficients calculated with "SetEnvSmoothProperties".

Parameter: "lastOut" You need to collect last returned value and to supply it to the this parameter. The best way to do that is to have unique global variable that will be used for "lastOut" and that will collect value on "EnvSmooth" return.
Parameter: "in" Set input value here that you would like to be smoothed.


inline float EnvSmooth(float lastOut, float in)

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Function: "LPSmooth" Uses coefficients calculated with "SetLPSmoothProperties".

Parameter: "lastOut" You need to collect last returned value and to supply it to the this parameter. The best way to do that is to have unique global variable that will be used for "lastOut" and that will collect value on "EnvSmooth" return.
Parameter: "in" Set input value here that you would like to be smoothed.


inline float LPSmooth(float lastOut, float in)

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Function: "VercticalSmooth" smooths every value in the array individually.

Parameter: "in" Set input array here that you would like to be smoothed.
Parameter: "uniqueBuffer" It should be the same size as "in" array and it should be global and you must not alter it's values.
Parameter: "arraySize" Size of the array. It can be smaller than actual size of the array.
Parameter: "attackMS" Sets attack in milliseconds.
Parameter: "releaseMS" Sets release in milliseconds.
Parameter: "expectedFps" You should put FPS that you want to use in video render, or you can use "int fps = clamp(App.FpsCounter, 15, 60);" to get current ZGE FPS.

Note: Array "in" and Array "uniqueBuffer" will hold the same data, but only modifying "in" is safe.


void VercticalSmooth(float[] in, float[] uniqueBuffer, int arraySize, float attackMS, float releaseMS, float expectedFps)

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Function: "AsymmetricalVercticalSmooth" It is similar to "VercticalSmooth" but the difference is that it can smooth array by setting coefficients for first and last array value separately and it will linearly interpolate coefficients in between. In other words, when you use this to smooth a spectrogram array, you can set different coefficients for bass and for highs. Since in spectrogram array bass is always less responsive, you can use more smoothing on highs to get uniform feel.

Parameter: "in" Set input array here that you would like to be smoothed.
Parameter: "uniqueBuffer" It should be the same size as "in" array and it should be global and you must not alter it's values.
Parameter: "arraySize" Size of the array. It can be smaller than actual size of the array.
Parameter: "attackStartMS" Sets attack in milliseconds for first value in array.
Parameter: "releaseStartMS" Sets release in milliseconds for first value in array.
Parameter: "attackEndMS" Sets attack in milliseconds for last i.e. (arraySize - 1) value in array.
Parameter: "releaseEndMS" Sets release in milliseconds for last i.e. (arraySize - 1) value in array.
Parameter: "expectedFps" You should put FPS that you want to use in video render, or you can use "int fps = clamp(App.FpsCounter, 15, 60);" to get current ZGE FPS.

Note: Array "in" and Array "uniqueBuffer" will hold the same data, but only modifying "in" is safe.


void AsymmetricalVercticalSmooth(float[] in, float[] uniqueBuffer, int arraySize, float attackStartMS, float releaseStartMS, float attackEndMS, float releaseEndMS, float expectedFps)

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Function: "HorizontalSmooth" will minimize differences between array values.

Parameter: "in" Set input array here that you would like to be smoothed.
Parameter: "arraySize" Size of the array. It can be smaller than actual size of the array.
Parameter: "smoothValue" will set the smoothness.
Parameter: "smoothEdges" will control if you want smoothness to be applied at the start and the end. If enabled it can create unnatural representation on start and end when using on spectrogram array for example.


void HorizontalSmooth(float[] in, int arraySize, float smoothValue, int smoothEdges)

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Function: "ResampleArray" will resample the array. This function can be useful if you want to convert 100 values array to 10 values array for example. It can also resample from 10 values to 100 etc.

Parameter: "in" Set input array.
Parameter: "out" Set output array. You will get your resampled array here.
Parameter: "inSize" Size of the input array. It can be smaller than actual size of the "in" array.
Parameter: "outSize" Size of the output array. It can be smaller or bigger than "in" array, but not bigger than size of the "out" array.
Parameter: "interpolateMethod" will control interpolation. This can be useful if you are upscaling the array, i.e. going from 10 values to 100 values array. When upscaling you can use interpolation to get better looking array.

Parameter Values: "interpolateMethod"

0 = nearest start
1 = nearest middle
2 = linear
3 = cosine
4 = cubic

Parameter: "innerProcessing" will control if you want to use all values to downsample the array. i.e. if inner processing is enabled resizing 100 values to 50 values array will use all 100 values to create that 50 values array. If not enabled, it will skip every second value. This can be useful if you want to downsample array and retain the best quality as possible.

Parameter Values: "innerProcessing"

0 = off
1 = max
2 = min
3 = average

Parameter: "genericInnerProcessing" This will control if you want to have faster inner processing. If set to 0 it will disable interpolation for inner processing and it will greatly improve the speed without sacrificing too much quality.


void ResampleArray(float[] in, float[] out, int inSize, int outSize, int interpolateMethod, int innerProcessing, int genericInnerProcessing)

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Function: "BassPump" This will multiply all values by the array values that you choose.

Parameter: "in" Set input array.
Parameter: "arraySize" Size of the array. It can be smaller than actual size of the array.
Parameter: "freq" (0->1) It will control how much of the beginning of the array will influence all values.
Parameter: "mix" (0->1) Sets how much to apply this effect.


void BassPump(float[] in, int arraySize, float freq, float mix)

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Function: "CopyArray" It will copy the buffer.

Parameter: "from" Set input array.
Parameter: "to" Set output array.
Parameter: "size" Size of the array. It can be smaller than actual size of the array.


void CopyArray(float[] from, float[] to, int size)

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Function: "max" It will return max value from v1 and v2.


inline float max(float v1, float v2)

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Function: "min" It will return min value from v1 and v2.


inline float min(float v1, float v2)

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

*/


// CODE -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


private inline float SlopeTension(int index, int arraySizeMinusOne, float tension)
{
   float floatIndex = index;
   float floatArraySizeMinusOne = arraySizeMinusOne;

   float pos;

   if (arraySizeMinusOne > 1)
      pos = floatIndex / floatArraySizeMinusOne;
   else return 0;

   return (1 - pow(1 - pos, tension)) * floatArraySizeMinusOne;
}

void ArraySlope(float[] in, int arraySize, float slopeValue)
{
   float slopeMultiplyer = (slopeValue - 1) / arraySize;

   for (int i = 0; i < arraySize; i++)
   {
      in[i] = in[i] * (slopeValue - (slopeMultiplyer * i));
   }
}

void ArraySlopeAdvanced(float[] in, int arraySize, float slopeValue, float slopeStartPercentage, float tension)
{
   int arrayStart = arraySize * (slopeStartPercentage / 100);
   int slopeArraySize = arraySize - arrayStart;

   float slopeMultiplyer = (slopeValue - 1) / arraySize;

   for (int i = 0; i < arraySize; i++)
   {
      int tensionIndex = SlopeTension(i - arrayStart < 0 ? 0 : i - arrayStart, slopeArraySize - 1, tension);

      in[i] = in[i] * (slopeValue - (slopeMultiplyer * tensionIndex));
   }
}

// Global variables used for smoothing

private float attackCoeff;
private float releaseCoeff;
private float smoothCoeff;

private inline float coeffCalc(float coeff, float expectedFps)
{
   if (coeff < 0.0000000001) return 0.0;
   return exp(-1.0 / (expectedFps * coeff * 0.001));
}

inline void SetEnvSmoothProperties(float attackMS, float releaseMS, float expectedFps)
{
   attackCoeff = coeffCalc(attackMS, expectedFps);
   releaseCoeff = coeffCalc(releaseMS, expectedFps);
}

inline void SetLPSmoothProperties(float smoothMS, float expectedFps)
{
   smoothCoeff = coeffCalc(smoothMS, expectedFps);
}

inline float EnvSmooth(float lastOut, float in)
{
   if (abs(in) > abs(lastOut))
      lastOut = attackCoeff * (lastOut - in) + in;
   else
      lastOut = releaseCoeff * (lastOut - in) + in;

   return lastOut;
}

inline float LPSmooth(float lastOut, float in)
{
   lastOut = smoothCoeff * (lastOut - in) + in;

   return lastOut;
}

void VercticalSmooth(float[] in, float[] uniqueBuffer, int arraySize, float attackMS, float releaseMS, float expectedFps)
{
   SetEnvSmoothProperties(attackMS, releaseMS, expectedFps);
   for (int i = 0; i < arraySize; i++)
   {
      uniqueBuffer[i] = EnvSmooth(uniqueBuffer[i], in[i]);
      in[i] = uniqueBuffer[i];
   }
}

void AsymmetricalVercticalSmooth(float[] in, float[] uniqueBuffer, int arraySize, float attackStartMS, float releaseStartMS, float attackEndMS, float releaseEndMS, float expectedFps)
{
   float attackSlopeAdd = 0;
   float releaseSlopeAdd = 0;
   float attack = attackStartMS;
   float release = releaseStartMS;

   if (attackEndMS - attackStartMS != 0)
   {
      attackSlopeAdd = (attackEndMS - attackStartMS) / arraySize;
   }
   if (releaseEndMS - releaseStartMS != 0)
   {
      releaseSlopeAdd = (releaseEndMS - releaseStartMS) / arraySize;
   }

   for (int i = 0; i < arraySize; i++)
   {
      SetEnvSmoothProperties(attack, release, expectedFps);
      uniqueBuffer[i] = EnvSmooth(uniqueBuffer[i], in[i]);
      in[i] = uniqueBuffer[i];

      attack = attack + attackSlopeAdd;
      release = release + releaseSlopeAdd;
   }
}

void HorizontalSmooth(float[] in, int arraySize, float smoothValue, int smoothEdges)
{
   float valueOut;

   if (smoothEdges == 0)
   {
      valueOut = in[0];
   }
   else
   {
      valueOut = 0;
   }

   SetLPSmoothProperties(smoothValue, arraySize);

   // Horizontal smoothing using LP filter
   for (int i = 0; i < arraySize; i++)
   {
      // From left to right
      valueOut = LPSmooth(valueOut, in[i]);
      in[i] = valueOut;
   }

   if (smoothEdges == 0)
   {
      valueOut = in[arraySize - 1];
   }
   else
   {
      valueOut = 0;
   }

   for (int i = arraySize - 1; i >= 0; i--)
   {
      // From right to left
      valueOut = LPSmooth(valueOut, in[i]);
      in[i] = valueOut;
   }
}

inline float NearestMiddle(float y0, float y1, float x)
{
   if (x > 0.5) return y1;
   else return y0;
}

inline float Linear(float y0, float y1, float x)
{
   return(y0*(1 - x) + y1 * x);
}

inline float Cosine(float y0, float y1, float x)
{
   float mu2;

   mu2 = (1 - cos(x*3.14159265358979323846)) / 2;
   return(y0*(1 - mu2) + y1 * mu2);
}

inline float Cubic(float yminus1, float y0, float y1, float y2, float x)
{
   float a0, a1, a2, a3;
   float mu2;

   mu2 = x * x;
   a0 = y2 - y1 - yminus1 + y0;
   a1 = yminus1 - y0 - a0;
   a2 = y1 - yminus1;
   a3 = y0;

   return(a0*x*mu2 + a1 * mu2 + a2 * x + a3);
}

inline float max(float v1, float v2)
{
   return v1 >= v2 ? v1 : v2;
}

inline float min(float v1, float v2)
{
   return v1 <= v2 ? v1 : v2;
}

private inline float GetInterpolationPosition(float input)
{
   return input - floor(input);
}

private inline float GetValueFromIndex(float[] in, int index, int arraySizeMinusOne)
{
   return in[min(max(index, 0), arraySizeMinusOne)];
}

private inline float GetValue(float[] in, int index, float position, int arraySizeMinusOne, int interpolate, int processing)
{
   float  minusOne, zero, one, two;
   int minusOneIndex, zeroIndex, oneIndex, twoIndex;

   zeroIndex = index;
   oneIndex = index + 1;
   twoIndex = index + 2;
   minusOneIndex = index - 1;

   // Get values
   zero = GetValueFromIndex(in, zeroIndex, arraySizeMinusOne);
   one = GetValueFromIndex(in, oneIndex, arraySizeMinusOne);

   // Get these only if we need it
   if (interpolate > 3)
   {
      minusOne = GetValueFromIndex(in, minusOneIndex, arraySizeMinusOne);
      two = GetValueFromIndex(in, twoIndex, arraySizeMinusOne);
   }

   switch (interpolate)
   {
      // Two point interpolation
   case 0: return zero;
   case 1: return NearestMiddle(zero, one, position);
   case 2: return Linear(zero, one, position);
   case 3: return Cosine(zero, one, position);

      // Four points interpolation
   case 4: return Cubic(minusOne, zero, one, two, position);
   }
   return 0;
}

private inline float InterpolateValue(float[] in, float index, float increasement, int arraySizeMinusOne, int interpolate, int processing, int genericInnerProcessing)
{
   float minValue;
   float maxValue;
   float average;

   maxValue = 3.4e-38; // Set minimum float value
   minValue = 3.4e38; // Set maximum float value
   average = 0;

   if (processing != 0 && increasement >= 1)
   {
      float innerIndex = index;
      int intInnerIndex = index;
      int intNextIndex = index + increasement;

      float innerValues = intNextIndex - intInnerIndex;
      float increaseInnerIndexBy = increasement / innerValues;
      float increaseMultiplyer = 0;

      int loopSize = intNextIndex - intInnerIndex;

      // If last value in array
      if (loopSize == 0)
      {
         if (processing == 1) maxValue = GetValue(in, intInnerIndex, GetInterpolationPosition(innerIndex), arraySizeMinusOne, interpolate, processing);
         else if (processing == 2) minValue = GetValue(in, intInnerIndex, GetInterpolationPosition(innerIndex), arraySizeMinusOne, interpolate, processing);
         else if (processing == 3) average = GetValue(in, intInnerIndex, GetInterpolationPosition(innerIndex), arraySizeMinusOne, interpolate, processing);
      }
      else while (intInnerIndex < intNextIndex)
      {
         if (genericInnerProcessing == 1)
         {
            if (intInnerIndex <= arraySizeMinusOne)
            {
               if (processing == 1) maxValue = max(maxValue, in[intInnerIndex]);
               else if (processing == 2) minValue = min(minValue, in[intInnerIndex]);
               else if (processing == 3) average = average + in[intInnerIndex];
            }
         }
         else
         {
            if (processing == 1) maxValue = max(maxValue, GetValue(in, intInnerIndex, GetInterpolationPosition(innerIndex), arraySizeMinusOne, interpolate, processing));
            else if (processing == 2) minValue = min(minValue, GetValue(in, intInnerIndex, GetInterpolationPosition(innerIndex), arraySizeMinusOne, interpolate, processing));
            else if (processing == 3) average = average + GetValue(in, intInnerIndex, GetInterpolationPosition(innerIndex), arraySizeMinusOne, interpolate, processing);
         }
         increaseMultiplyer++;
         innerIndex = index + increaseInnerIndexBy * increaseMultiplyer;
         intInnerIndex = innerIndex;
      }

      if (processing == 1) return maxValue;
      else if (processing == 2) return minValue;
      else if (processing == 3) return average / max(loopSize, 1);
   }
   else
   {
      return GetValue(in, index, GetInterpolationPosition(index), arraySizeMinusOne, interpolate, processing);
   }
   return 0;
}

void ResampleArray(float[] in, float[] out, int inSize, int outSize, int interpolateMethod, int innerProcessing, int genericInnerProcessing)
{
   if (inSize == 0 || outSize == 0) return;

   int inSizeMinusOne = inSize - 1;
   int outSizeMinusOne = outSize - 1;

   float floatInSizeMinusOne = inSizeMinusOne;
   float floatOutSizeMinusOne = outSizeMinusOne;

   float increasement = floatInSizeMinusOne / floatOutSizeMinusOne;

   float index = 0;
   for (int i = 0; i < outSize; )
   {
      out[i] = InterpolateValue(in, index, increasement, inSizeMinusOne, interpolateMethod, innerProcessing, genericInnerProcessing);
      i++;
      index = increasement * i;
   }
}

void BassPump(float[] in, int arraySize, float freq, float mix)
{
   int bassFreqSize = arraySize * freq;
   float maxValue = abs(in[0]);

   for (int i = 0; i < bassFreqSize; i++)
   {
      maxValue = max(maxValue, abs(in[i]));
   }

   for (int i = 0; i < arraySize; i++)
   {
      in[i] = (in[i] * (1 - mix) + (in[i] * maxValue)*mix);
   }
}

void CopyArray(float[] from, float[] to, int size)
{
   for (int i = 0; i < size; i++)
   {
      to[i] = from[i];
   }
}]]>
      </Source>
    </ZLibrary>
    <ZLibrary>
      <Source>
<![CDATA[inline float MakeBipolar(float x)
{
	return x * 2.0 - 1.0;
}

inline float LinearInterpolation(float x0, float x1, float m)
{
	return x0 * (1.0 - m) + x1 * m;
}]]>
      </Source>
    </ZLibrary>
    <ZExternalLibrary ModuleName="ZGameEditor Visualizer">
      <Source>
<![CDATA[void ParamsNotifyChanged(xptr Handle,int Layer) { }
void ParamsChangeName(xptr Handle,int Layer, int Parameters, string NewName) { }
void ParamsWriteValueForLayer(xptr Handle, int Layer,int Param, float NewValue) { }
int ReadPrivateData(xptr Handle, xptr Data, int Size) { }
void WritePrivateData(xptr Handle, xptr Data, int Size) { }]]>
      </Source>
    </ZExternalLibrary>
  </OnLoaded>
  <Content>
    <Group>
      <Children>
        <Array Name="Parameters" SizeDim1="14" Persistent="255">
          <Values>
<![CDATA[78DA636060B0676068B0638082B3677CEC20F4195B28DB1E0D33E432E433000021BB09BE]]>
          </Values>
        </Array>
        <Array Name="SpecBandArray" SizeDim1="32">
          <Values>
<![CDATA[78DA6360808106FBB3677CEC1850018A180076460545]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Type @list1000: "Zoom", "Rotate", "Move", "Opacity"
Amount
Audio Gate
Freq. Start
Freq. End
Bass Boost
Attack
Release
Origin X
Origin Y
Move Angle
Size
Invert Path @checkbox
Preview Origin @checkbox]]>
          </StringValue>
        </Constant>
        <Constant Name="AuthorInfo" Type="2" StringValue="Youlean"/>
        <Variable Name="IsPlaying" Type="1"/>
        <ZLibrary Name="FL_Library">
          <Source>
<![CDATA[void OnHostMessage(int id, int index, int value)
{
  if(id==12)
  {
  IsPlaying=value;
  }

}]]>
          </Source>
        </ZLibrary>
        <Array Name="SpecBandArrayBassBossted" SizeDim1="32">
          <Values>
<![CDATA[78DA6360808106FBB3677CEC1850018A180076460545]]>
          </Values>
        </Array>
      </Children>
    </Group>
    <Material Name="CanvasMaterial" Light="0" Blend="1" ZBuffer="0" Shader="CanvasShader">
      <Textures>
        <MaterialTexture Name="FeedbackMaterialTexture" TexCoords="1"/>
      </Textures>
    </Material>
    <Shader Name="CanvasShader">
      <VertexShaderSource>
<![CDATA[void main()
{
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;

  gl_Position = vertex;
  gl_TexCoord[0] = gl_MultiTexCoord0;
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[uniform sampler2D tex1;

uniform float resX;
uniform float resY;
uniform float viewportX;
uniform float viewportY;

uniform float posX, posY, scale, rotate, opacity,
scaleOriginX, scaleOriginY, rotateOriginX, rotateOriginY, previewOrigin;

#define PI 3.141592

const float pointSize = 0.015;

vec2 RotatePoint(float cx,float cy,float angle,vec2 p)
{
	float s = sin(angle);
	float c = cos(angle);

	// translate point back to origin:
	p.x -= cx;
	p.y -= cy;

	// rotate point
	float xnew = p.x * c - p.y * s;
	float ynew = p.x * s + p.y * c;

	// translate point back:
	p.x = xnew + cx;
	p.y = ynew + cy;
	return p;
}

void main(void)
{
	if (scale < 0.000001)
	{
		gl_FragColor = vec4(0.0);
		return;
	}

	vec2 iResolution = vec2(resX,resY);
	vec2 uv = RotatePoint(resX * rotateOriginX, resY * rotateOriginY, rotate * PI, gl_FragCoord.xy-vec2(viewportX,viewportY)) / iResolution.xy - vec2(posX, posY);

	// Set scale
	if (scale < 0.000001)
	{
		uv = vec2(0.0, 0.0);
	}
	else
	{
		float scaleInverted = 1.0 / scale;
		uv.x = uv.x * scaleInverted + (1.0 - scaleInverted) * scaleOriginX;
		uv.y = uv.y * scaleInverted + (1.0 - scaleInverted) * scaleOriginY;
	}

	vec4 color = texture2D(tex1, uv);

	// Draw origin preview
	if (previewOrigin > 0.5)
	{
		float scaleInverted = 999999.0;
		if (scale > 0.000001) scaleInverted = 1.0 / scale;

		// Fix ascpect ratio
		float XPointSize = pointSize * (resY / resX) * scaleInverted;
		float YPointSize = pointSize * scaleInverted;

		float uvxs = uv.x - (scaleOriginX - XPointSize);
		float uvys = uv.y - (scaleOriginY - YPointSize);

		if (uvxs >= 0.0 && uvxs < XPointSize * 2.0 && uvys >= 0.0 && uvys < YPointSize * 2.0)
		{
			color = vec4(1.0, 0.0, 0.0, 1.0);
		}
	}

	if (uv.x < 0.0 || uv.y < 0.0 || uv.x > 1.0 || uv.y > 1.0)
	{
		gl_FragColor = vec4(0.0);
		return;
	}

	color.a *= opacity;
	gl_FragColor = color;
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="resX" ValuePropRef="App.ViewportWidth"/>
        <ShaderVariable VariableName="resY" ValuePropRef="App.ViewportHeight"/>
        <ShaderVariable VariableName="viewportX" Value="262" ValuePropRef="App.ViewportX"/>
        <ShaderVariable VariableName="viewportY" Value="262" ValuePropRef="App.ViewportY"/>
        <ShaderVariable VariableName="posX" VariableRef="PosX"/>
        <ShaderVariable VariableName="posY" VariableRef="PosY"/>
        <ShaderVariable VariableName="scale" Value="1" VariableRef="Scale"/>
        <ShaderVariable VariableName="rotate" Value="0.5" VariableRef="Rotate"/>
        <ShaderVariable VariableName="scaleOriginX" VariableRef="ScaleOriginX"/>
        <ShaderVariable VariableName="scaleOriginY" VariableRef="ScaleOriginY"/>
        <ShaderVariable VariableName="rotateOriginX" VariableRef="RotateOriginX"/>
        <ShaderVariable VariableName="rotateOriginY" VariableRef="RotateOriginY"/>
        <ShaderVariable VariableName="opacity" Value="1" VariableRef="Opacity"/>
        <ShaderVariable VariableName="previewOrigin" VariableRef="PreviewOrigin"/>
      </UniformVariables>
    </Shader>
    <Model Name="Canvas">
      <OnRender>
        <ZExpression>
          <Expression>
<![CDATA[fps = clamp(App.FpsCounter, 15, 60);

int typeParam = floor(Parameters[0] * 1000);
float amountParam = pow(Parameters[1], 1.5);
float thresholdParam = Parameters[2];
float freqStartParam = Parameters[3];
float freqEndParam = Parameters[4];
float bassBosstParam = Parameters[5] * 5 + 1;
float attackParam = Pow(Parameters[6], 2) * 500;
float releaseParam = Pow(Parameters[7], 2) * 500;
float originXParam = Parameters[8];
float originYParam = Parameters[9];
float moveAngleParam = MakeBipolar(Parameters[10]);
float sizeParam = MakeBipolar(Parameters[11]) * 4;
float invertPathParam = Parameters[12];
float previewOriginParam = Parameters[13];

PreviewOrigin = previewOriginParam;

int specSize = specBandArray.SizeDim1 / 2;
int specStartIndex = freqStartParam * (specSize - 1);
int specEndIndex = freqEndParam * (specSize - 1);

float signal = 0;
float avg = 0;

SpecBandArrayBassBossted.SizeDim1 = specBandArray.SizeDim1;
CopyArray(specBandArray, SpecBandArrayBassBossted, specBandArray.SizeDim1);

// Bass Boost
ArraySlopeAdvanced(SpecBandArrayBassBossted, SpecBandArrayBassBossted.SizeDim1, bassBosstParam, 10, 8);

for (int i = specStartIndex; i <= specEndIndex; i++)
{
	float absValue = abs(SpecBandArrayBassBossted[i]) / bassBosstParam;

	if (absValue >= thresholdParam)
	{
		signal += absValue;
		avg++;
	}
}

if (avg > 0) signal /= avg;
else signal = 0;

ScaleOriginX = originXParam;
ScaleOriginY = originYParam;
RotateOriginX = originXParam;
RotateOriginY = originYParam;

SetEnvSmoothProperties(attackParam, releaseParam, fps);

// Scale
if (typeParam == 0)
{
	if (invertPathParam > 0.5)
	{
		signal *= -0.75;
	}

	lastSignal = EnvSmooth(lastSignal, signal * amountParam);

	Scale = 1.0 + lastSignal;

	PosX = 0.0;
	PosY = 0.0;
	Rotate = 0;
	Opacity = 1.0;
}
// Rotate
else if (typeParam == 1)
{
	if (invertPathParam > 0.5)
	{
		signal *= -1.0;
	}

	lastSignal = EnvSmooth(lastSignal, signal * (amountParam / 2));

	Rotate = lastSignal;

	PosX = 0.0;
	PosY = 0.0;
	Scale = 1.0;
	Opacity = 1.0;
}

// Move horizontally
else if (typeParam == 2)
{
	if (invertPathParam > 0.5)
	{
		signal *= -1.0;
	}

	float aX = sin(moveAngleParam * PI);
	float aY = cos(moveAngleParam * PI);

	lastSignal = EnvSmooth(lastSignal, signal * amountParam);

	PosX = lastSignal * aX;
	PosY = lastSignal * aY;

	Rotate = 0;
	Scale = 1.0;
	Opacity = 1.0;
}

// Change opacity
else if (typeParam == 3)
{
	if (invertPathParam > 0.5)
	{
  lastSignal = EnvSmooth(lastSignal, 1.0 - (signal * amountParam));
	}
  else
  {
    lastSignal = EnvSmooth(lastSignal, signal * amountParam);
  }


	Opacity = lastSignal;

	PosX = 0.0;
	PosY = 0.0;
	Rotate = 0;
	Scale = 1.0;
}

if (sizeParam > 0) Scale *= 1.0 + sizeParam;
else Scale *= 1.0 / (1.0 + sizeParam * -1.0);]]>
          </Expression>
        </ZExpression>
        <UseMaterial Material="CanvasMaterial"/>
        <RenderSprite/>
      </OnRender>
    </Model>
    <Variable Name="PosX"/>
    <Variable Name="PosY"/>
    <Variable Name="Scale"/>
    <Variable Name="Rotate"/>
    <Variable Name="ScaleOriginX"/>
    <Variable Name="ScaleOriginY"/>
    <Variable Name="RotateOriginX"/>
    <Variable Name="RotateOriginY"/>
    <Variable Name="Opacity"/>
    <Variable Name="PreviewOrigin"/>
    <Variable Name="fps"/>
    <Variable Name="lastSignal"/>
  </Content>
</ZApplication>
