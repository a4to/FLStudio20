<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" FileVersion="2">
  <Comment>
<![CDATA[|Description:|
This effect uses the color of another layer to create a point cloud of
 particles.  Gray scale depth and  bump maps are optimal, but this
effect works well any well light RGB image, layer effect, or video.

|Tips:|

|Peformance:|
This is an effect that requires use a fair amount processing power.  To
avoid  performance slow down, limit use to one or two layers of the effect
or use the Low-Res Version.  If you are done with a point cloud layer -
disable it with automation.  The disable toggle on the top of all layers
can do a lot to improve performance by turning off unused layers that
eat up performance power.

|Rendering:|
The preview window may  look a diferent from the full screen render.
It is best to preview in full screen mode or in window that is about the
same size as your video export.  You may need to increase the point
Size.

|Use the Alpha parameter:|
Alpha is a useful parameter for createing point cloud effects.
Use Alpha fade outs to morph  several Point Clouds inputs layers with only
one instance of the point cloud effect. Alpha is also useful for combining
point clouds.

|Use Color Cycle on Depth Maps:|
Depth maps are grayscle Images that work very well with displacement effects.
The problen is thay don't have color.  The post process effect called
ColorCyclePalette can add color to your point cloud and even animate color by
cycling through gradients.


]]>
  </Comment>
  <OnLoaded>
    <ZExternalLibrary Comment="OpenGL 4.0 graphics" ModuleName="opengl32" DefinitionsFile="opengl.txt">
      <BeforeInitExp>
<![CDATA[if(ANDROID) {
  if(App.GLBase==0)
    this.ModuleName="libGLESv1_CM.so";
  else
    this.ModuleName="libGLESv2.so";
}]]>
      </BeforeInitExp>
    </ZExternalLibrary>
    <ZLibrary Comment="OpenGL Context">
      <Source>
<![CDATA[void OnGLContextChange(){
  @CallComponent(Component:initVBO);
}]]>
      </Source>
    </ZLibrary>
    <ZExpression Comment="Create Point Cloud Array">
      <Expression>
<![CDATA[size=1024.0;

pnts.SizeDim1=floor(size*size*2f);

int idx=-1;
for (float X=0; X<floor(size); X++){
  for(float Y=0; Y<floor(size); Y++){
    idx++;
    if(idx<pnts.SizeDim1)pnts[idx]=((X*1.0/size)-.5);
    idx++;
    if(idx<pnts.SizeDim1)pnts[idx]=((Y*1.0/size)-.5);
  }
}

if(MACOS)
  useVAO=0;  //VAO not supported on Mac with compatibilty context
else
  useVAO=1;]]>
      </Expression>
    </ZExpression>
    <ZExpression Name="InitVBO" Comment="Init VBO">
      <Expression>
<![CDATA[//Generate Buffer ID's
//VAO=0;
//VBO=0;

glEnable(GL_PROGRAM_POINT_SIZE);
if(useVAO)
  glGenVertexArrays(1, VAO);
glGenBuffers(1, VBO);


if(useVAO) {
  glBindVertexArray(VAO);
}
glBindBuffer(GL_ARRAY_BUFFER, VBO);
glBufferData(GL_ARRAY_BUFFER, pnts.SizeDim1*4, pnts, GL_STATIC_DRAW);
if(useVAO) {
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2*4, null);
  glEnableVertexAttribArray(0);
}

glBindBuffer(GL_ARRAY_BUFFER, 0);

if(useVAO)
  glBindVertexArray(0);]]>
      </Expression>
    </ZExpression>
  </OnLoaded>
  <OnUpdate>
    <ZExpression Comment="Update Parmeters">
      <Expression>
<![CDATA[uAlpha=abs(Parameters[0]-1.0);
PointCloudGrid.Scale.X=Parameters[1]*24.0+1.0;
PointCloudGrid.Scale.Y=Parameters[2]*24.0+1.0;
PointCloudGrid.Position.X=(Parameters[3]-.5)*20.0;
PointCloudGrid.Position.Y=(Parameters[4]-.5)*20.0;
PointCloudGrid.Position.Z=(Parameters[5]-.5)*20.0;
PointCloudGrid.Rotation.X=(Parameters[6]-.5)*2.0;
PointCloudGrid.Rotation.Y=(Parameters[7]-.5)*2.0;
uPointSize=Parameters[8]*8.0*App.ViewportWidth/1080.0;
uAmp=(Parameters[9]-.5)*16.0;
App.FOV=20+(Parameters[10])*160.0;
FeedbackMaterial.Blend=ceil(Parameters[11]+1);
BitmapMaterial.Blend=ceil(Parameters[11]+1);
if(Parameters[12]==0){
  App.CameraRotation.Y=0;
  App.CameraRotation.Y=0;
}

if(Parameters[12]>0.0){
  float X = PointCloudGrid.Position.X-App.CameraPosition.X;
  float Y = PointCloudGrid.Position.Y-App.CameraPosition.Y;
  float Z = PointCloudGrid.Position.Z-App.CameraPosition.Z;

  // Calculate camera angles

  App.CameraRotation.Y = (atan2(X,Z*-1)/PI/2)*Parameters[12];
  App.CameraRotation.X = (atan2(Y*-1,sqrt(X*X+Z*Z))/PI/2)*Parameters[12];
}
//Parameters[13] is on Model:PointCloud-->OnPender-->Condition expression to toggle Material
uMap=ceil(Parameters[14]*3f);]]>
      </Expression>
    </ZExpression>
    <SpawnModel Model="PointCloudGrid" SpawnStyle="1"/>
  </OnUpdate>
  <OnClose>
    <ZExpression>
      <Expression>
<![CDATA[if(useVAO)
  glDeleteVertexArrays(1, VAO);
glDeleteBuffers(1, VBO);
VAO=0;
VBO=0;]]>
      </Expression>
    </ZExpression>
  </OnClose>
  <Content>
    <Shader Name="PointCloud">
      <VertexShaderSource>
<![CDATA[uniform sampler2D tex1;
uniform float pointSize,amp,mapType;
varying vec2 coord;


void main(){
  int map=int(mapType);
  gl_PointSize = pointSize;
  coord = gl_Vertex.xy+.5;
  vec4 vertex = gl_Vertex;
  float vr,vg,vb;
  vr=texture2D(tex1,vertex.xy+.5).r;
  vg=texture2D(tex1,vertex.xy+.5).g;
  vb=texture2D(tex1,vertex.xy+.5).b;
  if(map==0)vertex.z =.333*(vr+vg+vb);
  if(map==1)vertex.z =(vr*.22+vg*.72+vb*.07);
  if(map==2)vertex.z =(max(max(vr,vg),vb)+min(min(vr,vg),vb))*.5;
  vertex.z*=amp;
  gl_Position = gl_ModelViewProjectionMatrix * vertex;
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[uniform sampler2D tex1;
uniform float alpha;
uniform float transparent;
varying vec2 coord;

void main(){
  vec4 c = texture2D(tex1, coord);
  c.a = transparent > 0.1 ? c.a * alpha : alpha;
  gl_FragColor = c;
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="pointSize" VariableRef="uPointSize"/>
        <ShaderVariable VariableName="amp" VariableRef="uAmp"/>
        <ShaderVariable VariableName="mapType" VariableRef="uMap"/>
        <ShaderVariable VariableName="alpha" VariableRef="uAlpha"/>
        <ShaderVariable VariableName="transparent" ValuePropRef="Parameters[15]"/>
      </UniformVariables>
    </Shader>
    <Model Name="PointCloudGrid">
      <OnRender>
        <Condition Expression="return Parameters[13]&lt;.5;">
          <OnTrue>
            <UseMaterial Material="FeedbackMaterial"/>
          </OnTrue>
          <OnFalse>
            <UseMaterial Material="BitmapMaterial"/>
          </OnFalse>
        </Condition>
        <ZExpression>
          <Expression>
<![CDATA[if(VBO<=0)return;


if(useVAO) {
  glBindVertexArray(VAO);
  glDrawArrays(GL_Points, 0, pnts.SizeDim1*.5);
  glBindVertexArray(0);
} else {
  glBindBuffer(GL_ARRAY_BUFFER, VBO);

  glEnableClientState(GL_VERTEX_ARRAY);
  glVertexPointer(2,GL_FLOAT,0,null);

  glDrawArrays(GL_Points, 0, pnts.SizeDim1*.5);

  glDisableClientState(GL_VERTEX_ARRAY);

  glBindBuffer(GL_ARRAY_BUFFER, 0);
}]]>
          </Expression>
        </ZExpression>
      </OnRender>
    </Model>
    <Group Comment="Vertex Buffer">
      <Children>
        <Array Name="pnts" Comment="the interlaced array of points on a plane" SizeDim1="524288"/>
        <Variable Name="size" Comment="the sqrt of the number of points "/>
        <Variable Name="VBO" Type="1"/>
        <Variable Name="VAO" Type="1"/>
        <Variable Name="useVAO" Type="1"/>
      </Children>
    </Group>
    <Group Comment="FL Studio">
      <Children>
        <Material Name="FeedbackMaterial" Blend="1" Shader="PointCloud">
          <Textures>
            <MaterialTexture Name="FeedbackMaterialTexture" TexCoords="1"/>
          </Textures>
        </Material>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Alpha
Scale X
Scale Y
Pos X
Pos Y
Pos Z
Rot X
Rot Y
Point Size
Amp
FOV
Alpha one @checkbox
Center Cloud
In Source @list: "Post Effect", "IMAGE SRC"
Grayscale @list: "Average", "luminosity",lightness
Transparent @checkbox
]]>
          </StringValue>
        </Constant>
        <Array Name="Parameters" SizeDim1="16" Persistent="255">
          <Values>
<![CDATA[789C6360606038FC75851D080399F6609CF0D4EE47F063BB394718EC21E20AF6D2BC0A76853DF718404003082B180A18AA7F2A290300AF351398]]>
          </Values>
        </Array>
        <Constant Name="AuthorInfo" Type="2" StringValue="StevenM"/>
      </Children>
    </Group>
    <Group Comment="Shader">
      <Children>
        <Variable Name="uPointSize"/>
        <Variable Name="uAmp"/>
        <Variable Name="uMap"/>
        <Variable Name="uAlpha"/>
      </Children>
    </Group>
    <Group Comment="Cameras">
      <Children>
        <Camera Name="FreeCamera" ClipFar="1000"/>
        <Camera Name="TargetCamera" Position="0 0 10"/>
        <Variable Name="targetCamX"/>
        <Variable Name="targetCamY"/>
      </Children>
    </Group>
    <Bitmap Name="Bitmap" Width="512" Height="512">
      <Producers>
        <BitmapNoise/>
        <BitmapCells PointCount="128"/>
        <BitmapCombine/>
      </Producers>
    </Bitmap>
    <Material Name="BitmapMaterial" Blend="1" Shader="PointCloud">
      <Textures>
        <MaterialTexture Texture="Bitmap"/>
      </Textures>
    </Material>
  </Content>
</ZApplication>
