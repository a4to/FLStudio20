<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" FileVersion="2">
  <OnLoaded>
    <ZLibrary>
      <Source>
<![CDATA[inline float angle(float X)
{
  if(X >= 0 && X < 360)return X;
  if(X > 360)return X-floor(X/360)* 360;
  if(X <   0)return X+floor(X/360)*-360;
}

vec3 hsv(float H, float S, float V)
{
  vec3 Color;
  float R,G,B,I,F,P,Q,T;

  H = angle(H);
  S = clamp(S,0,100);
  V = clamp(V,0,100);

  H /= 60;
  S /= 100;
  V /= 100;

  if(S == 0)
  {
    Color[0] = V;
    Color[1] = V;
    Color[2] = V;
  }
  else
  {
    I = floor(H);
    F = H-I;

    P = V*(1-S);
    Q = V*(1-S*F);
    T = V*(1-S*(1-F));

    if(I == 0){R = V; G = T; B = P;}
    if(I == 1){R = Q; G = V; B = P;}
    if(I == 2){R = P; G = V; B = T;}
    if(I == 3){R = P; G = Q; B = V;}
    if(I == 4){R = T; G = P; B = V;}
    if(I == 5){R = V; G = P; B = Q;}

    Color[0] = R;
    Color[1] = G;
    Color[2] = B;
  }
  return Color;
}]]>
      </Source>
    </ZLibrary>
  </OnLoaded>
  <OnUpdate>
    <ZExpression>
      <Expression>
<![CDATA[uResolution=vector2(app.ViewportWidth,app.ViewportHeight);
uViewport=vector2(app.ViewportX,app.ViewportY);
uAlpha =1.0-Parameters[0];

float speed=(Parameters[4]-0.5)*4.0;
float delta=app.DeltaTime*Speed; //comment to use other time options
uTime+=delta;

uColorHSV = vector3(Parameters[1]*360,Parameters[2],1-Parameters[3]);
uDate = vector4(app.Time,app.Time,app.Time,uTime);]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <OnRender>
    <UseMaterial Material="mCanvas"/>
    <RenderSprite/>
  </OnRender>
  <Content>
    <Shader Name="MainShader">
      <VertexShaderSource>
<![CDATA[#version 120

void main(){
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[#version 120

uniform vec2 iResolution,iViewport;
uniform vec4 iDate;

uniform float iAlpha;
uniform vec3 iColorHSV;

float Truncate(float val) { return clamp(val,0.0,1.0); }

vec3 TransformHSV(vec3 c, float H, float S, float V) {
  float M_PI = 3.1415926;
	float VSU = V * S * cos(H * M_PI / 180.0);
	float VSW = V * S * sin(H * M_PI / 180.0);
  vec3 ret;
	ret.r = Truncate((0.299 * V + 0.701 * VSU + 0.168 * VSW) * c.r
		+ (0.587 * V - 0.587*VSU + 0.330 * VSW) * c.g
		+ (0.114 * V - 0.114*VSU - 0.497*VSW) * c.b);
	ret.g = Truncate((0.299 * V - .299 * VSU - 0.328 * VSW) * c.r
		+ (0.587 * V + 0.413 * VSU + 0.035 * VSW) * c.g
		+ (0.114 * V - 0.114 * VSU + 0.292 * VSW) * c.b);
	ret.b = Truncate((0.299 * V - 0.3 * VSU + 1.25 * VSW) * c.r
		+ (0.587 * V - 0.588 * VSU - 1.05 * VSW) * c.g
		+ (0.114 * V + 0.886 * VSU - 0.203 * VSW) * c.b);
	return ret;
} //


/*

	Fast, Minimal Animated Blocks
	-----------------------------

	Emulating the Voronoi triangle metric - the one that looks like blocks - with wrappable,
	rotated tiles.

	I've slimmed the code down a little bit, but mainly to get the point across that this
	approach requires far fewer instructions than the regular, random-offset-point grid setup.
	The main purpose of the exercise was efficiency... and to a lesser degree, novelty. :)

	For an instruction count comparison, take a look at a few of the Voronoi block - or
	standard - Voronoi examples on here.

	Relevant Examples:

	// About as minimal as it gets. Whoever made this had way too much time on his hands. :D
	One Tweet Cellular Pattern - Shane
	https://www.shadertoy.com/view/MdKXDD

    // Awesome. Also good for an instruction count comparison.
	Blocks -IQ
    https://www.shadertoy.com/view/lsSGRc

    // Similar code size, but very different instruction count.
    Triangular Voronoi Lighted - Aiekick
	https://www.shadertoy.com/view/ltK3WD

*/

// Distance metric. A slightly rounded triangle is being used, but the usual distance
// metrics all work.
float s(vec2 p){

    p = fract(p) - .5;
    //return max(abs(p.x)*.866 + p.y*.5, -p.y);
    //return (length(p)*1.5 + .25)*max(abs(p.x)*.866 + p.y*.5, -p.y);
    return (dot(p, p)*2. + .5)*max(abs(p.x)*.866 + p.y*.5, -p.y);
    //return dot(p, p)*2.;
    //return length(p);
    //return max(abs(p.x), abs(p.y)); // Etc.

    //return max(max(abs(p.x)*.866 + p.y*.5, -p.y), -max(abs(p.x)*.866 - p.y*.5, p.y) + .2);
}

// Very cheap wrappable cellular tiles. This one produces a block pattern on account of the
// metric used, but other metrics will produce the usual patterns.
//
// Construction is pretty simple: Plot two points in a wrappble cell and record their distance.
// Rotate by a third of a circle then repeat ad infinitum. Unbelievably, just one rotation
// is needed for a random looking pattern. Amazing... to me anyway. :)
//
// Note that there are no random points at all, no loops, and virtually no setup, yet the
// pattern appears random anyway.
float m(vec2 p){

    // Offset - used for animation. Put in as an afterthough, so probably needs more
    // tweaking, but it works well enough for the purpose of this demonstration.
    vec2 o = sin(vec2(1.93, 0) + iDate.w)*.166;

    // The distance to two wrappable, moving points.
    float a = s(p + vec2(o.x, 0)), b = s(p + vec2(0, .5 + o.y));

    // Rotate the layer (coordinates) by 120 degrees.
    p = -mat2(.5, -.866, .866, .5)*(p + .5);
    // The distance to another two wrappable, moving points.
    float c = s(p + vec2(o.x, 0)), d = s(p + vec2(0, .5 + o.y));

    // Return the minimum distance among the four points. If adding the points below,
    // be sure to comment the following line out.
    return min(min(a, b), min(c, d))*2.;


    // One more iteration. Gives an even more random pattern.
    // With this, it's still a very cheap process.
	/*
    // Rotate the layer (coordinates) by 120 degrees.
    p = -mat2(.5, -.866, .866, .5)*(p + .5);
    // The distance to yet another two wrappable, moving points.
    float e = s(p + vec2(o.x, 0)), f = s(p + vec2(0, .5 + o.y));

    // Return the minimum distance among the six points.
    return min(min(min(a, b), min(c, d)),  min(e, f))*2.;
    */

}

void mainImage(out vec4 o, vec2 p){

    // Screen coordinates.
    p /= iResolution.y/3.;

    // The function value.
    o = vec4(1)*m(p);

    // Cheap highlighting.
    vec4 b = vec4(.8, .5, 1, 0)*max(o - m(p + .02), 0.)/.1;

    // Colorize, add the highlighting and do some rough gamma correction.
    o = sqrt(pow(vec4(1.5, 1, 1, 0)*o, vec4(1, 3.5, 16, 0))  + b*b*(.5 + b*b));
     o.rgb = TransformHSV(clamp(o.rgb, 0., 1.), iColorHSV[0], iColorHSV[1], iColorHSV[2]);
     o.a = iAlpha;
}

void main(){
    //ZGE does not use mainImage( out vec4 fragColor, in vec2 fragCoord )
    //Rededefined fragCoord as gl_FragCoord.xy-iViewport(dynamic window)
    mainImage(gl_FragColor,gl_FragCoord.xy-iViewport);
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="iResolution" VariableRef="uResolution"/>
        <ShaderVariable VariableName="iViewport" VariableRef="uViewport"/>
        <ShaderVariable VariableName="iAlpha" VariableRef="uAlpha"/>
        <ShaderVariable VariableName="iColorHSV" VariableRef="uColorHSV"/>
        <ShaderVariable VariableName="iDate" VariableRef="uDate"/>
      </UniformVariables>
    </Shader>
    <Group Comment="Default ShaderToy Uniform Variable Inputs">
      <Children>
        <Variable Name="uResolution" Type="6"/>
        <Variable Name="uTime"/>
        <Variable Name="uViewport" Type="6"/>
      </Children>
    </Group>
    <Group Comment="FL Studio Variables">
      <Children>
        <Array Name="Parameters" SizeDim1="5" Persistent="255">
          <Values>
<![CDATA[789C63608081067B10396BA6A43D000BEF024B]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Alpha
Hue
Saturation
Lightness
Speed]]>
          </StringValue>
        </Constant>
        <Constant Name="AuthorInfo" Type="2">
          <StringValue>
<![CDATA[Shane (converted by Youlean & StevenM)
https://www.shadertoy.com/view/MlVXzd
]]>
          </StringValue>
        </Constant>
      </Children>
    </Group>
    <Group Comment="Unique Uniform Variable Inputs">
      <Children>
        <Variable Name="uAlpha"/>
        <Variable Name="uColorHSV" Type="7"/>
        <Variable Name="uDate" Type="8"/>
      </Children>
    </Group>
    <Group Comment="Materials and Textures">
      <Children>
        <Material Name="mCanvas" Blend="1" Shader="MainShader"/>
      </Children>
    </Group>
  </Content>
</ZApplication>
