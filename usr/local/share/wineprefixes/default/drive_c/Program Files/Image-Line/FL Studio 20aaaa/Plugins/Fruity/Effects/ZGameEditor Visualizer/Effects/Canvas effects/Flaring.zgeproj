<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" FileVersion="2">
  <OnLoaded>
    <SpawnModel Model="ScreenModel"/>
    <ZLibrary Comment="HSV Library">
      <Source>
<![CDATA[vec3 hsv(float h, float s, float v)
{
  s = clamp(s/100, 0, 1);
  v = clamp(v/100, 0, 1);

  if(!s)return vector3(v, v, v);

  h = h < 0 ? frac(1-abs(frac(h/360)))*6 : frac(h/360)*6;

  float c, f, p, q, t;

  c = floor(h);
  f = h-c;

  p = v*(1-s);
  q = v*(1-s*f);
  t = v*(1-s*(1-f));

  switch(c)
  {
    case 0: return vector3(v, t, p);
    case 1: return vector3(q, v, p);
    case 2: return vector3(p, v, t);
    case 3: return vector3(p, q, v);
    case 4: return vector3(t, p, v);
    case 5: return vector3(v, p, q);
  }
}

float lerp(float target,float value){
  float mul=clamp(abs(target-value),.1,900.0);
    if(abs(target-value)>app.DeltaTime*mul)
      {
      if (value-target>0.0)value-=app.DeltaTime*mul*.5;
      if (value-target<0.0)value+=app.DeltaTime*mul*.5;
      }
  else value=target;
  return value;
}

float lerpMed(float target,float value){
  float mul=clamp(abs(target-value),.2,900.0);
    if(abs(target-value)>app.DeltaTime*mul*2.0)
      {
      if (value-target>0.0)value-=app.DeltaTime*mul*1.5;
      if (value-target<0.0)value+=app.DeltaTime*mul*1.5;
      }
  else value=target;
  return value;
}

float lerpSlow(float target,float value){
  float mul=clamp(abs(target-value),.4,900.0);
    if(abs(target-value)>app.DeltaTime*mul*parameters[7])
      {
      if (value-target>0.0)value-=app.DeltaTime*mul*.5*parameters[7];
      if (value-target<0.0)value+=app.DeltaTime*mul*.5*parameters[7];
      }
  else value=target;
  return value;
}]]>
      </Source>
    </ZLibrary>
    <ZExpression>
      <Expression>
<![CDATA[//StevenM: init target interpolation Values for parameter slidetBrightness[0]=1.0;
uBrightness=targetBrightness[0];
uRayBrightness=targetRayBrightness[0];
uGamma=targetGamma[0];
uSpot=targetSpot[0];
uRayDensity=targetRayDensity[0];
uCurvature=targetCurvature[0];
uRed=targetRed[0];
uGreen=targetGreen[0];
uBlue=targetBlue[0];
uSinFreq=targetSinFreq[0];]]>
      </Expression>
    </ZExpression>
  </OnLoaded>
  <OnUpdate>
    <ZExpression>
      <Expression>
<![CDATA[int presetNum;

//Steven use a random multplier every 300 frames or on toggle randomize
if(Parameters[8]){
  if(frame==0){
    for(int i=0; i<10; i++)rand[i]=rnd()*2.0;
    for(int i=10; i<20; i++)rand[i]=rnd()*5.0;
    }
  frame++;
  if(frame>900*(rnd()+.25))frame=0;
}
else for(int i=0; i<11; i++)rand[i]=1.0;

const int
  ALPHA = 0,
  HUE = 1,
  SATURATION = 2,
  LIGHTNESS = 3;

vec3 c=hsv(Parameters[HUE]*360,Parameters[SATURATION]*100,(1-Parameters[LIGHTNESS])*100);

ShaderColor.r=c.r*4;
ShaderColor.g=c.g*4;
ShaderColor.b=c.b*4;

uSize=5.0-(clamp(Parameters[4],.05,1.0)*4.0);
uPos=vector2((1.0-Parameters[5])-.5,Parameters[6]-.5);

//StevenM Fix Speed Automation
float delta=app.DeltaTime*(Parameters[7]*.5);
dynamicTime+=delta;

//StevenM Interpolate shader presets


if(!Parameters[8])presetNum=clamp(round(Parameters[9]*5), 0, 4);
if(!Parameters[8])uNoiseType=targetNoiseType[presetNum];
else uNoiseType=0.0;

//StevenM Interpolation of uniforms
if(Parameters[8])presetNum=floor(rand[10]);
uBrightness=lerp(targetBrightness[presetNum]*rand[0],uBrightness);
if(Parameters[8])presetNum=floor(rand[11]);
uRayBrightness=lerp(targetRayBrightness[presetNum]*rand[1],uRayBrightness);
if(Parameters[8])presetNum=floor(rand[12]);
uGamma=lerp(targetGamma[presetNum]*rand[2],uGamma);
if(Parameters[8])presetNum=floor(rand[13]);
uSpot=lerp(targetSpot[presetNum]*rand[3],uSpot);
if(Parameters[8])presetNum=floor(rand[14]);
uRayDensity=lerpSlow(targetRayDensity[presetNum]*rand[4],uRayDensity);
if(Parameters[8])presetNum=floor(rand[15]);
uCurvature= lerp(targetCurvature[presetNum]*rand[5],uCurvature);
if(Parameters[8])presetNum=floor(rand[16]);
uRed=lerpMed(targetRed[presetNum]*rand[6],uRed);
if(Parameters[8])presetNum=floor(rand[17]);
uGreen=lerpMed(targetGreen[presetNum]*rand[7],uGreen);
if(Parameters[8])presetNum=floor(rand[18]);
uBlue=lerpMed(targetBlue[presetNum]*rand[8],uBlue);
if(Parameters[8])presetNum=floor(rand[19]);
uSinFreq=lerpMed(targetSinFreq[presetNum]*rand[9],uSinFreq);
uThreshold=Parameters[10]*3.0001;]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <Content>
    <Group>
      <Children>
        <Array Name="Parameters" SizeDim1="11" Persistent="255">
          <Values>
<![CDATA[78DA63600081067B54CC00C11196760C4800007376044A]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Alpha
Hue
Saturation
Lightness
Size
Position X
Position Y
Speed
Randomize @checkbox
Presets @list: "Preset 1","Preset 2","Preset 3","Preset 4","Preset 5"
Threshold]]>
          </StringValue>
        </Constant>
        <Constant Name="AuthorInfo" Type="2">
          <StringValue>
<![CDATA[nimitz 
Twitter: @stormoid
Adapted from: https://www.shadertoy.com/view/lsSGzy]]>
          </StringValue>
        </Constant>
        <Group Comment="preset values">
          <Children>
            <Array Name="targetRayDensity" SizeDim1="5" Persistent="255">
              <Values>
<![CDATA[78DA636038E0C0C090E070F8AB07903E60CFC0A0E00800394B0541]]>
              </Values>
            </Array>
            <Array Name="targetSpot" SizeDim1="5" Persistent="255">
              <Values>
<![CDATA[78DA63603860CFC050E0686C5C0CA41B1C66CDBC690F0030DE05D4]]>
              </Values>
            </Array>
            <Array Name="targetGamma" SizeDim1="5" Persistent="255">
              <Values>
<![CDATA[78DA636038E0C0C0C0E0C8C0D000A41780310022E103C2]]>
              </Values>
            </Array>
            <Array Name="targetBrightness" SizeDim1="5" Persistent="255">
              <Values>
<![CDATA[78DA63607070606038600FC10D40CCE0000024B9037E]]>
              </Values>
            </Array>
            <Array Name="targetRayBrightness" SizeDim1="5" Persistent="255">
              <Values>
<![CDATA[78DA636058E0C0C0A0E0C8C0B000880D40B40300210F0374]]>
              </Values>
            </Array>
            <Array Name="targetCurvature" SizeDim1="5" Persistent="255">
              <Values>
<![CDATA[78DA6360D8E2C4C050E0C8C0D0E178F6CC195B0686067B00355805D2]]>
              </Values>
            </Array>
            <Array Name="targetRed" SizeDim1="5" Persistent="255">
              <Values>
<![CDATA[78DA4B4B7B66CFC0D0E0306BA6A50303C303206EB00700483D063D]]>
              </Values>
            </Array>
            <Array Name="targetGreen" SizeDim1="5" Persistent="255">
              <Values>
<![CDATA[78DA63607070606068B0373636B64F4B5B660F643B0000282A0489]]>
              </Values>
            </Array>
            <Array Name="targetBlue" SizeDim1="5" Persistent="255">
              <Values>
<![CDATA[78DA636060B03F7BE68C2D03438203034303903DC701003C9306B6]]>
              </Values>
            </Array>
            <Array Name="targetSinFreq" SizeDim1="5" Persistent="255">
              <Values>
<![CDATA[78DA636038E0C0C0D000C4018E0C0C1E4E0C0C0B1C0025F703BC]]>
              </Values>
            </Array>
            <Array Name="targetNoiseType" SizeDim1="5" Persistent="255">
              <Values>
<![CDATA[78DA63608081067B1806000CC5023E]]>
              </Values>
            </Array>
            <Array Name="rand" SizeDim1="20"/>
          </Children>
        </Group>
        <Group Comment="Uniforms">
          <Children>
            <Variable Name="dynamicTime"/>
            <Variable Name="uPos" Type="6"/>
            <Variable Name="uSize"/>
            <Variable Name="uBrightness"/>
            <Variable Name="uRayBrightness"/>
            <Variable Name="uGamma"/>
            <Variable Name="uSpot"/>
            <Variable Name="uRayDensity"/>
            <Variable Name="uCurvature"/>
            <Variable Name="uNoiseType"/>
            <Variable Name="uRed"/>
            <Variable Name="uGreen"/>
            <Variable Name="uSinFreq"/>
            <Variable Name="uBlue"/>
            <Variable Name="uThreshold"/>
          </Children>
        </Group>
        <Variable Name="frame" Type="1"/>
      </Children>
    </Group>
    <Material Name="ScreenMaterial" Shading="1" Light="0" Blend="1" ZBuffer="0" Shader="ScreenShader">
      <Textures>
        <MaterialTexture Texture="NoiseBitmap" TextureWrapMode="1" TexCoords="1"/>
      </Textures>
    </Material>
    <Shader Name="ScreenShader">
      <VertexShaderSource>
<![CDATA[varying vec2 position;

void main(){
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
  position=vec2(vertex.x,vertex.y);
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[//Flaring by nimitz (twitter: @stormoid)
//Adapted from https://www.shadertoy.com/view/lsSGzy

uniform float iGlobalTime;
uniform float resX;
uniform float resY;
uniform float viewportX;
uniform float viewportY;
uniform vec2 pos;
uniform float size;
uniform float Alpha;
uniform sampler2D tex1;
vec2 iResolution = vec2(resX,resY);
uniform vec3 pColor;
uniform float threshold;


//StevenM : #define Presets Converted to menu list preset uniform varaibles
uniform float brightness;
uniform float curvature;
uniform float ray_brightness;
uniform float gamma;
uniform float spot_brightness;
uniform float ray_density;
uniform float pNoisetype;
uniform float red,green,blue;
uniform float sin_freq;

#define iChannel0 tex1
#define noisetype 2

//change this value (1 to 5) or tweak the settings yourself.
//the gamma and spot brightness parameters can use negative values
#define TYPE 4


#define YO_DAWG


//#define PROCEDURAL_NOISE
//#define YO_DAWG


float hash( float n ){return fract(sin(n)*43758.5453);}

float noise( in vec2 x )
{
	#ifdef PROCEDURAL_NOISE
	x *= 1.75;
    vec2 p = floor(x);
    vec2 f = fract(x);

    f = f*f*(3.0-2.0*f);

    float n = p.x + p.y*57.0;

    float res = mix(mix( hash(n+  0.0), hash(n+  1.0),f.x),
                    mix( hash(n+ 57.0), hash(n+ 58.0),f.x),f.y);
    return res;
	#else
	return texture2D(iChannel0, x*.01).x;
	#endif
}

mat2 m2 = mat2( 0.80,  0.60, -0.60,  0.80 );
float fbm( in vec2 p )
{
	float z=2.;
	float rz = 0.;
	p *= 0.25;
	for (float i= 1.;i < 6.;i++ )
	{
		if (pNoisetype == 0.0){
      rz+= abs((noise(p)-0.5)*2.)/z;
      }
    if (pNoisetype == 1.0){
      rz+=(sin(noise(p)*sin_freq)*0.5+0.5) /z;
      rz+= noise(p)/z;
      }
		z = z*2.;
		p = p*2.*m2;
	}
	return rz;
}

void main()
{
	float t = -iGlobalTime;
	vec2 uv = (gl_FragCoord.xy-vec2(viewportX,viewportY)) / iResolution.xy-0.5;
  uv=uv*size+pos*size;
  uv*= curvature*.05+0.0001;
  uv.x *= iResolution.x/iResolution.y;



	float r  = sqrt(dot(uv,uv));
	float x = dot(normalize(uv), vec2(.5,0.))+t;
	float y = dot(normalize(uv), vec2(.0,.5))+t;

	#ifdef YO_DAWG
	x = fbm(vec2(y*ray_density*0.5,r+x*ray_density*.2));
	y = fbm(vec2(r+y*ray_density*0.1,x*ray_density*.5));
	#endif

    float val;
    val = fbm(vec2(r+y*ray_density,r+x*ray_density-y));
	val = smoothstep(gamma*.02-.1,ray_brightness+(gamma*0.02-.1)+.001,val);
	val = sqrt(val);

	vec3 col = val/vec3(red,green,blue);
	col = clamp(1.-col,0.,1.);
	col = mix(col,vec3(1.),spot_brightness-r/0.1/curvature*200./brightness);
  col.r=(1.0-fract(col.r))*pColor.r*.5+col.r;
  col.g=(1.0-fract(col.g))*pColor.g*.5+col.g;
  col.b=(1.0-fract(col.b))*pColor.b*.5+col.b;
  col=clamp(col,vec3(0.0),vec3(1.0));
	gl_FragColor = vec4(col,(col.r+col.g+col.b<threshold ? 0.0:1.0)*Alpha);
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="iGlobalTime" VariableRef="dynamicTime"/>
        <ShaderVariable VariableName="resX" Value="1163" ValuePropRef="App.ViewportWidth"/>
        <ShaderVariable VariableName="resY" Value="432" ValuePropRef="App.ViewportHeight"/>
        <ShaderVariable VariableName="viewportX" Value="262" ValuePropRef="App.ViewportX"/>
        <ShaderVariable VariableName="viewportY" Value="262" ValuePropRef="App.ViewportY"/>
        <ShaderVariable VariableName="Alpha" ValuePropRef="1-Parameters[0];"/>
        <ShaderVariable VariableName="curvature" VariableRef="uCurvature"/>
        <ShaderVariable VariableName="pColor" VariableRef="ShaderColor"/>
        <ShaderVariable VariableName="pos" VariableRef="uPos"/>
        <ShaderVariable VariableName="size" VariableRef="uSize"/>
        <ShaderVariable VariableName="brightness" VariableRef="uBrightness"/>
        <ShaderVariable VariableName="ray_brightness" VariableRef="uRayBrightness"/>
        <ShaderVariable VariableName="gamma" VariableRef="uGamma"/>
        <ShaderVariable VariableName="spot_brightness" VariableRef="uSpot"/>
        <ShaderVariable VariableName="ray_density" VariableRef="uRayDensity"/>
        <ShaderVariable VariableName="pNoisetype" VariableRef="uNoiseType"/>
        <ShaderVariable VariableName="red" VariableRef="uRed"/>
        <ShaderVariable VariableName="green" VariableRef="uGreen"/>
        <ShaderVariable VariableName="blue" VariableRef="uBlue"/>
        <ShaderVariable VariableName="sin_freq" VariableRef="uSinFreq"/>
        <ShaderVariable VariableName="threshold" VariableRef="uThreshold"/>
      </UniformVariables>
    </Shader>
    <Model Name="ScreenModel">
      <OnRender>
        <UseMaterial Material="ScreenMaterial"/>
        <RenderSprite/>
      </OnRender>
    </Model>
    <Bitmap Name="NoiseBitmap" Comment="NoCustom" Width="256" Height="256">
      <Producers>
        <BitmapExpression>
          <Expression>
<![CDATA[//X,Y : current coordinate (0..1)
//Pixel : current color (rgb)
//Sample expression: Pixel.R=abs(sin(X*16));
float s=1.0*rnd();
Pixel.r=s;
Pixel.g=s;
Pixel.b=s;]]>
          </Expression>
        </BitmapExpression>
      </Producers>
    </Bitmap>
    <Variable Name="ShaderColor" Type="7"/>
  </Content>
</ZApplication>
