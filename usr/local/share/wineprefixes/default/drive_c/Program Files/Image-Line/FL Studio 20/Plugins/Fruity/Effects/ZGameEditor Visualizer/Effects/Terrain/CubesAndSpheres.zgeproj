<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" NoSound="1" FileVersion="2">
  <OnLoaded>
    <SpawnModel Model="ScreenModel"/>
    <ZExpression Expression="FFTmulti=SpecBandArray.SizeDim1/32;"/>
  </OnLoaded>
  <OnUpdate>
    <ZExpression>
      <Expression>
<![CDATA[AvLevel=0;
for (int b=0; b<32; b++) AvLevel+=SpecBandArray[b*FFTmulti];
if (AvLevelSmooth<AvLevel) AvLevelSmooth+=(AvLevel-AvLevelSmooth)*0.25;
AvLevelSmooth*=.95;]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <OnRender>
    <ZExpression>
      <Expression>
<![CDATA[Alpha.Value=Parameters[0];
Hue.Value=Parameters[1];
Saturation.Value=Parameters[2];
Lightness.Value=Parameters[3];
Size.Value=Parameters[4];
PositionX.Value=Parameters[5];
PositionY.Value=Parameters[6];
CameraYaw.Value=Parameters[7];
CameraPitch.Value=Parameters[8];
CameraDistance.Value=Parameters[9];
CubeSphereSize.Value=Parameters[10];
Object1Hue.Value=Parameters[11];
Object1Saturation.Value=Parameters[12];
Object1Lightness.Value=Parameters[13];
Object2Hue.Value=Parameters[14];
Object2Saturation.Value=Parameters[15];
Object2Lightness.Value=Parameters[16];
SoundInfluence.Value=Parameters[17];]]>
      </Expression>
    </ZExpression>
  </OnRender>
  <Content>
    <Group>
      <Children>
        <Array Name="Parameters" SizeDim1="18" Persistent="255">
          <Values>
<![CDATA[78DA6360C00AEC41382D4DCDBED096CBFEEC9933760C0C0DF6D8310834D80300251B0A83]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Alpha
Hue
Saturation
Lightness
Size
Position X
Position Y
Camera Yaw
Camera Pitch
Camera Distance
Object Size
Object 1 Hue
Object 1 Saturation
Object 1 Lightness
Object 2 Hue
Object 2 Saturation
Object 2 Lightness
Sound Influence]]>
          </StringValue>
        </Constant>
        <Array Name="SpecBandArray" SizeDim1="32" SizeDim2="2"/>
        <Array Name="AudioArray" SizeDim1="32"/>
        <Variable Name="AvLevel"/>
        <Variable Name="AvLevelSmooth"/>
        <Variable Name="FFTmulti" Type="1"/>
        <Constant Name="AuthorInfo" Type="2" StringValue="@paulofalcao"/>
      </Children>
    </Group>
    <Material Name="ScreenMaterial" Shading="1" Light="0" Blend="1" ZBuffer="0" Shader="ScreenShader"/>
    <Shader Name="ScreenShader">
      <VertexShaderSource>
<![CDATA[varying vec2 position;

void main(){
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
  position=vec2(vertex.x,vertex.y);
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[// Cubes and Spheres
//
// by @paulofalcao
//

varying vec2 position;
//uniform sampler1D audio;
//uniform sampler1D bands;
uniform float iGlobalTime;
uniform float resX;
uniform float resY;
uniform float viewportX;
uniform float viewportY;
uniform float Alpha;
//uniform float Hue;
uniform float Saturation;
uniform float Lightness;
uniform float Size;
uniform float PositionX;
uniform float PositionY;
uniform float CameraYaw;
uniform float CameraPitch;
uniform float CameraDistance;
uniform float CubeSphereSize;
uniform float Object1Hue;
uniform float Object1Saturation;
uniform float Object1Lightness;
uniform float Object2Hue;
uniform float Object2Saturation;
uniform float Object2Lightness;
uniform float SoundInfluence;
uniform float Avl;

vec2 resolution = vec2(resX,resY);
float time=iGlobalTime;

float Avlc=Avl*SoundInfluence;

//Util Start
float PI=3.14159265;

vec3 HSVtoRGB(float h,float s,float v){
  float c=v*s;
  float h1=h*6.0;
  float x=c*(1.0-abs(mod(h1,2.0)-1.0));
  float i=floor(h1);
  vec3 rgb;
  if      (i==0.0){rgb=vec3(c,x,0);}
  else if (i==1.0){rgb=vec3(x,c,0);}
  else if (i==2.0){rgb=vec3(0,c,x);}
  else if (i==3.0){rgb=vec3(0,x,c);}
  else if (i==4.0){rgb=vec3(x,0,c);}
  else            {rgb=vec3(c,0,x);}
  return rgb+(v-c);
}

vec2 sim2d(
  in vec2 p,
  in float s)
{
   vec2 ret=p;
   ret=p+s/2.0;
   ret=fract(ret/s)*s-s/2.0;
   return ret;
}

vec3 stepspace(
  in vec3 p,
  in float s)
{
  return p-mod(p-s/2.0,s);
}

vec3 phong(
  in vec3 pt,
  in vec3 prp,
  in vec3 normal,
  in vec3 light,
  in vec3 color,
  in float spec,
  in vec3 ambLight)
{
   vec3 lightv=normalize(light-pt);
   float diffuse=dot(normal,lightv);
   vec3 refl=-reflect(lightv,normal);
   vec3 viewv=normalize(prp-pt);
   float specular=pow(max(dot(refl,viewv),0.0),spec);
   return (max(diffuse,0.0)+ambLight)*color+specular;
}

//Util End

//Scene Start

vec2 obj(in vec3 p)
{
  vec3 fp=stepspace(p,2.0);;
  float d=sin(fp.x*0.3+time*4.0+Avlc)+cos(fp.z*0.3+time*2.0+Avlc)-Avlc;
  p.y=p.y+d;
  p.xz=sim2d(p.xz,2.0);
  float s=CubeSphereSize*0.9+0.1-Avlc*0.05;
  float c1=length(max(abs(p)-vec3(0.6,0.6,0.6)*s,0.0))-(0.35*s);
  float c2=length(p)-s;
  float cf=sin(time)*0.5+0.5;
  return vec2(mix(c1,c2,cf),1.0);
}

vec3 obj_c(vec3 p){
  vec2 fp=sim2d(p.xz-1.0,4.0);
  if (fp.y>0.0) fp.x=-fp.x;
  if (fp.x>0.0) return HSVtoRGB(Object1Hue,Object1Saturation,Object1Lightness);
    else return HSVtoRGB(Object2Hue,Object2Saturation,Object2Lightness);
}

//Scene End

float raymarching(
  in vec3 prp,
  in vec3 scp,
  in int maxite,
  in float precis,
  in float startf,
  in float maxd,
  out float objid)
{
  const vec3 e=vec3(0.1,0,0.0);
  vec2 s=vec2(startf,0.0);
  vec3 c,p,n;
  float f=startf;
  for(int i=0;i<256;i++){
    if (abs(s.x)<precis||f>maxd||i>maxite) break;
    f+=s.x;
    p=prp+scp*f;
    s=obj(p);
    objid=s.y;
  }
  if (f>maxd) objid=-1.0;
  return f;
}

vec3 camera(
  in vec3 prp,
  in vec3 vrp,
  in vec3 vuv,
  in float vpd,
  in vec2 pos
)
{
  vec2 vPos=-1.0+2.0*(gl_FragCoord.xy-vec2(viewportX,viewportY))/resolution.xy;
  vec3 vpn=normalize(vrp-prp);
  vec3 u=normalize(cross(vuv,vpn));
  vec3 v=cross(vpn,u);
  vec3 scrCoord=prp+vpn*vpd+(vPos.x+pos.x-0.5)*u*resolution.x/resolution.y+(vPos.y+pos.y-0.5)*v;
  return normalize(scrCoord-prp);
}

vec3 normal(in vec3 p)
{
  //tetrahedron normal
  const float n_er=0.01;
  float v1=obj(vec3(p.x+n_er,p.y-n_er,p.z-n_er)).x;
  float v2=obj(vec3(p.x-n_er,p.y-n_er,p.z+n_er)).x;
  float v3=obj(vec3(p.x-n_er,p.y+n_er,p.z-n_er)).x;
  float v4=obj(vec3(p.x+n_er,p.y+n_er,p.z+n_er)).x;
  return normalize(vec3(v4+v1-v3-v2,v3+v4-v1-v2,v2+v4-v3-v1));
}

vec4 render(
  in vec3 prp,
  in vec3 scp,
  in int maxite,
  in float precis,
  in float startf,
  in float maxd,
  in vec4 background,
  in vec3 light,
  in float spec,
  in vec3 ambLight,
  out vec3 n,
  out vec3 p,
  out float f,
  out float objid)
{
  objid=-1.0;
  f=raymarching(prp,scp,maxite,precis,startf,maxd,objid);
  if (objid>-0.5){
    p=prp+scp*f;
    vec3 c=obj_c(p);
    n=normal(p);
    vec3 cf=phong(p,prp,n,light,c,spec,ambLight);
    return vec4(cf,1.0);
  }
  f=maxd;
  return vec4(background); //background color
}




void main(void){

  //Camera animation
  vec3 vuv=vec3(0,1,0);
  vec3 vrp=vec3(time*4.0,0.0,0.0);
  float mx=CameraYaw*PI*2.0;
  float my=CameraPitch*PI/2.01;
  vec3 prp=vrp+vec3(cos(my)*cos(mx),sin(my),cos(my)*sin(mx))*CameraDistance*38.0; //Trackball style camera pos
  float vpd=Size*16.0+1.0;

  vec3 light=prp+vec3(5.0,0,5.0);

  vec3 scp=camera(prp,vrp,vuv,vpd,vec2(PositionX,PositionY));

  vec3 n,p;
  float f,o;
  const float maxe=0.01;
  const float startf=0.1;
  const vec4 backc=vec4(0.0,0.0,0.0,0.0);
  const float spec=8.0;
  const vec3 ambi=vec3(0.1,0.1,0.1);

  vec4 c1=render(prp,scp,256,maxe,startf,60.0,backc,light,spec,ambi,n,p,f,o);
  c1=c1*max(1.0-f*.015,0.0);
  vec4 c2=backc;
  if (o>0.5){
    scp=reflect(scp,n);
    c2=render(p+scp*0.05,scp,32,maxe,startf,10.0,backc,light,spec,ambi,n,p,f,o);
  }
  c2=c2*max(1.0-f*.1,0.0);
  vec3 c3=vec3(c1.xyz*0.75+c2.xyz*0.25);
  float greyc3=(c3.x+c3.y+c3.z)/3.0;
  gl_FragColor=vec4((c3*(1.0-Saturation)+greyc3*Saturation)*(1.0-Lightness),c1.w*(1.0-Alpha));
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable DesignDisable="1" VariableName="bands" ValueArrayRef="SpecBandArray"/>
        <ShaderVariable DesignDisable="1" VariableName="audio" ValueArrayRef="AudioArray"/>
        <ShaderVariable VariableName="iGlobalTime" ValuePropRef="App.Time"/>
        <ShaderVariable Name="Alpha" VariableName="Alpha"/>
        <ShaderVariable Name="Hue" DesignDisable="1" VariableName="Hue"/>
        <ShaderVariable Name="Saturation" VariableName="Saturation"/>
        <ShaderVariable Name="Lightness" VariableName="Lightness"/>
        <ShaderVariable Name="Size" VariableName="Size"/>
        <ShaderVariable Name="PositionX" VariableName="PositionX" Value="0.5"/>
        <ShaderVariable Name="PositionY" VariableName="PositionY" Value="0.5"/>
        <ShaderVariable Name="CameraYaw" VariableName="CameraYaw" Value="0.65"/>
        <ShaderVariable Name="CameraPitch" VariableName="CameraPitch" Value="0.54"/>
        <ShaderVariable Name="CameraDistance" VariableName="CameraDistance" Value="0.4"/>
        <ShaderVariable Name="CubeSphereSize" VariableName="CubeSphereSize" Value="1"/>
        <ShaderVariable Name="Object1Hue" VariableName="Object1Hue" Value="1"/>
        <ShaderVariable Name="Object1Saturation" VariableName="Object1Saturation" Value="1"/>
        <ShaderVariable Name="Object1Lightness" VariableName="Object1Lightness" Value="1"/>
        <ShaderVariable Name="Object2Hue" VariableName="Object2Hue" Value="1"/>
        <ShaderVariable Name="Object2Saturation" VariableName="Object2Saturation" Value="1"/>
        <ShaderVariable Name="Object2Lightness" VariableName="Object2Lightness"/>
        <ShaderVariable Name="SoundInfluence" VariableName="SoundInfluence" Value="1"/>
        <ShaderVariable VariableName="Avl" ValuePropRef="AvLevelSmooth"/>
        <ShaderVariable VariableName="resX" Value="1163" ValuePropRef="App.ViewportWidth"/>
        <ShaderVariable VariableName="resY" Value="432" ValuePropRef="App.ViewportHeight"/>
        <ShaderVariable VariableName="viewportX" Value="262" ValuePropRef="App.ViewportX"/>
        <ShaderVariable VariableName="viewportY" Value="262" ValuePropRef="App.ViewportY"/>
      </UniformVariables>
    </Shader>
    <Model Name="ScreenModel">
      <OnRender>
        <UseMaterial Material="ScreenMaterial"/>
        <RenderSprite/>
      </OnRender>
    </Model>
  </Content>
</ZApplication>
