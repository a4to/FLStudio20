FLhd   0 * ` FLdt�  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��$?  ﻿[General]
GlWindowMode=1
LayerCount=11
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,1,4,5,2,3,20,17,19,6,23
WizardParams=1139,1140,226,82

[AppSet0]
App=Background\SolidColor
FParamValues=0,0,0,0.84
ParamValues=0,0,0,840
ParamValuesF=0,0,0,0.84
Enabled=1
Collapsed=1

[AppSet1]
App=HUD\HUD Prefab
FParamValues=1,0.9861,0.2,0.804,0,0.5,0.5,0.326,1,1,4,0,0.5,1,0.368,0,1,1
ParamValues=1,986,200,804,0,500,500,326,1000,1000,4,0,500,1,368,0,1000,1
ParamValuesF=1,0.7133,0.2,0.804,0,0.5,0.5,0.326,1,1,4,0,0.5,1,0.368,0,1,1
Enabled=1
Collapsed=1
LayerPrivateData=78014B4A4CCE4E2FCA2FCD4B89490232750D0C0CF53273CA18460A0000F04C0845

[AppSet4]
App=HUD\HUD Prefab
FParamValues=4,0.0139,0.788,0.776,0,0.5,0.5,0.326,1,1,4,0,0.5,1,0.368,0,1,1
ParamValues=4,13,788,776,0,500,500,326,1000,1000,4,0,500,1,368,0,1000,1
ParamValuesF=4,0.2867,0.788,0.776,0,0.5,0.5,0.326,1,1,4,0,0.5,1,0.368,0,1,1
Enabled=1
Collapsed=1
LayerPrivateData=78014B4A4CCE4E2FCA2FCD4B89490232750D0C4CF43273CA18460A0000F3160848

[AppSet5]
App=Misc\Automator
FParamValues=1,2,2,2,0.5,0.029,0.25,1,5,2,2,0.5,0.029,0.75,0,3,5,2,0.5,0.071,0.487,0,0,0,0,0,0.25,0.25
ParamValues=1,2,2,2,500,29,250,1,5,2,2,500,29,750,0,3,5,2,500,71,487,0,0,0,0,0,250,250
ParamValuesF=1,2,2,2,0.5,0.029,0.25,1,5,2,2,0.5,0.029,0.75,0,3,5,2,0.5,0.071,0.487,0,0,0,0,0,0.25,0.25
Enabled=1
Collapsed=1

[AppSet2]
App=Postprocess\Point Cloud High
FParamValues=0,0.33,0.33,0.5323,0.4981,0.6205,0.444,0.503,0.33,1,0,0,0,0,2
ParamValues=0,330,330,532,498,620,444,503,330,1000,0,0,0,0,2
ParamValuesF=0,0.33,0.33,0.5121,0.4812,0.5529,0.444,0.503,0.33,1,0,0,0,0,2
Enabled=1
Collapsed=1

[AppSet3]
App=Misc\Automator
FParamValues=1,3,4,2,0.5,0.011,0.478,1,3,6,2,0.5,0.029,0.438,1,3,5,2,0.5,0.071,0.489,0,0,0,0,0,0.25,0.25
ParamValues=1,3,4,2,500,11,478,1,3,6,2,500,29,438,1,3,5,2,500,71,489,0,0,0,0,0,250,250
ParamValuesF=1,3,4,2,0.5,0.011,0.478,1,3,6,2,0.5,0.029,0.438,1,3,5,2,0.5,0.071,0.489,0,0,0,0,0,0.25,0.25
Enabled=1
Collapsed=1

[AppSet20]
App=Misc\Automator
FParamValues=1,19,17,2,0.5,0.17,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,19,17,2,500,170,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
ParamValuesF=1,19,17,2,0.5,0.17,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
Enabled=1
Collapsed=1

[AppSet17]
App=Postprocess\ParameterShake
FParamValues=1,0,15,16,1,0,16,16,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=1000,0,15,16,1000,0,16,16,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=1,0,15,16,1,0,16,16,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet19]
App=Postprocess\ParameterShake
FParamValues=0.5,0.5,18,16,1,0,18,12,8,3,1,0,6,6,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=500,500,18,16,1000,0,18,12,8,3,1,0,6,6,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=0.5,0.5,18,16,1,0,18,12,8,3,1,0,6,6,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet6]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.556,0,0.274,0.102,1,0,0,0
ParamValues=556,0,274,102,1000,0,0,0
ParamValuesF=0.556,0,0.274,0.102,1,0,0,0
Enabled=1
Collapsed=1

[AppSet23]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
ParamValuesF=0.5,0.5,0.5,0.5,0.5,0.5
Enabled=1
Collapsed=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""3""><position y=""3""><p align=""left""><font face=""American-Captain"" size=""10"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""3""><position y=""26""><p align=""left""><font face=""Chosence-Bold"" size=""8"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""0""><position y=""10""><p align=""right""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[extra1]</font></p></position>","<position x=""0""><position y=""3""><p align=""right""><font face=""Chosence-Bold"" size=""4"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""0""><position y=""20""><p align=""right""><font face=""Chosence-Bold"" size=""4 "" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""0""><position y=""25""><p align=""right""><font face=""Chosence-regular"" size=""3"" color=""#FFFFF"">[extra3]</font></p></position>",
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

