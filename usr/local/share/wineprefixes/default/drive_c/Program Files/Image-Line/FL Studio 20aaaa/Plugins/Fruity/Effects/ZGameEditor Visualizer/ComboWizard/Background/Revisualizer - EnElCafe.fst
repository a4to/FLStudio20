FLhd   0 * ` FLdt�  �20.6.2.1585 �1  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ե!  ﻿[General]
GlWindowMode=1
LayerCount=15
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=13,12,0,1,2,16,3,4,9,11,5,6,18,7,14

[AppSet13]
App=Background\SolidColor
FParamValues=0,0,0,0
ParamValues=0,0,0,0
Enabled=1

[AppSet12]
App=Postprocess\Vignette
FParamValues=0,0,0,0.6,0.04,0.699
ParamValues=0,0,0,600,40,699
Enabled=1
UseBufferOutput=1

[AppSet0]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1

[AppSet1]
App=Peak Effects\Polar
FParamValues=0,0.097,0,0,0.312,0.498,0.488,0.287,1,0.5,0.157,0.319,0,0.5,0,0.5,0,0.477,1,0,0,0,0,1
ParamValues=0,97,0,0,312,498,488,287,1000,500,157,319,0,500,0,500,0,477,1000,0,0,0,0,1
Enabled=1
ImageIndex=4

[AppSet2]
App=Peak Effects\Polar
FParamValues=0,0.097,1,0,0.312,0.498,0.496,0.287,1,0.5,0.157,0.319,0,0.5,0,0.5,0,0.456,0.064,0,0,0,1,1
ParamValues=0,97,1000,0,312,498,496,287,1000,500,157,319,0,500,0,500,0,456,64,0,0,0,1,1
Enabled=1
ImageIndex=4

[AppSet16]
App=HUD\HUD 3D
FParamValues=0,0,0.473,0.487,0.5,0.5,0.5,0.5,0.5,0.5,0.5025,1,1,0.6654,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,0
ParamValues=0,0,473,487,500,500,500,500,500,500,502,1000,1000,665,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,0
Enabled=1
ImageIndex=4

[AppSet3]
App=Misc\Automator
FParamValues=1,17,11,2,0.497,0.038,0.503,1,12,11,2,0.497,0.038,0.503,1,17,14,2,0.662,0.25,0.502,0,0,0,0,0,0.25,0.25
ParamValues=1,17,11,2,497,38,503,1,12,11,2,497,38,503,1,17,14,2,662,250,502,0,0,0,0,0,250,250
Enabled=1

[AppSet4]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1

[AppSet9]
App=Background\SolidColor
FParamValues=0,0,0,1
ParamValues=0,0,0,1000
Enabled=1

[AppSet11]
App=HUD\HUD 3D
FParamValues=0,0,0.47,0.497,0.5,0.5,0.5,0.5,0.5,0.5,0.5025,0.5,0.5,0.5,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,0
ParamValues=0,0,470,497,500,500,500,500,500,500,502,500,500,500,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,0
Enabled=1
ImageIndex=4

[AppSet5]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1

[AppSet6]
App=Blend\BufferBlender
FParamValues=0,1,13,0.508,0,0,0,0,0.5,0.5,0.75,0
ParamValues=0,1,13,508,0,0,0,0,500,500,750,0
Enabled=1
ImageIndex=1

[AppSet18]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1

[AppSet7]
App=Postprocess\Youlean Blur
FParamValues=0.726,0,0,1,0,0
ParamValues=726,0,0,1,0,0
Enabled=1
ImageIndex=3

[AppSet14]
App=Postprocess\Vignette
FParamValues=0,0,0,0.6,1,0.215
ParamValues=0,0,0,600,1000,215
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<p align=""center"">[textline]</p>"
Images="[plugpath]ComboWizard\Assets Revisualizer\EnElCafe.jpg"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

