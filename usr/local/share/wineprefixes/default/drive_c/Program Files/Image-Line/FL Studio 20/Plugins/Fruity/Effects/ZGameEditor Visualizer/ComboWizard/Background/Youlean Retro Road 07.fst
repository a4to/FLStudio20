FLhd   0 1 ` FLdtC  �20.5.1.1193 ��  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4               A                  3      �  �  �    �HQV չ�  ﻿[General]
GlWindowMode=1
LayerCount=3
FPS=1
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,1,2

[AppSet0]
App=Background\Youlean Retro Road
ParamValues=0,754,716,500,892,200,924,1000,250,0,0,106,274,1000,476,300,960,500,900,111,222
ParamValuesBackground\Youlean Background MDL=1000,892,0,0,1000,1000,1000,80,0,0,480,344,0,1000,1000,300,750,0,0,500
ParamValuesCanvas effects\Lava=0,0,500,512,1000,500,500,500,672,500,0
ParamValuesCanvas effects\Stack Trace=0,83,1000,498,130,343,500,280
ParamValuesImage effects\ImageSphereWarp=0,236,750,944,0,1000,0,500,500,500
ParamValuesScenes\Mandelbulb=300,268,500,0,436,500,500,1000,120,1000,1000,125,0,0,0,0,0,0
ParamValuesScenes\Neptune Racing=0,500,250,916,1000,848,932,0,118,0,0
ParamValuesScenes\OnOffSpikes=0,1000,900,333,0,175,951,1000,109,600,800,0,1000,0,850,500,671,800,1000,1000,1000,1000,250
ParamValuesScenes\RhodiumLiquidCarbon=0,333,1000,749,396,809,369,487,732,139
ParamValuesScenes\Space Jewels=0,500,0,180,232,1,500,181,0,0,0
ParamValuesScenes\Spherical Polyhedra=0,484,676,0,0,500,5,126,625,635,500,1
ParamValuesScenes\Xyptonjtroz=0,252,500,182,1000
ParamValuesTunnel\Oblivion=0,312,804,212,352,500,500,100,100,1000,500
ParamValuesTunnel\TorusJourney=0,0,0,0,104,500,500,20,992,1000,780,0,1000,1000,976,164,648,1000,64,0
ParamValuesPeak Effects\Fluidity=0,488,1000,0,472,508,512
ParamValuesPeak Effects\PeekMe=0,792,1000,328,500,500,552,150,520,0,892,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesBackground\SolidColor=0,684,512,340
ParamValuesScenes\Boaty Goes Caving=0,250,0
ParamValuesScenes\Alien Thorns=0,500,542,724,4,144,308,572
ParamValuesTerrain\CubesAndSpheres=0,0,0,0,112,500,116,650,140,400,860,1000,1000,1000,1000,1000,0,0
ParamValuesImage effects\Image=0,0,0,0,660,500,500,1000,1000,0,0,0,0,0
ParamValuesCanvas effects\Electric=0,333,611,842,120,212,500,73,0,1000,500
ParamValuesMisc\CoreDump=0,208,1000,0,500,500,500,0,0,0,0,0,1000,456,292,408,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,942,500,500,818,500,644,0,696,80
ParamValuesCanvas effects\Digital Brain=0,0,0,0,640,500,500,600,100,300,100,125,1000,500,1000,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesScenes\Frozen Wasteland=0,0,0,0,364,1,0,0,0,0,0
ParamValuesPeak Effects\Reactive Sphere=0,1000,833,0,900,500,500,252,252,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesScenes\Cloud Ten=0,0,0,0,272,1000,531,0,0,0,0
ParamValuesObject Arrays\Filaments=0,0,0,0,500,500,500,500,292,296,212,0,500,0
ParamValuesPeak Effects\Linear=0,760,648,0,574,500,160,1000,0,672,260,350,0,0,500,0,0,500,500,500,500,1000,0,0,1000,0,0,212,330,250,100
ParamValuesScenes\Alps=0,71,362,0,250,348,304,0,0
ParamValuesCanvas effects\Flow Noise=0,0,0,720,608,756,332,100,0,0,0
Enabled=1

[AppSet1]
App=Postprocess\ParameterShake
ParamValues=816,0,0,2,592,200,0,14,1,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet2]
App=Misc\Automator
ParamValues=1,1,2,3,1000,8,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""0"" y=""44""><p align=""center""><i><font face=""KaushanScript-Regular.ttf"" color=""#FFFFFF"" size=""11"">[author]</font></i></position>","<position x=""0"" y=""60""><p align=""center""><font face=""Times"" color=""#ffffff"" size=""7"">[title]</font></p></position>","<position x=""0"" y=""40""><p align=""center""><font face=""OrnamentsFont"" color=""#ffffff"" size=""66"">*</p></position>",,,
VideoUseSync=0
Filtering=0

[Detached]
Top=122
Left=791
Width=689
Height=577

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

