FLhd   0  ` FLdtC  �20.5.1.1193 ��  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4               A                        �  �  �    �HQV չ�  ﻿[General]
GlWindowMode=1
LayerCount=4
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=2,0,3,1

[AppSet2]
App=Background\FourCornerGradient
ParamValues=0,1000,828,376,1000,910,576,1000,500,1000,40,750,1000,260
ParamValuesBackground\SolidColor=0,0,288,0
Enabled=1

[AppSet0]
App=Terrain\CubesAndSpheres
ParamValues=0,0,0,0,20,500,636,772,140,400,497,215,1000,1000,315,1000,1000,0
ParamValuesPeak Effects\Fluidity=0,488,1000,0,472,508,512
ParamValuesCanvas effects\Digital Brain=0,0,0,0,640,500,500,600,100,300,100,125,1000,500,1000,0
ParamValuesScenes\Spherical Polyhedra=0,484,676,0,0,1000,5,686,625,635,1000,1
ParamValuesScenes\Xyptonjtroz=0,252,500,182,1000
ParamValuesPeak Effects\PeekMe=0,792,1000,328,500,500,552,150,520,0,892,0
ParamValuesScenes\Frozen Wasteland=0,0,0,0,364,1,0,0,0,0,0
ParamValuesPeak Effects\Reactive Sphere=0,1000,833,0,900,500,500,252,252,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesScenes\Space Jewels=0,500,0,180,232,1,500,181,0,0,0
ParamValuesBackground\Youlean Background MDL=1000,892,0,0,500,500,50,320,0,170,300,764,744,1000,0,300,500,0,0,500
ParamValuesScenes\Alien Thorns=0,500,542,724,4,144,308,572
ParamValuesScenes\Boaty Goes Caving=0,250,0
ParamValuesScenes\Cloud Ten=0,0,0,0,272,1000,531,0,0,0,0
ParamValuesPeak Effects\Linear=0,760,648,0,574,500,160,1000,0,672,260,350,0,0,500,0,0,500,500,500,500,1000,0,0,1000,0,0,212,330,250,100
ParamValuesScenes\Mandelbulb=300,268,500,0,436,500,500,1000,120,1000,1000,125,0,0,0,0,0,0
ParamValuesScenes\Neptune Racing=0,500,250,916,1000,848,932,0,118,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,942,500,500,818,500,644,0,696,80
ParamValuesScenes\Alps=0,71,362,0,250,348,304,0,0
ParamValuesMisc\CoreDump=0,208,1000,0,500,500,500,0,0,0,0,0,1000,456,292,408,0
Enabled=1

[AppSet3]
App=Postprocess\Youlean Bloom
ParamValues=288,0,150,151,1000,0,0,0
Enabled=1

[AppSet1]
App=Misc\Automator
ParamValues=1,1,8,3,1000,8,250,1,1,12,3,1000,10,250,1,1,11,2,428,104,386,1,1,15,3,1000,24,250
ParamValuesPostprocess\ParameterShake=1000,0,0,7,0,0,0,0,3,0,0,0,7,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""0""><position y=""5""><p align=""right""><font face=""04b03_COMP_MAT-Regular"" size=""10"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""0""><position y=""14""><p align=""right""><font face=""04b03_COMP_MAT-Regular"" size=""8"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""0""><position y=""53""><p align=""right""><font face=""04b03_COMP_MAT-Regular"" size=""3"" color=""#FFFFFF"">[extra1]</font></p></position>","<position x=""0""><position y=""92""><p align=""right""><font face=""04b03_COMP_MAT-Regular"" size=""4"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""0""><position y=""81""><p align=""right""><font face=""04b03_COMP_MAT-Regular"" size=""3"" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""0""><position y=""85""><p align=""right""><font face=""04b03_COMP_MAT-Regular"" size=""3"" color=""#FFFFFF"">[extra3]</font></p></position>",
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

