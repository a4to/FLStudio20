FLhd   0 * ` FLdt  �20.6.9.1651 �s  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV Օ5�  ﻿[General]
GlWindowMode=1
LayerCount=16
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,1,10,11,12,13,9,14,15,4,2,7,8,17,16,3

[AppSet0]
App=Background\SolidColor
FParamValues=0,0,0,0.952
ParamValues=0,0,0,952
Enabled=1
Collapsed=1
Name=Background

[AppSet1]
App=Peak Effects\Reactive Sphere
FParamValues=0.688,0.596,1,0,1,0.5,0.5,0.252,0.188,0
ParamValues=688,596,1000,0,1000,500,500,252,188,0
ParamValuesPeak Effects\Linear=0,580,0,0,754,500,500,268,0,500,272,844,504,1000,336,144,0,500,500,1000,350,0,572,1000,220
ParamValuesImage effects\Image=0,0,0,0,0,500,500,0,0,0,0
ParamValuesPeak Effects\Polar=0,1000,0,0,788,498,506,416,516,1000,552,448,0,628,0
Enabled=1
Collapsed=1
Name=EQ or FX

[AppSet10]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.84,0.5,0.5,0.5,0,0,0
ParamValues=0,0,0,0,840,500,500,500,0,0,0
ParamValuesFeedback\BoxedIn=0,0,1000,0,868,0,0,1000,596,324
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,500,408,500,500,500,0,284,500
ParamValuesBackground\Grid=666,0,0,980,40,68,1000,0,0,0
ParamValuesImage effects\Image=0,0,0,1000,1000,500,500,596,764,124,0
ParamValuesFeedback\FeedMe=0,756,1000,640,120,884,296
Enabled=1
Collapsed=1
Name=GFX 1

[AppSet11]
App=Background\Youlean Background MDL
FParamValues=1,0.892,1,0,0.684,0.552,0.426,0.32,0,0.258,0,0.372,1,1,0,0.3,0.5,0,0,0.5
ParamValues=1000,892,1000,0,684,552,426,320,0,258,0,372,1000,1000,0,300,500,0,0,500
ParamValuesBackground\ItsFullOfStars=0,0,0,0,840,500,500,500,0,0,0
Enabled=1
Collapsed=1
Name=L1

[AppSet12]
App=Background\Youlean Background MDL
FParamValues=1,0.892,0,0,0.684,0.552,0.426,0.32,0.08,0.258,0,0.372,0.716,0.604,0,0.568,0.676,0,0,0.5
ParamValues=1000,892,0,0,684,552,426,320,80,258,0,372,716,604,0,568,676,0,0,500
Enabled=1
Collapsed=1
Name=L2

[AppSet13]
App=Background\Youlean Background MDL
FParamValues=0,0.892,0,1,0.684,0.552,0.426,0.32,0.08,0.254,0.176,0.336,0.716,0.448,1,0.212,0.676,4,0,0.776
ParamValues=0,892,0,1000,684,552,426,320,80,254,176,336,716,448,1,212,676,4,0,776
Enabled=1
Collapsed=1
Name=L3

[AppSet9]
App=Feedback\BoxedIn
FParamValues=0,0,0,1,1,0,0,0.5,0,0.284
ParamValues=0,0,0,1000,1000,0,0,500,0,284
ParamValuesFeedback\WormHoleEclipse=24,1000,0,0,0,1000,444,0,600,500,500
ParamValuesCanvas effects\ShimeringCage=672,76,0,0,228,500,500,0,0,0,268,0,0,316
ParamValuesHUD\HUD 3D=0,0,500,500,500,500,500,500,500,500,500,500,500,500,0,500,500,500,500,500,500,500,500,1000,1000,0,0,1000,1000,0
Enabled=1
Collapsed=1
Name=GFX 2

[AppSet14]
App=Feedback\WormHoleEclipse
FParamValues=0,0,0,1,0,0.252,0.5,0.216,0.664,0.5,0.5
ParamValues=0,0,0,1000,0,252,500,216,664,500,500
ParamValuesFeedback\FeedMe=0,0,0,1000,716,444,476
ParamValuesFeedback\BoxedIn=0,0,0,1000,1000,180,0,500,520,768
ParamValuesFeedback\FeedMeFract=0,0,0,592,0,0
Enabled=1
Collapsed=1
Name=Space X

[AppSet15]
App=Postprocess\Youlean Bloom
AppVersion=2
FParamValues=0.636,0.32,0.422,0.4145,0.768,0,1,1
ParamValues=636,320,422,414,768,0,1,1
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,0,252,500,216,664,500,500
ParamValuesHUD\HUD 3D=0,0,500,500,500,500,500,500,500,500,500,500,500,500,0,500,500,500,500,500,500,500,500,1000,1000,0,0,1000,1000,0
Enabled=1
Collapsed=1
Name=Bloom

[AppSet4]
App=Background\FourCornerGradient
FParamValues=13,1,0.088,1,0,0.422,1,0,0.464,1,0.956,0.666,1,0.968
ParamValues=13,1000,88,1000,0,422,1000,0,464,1000,956,666,1000,968
ParamValuesBackground\Grid=526,0,0,980,40,68,1000,0,0,0
ParamValuesImage effects\Image=0,0,0,1000,1000,500,500,0,0,0,0
Enabled=1
Collapsed=1
Name=FILTER for GFX

[AppSet2]
App=Canvas effects\ShimeringCage
FParamValues=0,0,1,0,0.228,0.5,0.5,0.264,0.212,0.108,0.268,0,0,0.316
ParamValues=0,0,1000,0,228,500,500,264,212,108,268,0,0,316
ParamValuesImage effects\Image=0,0,0,1000,1000,500,500,596,764,124,0
Enabled=1
Collapsed=1
Name=PLFX 1

[AppSet7]
App=Canvas effects\ShimeringCage
FParamValues=0,0.076,1,0,0.228,0.5,0.5,0,0,0,0.268,0,0,0.316
ParamValues=0,76,1000,0,228,500,500,0,0,0,268,0,0,316
Enabled=1
Collapsed=1
Name=PLFX 2

[AppSet8]
App=Canvas effects\ShimeringCage
FParamValues=0.672,0.076,0,0,0.228,0.5,0.5,0,0,0,0.268,0,0,0.316
ParamValues=672,76,0,0,228,500,500,0,0,0,268,0,0,316
Enabled=1
Collapsed=1
Name=PLFX 3

[AppSet17]
App=Feedback\WormHoleEclipse
FParamValues=0,0,0,1,0,0.224,0.852,0,0.728,0.588,0.508
ParamValues=0,0,0,1000,0,224,852,0,728,588,508
ParamValuesFeedback\WarpBack=0,0,0,0,1000,500,268
ParamValuesFeedback\70sKaleido=504,0,0,1000,916,1000
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,0,0,500,1000,1000,996,904,1000
Enabled=1
Collapsed=1
Name=Effector 1

[AppSet16]
App=Feedback\70sKaleido
FParamValues=0.504,0,0,1,0.916,1
ParamValues=504,0,0,1000,916,1000
ParamValuesScenes\Boaty Goes Caving=0,498,4,1000
ParamValuesScenes\Alien Thorns=0,500,250,500,3,500,500,0
ParamValuesScenes\Cloud Ten=756,0,0,12,296,488,1000,0,0,0,0
ParamValuesPostprocess\Youlean Image Rotation=500,500,0,556,500,632,0,1
ParamValuesPostprocess\Youlean Handheld=236,444,850,1000,112,0
ParamValuesScenes\Alps=0,167,206,560,250,628,628,388,2
ParamValuesCanvas effects\ShimeringCage=672,76,0,0,228,500,500,0,0,0,268,0,0,316
ParamValuesPostprocess\Youlean Kaleidoscope=500,500,0,500,1000,0,500,608,544,232,164
Enabled=1
Collapsed=1
Name=Effector 2

[AppSet3]
App=Background\FourCornerGradient
FParamValues=7,1,0.508,1,1,0.68,0.768,1,0.12,1,1,0.964,1,1
ParamValues=7,1000,508,1000,1000,680,768,1000,120,1000,1000,964,1000,1000
ParamValuesFeedback\WormHoleDarkn=380,0,0,1000,500,500,500,500,500,500,500,500
ParamValuesFeedback\70sKaleido=0,436,972,52,628,656
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,0,0,0,0,500,500,500
ParamValuesFeedback\SphericalProjection=0,0,0,0,976,377,592,430,312,404,0,333,530,0,916,492,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesCanvas effects\SkyOcean=0,0,1000,0,732,500,500,648,928,500,240,1000,668
ParamValuesCanvas effects\Rain=856,148,1000,888,268,500,500,500,500,476,772,1000
ParamValuesImage effects\Image=0,0,0,0,0,500,500,0,0,0,0
ParamValuesCanvas effects\ShimeringCage=652,916,0,1000,88,500,500,208,0,0,592,0,0,292
ParamValuesFeedback\FeedMe=0,0,600,656,788,640,1000
Enabled=1
Collapsed=1
Name=FILTER COLOR

[Video export]
VideoH=2160
VideoW=3840
VideoRenderFps=30
SampleRate=48000
VideoCodecName=
AudioCodecName=(default)
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=C:\Users\spyro\Desktop\LollieVox - Optimum Momentum_3.mp4
Bitrate=70000000
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="You can","change this text","in settings."
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

