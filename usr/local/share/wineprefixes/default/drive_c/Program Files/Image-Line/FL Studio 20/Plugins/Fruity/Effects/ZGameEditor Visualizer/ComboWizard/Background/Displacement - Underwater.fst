FLhd   0  ` FLdt  �20.7.3.1981 ��  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4               A                        o  �  �    �HQV Ս�  ﻿[General]
GlWindowMode=1
LayerCount=5
FPS=1
AspectRatio=16:9
LayerOrder=0,1,2,3,4

[AppSet0]
App=HUD\HUD Image
FParamValues=0,0,0.5,0.5,1,1,0.672,4,0.5,0,0,1,1,1,0
ParamValues=0,0,500,500,1000,1000,672,4,500,0,0,1000,1000,1,0
Enabled=1
UseBufferOutput=1

[AppSet1]
App=Canvas effects\One Tweet Cellular Pattern
FParamValues=0.9,0.5,0,0.5,0.528
ParamValues=900,500,0,500,528
ParamValuesCanvas effects\Lava=216,0,0,720,180,500,500,500,500,0,0
Enabled=1
UseBufferOutput=1

[AppSet2]
App=Blend\BufferBlender
FParamValues=0,0,0,1,1,1,0.62,0,0.5,0.5,0.75,0
ParamValues=0,0,0,1000,1,1,620,0,500,500,750,0
Enabled=1
ImageIndex=1

[AppSet3]
App=Canvas effects\Round Voronoi Border Refinement
FParamValues=0.984,0.72
ParamValues=984,720
ParamValuesCanvas effects\TaffyPulls=500,0,0,0,0,500,1000,0,500,500,0,0,0,472
ParamValuesCanvas effects\Rain=560,500,500,500,500,500,500,500,500,500,500,500
ParamValuesPostprocess\Raindrops=1000,0
Enabled=1
ImageIndex=3

[AppSet4]
App=Canvas effects\Combustible Voronoi
FParamValues=0.916,0,0,0,0.548,0
ParamValues=916,0,0,0,548,0
ParamValuesCanvas effects\DarkSpark=632,0,500,500,500,500,500,500,500,500,0,0
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=256000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="This is the default text."
Images=@Stream:https://images.pexels.com/photos/2170473/pexels-photo-2170473.jpeg
VideoUseSync=0
Filtering=0

[Detached]
Top=-864
Left=0
Width=1536
Height=864

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

[Wizard]
Section0Cb=Displacement - Mountain View
Section1Cb=None
Section2Cb=Waveform Horiz 02
Section3Cb=Handwritten sweet
Macro0=Song Title
Macro1=Author
Macro2=Comment

