<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" UseStencilBuffer="255" FileVersion="2">
  <OnLoaded>
    <ZExternalLibrary Comment="Vector graphics library" ModuleName="ZgeNano" CallingConvention="1">
      <Source>
<![CDATA[/*
  ZgeNano Library; vector graphics rendering library
  build on NanoVG: https://github.com/memononen/nanovg
  and NanoSVG: https://github.com/memononen/nanosvg

  Version: 1.4 (2018-09-10)

  Download Windows DLL and Android shared library from
  https://github.com/Rado-1/ZgeNano/releases

  Project home
  https://github.com/Rado-1/ZgeNano

  Copyright (c) 2016-2018 Radovan Cervenka
*/

// init flags
const int
  NVG_ANTIALIAS = 1<<0,
	NVG_STENCIL_STROKES = 1<<1,
	NVG_DEBUG = 1<<2;

// winding (direction of arcs)
const int
  NVG_CCW = 1, // counter-clockwise
  NVG_CW = 2; // clockwise

// solidity
const int
  NVG_SOLID = 1,
	NVG_HOLE = 2;

// line caps
const int
	NVG_BUTT = 0,
	NVG_ROUND = 1,
	NVG_SQUARE = 2,
	NVG_BEVEL = 3,
	NVG_MITER = 4;

// align
const int
	// Horizontal align
	NVG_ALIGN_LEFT 		= 1<<0,	// Default, align text horizontally to left.
	NVG_ALIGN_CENTER 	= 1<<1,	// Align text horizontally to center.
	NVG_ALIGN_RIGHT 	= 1<<2,	// Align text horizontally to right.
	// Vertical align
	NVG_ALIGN_TOP 		= 1<<3,	// Align text vertically to top.
	NVG_ALIGN_MIDDLE	= 1<<4,	// Align text vertically to middle.
	NVG_ALIGN_BOTTOM	= 1<<5,	// Align text vertically to bottom.
	NVG_ALIGN_BASELINE	= 1<<6; // Default, align text vertically to baseline.

// blend factor
const int
	NVG_ZERO = 1<<0,
	NVG_ONE = 1<<1,
	NVG_SRC_COLOR = 1<<2,
	NVG_ONE_MINUS_SRC_COLOR = 1<<3,
	NVG_DST_COLOR = 1<<4,
	NVG_ONE_MINUS_DST_COLOR = 1<<5,
	NVG_SRC_ALPHA = 1<<6,
	NVG_ONE_MINUS_SRC_ALPHA = 1<<7,
	NVG_DST_ALPHA = 1<<8,
	NVG_ONE_MINUS_DST_ALPHA = 1<<9,
	NVG_SRC_ALPHA_SATURATE = 1<<10;

// composite operation
const int
	NVG_SOURCE_OVER = 0,
	NVG_SOURCE_IN = 1,
	NVG_SOURCE_OUT = 2,
	NVG_ATOP = 3,
	NVG_DESTINATION_OVER = 4,
	NVG_DESTINATION_IN = 5,
	NVG_DESTINATION_OUT = 6,
	NVG_DESTINATION_ATOP = 7,
	NVG_LIGHTER = 8,
	NVG_COPY = 9,
	NVG_XOR = 10;

// image flags
const int
  NVG_IMAGE_GENERATE_MIPMAPS	= 1<<0,     // Generate mipmaps during creation of the image.
	NVG_IMAGE_REPEATX			= 1<<1,		// Repeat image in X direction.
	NVG_IMAGE_REPEATY			= 1<<2,		// Repeat image in Y direction.
	NVG_IMAGE_FLIPY				= 1<<3,		// Flips (inverses) image in Y direction when rendered.
	NVG_IMAGE_PREMULTIPLIED		= 1<<4;		// Image data has premultiplied alpha.

// Init
xptr nvg_Init(int flags) {} // Returns NanoVG context used in other functions.
void nvg_SetContext(xptr context) {}
void nvg_Finish(xptr context) {}
int nvg_SetViewport(xptr context) {} // Returns 1 if changed viewport, 0 otherwise.

// Drawing
void nvg_BeginFrame() {}
void nvg_CancelFrame() {}
void nvg_EndFrame() {}

// Global composite operation
void nvg_GlobalCompositeOperation(int op) {}
void nvg_GlobalCompositeBlendFunc(int sfactor, int dfactor) {}
void nvg_GlobalCompositeBlendFuncSeparate(int srcRGB, int dstRGB, int srcAlpha, int dstAlpha) {}

// State handling
void nvg_Save() {}
void nvg_Restore() {}
void nvg_Reset() {}

// Render styles
void nvg_StrokeColor(float r, float g, float b, float a) {}
void nvg_StrokePaint(xptr paint) {} // paint - result of Paints functions
void nvg_FillColor(float r, float g, float b, float a) {}
void nvg_FillPaint(xptr paint) {} // paint - result of Paints functions
void nvg_MiterLimit(float limit) {}
void nvg_StrokeWidth(float size) {}
void nvg_LineCap(int cap) {}
void nvg_LineJoin(int join) {}
void nvg_GlobalAlpha(float alpha) {}

// Transformations
void nvg_ResetTransform() {}
void nvg_Transform(float a, float b, float c, float d, float e, float f) {}
void nvg_Translate(float x, float y) {}
void nvg_Rotate(float angle) {}
void nvg_SkewX(float angle) {}
void nvg_SkewY(float angle) {}
void nvg_Scale(float x, float y) {}

// Images
int nvg_CreateImage(string filename, int imageFlags) {}
int nvg_CreateImageMem(int imageFlags, xptr data, int ndata) {}
int nvg_CreateImageRGBA(int w, int h, int imageFlags, xptr data) {}
void nvg_UpdateImage(int image, xptr data) {}
void nvg_ImageSize(int image, ref int w, ref int h) {}
void nvg_DeleteImage(int image) {}

// Paints
xptr nvg_LinearGradient(float sx, float sy, float ex, float ey,
	float ir, float ig, float ib, float ia,
	float or, float og, float ob, float oa) {}
xptr nvg_BoxGradient(float x, float y, float w, float h, float r, float f,
	float ir, float ig, float ib, float ia,
	float or, float og, float ob, float oa) {}
xptr nvg_RadialGradient(float cx, float cy, float inr, float outr,
	float ir, float ig, float ib, float ia,
	float or, float og, float ob, float oa) {}
xptr nvg_ImagePattern(float ox, float oy, float ex, float ey,
	float angle, int image, float alpha) {}
void nvg_FreePaint(xptr paint) {}

// Scissoring
void nvg_Scissor(float x, float y, float w, float h) {}
void nvg_IntersectScissor(float x, float y, float w, float h) {}
void nvg_ResetScissor() {}

// Paths
void nvg_BeginPath() {}
void nvg_MoveTo(float x, float y) {}
void nvg_LineTo(float x, float y) {}
void nvg_BezierTo(float c1x, float c1y, float c2x, float c2y, float x, float y) {}
void nvg_QuadTo(float cx, float cy, float x, float y) {}
void nvg_ArcTo(float x1, float y1, float x2, float y2, float radius) {}
void nvg_ClosePath() {}
void nvg_PathWinding(int dir) {}
void nvg_Arc(float cx, float cy, float r, float a0, float a1, int dir) {}
void nvg_Rect(float x, float y, float w, float h) {}
void nvg_RoundedRect(float x, float y, float w, float h, float r) {}
void nvg_RoundedRectVarying(float x, float y, float w, float h,
	float radTopLeft, float radTopRight, float radBottomRight, float radBottomLeft) {}
void nvg_Ellipse(float cx, float cy, float rx, float ry) {}
void nvg_Circle(float cx, float cy, float r) {}
void nvg_Fill() {}
void nvg_Stroke() {}
void vg_StrokeNoScale() {}

// Text
int nvg_CreateFont(string name, string filename) {}
int nvg_CreateFontMem(string name, xptr data, int ndata, int freeData) {}
int nvg_FindFont(string name) {}
int nvg_AddFallbackFontId(int baseFont, int fallbackFont) {}
int nvg_AddFallbackFont(string baseFont, string fallbackFont) {}
void nvg_FontSize(float size) {}
void nvg_FontBlur(float blur) {}
void nvg_TextLetterSpacing(float spacing) {}
void nvg_TextLineHeight(float lineHeight) {}
void nvg_TextAlign(int align) {}
void nvg_FontFaceId(int font) {}
void nvg_FontFace(string font) {}
void nvg_Text(float x, float y, string str, string end) {}
void nvg_TextBox(float x, float y, float breakRowWidth, string str, string end) {}
float nvg_TextBounds(float x, float y, string str, string end, vec4 bounds) {}
void nvg_TextBoxBounds(float x, float y, float breakRowWidth, string str, string end, vec4 bounds) {}
void nvg_TextMetrics(ref float ascender, ref float descender, ref float lineh) {}

// SVG support
xptr nsvg_ParseFromFile(string filename, string units, float dpi) {}
xptr nsvg_ParseMem(string data, int ndata, string units, float dpi) {}
void nsvg_ImageSize(xptr image, ref float width, ref float height) {}
int nsvg_ImageShapeCount(xptr image, string shapeIdPrefix) {}
void nsvg_Draw(xptr image, string shapeIdPrefix,
  int strokeWidthScaling, float strokeWidthFactor, float buildUpFromFactor, float buildUpToFactor, xptr color) {}
void nsvg_Rasterize(xptr image, float tx, float ty, float scale, xptr dst, int w, int h) {}
void nsvg_Delete(xptr image) {}]]>
      </Source>
    </ZExternalLibrary>
    <ZExternalLibrary ModuleName="ZGameEditor Visualizer">
      <Source>
<![CDATA[void ParamsNotifyChanged(xptr Handle,int Layer) { }
void ParamsChangeName(xptr Handle,int Layer, int Parameters, string NewName) { }
void ParamsWriteValueForLayer(xptr Handle, int Layer,int Param, float NewValue) { }
int ReadPrivateData(xptr Handle, xptr Data, int Size) { }
void WritePrivateData(xptr Handle, xptr Data, int Size) { }]]>
      </Source>
    </ZExternalLibrary>
    <ZLibrary>
      <Source>
<![CDATA[string FloatToStr(float F, int D) // D = Number of decimals
{
  string S = F < 0 ? "-" : "";
  F = abs(F);
  int N = floor(F);
  S += intToStr(N)+".";

  F -= N;

  for(int B=0; B<D; B++)
  {
    F *= 10;
    N = floor(F);
    S += intToStr(N);
    F -= N;
  }

  return S;
}

//

float StrToFloat(string S)
{
  int L, C, A, N, D;

  N = D = 0;
  L = length(S);

  float F = 0;

  for(C=0; C<L; C++)
  {
    A = ord(subStr(S,C,1));

    switch(A)
    {
      case 45: N = 1; break;
      case 46: D = 1; break;

      default:
        if(D)
        {
          F += (A-48)/pow(10,D);
          D++;
        }
        else
        {
          F *= 10;
          F += A-48;
        }
    }
  }

  if(N)F = 0-F;

  return F;
}

float angle(float X)
{
  if(X >= 0 && X < 360)return X;
  if(X > 360)return X-floor(X/360)* 360;
  if(X <   0)return X+floor(X/360)*-360;
}

vec3 hsv(float H, float S, float V)
{
  vec3 Color;
  float R,G,B,I,F,P,Q,T;

  H = angle(H);
  S = clamp(S,0,100);
  V = clamp(V,0,100);

  H /= 60;
  S /= 100;
  V /= 100;

  if(S == 0)
  {
    Color[0] = V;
    Color[1] = V;
    Color[2] = V;
  }
  else
  {
    I = floor(H);
    F = H-I;

    P = V*(1-S);
    Q = V*(1-S*F);
    T = V*(1-S*(1-F));

    if(I == 0){R = V; G = T; B = P;}
    if(I == 1){R = Q; G = V; B = P;}
    if(I == 2){R = P; G = V; B = T;}
    if(I == 3){R = P; G = Q; B = V;}
    if(I == 4){R = T; G = P; B = V;}
    if(I == 5){R = V; G = P; B = Q;}

    Color[0] = R;
    Color[1] = G;
    Color[2] = B;
  }
  return Color;
}]]>
      </Source>
    </ZLibrary>
    <ZLibrary Name="Smoothing">
      <Source>
<![CDATA[// Smoothing stuff by Youlean
// ======================================================================================================================================
// ======================================================================================================================================
// ======================================================================================================================================

// Helper function for slope ===================================================================
inline float SlopeTension(int index, int arraySizeMinusOne, float tension)
{
	float floatIndex = index;
	float floatArraySizeMinusOne = arraySizeMinusOne;

	float pos;

	if (arraySizeMinusOne > 1)
	pos = floatIndex / floatArraySizeMinusOne;
	else return 0;

	return (1 - pow(1  -pos, tension)) * floatArraySizeMinusOne;
}
// End of helper function for slope ============================================================


/*
"ArraySlope" will change slope of the array. Values above 1 will boost start of the array
and values below 1 will decrease values at start.
*/

inline void ArraySlope(float[] in, int arraySize, float slopeValue)
{
	float slopeMultiplyer = (slopeValue - 1) / arraySize;

	for(int i = 0; i < arraySize; i++)
	{
		in[i] = in[i] * (slopeValue - (slopeMultiplyer * i));
	}
}

/*
"ArraySlopeAdvanced" is similar to "ArraySlope" but it has some advanced functions.
Higher tension can be used with spectrogram to boost lows without affecting mids and highs that much...
*/

inline void ArraySlopeAdvanced(float[] in, int arraySize, float slopeValue, float slopeStartPercentage, float tension)
{
	int arrayStart = arraySize * (slopeStartPercentage / 100);
	int slopeArraySize = arraySize - arrayStart;

	float slopeMultiplyer = (slopeValue - 1) / arraySize;

	for(int i = 0; i < arraySize; i++)
	{
		int tensionIndex = SlopeTension(i - arrayStart < 0 ? 0 : i - arrayStart, slopeArraySize - 1, tension);

		in[i] = in[i] * (slopeValue - (slopeMultiplyer * tensionIndex));
	}
}


// Global variables used for smoothing
// You can set coeffs manually for example "attackCoeff = 0.77;", but keep in mind that coeffs will go from 0 to 1.0 range.

float attackCoeff;
float releaseCoeff;
float smoothCoeff;

// Helper function for smoothing ===================================================================
inline float coeffCalc(float coeff, float expectedFps)
{
	if (coeff < 0.0000000001) return 0.0;
	return exp(-1.0 / (expectedFps * coeff * 0.001));
}
// End of helper function for smoothing ============================================================

/*
You need to call "SetEnvSmoothProperties" or  "SetLPSmoothProperties" to calculate the coeffs for smoothing.
This should be called every time you need to change "attackMS", "releaseMS" or "smoothMS" parameter

"expectedFps" is used only to calculate coeefs for attack, release and smooth.
You should supply to max FPS and video export FPS to "expectedFps"
*/

inline void SetEnvSmoothProperties(float attackMS, float releaseMS, float expectedFps)
{
	attackCoeff = coeffCalc(attackMS, expectedFps);
	releaseCoeff = coeffCalc(releaseMS, expectedFps);
}

inline void SetLPSmoothProperties(float smoothMS, float expectedFps)
{
	smoothCoeff = coeffCalc(smoothMS, expectedFps);
}

/*
You can call "EnvSmooth" or "LPSmooth" to smooth float value.

"EnvSmooth" uses coeff calculated with "SetEnvSmoothProperties"
"LPSmooth" uses coeff calculated with "SetLPSmoothProperties"
*/

inline float EnvSmooth(float lastOut, float in)
{
	if (abs(in) > abs(lastOut))
	lastOut = attackCoeff * (lastOut - in) + in;
	else
	lastOut = releaseCoeff * (lastOut - in) + in;

	return lastOut;
}

inline float LPSmooth(float lastOut, float in)
{
	lastOut = smoothCoeff * (lastOut - in) + in;

	return lastOut;
}

/*
"VercticalSmooth" smooths every array value individually.
You will need to supply "uniqueBuffer" that will be same size as "in" and this buffer should be global
and you must not alter it's values.

After "VercticalSmooth" function call "in" and "uniqueBuffer" will hold same data,
but it is only modifying "in" safe.
*/

inline void VercticalSmooth(float[] in, float[] uniqueBuffer, int arraySize, float attackMS, float releaseMS, float expectedFps)
{
	SetEnvSmoothProperties(attackMS, releaseMS, expectedFps);
	for(int i = 0; i < arraySize; i++)
	{
		uniqueBuffer[i] = EnvSmooth(uniqueBuffer[i], in[i]);
		in[i] = uniqueBuffer[i];
	}
}

/*
"AsymmetricalVercticalSmooth" is similar to "VercticalSmooth"
but difference is that it can smooth array by setting coeffs for first and last point separately
and lineary interpolating coeffs inbetween.

In other words when you use this to smooth spectogram you can set different coeffs for bass and for highs.
Since in spectogram bass is always less responsive, you can use more smoothing on highs to get uniform feel.
TODO: Add log interpolation for X axis...?
*/

inline void AsymmetricalVercticalSmooth(float[] in, float[] uniqueBuffer, int arraySize, float attackStartMS, float releaseStartMS, float attackEndMS, float releaseEndMS, float expectedFps)
{
	float attackSlopeAdd = 0;
	float releaseSlopeAdd = 0;
	float attack = attackStartMS;
	float release = releaseStartMS;

	if (attackEndMS - attackStartMS != 0)
	{
		attackSlopeAdd = (attackEndMS - attackStartMS) / arraySize;
	}
	if (releaseEndMS - releaseStartMS != 0)
	{
		releaseSlopeAdd = (releaseEndMS - releaseStartMS) / arraySize;
	}

	for(int i = 0; i < arraySize; i++)
	{
		SetEnvSmoothProperties(attack, release, expectedFps);
		uniqueBuffer[i] = EnvSmooth(uniqueBuffer[i], in[i]);
		in[i] = uniqueBuffer[i];

		attack = attack + attackSlopeAdd;
		release = release + releaseSlopeAdd;
	}
}


/*
"HorizontalSmooth" will smooth array by X axis.
For symetrical smooth we need to do Low Pass smoothing from left to right and then from right to left.

Smoothing is independed of "in" size.
*/

inline void HorizontalSmooth(float[] in, int arraySize, float smoothValue, int smoothEdges)
{
	float valueOut;

	if (smoothEdges == 0)
	{
		valueOut = in[0];
	}
	else
	{
		valueOut = 0;
	}

	SetLPSmoothProperties(smoothValue, arraySize);

	// Horizontal smoothing using LP filter
	for(int i = 0; i < arraySize; i++)
	{
		// From left to right
		valueOut = LPSmooth(valueOut, in[i]);
		in[i] =  valueOut;
	}

	if (smoothEdges == 0)
	{
		valueOut = in[arraySize - 1];
	}
	else
	{
		valueOut = 0;
	}

	for(int i = arraySize-1; i >= 0; i--)
	{
		// From right to left
		valueOut = LPSmooth(valueOut, in[i]);
		in[i] =  valueOut;
	}
}

// Helper functions for "ResampleArray" ========================================================================================================
inline float NearestMiddle(float y0, float y1, float x)
{
	if (x > 0.5) return y1;
	else return y0;
}

inline float Linear(float y0, float y1, float x)
{
	return(y0*(1 - x) + y1*x);
}

inline float Cosine(float y0, float y1, float x)
{
	float mu2;

	mu2 = (1 - cos(x*3.14159265358979323846)) / 2;
	return(y0*(1 - mu2) + y1*mu2);
}

inline float Cubic(float yminus1, float y0, float y1, float y2, float x)
{
	float a0, a1, a2, a3;
	float mu2;

	mu2 = x*x;
	a0 = y2 - y1 - yminus1 + y0;
	a1 = yminus1 - y0 - a0;
	a2 = y1 - yminus1;
	a3 = y0;

	return(a0*x*mu2 + a1*mu2 + a2*x + a3);
}

inline float max(float v1, float v2)
{
	return v1 >= v2 ? v1 : v2;
}

inline float min(float v1, float v2)
{
	return v1 <= v2 ? v1 : v2;
}

inline float GetInterpolationPosition(float input)
{
	return input - floor(input);
}

inline float GetValueFromIndex(float[] in, int index, int arraySizeMinusOne)
{
	return in[min(max(index, 0), arraySizeMinusOne)];
}

inline float GetValue(float[] in, int index, float position, int arraySizeMinusOne, int interpolate, int processing)
{
	float  minusOne, zero, one, two;
	int minusOneIndex, zeroIndex, oneIndex, twoIndex;

	zeroIndex = index;
	oneIndex = index + 1;
	twoIndex =index + 2;
	minusOneIndex = index - 1;

	// Get values
	zero = GetValueFromIndex(in, zeroIndex, arraySizeMinusOne);
	one = GetValueFromIndex(in, oneIndex, arraySizeMinusOne);

	// Get these only if we need it
	if (interpolate > 3)
	{
		minusOne = GetValueFromIndex(in, minusOneIndex, arraySizeMinusOne);
		two = GetValueFromIndex(in, twoIndex, arraySizeMinusOne);
	}

	switch (interpolate)
	{
		// Two point interpolation
	case 0: return zero;
	case 1: return NearestMiddle(zero, one, position);
	case 2: return Linear(zero, one, position);
	case 3: return Cosine(zero, one, position);

		// Four points interpolation
	case 4: return Cubic(minusOne, zero, one, two, position);
	}
	return 0;
}

inline float InterpolateValue(float[] in, float index, float increasement, int arraySizeMinusOne, int interpolate, int processing, int genericInnerProcessing)
{
	float minValue;
	float maxValue;
	float average;

	maxValue = 3.4e-38; // Set minimum float value
	minValue = 3.4e38; // Set maximum float value
	average = 0;

	if (processing != 0 && increasement >= 1)
	{
		float innerIndex = index;
		int intInnerIndex = index;
		int intNextIndex = index + increasement;

		float innerValues = intNextIndex - intInnerIndex;
		float increaseInnerIndexBy = increasement / innerValues;
		float increaseMultiplyer = 0;

		int loopSize = intNextIndex - intInnerIndex;

		// If last value in array
		if (loopSize == 0)
		{
			if (processing == 1) maxValue = GetValue(in, intInnerIndex, GetInterpolationPosition(innerIndex), arraySizeMinusOne, interpolate, processing);
			else if (processing == 2) minValue = GetValue(in, intInnerIndex, GetInterpolationPosition(innerIndex), arraySizeMinusOne, interpolate, processing);
			else if (processing == 3) average = GetValue(in, intInnerIndex, GetInterpolationPosition(innerIndex), arraySizeMinusOne, interpolate, processing);
		}
		else while (intInnerIndex < intNextIndex)
		{
			if (genericInnerProcessing == 1)
			{
				if (intInnerIndex <= arraySizeMinusOne)
				{
					if (processing == 1) maxValue = max(maxValue, in[intInnerIndex]);
					else if (processing == 2) minValue = min(minValue, in[intInnerIndex]);
					else if (processing == 3) average = average + in[intInnerIndex];
				}
			}
			else
			{
				if (processing == 1) maxValue = max(maxValue, GetValue(in, intInnerIndex, GetInterpolationPosition(innerIndex), arraySizeMinusOne, interpolate, processing));
				else if (processing == 2) minValue = min(minValue, GetValue(in, intInnerIndex, GetInterpolationPosition(innerIndex), arraySizeMinusOne, interpolate, processing));
				else if (processing == 3) average = average + GetValue(in, intInnerIndex, GetInterpolationPosition(innerIndex), arraySizeMinusOne, interpolate, processing);
			}
			increaseMultiplyer++;
			innerIndex = index + increaseInnerIndexBy * increaseMultiplyer;
			intInnerIndex = innerIndex;
		}

		if (processing == 1) return maxValue;
		else if (processing == 2) return minValue;
		else if (processing == 3) return average / max(loopSize, 1);
	}
	else
	{
		return GetValue(in, index, GetInterpolationPosition(index), arraySizeMinusOne, interpolate, processing);
	}
	return 0;
}
// End of helper functions for "ResampleArray" ====================================================================================================

/*
Call this function to resample array

in = input float array
out = output float array. It can't be same as input since interpolation will use multiple points in input array
inSize = size of input array. You can resample only part of the array.
outSize = size of output array. You can resample only part of the array.

interpolateMethod = This will determent interpolation method. See table below:

Two point interpolation
0 = nearest_start
1 = nearest_middle
2 = linear
3 = cosine

Four points interpolation
4 = cubic

innerProcessing = This will determent if we need to take in considaration inner walues when downsampling. See table below:

0 = off
1 = max
2 = min
3 = average
*/

inline void ResampleArray(float[] in, float[] out, int inSize, int outSize, int interpolateMethod, int innerProcessing, int genericInnerProcessing)
{
	if (inSize == 0 || outSize == 0) return;

	int inSizeMinusOne = inSize - 1;
	int outSizeMinusOne = outSize - 1;

	float floatInSizeMinusOne = inSizeMinusOne;
	float floatOutSizeMinusOne = outSizeMinusOne;

	float increasement = floatInSizeMinusOne / floatOutSizeMinusOne;

	float index = 0;
	for (int i = 0; i < outSize; )
	{
		out[i] = InterpolateValue(in, index, increasement, inSizeMinusOne, interpolateMethod, innerProcessing, genericInnerProcessing);
		i++;
		index = increasement * i;
	}
}

// Bass Pump function
inline void BassPump(float[] in, int inSize, float freq, float mix)
{
	int bassFreqSize = inSize * freq;
	float maxValue = abs(in[0]);

	for (int i = 0; i < bassFreqSize; i++)
	{
		maxValue = max(maxValue, abs(in[i]));
	}

	for (int i = 0; i < inSize; i++)
	{
		in[i] = (in[i]*(1 - mix) + (in[i] * maxValue)*mix);
	}
}]]>
      </Source>
    </ZLibrary>
    <ZExpression>
      <Expression>
<![CDATA[MAXBAND=SpecBandArray.SizeDim1;
PrevBuffer1.SizeDim1 = MAXBAND;
Buffer1.SizeDim1 = MAXBAND;]]>
      </Expression>
    </ZExpression>
    <ZLibrary Comment="InitNanoVG">
      <Source>
<![CDATA[// callback on hanged OpenGL context
void OnGLContextChange() {
  // reset NanoVG
  @CallComponent(Component: InitNanoVG);
}]]>
      </Source>
    </ZLibrary>
    <ZExpression Name="InitNanoVG">
      <Expression>
<![CDATA[// init NanoVG
NvgContext = nvg_Init(NVG_STENCIL_STROKES);
if (NvgContext == null) {
  trace("Error to init NanoVG.");
  quit();
}

RotationAngle = 0;]]>
      </Expression>
    </ZExpression>
  </OnLoaded>
  <OnUpdate>
    <ZExpression>
      <Expression>
<![CDATA[uAlpha=1.0 - Parameters[0];
uCol1=hsv(Parameters[1]*360,Parameters[2]*100,(1-Parameters[3])*100);
uCol=hsv(Parameters[4]*360,Parameters[5]*100,(1-Parameters[6])*100);]]>
      </Expression>
    </ZExpression>
    <SpawnModel Model="EQ" SpawnStyle="1"/>
  </OnUpdate>
  <OnClose>
    <ZExpression Name="FinishNanoVG" Expression="nvg_Finish(NvgContext);"/>
  </OnClose>
  <Content>
    <Model Name="EQ">
      <OnRender>
        <UseMaterial Material="EQMaterial"/>
        <ZExpression>
          <Expression>
<![CDATA[float lineWidth = Parameters[15] * Parameters[7] * 0.2;

nvg_SetContext(NvgContext);
nvg_SetViewport(NvgContext);

nvg_BeginFrame();

float ViewportWidth = App.ViewportWidth;
float ViewportHeight = App.ViewportHeight;
float AspectRatio = ViewportWidth / ViewportHeight;

// Init NanoVG scaling
nvg_Scale(ViewportWidth / 3.0 / AspectRatio * 1,  ViewportHeight / 3.0 * 1);
nvg_Translate(1.5 * AspectRatio / 1, 1.5 / 1);

nvg_GlobalAlpha(uAlpha);

EQ.Scale = Parameters[7] * 3;

EQ.Rotation.X = Parameters[8];
EQ.Rotation.Y = Parameters[9];
EQ.Rotation.Z = Parameters[10];

mat4 modelViewMatrix;
getMatrix(0, modelViewMatrix);

// Create grid
const int maxRowsColumns = 200;
vec3[maxRowsColumns, maxRowsColumns] grid;
float[maxRowsColumns, maxRowsColumns] noise;
int rows = Parameters[19] * (maxRowsColumns - 20) + 20;
int columns = Parameters[20] * (maxRowsColumns - 20) + 20;

float step = 0.5;
float xStart = columns * step / -2;
float yStart = rows * step / -2;

flyingYVar -= 0.4 * Parameters[11];

int space = 10 * Parameters[12] + 1;
float height = 30 * Parameters[14];

// Create Nose
float flyingY = flyingYcVar;
float flyingYS = flyingYcVar / 10;

float flyStepY = 0.5;
float flyStepX = 0.17;

for (int r = 0; r < rows; r++)
{
	float fr = r + 0.000001 - flyingYVar * 2;
	float rhs = rows - 1 + 0.000001;
	float rM = pow((fr / rhs), 0.5);

	// Create Columns
	float flyingX = 0;
	float flyingXS = 0;
	for (int c = 0; c < columns / 2 - space; c++)
	{
		float fc = c + 0.000001;
		float hs = columns / 2 - space - 1 + 0.000001;
		float M = 1.0 - (fc / hs);
    M = M * (Parameters[20] * 4 + 1);
		M = 1.0 - pow(1.0 - M, 1);

		noise[r,c] = (noise2(flyingX, flyingY) - 0.3) * height * M * rM;

		flyingX += flyStepX;
		flyingXS += flyStepX / 10;
	}
	flyingX = 1;
	flyingXS = 1;
	for (int c = columns / 2 + space + 1; c < columns; c++)
	{
		float fc = c - (columns / 2 + space + 1) + 0.000001;
		float hs = (columns / 2 - space) + 1 + 0.000001;
		float M = (fc / hs);
    M = M * (Parameters[20] * 4 + 1);
		M = 1.0 - pow(1.0 - M, 1);

		noise[r,c] = (noise2(flyingX, flyingY) - 0.3) * height * M * rM;

		flyingX += flyStepX;
		flyingXS += flyStepX / 10;
	}

	flyingY += flyStepY;
	flyingYS += flyStepY / 10;
}

// Create Rows
vec3 point3D;
for (int r = 0; r < rows; r++)
{
	float fr = r;
	float frr = r + 0.000001 - flyingYVar * 2;
	float rhs = rows - 1 + 0.000001;
	float rM = pow((frr / rhs), 1.0) * PI * -1;

	// Create Columns
	for (int c = 0; c < columns; c++)
	{
		float fc = c;

		point3D.x = xStart + fc * step;
		point3D.y = yStart + fr * step - flyingYVar;

		point3D.z = 1.0 + noise[r,c]; //sin(rM) * 0

		point3D = transformPoint(modelViewMatrix, point3D);

		if (point3D.z != 0)
		{
			grid[r,c].x = point3D.x / point3D.z;
			grid[r,c].y = point3D.y / point3D.z;
			grid[r,c].z = point3D.z;
		}
	}
}

if (flyingYVar < step * -1)
{
	flyingYVar = 0;
	flyingYcVar -= flyStepY;
}

float stroke = lineWidth;//0.029;
nvg_StrokeWidth(stroke);
nvg_LineJoin(NVG_ROUND);

nvg_StrokeColor(uCol[0], uCol[1], uCol[2], 1.0);

float minDrawSize = 0.005;

// Draw grid
for (int r = 0; r < rows - 1; r++)
{
	float fr = r + 0.0001;
	float adaptiveW = pow(fr / (rows - 1 + 0.0001),4);

	nvg_StrokeWidth(Linear(stroke, stroke * adaptiveW, Parameters[16]));

	for (int c = 0; c < columns - 1; c++)
	{
		if (grid[r,c].z > 0 || grid[r,c+1].z > 0 || grid[r+1,c+1].z > 0 || grid[r+1,c].z > 0)
		continue;

		// Simulate lightning
		float light = noise[r,c] * 0.2 + 0.45;
		float heightLight = Linear(0, light, Parameters[17]);
		float sunLight = pow(min(grid[r,c].z * -0.08, 3), (1.0 - Parameters[18]) * 3) * Parameters[13] * Linear(10, 1, Parameters[18]);

		nvg_FillColor(uCol1[0] + heightLight, uCol1[1] + heightLight, uCol1[2] + heightLight, sunLight + 1.0);

		nvg_BeginPath();
		nvg_MoveTo(grid[r,c].x, grid[r,c].y);
		nvg_LineTo(grid[r,c + 1].x, grid[r,c + 1].y);
		nvg_LineTo(grid[r + 1,c + 1].x, grid[r + 1,c + 1].y);
		nvg_LineTo(grid[r + 1,c].x, grid[r + 1,c].y);
		nvg_ClosePath();
		nvg_Fill();
		nvg_Stroke();
	}
}
nvg_EndFrame();]]>
          </Expression>
        </ZExpression>
      </OnRender>
    </Model>
    <Group Comment="Uniform">
      <Children>
        <Variable Name="uAlpha"/>
        <Variable Name="res" Type="6"/>
        <Variable Name="uFill"/>
        <Array Name="PrevBuffer1"/>
        <Array Name="Buffer1"/>
        <Variable Name="uCol" Type="7"/>
        <Variable Name="RotationAngle"/>
        <Variable Name="uCol1" Type="7"/>
      </Children>
    </Group>
    <Group Comment="FLStudio">
      <Children>
        <Array Name="SpecBandArray"/>
        <Variable Name="MAXBAND" Type="1"/>
        <Array Name="Parameters" SizeDim1="21" Persistent="255">
          <Values>
<![CDATA[78DA63606060484B73B31759976E0F64DA3344A5D8339CF1B1636068B08762201B06406C301FAC76D6CC9976FD874AC1ECB4B434FB1FC18F6D7F0427DB0100ADF51398]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Alpha
Hue
Saturation
Lightness
L. Hue
L. Saturation
L. Lightness
Size
Rotation X
Rotation Y
Rotation Z
Speed
Road Width
Sun Light
Height
Line Width
Adapt. L. Width
Shadows
Sun Reach
Rows
Columns]]>
          </StringValue>
        </Constant>
        <Variable Name="FLPluginHandle" Type="9"/>
        <Variable Name="LayerNr" Type="1"/>
      </Children>
    </Group>
    <Constant Name="AuthorInfo" Type="2" StringValue="Youlean"/>
    <Shader Name="Shader1" UpdateVarsOnEachUse="255">
      <VertexShaderSource>
<![CDATA[/*
void main()
{
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
  gl_TexCoord[0] = gl_MultiTexCoord0;
}


void main() {
    vec4 p=gl_Vertex;
    gl_Position = gl_ModelViewProjectionMatrix*p;
}
*/

varying vec4 vColor;

void main(void)
{
   vec4 a = gl_Vertex;
   a.x = a.x * 2.0;
   a.y = a.y * 2.0;

  vColor = gl_Color;
   gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[uniform float alpha;
varying vec4 vColor;

void main (void)
{
gl_FragColor = vec4(vColor.r, vColor.g, vColor.b,vColor.a * (1.0 - alpha));
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="alpha" Value="1" VariableRef="uAlpha"/>
      </UniformVariables>
    </Shader>
    <Material Name="EQMaterial" Shader="Shader1"/>
    <Variable Name="flyingYVar"/>
    <Variable Name="flyingYcVar"/>
    <Variable Name="NvgContext" Type="9"/>
  </Content>
</ZApplication>
