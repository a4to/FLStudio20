<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" FileVersion="2">
  <OnLoaded>
    <ZExternalLibrary Comment="OpenGL 4.0 graphics" ModuleName="opengl32" DefinitionsFile="opengl.txt">
      <BeforeInitExp>
<![CDATA[if(ANDROID) {
  if(App.GLBase==0)
    this.ModuleName="libGLESv1_CM.so";
  else
    this.ModuleName="libGLESv2.so";
}]]>
      </BeforeInitExp>
    </ZExternalLibrary>
    <ZLibrary>
      <Source>
<![CDATA[float angle(float X)
{
  if(X >= 0 && X < 360)return X;
  if(X > 360)return X-floor(X/360)* 360;
  if(X <   0)return X+floor(X/360)*-360;
}

vec3 hsv(float H, float S, float V)
{
  vec3 Color;
  float R,G,B,I,F,P,Q,T;

  H = angle(H);
  S = clamp(S,0,100);
  V = clamp(V,0,100);

  H /= 60;
  S /= 100;
  V /= 100;

  if(S == 0)
  {
    Color[0] = V;
    Color[1] = V;
    Color[2] = V;
  }
  else
  {
    I = floor(H);
    F = H-I;

    P = V*(1-S);
    Q = V*(1-S*F);
    T = V*(1-S*(1-F));

    if(I == 0){R = V; G = T; B = P;}
    if(I == 1){R = Q; G = V; B = P;}
    if(I == 2){R = P; G = V; B = T;}
    if(I == 3){R = P; G = Q; B = V;}
    if(I == 4){R = T; G = P; B = V;}
    if(I == 5){R = V; G = P; B = Q;}

    Color[0] = R;
    Color[1] = G;
    Color[2] = B;
  }
  return Color;
}]]>
      </Source>
    </ZLibrary>
    <ZExpression>
      <Expression>
<![CDATA[MAXBAND=SpecBandArray.SizeDim1;
prevBands = MAXBAND;
SpecBandArrayResampled.SizeDim1=SpecBandArray.SizeDim1;
SpecBandVSmoothBuffer.SizeDim1 = SpecBandArray.SizeDim1;
SmoothBand.SizeDim1=SpecBandArray.SizeDim1;]]>
      </Expression>
    </ZExpression>
    <ZLibrary>
      <Source>
<![CDATA[// Smoothing stuff by Youlean
// ======================================================================================================================================
// ======================================================================================================================================
// ======================================================================================================================================

// Helper function for slope ===================================================================
float SlopeTension(int index, int arraySizeMinusOne, float tension)
{
	float floatIndex = index;
	float floatArraySizeMinusOne = arraySizeMinusOne;

	float pos;

	if (arraySizeMinusOne > 1)
	pos = floatIndex / floatArraySizeMinusOne;
	else return 0;

	return (1 - pow(1  -pos, tension)) * floatArraySizeMinusOne;
}
// End of helper function for slope ============================================================


/*
"ArraySlope" will change slope of the array. Values above 1 will boost start of the array
and values below 1 will decrease values at start.
*/

void ArraySlope(float[] in, int arraySize, float slopeValue)
{
	float slopeMultiplyer = (slopeValue - 1) / arraySize;

	for(int i = 0; i < arraySize; i++)
	{
		in[i] = in[i] * (slopeValue - (slopeMultiplyer * i));
	}
}

/*
"ArraySlopeAdvanced" is similar to "ArraySlope" but it has some advanced functions.
Higher tension can be used with spectrogram to boost lows without affecting mids and highs that much...
*/

void ArraySlopeAdvanced(float[] in, int arraySize, float slopeValue, float slopeStartPercentage, float tension)
{
	int arrayStart = arraySize * (slopeStartPercentage / 100);
	int slopeArraySize = arraySize - arrayStart;

	float slopeMultiplyer = (slopeValue - 1) / arraySize;

	for(int i = 0; i < arraySize; i++)
	{
		int tensionIndex = SlopeTension(i - arrayStart < 0 ? 0 : i - arrayStart, slopeArraySize - 1, tension);

		in[i] = in[i] * (slopeValue - (slopeMultiplyer * tensionIndex));
	}
}


// Global variables used for smoothing
// You can set coeffs manually for example "attackCoeff = 0.77;", but keep in mind that coeffs will go from 0 to 1.0 range.

float attackCoeff;
float releaseCoeff;
float smoothCoeff;

// Helper function for smoothing ===================================================================
float coeffCalc(float coeff, float expectedFps)
{
	if (coeff < 0.0000000001) return 0.0;
	return exp(-1.0 / (expectedFps * coeff * 0.001));
}
// End of helper function for smoothing ============================================================

/*
You need to call "SetEnvSmoothProperties" or  "SetLPSmoothProperties" to calculate the coeffs for smoothing.
This should be called every time you need to change "attackMS", "releaseMS" or "smoothMS" parameter

"expectedFps" is used only to calculate coeefs for attack, release and smooth.
You should supply to max FPS and video export FPS to "expectedFps"
*/

void SetEnvSmoothProperties(float attackMS, float releaseMS, float expectedFps)
{
	attackCoeff = coeffCalc(attackMS, expectedFps);
	releaseCoeff = coeffCalc(releaseMS, expectedFps);
}

void SetLPSmoothProperties(float smoothMS, float expectedFps)
{
	smoothCoeff = coeffCalc(smoothMS, expectedFps);
}

/*
You can call "EnvSmooth" or "LPSmooth" to smooth float value.

"EnvSmooth" uses coeff calculated with "SetEnvSmoothProperties"
"LPSmooth" uses coeff calculated with "SetLPSmoothProperties"
*/

float EnvSmooth(float lastOut, float in)
{
	if (abs(in) > abs(lastOut))
	lastOut = attackCoeff * (lastOut - in) + in;
	else
	lastOut = releaseCoeff * (lastOut - in) + in;

	return lastOut;
}

float LPSmooth(float lastOut, float in)
{
	lastOut = smoothCoeff * (lastOut - in) + in;

	return lastOut;
}

/*
"VercticalSmooth" smooths every array value individually.
You will need to supply "uniqueBuffer" that will be same size as "in" and this buffer should be global
and you must not alter it's values.

After "VercticalSmooth" function call "in" and "uniqueBuffer" will hold same data,
but it is only modifying "in" safe.
*/

void VercticalSmooth(float[] in, float[] uniqueBuffer, int arraySize, float attackMS, float releaseMS, float expectedFps)
{
	SetEnvSmoothProperties(attackMS, releaseMS, expectedFps);
	for(int i = 0; i < arraySize; i++)
	{
		uniqueBuffer[i] = EnvSmooth(uniqueBuffer[i], in[i]);
		in[i] = uniqueBuffer[i];
	}
}

/*
"AsymmetricalVercticalSmooth" is similar to "VercticalSmooth"
but difference is that it can smooth array by setting coeffs for first and last point separately
and lineary interpolating coeffs inbetween.

In other words when you use this to smooth spectogram you can set different coeffs for bass and for highs.
Since in spectogram bass is always less responsive, you can use more smoothing on highs to get uniform feel.
TODO: Add log interpolation for X axis...?
*/

void AsymmetricalVercticalSmooth(float[] in, float[] uniqueBuffer, int arraySize, float attackStartMS, float releaseStartMS, float attackEndMS, float releaseEndMS, float expectedFps)
{
	float attackSlopeAdd = 0;
	float releaseSlopeAdd = 0;
	float attack = attackStartMS;
	float release = releaseStartMS;

	if (attackEndMS - attackStartMS != 0)
	{
		attackSlopeAdd = (attackEndMS - attackStartMS) / arraySize;
	}
	if (releaseEndMS - releaseStartMS != 0)
	{
		releaseSlopeAdd = (releaseEndMS - releaseStartMS) / arraySize;
	}

	for(int i = 0; i < arraySize; i++)
	{
		SetEnvSmoothProperties(attack, release, expectedFps);
		uniqueBuffer[i] = EnvSmooth(uniqueBuffer[i], in[i]);
		in[i] = uniqueBuffer[i];

		attack = attack + attackSlopeAdd;
		release = release + releaseSlopeAdd;
	}
}


/*
"HorizontalSmooth" will smooth array by X axis.
For symetrical smooth we need to do Low Pass smoothing from left to right and then from right to left.

Smoothing is independed of "in" size.
*/

void HorizontalSmooth(float[] in, int arraySize, float smoothValue, int smoothEdges)
{
	float valueOut;

	if (smoothEdges == 0)
	{
		valueOut = in[0];
	}
	else
	{
		valueOut = 0;
	}

	SetLPSmoothProperties(smoothValue, arraySize);

	// Horizontal smoothing using LP filter
	for(int i = 0; i < arraySize; i++)
	{
		// From left to right
		valueOut = LPSmooth(valueOut, in[i]);
		in[i] =  valueOut;
	}

	if ( (smoothEdges == 0) && (arraySize>0) )
	{
		valueOut = in[arraySize - 1];
	}
	else
	{
		valueOut = 0;
	}

	for(int i = arraySize-1; i >= 0; i--)
	{
		// From right to left
		valueOut = LPSmooth(valueOut, in[i]);
		in[i] =  valueOut;
	}
}

// Helper functions for "ResampleArray" ========================================================================================================
inline float NearestMiddle(float y0, float y1, float x)
{
	if (x > 0.5) return y1;
	else return y0;
}

inline float Linear(float y0, float y1, float x)
{
	return(y0*(1 - x) + y1*x);
}

inline float Cosine(float y0, float y1, float x)
{
	float mu2;

	mu2 = (1 - cos(x*3.14159265358979323846)) / 2;
	return(y0*(1 - mu2) + y1*mu2);
}

inline float Cubic(float yminus1, float y0, float y1, float y2, float x)
{
	float a0, a1, a2, a3;
	float mu2;

	mu2 = x*x;
	a0 = y2 - y1 - yminus1 + y0;
	a1 = yminus1 - y0 - a0;
	a2 = y1 - yminus1;
	a3 = y0;

	return(a0*x*mu2 + a1*mu2 + a2*x + a3);
}

inline float max(float v1, float v2)
{
	return v1 >= v2 ? v1 : v2;
}

inline float min(float v1, float v2)
{
	return v1 <= v2 ? v1 : v2;
}

inline float GetInterpolationPosition(float input)
{
	return input - floor(input);
}

inline float GetValueFromIndex(float[] in, int index, int arraySizeMinusOne)
{
	return in[min(max(index, 0), arraySizeMinusOne)];
}

inline float GetValue(float[] in, int index, float position, int arraySizeMinusOne, int interpolate, int processing)
{
	float  minusOne, zero, one, two;
	int minusOneIndex, zeroIndex, oneIndex, twoIndex;

	zeroIndex = index;
	oneIndex = index + 1;
	twoIndex =index + 2;
	minusOneIndex = index - 1;

	// Get values
	zero = GetValueFromIndex(in, zeroIndex, arraySizeMinusOne);
	one = GetValueFromIndex(in, oneIndex, arraySizeMinusOne);

	// Get these only if we need it
	if (interpolate > 3)
	{
		minusOne = GetValueFromIndex(in, minusOneIndex, arraySizeMinusOne);
		two = GetValueFromIndex(in, twoIndex, arraySizeMinusOne);
	}

	switch (interpolate)
	{
		// Two point interpolation
	case 0: return zero;
	case 1: return NearestMiddle(zero, one, position);
	case 2: return Linear(zero, one, position);
	case 3: return Cosine(zero, one, position);

		// Four points interpolation
	case 4: return Cubic(minusOne, zero, one, two, position);
	}
	return 0;
}

inline float InterpolateValue(float[] in, float index, float increasement, int arraySizeMinusOne, int interpolate, int processing, int genericInnerProcessing)
{
	float minValue;
	float maxValue;
	float average;

	maxValue = 3.4e-38; // Set minimum float value
	minValue = 3.4e38; // Set maximum float value
	average = 0;

	if (processing != 0 && increasement >= 1)
	{
		float innerIndex = index;
		int intInnerIndex = index;
		int intNextIndex = index + increasement;

		float innerValues = intNextIndex - intInnerIndex;
		float increaseInnerIndexBy = increasement / innerValues;
		float increaseMultiplyer = 0;

		int loopSize = intNextIndex - intInnerIndex;

		// If last value in array
		if (loopSize == 0)
		{
			if (processing == 1) maxValue = GetValue(in, intInnerIndex, GetInterpolationPosition(innerIndex), arraySizeMinusOne, interpolate, processing);
			else if (processing == 2) minValue = GetValue(in, intInnerIndex, GetInterpolationPosition(innerIndex), arraySizeMinusOne, interpolate, processing);
			else if (processing == 3) average = GetValue(in, intInnerIndex, GetInterpolationPosition(innerIndex), arraySizeMinusOne, interpolate, processing);
		}
		else while (intInnerIndex < intNextIndex)
		{
			if (genericInnerProcessing == 1)
			{
				if (intInnerIndex <= arraySizeMinusOne)
				{
					if (processing == 1) maxValue = max(maxValue, in[intInnerIndex]);
					else if (processing == 2) minValue = min(minValue, in[intInnerIndex]);
					else if (processing == 3) average = average + in[intInnerIndex];
				}
			}
			else
			{
				if (processing == 1) maxValue = max(maxValue, GetValue(in, intInnerIndex, GetInterpolationPosition(innerIndex), arraySizeMinusOne, interpolate, processing));
				else if (processing == 2) minValue = min(minValue, GetValue(in, intInnerIndex, GetInterpolationPosition(innerIndex), arraySizeMinusOne, interpolate, processing));
				else if (processing == 3) average = average + GetValue(in, intInnerIndex, GetInterpolationPosition(innerIndex), arraySizeMinusOne, interpolate, processing);
			}
			increaseMultiplyer++;
			innerIndex = index + increaseInnerIndexBy * increaseMultiplyer;
			intInnerIndex = innerIndex;
		}

		if (processing == 1) return maxValue;
		else if (processing == 2) return minValue;
		else if (processing == 3) return average / max(loopSize, 1);
	}
	else
	{
		return GetValue(in, index, GetInterpolationPosition(index), arraySizeMinusOne, interpolate, processing);
	}
	return 0;
}
// End of helper functions for "ResampleArray" ====================================================================================================

/*
Call this function to resample array

in = input float array
out = output float array. It can't be same as input since interpolation will use multiple points in input array
inSize = size of input array. You can resample only part of the array.
outSize = size of output array. You can resample only part of the array.

interpolateMethod = This will determent interpolation method. See table below:

Two point interpolation
0 = nearest_start
1 = nearest_middle
2 = linear
3 = cosine

Four points interpolation
4 = cubic

innerProcessing = This will determent if we need to take in considaration inner walues when downsampling. See table below:

0 = off
1 = max
2 = min
3 = average
*/

inline void ResampleArray(float[] in, float[] out, int inSize, int outSize, int interpolateMethod, int innerProcessing, int genericInnerProcessing)
{
	if (inSize == 0 || outSize == 0) return;

	int inSizeMinusOne = inSize - 1;
	int outSizeMinusOne = outSize - 1;

	float floatInSizeMinusOne = inSizeMinusOne;
	float floatOutSizeMinusOne = outSizeMinusOne;

	float increasement = floatInSizeMinusOne / floatOutSizeMinusOne;

	float index = 0;
	for (int i = 0; i < outSize; )
	{
		out[i] = InterpolateValue(in, index, increasement, inSizeMinusOne, interpolateMethod, innerProcessing, genericInnerProcessing);
		i++;
		index = increasement * i;
	}
}

// Bass Pump function
void BassPump(float[] in, int inSize, float freq, float mix)
{
	int bassFreqSize = inSize * freq;
	float maxValue = abs(in[0]);

	for (int i = 0; i < bassFreqSize; i++)
	{
		maxValue = max(maxValue, abs(in[i]));
	}

	for (int i = 0; i < inSize; i++)
	{
		in[i] = (in[i]*(1 - mix) + (in[i] * maxValue)*mix);
	}
}]]>
      </Source>
    </ZLibrary>
  </OnLoaded>
  <OnUpdate>
    <ZExpression>
      <Expression>
<![CDATA[/*
[0]=Alpha
[1]=Hue
[2]=Saturation
[3]=Lightness
[4]=Size
[5]=Pos X
[6]=Pos Y
[7]=Bands - In EQ model OnRender zExpression
[8]=Rotation
[9]=Gradient
[10]=Gap -  In EQ model OnRender zExpression
[11]=Power - In EQ model OnRender zExpression
[12]=Polarity - In EQ model OnRender zExpression
[13]=Hide Floor - In EQ model OnRender zExpression
[14]=Round Corners - In EQ model OnRender zExpression
[15]=Smoothing - In EQ model OnRender zExpression
[16]=Fill Mode
[17]=BitmapX
[18]=BitmapY
[19]=BitmapScaleX
[20]=BitmapScaleY
[21]=Peak Brightnes - In EQ model OnRender zExpression
[22]=Smooth Band Curve - In EQ model OnRender zExpression
[23]=Attack - In EQ model OnRender zExpression
[24]=Decay - In EQ model OnRender zExpression
[25]=Flood peaks - In EQ model OnRender zExpression
[26]=Bass Boost - In EQ model OnRender zExpression
[27]=Boost Start - In EQ model OnRender zExpression
[28]=Boost Tension - In EQ model OnRender zExpression
*/


res=vector2(app.ViewportWidth,app.ViewportHeight);
uAlpha=Parameters[0];
uCol=hsv(Parameters[1]*360,Parameters[2]*100,(1-Parameters[3])*100);
EQ.Scale=Parameters[4]*5.0;
EQ.Position.X=(Parameters[5]-.5)*20.0;
EQ.Position.Y=(Parameters[6]-.5)*10.0;
EQ.Rotation.Z=Parameters[8];
uGradient=(Parameters[9]-.5)*2.0;
uFill=Parameters[16];
uBitmapX=-2f*(Parameters[17]-.5);
uBitmapY=-2f*(Parameters[18]-.5);
uBitmapScaleX=6.0-Parameters[19]*5.0;
uBitmapScaleY=6.0-Parameters[20]*5.0;]]>
      </Expression>
    </ZExpression>
    <SpawnModel Model="EQ" SpawnStyle="1"/>
  </OnUpdate>
  <Content>
    <Shader Name="MultibandEQ">
      <VertexShaderSource>
<![CDATA[varying vec2 pos;
varying vec4 bandPeak;

void main() {
    bandPeak=gl_Color;
    vec4 p=gl_Vertex;
    pos=vec2(p);
    gl_Position = gl_ModelViewProjectionMatrix*p;
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[varying vec2 pos;
uniform float alpha,grad,useTex,bitmapX,bitmapY,bitmapScaleX,bitmapScaleY;
uniform vec3 col;
uniform vec2 iResolution;
uniform sampler2D tex1;
varying vec4 bandPeak;



void main() {
  vec4 bandColor;
  vec3 bands;
  vec2 texScaleOffset=vec2(bitmapScaleX,bitmapScaleY);
  vec2 texOffset=vec2(bitmapX,bitmapY);
  vec2 uv = (gl_FragCoord.xy / iResolution.xy*texScaleOffset);
  uv+=texOffset;
  if(useTex>0.5)bandColor=texture2D(tex1,uv)-(vec4(clamp(pos.x*grad,0.1,1.0)));
  else bandColor=vec4(col,1.0)-(vec4(clamp(pos.x*grad,0.1,1.0)));
  bands=bandColor.rgb*vec3(bandPeak.g);
  bandColor.rgb=clamp(bandColor.rgb-vec3(bandPeak.r),0.0,1.0)+bands;
  gl_FragColor=vec4(bandColor.rgb,1.0-alpha);
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="alpha" VariableRef="uAlpha"/>
        <ShaderVariable VariableName="col" VariableRef="uCol"/>
        <ShaderVariable VariableName="grad" VariableRef="uGradient"/>
        <ShaderVariable VariableName="iResolution" VariableRef="res"/>
        <ShaderVariable VariableName="useTex" VariableRef="uFill"/>
        <ShaderVariable VariableName="bitmapX" VariableRef="uBitmapX"/>
        <ShaderVariable VariableName="bitmapY" VariableRef="uBitmapY"/>
        <ShaderVariable VariableName="bitmapScaleX" VariableRef="uBitmapScaleX"/>
        <ShaderVariable VariableName="bitmapScaleY" VariableRef="uBitmapScaleY"/>
      </UniformVariables>
    </Shader>
    <Model Name="EQ" Scale="2 0.5 1">
      <OnRender>
        <UseMaterial Material="matEQ"/>
        <ZExpression>
          <Expression>
<![CDATA[int fps = clamp(App.FpsCounter, 15, 60);
float bandsParameter=Parameters[7];
int bands=floor(bandsParameter*SpecBandArray.SizeDim1);
bands = bands < 2 ? 2 : bands;
float by=1.0;
float attack = 1000 * (Parameters[23] * Parameters[23]);
float decay = 1000 * (Parameters[24] * Parameters[24]);
float taper = Parameters[25];
float bx=by*2.0;
float rounding=Parameters[14]*.5;
int taperEdges = Parameters[15];

float barWidth;

if (taperEdges == 1)
{
   barWidth=(bx*2.0)/bands;
}
else
{
   if (rounding == 0.0)
   {
      barWidth=(bx*2.0)/(bands-1);
   }
   else
   {
      barWidth=(bx*2.0)/bands;
   }
}

float step=barWidth;
float gap=barWidth*parameters[10];
float barStartX=-1*bx + barWidth;
float pow=Parameters[11]*3.0;
barWidth-=gap;
float endPoint;
float polarity=Parameters[12];
float smoothing= 20 * Parameters[22];
float pb=Parameters[21];
float yLast = 0.0;
float yLast_Inv = 0.0;
float bassBoost = 5 * Parameters[26] + 1;
float boostStart = 100 * Parameters[27];
float boostTension = 20 * Parameters[28] + 1;

// Resample array
ResampleArray(SpecBandArray, SpecBandArrayResampled, SpecBandArray.SizeDim1, bands, 0, 1, 1);

// Add vertical smoothing
VercticalSmooth(SpecBandArrayResampled, SpecBandVSmoothBuffer, bands, attack, decay, fps);

// Bass pump
BassPump(SpecBandArrayResampled, bands, Parameters[30], Parameters[29]);

// Add horizontal smoothing
HorizontalSmooth(SpecBandArrayResampled, bands, smoothing, 0);

// Add slope
ArraySlopeAdvanced(SpecBandArrayResampled, bands, bassBoost, boostStart, boostTension);


float x=barStartX;

glBegin(GL_TRIANGLES);
for (int count = 0; count<bands; count++)
{
   float amp = SpecBandArrayResampled[count] * pow;

   float abfr = amp;
   if (taper <= 0 || bandsParameter < 0.33599853515625){
      abfr = amp;
   } else {
      if (bandsParameter < 0.50201416015625) {
         abfr = amp*clamp((((bands*2)-count)/((bands*2)*taper)),0,1);
      } else {
         abfr = amp*clamp(((bands-count)/(bands*taper)),0,1);
      }
   }

   float ampDown = -1f*abfr*polarity;
   float ampUp = abfr-abs(ampDown);
   if (rounding > 0.0){
      yLast = ampUp;
      yLast_Inv = ampDown;
   }

   if (taperEdges == 0 && count == 0 && rounding == 0.0)
   {
      yLast = ampUp;
      yLast_Inv = ampDown;
   }

   if (taperEdges == 1 && count == bands - 1 && rounding == 0.0)
   {
      endpoint = x;
      continue;
   }

  if (taperEdges == 0 && count == bands - 1 && rounding == 0.0)
   {
      continue;
   }


   glColor4f(pb,clamp(pb*SpecBandArrayResampled[count]*(1.25-(Parameters[29])*.25)*(1.0+Parameters[29]*1.5),.25,1.0),0.0,1.0);  //Peak Bightness sent to shader as gl_Color

   if (taperEdges == 0 && count == 1 && rounding == 0.0)
   {
      glVertex2f(x-barWidth,yLast);
      glVertex2f(x-barWidth,yLast_Inv);
      glVertex2f(x,ampUp);
   }

   glVertex2f(x,ampUp);
   glVertex2f(x-barWidth,yLast_Inv);
   glVertex2f(x,ampDown);
   glVertex2f(x,ampUp);
   glVertex2f(x-barWidth,yLast);
   glVertex2f(x-barWidth,yLast_Inv);

   if (taperEdges == 1 && count == bands - 2 && rounding == 0.0)
   {
      glVertex2f(x + gap,ampUp);
      glVertex2f(x + gap,ampDown);
      glVertex2f(x + barWidth + gap,0);
   }

/* Ignore rounding for now, it takes too much CPU
   if (rounding>0.0&&Parameters[13]<.5){
      float cxR=x-barWidth*rounding;
      float cxL=x-barWidth*(1f-rounding);
      float cy=ampUp;
      float cr=barWidth*rounding;
      float arcRes=15.0;
      if (polarity!=1.0){
         for (float deg=0f; deg<90f; deg+=arcRes){
            float rad2=deg*pi/180.0;
            float rad1=(deg+arcRes)*pi/180.0;
            float vx1=cos(rad1)*cr+cxR;
            float vx2=cos(rad2)*cr+cxR;
            float vy1=sin(rad1)*cr+cy;
            float vy2=sin(rad2)*cr+cy;
            glVertex2f(cxR,cy);
            glVertex2f(vx2,vy2);
            glVertex2f(vx1,vy1);
         }

         for (float deg=90f; deg<180f; deg+=arcRes){
            float rad2=deg*pi/180.0;
            float rad1=(deg+arcRes)*pi/180.0;
            float vx1=cos(rad1)*cr+cxL;
            float vx2=cos(rad2)*cr+cxL;
            float vy1=sin(rad1)*cr+cy;
            float vy2=sin(rad2)*cr+cy;
            glVertex2f(cxL,cy);
            glVertex2f(vx2,vy2);
            glVertex2f(vx1,vy1);
         }
         if(rounding<.5){
            glVertex2f(cxR,ampUp+cr);
            glVertex2f(cxL,ampUP);
            glVertex2f(cxR,ampUP);
            glVertex2f(cxR,ampUp+cr);
            glVertex2f(cxL,ampUp+cr);
            glVertex2f(cxL,ampUp);
         }
      }

      cy=ampDown;
      cr=barWidth*rounding;

      if (polarity!=0.0){
         for (float deg=180f; deg<270f; deg+=arcRes){
            float rad2=deg*pi/180.0;
            float rad1=(deg+arcRes)*pi/180.0;
            float vx1=cos(rad1)*cr+cxL;
            float vx2=cos(rad2)*cr+cxL;
            float vy1=sin(rad1)*cr+cy;
            float vy2=sin(rad2)*cr+cy;
            glVertex2f(cxL,cy);
            glVertex2f(vx2,vy2);
            glVertex2f(vx1,vy1);
         }

         for (float deg=270f; deg<360f; deg+=arcRes){
            float rad2=deg*pi/180.0;
            float rad1=(deg+arcRes)*pi/180.0;
            float vx1=cos(rad1)*cr+cxR;
            float vx2=cos(rad2)*cr+cxR;
            float vy1=sin(rad1)*cr+cy;
            float vy2=sin(rad2)*cr+cy;
            glVertex2f(cxR,cy);
            glVertex2f(vx2,vy2);
            glVertex2f(vx1,vy1);
         }

         if(rounding<.5){
            glVertex2f(cxR,ampDown);
            glVertex2f(cxL,ampDown-cr);
            glVertex2f(cxR,ampDown-cr);
            glVertex2f(cxR,ampDown);
            glVertex2f(cxL,ampDown);
            glVertex2f(cxL,ampDown-cr);
         }
      }
   }
*/

   endpoint = x;
   yLast = ampUp;
   yLast_Inv = ampDown;
   x+=step;
}
glEnd();

if(Parameters[13]<.5){
   glLineWidth(1.0);
   glBegin(GL_LINES);
   glVertex2f(barStartX-barWidth,0.0);
   glVertex2f(endpoint,0.0);
   glEnd();
}]]>
          </Expression>
        </ZExpression>
      </OnRender>
    </Model>
    <Material Name="matEQ" Blend="1" Shader="MultibandEQ">
      <Textures>
        <MaterialTexture Texture="Bitmap1" TexCoords="1"/>
      </Textures>
    </Material>
    <Group Comment="Uniform">
      <Children>
        <Variable Name="uAlpha"/>
        <Variable Name="uCol" Type="7"/>
        <Variable Name="uGradient"/>
        <Variable Name="res" Type="6"/>
        <Variable Name="uFill"/>
        <Variable Name="uBitmapX"/>
        <Variable Name="uBitmapY"/>
        <Variable Name="uBitmapScaleX"/>
        <Variable Name="uBitmapScaleY"/>
      </Children>
    </Group>
    <Group Comment="FLStudio">
      <Children>
        <Array Name="SpecBandArray"/>
        <Variable Name="MAXBAND" Type="1"/>
        <Array Name="Parameters" SizeDim1="31" Persistent="255">
          <Values>
<![CDATA[78DA636000829365F6218F55ED414C066569106DCF5032D98E61C5313B0608B067906BB53336DE0CE343C450D908FC2318422F91B66338E303D6932FC46CCB20126977F8EB0A20BFC1EEEC9933B600988315FF]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Alpha
Hue
Saturation
Lightness
Size
Position X
Position Y
Bands
Rotation
Gradient
Gap Width
Power
Polarity
Hide Floor @checkbox
Draw @list: Curve,Bars
Taper Edges @checkbox
Enable Bitmap @checkbox
Bitmap X
Bitmap Y
Bitmap Scale X
Bitmap Scale Y
Peak Brightness
Smooth Curve
Attack
Decay
Taper high-end
Bass Boost
Boost Start
Boost Tension
Bass Pump
Pump Start]]>
          </StringValue>
        </Constant>
        <Array Name="SpecBandArrayResampled"/>
        <Array Name="SmoothBand"/>
        <Array Name="SpecBandVSmoothBuffer"/>
        <Variable Name="prevBands"/>
      </Children>
    </Group>
    <Bitmap Name="Bitmap1">
      <Producers>
        <BitmapCells PointsPlacement="2" UsedMetrics="4" RandomSeed="67" BorderPixels="0" PointCount="24"/>
      </Producers>
    </Bitmap>
    <Constant Name="AuthorInfo" Type="2" StringValue="StevenM,Dubswitcher,Youlean"/>
  </Content>
</ZApplication>
