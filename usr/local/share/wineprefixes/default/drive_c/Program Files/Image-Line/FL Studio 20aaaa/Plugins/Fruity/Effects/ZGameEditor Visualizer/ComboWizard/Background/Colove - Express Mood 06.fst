FLhd   0 * ` FLdtY2  �20.6.9.1657 �y  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��c�1  ﻿[General]
GlWindowMode=1
LayerCount=19
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,5,3,1,2,4,9,8,24,23,25,22,28,11,26,27,17,7,20

[AppSet0]
App=Background\SolidColor
FParamValues=0.356,0.648,0.636,0.728
ParamValues=356,648,636,728
ParamValuesBackground\FourCornerGradient=0,1000,664,732,1000,898,736,1000,504,620,1000,150,812,1000
Enabled=1
Collapsed=1
Name=Background XSTry-Ray

[AppSet5]
App=Canvas effects\Flaring
FParamValues=0.524,1,1,0.092,0.952,0.5,0.5,0.653,0,2,0.112
ParamValues=524,1000,1000,92,952,500,500,653,0,2,112
Enabled=1
Collapsed=1
Name=Fatele Mod

[AppSet3]
App=Canvas effects\SkyOcean
FParamValues=0,0.96,1,0,0,0.5,0.5,0.708,0.34,0.736,1,1,0.576,0.68
ParamValues=0,960,1000,0,0,500,500,708,340,736,1000,1000,576,680
Enabled=1
Collapsed=1
ImageIndex=1
Name=React 1

[AppSet1]
App=Background\FogMachine
FParamValues=0.672,0.588,1,0.064,0.456,0.5,0.5,0.5,0.5,0.636,0,0.516
ParamValues=672,588,1000,64,456,500,500,500,500,636,0,516
ParamValuesBackground\Youlean Background MDL=1000,1000,1000,420,884,424,198,320,648,558,376,696,800,664,1000,0,500,4,0,500
Enabled=1
Collapsed=1
ImageIndex=1
Name=Liner Color

[AppSet2]
App=Canvas effects\SkyOcean
FParamValues=0,0.856,1,1,0,0.5,0.5,0,0.404,0.656,1,0.568,0.048,0.488
ParamValues=0,856,1000,1000,0,500,500,0,404,656,1000,568,48,488
ParamValuesBlend\VideoAlphaKey=0,636,1000,0,1000,656,344,0,0,0,474,92,340
Enabled=1
Collapsed=1
ImageIndex=1
Name=React 2

[AppSet4]
App=Canvas effects\SkyOcean
FParamValues=0,0.592,1,1,0.232,0.5,0.5,0.708,0.34,0.568,0.864,0.924,0,0
ParamValues=0,592,1000,1000,232,500,500,708,340,568,864,924,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=React 3

[AppSet9]
App=Canvas effects\Rain
FParamValues=0.896,1,1,0.648,0.216,0.5,0.5,0.5,0.5,0,0.736,1
ParamValues=896,1000,1000,648,216,500,500,500,500,0,736,1000
ParamValuesCanvas effects\OverlySatisfying=0,0,0,0,600,500,500,100,100,1000,500
Enabled=1
Collapsed=1
ImageIndex=1
Name=Modliner X1

[AppSet8]
App=Postprocess\Blooming
FParamValues=0,0,0,1,0.5,0.552,0.928,1,0
ParamValues=0,0,0,1000,500,552,928,1000,0
ParamValuesFeedback\WarpBack=0,0,0,0,500,500,200
ParamValuesParticles\ReactiveFlow=0,125,500,250,636,0,500,100,500,500,500,500,500,0,0,500,200,500,125,500,500,500,1000,1000,100
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPeak Effects\Fluidity=912,488,1000,0,0,264,8
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesPostprocess\ColorCyclePalette=0,0,0,0,0,1000,0,500,0,0,0
ParamValuesParticles\fLuids=0,0,0,0,884,492,476,0,0,0,392,628,500,780,708,766,500,608,468,728,500,500,168,208,500,0,0,250,248,0,0,0
ParamValuesFeedback\BoxedIn=0,0,0,1000,1000,236,504,500,0,500
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesCanvas effects\SkyOcean=0,592,1000,1000,232,500,500,708,340,568,864,924,0
ParamValuesPhysics\Columns=200,300,1000,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesFeedback\FeedMe=0,0,0,1000,500,1000,488
ParamValuesPostprocess\Dot Matrix=500,336,500,0,960,856,1000,1000
ParamValuesPostprocess\Luminosity=304
ParamValuesPostprocess\Notebook Drawings=0,200,756,250,500,750,1000,992,250,500,0,500,0
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,0,536,892,500,356,948,500,500
ParamValuesPeak Effects\Linear=0,0,1000,0,486,388,548,392,0,500,260,766,484,1000,0,0,0,500,500,500,500,828,500,0,20,192,0,700,206,398,660
ParamValuesPeak Effects\Stripe Peeks=0,0,0,0,1000,0,138,468,500,0,250,500,700,1000,500,150,300,200,200,300,1000,0,0
ParamValuesBackground\Youlean Background MDL=1000,72,1000,0,500,500,50,320,0,170,300,120,1000,1000,0,300,500,0,0,500
ParamValuesTunnel\Youlean Tunnel=0,0,0,1000,1000,500,500,0,0,0,0,250,500,250,500,500,500,500,500,0,0,250,0,500,500
ParamValuesPeak Effects\JoyDividers=0,0,0,1000,700,500,500,0,500,100,600,650,510
ParamValuesPostprocess\Blur=844
ParamValuesPostprocess\FrameBlur=60,664,684,632,750,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
Name=EFX

[AppSet24]
App=Tunnel\Youlean Tunnel
FParamValues=0,0,0,1,1,0.5,0.5,0,0,0,0,0.13,0.472,0.25,0.5,0.5,0.5,0.5,0.5,0,0,0.338,1,0.5,0.5
ParamValues=0,0,0,1000,1000,500,500,0,0,0,0,130,472,250,500,500,500,500,500,0,0,338,1,500,500
ParamValuesHUD\HUD Prefab=27,0,500,0,0,500,500,158,1000,1000,444,0,500,1000,368,0,1000,1000
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPostprocess\Youlean Audio Shake=0,250,0,0,200,0,100,200,500,500,500,500,0,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesImage effects\ImageTileSprite=4,4,0,0,0,0,4,250,250,500,500,500,500,0,0,0,0
ParamValuesPostprocess\ColorCyclePalette=0,0,0,0,0,1000,0,500,0,0,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesHUD\HUD Image=0,0,500,500,1000,1000,500,444,500,0,0,1000,1000,0,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesBackground\Youlean Background MDL=1000,892,0,0,500,500,50,320,0,2,300,120,1000,1000,0,300,500,0,0,500
ParamValuesPostprocess\Youlean Pixelate=628,0,0,0
ParamValuesImage effects\Image=0,0,0,1000,672,500,500,0,0,0,0,0,1000,1000
ParamValuesObject Arrays\BallZ=0,0,1000,1000,392,500,500,818,500,500,500,500,500
ParamValuesPostprocess\Blur=1000
Enabled=1
Collapsed=1
ImageIndex=1
Name=Core Effect

[AppSet23]
App=Peak Effects\Stripe Peeks
FParamValues=0,4,0,0,0,0,0.142,0.5,0.5,0,0.294,0.5,0.612,0,0.5,0,0.356,0.108,0.188,1,1,0,0
ParamValues=0,4,0,0,0,0,142,500,500,0,294,500,612,0,500,0,356,108,188,1,1,0,0
ParamValuesFeedback\WarpBack=500,0,0,0,500,500,24
ParamValuesPeak Effects\Reactive Sphere=0,1000,973,0,773,500,500,252,108,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesPeak Effects\ReflectedPeeks=0,60,712,104,336,500,500,744,24
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,1000,500,500,500,500,500,500,500
ParamValuesHUD\HUD 3D=0,0,500,500,500,500,500,500,500,500,500,500,500,500,0,500,500,500,500,500,500,500,500,1000,1000,0,0,1000,1000,0
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,0,204,532,408,668,500,500
Enabled=1
Collapsed=1
Name=EQ
LayerPrivateData=780163608081067B10CB2F49004837D84BEBDFB505F1D50D39C07C109B18306BE64CBBB3677CE018A467D64C493B9038880D92FBF2F70A50FE8CDD0E3957FB45AEDBEC407609353BD8AD38761A6C17503DDC4E0039F0243D

[AppSet25]
App=Feedback\BoxedIn
FParamValues=0,0,0,1,1,0.252,0.472,0,0.116,0.972
ParamValues=0,0,0,1000,1000,252,472,0,116,972
ParamValuesHUD\HUD Prefab=18,0,500,0,1000,500,500,664,1000,1000,444,0,500,0,368,0,1000,1000
Enabled=1
Collapsed=1
Name=Cinema Effect

[AppSet22]
App=Postprocess\Youlean Audio Shake
FParamValues=0,0.206,0.176,0.052,0.348,0.032,0.064,0.196,0.5,0.5,0.484,0.42,0,0
ParamValues=0,206,176,52,348,32,64,196,500,500,484,420,0,0
ParamValuesFeedback\WarpBack=0,0,0,0,500,500,20
ParamValuesImage effects\ImageMasked=1000,0,412,20,820,500,500,200,200,1000,0
ParamValuesImage effects\ImageSphereWarp=0,500,0,1000,250,400,1000,204,260,500
ParamValuesPeak Effects\StereoWaveForm=0,0,0,1000,0,0,1000,1000,1000,1000,1000,500,500,500,500,500,500,500,180,500,500,1000,12,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesFeedback\WormHoleEclipse=0,0,0,0,0,0,500,400,436,500,500
ParamValuesImage effects\ImageTileSprite=4,4,0,0,0,0,4,250,250,500,500,500,500,0,0,0,0
ParamValuesHUD\HUD Grid=0,500,0,1000,500,500,1000,1000,500,444,500,1000,500,100,0,500,1000,100,500,300,0,1000
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesPostprocess\ColorCyclePalette=0,0,0,0,0,1000,0,500,0,0,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesHUD\HUD Image=0,0,500,500,1000,1000,500,444,500,0,0,1000,1000,0,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPostprocess\AudioShake=16,0,0,328,244,900
ParamValuesImage effects\ImageSphinkter=0,0,0,1000,1000,500,500,500,500,500,500,500,500,0,0
ParamValuesImage effects\Image=580,0,0,1000,1000,480,500,112,0,0,0,0,1000,1000
ParamValuesHUD\HUD 3D=0,0,500,500,500,500,500,500,500,500,500,500,500,500,0,500,500,500,500,500,500,500,500,1000,1000,0,0,1000,1000,0
Enabled=1
Collapsed=1
Name=Shake

[AppSet28]
App=Feedback\FeedMeFract
FParamValues=0,0,0,0.552,0.32,0.068
ParamValues=0,0,0,552,320,68
ParamValuesFeedback\BoxedIn=0,0,0,1000,1000,0,0,1000,0,500
Enabled=1
Collapsed=1
Name=Mirror

[AppSet11]
App=Feedback\BoxedIn
FParamValues=0,0,0,1,0.268,0,0,0.5,0.264,0.544
ParamValues=0,0,0,1000,268,0,0,500,264,544
ParamValuesFeedback\FeedMeFract=0,0,0,248,28,68
ParamValuesCanvas effects\ShimeringCage=0,0,0,0,160,500,500,0,0,0,0,0,0,76
ParamValuesCanvas effects\FreqRing=500,500,1000,0,312,500,500,92,140,312,315,500,500,320
Enabled=1
Collapsed=1
Name=Move to Stars

[AppSet26]
App=Postprocess\Point Cloud High
FParamValues=0.376,0.77,0.694,0.376,0.508,0.692,0.496,0.431,0.17,0.809,0.2323,0,0.108,0,0
ParamValues=376,770,694,376,508,692,496,431,170,809,232,0,108,0,0
ParamValuesFeedback\SphericalProjection=0,0,0,0,618,1000,0,590,500,880,0,333,530,1000,56,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesFeedback\BoxedIn=0,0,0,1000,208,0,252,464,64,824
ParamValuesFeedback\70sKaleido=0,0,0,1000,532,0
ParamValuesFeedback\FeedMe=0,0,0,1000,1000,1000,164
Enabled=1
Collapsed=1
ImageIndex=1
Name=Parter

[AppSet27]
App=Postprocess\Youlean Kaleidoscope
FParamValues=0.5,0.5,0,0.36,1,0,0.5,0.5,0.5,0,0
ParamValues=500,500,0,360,1000,0,500,500,500,0,0
ParamValuesHUD\HUD 3D=0,0,500,500,500,500,500,500,500,500,500,500,500,500,0,500,500,500,500,500,500,500,500,1000,1000,0,0,1000,1000,0
ParamValuesPostprocess\Youlean Image Rotation=896,500,0,336,524,664,0,1
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,0,0,1000,668,588,500,500
ParamValuesHUD\HUD Image=0,0,500,500,1000,1000,500,444,500,0,0,1000,1000,0,1000
ParamValuesFeedback\SphericalProjection=60,0,0,0,770,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
Name=Magical

[AppSet17]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=1,0.12,0.414,0.5065,0.66,0,0,1
ParamValues=1000,120,414,506,660,0,0,1
ParamValuesScenes\Alien Thorns=0,500,250,500,2,500,500,0
ParamValuesTerrain\CubesAndSpheres=0,0,0,0,236,500,500,650,380,400,552,568,684,1000,1000,1000,0,1000
ParamValuesPostprocess\Blooming=0,0,0,1000,500,800,500,464,0
ParamValuesPostprocess\Youlean Audio Shake=0,250,0,0,200,0,100,200,500,500,500,500,0,0
ParamValuesPostprocess\ColorCyclePalette=0,0,0,0,0,1000,0,500,0,0,0
ParamValuesPostprocess\Point Cloud High=0,434,346,520,492,472,512,491,50,265,0,0,224,0,333
ParamValuesTunnel\Youlean Tunnel=0,0,0,1000,912,500,500,0,0,0,0,250,500,674,676,500,500,500,500,0,4,1000,1000,500,500
ParamValuesTerrain\GoopFlow=0,928,1000,0,280,500,500,0,500,500,500,424,1000,1000,596,1000
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=Bloom

[AppSet7]
App=Blend\Youlean From Buffer
FParamValues=0,1,0
ParamValues=0,1000,0
Enabled=1
Collapsed=1
Name=The Out

[AppSet20]
App=Background\FourCornerGradient
FParamValues=6,1,0.56,1,1,0.568,1,1,0.072,1,1,0.862,1,1
ParamValues=6,1000,560,1000,1000,568,1000,1000,72,1000,1000,862,1000,1000
ParamValuesBackground\Youlean Background MDL=1000,892,0,0,500,500,230,320,0,242,300,120,1000,1000,0,300,500,0,0,500
ParamValuesBlend\VideoAlphaKey=0,888,1000,916,772,300,300,0,0,0,250,500,500
Enabled=1
Collapsed=1
Name=Filter-Color

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="You can","change this text","in settings."
Html="<position y=""37.8""><p align=""center""><font face=""American-Captain"" size=""9"" color=""#fff"">[author]</font></p></position>","<position y=""45.1""><p align=""center""><font face=""Chosence-Bold"" size=""4"" color=""#f4f4f4"">[title]</font></p></position>","<position y=""52.2""><p align=""center""><font face=""Chosence-Bold"" size=""2.5"" color=""#fff"">version</font></p></position>","<position y=""55""><p align=""center""><font face=""Chosence-Bold"" size=""3.5"" color=""#fff"">[comment]</font></p></position>",
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

[Wizard]
Section0Cb=Colove - Drone 01
Section1Cb=None
Section2Cb=HUD Meter 02
Section3Cb=Arcade cabinet
Macro0=Song Title
Macro1=Song Author
Macro2=Song made with love and time

