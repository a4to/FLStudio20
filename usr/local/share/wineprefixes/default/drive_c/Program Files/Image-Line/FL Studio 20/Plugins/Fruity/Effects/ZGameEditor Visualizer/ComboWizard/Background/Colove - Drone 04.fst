FLhd   0 * ` FLdt&  �20.6.9.1657 �y  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��Jv%  ﻿[General]
GlWindowMode=1
LayerCount=22
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,5,3,1,2,4,9,6,13,14,30,31,33,24,22,28,11,23,27,17,7,20

[AppSet0]
App=Background\SolidColor
FParamValues=0.356,0.648,0.636,0.728
ParamValues=356,648,636,728
ParamValuesBackground\FourCornerGradient=0,1000,664,732,1000,898,736,1000,504,620,1000,150,812,1000
Enabled=1
Name=Background XSTry-Ray

[AppSet5]
App=Canvas effects\Flaring
FParamValues=0.524,1,1,0.092,0.952,0.5,0.5,0.653,0,2,0.112
ParamValues=524,1000,1000,92,952,500,500,653,0,2,112
Enabled=1
Name=Fatele Mod

[AppSet3]
App=Canvas effects\SkyOcean
FParamValues=0,0.96,1,0,0,0.5,0.5,0.708,0.34,0.736,1,1,0.576,0.68
ParamValues=0,960,1000,0,0,500,500,708,340,736,1000,1000,576,680
Enabled=1
ImageIndex=1
Name=React 1

[AppSet1]
App=Background\FogMachine
FParamValues=0.672,0.588,1,0.064,0.456,0.5,0.5,0.5,0.5,0.636,0,0.516
ParamValues=672,588,1000,64,456,500,500,500,500,636,0,516
ParamValuesBackground\Youlean Background MDL=1000,1000,1000,420,884,424,198,320,648,558,376,696,800,664,1000,0,500,4,0,500
Enabled=1
ImageIndex=1
Name=Liner Color

[AppSet2]
App=Canvas effects\SkyOcean
FParamValues=0,0.856,1,1,0,0.5,0.5,0,0.404,0.656,1,0.568,0.048,0.488
ParamValues=0,856,1000,1000,0,500,500,0,404,656,1000,568,48,488
ParamValuesBlend\VideoAlphaKey=0,636,1000,0,1000,656,344,0,0,0,474,92,340
Enabled=1
ImageIndex=1
Name=React 2

[AppSet4]
App=Canvas effects\SkyOcean
FParamValues=0,0.592,1,1,0.232,0.5,0.5,0.708,0.34,0.568,0.864,0.924,0,0
ParamValues=0,592,1000,1000,232,500,500,708,340,568,864,924,0,0
Enabled=1
ImageIndex=1
Name=React 3

[AppSet9]
App=Canvas effects\Rain
FParamValues=0.896,1,1,0.648,0.216,0.5,0.5,0.5,0.5,0,0.736,1
ParamValues=896,1000,1000,648,216,500,500,500,500,0,736,1000
ParamValuesCanvas effects\OverlySatisfying=0,0,0,0,600,500,500,100,100,1000,500
Enabled=1
ImageIndex=1
Name=Modliner X1

[AppSet6]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.932,0.068,0.696,0.496,0.472,0,0
ParamValues=0,0,0,0,932,68,696,496,472,0,0
ParamValuesImage effects\Image=0,0,0,1000,884,500,500,0,0,0,0,0,1000,1000
Enabled=1
ImageIndex=1
Name=Start Riser 1

[AppSet13]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.932,0.532,0.288,0.496,0.472,0,0.72
ParamValues=0,0,0,0,932,532,288,496,472,0,720
Enabled=1
ImageIndex=1
Name=Start Riser 3

[AppSet14]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.932,0.304,0.704,0.5,0.472,0,0.72
ParamValues=0,0,0,0,932,304,704,500,472,0,720
Enabled=1
ImageIndex=1
Name=Start Riser 5

[AppSet30]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.932,0.732,0.704,0.5,0.472,0,0.704
ParamValues=0,0,0,0,932,732,704,500,472,0,704
Enabled=1
ImageIndex=1
Name=Start Riser 7

[AppSet31]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.92,0.928,0.72,0.496,0.472,0,0.672
ParamValues=0,0,0,0,920,928,720,496,472,0,672
Enabled=1
ImageIndex=1
Name=Start Riser 9

[AppSet33]
App=Background\ItsFullOfStars
FParamValues=0.048,0,0,0,0.876,0.052,0.504,0.496,0.472,0,0.672
ParamValues=48,0,0,0,876,52,504,496,472,0,672
Enabled=1
ImageIndex=1
Name=Start Riser 11

[AppSet24]
App=Tunnel\Youlean Tunnel
FParamValues=0,0,0,1,1,0.5,0.5,0,0,0,0,0.102,0.5,0.25,0.5,0.5,0.5,0.5,0.5,0,0,0.25,0,0.5,0.5
ParamValues=0,0,0,1000,1000,500,500,0,0,0,0,102,500,250,500,500,500,500,500,0,0,250,0,500,500
ParamValuesHUD\HUD Prefab=27,0,500,0,0,500,500,158,1000,1000,444,0,500,1000,368,0,1000,1000
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPostprocess\Youlean Audio Shake=0,250,0,0,200,0,100,200,500,500,500,500,0,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesImage effects\ImageTileSprite=4,4,0,0,0,0,4,250,250,500,500,500,500,0,0,0,0
ParamValuesPostprocess\ColorCyclePalette=0,0,0,0,0,1000,0,500,0,0,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesHUD\HUD Image=0,0,500,500,1000,1000,500,444,500,0,0,1000,1000,0,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesBackground\Youlean Background MDL=1000,892,0,0,500,500,50,320,0,2,300,120,1000,1000,0,300,500,0,0,500
ParamValuesPostprocess\Youlean Pixelate=628,0,0,0
ParamValuesImage effects\Image=0,0,0,1000,672,500,500,0,0,0,0,0,1000,1000
ParamValuesObject Arrays\BallZ=0,0,1000,1000,392,500,500,818,500,500,500,500,500
ParamValuesPostprocess\Blur=1000
Enabled=1
ImageIndex=1
Name=Core Effect 1

[AppSet22]
App=Postprocess\AudioShake
FParamValues=0.016,0,0,0.328,0.244,0.9
ParamValues=16,0,0,328,244,900
ParamValuesFeedback\WarpBack=0,0,0,0,500,500,20
ParamValuesImage effects\ImageMasked=1000,0,412,20,820,500,500,200,200,1000,0
ParamValuesImage effects\ImageSphereWarp=0,500,0,1000,250,400,1000,204,260,500
ParamValuesPeak Effects\StereoWaveForm=0,0,0,1000,0,0,1000,1000,1000,1000,1000,500,500,500,500,500,500,500,180,500,500,1000,12,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesFeedback\WormHoleEclipse=0,0,0,0,0,0,500,400,436,500,500
ParamValuesImage effects\ImageTileSprite=4,4,0,0,0,0,4,250,250,500,500,500,500,0,0,0,0
ParamValuesHUD\HUD Grid=0,500,0,1000,500,500,1000,1000,500,444,500,1000,500,100,0,500,1000,100,500,300,0,1000
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesPostprocess\ColorCyclePalette=0,0,0,0,0,1000,0,500,0,0,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesHUD\HUD Image=0,0,500,500,1000,1000,500,444,500,0,0,1000,1000,0,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesImage effects\ImageSphinkter=0,0,0,1000,1000,500,500,500,500,500,500,500,500,0,0
ParamValuesImage effects\Image=580,0,0,1000,1000,480,500,112,0,0,0,0,1000,1000
ParamValuesHUD\HUD 3D=0,0,500,500,500,500,500,500,500,500,500,500,500,500,0,500,500,500,500,500,500,500,500,1000,1000,0,0,1000,1000,0
Enabled=1
Name=Shake

[AppSet28]
App=Feedback\BoxedIn
FParamValues=0,0,0,1,0.5,0.236,0.192,0.364,0,0.5
ParamValues=0,0,0,1000,500,236,192,364,0,500
ParamValuesFeedback\WarpBack=0,0,0,0,500,504,52
ParamValuesFeedback\WormHoleDarkn=1000,0,0,0,0,500,500,1000,0,500,500,500
ParamValuesFeedback\FeedMeFract=0,0,0,220,0,0
ParamValuesFeedback\WormHoleEclipse=0,0,0,652,760,1000,364,0,780,500,500
Enabled=1
Name=Mirror

[AppSet11]
App=Postprocess\Youlean Kaleidoscope
FParamValues=0.5,0.5,0,0.5,1,0,0.5,0.5,0.5,0,0
ParamValues=500,500,0,500,1000,0,500,500,500,0,0
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,532,1000,364,0,780,500,500
Enabled=1
Name=Magical

[AppSet23]
App=Peak Effects\Stripe Peeks
FParamValues=0,0,0,0,1,0,0.15,0.5,0.5,0,0.526,0.548,0.54,1,0.116,0.642,0.148,0.048,0.152,1,1,0.184,0
ParamValues=0,0,0,0,1000,0,150,500,500,0,526,548,540,1,116,642,148,48,152,1,1,184,0
ParamValuesFeedback\WarpBack=500,0,0,0,500,500,24
ParamValuesPeak Effects\Reactive Sphere=0,1000,973,0,773,500,500,252,108,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesPeak Effects\ReflectedPeeks=0,60,712,104,336,500,500,744,24
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,1000,500,500,500,500,500,500,500
ParamValuesHUD\HUD 3D=0,0,500,500,500,500,500,500,500,500,500,500,500,500,0,500,500,500,500,500,500,500,500,1000,1000,0,0,1000,1000,0
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,0,204,532,408,668,500,500
Enabled=1
Name=EQ
LayerPrivateData=780163608081067B080B44C3D80C0CB3664ADA21F30F7F5D81C2EF3F640A563F6BE64CA038034340AFB8DDB2D92EB68F974ADB9D3DE3031603998160CF84B3D3D29EC1D94035F6B2512A0E10BB606E68B00700BDDB24B9

[AppSet27]
App=Feedback\BoxedIn
FParamValues=0,0,0,1,1,0,0.252,0.816,0,1
ParamValues=0,0,0,1000,1000,0,252,816,0,1000
Enabled=1

[AppSet17]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.728,0,0.238,0.483,0.844,0,0,1
ParamValues=728,0,238,483,844,0,0,1
ParamValuesScenes\Alien Thorns=0,500,250,500,2,500,500,0
ParamValuesTerrain\CubesAndSpheres=0,0,0,0,236,500,500,650,380,400,552,568,684,1000,1000,1000,0,1000
ParamValuesTerrain\GoopFlow=0,928,1000,0,280,500,500,0,500,500,500,424,1000,1000,596,1000
ParamValuesPostprocess\Blooming=0,0,0,1000,500,900,408,852,0
ParamValuesPostprocess\Point Cloud High=0,434,346,520,492,472,512,491,50,265,0,0,224,0,333
ParamValuesTunnel\Youlean Tunnel=0,0,0,1000,912,500,500,0,0,0,0,250,500,674,676,500,500,500,500,0,4,1000,1000,500,500
Enabled=1
UseBufferOutput=1
Name=Bloom

[AppSet7]
App=Blend\Youlean From Buffer
FParamValues=0,1,0
ParamValues=0,1000,0
Enabled=1
Name=The Out

[AppSet20]
App=Background\FourCornerGradient
FParamValues=6,1,0.56,1,1,0.568,1,1,0.072,1,1,0.862,1,1
ParamValues=6,1000,560,1000,1000,568,1000,1000,72,1000,1000,862,1000,1000
ParamValuesBackground\Youlean Background MDL=1000,892,0,0,500,500,230,320,0,242,300,120,1000,1000,0,300,500,0,0,500
ParamValuesBlend\VideoAlphaKey=0,888,1000,916,772,300,300,0,0,0,250,500,500
Enabled=1
Name=Filter-Color

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="You can","change this text","in settings."
Html="This is the default text."
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

[Wizard]
Section0Cb=Colove - Drone 01
Section1Cb=None
Section2Cb=HUD Meter 02
Section3Cb=Arcade cabinet

