FLhd   0 * ` FLdtJ?  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��}�>  ﻿[General]
GlWindowMode=1
LayerCount=7
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=1,0,3,2,10,9,4
WizardParams=227

[AppSet1]
App=Background\SolidColor
FParamValues=0,0,0,0.916
ParamValues=0,0,0,916
ParamValuesFeedback\WarpBack=500,0,1000,0,500,500,200
ParamValuesParticles\ColorBlobs=834,0,460,548,700,500,500,310,0,0
ParamValuesPostprocess\Blooming=0,0,0,1000,500,800,500,500,532
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesObject Arrays\8x8x8_Eggs=0,48,1000,0,0,500,500,480,500,500,0,0,732
ParamValuesParticles\BugTails=0,646,758,982,1000,728,284,556,1000,904,384,632
ParamValuesParticles\PlasmaFlys=0,452,0,40,1000,644,584,1000,0,580,444,68,888,652
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPostprocess\Point Cloud Default=0,330,330,500,500,448,444,503,174,0,156,0,484,0,0
ParamValuesPostprocess\AudioShake=32,0,0,600,700,200
ParamValuesBackground\ItsFullOfStars=0,0,0,0,500,464,0,104,72,256,232
ParamValuesTerrain\CubesAndSpheres=0,0,0,0,0,500,500,650,0,400,1000,1000,1000,1000,1000,1000,952,1000
ParamValuesPeak Effects\JoyDividers=0,0,0,0,520,508,1000,136,500,100,600,650,510
ParamValuesFeedback\SphericalProjection=448,0,0,0,250,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesImage effects\Image=0,0,0,972,1000,500,500,0,0,0,0,0,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,306,500,500,500,500,500
ParamValuesCanvas effects\Electric=0,428,728,680,548,500,500,68,100,1000,500
ParamValuesBackground\FourCornerGradient=0,1000,0,0,20,0,0,20,0,0,20,0,0,20
ParamValuesScenes\Musicball=872,0,0,0,500,1000,228,0,0,500,0,500,500
ParamValuesParticles\ReactiveMob=0,80,0,0,712,376,772,1000,136,500,272,612,440,732,500,236,200,198,1000,380,500,500,52,724,156,84,500,56,1000,0
ParamValuesParticles\ReactiveFlow=0,528,536,700,299,136,479,320,305,516,489,176,800,1000,127,0,744,429,604,0,53,765,1000,1000,947
ParamValuesCanvas effects\Digital Brain=0,712,1000,0,1000,500,500,600,100,300,100,250,1000,500,1000
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesScenes\RhodiumLiquidCarbon=392,41,1000,0,500,500,500,500,700,1000
ParamValuesFeedback\WormHoleEclipse=0,616,1000,0,0,0,0,1000,1000,0,1000
ParamValuesPeak Effects\Polar=0,108,1000,892,325,500,716,632,1000,1000,412,792,32,500,0,500,0,1000,1000,0,0,0,0,1000
ParamValuesPostprocess\ScanLines=200,800,0,0,0,0
ParamValuesParticles\fLuids=88,35,1000,0,544,500,500,211,0,0,0,1000,1000,0,1000,456,404,1000,1000,500,0,1000,211,0,500,1000,1000,298,1000,0,895,0
ParamValuesFeedback\BoxedIn=0,0,0,1000,500,0,0,0,720,108
ParamValuesScenes\Postcard=692,740,388,1000,524,436,0,0,0,0,0
ParamValuesCanvas effects\N-gonFigure=896,670,0,200,500,500,500,500,500,400,0
ParamValuesObject Arrays\Rings=768,0,0,1000,552,500,500,560,500,0,0,480,0,0,0
ParamValuesFeedback\FeedMe=0,864,1000,0,712,1000,396
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPostprocess\Dot Matrix=1000,456,496,0,0,0,1000,1000
ParamValuesTunnel\TorusJourney=972,0,0,0,0,500,500,600,800,1000,500,40,390,1000,500,60,800,1000,500,1000
ParamValuesFeedback\WormHoleDarkn=1000,0,0,821,336,228,500,500,500,316,396,396
ParamValuesPeak Effects\Linear=0,0,0,0,812,468,504,348,0,500,0,1000,496,0,76,800,0,500,500,0,0,0,1000,1000,1000,0
ParamValuesScenes\Cloud Ten=932,484,392,624,732,660,300,100,100,1000,500
ParamValuesObject Arrays\CubesGrasping=676,612,960,0,416,548,240,0,944,476,960
ParamValuesObject Arrays\CubicMatrix=0,0,0,0,356,40,1000,500,500,368,264,304,104
ParamValuesBackground\FogMachine=500,0,0,500,216,500,500,500,500,384,0,0
ParamValuesBackground\Grid=874,0,0,1000,836,1000,1000,0,0,0
ParamValuesCanvas effects\Flaring=0,1000,1000,1000,1000,500,348,181,0,400,500
ParamValuesMisc\FruityIndustry=0,844,0,176,768,152,380,500,676,428,160,500,500
ParamValuesPeak Effects\ReflectedPeeks=500,500,500,0,500,504,712,500,0
ParamValuesPostprocess\Blur=1000
ParamValuesTerrain\GoopFlow=0,728,1000,304,320,532,500,0,424,500,264,472,680,260,500,80
Enabled=1
Collapsed=1

[AppSet0]
App=HUD\HUD Grid
AppVersion=1
FParamValues=0.724,0.5,0,0.82,0.5,0.5,1,1,1,4,0.5,1,0.5,0,0,0.5,1,0.104,1,0,0,1
ParamValues=724,500,0,820,500,500,1000,1000,1000,4,500,1000,500,0,0,500,1000,104,1000,0,0,1
ParamValuesCanvas effects\DarkSpark=0,408,892,480,1000,500,500,500,500,0,0,0
ParamValuesPostprocess\RGB Shift=1000,0,0,600,700,200
ParamValuesPostprocess\Blooming=0,0,0,912,500,600,296,317,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesBackground\SolidColor=0,0,0,900
ParamValuesParticles\BugTails=0,636,1000,0,500,500,500,0,0,0,0,0
ParamValuesCanvas effects\SkyOcean=0,659,708,0,0,442,916,1000,650,734,1000,899,1000
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPostprocess\Point Cloud Default=0,678,506,500,500,448,444,519,1000,485,156,0,264,0,333
ParamValuesImage effects\ImageWarp=0,0,241,0,0,473,529,78
ParamValuesBackground\ItsFullOfStars=0,0,0,0,0,1000,500,500,0,0,22
ParamValuesTerrain\CubesAndSpheres=0,0,0,0,0,644,1000,538,540,645,1000,581,558,1000,555,939,520,127
ParamValuesPeak Effects\JoyDividers=0,0,0,0,700,500,500,726,500,1000,600,650,510
ParamValuesObject Arrays\DiamondBit=500,500,500,0,658,500,500,0,500,500,1000,500,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,437,500,500,500,500,500
ParamValuesPostprocess\FrameBlur=868,228,1000,116,666,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesParticles\StrangeAcid=0,0,0,0,508,36,80,86,464,60,804,560,8,1000,92,540,1000,1000,0,0
ParamValuesPostprocess\Ascii=0,428,0,600,700,200
ParamValuesImage effects\ImageSphereWarp=708,500,750,1000,250,400,0,500,500,500
ParamValuesCanvas effects\Digital Brain=0,0,0,0,584,500,500,600,100,300,100,250,1000,500,1000
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesBlend\VideoAlphaKey=0,0,1000,0,1000,320,300,0,0,1000,250,500,500
ParamValuesMisc\FruityDanceLine=0,0,0,0,772,500,500,532,788,588,0,492,60
ParamValuesParticles\fLuids=0,0,0,0,0,500,500,432,380,104,500,500,500,0,604,346,500,0,576,500,500,500,0,0,500,0,0,0,500,0,0,0
ParamValuesPeak Effects\Reactive Sphere=0,0,893,0,381,500,500,252,252,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesCanvas effects\N-gonFigure=0,200,900,0,807,500,494,480,0,400,48
ParamValuesCanvas effects\ShimeringCage=0,0,0,0,0,500,500,0,0,323,0,134,173,0
ParamValuesBlend\BufferBlender=0,0,0,1000,0,0,0,0,0,0,750,0
ParamValuesCanvas effects\TaffyPulls=900,0,0,0,0,500,500,100,500,500,0,0,0,500
ParamValuesObject Arrays\Rings=784,0,0,1000,940,500,500,500,488,56,0,112,0,532,296
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesCanvas effects\FreqRing=490,846,1000,0,885,500,500,1000,1000,856,245,251,554,0
ParamValuesFeedback\70sKaleido=0,0,266,1000,595,743
ParamValuesPostprocess\Point Cloud High=0,1000,330,500,500,448,444,503,1000,625,156,0,516,0,0
ParamValuesObject Arrays\CubicMatrix=0,0,0,0,228,500,500,500,500,0,0,0,0
ParamValuesImage effects\ImageSphinkter=0,0,0,0,853,500,500,0,411,730,843,210,538,0,378
ParamValuesBackground\FogMachine=0,491,0,422,901,529,510,1000,0,1000,137,402
ParamValuesPeak Effects\ReflectedPeeks=500,308,1000,112,478,500,500,0,576
ParamValuesPostprocess\Blur=1000
ParamValuesTerrain\GoopFlow=0,704,1000,92,744,864,504,0,444,1000,152,592,1000,1000,536,592
ParamValuesCanvas effects\Stack Trace=374,80,1000,983,348,714,602
Enabled=1
Collapsed=1
Name=Grid

[AppSet3]
App=Peak Effects\Linear
FParamValues=0,0,1,0,0.684,0.5,0.5,1,0,0.5,0,0.156,0.512,1,0,1,0,0.452,0.5,1,0.35,0,0.156,0.001,0.032,0.22,0.432,0.212,0.33,0.426,0.1
ParamValues=0,0,1000,0,684,500,500,1000,0,500,0,156,512,1,0,1,0,452,500,1000,350,0,156,1,32,220,432,212,330,426,100
ParamValuesCanvas effects\DarkSpark=232,0,1000,388,424,500,500,500,500,460,0,468
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesObject Arrays\CubesGrasping=0,0,1000,0,872,500,500,500,500,500,500
ParamValuesCanvas effects\Rain=928,500,1000,500,1000,500,500,500,500,436,844,500
ParamValuesImage effects\Image=0,0,0,972,1000,500,500,0,0,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,499,500,500,500,500,500
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPhysics\Columns=200,300,0,780,500,600,1000,1000,0,450,500,500,500,500
Enabled=1
Collapsed=1
Name=EQ

[AppSet2]
App=Background\FourCornerGradient
FParamValues=7,1,0.08,1,1,0.926,1,1,0.184,1,1,0.738,1,1
ParamValues=7,1000,80,1000,1000,926,1000,1000,184,1000,1000,738,1000,1000
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesScenes\RhodiumLiquidCarbon=0,41,0,1000,1000,500,500,1000,700,1000
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,0,1000,500,500,0,500,500
ParamValuesPeak Effects\Polar=948,16,936,0,548,500,532,884,652,916,320,868,0,500,0,500,0,1000,1000,0,0,0,0,1000
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesParticles\fLuids=0,0,0,0,772,500,500,0,0,0,0,1000,702,0,0,250,500,0,158,500,860,316,0,0,500,0,0,250,500,0,193,0
ParamValuesPostprocess\ColorCyclePalette=1000,0,0,0,0,696,0,1000,0,250,772
ParamValuesObject Arrays\WaclawGasket=596,0,0,1000,448,500,500,500,500,331,156,1000,212,328,144
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPostprocess\Point Cloud Default=0,610,394,484,500,440,472,503,1000,725,144,0,0,0,0
ParamValuesObject Arrays\Rings=0,0,0,0,916,216,500,500,500,0,0,500,0,0,0
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesBackground\ItsFullOfStars=0,724,1000,1000,504,500,500,500,0,72,644
ParamValuesCanvas effects\Lava=896,0,0,900,1000,1000,500,500,500,500,500
ParamValuesFeedback\70sKaleido=0,708,576,0,296,516
ParamValuesObject Arrays\Filaments=8,1000,940,0,548,500,444,548,620,500,4,0,0,872
ParamValuesTerrain\CubesAndSpheres=0,0,1000,0,0,500,500,0,404,400,540,940,316,556,684,456,0,1000
ParamValuesImage effects\ImageSphinkter=0,684,1000,0,0,500,500,500,1000,500,500,276,0,1000,0
ParamValuesObject Arrays\DiamondBit=500,500,1000,0,750,500,500,0,500,500,0,500,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,109,500,500,500,500,500
ParamValuesPostprocess\Blur=656
Enabled=1
Collapsed=1
Name=FILTER COLOR

[AppSet10]
App=Text\TextTrueType
FParamValues=0.052,0,0,0,0,0.5,0.5,0,0,0,0.5
ParamValues=52,0,0,0,0,500,500,0,0,0,500
ParamValuesParticles\ColorBlobs=944,0,0,0,700,500,500,350,1000,148
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPostprocess\Blooming=0,0,0,412,500,632,500,60,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesPeak Effects\Polar=456,980,1000,0,356,500,500,200,1000,568,352,556,0,500,0,500,0,1000,1000,0,0,0,0,1000
ParamValuesPostprocess\ColorCyclePalette=1000,0,0,0,0,464,0,0,0,0,0
ParamValuesPostprocess\RGB Shift=324,512,0,600,700,200
ParamValuesPostprocess\ScanLines=0,600,0,0,0,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesObject Arrays\Rings=0,0,0,1000,500,500,500,500,500,0,0,1000,0,0,0
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPeak Effects\Linear=0,511,565,1000,390,392,440,1000,0,500,0,500,0,0,0,500,0,500,500,1000,350,0,1000,1000,500,0
ParamValuesPostprocess\Point Cloud High=0,318,286,500,464,448,444,503,330,625,156,0,0,0,0
ParamValuesBackground\FogMachine=484,72,1000,0,500,500,500,500,500,952,368,524
ParamValuesImage effects\Image=0,0,0,0,0,500,500,0,0,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,855,500,500,500,500,500
ParamValuesBackground\FourCornerGradient=0,32,140,812,1000,170,1000,1000,524,1000,1000,232,652,1000
ParamValuesPostprocess\Blur=1000
ParamValuesTerrain\GoopFlow=504,564,1000,0,500,500,500,36,500,500,500,0,500,500,500,500
Enabled=1
Collapsed=1
Name=TEXT MAIN

[AppSet9]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
ParamValuesFeedback\WarpBack=500,0,0,0,0,500,200
ParamValuesParticles\ColorBlobs=0,612,1000,1000,440,712,496,1000,16,140
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesImage effects\ImageSlices=0,0,0,1000,948,500,500,0,0,4,500,1000,1000,500,0,0,0
ParamValuesObject Arrays\8x8x8_Eggs=0,564,1000,0,380,596,444,200,388,500,1000,492,416
ParamValuesCanvas effects\SkyOcean=780,496,1000,0,92,432,272,1000,756,668,244,496,588
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesMisc\PentUp=684,603,0,0,268,508,220,0,656,340,1000,0
ParamValuesPostprocess\Point Cloud Default=576,970,814,496,492,448,444,503,330,625,144,0,0,0,0
ParamValuesImage effects\ImageWarp=0,868,1000,0,640,500,476,20
ParamValuesPeak Effects\JoyDividers=0,0,0,0,700,500,500,0,500,100,0,594,1000
ParamValuesImage effects\Image=1000,0,0,0,1000,500,500,0,0,0,0,0,0,0
ParamValuesObject Arrays\BallZ=240,0,0,772,722,412,504,818,292,568,560,940,148
ParamValuesPostprocess\FrameBlur=736,544,616,480,944,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesBackground\FourCornerGradient=0,64,740,1000,1000,698,1000,1000,568,1000,1000,750,1000,1000
ParamValuesImage effects\ImageSphereWarp=616,500,750,1000,250,400,0,500,500,500
ParamValuesParticles\ReactiveFlow=0,125,500,0,680,684,592,984,76,772,220,404,1000,0,712,500,200,500,573,500,688,492,1000,1000,144
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,0,688,188,500,440,1000,464
ParamValuesPeak Effects\Polar=624,0,992,708,368,500,500,200,1000,752,332,732,1000,516,0,500,0,1000,1000,0,0,0,0,1000
ParamValuesPostprocess\ColorCyclePalette=1000,0,0,0,0,1000,0,432,0,0,140
ParamValuesCanvas effects\Rain=0,500,500,1000,1000,792,844,776,628,496,500,500
ParamValuesObject Arrays\Rings=0,0,0,0,456,500,500,500,500,0,0,500,0,0,0
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesInternal controllers\Peak Band Controller=212,1000,420,1000,0,676,448,572,0,624,0,652,1000,628,0,664,1000,0,0,0,0,0,0,0,0,0
ParamValuesPeak Effects\Linear=0,207,1000,0,90,502,344,896,0,500,0,724,496,0,0,456,0,500,500,1000,350,0,1000,1000,424,0
ParamValuesPostprocess\Point Cloud High=0,662,462,484,488,224,928,535,584,601,0,0,568,0,0
ParamValuesObject Arrays\CubicMatrix=0,0,0,0,560,556,500,500,1000,352,496,620,836
ParamValuesBackground\Grid=434,0,0,1000,596,596,1000,0,0,0
ParamValuesPostprocess\Blur=1000
ParamValuesTerrain\GoopFlow=516,616,1000,608,252,452,440,0,500,500,500,364,500,500,500,500
Enabled=1
Collapsed=1
Name=Fade in-out

[AppSet4]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="<position y=""86""><p align=""center""><font face=""Chosence-Bold"" size=""2"" color=""#fff"">Image-Line Software</font></p></position>","<position y=""88  ""><p align=""center""><font face=""Chosence-regular"" size=""2"" color=""#CCCCC1"">Exclusive Sounds</font></p></position>"
Html="<position x=""4"" y=""5""><p><font face=""American-Captain"" size=""4"" color=""#000"">[author]</font></p></position>","<position x=""4"" y=""8""><p><font face=""Chosence-Bold"" size=""3"" color=""#000"">[title]</font></p></position>","<position x=""4"" y=""14""><p> <font face=""Chosence-Bold"" size=""3"" color=""#000"">[comment]</font></p></position>"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

