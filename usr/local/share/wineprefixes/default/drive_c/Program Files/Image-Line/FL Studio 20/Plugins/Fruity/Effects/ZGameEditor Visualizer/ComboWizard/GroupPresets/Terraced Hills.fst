FLhd   0  0 FLdtD  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   ��/�  ﻿[General]
GlWindowMode=1
LayerCount=8
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=2,6,5,4,3,0,1,7

[AppSet2]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
ParamValuesPeak Effects\Youlean Peak Shapes=0,0,0,0,74,500,368,500,490,104,564,500,120,144,500,0,0,200,200,360,358,0,520,5,0,1000,0,500,500,0,0,0
ParamValuesPeak Effects\Linear=0,964,0,0,178,752,508,112,250,500,196,350,1000,1000,500,0,0,500,500,500,468,0,500,152,200,0,32,212,330,250,100
ParamValuesPeak Effects\Youlean Waveform=0,0,0,0,186,859,500,250,372,250,500,210,500,0,100,0,750,1000,16,88,0,500,100,100,0,0,282,500,500,1000,0,1000
ParamValuesPeak Effects\Polar=0,0,0,0,172,500,158,300,556,500,128,420,0,631,1000,500,1000,1000,968,1000,1000,0,0,1000
ParamValuesPeak Effects\SplinePeaks=940,0,0,0,1000,500,1000,0,0,0
ParamValuesMisc\FruityIndustry=0,0,0,0,500,500,0,500,500,0,500,500,500
ParamValuesHUD\HUD Graph Polar=0,500,0,0,500,856,106,444,784,0,1000,0,1000,0,0,500,0,348,0,0,500,0
Enabled=1
ImageIndex=1
Name=Section2

[AppSet6]
App=HUD\HUD 3D
FParamValues=0,1,0.1788,0.5788,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.2656,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,0
ParamValues=0,1,178,578,500,500,500,500,500,500,500,500,500,265,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,0
Enabled=1
ImageIndex=3
Name=ForegroundMod

[AppSet5]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
Name=TextFst

[AppSet4]
App=Text\TextTrueType
FParamValues=0,0.8333,0,1,0,0.5,0.5,0,0,0,0.5
ParamValues=0,833,0,1000,0,500,500,0,0,0,500
Enabled=0
Name=Text

[AppSet3]
App=Postprocess\Youlean Drop Shadow
FParamValues=0.5,0.2,0,1,0.02,0.875,0
ParamValues=500,200,0,1000,20,875,0
Enabled=1
UseBufferOutput=1

[AppSet0]
App=Youlean new shaders\Terrain\Terraced Hills
FParamValues=0,0.6
ParamValues=0,600
ParamValuesImage effects\Image=0,0,0,0,1000,500,500,0,0,0,1000,0,0,0
Enabled=1
Name=Section0

[AppSet1]
App=Postprocess\Vignette
FParamValues=0,0,0,0.6,0.7738,0
ParamValues=0,0,0,600,773,0
ParamValuesPostprocess\Youlean Color Correction=226,500,539,500,500,500
ParamValuesPostprocess\Raindrops=404,0
ParamValuesPostprocess\RGB Shift=128,0,0,600,700,200
Enabled=1
Name=Section1

[AppSet7]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
ImageIndex=2

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="This is the default text."
Images="@EmbeddedFst:""Name=Elegant order.fst"",Data=7801C554CD6EDB300CBE07C83B14397B9D24CB7F435520B1E30658BB75CDDA1EB21EB4986B0438B6E1286DF36C3BEC91F60AFBE4FCAC3DECD0ED301889488AFC487D20F9F3FB8FD91955D4EAF2AEDF3B2B6F4D55D48F1775418AF77BE77A436D5AAF2BABFC7E2FBF9C3AE38529CC65DD5AF506CA70D5D0DC5E696B6AC5C377C92EE6635B50AB98C73DD1EFF57BB361D34CC932A480A426D7D917FC8E3ED39375B0BAD5CB1B5DAE698510761C78F8C79988D0C711074CFEB6E22676563F4C62C8C2F391A48BE9E220771F4A7B091BB0ED05403D207A7BDD590108B83D1A83A7BBDD7A70A71E30C795FE5A5271E0E6B2350FDA52A6AD5651CC381BA5593A92A91CE732CDE5280B5826873213799AC944A499188EE4301D85118FA58366324C862C8AF36734F1D7D114F12802030967E18E089F8B5DC9D299A0E2783D5500F680DA11F10272AB74D4FF0B59B99F8E659CA6A089FF99A620713489713A6461882E3C7493781D4D3117AE511219B8F6418F1DC7D27590FB3A9A04C77BFF8226007B40ED687A0109C03DDE7FA1E9C614541FD153836105579D3A519CC56CA7DC2A9E88BD72451566366F56CA8769AA974D49986B527B87144B61FE412F49618AD785A99F1B3AF04F6B5D1ABB711117FAE93D6D720C364D1B3D37D53D60192E725352B5051919DBBA043C923C1612971DECC12CE22EE2BA9AD7CBA6A5D50A8307A7E9BAA176D5D5E754F4C3F5CA6DA9CA52E5DEE9B68A9A59634BBAF3667A6D17750B0120CBADC3C42E4B3538698E50ED7DA5068339ECD40E06A7338BD8D2547477F2B6391DEC6802FC7453CD5D32540F4FF79A6DE68CAC9E2FA87069EB46F989C402A46F56711FABF0D61476A1C218E284CCFDC2AA208ABA8A5DB56D5D96D422F28A8AF387528920C4066E89AA8336C24E3C28E7EBE541EEF77E01167F825F","@EmbeddedFst:""Name=Song Position circle.fst"",Data=78018D544B6EDB3010DD0BD01D04AF5D97D4DF4518C0DF0468D2BA713E28922C58691C13952581A292F86C5DF448BD426728C7B0912E0A1B1467867C33F3F8C83FBF7EDF9F41095A168FAE7356DCA932AF5E2EAB1C04779D0BB9053DA9DAD288C075E68B25392F55AE169536E2031AA3A686CC5C49A32AC1E34FC3DD9EAF3A072D589FF77DD7719DFB515D2FC1304C813371AD36E06122954953E9E6E17BD516204BCFFAC752532AA9E5E656162D3408C306511CF6E9CBFC94AC616C2D3F883105F9E3043D031EE2AA4144B308FD38E2C070B03F4E6B0758F43178078DB81DAAC5648CB6102841466811208F18CE0FD0386347680B903FBDD96A8594340F8BAA90C4C12E79E2EF70D27E407894C20E9824F46DBA0053909BF21DAED86360D07566A5FC51404E47B16796FF0FB34B2337F5BFB8ED4A1C8411719B86098EC4197D896EAAC552476C52C052DE3F6E1D1BEDCA667DC22194CE4EFA7EDA3570D0DD41873676D49555DD42AB6769602A8D1449CA78124C6693D49F87B3C9341C87B369944493D1641E0E63CED36488D4B0F1281EB1709A1C10E3EF88797F32EF89B09D62CB81DFF5B91351CCACA822121831C06D9C9486464C676603DD7718A71D5944D39136F64A207CEA9F1445D811225B1BFD61487474A0D64772B09AD801934D3FC43ED6C1ADCAA1F2E0B5C69B893D5BF35C7096E23159E34EF0A1FF665C418917745E372240D7127551005E62106F0B26F802645FE406045ED93657D5A1C3E27D6B65A1CC96765CCAD7CFB09DE38D85652D33553E212C4975AE0A283B90B1329A12F024E4A91F62D0C2EEDD2812DA715366D5A6D6D034A870B4976D0DBAB1F5918982BF69E8492A0D94D4E735BC1AD1BB5EABC6C3BF598397C34AB685F10C46063DD739379B42F44E6A0FCB7D2A45AF97E14ED0BDDEE93D2D2954098F271FEB535C6AFB42FCE5B6CC281B968F2BA99D2EF5148CCCD69053DEAA16C130C4E70E5646F0001FBE3B959BB588539C9E837A5A1B11259D14A95C5D150568DC7905F9C57321FC28C6F75603947B6B8CAFDDDEB86837FBB9EBFC0570968376"
VideoUseSync=0
Filtering=0

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

[Wizard]
Section0Cb=Terraced Hills
Section1Cb=Vignette
Section2Cb=Song Position circle
Section3Cb=Elegant order
Macro0=Song Title
Macro1=Song Author
Macro2=Song made with love and time

