FLhd   0  0 FLdtJ$  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   ��G�#  ﻿[General]
GlWindowMode=1
LayerCount=48
FPS=2
MidiPort=-1
Aspect=1
LayerOrder=3,32,33,11,10,9,1,4,12,2,6,25,24,7,23,16,13,0,18,19,5,28,36,39,43,37,40,42,38,41,44,35,17,34,15,14,20,21,8,26,27,22,47,31,45,46,29,30
WizardParams=1525,1526,1527,1528,1529,466,514,562,82

[AppSet3]
App=Background\SolidColor
ParamValues=0,0,0,1000
Enabled=1
Collapsed=1

[AppSet32]
App=HUD\HUD Prefab
ParamValues=15,0,112,704,0,899,12,173,1000,1000,4,0,901,1,240,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C4CF53273CA18863D00000C6D0AA0

[AppSet33]
App=HUD\HUD Prefab
ParamValues=15,0,112,716,0,95,12,173,1000,1000,4,0,901,1,240,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C4CF53273CA18863D00000C6D0AA0

[AppSet11]
App=HUD\HUD Prefab
ParamValues=53,0,524,776,0,692,635,58,1000,1000,4,0,740,1,476,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C2DF43273CA18460000002E3F0AB5

[AppSet10]
App=HUD\HUD Prefab
ParamValues=53,0,524,776,0,340,635,58,1000,1000,4,0,740,1,476,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C2DF43273CA18460000002E3F0AB5

[AppSet9]
App=HUD\HUD Prefab
ParamValues=53,0,112,820,0,500,921,58,1000,1000,4,0,740,1,476,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C2DF43273CA18460000002E3F0AB5

[AppSet1]
App=HUD\HUD Prefab
ParamValues=7,772,500,552,0,415,130,1000,1000,1000,4,0,500,1,204,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4B4A4CCE4E2FCA2FCD4B89490232750D0CCCF53273CA18460A0000F5E0084B

[AppSet4]
App=Misc\Automator
ParamValues=1,2,6,2,500,2,250,1,2,7,2,500,11,250,1,10,7,2,500,56,250,1,11,7,2,320,108,338
Enabled=1
Collapsed=1

[AppSet12]
App=Misc\Automator
ParamValues=1,33,13,3,1000,11,250,1,34,13,3,1000,11,250,0,10,7,2,500,56,250,1,12,7,2,320,108,338
Enabled=1
Collapsed=1

[AppSet2]
App=Postprocess\Youlean Bloom
ParamValues=500,0,150,455,1000,0,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet6]
App=Canvas effects\TaffyPulls
ParamValues=0,404,1000,0,0,0,0,0,500,500,0,0,0,500
ParamValuesParticles\BugTails=228,524,748,0,676,500,500,1000,0,1000,0,0
Enabled=1
Collapsed=1
MeshIndex=1

[AppSet25]
App=Canvas effects\TaffyPulls
ParamValues=392,404,732,0,0,452,0,0,500,500,0,0,0,500
Enabled=1
Collapsed=1
MeshIndex=1

[AppSet24]
App=Canvas effects\TaffyPulls
ParamValues=0,404,944,0,0,1000,156,0,500,500,0,0,0,500
Enabled=1
Collapsed=1
MeshIndex=1

[AppSet7]
App=Postprocess\Youlean Bloom
ParamValues=500,0,150,455,1000,0,0,0
Enabled=1
Collapsed=1

[AppSet23]
App=Postprocess\Vignette
ParamValues=0,0,0,600,480,92
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet16]
App=Background\SolidColor
ParamValues=0,0,0,1000
Enabled=0
Collapsed=1

[AppSet13]
App=HUD\HUD Mesh
ParamValues=0,112,1000,1000,268,500,580,3,0,150,500,1000,500,500,500,0
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet0]
App=Misc\MatCap
ParamValues=0,0,0,212,500,604,1000,500,500,500,500,500,632,116,0,0
ParamValuesImage effects\ImageBox=0,0,0,0,0,500,500,552,588,500,0,0,0,0,0,0,500,500,500
Enabled=1
UseBufferOutput=1
Collapsed=1
ImageIndex=2

[AppSet18]
App=Image effects\Image
ParamValues=0,528,440,584,0,500,212,0,0,0,0,0,1,1000
Enabled=1
UseBufferOutput=1
Collapsed=1
ImageIndex=7

[AppSet19]
App=Image effects\Image
ParamValues=0,0,0,0,0,556,260,324,0,0,0,0,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1
ImageIndex=4

[AppSet5]
App=Background\SolidColor
ParamValues=0,480,264,780
Enabled=1
Collapsed=1

[AppSet28]
App=Background\Grid
ParamValues=250,0,0,496,224,224,1000
Enabled=1
Collapsed=1
MeshIndex=1

[AppSet36]
App=HUD\HUD Prefab
ParamValues=233,860,500,0,0,111,599,901,1000,1000,4,0,500,1,368,0,0,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0686667A9939650C23030000A736090C

[AppSet39]
App=HUD\HUD Prefab
ParamValues=233,768,500,456,0,80,727,581,1000,1000,4,0,500,1,368,0,0,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0686667A9939650C23030000A736090C

[AppSet43]
App=HUD\HUD Prefab
ParamValues=233,768,104,456,0,141,727,581,1000,1000,4,0,500,1,368,0,0,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0686667A9939650C23030000A736090C

[AppSet37]
App=HUD\HUD Prefab
ParamValues=233,860,500,0,0,890,599,901,1000,1000,4,0,500,1,368,0,0,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0686667A9939650C23030000A736090C

[AppSet40]
App=HUD\HUD Prefab
ParamValues=233,768,500,456,0,919,727,581,1000,1000,4,0,500,1,368,0,0,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0686667A9939650C23030000A736090C

[AppSet42]
App=HUD\HUD Prefab
ParamValues=233,768,104,456,0,860,727,581,1000,1000,4,0,500,1,368,0,0,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0686667A9939650C23030000A736090C

[AppSet38]
App=Postprocess\ParameterShake
ParamValues=1000,0,36,16,1000,0,37,16,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet41]
App=Postprocess\ParameterShake
ParamValues=1000,0,39,16,1000,0,40,16,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet44]
App=Postprocess\ParameterShake
ParamValues=1000,0,42,16,1000,0,43,16,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet35]
App=Image effects\Image
ParamValues=732,0,0,1000,1000,500,513,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet17]
App=Image effects\Image
ParamValues=732,0,0,1000,1000,500,507,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet34]
App=Image effects\Image
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet15]
App=Image effects\Image
ParamValues=1000,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=3

[AppSet14]
App=Misc\Automator
ParamValues=1,16,1,2,1000,126,454,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1
Collapsed=1

[AppSet20]
App=Image effects\Image
ParamValues=0,0,0,0,930,506,484,0,0,12,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=5

[AppSet21]
App=Image effects\Image
ParamValues=0,0,0,0,930,502,416,0,0,474,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=5

[AppSet8]
App=Image effects\Image
ParamValues=568,0,0,0,1000,362,601,3,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=6

[AppSet26]
App=Image effects\Image
ParamValues=568,0,0,0,1000,635,601,3,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=6

[AppSet27]
App=Misc\Automator
ParamValues=1,9,8,2,92,186,454,1,27,8,2,92,186,454,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1
Collapsed=1

[AppSet22]
App=Postprocess\Youlean Color Correction
ParamValues=500,564,500,500,636,500
Enabled=1
Collapsed=1

[AppSet47]
App=Image effects\Image
ParamValues=464,0,0,1000,339,500,66,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet31]
App=HUD\HUD Prefab
ParamValues=0,0,500,0,0,500,107,182,1000,1000,4,0,500,1,368,122,1000,1
Enabled=1
Name=LOGO
LayerPrivateData=78DA73C8CBCF4BD52B2E4B671899000060F9036F

[AppSet45]
App=HUD\HUD Prefab
ParamValues=12,0,500,0,456,500,114,57,1000,1000,4,0,773,1,368,764,994,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C8CF43273CA18863D000009B80A9D

[AppSet46]
App=Misc\Automator
ParamValues=1,46,13,3,1000,5,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1
Collapsed=1

[AppSet29]
App=Text\TextTrueType
ParamValues=536,0,0,1000,0,493,498,0,0,0,500
Enabled=1

[AppSet30]
App=Text\TextTrueType
ParamValues=0,0,0,0,0,493,500,0,0,0,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0

[UserContent]
Text="This is the default text."
Html="<position x=""4""><position y=""4""><p align=""left""><font face=""American-Captain"" size=""10"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""4""><position y=""13""><p align=""left""><font face=""American-Captain"" size=""8"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""0""><position y=""5""><p align=""right""><font face=""Chosence-Bold"" size=""4"" color=""#FFFFFF"">[extra1]</font></p></position>","<position x=""0""><position y=""12""><p align=""right""><font face=""Chosence-Bold"" size=""4"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""3""><position y=""88""><p align=""center""><font face=""Chosence-Bold"" size=""5"" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""3""><position y=""94""><p align=""center""><font face=""Chosence-Bold"" size=""5"" color=""#FFFFF"">[extra3]</font></p></position>"
Meshes=[plugpath]Content\Meshes\Heroine.zgeMesh
Images=[plugpath]Content\Bitmaps\Particles\earth1.png
VideoUseSync=0
EnableMipmap=1

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

