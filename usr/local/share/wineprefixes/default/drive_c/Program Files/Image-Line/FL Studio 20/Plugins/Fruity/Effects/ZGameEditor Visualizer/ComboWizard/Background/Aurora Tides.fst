FLhd   0 * ` FLdt+,  �20.6.2.1585 �1  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV աW�+  ﻿[General]
GlWindowMode=1
LayerCount=12
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=4,7,5,8,11,0,2,16,17,13,14,12

[AppSet4]
App=Particles\ColorBlobs
FParamValues=0.94,0,0,0,0.764,0.5,0.5,0.342,0,0.02
ParamValues=940,0,0,0,764,500,500,342,0,20
ParamValuesPostprocess\Blooming=407,495,622,13,500,527,212,1000,156
ParamValuesPhysics\Cage=968,200,300,0,800,500,500,500,500,500
ParamValuesObject Arrays\8x8x8_Eggs=0,0,0,929,0,362,442,174,581,500,1000,0,664
ParamValuesParticles\PlasmaFlys=500,688,500,549,604,654,738,500,564,815,500,761,654,734
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPostprocess\BufferBlender=0,0,267,654,750,0,323,0,0,757,758
ParamValuesPostprocess\Luminosity=0
ParamValuesBackground\ItsFullOfStars=990,605,442,52,713,500,500,500,0,0,0
ParamValuesObject Arrays\Pentaskelion=968,964,1000,0,592,500,500,0,500,500,500,0,0,0
ParamValuesObject Arrays\CrystalCube=270,100,500,500,861,500,500,500
ParamValuesImage effects\Image=0,0,0,0,537,502,500,1000,1000,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,773,500,500,500,500,500
ParamValuesCanvas effects\StarTaser2=842,500,500,0,472,500,500,0,0,0,1000,0,200
ParamValuesScenes\Musicball=848,584,1000,0,500,500,500,0,0,0,0,116,500
ParamValuesImage effects\ImageMasked=0,0,0,0,438,500,500,0,0,292,450
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesCanvas effects\ImageTileSprite=4,4,0,0,0,0,4,250,250,500,500,500,500,0,0,0,0
ParamValuesPeak Effects\Polar=672,700,800,824,462,500,500,125,500,0,479,818,797,722,0,500,0,1000,1000,0,0,0,0,1000
ParamValuesScenes\Postcard=728,860,484,780,580,452,0,0,0,0,0
ParamValuesObject Arrays\DeathClock8=0,0,0,0,32,500,500,224,0,0,0,0
ParamValuesCanvas effects\N-gonFigure=944,654,900,200,500,500,500,500,500,400,0
ParamValuesObject Arrays\Rings=464,503,305,0,266,523,565,523,511,20,0,189,0,0,0
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesFeedback\FeedMe=0,695,492,501,665,247,0
ParamValuesPostprocess\Vignette=0,0,0,0,700,361
ParamValuesInternal controllers\Peak Band Controller=0,1000,0,1000,0,0,0,1000,1000,0,0,1000,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesObject Arrays\Filaments=952,528,1000,0,772,500,500,500,500,500,0,0,124,808
Enabled=1
Name=BACKGROUND

[AppSet7]
App=Feedback\FeedMe
FParamValues=0,0.668,0,1,0.5,0.722,0
ParamValues=0,668,0,1000,500,722,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPostprocess\Blooming=0,0,0,0,0,1000,902,0,12
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesParticles\fLuids=372,917,672,695,917,627,500,0,0,0,0,0,500,0,0,250,734,0,0,500,500,500,757,0,500,0,0,0,0,0,0,0
ParamValuesText\TextTrueType=280,0,0,0,0,500,484,0,0,0,500
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPostprocess\AudioShake=27,0,0,600,700,200
ParamValuesBlend\BufferBlender=100,0,67,1000,0,0,0,0,500,500,750,0
ParamValuesImage effects\ImageBox=898,550,530,0,369,500,500,500,500,496,0,0,0,0,0,0,1000,1000,500
ParamValuesBackground\FogMachine=982,0,0,0,273,500,500,500,274,500,1000,1000
ParamValuesImage effects\Image=0,653,634,0,941,500,500,1000,1000,0,0
ParamValuesCanvas effects\Electric=688,523,485,996,0,500,500,0,100,1000,500
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,167,500,500,500,500,500
Enabled=1
Name=STARFIELD

[AppSet5]
App=Postprocess\Point Cloud High
FParamValues=0,0.683,0.418,0.484,0,0.354,0.346,0.499,0.124,0.816,0.074,0,0.78,0,0
ParamValues=0,683,418,484,0,354,346,499,124,816,74,0,780,0,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesPeak Effects\Polar=601,0,0,856,654,500,500,512,0,599,279,485,1000,500,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPostprocess\AudioShake=16,0,0,600,700,200
ParamValuesCanvas effects\Lava=415,550,882,995,1000,500,500,517,517,0,500
ParamValuesImage effects\ImageBox=168,0,0,0,166,494,577,500,500,500,0,0,333,0,0,0,500,281,500
ParamValuesPeak Effects\Linear=0,0,0,0,1000,671,509,1000,250,512,0,342,1000,1000,0,0,1000,508,431,562,0,0,1000,1000,0
ParamValuesScenes\Mandelbulb=0,500,500,500,500,500,500,500,1000,1000,1000,125,0,0,0,0,0,0
ParamValuesBackground\FogMachine=46,568,557,0,288,500,500,500,500,386,0,0
ParamValuesImage effects\Image=0,0,0,0,820,485,474,0,0,0,0
ParamValuesCanvas effects\Electric=965,515,0,992,0,0,1000,32,100,1000,500
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,766,500,500,500,500,500
ParamValuesMisc\Automator=0,0,0,143,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0
ParamValuesCanvas effects\Flow Noise=0,553,564,988,1000,0,1000,0,100,1000,500
ParamValuesMidi\Midi Spiral=276,750,500,500,630,0,0,0,0,125,250,0
Enabled=1
ImageIndex=1
Name=NEBULA

[AppSet8]
App=Postprocess\Blooming
FParamValues=0,0,0,0,0,0.655,0,0.201,0.301
ParamValues=0,0,0,0,0,655,0,201,301
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesImage effects\ImageSlices=457,1000,0,1000,977,477,345,0,0,0,500,333,0,500,0,0,0
ParamValuesParticles\fLuids=0,622,744,0,610,500,500,0,0,0,0,0,316,0,526,1000,366,0,0,500,557,239,0,0,500,0,829,0,500,0,0,0
ParamValuesText\TextTrueType=0,0,0,0,0,469,216,0,0,0,500
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesCanvas effects\ShimeringCage=0,722,742,31,956,500,257,0,0,0,0,0,0,0
ParamValuesFeedback\FeedMe=0,0,0,0,0,1000,0
ParamValuesCanvas effects\FreqRing=500,500,500,500,397,500,500,500,0,312,123,500,500,500
ParamValuesFeedback\70sKaleido=530,550,677,1000,276,1000
ParamValuesPeak Effects\Linear=0,0,0,0,1000,354,500,1000,250,512,0,342,0,1000,0,0,0,508,431,562,0,0,1000,1000,0
ParamValuesText\MeshText=0,539,566,0,239,235,346,500,500,500,500,377,469,500,500,500,0,0,500,0
ParamValuesImage effects\Image=0,0,0,0,0,500,500,0,0,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,871,500,500,500,500,500
ParamValuesCanvas effects\StarTaser2=826,500,500,746,1000,500,500,500,500,500,0,0,0
ParamValuesPostprocess\FrameBlur=60,0,0,0,855,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesTerrain\GoopFlow=0,0,0,0,0,535,396,292,500,500,500,500,500,500,500,500
Enabled=1
UseBufferOutput=1
Name=COLOR BOOST

[AppSet11]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
ParamValuesBlend\BufferBlender=0,111,667,1000,0,0,0,0,500,500,750,0
Enabled=1

[AppSet0]
App=Background\SolidColor
FParamValues=0.711,0.668,0.801,0.4
ParamValues=711,668,801,400
ParamValuesCanvas effects\DarkSpark=938,0,0,500,500,500,500,500,500,500,0,0
ParamValuesImage effects\Image=0,515,601,795,1000,688,469,1000,1000,0,0
ParamValuesFeedback\FeedMeFract=0,1000,0,109,338,0
ParamValuesCanvas effects\Electric=0,559,347,903,1000,500,500,16,100,1000,500
ParamValuesImage effects\ImageMashup=0,769,0,0,0,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,695,500,500,500,500,500
ParamValuesParticles\StrangeAcid=0,603,610,964,204,500,500,96,1000,1000,330,969,1000,1000,1000,426,980,0,374,939
Enabled=1
Name=COLOR BG

[AppSet2]
App=Postprocess\Vignette
FParamValues=0,0,0,0,0.62,0.173
ParamValues=0,0,0,0,620,173
ParamValuesPhysics\Heightfield=580,1000,532,1000,500,78,539,1000,0,404,1000,323,542,0
ParamValuesPeak Effects\PeekMe=0,491,1000,0,0,1000,462,0,1000,0,0,0
ParamValuesPeak Effects\VectorScope=411,0,0,0,615,59,667,0
ParamValuesBackground\SolidColor=572,503,538,574
ParamValuesImage effects\ImageSlices=0,557,0,0,31,15,475,0,0,0,500,333,0,500,0,0,0
ParamValuesPeak Effects\Polar=0,522,411,929,869,524,497,124,1000,0,286,555,0,500,0
ParamValuesPeak Effects\WaveSimple=0,0,0,0,0,842,500,1000,0
ParamValuesPhysics\Cage=600,542,264,833,0,0,523,377,0,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesCanvas effects\ShimeringCage=710,620,737,0,123,500,500,952,1000,246,0,0,0,0
ParamValuesFeedback\FeedMe=1000,683,1000,0,0,1000,0
ParamValuesBlend\BufferBlender=200,0,333,0,750,333,689,0,500,500,520,0
ParamValuesPhysics\Columns=534,1000,0,930,500,772,90,1000,0,408,339,734,514,1000
ParamValuesPostprocess\AudioShake=84,0,0,600,700,200
ParamValuesPeak Effects\Linear=507,534,261,0,764,502,616,745,0,477,388,92,0,0,0,584,0,500,500,340,350,0,1000,1000,1000
ParamValuesPostprocess\Dot Matrix=372,569,392,4,565,539,167,1000
ParamValuesImage effects\Image=0,0,0,0,791,500,500,0,0,0,0
ParamValuesCanvas effects\Stack Trace=868,0,0,500,130,500,500
Enabled=1
Name=VIGNETTE

[AppSet16]
App=Particles\ColorBlobs
FParamValues=0.745,0,1,0,0.06,0.5,0.5,0.31,0,0.097
ParamValues=745,0,1000,0,60,500,500,310,0,97
Enabled=0
Name=FIREFLIES

[AppSet17]
App=Particles\fLuids
FParamValues=0,0.668,0.48,0.372,0.681,0.5,0.5,0.333,0.517,0.526,0.5,0.5,0.5,0,1,0.074,1,0.141,0.051,0.423,0.59,0.578,0,0.484,0,0,0,0,0,0,0,0
ParamValues=0,668,480,372,681,500,500,333,517,526,500,500,500,0,1000,74,1000,141,51,423,590,578,0,484,0,0,0,0,0,0,0,0
ParamValuesPostprocess\RGB Shift=16,507,0,600,700,200
Enabled=1
ImageIndex=1
Name=RGB SHIFT

[AppSet13]
App=Postprocess\Blur
FParamValues=0
ParamValues=0
Enabled=1
Name=BLUR1

[AppSet14]
App=Postprocess\Blur
FParamValues=0
ParamValues=0
Enabled=1
Name=BLUR2

[AppSet12]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Name=FADE I/O

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text=,"Social Link #1","Social Link #2","Social Link #3","Social Link #4","Social Link #5","Social Link #6","Social Link #7","Social Link #8",,"Replace the lines above with your social link url's. ","Do NOT remove (or add) any of the lines (including this one) even if you ony use a few of them."
Html="<!--Do not change this code unless you know what you're doing -->",,"<position x=""27""  y=""91""><b><p align=""Left"" ><font face=""Calibri"" color=""#FFFFFF"" size=""3"">[author]</font></p></position>  ","<position x=""27""  y=""94""><b><p align=""Left"" ><font face=""Calibri"" color=""#FFFFFF"" size=""3"">[title]</font></p>  ","<position x=""27""  y=""97""><hr width=""27"" height=""0.1""/></position>  ","<position x=""27""  y=""98""><b><p align=""Left"" ><font face=""Calibri"" color=""#FFFFFF"" size=""2"">[textline]</font></p>",,"<!--Do not change this code unless you know what you're doing -->"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=0
GreenLvl=0
BlueLvl=0
LumLvl=0

