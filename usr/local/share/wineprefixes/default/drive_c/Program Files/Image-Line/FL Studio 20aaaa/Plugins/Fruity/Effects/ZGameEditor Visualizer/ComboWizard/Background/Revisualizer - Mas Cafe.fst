FLhd   0 * ` FLdt�#  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��FA#  ﻿[General]
GlWindowMode=1
LayerCount=46
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,31,18,30,2,23,17,20,35,32,36,37,34,24,33,21,10,9,3,4,1,7,5,13,14,15,12,6,11,8,16,25,22,44,19,26,28,29,38,46,39,45,42,41,40,43

[AppSet0]
App=HUD\HUD 3D
FParamValues=0,0,0.51,0.496,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.612,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=0,0,510,496,500,500,500,500,500,500,500,500,500,612,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1

[AppSet31]
App=Postprocess\ColorCyclePalette
FParamValues=1,4,0,0,14,0.241,0.226,0.5,0,0,0.216
ParamValues=1,4,0,0,14,241,226,500,0,0,216
Enabled=1
ImageIndex=11

[AppSet18]
App=Postprocess\Vignette
FParamValues=0,0,0,0.6,0.803,0.115
ParamValues=0,0,0,600,803,115
Enabled=1

[AppSet30]
App=Misc\Automator
FParamValues=1,19,5,6,0.862,0.25,0.495,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,19,5,6,862,250,495,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1

[AppSet2]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.525,0.5,0.5,0.323,0.5
ParamValues=500,525,500,500,323,500
Enabled=1
UseBufferOutput=1

[AppSet23]
App=Canvas effects\Flaring
FParamValues=0,1,1,1,0.714,0.5,0.5,0.099,0,4,0
ParamValues=0,1000,1000,1000,714,500,500,99,0,4,0
Enabled=1

[AppSet17]
App=Canvas effects\FreqRing
FParamValues=0.812,0.5,0.5,0.5,0.091,0.5,0.5,0.118,0,0.312,0.123,0.5,0.5,0.5
ParamValues=812,500,500,500,91,500,500,118,0,312,123,500,500,500
ParamValuesCanvas effects\OverlySatisfying=0,740,484,0,600,500,500,100,100,1000,500
ParamValuesPostprocess\ColorCyclePalette=500,0,0,0,0,1000,0,500,0,0,0
Enabled=1
ImageIndex=5

[AppSet20]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1

[AppSet35]
App=Background\SolidColor
FParamValues=0,0,0,0
ParamValues=0,0,0,0
Enabled=1

[AppSet32]
App=Peak Effects\Polar
FParamValues=0,0,0,0.819,0.086,0.249,0.753,0.038,1,0.5,0.175,0.402,1,0.64,0,0.5,0,0.283,1,0,0,0,0,0
ParamValues=0,0,0,819,86,249,753,38,1000,500,175,402,1000,640,0,500,0,283,1000,0,0,0,0,0
Enabled=1
ImageIndex=11

[AppSet36]
App=Background\FourCornerGradient
FParamValues=2,0.181,0,1,1,0.25,1,1,0.5,1,1,0.75,1,1
ParamValues=2,181,0,1000,1000,250,1000,1000,500,1000,1000,750,1000,1000
Enabled=1

[AppSet37]
App=Postprocess\Blur
FParamValues=0.325
ParamValues=325
ParamValuesPostprocess\Youlean Blur=323,0,0,0,0,0
ParamValuesPostprocess\Youlean Bloom=217,0,120,320,1000,0,0,0
Enabled=1

[AppSet34]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1

[AppSet24]
App=Blend\BufferBlender
FParamValues=0,7,14,0.561,3,5,0.399,0,0.5,0.5,0.75,0
ParamValues=0,7,14,561,3,5,399,0,500,500,750,0
Enabled=1
ImageIndex=1

[AppSet33]
App=Misc\Automator
FParamValues=1,28,8,3,0,0.006,0.75,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,28,8,3,0,6,750,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1

[AppSet21]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1

[AppSet10]
App=Image effects\Image
FParamValues=0,0,0,0,0.996,0.5,0.5,0,0,0,0,0,1,1
ParamValues=0,0,0,0,996,500,500,0,0,0,0,0,1,1000
Enabled=1
ImageIndex=11

[AppSet9]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1

[AppSet3]
App=Image effects\Image
FParamValues=0,0,0,0,0,0.5,0.5,0.433,0,0,0,0,1,0.529
ParamValues=0,0,0,0,0,500,500,433,0,0,0,0,1,529
Enabled=1
ImageIndex=11

[AppSet4]
App=HUD\HUD 3D
FParamValues=0,0,0.6,0.504,0.5,0.5,0.5,0.5,0.5,0.5,0.493,0.5,0.5,0.222,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,0
ParamValues=0,0,600,504,500,500,500,500,500,500,493,500,500,222,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,0
Enabled=1
ImageIndex=1

[AppSet1]
App=Image effects\Image
FParamValues=0,0,0,0,0.766,0.5,0.5,0,0,0,0,0,0,1
ParamValues=0,0,0,0,766,500,500,0,0,0,0,0,0,1000
Enabled=1
ImageIndex=3

[AppSet7]
App=Postprocess\Youlean Blur
FParamValues=0.108,0,0,0,0,1
ParamValues=108,0,0,0,0,1
Enabled=1

[AppSet5]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1

[AppSet13]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=7

[AppSet14]
App=Blend\Youlean Mask
FParamValues=0
ParamValues=0
Enabled=1
ImageIndex=2

[AppSet15]
App=Postprocess\Youlean Blur
FParamValues=0.0086,0,0,0,0,1
ParamValues=8,0,0,0,0,1
Enabled=1
ImageIndex=11

[AppSet12]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1

[AppSet6]
App=Blend\BufferBlender
FParamValues=0,0,0,1,0,0,0,0,0.5,0.5,0.75,0
ParamValues=0,0,0,1000,0,0,0,0,500,500,750,0
Enabled=1
ImageIndex=7

[AppSet11]
App=Postprocess\Youlean Blur
FParamValues=0.1777,0,0,0,0,1
ParamValues=177,0,0,0,0,1
ParamValuesPostprocess\Youlean Bloom=61,0,150,455,1000,0,0,0
Enabled=1
ImageIndex=11

[AppSet8]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=4

[AppSet16]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1

[AppSet25]
App=Background\SolidColor
FParamValues=0,0,0,0.719
ParamValues=0,0,0,719
Enabled=1

[AppSet22]
App=HUD\HUD 3D
FParamValues=0,0,0.4957,0.5166,0.5528,0.5,0.5,0.5,0.5,0.5,0.4977,0.5,0.5,0.709,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=0,0,495,516,552,500,500,500,500,500,497,500,500,709,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
ImageIndex=5

[AppSet44]
App=Background\FogMachine
FParamValues=0.897,0,0,0,0.5,0.5,0.5,0.5,0.5,0.5,0,0
ParamValues=897,0,0,0,500,500,500,500,500,500,0,0
Enabled=1
ImageIndex=11

[AppSet19]
App=Misc\Automator
FParamValues=1,23,3,2,0.496,0.017,0.505,1,23,4,2,0.515,0.088,0.496,1,23,11,2,0.499,0.095,0.514,0,0,0,0,0,0.25,0.25
ParamValues=1,23,3,2,496,17,505,1,23,4,2,515,88,496,1,23,11,2,499,95,514,0,0,0,0,0,250,250
Enabled=1

[AppSet26]
App=Misc\Automator
FParamValues=1,23,5,2,0.515,0.023,0.52,1,12,1,2,0,0.023,0.594,1,16,1,2,0.054,0.023,0.476,0,0,0,0,0,0.25,0.25
ParamValues=1,23,5,2,515,23,520,1,12,1,2,0,23,594,1,16,1,2,54,23,476,0,0,0,0,0,250,250
Enabled=1

[AppSet28]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.4256,0.5,0.5,0.4051,0.5
ParamValues=500,425,500,500,405,500
Enabled=1

[AppSet29]
App=Misc\Automator
FParamValues=1,29,2,6,0.402,0.25,0.502,1,29,5,2,0.429,0.78,0.512,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,29,2,6,402,250,502,1,29,5,2,429,780,512,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1

[AppSet38]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1

[AppSet46]
App=Background\SolidColor
FParamValues=0,0,0,1
ParamValues=0,0,0,1000
Enabled=1

[AppSet39]
App=Canvas effects\Lava
FParamValues=0.7555,0,0.5,0.72,0.919,0.5,0.5,0.4,0.528,0.701,0
ParamValues=755,0,500,720,919,500,500,400,528,701,0
Enabled=1

[AppSet45]
App=Misc\Automator
FParamValues=1,40,1,6,0.847,0.094,0.491,1,45,1,6,0.958,0.094,0.494,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,40,1,6,847,94,491,1,45,1,6,958,94,494,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1

[AppSet42]
App=Postprocess\Youlean Blur
FParamValues=0.249,0,0,0,0,1
ParamValues=249,0,0,0,0,1
ParamValuesBackground\FogMachine=500,0,0,557,500,500,500,500,500,500,0,0
Enabled=1
ImageIndex=11

[AppSet41]
App=Postprocess\Vignette
FParamValues=0,0,0,0.6,0.299,0
ParamValues=0,0,0,600,299,0
Enabled=1

[AppSet40]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1

[AppSet43]
App=Blend\BufferBlender
FParamValues=0,0,0,1,1,1,0.148,0,0.5,0.5,0.75,0
ParamValues=0,0,0,1000,1,1,148,0,500,500,750,0
Enabled=1
ImageIndex=9

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text=""
Html="<p align=""center"">[textline]</p>"
Images="[plugpath]ComboWizard\Assets Revisualizer\bg mas cafe.jpg"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

