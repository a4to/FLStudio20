FLhd   0 / ` FLdt�
  �20.5.1.1193 ��  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4               A                        /  �  �    �HQV ��h
  ﻿[General]
GlWindowMode=1
LayerCount=4
FPS=1
MidiPort=-1
AspectRatio=16:9
LayerOrder=1,0,3,2

[AppSet1]
App=Background\FourCornerGradient
ParamValues=0,1000,840,1000,440,846,1000,300,688,1000,224,750,1000,232
Enabled=1

[AppSet0]
App=Object Arrays\BallZ
ParamValues=0,0,1000,1000,942,500,500,10,44,412,0,1000,525
ParamValuesImage effects\ImageSphereWarp=0,236,750,944,0,1000,0,500,500,500
ParamValuesCanvas effects\Lava=0,0,500,512,1000,500,500,500,672,500,0
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,584,668,780,824,500,500,500
ParamValuesImage effects\ImageTileSprite=4,4,0,0,0,0,4,250,250,500,500,500,500,0,0,0,0
ParamValuesMisc\FruityDanceLine=0,0,0,0,1000,500,500,0,300,100,0,0,0
ParamValuesHUD\HUD 3D=0,0,500,500,500,500,500,500,500,500,500,500,500,500,0,500,500,500,500,500,500,500,500,1000,1000,0,0,1000,1000,0
ParamValuesMisc\CoreDump=0,208,1000,0,500,500,500,0,0,0,0,0,1000,456,292,408,0
ParamValuesHUD\HUD Image=0,0,500,500,1000,1000,500,444,500,0,0,1000,1000,0,1000
ParamValuesCanvas effects\Flow Noise=0,0,0,720,608,756,332,100,0,0,0
Enabled=1

[AppSet3]
App=Object Arrays\BallZ
ParamValues=0,0,1000,1000,942,500,500,10,40,540,0,1000,270
Enabled=1

[AppSet2]
App=Misc\Automator
ParamValues=1,1,13,2,494,592,594,1,3,5,2,0,250,250,1,4,13,2,92,182,406,0,0,0,0,0,250,250
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""3""><position y=""3""><p align=""left""><font face=""American-Captain"" size=""10"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""3""><position y=""26""><p align=""left""><font face=""Chosence-Bold"" size=""8"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""0""><position y=""10""><p align=""right""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[extra1]</font></p></position>","<position x=""0""><position y=""3""><p align=""right""><font face=""Chosence-Bold"" size=""4"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""0""><position y=""20""><p align=""right""><font face=""Chosence-Bold"" size=""4 "" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""0""><position y=""25""><p align=""right""><font face=""Chosence-regular"" size=""3"" color=""#FFFFF"">[extra3]</font></p></position>",
VideoUseSync=0
Filtering=0

[Detached]
Top=126
Left=860
Width=689
Height=577

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

