FLhd   0 * ` FLdt�  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ի.'  ﻿[General]
GlWindowMode=1
LayerCount=18
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=52,61,50,62,66,67,69,71,73,0,6,39,51,63,65,68,70,72
WizardParams=1907,1908,1909,2434,3010,3202,3250,3346,3442

[AppSet52]
App=HUD\HUD Prefab
FParamValues=78,0.516,0,0,1,0.186,0.602,0.744,1,1,4,0,0.5,1,0.48,0,1,0
ParamValues=78,516,0,0,1000,186,602,744,1000,1000,4,0,500,1,480,0,1000,0
Enabled=1
LayerPrivateData=78014B2FCA4C89490712BA0606667A9939650C230B0000B8E805E5

[AppSet61]
App=Background\SolidColor
FParamValues=0,0.492,0,0.972
ParamValues=0,492,0,972
ParamValuesHUD\HUD Prefab=9,352,500,1000,0,500,500,290,1000,964,444,0,500,1000,368,0,240,1000
Enabled=1

[AppSet50]
App=HUD\HUD Prefab
FParamValues=7,0.616,0.772,1,0,0.4611,0.34,0.724,1,1,4,0,0.726,1,0.2865,0,1,0
ParamValues=7,616,772,1000,0,461,340,724,1000,1000,4,0,726,1,286,0,1000,0
Enabled=1
LayerPrivateData=78014B4A4CCE4E2FCA2FCD4B89490232750D0CCCF53273CA18460A0000F5E0084B

[AppSet62]
App=HUD\HUD Prefab
FParamValues=7,0.616,0.856,1,0,0.6015,0.28,0.724,1,1,4,0,0.726,1,0.2865,0,1,0
ParamValues=7,616,856,1000,0,601,280,724,1000,1000,4,0,726,1,286,0,1000,0
Enabled=1
LayerPrivateData=78014B4A4CCE4E2FCA2FCD4B89490232750D0CCCF53273CA18460A0000F5E0084B

[AppSet66]
App=HUD\HUD Prefab
FParamValues=7,0.616,0.716,1,0,0.4297,0.224,0.724,1,1,4,0,0.726,1,0.2865,0,1,0
ParamValues=7,616,716,1000,0,429,224,724,1000,1000,4,0,726,1,286,0,1000,0
Enabled=1
LayerPrivateData=78014B4A4CCE4E2FCA2FCD4B89490232750D0CCCF53273CA18460A0000F5E0084B

[AppSet67]
App=HUD\HUD Prefab
FParamValues=7,0.616,0.616,1,0,0.6015,0.16,0.724,1,1,4,0,0.726,1,0.2865,0,1,0
ParamValues=7,616,616,1000,0,601,160,724,1000,1000,4,0,726,1,286,0,1000,0
Enabled=1
LayerPrivateData=78014B4A4CCE4E2FCA2FCD4B89490232750D0CCCF53273CA18460A0000F5E0084B

[AppSet69]
App=HUD\HUD Prefab
FParamValues=7,0.616,0.648,1,0,0.5688,0.124,0.724,1,1,4,0,0.726,1,0.2865,0,1,0
ParamValues=7,616,648,1000,0,568,124,724,1000,1000,4,0,726,1,286,0,1000,0
Enabled=1
LayerPrivateData=78014B4A4CCE4E2FCA2FCD4B89490232750D0CCCF53273CA18460A0000F5E0084B

[AppSet71]
App=HUD\HUD Prefab
FParamValues=7,0.616,0.588,1,0,0.5785,0,0.724,1,1,4,0,0.726,1,0.327,0,1,0
ParamValues=7,616,588,1000,0,578,0,724,1000,1000,4,0,726,1,327,0,1000,0
Enabled=1
LayerPrivateData=78014B4A4CCE4E2FCA2FCD4B89490232750D0CCCF53273CA18460A0000F5E0084B

[AppSet73]
App=Misc\Automator
FParamValues=1,51,6,2,0.5,0.011,0.446,1,63,6,2,0.5,0.004,0.446,1,67,6,2,0.5,0.045,0.446,1,68,6,2,0.5,0.004,0.446
ParamValues=1,51,6,2,500,11,446,1,63,6,2,500,4,446,1,67,6,2,500,45,446,1,68,6,2,500,4,446
Enabled=1

[AppSet0]
App=Misc\Automator
FParamValues=1,70,6,2,0.5,0.016,0.446,1,72,6,2,0.5,0,0.446,0,67,6,2,0.5,0.016,0.446,0,68,6,2,0.5,0.016,0.446
ParamValues=1,70,6,2,500,16,446,1,72,6,2,500,0,446,0,67,6,2,500,16,446,0,68,6,2,500,16,446
Enabled=1

[AppSet6]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.768,0,0.15,0.266,1,0,0,0
ParamValues=768,0,150,266,1000,0,0,0
Enabled=1

[AppSet39]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1

[AppSet51]
App=Postprocess\ParameterShake
FParamValues=1,0.26,50,3,0.396,0,50,14,0,2,0,0,3,3,0,0,0,0,0,0,0.372,0.144,1,1,0,0
ParamValues=1000,260,50,3,396,0,50,14,0,2,0,0,3,3,0,0,0,0,0,0,372,144,1,1,0,0
Enabled=1

[AppSet63]
App=Postprocess\ParameterShake
FParamValues=1,0.26,62,3,0.396,0,62,14,0,2,0,0,3,3,0,0,0,0,0,0,0.372,0.144,1,1,0,0
ParamValues=1000,260,62,3,396,0,62,14,0,2,0,0,3,3,0,0,0,0,0,0,372,144,1,1,0,0
Enabled=1

[AppSet65]
App=Postprocess\ParameterShake
FParamValues=1,0.26,66,3,0.396,0,66,14,0,2,0,0,3,3,0,0,0,0,0,0,0.372,0.144,1,1,0,0
ParamValues=1000,260,66,3,396,0,66,14,0,2,0,0,3,3,0,0,0,0,0,0,372,144,1,1,0,0
Enabled=1

[AppSet68]
App=Postprocess\ParameterShake
FParamValues=1,0.26,67,3,0.396,0,67,14,0,2,0,0,3,3,0,0,0,0,0,0,0.372,0.144,1,1,0,0
ParamValues=1000,260,67,3,396,0,67,14,0,2,0,0,3,3,0,0,0,0,0,0,372,144,1,1,0,0
Enabled=1

[AppSet70]
App=Postprocess\ParameterShake
FParamValues=1,0.26,69,3,0.396,0,69,14,0,2,0,0,3,3,0,0,0,0,0,0,0.372,0.144,1,1,0,0
ParamValues=1000,260,69,3,396,0,69,14,0,2,0,0,3,3,0,0,0,0,0,0,372,144,1,1,0,0
Enabled=1

[AppSet72]
App=Postprocess\ParameterShake
FParamValues=1,0.26,71,3,0.452,0,71,14,0,2,0,0,3,3,0,0,0,0,0,0,0.372,0.144,0,0,0,0
ParamValues=1000,260,71,3,452,0,71,14,0,2,0,0,3,3,0,0,0,0,0,0,372,144,0,0,0,0
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""0""><position y=""10""><p align=""center""><font face=""American-Captain"" size=""9"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""0""><position y=""20""><p align=""center""><font face=""Chosence-Bold"" size=""4"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""0""><position y=""66""><p align=""center""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[extra1]</font></p></position>","<position x=""0""><position y=""80""><p align=""center""><font face=""Chosence-Bold"" size=""4"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""0""><position y=""86""><p align=""center""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""0""><position y=""90""><p align=""center""><font face=""Chosence-regular"" size=""2"" color=""#FFFFF"">[extra3]</font></p></position>"
Images="[presetpath]Wizard\Assets\Sacco\Panel 01.svg","[presetpath]Wizard\Assets\Sacco\Panel 01b black.svg"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

