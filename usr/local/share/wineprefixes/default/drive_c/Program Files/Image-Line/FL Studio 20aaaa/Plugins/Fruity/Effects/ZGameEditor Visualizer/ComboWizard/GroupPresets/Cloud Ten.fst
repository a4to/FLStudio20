FLhd   0  0 FLdt  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   Փ/�  ﻿[General]
GlWindowMode=1
LayerCount=8
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=2,6,5,4,3,0,1,7

[AppSet2]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
ParamValuesPeak Effects\Polar=0,0,0,0,172,500,158,300,556,500,128,420,0,631,1000,500,1000,1000,968,1000,1000,0,0,1000
ParamValuesPeak Effects\Youlean Peak Shapes=0,0,0,0,74,500,368,500,490,104,564,500,120,144,500,0,0,200,200,360,358,0,520,5,0,1000,0,500,500,0,0,0
Enabled=1
ImageIndex=1
Name=Section2

[AppSet6]
App=HUD\HUD 3D
FParamValues=0,1,0.5029,0.5113,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,0
ParamValues=0,1,502,511,500,500,500,500,500,500,500,500,500,500,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,0
Enabled=1
ImageIndex=3
Name=ForegroundMod

[AppSet5]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
Name=TextFst

[AppSet4]
App=Text\TextTrueType
FParamValues=0,0.511,0.298,0,0,0.5,0.5,0,0,0,0.5
ParamValues=0,511,298,0,0,500,500,0,0,0,500
Enabled=0
Name=Text

[AppSet3]
App=Postprocess\Youlean Drop Shadow
FParamValues=0.5,0.2,0,1,0.02,0.875,0
ParamValues=500,200,0,1000,20,875,0
Enabled=1
UseBufferOutput=1

[AppSet0]
App=Scenes\Cloud Ten
FParamValues=0,0,0,0,0,0,0.531
ParamValues=0,0,0,0,0,0,531
ParamValuesImage effects\Image=0,0,0,0,1000,500,500,0,0,0,1000,0,0,0
ParamValuesCanvas effects\Electric=0,333,611,842,120,212,500,73,0,1000,500
ParamValuesScenes\Alps=0,167,206,0,250,628,628,80,0
ParamValuesCanvas effects\Stack Trace=0,83,1000,498,130,343,500,280
Enabled=1
Name=Section0

[AppSet1]
App=Postprocess\Youlean Color Correction
FParamValues=0.5005,0.3684,0.5005,0.5,0.5,0.5
ParamValues=500,368,500,500,500,500
ParamValuesPostprocess\Vignette=0,0,0,600,664,200
ParamValuesPostprocess\WetInkSpiral=236,807,508,456,234,614
ParamValuesPostprocess\RGB Shift=1000,456,0,600,700,200
ParamValuesPostprocess\Watercolor=0,1000,1000,1000,250,250,200,0,250,500,0,500
ParamValuesPostprocess\ScanLines=0,143,0,304,0,0
ParamValuesPostprocess\Projection=24,140,24,0,24,0,24,176,0,0,0,0,0,0,0,0
ParamValuesPostprocess\Youlean Kaleidoscope=500,500,1000,500,1000,5,416,500,672,0,0
Enabled=1
Name=Section1

[AppSet7]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
ImageIndex=2

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Images="@EmbeddedFst:""Name=Dynamic movement.fst"",Data=7801C5944D6EDB301085F7067C87C06B3715295A3F4518C096AC18A8DDA671932CDC2C586B1A13902581A6D3F86C5DF448BD421F655B4D165D245D148224CE70E6E3F081C35F3F7E2E2EA824A38ABB6EE7A2B8D5655E7D9F553949D6ED4CD58E4C526D4B2BFD6E27BB9C3BE74CE7FAB23256BE8131DCD4B4B457CAEA4AB2E05D7CC8F9687232D2EBB33EEF76BA9DC5B0AEE7643D2C81919C5CA75FF09E7CA647EBB0CAA8F58D2AB6B4418A773AE8E3EBFE9E8F2F0B19FBE3C4848FF9533FE62E8CF57DBC4D4A938671F3A0B2E7D481B79F704C10FB47DB790104EE48F310E966F711CC992D735CAAAF05E5AD3497463F284BA9B24A8691C7425F8C78C2C722E669C6934C8CD281978AA1487996A4F026291F8EC4301905218BFC18706FE42333F2C4139DD84B758A50FD6910C7C1410A9FB9EDB8473897F03CFC5E2356D407B591E21912C023EF5FE4CAFC642CA224814CECEF320D1A99F838197A418063D81E27FE6299428811C6421C648A44D4887490C9E71C01AF9309D446A66748008FBCFF22D38DCEA93AA1C71ADD0AAD1A7322194EDBC1B8952CE647E38A4A346D566FA40FD75CADEB82D0D8248F01096E85E507B5268936DEE6BA7AEA68E09FB6AAD076E73266EAF13DED327436CD6BB5D4E53DB01E26325D50B9878CB4356E01160A167181C906DBBA79D4645C97CB6A5D1BDA6CD07A089A6F6B329BA63E67E23C5C6FDC35555A2ADD3EDDB5221756DB82EEFA0BB5B5ABCA6000C87A1F30B1EB42F6CEEA13547B5FCA5E6F093F995EEF7C61915BE892EECEDED6E7BD834CC0CF77E5D22D86EA11E976B35F3925AB962BCADDB2552DFD187D3CA56F56321F77E1ADCEED4A06118613D2F72B2B0761D854ECAA3555519041E615E5D38742F241802BD81095AD35C2A5D81AD3EDBA1D773BBF01D39E8249","@EmbeddedFst:""Name=Song Position horizontal.fst"",Data=78018D544B6EDB3010DD0BD01D04AF5D97A4FE4518C0DF06A8D3BA713E28922C58691213952541A2D3F86C5DF448BD426728DBB0D12C8A381467C8796FE671C83FBF7EDF7F84121A553CBACEC7E24E9779F5F3B2CA4172D799AB2D34E36A531AE9BBCE6CB124E7A5CEF5A26A8C7C87C6B0AD213357CAE84AF2E843BA8BF9D2E4D048D6E77DE13AAE733FACEB251886143893D77A0D1E12E94C99AA691FBE559B0254E959FF483544A51AB5BE55C5065A846183300AFAF46522212B8DAC25FC0829C81FC5E819F000770D429A0941234D190EF6AFDB89499F8277D088DBA15A4CC62884400932448B0079C8707E84C6193B415B80FAE14D9F9E5092F66151158A34D891C7A2C30993BE4F78446107240984A5F379E721BEE31D070C8C719D69A9BE1790D3511C94E5FFA3ECD2A875FD96B65D8A83204A51AED48A469AC5688900F5A6644867AB2CAD5825FBA7B563A55DDEAC4F4004D3D971FF0884AA26F75189B6BA93B26CDB2D1AFDA20C4C9451324E188FFDF1749C8859301D4F8251309D8471381E8E67411A719EC42922B3D1301AB260121F292376CA9C1CCD5C97F0669BA55D9B61A95C504351FF08665B49740D683DB4E47742ECA4D84BB3FFEE627948B1F88F23F3ED9CD3E8FBE426AC7FDA719F0225402A113DB7E4D68A58DFB73D684F0481F76AEEBF9D8788856D236415C8E9FB088581B6654F5BE856E75079F05AE3A546B5AC7921394BF080AD7127792AF6C6159478B767752B7D742DB1A50AC0FB0F72BF618C8F47F659AD41E26DDFE4BA3A7658BCAF1B5568B3A5884BF5FA09B633BCECB0AC55A6CB6784A52E9FE902CA0E64A44D43043C0E7822025CB4B007B7486CC44D9955EBBA81B6C5CB819B969B1A9AD6E64726DE959B965EB3D24049755EC3AB91BDEB956E3DFC991578393CA94D613C832B839EEB5C9875217B67B587E93E97B2D7CB30129A5EEFFC9EB614D8458F67EFEB73DC6AEB42FCE5B6CC880DD3C79D544E473D01A3B215E4C45BD5D24F037C29E1C948EEE39B79A773B3925182D30BD0CF2B23C3B86B624AB7A98A021A8CBC827CFE52481146F8543700E5C11AE1437930E69BF561EE3A7F017A538E8B"
VideoUseSync=0
Filtering=0

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

[Wizard]
Section0Cb=Cloud Ten 01
Section1Cb=Brightness-Gamma-Contrast
Section2Cb=Song Position horizontal
Section3Cb=Dynamic movement

