FLhd   0  0 FLdt  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   ՛%�  ﻿[General]
GlWindowMode=1
LayerCount=8
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=2,6,5,4,3,0,1,7

[AppSet2]
App=Peak Effects\Linear
FParamValues=0,0.964,0,0,0.178,0.748,0.5,0.112,0.25,0.5,0.196,0.35,0,1,1,0,0,0.5,0.5,0.5,0.468,1,0.5,0.152,0.2,0,0.032,0.212,0.33,0.25,0.1
ParamValues=0,964,0,0,178,748,500,112,250,500,196,350,0,1,1,0,0,500,500,500,468,1000,500,152,200,0,32,212,330,250,100
ParamValuesPeak Effects\Youlean Waveform=0,0,0,0,186,859,500,250,372,250,500,210,500,0,100,0,750,1000,16,88,0,500,100,100,0,0,282,500,500,1000,0,1000
ParamValuesPeak Effects\Youlean Peak Shapes=0,0,0,0,74,500,368,500,490,104,564,500,120,144,500,0,0,200,200,360,358,0,520,5,0,1000,0,500,500,0,0,0
ParamValuesPeak Effects\Stripe Peeks=0,0,0,0,0,0,74,500,220,0,250,500,700,0,500,150,300,200,200,300,1000,0,0
ParamValuesPeak Effects\Polar=0,0,0,0,172,500,158,300,556,500,128,420,0,631,1000,500,1000,1000,968,1000,1000,0,0,1000
ParamValuesPeak Effects\SplinePeaks=940,0,0,0,900,500,360,0,0,0
ParamValuesMisc\FruityIndustry=0,0,0,0,500,500,0,500,500,0,500,500,500
ParamValuesImage effects\Image=0,0,0,0,1000,500,500,0,0,0,1000,0,0,0
ParamValuesHUD\HUD Graph Polar=0,500,0,0,500,856,106,444,784,0,1000,0,1000,0,0,500,0,348,0,0,500,0
Enabled=1
ImageIndex=2
Name=Section2

[AppSet6]
App=HUD\HUD 3D
FParamValues=0,0,0.125,0.4731,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.4219,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,0
ParamValues=0,0,125,473,500,500,500,500,500,500,500,500,500,421,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,0
Enabled=1
ImageIndex=2
Name=ForegroundMod

[AppSet5]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
Name=TextFst

[AppSet4]
App=Text\TextTrueType
FParamValues=0,0.8333,0,1,0,0.5,0.5,0,0,0,0.5
ParamValues=0,833,0,1000,0,500,500,0,0,0,500
Enabled=0
Name=Text

[AppSet3]
App=Postprocess\Youlean Drop Shadow
FParamValues=0.5,0.2,0,1,0.02,0.875,0
ParamValues=500,200,0,1000,20,875,0
Enabled=1
UseBufferOutput=1

[AppSet0]
App=Youlean new shaders\Patterns\Arbitrary Weave
FParamValues=0,0.5,0,0.22,0.6,0,1,1,3,0
ParamValues=0,500,0,220,600,0,1,1,3,0
Enabled=1
Name=Section0

[AppSet1]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.2342,0.2683,0.5,0.5,0.5
ParamValues=500,234,268,500,500,500
ParamValuesPostprocess\Vignette=0,0,0,600,1000,0
ParamValuesPostprocess\Ascii=0,232,0,600,700,200
ParamValuesYoulean new shaders\Postprocess\Extruded Video Image=208,297,0
Enabled=1
Name=Section1

[AppSet7]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
ImageIndex=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="This is the default text."
Images="@EmbeddedFst:""Name=Chosen hero.fst"",Data=7801C594CD6EDB300CC7EF01F20E45CE5E27C9F2D75015F047DC006BB7AE59DB43D68316738D00C73614B96B9E6D873DD25E61949278E9B1DB61306C8BA4F817FD83C95F3F7E2E2EA0012DEB87F1E8A2BE574DD57EBF6A2B10743CBA945BD079DB3746F8E351793DB7CE2B55A9EB561BF1068D74D3C1D2DC48A35A41C377C93EE7A3AE400BE2518F8D47E3D122EDBA39188247E04ACC6E8B2F789F7C86676365A596EB3B59F7B0C114721A78F8B4EF88707CF190857FBC18612EC88208D7D4F3F176392E0FD7EEC2D25ECA06641740510F15BD836DBD2818307BAA5323B8D346773BA83507CD6923BFD6500D6CAEB57A92060A69A4886242233F9FE6312BF9342F78C6A7451005799A973C2928CB58C293D0A77190A02861249D923040B00320FA3A403EF51D196E3161FDA73E65FB5AB90DF891EFCC572242592F404D0BE085E291DEBF402A11128FF39C670525054F79C14AC495B0BC6069C6D33C0BA301D2344F49181E4362AF8684284242F0777190621E1F436221DDFD347F4109551DA517922878A0FE5F28DDA90ADA1378EEB04711953367829298EC8D7B411376306EA0C1562DBB8DF0D13597EBAE066C6710870D39CE82E507B90681CDDB57AA3D7638F14FBDAC95D9DA8C2BF9FC1EB625F633CC3BB954CD23CA120C94AA8666279229A3ED0134E234661C834E7670B3D865DC36CB76DD69D86CB0DF70D3BCEF406F5C7DD6C49EB9DDD8E1D41868EC77DA61221646991A1EBC85ECCDAAD5B84091F56EC3CCAC6B3139EB4EB0DAC7464C264BF4839E4CCE1706736BD5C0C3D9DBEE7CB2C784F2F36DB3B48761F5B8D37ECDEEE4028C5CAEA0B2C7B69DF0138E730FBE19417D9C80F7AA322B11C6B89C817A5C19114491ABD856ABDBBA068D9937505D3ED58205210E5E0DD00C5686A370302EFBF5B01E8F7E03BBC27FCC"
VideoUseSync=0
Filtering=0

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

[Wizard]
Section0Cb=Arbitrary Weave
Section1Cb=Brightness-Gamma-Contrast
Section2Cb=Linear Grad Vert Less bands Left polarity
Section3Cb=Chosen hero
Macro0=Song Title
Macro1=Song Author
Macro2=Song made with love and time

