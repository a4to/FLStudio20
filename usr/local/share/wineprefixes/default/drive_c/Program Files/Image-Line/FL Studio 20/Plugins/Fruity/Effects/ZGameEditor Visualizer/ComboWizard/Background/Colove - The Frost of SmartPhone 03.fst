FLhd   0 * ` FLdt�  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ն02  ﻿[General]
GlWindowMode=1
LayerCount=16
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,9,12,14,15,16,13,1,4,3,2,5,8,17,18,19
WizardParams=947

[AppSet0]
App=Background\SolidColor
FParamValues=0,0.892,1,0.124
ParamValues=0,892,1000,124
Enabled=1
Name=Background FX

[AppSet9]
App=HUD\HUD Grid
AppVersion=1
FParamValues=0,0.5,0,0,0.5,0.5,1,1,0.916,4,0.5,1,0.5,0.52,0.172,0.5,1,0.04,0.684,0.184,0,1
ParamValues=0,500,0,0,500,500,1000,1000,916,4,500,1000,500,520,172,500,1000,40,684,184,0,1
Enabled=1
UseBufferOutput=1
Name=Grid FX

[AppSet12]
App=Background\SolidColor
FParamValues=0,0.756,0.576,0.036
ParamValues=0,756,576,36
Enabled=1
Name=Background Main

[AppSet14]
App=HUD\HUD Prefab
FParamValues=38,0,0.5,0,0.852,0.5,0.148,1,1,1,4,0,0.5,1,0.2,0,1,0
ParamValues=38,0,500,0,852,500,148,1000,1000,1000,4,0,500,1,200,0,1000,0
ParamValuesBackground\SolidColor=0,792,760,120
ParamValuesHUD\HUD Grid=0,500,0,0,500,500,1000,1000,928,444,500,1000,500,520,0,500,1000,100,840,300,0,1000
Enabled=1
Name=Border BG 1
LayerPrivateData=78014B4ECCC9C92F2D51C8C9CC4B8D498670740D0CCCF43273CA1846000000C5620A49

[AppSet15]
App=HUD\HUD Prefab
FParamValues=38,0,0.5,0,0.852,0.5,0.856,1,1,1,4,0,1,1,0.2,0,1,0
ParamValues=38,0,500,0,852,500,856,1000,1000,1000,4,0,1000,1,200,0,1000,0
Enabled=1
Name=Border BG 2
LayerPrivateData=78014B4ECCC9C92F2D51C8C9CC4B8D498670740D0CCCF43273CA1846000000C5620A49

[AppSet16]
App=Postprocess\ScanLines
FParamValues=0,1,0,0,0
ParamValues=0,1,0,0,0
ParamValuesHUD\HUD Prefab=31,0,500,0,852,500,856,1000,1000,1000,444,0,1000,1000,200,0,1000,1000
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesHUD\HUD Text=0,500,0,0,168,344,500,0,0,0,0,0,100,0,0,750,1000,500,500,1000
ParamValuesPeak Effects\Polar=0,0,0,0,188,36,572,228,1000,856,596,448,0,500,0,500,0,1000,1000,1000,320,480,1000,1000
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesParticles\fLuids=0,0,0,0,500,0,500,0,0,0,500,500,500,0,500,250,500,0,0,500,500,500,0,0,500,0,0,250,500,0,0,0
ParamValuesParticles\PlasmaFlys=500,500,500,0,1000,500,500,500,0,500,500,500,500,500
ParamValuesFeedback\BoxedIn=0,0,0,1000,500,48,228,500,0,500
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesHUD\HUD Meter Linear=0,208,884,1000,0,0,0,824,128,753,224,68,0,11,500,0,584,560,404,364,568,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesFeedback\70sKaleido=0,0,212,1000,208,500
ParamValuesPeak Effects\Stripe Peeks=0,200,1000,1000,0,0,86,264,500,0,250,500,612,0,500,250,200,0,150,1000,1000,300,0
Enabled=1
Name=Grid

[AppSet13]
App=Image effects\Image
FParamValues=0,0,0,0,0.709,0.5,0.501,0.042,0,0.25,0,0,0,0
ParamValues=0,0,0,0,709,500,501,42,0,250,0,0,0,0
ParamValuesImage effects\ImageWall=0,0,0,0,0,0,0
ParamValuesHUD\HUD Image=0,0,500,500,1000,1000,500,444,500,0,0,1000,1000,500,1000
ParamValuesBackground\SolidColor=0,892,1000,124
ParamValuesImage effects\ImageSlices=0,0,0,0,500,496,488,0,0,0,500,269,1000,500,0,0,0
Enabled=1
ImageIndex=5
Name=LCD - Mask

[AppSet1]
App=Image effects\Image
FParamValues=0,0,0,0.864,0.947,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,864,947,500,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=2
Name=iPhone - Buttons

[AppSet4]
App=Image effects\Image
FParamValues=0.92,0,0,0,0.952,0.501,0.5,0,0.004,0,0,0,0,0
ParamValues=920,0,0,0,952,501,500,0,4,0,0,0,0,0
Enabled=0
ImageIndex=4
Name=iPhone - Glass

[AppSet3]
App=Image effects\Image
FParamValues=0,0,0,0.94,0.972,0.5,0.514,0,0,0,0,0,0,0
ParamValues=0,0,0,940,972,500,514,0,0,0,0,0,0,0
Enabled=1
ImageIndex=1
Name=iPhone - Border 2

[AppSet2]
App=Image effects\Image
FParamValues=0,0,0,0.872,0.947,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,872,947,500,500,0,0,0,0,0,0,0
Enabled=1
Name=iPhone - Border 1

[AppSet5]
App=Image effects\Image
FParamValues=0,0,0,0,0.95,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,950,500,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=3
Name=iPhone - FaceID

[AppSet8]
App=HUD\HUD Free Line
AppVersion=1
FParamValues=0,0.5,0,0.964,0.66,0.492,0.388,0.304,2,0.092,2,0.22,1,0.312,0,0.272,0.5
ParamValues=0,500,0,964,660,492,388,304,2,92,2,220,1,312,0,272,500
ParamValuesHUD\HUD Text=44,500,0,0,652,520,500,552,1000,1000,324,1,240,0,0,750,1000,536,504,1000
Enabled=1
Name=Line for EQ
LayerPrivateData=78016360606098E6DC6D07A4EC19181AF60369281865430262341CC809070051AC3EF1

[AppSet17]
App=Background\FourCornerGradient
FParamValues=12,0.112,0.924,0.68,1,0.638,0.668,1,0.784,1,1,0.794,0.468,1
ParamValues=12,112,924,680,1000,638,668,1000,784,1000,1000,794,468,1000
ParamValuesHUD\HUD Meter Linear=0,208,884,1000,0,0,0,824,128,753,224,68,0,11,500,0,584,560,404,364,568,1000
Enabled=1
Name=Filter Color

[AppSet18]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Name=Fade-in and out

[AppSet19]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="<position y=""86""><p align=""center""><font face=""Chosence-Bold"" size=""2"" color=""#fff"">Image-Line Software</font></p></position>","<position y=""88  ""><p align=""center""><font face=""Chosence-regular"" size=""2"" color=""#CCCCC1"">Exclusive Sounds</font></p></position>"
Html=," ",,,
Images=[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Border-1.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Border-2.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Buttons.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Face-id.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Glass.svg
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

