FLhd   0 * ` FLdtC  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV չ=�  ﻿[General]
GlWindowMode=1
LayerCount=20
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,8,14,17,10,12,15,11,6,13,19,7,1,4,3,2,5,18,9,16
WizardParams=467,517,518,519,520,521,803

[AppSet0]
App=Background\SolidColor
FParamValues=0,0.564,0.84,0.904
ParamValues=0,564,840,904
Enabled=1
Collapsed=1
Name=Background FX

[AppSet8]
App=Postprocess\Youlean Motion Blur
FParamValues=0.232,1,0.636,0.956,0,0,0
ParamValues=232,1,636,956,0,0,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesHUD\HUD Text=44,500,0,0,652,520,500,552,1000,1000,324,1,240,0,0,750,1000,536,504,1000
ParamValuesHUD\HUD Grid=148,144,0,0,500,500,1000,1000,1000,444,500,1000,500,888,232,500,1000,0,784,0,392,1000
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPostprocess\Youlean Bloom=548,708,326,282
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesObject Arrays\Filaments=1000,0,0,1000,500,500,500,500,500,500,0,0,500,0
ParamValuesObject Arrays\CubicMatrix=0,0,0,1000,932,500,500,500,500,0,0,0,0
ParamValuesObject Arrays\CubesGrasping=0,0,1000,1000,500,500,500,500,500,500,500
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,15,500,500,500,500,500
ParamValuesPostprocess\Blur=1000
ParamValuesPostprocess\Youlean Blur=500,1000,0
ParamValuesHUD\HUD Free Line=0,500,0,964,660,492,388,304,182,92,182,220,1000,312,0,272,500
ParamValuesPostprocess\Youlean Color Correction=520,392,792,500,1000,136
Enabled=1
Collapsed=1
ImageIndex=7
Name=Grid FX

[AppSet14]
App=HUD\HUD Grid
AppVersion=1
FParamValues=0,1,1,0,0.5,0.5,1,1,1,4,0.5,1,0.5,0.888,0.232,0.5,1,0,0.784,0,0.392,1
ParamValues=0,1000,1000,0,500,500,1000,1000,1000,4,500,1000,500,888,232,500,1000,0,784,0,392,1
ParamValuesHUD\HUD Prefab=31,0,500,0,852,500,148,1000,1000,1000,444,0,500,1000,200,0,1000,1000
ParamValuesBackground\SolidColor=0,792,760,120
Enabled=1
Collapsed=1
Name=HUD Best

[AppSet17]
App=Background\FourCornerGradient
FParamValues=7,0.504,0,1,1,0.856,1,1,0,1,1,0.852,1,1
ParamValues=7,504,0,1000,1000,856,1000,1000,0,1000,1000,852,1000,1000
ParamValuesHUD\HUD Meter Linear=0,208,884,1000,0,0,0,824,128,753,224,68,0,11,500,0,584,560,404,364,568,1000
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=Filter Color

[AppSet10]
App=HUD\HUD Prefab
FParamValues=0,0.092,0.5,0,0.036,0.502,0.661,0.32,1,1,4,0,0.5,1,0.368,0.102,1,1
ParamValues=0,92,500,0,36,502,661,320,1000,1000,4,0,500,1,368,102,1000,1
ParamValuesHUD\HUD Graph Radial=0,500,0,0,200,500,250,444,500,0,1000,0,1000,0,250,500,200,0,0,0,500,1000
ParamValuesHUD\HUD Meter Radial=0,248,1000,0,0,0,0,992,492,708,156,34,1000,0,374,0,1000,1000,500,1000
ParamValuesHUD\HUD Free Line=0,500,0,0,656,552,168,304,182,92,182,168,1000,312,0,280,500
ParamValuesHUD\HUD Graph Linear=0,500,0,0,816,468,1000,1000,250,444,500,1000,212,1000,0,500,200,0,0,0,500,1000
ParamValuesHUD\HUD Meter Linear=0,500,0,0,0,0,750,0,800,664,300,100,0,125,500,1,238,0,0,1000,612,1000
Enabled=1
UseBufferOutput=1
Name=LOGO
LayerPrivateData=780173C8CBCF4BD52B2E4B671899000060F9036F

[AppSet12]
App=Background\SolidColor
FParamValues=0,0.524,0,0
ParamValues=0,524,0,0
Enabled=1
Collapsed=1
Name=Background Main

[AppSet15]
App=HUD\HUD Prefab
FParamValues=7,0.756,0.5,0,0.484,0.5,0.5,0.3,1,1,4,0,0.5,0,0.444,0,1,1
ParamValues=7,756,500,0,484,500,500,300,1000,1000,4,0,500,0,444,0,1000,1
Enabled=1
Collapsed=1
Name=Border BG
LayerPrivateData=78014B4A4CCE4E2FCA2FCD4B89490232750D0CCCF53273CA18460A0000F5E0084B

[AppSet11]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0.14,0.056,1,1,0.5,0.357,1,0.284,1,4,0.5,1,0.284,0.644,0,0.784,0.076,0.244,0.312,2,0,1
ParamValues=140,56,1000,1000,500,357,1000,284,1000,4,500,1,284,644,0,784,76,244,312,2,0,1
ParamValuesHUD\HUD Free Line=0,500,0,0,656,552,168,304,182,92,182,168,1000,312,0,280,500
Enabled=1
Collapsed=1
Name=EQ UP

[AppSet6]
App=HUD\HUD Graph Linear
AppVersion=1
FParamValues=0.14,0.536,1,1,0.5,0.642,1,0.284,1,4,1,1,0.284,0.644,0,0.784,0.076,0.244,0.312,2,0,1
ParamValues=140,536,1000,1000,500,642,1000,284,1000,4,1000,1,284,644,0,784,76,244,312,2,0,1
ParamValuesHUD\HUD Text=60,500,0,1000,660,686,500,552,1000,1000,324,1,240,0,0,750,1000,536,504,1000
Enabled=1
Collapsed=1
Name=EQ DOWN

[AppSet13]
App=Image effects\Image
FParamValues=0,0,0,0,0.708,0.5,0.5,0.042,0,0.25,0,0,0,0
ParamValues=0,0,0,0,708,500,500,42,0,250,0,0,0,0
ParamValuesImage effects\ImageWall=0,0,0,0,0,0,0
ParamValuesHUD\HUD Image=0,0,500,500,1000,1000,500,444,500,0,0,1000,1000,500,1000
ParamValuesBackground\SolidColor=0,892,1000,124
ParamValuesImage effects\ImageSlices=0,0,0,0,500,496,488,0,0,0,500,269,1000,500,0,0,0
Enabled=1
Collapsed=1
ImageIndex=6
Name=LCD - Mask

[AppSet19]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=5

[AppSet7]
App=Text\TextTrueType
FParamValues=0.08,0,0,0,0,0.493,0.5,0,0,0,0.5
ParamValues=80,0,0,0,0,493,500,0,0,0,500
Enabled=1
Name=Main Text

[AppSet1]
App=Image effects\Image
FParamValues=0,0,0,0.484,0.946,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,484,946,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=iPhone - Buttons

[AppSet4]
App=Image effects\Image
FParamValues=0.64,0,0,0,0.948,0.5,0.498,0,0.002,0,0,0,0,0
ParamValues=640,0,0,0,948,500,498,0,2,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=3
Name=iPhone - Glass

[AppSet3]
App=Image effects\Image
FParamValues=0,0,0,0.92,0.944,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,920,944,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=4
Name=iPhone - Border 2

[AppSet2]
App=Image effects\Image
FParamValues=0,0,0,0.852,0.946,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,852,946,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
Name=iPhone - Border 1

[AppSet5]
App=Image effects\Image
FParamValues=0,0,0,0,0.946,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,946,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=2
Name=iPhone - FaceID

[AppSet18]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fade-in and out

[AppSet9]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
Collapsed=1

[AppSet16]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""3"" y=""2""><p align=""left""><font face=""American-Captain"" size=""8"" color=""#333333"">[author]</font></p></position>","<position x=""3"" y=""10""><p align=""left""><b><font face=""Chosence-Bold"" size=""6"" color=""#333333"">[title]</font></b></p></position>","<position y=""92""><p align=""right""><b><font face=""Chosence-Bold"" size=""3"" color=""#333333"">[comment]</font></b></p></position>"," ",,," ",,,
Images=[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Border-1.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Buttons.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Face-id.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Glass.svg,[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Border-6.svg
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

