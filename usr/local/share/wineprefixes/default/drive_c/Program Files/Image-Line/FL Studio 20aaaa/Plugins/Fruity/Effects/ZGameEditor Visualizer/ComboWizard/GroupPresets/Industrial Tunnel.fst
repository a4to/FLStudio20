FLhd   0  0 FLdt�  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   Յ)�  ﻿[General]
GlWindowMode=1
LayerCount=8
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=2,6,5,4,3,0,1,7

[AppSet2]
App=Peak Effects\Youlean Waveform
FParamValues=0,0.8333,0,0,0.186,0.5,0.148,0.5,0.26,0.25,0.5,0.21,0.5,0,0.1,0,1,1,15,0.12,0,0.5,0.1,0.1,0,0,0.282,0.5,0.5,1,0,1
ParamValues=0,833,0,0,186,500,148,500,260,250,500,210,500,0,100,0,1,1,15,120,0,500,100,100,0,0,282,500,500,1,0,1000
ParamValuesHUD\HUD Graph Polar=0,500,0,0,500,856,106,444,784,0,1000,0,1000,0,0,500,0,348,0,0,500,0
ParamValuesImage effects\Image=0,0,0,0,1000,500,500,0,0,0,1000,0,0,0
ParamValuesMisc\FruityIndustry=0,0,0,0,500,500,0,500,500,0,500,500,500
ParamValuesPeak Effects\Linear=0,964,0,0,178,752,508,112,250,500,196,350,1000,1000,500,0,0,500,500,500,468,0,500,152,200,0,32,212,330,250,100
ParamValuesPeak Effects\Polar=0,833,0,1000,216,500,158,300,0,500,180,520,368,631,1000,500,0,1000,1000,0,0,0,0,1000
ParamValuesPeak Effects\SplinePeaks=940,0,0,0,900,500,360,0,0,0
ParamValuesPeak Effects\Stripe Peeks=0,0,0,0,0,0,74,500,220,0,250,500,700,0,500,150,300,200,200,300,1000,0,0
ParamValuesPeak Effects\Youlean Peak Shapes=0,0,0,0,74,500,368,500,490,104,564,500,120,144,500,0,0,200,200,360,358,0,520,5,0,1000,0,500,500,0,0,0
Enabled=1
ImageIndex=2
Name=Section2

[AppSet6]
App=HUD\HUD 3D
FParamValues=0,0,0.7983,0.631,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.207,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,0
ParamValues=0,0,798,631,500,500,500,500,500,500,500,500,500,207,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,0
Enabled=1
ImageIndex=2
Name=ForegroundMod

[AppSet5]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
Name=TextFst

[AppSet4]
App=Text\TextTrueType
FParamValues=0,0.8333,0,0,0,0.5,0.5,0,0,0,0.5
ParamValues=0,833,0,0,0,500,500,0,0,0,500
Enabled=0
Name=Text

[AppSet3]
App=Postprocess\Youlean Drop Shadow
FParamValues=0.5,0.2,0,1,0.02,0.875,0
ParamValues=500,200,0,1000,20,875,0
Enabled=1
UseBufferOutput=1

[AppSet0]
App=Youlean new shaders\Tunnel\Industrial Tunnel
FParamValues=0,0.5,0,0.5,0.6
ParamValues=0,500,0,500,600
ParamValuesCanvas effects\Electric=0,333,611,842,120,212,500,73,0,1000,500
ParamValuesImage effects\Image=0,0,0,0,1000,500,500,0,0,0,1000,0,0,0
ParamValuesObject Arrays\Filaments=0,0,0,0,500,500,716,500,292,296,212,0,500,0
ParamValuesPeak Effects\Fluidity=0,488,1000,0,472,508,512
ParamValuesScenes\Alien Thorns=0,500,542,724,4,500,500,0
ParamValuesScenes\Alps=0,71,362,0,250,348,304,0,0
ParamValuesScenes\Cloud Ten=0,0,0,0,272,1000,531,0,0,0,0
ParamValuesScenes\Frozen Wasteland=0,0,0,0,364,1,0,0,0,0,0
Enabled=1
Name=Section0

[AppSet1]
App=Postprocess\Vignette
FParamValues=0,0,0,0.5996,1,0
ParamValues=0,0,0,599,1000,0
ParamValuesPostprocess\Ascii=0,232,0,600,700,200
ParamValuesPostprocess\Dot Matrix=1000,500,500,0,368,500,1000,1000
ParamValuesPostprocess\RGB Shift=246,300,0,600,700,200
ParamValuesPostprocess\Youlean Color Correction=500,500,500,500,500,500
ParamValuesYoulean new shaders\Postprocess\Extruded Video Image=208,297,0
Enabled=1
Name=Section1

[AppSet7]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
ImageIndex=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Images="@EmbeddedFst:""Name=Tech showdown.fst"",Data=7801C554C16EDB300CBD07C83F14397B9D24DBB13D54051C276980A55BD7ACED21EB41B3B94680631B8ADC35DFB6C33E69BFB027A7C9DAC30E5D0F83608BA4C847EA41E4AF1F3F9767549151E56DBF7756DEE8AAA8BF9FD70549DEEFCDD5964C56B795957EBF37BD5838E3B92EF4456DAC7C0325DD3494DB4B65752DF9F05DF218F3D1146424F3B827FABD7E6F9936CD822C430A48727635FE82EFE8333D5807AB8C5A5FABB2A50D42D871E8E18F3D8912812D0AA2E11F2B4EC208AA1F310E59783E9274315D9CB3B985D29EC3866C7700500F88DE5E775600020E4287C6E0E94E771EDCA9BB05CC49A5BE96541CB8B930FA5E591A2BAB6414333EF5C53448B3546422C9D26C3AF6A3D0ED4132E66224922019FA3C0E1380B22062211B46E91382F84B098A63DC3D0EC198BBFDB1CF4158B7842345443198FB178E62CF813A069E4102708FF71A96223F9B6431989A64E360144CC6E1DF59122C9DB06188E7777846E2C52C396EE224D8D31407EE1A6E7534F1D7D094041D4DCF2001F85F69BAD605D547F4D0A04BC155A7CE2467317B546E244FC45EB9A40ACD3A6D36D28769A1D64D496868927B870CD320FFA0D624D1BE6DA1EBA7860EFC53AB4A6DB72EE25C3DBCA7ED141D4D8B46E5BABA032CC3C1549754ED4046DA1A978047018F4580C30EF6601671177155E5F5BA31B4D9A0E3E0B4681B329BAE3EA7E23D5C6DDC78AA2C55EE9E6E9CC8A5D5B6A45B6FA95ABBAA0D0480AC770E33BB2EE5E0A43942B577951C0C72D8C90C06A74B8BD85257747BF2B6391D3CD204F8C5B6CA5D32540F4F779B5DE6315995AFA87069EB46FA4980C947DFACE43E66E08D2EEC4A0E638833D2772B2BC328EA2A76D59ABA2CC920F2928AF97D294538C4E83544D5411B61181E9479BB3EC8FDDE6F674F8065"
VideoUseSync=0
Filtering=0

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

[Wizard]
Section0Cb=Industrial Tunnel
Section1Cb=Vignette
Section2Cb=Waveform Horiz 02
Section3Cb=Tech showdown
Macro0=Song Title
Macro1=Song Author
Macro2=Song made with love and time

