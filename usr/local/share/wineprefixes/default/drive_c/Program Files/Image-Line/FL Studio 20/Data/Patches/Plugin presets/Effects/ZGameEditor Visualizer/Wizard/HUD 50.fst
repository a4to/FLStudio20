FLhd   0  0 FLdt
  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   Ֆ;�  ﻿[General]
GlWindowMode=1
LayerCount=29
FPS=2
MidiPort=-1
Aspect=1
LayerOrder=18,7,29,13,9,4,2,1,6,10,16,3,0,11,8,5,12,15,17,21,22,23,25,26,24,27,28,19,20
WizardParams=130,1381,1382,1383,1384,1385,466,658,82

[AppSet18]
App=Background\SolidColor
ParamValues=0,0,0,1000
Enabled=1
Collapsed=1

[AppSet7]
App=Misc\Automator
ParamValues=1,14,13,3,1000,4,250,1,10,13,3,0,4,750,1,5,13,3,1000,2,250,0,0,0,0,0,250,250
Enabled=1
Collapsed=1

[AppSet29]
App=HUD\HUD Prefab
ParamValues=66,884,500,0,0,500,500,708,1000,1000,4,0,500,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4B2FCA4C89490712BA0606867A9939650C230B0000B42405E0

[AppSet13]
App=HUD\HUD Prefab
ParamValues=17,0,500,936,0,364,500,214,1000,1000,4,0,603,1,192,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0CCCF53273CA18863D00000E3B0AA2

[AppSet9]
App=HUD\HUD Prefab
ParamValues=14,0,820,936,0,60,536,250,1000,1000,4,0,397,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C4CF43273CA18863D00000B860A9F

[AppSet4]
App=HUD\HUD Prefab
ParamValues=13,0,112,936,0,548,500,250,1000,1000,4,0,802,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C8CF53273CA18863D00000A9F0A9E

[AppSet2]
App=HUD\HUD Prefab
ParamValues=36,0,456,364,0,348,500,1000,1000,1000,4,0,500,1,0,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C0CF53273CA184600000026F60AAD

[AppSet1]
App=HUD\HUD Prefab
ParamValues=41,0,340,288,0,340,656,1000,1000,1000,4,0,500,1,0,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0CCCF43273CA18460000002B830AB2

[AppSet6]
App=Postprocess\ParameterShake
ParamValues=432,0,1,14,492,0,2,14,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet10]
App=Postprocess\ParameterShake
ParamValues=332,364,11,1,220,556,12,4,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet16]
App=Postprocess\ParameterShake
ParamValues=144,364,15,7,332,556,12,4,0,0,0,1,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet3]
App=Postprocess\Youlean Bloom
ParamValues=500,0,30,455,1000,0,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet0]
App=Background\SolidColor
ParamValues=0,0,0,880
Enabled=1
Collapsed=1

[AppSet11]
App=Image effects\ImageSphereWarp
ParamValues=48,0,614,688,0,544,0,500,500
ParamValuesHUD\HUD 3D=0,0,500,500,500,500,500,500,500,500,500,500,500,500,0,500,500,500,500,500,500,500,500,1000,1000,0,0,1000,1000,0
Enabled=1
Collapsed=1
ImageIndex=9

[AppSet8]
App=Postprocess\Youlean Color Correction
ParamValues=500,500,500,500,500,664
Enabled=1
Collapsed=1

[AppSet5]
App=Postprocess\FrameBlur
ParamValues=60,0,0,0,650
Enabled=1
Collapsed=1

[AppSet12]
App=Image effects\Image
ParamValues=0,0,0,1000,0,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet15]
App=HUD\HUD Prefab
ParamValues=16,0,500,1000,0,500,500,0,277,277,4,0,0,1,368,102,243,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0CCCF43273CA18863D00000D540AA1

[AppSet17]
App=Postprocess\ParameterShake
ParamValues=1000,0,15,12,332,556,12,4,3,0,0,1,6,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet21]
App=Image effects\Image
ParamValues=848,0,0,0,661,587,652,0,0,0,0,1,0,0
Enabled=1
Collapsed=1
ImageIndex=2

[AppSet22]
App=Image effects\Image
ParamValues=848,0,0,0,661,395,346,0,0,500,0,1,0,0
Enabled=1
Collapsed=1
ImageIndex=2

[AppSet23]
App=HUD\HUD Prefab
ParamValues=68,464,848,0,0,500,500,55,728,1000,4,0,500,1,484,194,19,1
ParamValuesBackground\Grid=250,0,0,1000,1000,1000,1000,0,0,0
Enabled=1
Collapsed=1
LayerPrivateData=78DA4B2FCA4C89490712BA0606C67A9939650C230B0000B60C05E2

[AppSet25]
App=HUD\HUD Prefab
ParamValues=70,844,500,0,0,500,500,55,728,1000,4,0,500,1,484,995,922,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4B2FCA4C89490712BA0606A67A9939650C230B0000B7F405E4

[AppSet26]
App=HUD\HUD Prefab
ParamValues=76,784,500,0,0,500,500,55,728,1000,4,0,500,1,484,812,828,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4B2FCA4C89490712BA0686867A9939650C230B0000B51905E1

[AppSet24]
App=Misc\Automator
ParamValues=1,24,16,2,500,890,250,1,24,17,2,500,638,250,1,26,16,2,500,1000,250,1,26,17,2,500,718,250
Enabled=1
Collapsed=1

[AppSet27]
App=Misc\Automator
ParamValues=1,27,16,2,500,586,250,1,27,17,2,500,450,250,1,26,16,2,500,1000,250,1,26,17,2,500,718,250
Enabled=1
Collapsed=1

[AppSet28]
App=HUD\HUD Prefab
ParamValues=0,0,500,0,0,500,500,274,1000,1000,4,0,500,0,368,96,1000,1
Enabled=1
Name=LOGO
LayerPrivateData=78DA73C8CBCF4BD52B2E4B671899000060F9036F

[AppSet19]
App=Text\TextTrueType
ParamValues=552,0,0,1000,0,500,497,0,0,0,500
Enabled=1
Collapsed=1

[AppSet20]
App=Text\TextTrueType
ParamValues=0,0,0,0,0,500,500,0,0,0,500
Enabled=1
Collapsed=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0

[UserContent]
Text="This is the default text."
Html="<position x=""3""><position y=""4""><p align=""center""><font face=""American-Captain"" size=""10"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""3""><position y=""14""><p align=""center""><font face=""Chosence-Bold"" size=""6"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""0""><position y=""86""><p align=""right""><font face=""Chosence-Bold"" size=""4"" color=""#FFFFFF"">[extra1]</font></p></position>","<position x=""0""><position y=""91""><p align=""right""><font face=""Chosence-Bold"" size=""4"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""3""><position y=""86""><p align=""left""><font face=""Chosence-Bold"" size=""5"" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""3""><position y=""91""><p align=""left""><font face=""Chosence-regular"" size=""5"" color=""#FFFFF"">[extra3]</font></p></position>"
Meshes=[plugpath]Content\Meshes\Brain.zgeMesh,[plugpath]Content\Meshes\Car.zgeMesh,[plugpath]Content\Meshes\CircularPlane.zgeMesh,[plugpath]Content\Meshes\Cone(Textue).zgeMesh,[plugpath]Content\Meshes\Cone.zgeMesh,[plugpath]Content\Meshes\Cube.zgeMesh,[plugpath]Content\Meshes\Cylinder(Texture).zgeMesh,[plugpath]Content\Meshes\Cylinder.zgeMesh,[plugpath]Content\Meshes\Drone.zgeMesh,[plugpath]Content\Meshes\Hero.zgeMesh,[plugpath]Content\Meshes\Heroine.zgeMesh,[plugpath]Content\Meshes\Monkey.zgeMesh,[plugpath]Content\Meshes\Plane.zgeMesh,[plugpath]Content\Meshes\Skull.zgeMesh,[plugpath]Content\Meshes\Sphere(Ico).zgeMesh,[plugpath]Content\Meshes\Sphere(Texture).zgeMesh,[plugpath]Content\Meshes\Sphere.zgeMesh,[plugpath]Content\Meshes\Sphere.zgeMesh,"[plugpath]Content\Meshes\Spheres 1.zgeMesh",[plugpath]Content\Meshes\Torus.zgeMesh,"[plugpath]Content\Meshes\World map.zgeMesh"
Images=[plugpath]Content\Bitmaps\Particles\earth1.png,"[presetpath]Wizard\Assets\Sacco\Panel 01.svg","[presetpath]Wizard\Assets\Sacco\Panel 01b black.svg","[presetpath]Wizard\Assets\Sacco\Panel 01b.svg","[presetpath]Wizard\Assets\Sacco\Panel 02.svg","[presetpath]Wizard\Assets\Sacco\Panel 03.svg","[presetpath]Wizard\Assets\Sacco\Panel 03b.svg","[presetpath]Wizard\Assets\Sacco\Panel 04.svg","[presetpath]Wizard\Assets\Sacco\Panel 05.svg"
VideoUseSync=0
EnableMipmap=1

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

