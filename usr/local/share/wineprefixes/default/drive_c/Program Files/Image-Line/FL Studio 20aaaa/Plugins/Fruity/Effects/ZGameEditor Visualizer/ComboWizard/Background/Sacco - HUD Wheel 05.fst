FLhd   0 * ` FLdt�E  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ՗�E  ﻿[General]
GlWindowMode=1
LayerCount=60
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=59,60,56,53,54,52,0,1,43,2,3,4,42,5,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,45,46,47,44,35,49,50,51,48,6,36,57,40,55,41,39,58,61
WizardParams=1907,1908,1909,320,321,322,323

[AppSet59]
App=HUD\HUD Prefab
FParamValues=86,0,0.5,0,0,0.4557,0.4879,1,1,1,4,0,0.5,1,0.368,0,1,0
ParamValues=86,0,500,0,0,455,487,1000,1000,1000,4,0,500,1,368,0,1000,0
Enabled=1
LayerPrivateData=78014B2FCA4C89490712BA0686267A9939650C230B0000B7F505E4

[AppSet60]
App=Misc\Automator
FParamValues=1,60,6,2,0.5,0,0.39,1,60,7,2,0.5,0.012,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,60,6,2,500,0,390,1,60,7,2,500,12,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1

[AppSet56]
App=Postprocess\Vignette
FParamValues=0,0,0,0.6,0.7,0.2
ParamValues=0,0,0,600,700,200
Enabled=1
UseBufferOutput=1

[AppSet53]
App=HUD\HUD Prefab
FParamValues=86,0,0.5,0,0,0.4557,0.4879,0.66,1,1,4,0,0.5,1,0.368,0,1,0
ParamValues=86,0,500,0,0,455,487,660,1000,1000,4,0,500,1,368,0,1000,0
Enabled=1
LayerPrivateData=78014B2FCA4C89490712BA0686267A9939650C230B0000B7F505E4

[AppSet54]
App=Misc\Automator
FParamValues=1,54,6,2,0.5,0,0.39,1,54,7,2,0.5,0.012,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,54,6,2,500,0,390,1,54,7,2,500,12,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1

[AppSet52]
App=Postprocess\Vignette
FParamValues=0,0,0,0.6,0.508,0.2
ParamValues=0,0,0,600,508,200
Enabled=1
UseBufferOutput=1

[AppSet0]
App=HUD\HUD Prefab
FParamValues=73,0.336,0.852,1,0,0.5,0.5,0.29,1,0.964,4,0,0.5,1,0.704,0,0.035,0
ParamValues=73,336,852,1000,0,500,500,290,1000,964,4,0,500,1,704,0,35,0
ParamValuesHUD\HUD 3D=0,0,500,520,440,496,452,396,316,500,576,500,500,440,20,500,500,1000,500,500,644,500,500,1000,1000,0,0,1000,1000,500
Enabled=1
LayerPrivateData=78014B2FCA4C89490712BA0606867A9939650C230B0000B42405E0

[AppSet1]
App=HUD\HUD Prefab
FParamValues=73,0.296,0.852,1,0,0.5,0.5,0.29,1,0.964,4,0,0.5,1,0.6562,0.25,0.2481,0
ParamValues=73,296,852,1000,0,500,500,290,1000,964,4,0,500,1,656,250,248,0
Enabled=1
LayerPrivateData=78014B2FCA4C89490712BA0606867A9939650C230B0000B42405E0

[AppSet43]
App=HUD\HUD Grid
AppVersion=1
FParamValues=0.212,0.852,1,0,0.5,0.5,1,1,1,4,0.5,0.424,1,0.592,0,1,1,0.1,0.5,0,0,0
ParamValues=212,852,1000,0,500,500,1000,1000,1000,4,500,424,1000,592,0,1000,1000,100,500,0,0,0
ParamValuesHUD\HUD Prefab=66,296,852,1000,0,500,500,290,1000,964,444,0,500,1000,519,250,470,1000
Enabled=1

[AppSet2]
App=HUD\HUD Prefab
FParamValues=73,0.244,0.852,1,0,0.5,0.5,0.29,1,0.964,4,0,0.5,1,0.6022,0.5,0.517,0
ParamValues=73,244,852,1000,0,500,500,290,1000,964,4,0,500,1,602,500,517,0
Enabled=1
LayerPrivateData=78014B2FCA4C89490712BA0606867A9939650C230B0000B42405E0

[AppSet3]
App=HUD\HUD Prefab
FParamValues=73,0.352,0.852,1,0,0.5,0.5,0.29,1,0.964,4,0,0.5,1,0.482,0.738,0.6831,0
ParamValues=73,352,852,1000,0,500,500,290,1000,964,4,0,500,1,482,738,683,0
Enabled=1
LayerPrivateData=78014B2FCA4C89490712BA0606867A9939650C230B0000B42405E0

[AppSet4]
App=Misc\Automator
FParamValues=1,1,17,2,0.125,0.026,0.438,1,2,17,2,0.372,0.023,0.438,0,3,17,2,0.622,0.019,0.438,1,4,17,2,0.807,0.023,0.438
ParamValues=1,1,17,2,125,26,438,1,2,17,2,372,23,438,0,3,17,2,622,19,438,1,4,17,2,807,23,438
Enabled=1

[AppSet42]
App=Misc\Automator
FParamValues=1,1,15,2,0.633,0.126,0.438,1,2,15,2,0.548,0.439,0.438,1,3,15,2,0.658,0.243,0.438,1,4,15,2,0.591,0.383,0.438
ParamValues=1,1,15,2,633,126,438,1,2,15,2,548,439,438,1,3,15,2,658,243,438,1,4,15,2,591,383,438
Enabled=1

[AppSet5]
App=HUD\HUD Prefab
FParamValues=84,0.844,0.852,1,0,0.5,0.5,0.29,1,0.964,4,0,0.5,1,0.433,0,1,0
ParamValues=84,844,852,1000,0,500,500,290,1000,964,4,0,500,1,433,0,1000,0
Enabled=1
LayerPrivateData=78014B2FCA4C89490712BA0686467A9939650C230B0000B60D05E2

[AppSet7]
App=HUD\HUD Prefab
FParamValues=18,0,0.436,1,0,1,0.5,0.301,1,1,4,0,0.221,1,0.293,0,0.096,1
ParamValues=18,0,436,1000,0,1000,500,301,1000,1000,4,0,221,1,293,0,96,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0CF53273CA18863D000008D10A9C

[AppSet8]
App=HUD\HUD Prefab
FParamValues=18,0,0.436,1,0,1,0.5,0.353,1,1,4,0,0.063,1,0.2997,0.096,0.129,1
ParamValues=18,0,436,1000,0,1000,500,353,1000,1000,4,0,63,1,299,96,129,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0CF53273CA18863D000008D10A9C

[AppSet9]
App=HUD\HUD Prefab
FParamValues=18,0,0.436,1,0,1,0.5,0.297,1,1,4,0,0.126,1,0.518,0.13,0.445,1
ParamValues=18,0,436,1000,0,1000,500,297,1000,1000,4,0,126,1,518,130,445,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0CF53273CA18863D000008D10A9C

[AppSet10]
App=HUD\HUD Prefab
FParamValues=18,0,0.436,1,0,1,0.5,0.301,1,1,4,0,0.095,1,0.365,0.441,0.529,1
ParamValues=18,0,436,1000,0,1000,500,301,1000,1000,4,0,95,1,365,441,529,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0CF53273CA18863D000008D10A9C

[AppSet11]
App=HUD\HUD Prefab
FParamValues=18,0,0.436,1,0,1,0.5,0.451,1,1,4,0,0.158,1,0.3496,0.54,0.578,1
ParamValues=18,0,436,1000,0,1000,500,451,1000,1000,4,0,158,1,349,540,578,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0CF53273CA18863D000008D10A9C

[AppSet12]
App=HUD\HUD Prefab
FParamValues=18,0,0.436,1,0,1,0.5,0.294,1,1,4,0,0.095,1,0.206,0.578,0.641,1
ParamValues=18,0,436,1000,0,1000,500,294,1000,1000,4,0,95,1,206,578,641,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0CF53273CA18863D000008D10A9C

[AppSet13]
App=HUD\HUD Prefab
FParamValues=18,0,0.436,1,0,1,0.5,0.294,1,1,4,0,0.095,1,0.182,0.643,0.655,1
ParamValues=18,0,436,1000,0,1000,500,294,1000,1000,4,0,95,1,182,643,655,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0CF53273CA18863D000008D10A9C

[AppSet14]
App=HUD\HUD Prefab
FParamValues=18,0,0.436,1,0,1,0.5,0.282,1,1,4,0,0.756,1,0.2997,0.657,0.97,1
ParamValues=18,0,436,1000,0,1000,500,282,1000,1000,4,0,756,1,299,657,970,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0CF53273CA18863D000008D10A9C

[AppSet15]
App=HUD\HUD Prefab
FParamValues=18,0,0.436,1,0,1,0.5,0.294,1,1,4,0,0.41,1,0.417,0.97,0.98,1
ParamValues=18,0,436,1000,0,1000,500,294,1000,1000,4,0,410,1,417,970,980,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0CF53273CA18863D000008D10A9C

[AppSet16]
App=HUD\HUD Prefab
FParamValues=28,0,0.436,1,0,1,0.5,0.294,1,1,4,0,0.189,1,0.171,0.001,0.271,1
ParamValues=28,0,436,1000,0,1000,500,294,1000,1000,4,0,189,1,171,1,271,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0DF53273CA18863D000009B90A9D

[AppSet17]
App=HUD\HUD Prefab
FParamValues=28,0,0.436,1,0,1,0.5,0.239,1,1,4,0,0.252,1,0.292,0.493,0.576,1
ParamValues=28,0,436,1000,0,1000,500,239,1000,1000,4,0,252,1,292,493,576,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0DF53273CA18863D000009B90A9D

[AppSet18]
App=HUD\HUD Prefab
FParamValues=28,0,0.436,1,0,1,0.5,0.294,1,1,4,0,0.189,1,0.3496,0.572,0.653,1
ParamValues=28,0,436,1000,0,1000,500,294,1000,1000,4,0,189,1,349,572,653,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0DF53273CA18863D000009B90A9D

[AppSet19]
App=HUD\HUD Prefab
FParamValues=28,0,0.436,1,0,1,0.5,0.294,1,1,4,0,0.284,1,0.368,0.666,0.7,1
ParamValues=28,0,436,1000,0,1000,500,294,1000,1000,4,0,284,1,368,666,700,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0DF53273CA18863D000009B90A9D

[AppSet20]
App=HUD\HUD Prefab
FParamValues=28,0,0.436,1,0,1,0.5,0.3,1,1,4,0,0.158,1,0.368,0.717,0.789,1
ParamValues=28,0,436,1000,0,1000,500,300,1000,1000,4,0,158,1,368,717,789,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0DF53273CA18863D000009B90A9D

[AppSet21]
App=HUD\HUD Prefab
FParamValues=28,0,0.436,1,0,1,0.5,0.247,1,1,4,0,0.315,1,0.368,0.88,0.988,1
ParamValues=28,0,436,1000,0,1000,500,247,1000,1000,4,0,315,1,368,880,988,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0DF53273CA18863D000009B90A9D

[AppSet22]
App=HUD\HUD Prefab
FParamValues=28,0,0.436,1,0,1,0.5,0.294,1,1,4,0,0.63,1,0.368,0.997,1,1
ParamValues=28,0,436,1000,0,1000,500,294,1000,1000,4,0,630,1,368,997,1000,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0DF53273CA18863D000009B90A9D

[AppSet23]
App=Misc\Automator
FParamValues=1,8,13,3,1,0.007,0.25,1,9,13,3,1,0.002,0.25,1,10,13,3,1,0.004,0.25,1,11,13,3,1,0.003,0.25
ParamValues=1,8,13,3,1000,7,250,1,9,13,3,1000,2,250,1,10,13,3,1000,4,250,1,11,13,3,1000,3,250
Enabled=1

[AppSet24]
App=Misc\Automator
FParamValues=1,12,13,3,1,0.005,0.25,1,13,13,3,1,0.003,0.25,1,14,13,3,1,0.003,0.25,1,15,13,3,1,0.024,0.25
ParamValues=1,12,13,3,1000,5,250,1,13,13,3,1000,3,250,1,14,13,3,1000,3,250,1,15,13,3,1000,24,250
Enabled=1

[AppSet25]
App=Misc\Automator
FParamValues=1,16,13,3,1,0.013,0.25,1,17,13,3,1,0.006,0.25,1,18,13,3,1,0.008,0.25,1,19,13,3,1,0.006,0.25
ParamValues=1,16,13,3,1000,13,250,1,17,13,3,1000,6,250,1,18,13,3,1000,8,250,1,19,13,3,1000,6,250
Enabled=1

[AppSet26]
App=Misc\Automator
FParamValues=1,20,13,3,1,0.009,0.25,1,21,13,3,1,0.005,0.25,1,22,13,3,1,0.01,0.25,1,23,13,3,1,0.02,0.25
ParamValues=1,20,13,3,1000,9,250,1,21,13,3,1000,5,250,1,22,13,3,1000,10,250,1,23,13,3,1000,20,250
Enabled=1

[AppSet27]
App=HUD\HUD Prefab
FParamValues=20,0,0.436,1,0,1,0.5,0.47,1,1,4,0,0.158,1,0.27,0.001,0.087,1
ParamValues=20,0,436,1000,0,1000,500,470,1000,1000,4,0,158,1,270,1,87,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C8CF53273CA18863D00000A9F0A9E

[AppSet28]
App=HUD\HUD Prefab
FParamValues=20,0.702,0.436,1,0,1,0.5,0.844,1,1,4,0,0.221,1,0.233,0.311,0.334,1
ParamValues=20,702,436,1000,0,1000,500,844,1000,1000,4,0,221,1,233,311,334,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C8CF53273CA18863D00000A9F0A9E

[AppSet29]
App=HUD\HUD Prefab
FParamValues=20,0.353,0.436,1,0,1,0.5,0.445,1,1,4,0,0.221,1,0.233,0.533,0.568,1
ParamValues=20,353,436,1000,0,1000,500,445,1000,1000,4,0,221,1,233,533,568,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C8CF53273CA18863D00000A9F0A9E

[AppSet30]
App=HUD\HUD Prefab
FParamValues=21,0.353,0.436,1,0,1,0.5,0.43,1,1,4,0,0.315,1,0.368,0.533,0.78,1
ParamValues=21,353,436,1000,0,1000,500,430,1000,1000,4,0,315,1,368,533,780,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C4CF43273CA18863D00000B860A9F

[AppSet31]
App=HUD\HUD Prefab
FParamValues=22,0.353,0.436,1,0,1,0.5,0.518,1,1,4,0,0.252,1,0.368,0.533,0.78,1
ParamValues=22,353,436,1000,0,1000,500,518,1000,1000,4,0,252,1,368,533,780,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C4CF53273CA18863D00000C6D0AA0

[AppSet32]
App=HUD\HUD Prefab
FParamValues=23,0.353,0.436,1,0,1,0.5,0.585,1,1,4,0,0.189,1,0.368,0.533,0.78,1
ParamValues=23,353,436,1000,0,1000,500,585,1000,1000,4,0,189,1,368,533,780,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0CCCF43273CA18863D00000D540AA1

[AppSet33]
App=HUD\HUD Prefab
FParamValues=24,0.353,0.436,1,0,1,0.5,0.632,1,1,4,0,0.126,1,0.212,0.533,0.78,1
ParamValues=24,353,436,1000,0,1000,500,632,1000,1000,4,0,126,1,212,533,780,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0CCCF53273CA18863D00000E3B0AA2

[AppSet34]
App=Misc\Automator
FParamValues=1,28,13,3,1,0.005,0.25,1,29,13,3,1,0.007,0.25,1,30,13,3,1,0.007,0.25,1,31,13,3,1,0.01,0.25
ParamValues=1,28,13,3,1000,5,250,1,29,13,3,1000,7,250,1,30,13,3,1000,7,250,1,31,13,3,1000,10,250
Enabled=1

[AppSet45]
App=HUD\HUD Prefab
FParamValues=227,0.5734,0.5,0.364,0,0.223,0.419,0.41,0.464,1,4,0,0.5,1,0.3952,0,1,1
ParamValues=227,573,500,364,0,223,419,410,464,1000,4,0,500,1,395,0,1000,1
Enabled=1
LayerPrivateData=78012B28CA4F2F4A2D2E56484A2C2A8E0112BA0606C67A9939650C23030000A3850908

[AppSet46]
App=HUD\HUD Prefab
FParamValues=227,0.5549,0.5,0.364,0,0.223,0.582,0.41,0.464,1,4,0,0.5,1,0.3952,0,1,1
ParamValues=227,554,500,364,0,223,582,410,464,1000,4,0,500,1,395,0,1000,1
Enabled=1
LayerPrivateData=78012B28CA4F2F4A2D2E56484A2C2A8E0112BA0606C67A9939650C23030000A3850908

[AppSet47]
App=HUD\HUD Prefab
FParamValues=229,0.6437,0.5,1,0,0.091,0.504,1,1,1,4,0,0.5,1,0.8683,0,1,1
ParamValues=229,643,500,1000,0,91,504,1000,1000,1000,4,0,500,1,868,0,1000,1
Enabled=1
LayerPrivateData=78012B28CA4F2F4A2D2E56484A2C2A8E0112BA0606A67A9939650C23030000A55D090A

[AppSet44]
App=HUD\HUD Prefab
FParamValues=228,0.5356,0.5,0.364,0,0.223,0.5,0.19,1,1,4,0,0.5,1,0.3952,0,1,1
ParamValues=228,535,500,364,0,223,500,190,1000,1000,4,0,500,1,395,0,1000,1
Enabled=1
LayerPrivateData=78012B28CA4F2F4A2D2E56484A2C2A8E0112BA0606267A9939650C23030000A4710909

[AppSet35]
App=Postprocess\ParameterShake
FParamValues=0.8,0,45,1,0.488,0.292,45,14,1,1,0,0,3,3,0,0,0,0,0,0,0.98,0,1,1,0,0
ParamValues=800,0,45,1,488,292,45,14,1,1,0,0,3,3,0,0,0,0,0,0,980,0,1,1,0,0
ParamValuesMisc\Automator=0,317,394,429,1000,8,250,0,327,394,429,1000,6,250,0,337,394,429,1000,4,250,0,347,394,429,1000,0,250,0,0,0,0
Enabled=1

[AppSet49]
App=Postprocess\ParameterShake
FParamValues=0.816,0,46,1,0.488,0.292,46,14,1,1,0,0,3,3,0,0,0,0,0,0,1,0.08,1,1,0,0
ParamValues=816,0,46,1,488,292,46,14,1,1,0,0,3,3,0,0,0,0,0,0,1000,80,1,1,0,0
Enabled=1

[AppSet50]
App=Postprocess\ParameterShake
FParamValues=0.732,0,47,1,1,0.048,47,14,1,0,0,0,3,3,0,0,0,0,0,0,0,0,1,1,0,0
ParamValues=732,0,47,1,1000,48,47,14,1,0,0,0,3,3,0,0,0,0,0,0,0,0,1,1,0,0
Enabled=1

[AppSet51]
App=Postprocess\ParameterShake
FParamValues=0.832,0,44,1,0.488,0.292,44,14,1,1,0,0,3,3,0,0,0,0,0,0,0,0.092,1,1,0,0
ParamValues=832,0,44,1,488,292,44,14,1,1,0,0,3,3,0,0,0,0,0,0,0,92,1,1,0,0
Enabled=1

[AppSet48]
App=Misc\Automator
FParamValues=1,32,13,3,1,0.008,0.25,1,33,13,3,1,0.006,0.25,1,34,13,3,1,0.004,0.25,0,35,13,3,1,0,0.25
ParamValues=1,32,13,3,1000,8,250,1,33,13,3,1000,6,250,1,34,13,3,1000,4,250,0,35,13,3,1000,0,250
Enabled=1

[AppSet6]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.68,0,0.15,0.191,1,0,0,0
ParamValues=680,0,150,191,1000,0,0,0
Enabled=1
UseBufferOutput=1

[AppSet36]
App=Background\SolidColor
FParamValues=0,0.64,1,0.988
ParamValues=0,640,1000,988
Enabled=1

[AppSet57]
App=HUD\HUD 3D
FParamValues=0,0,0.295,0.373,0.62,0.5,0.5,0.5,0.472,0.499,0.5,0.413,0.311,0.16,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=0,0,295,373,620,500,500,500,472,499,500,413,311,160,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
ImageIndex=4

[AppSet40]
App=HUD\HUD 3D
FParamValues=0,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5043,0.499,0.5,0.541,0.571,0.5,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=0,0,500,500,500,500,500,500,504,499,500,541,571,500,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
ImageIndex=2

[AppSet55]
App=HUD\HUD 3D
FParamValues=0,0,0.332,0.628,0.62,0.5,0.5,0.5,0.472,0.499,0.5,0.769,0.311,0.16,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=0,0,332,628,620,500,500,500,472,499,500,769,311,160,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
ImageIndex=3

[AppSet41]
App=Misc\Automator
FParamValues=1,41,9,2,0.49,0.059,0.491,0,0,0,0,0,0.131,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,41,9,2,490,59,491,0,0,0,0,0,131,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1

[AppSet39]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.588,0.5,0.5,0.5,0.5
ParamValues=500,588,500,500,500,500
Enabled=1

[AppSet58]
App=Postprocess\ParameterShake
FParamValues=0.368,0.472,8,14,0.364,0.52,11,14,4,4,0,0,2,2,1,1,0,0,0,0,0,0,0,0,0,0
ParamValues=368,472,8,14,364,520,11,14,4,4,0,0,2,2,1,1,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet61]
App=Postprocess\ParameterShake
FParamValues=0.368,0.472,14,14,0.364,0.52,18,14,4,4,0,0,2,2,1,1,0,0,0,0,0,0,0,0,0,0
ParamValues=368,472,14,14,364,520,18,14,4,4,0,0,2,2,1,1,0,0,0,0,0,0,0,0,0,0
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""5""><position y=""10""><p align=""left""><font face=""American-Captain"" size=""9"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""12""><position y=""20""><p align=""left""><font face=""Chosence-Bold"" size=""4"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""5""><position y=""66""><p align=""left""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[extra1]</font></p></position>","<position x=""5""><position y=""80""><p align=""left""><font face=""Chosence-Bold"" size=""4"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""5""><position y=""86""><p align=""left""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""5""><position y=""90""><p align=""left""><font face=""Chosence-regular"" size=""2"" color=""#FFFFF"">[extra3]</font></p></position>"
Images="[presetpath]Wizard\Assets\Sacco\Panel 01.svg","[presetpath]Wizard\Assets\Sacco\Panel 01b black.svg"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

