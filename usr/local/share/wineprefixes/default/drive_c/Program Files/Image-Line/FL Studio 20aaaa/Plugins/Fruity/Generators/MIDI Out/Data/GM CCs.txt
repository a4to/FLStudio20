Bank select MSB
Modulation wheel
Breath controller

Foot controller
Portamento time
Data entry MSB
Main volume
Balance

Pan
Expression controller
Control 1
Control 2


General purpose controller 1
General purpose controller 2
General purpose controller 3
General purpose controller 4












Bank select LSB
Modulation wheel LSB
Breath controller LSB

Foot controller LSB
Portamento time LSB
Data entry LSB 
Main volume LSB
Balance LSB

Pan LSB
Expression controller LSB
Control 1 LSB
Control 2 LSB


General purpose controller 1 LSB
General purpose controller 2 LSB
General purpose controller 3 LSB
General purpose controller 4 LSB












Damper pedal (sustain)
Portamento
Sostenuto
Soft pedal
Legato footswitch
Hold 2
Sound variation
Harmonic content
Release time
Attack time
Brightness
Sound controller 6
Sound controller 7
Sound controller 8
Sound controller 9
Sound controller 10
General purpose controller 5
General purpose controller 6
General purpose controller 7
General purpose controller 8
Portamento control






Reverb depth
Tremolo depth
Chorus depth
Celeste depth
Phaser depth
Data increment
Data decrement
NRPN LSB
NRPN MSB
RPN LSB
RPN MSB


















All sound off
Reset all controllers
Local control on/off
All notes off
Omni mode off
Omni mode on
Mono mode on
Poly mode on