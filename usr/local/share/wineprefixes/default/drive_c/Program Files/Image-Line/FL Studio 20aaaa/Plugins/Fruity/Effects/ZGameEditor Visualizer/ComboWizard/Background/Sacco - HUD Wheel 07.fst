FLhd   0 * ` FLdt5  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ի9�  ﻿[General]
GlWindowMode=1
LayerCount=24
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=28,31,30,29,26,9,27,0,8,1,2,3,4,5,6,7,32,10,11,33,12,34,35,13
WizardParams=1715,1717

[AppSet28]
App=Canvas effects\TaffyPulls
FParamValues=0.772,0.26,1,0,0,0,0,0,0.5,0.5,0,0,0,0.5
ParamValues=772,260,1000,0,0,0,0,0,500,500,0,0,0,500
Enabled=1

[AppSet31]
App=Canvas effects\TaffyPulls
FParamValues=0.748,0.26,1,0,0,1,0,0,0.5,0.5,0,0,0,0.5
ParamValues=748,260,1000,0,0,1000,0,0,500,500,0,0,0,500
Enabled=1

[AppSet30]
App=Canvas effects\TaffyPulls
FParamValues=0.624,0.512,1,0,0,0.748,0,0,0.5,0.5,0,0,0,0.5
ParamValues=624,512,1000,0,0,748,0,0,500,500,0,0,0,500
Enabled=1

[AppSet29]
App=Canvas effects\TaffyPulls
FParamValues=0.624,0.512,1,0,0,0.248,0,0,0.5,0.5,0,0,0,0.5
ParamValues=624,512,1000,0,0,248,0,0,500,500,0,0,0,500
Enabled=1

[AppSet26]
App=Canvas effects\TaffyPulls
FParamValues=0.468,0,1,0,0,0.5,0,0,0.5,0.5,0,0,0,0.5
ParamValues=468,0,1000,0,0,500,0,0,500,500,0,0,0,500
Enabled=1
UseBufferOutput=1

[AppSet9]
App=Background\FourCornerGradient
FParamValues=0,1,0.844,0.444,1,0.782,0.444,1,0.5,1,1,0.514,1,1
ParamValues=0,1000,844,444,1000,782,444,1000,500,1000,1000,514,1000,1000
Enabled=1

[AppSet27]
App=HUD\HUD 3D
FParamValues=0,0,0.5,0.721,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=0,0,500,721,500,500,500,500,500,500,500,500,500,1000,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
ImageIndex=6

[AppSet0]
App=HUD\HUD Prefab
FParamValues=84,0.92,0.5,0,1,0.5,0.5,0.277,1,1,4,0,0.5,1,0.368,0,1,0
ParamValues=84,920,500,0,1000,500,500,277,1000,1000,4,0,500,1,368,0,1000,0
Enabled=1
LayerPrivateData=78014B2FCA4C89490712BA0686467A9939650C230B0000B60D05E2

[AppSet8]
App=HUD\HUD Prefab
FParamValues=32,0,0.5,0,1,0.5,1,0.172,1,1,4,0,0.884,1,0.4458,0,1,1
ParamValues=32,0,500,0,1000,500,1000,172,1000,1000,4,0,884,1,445,0,1000,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C4DF53273CA18863D00000D550AA1

[AppSet1]
App=HUD\HUD Prefab
FParamValues=31,0,0.5,0.192,0,0.5,1,0.192,1,1,4,0,0.082,1,0.356,0,1,1
ParamValues=31,0,500,192,0,500,1000,192,1000,1000,4,0,82,1,356,0,1000,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C4DF43273CA18863D00000C6E0AA0

[AppSet2]
App=HUD\HUD Prefab
FParamValues=30,0,0.5,0,1,0.5,1,0.222,1,1,4,0,0.541,1,0.3,0,1,1
ParamValues=30,0,500,0,1000,500,1000,222,1000,1000,4,0,541,1,300,0,1000,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C8DF53273CA18863D00000B870A9F

[AppSet3]
App=HUD\HUD Prefab
FParamValues=29,0.668,0.5,0,1,0.5,1,0.323,1,1,4,0,0.163,1,0.288,0,1,1
ParamValues=29,668,500,0,1000,500,1000,323,1000,1000,4,0,163,1,288,0,1000,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C8DF43273CA18863D00000AA00A9E

[AppSet4]
App=HUD\HUD Prefab
FParamValues=28,0,0.5,0,1,0.5,1,0.357,1,1,4,0,0.262,1,0.268,0,1,1
ParamValues=28,0,500,0,1000,500,1000,357,1000,1000,4,0,262,1,268,0,1000,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0DF53273CA18863D000009B90A9D

[AppSet5]
App=HUD\HUD Prefab
FParamValues=27,0,0.512,0.236,0,0.5,1,0.469,1,1,4,0,0.721,1,0.5897,0.2,1,1
ParamValues=27,0,512,236,0,500,1000,469,1000,1000,4,0,721,1,589,200,1000,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0DF43273CA18863D000008D20A9C

[AppSet6]
App=HUD\HUD Prefab
FParamValues=26,0,0.5,0.456,0,0.5,1,0.551,1,1,4,0,0.541,1,0.284,0.56,1,1
ParamValues=26,0,500,456,0,500,1000,551,1000,1000,4,0,541,1,284,560,1000,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C2CF53273CA18863D000010090AA4

[AppSet7]
App=HUD\HUD Prefab
FParamValues=25,0.652,0.5,0,1,0.5,1,0.622,1,1,4,0,0.361,1,0.244,0.352,1,1
ParamValues=25,652,500,0,1000,500,1000,622,1000,1000,4,0,361,1,244,352,1000,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C2CF43273CA18863D00000F220AA3

[AppSet32]
App=HUD\HUD Prefab
FParamValues=18,0,0.5,0,1,0.5,1,0.071,1,1,4,0,0.262,1,0.46,0,1,1
ParamValues=18,0,500,0,1000,500,1000,71,1000,1000,4,0,262,1,460,0,1000,1
Enabled=1
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0CF53273CA18863D000008D10A9C

[AppSet10]
App=Misc\Automator
FParamValues=1,9,13,3,1,0.016,0.25,1,2,13,3,1,0.006,0.25,1,3,13,3,1,0.003,0.25,1,4,13,3,1,0.012,0.25
ParamValues=1,9,13,3,1000,16,250,1,2,13,3,1000,6,250,1,3,13,3,1000,3,250,1,4,13,3,1000,12,250
Enabled=1

[AppSet11]
App=Misc\Automator
FParamValues=1,5,13,3,1,0.007,0.25,1,6,13,3,1,0.004,0.25,1,7,13,3,1,0.003,0.25,1,8,13,3,1,0.002,0.25
ParamValues=1,5,13,3,1000,7,250,1,6,13,3,1000,4,250,1,7,13,3,1000,3,250,1,8,13,3,1000,2,250
Enabled=1

[AppSet33]
App=Misc\Automator
FParamValues=1,33,13,3,1,0.007,0.25,0,6,13,3,1,0.004,0.25,0,7,13,3,1,0.003,0.25,0,8,13,3,1,0.002,0.25
ParamValues=1,33,13,3,1000,7,250,0,6,13,3,1000,4,250,0,7,13,3,1000,3,250,0,8,13,3,1000,2,250
Enabled=1

[AppSet12]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.384,0.088,0.25,0.006,1,0,0,0
ParamValues=384,88,250,6,1000,0,0,0
Enabled=1

[AppSet34]
App=Postprocess\Vignette
FParamValues=0,0,0,0.6,0.836,0.2
ParamValues=0,0,0,600,836,200
Enabled=1

[AppSet35]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.588,0.5,0.5,0.5,0.528
ParamValues=500,588,500,500,500,528
Enabled=1

[AppSet13]
App=Postprocess\ParameterShake
FParamValues=0.603,0.162,5,14,0.599,0.076,8,14,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=603,162,5,14,599,76,8,14,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""3""><position y=""3""><p align=""left""><font face=""American-Captain"" size=""10"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""3""><position y=""26""><p align=""left""><font face=""Chosence-Bold"" size=""8"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""0""><position y=""10""><p align=""right""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[extra1]</font></p></position>","<position x=""0""><position y=""3""><p align=""right""><font face=""Chosence-Bold"" size=""4"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""0""><position y=""20""><p align=""right""><font face=""Chosence-Bold"" size=""4 "" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""0""><position y=""25""><p align=""right""><font face=""Chosence-regular"" size=""3"" color=""#FFFFF"">[extra3]</font></p></position>"
Images="[presetpath]Wizard\Assets\Sacco\Panel 01 stroke.svg","[presetpath]Wizard\Assets\Sacco\Panel 01.svg","[presetpath]Wizard\Assets\Sacco\Panel 01b black.svg","[presetpath]Wizard\Assets\Sacco\Panel 01b stroke.svg","[presetpath]Wizard\Assets\Sacco\Panel 01b.svg","[presetpath]Wizard\Assets\Sacco\Panel 02.svg"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

