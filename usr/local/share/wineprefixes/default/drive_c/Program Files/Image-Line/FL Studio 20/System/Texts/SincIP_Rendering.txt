﻿While this resampling interpolation provides the best quality, it's also |SLOW AS HELL|. Be patient, even if the progress bar seems to be stuck.

You might want to use it only for your very last rendering. The 6-point hermite interpolation is far faster & is nearly as good in most cases.
