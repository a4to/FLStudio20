<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" FileVersion="2">
  <OnLoaded>
    <ZExternalLibrary ModuleName="ZGameEditor Visualizer" Source="void ZgeVizSetEnablePostProcess(xptr Handle,int Layer, int Value) { }"/>
    <SpawnModel Model="Canvas" SpawnStyle="1"/>
    <ZLibrary>
      <Source>
<![CDATA[inline float MakeBipolar(float x, float p)
{
  float out = x * 2.0 - 1.0;

  if (out < 0.0)
  {
     out = pow(out * -1.0, p) * -1.0;
  }
  else
  {
     out = pow(out, p);
  }

	return out;
}]]>
      </Source>
    </ZLibrary>
  </OnLoaded>
  <Content>
    <Group>
      <Children>
        <Array Name="Parameters" SizeDim1="8" Persistent="255">
          <Values>
<![CDATA[789C636060B06780600606041BC66700001703013C]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Origin X
Origin Y
Preview Orgin @checkbox
Angle
Size
Rotate
Image Source @checkbox
Fill Type @list1000: "Normal", "Mirror", "Tile", "Clamp" ]]>
          </StringValue>
        </Constant>
        <Constant Name="AuthorInfo" Type="2" StringValue="Youlean"/>
        <Variable Name="FLPluginHandle" Type="9"/>
        <Variable Name="LayerNr" Type="1"/>
      </Children>
    </Group>
    <Material Name="CanvasMaterial1" Light="0" Blend="1" ZBuffer="0" Shader="CanvasShader">
      <Textures>
        <MaterialTexture Name="FeedbackMaterialTexture" TexCoords="1"/>
      </Textures>
    </Material>
    <Material Name="CanvasMaterial2" Light="0" Blend="1" ZBuffer="0" Shader="CanvasShader">
      <Textures>
        <MaterialTexture Name="BitmapTexture" Texture="Bitmap1" TexCoords="1"/>
      </Textures>
    </Material>
    <Shader Name="CanvasShader">
      <VertexShaderSource>
<![CDATA[void main()
{
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;

  gl_Position = vertex;
  gl_TexCoord[0] = gl_MultiTexCoord0;
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[uniform sampler2D tex1;

uniform float resX;
uniform float resY;
uniform float viewportX;
uniform float viewportY;

uniform float rotate, scale, mirror, rotateOriginX, rotateOriginY, previewRotateOrigin;

#define PI 3.141592

const float pointSize = 0.015;

vec2 RotatePoint(float cx,float cy,float angle,vec2 p)
{
	float s = sin(angle);
	float c = cos(angle);

	// translate point back to origin:
	p.x -= cx;
	p.y -= cy;

	// rotate point
	float xnew = p.x * c - p.y * s;
	float ynew = p.x * s + p.y * c;

	// translate point back:
	p.x = xnew + cx;
	p.y = ynew + cy;
	return p;
}

vec2 GetCoordinate()
{
  vec2 ro = vec2(rotateOriginX, rotateOriginY);
	ro = ro * scale + ((1.0 - scale) / 2.0);
	vec2 iResolution = vec2(resX,resY);
	vec2 uv = RotatePoint(resX * ro.x, resY * ro.y, rotate * PI, gl_FragCoord.xy-vec2(viewportX,viewportY)) / iResolution.xy;

	// Set scale
	if (scale < 0.000001)
	{
		uv = vec2(0.0, 0.0);
	}
	else
	{
		float scaleInverted = 1.0 / scale;
		uv.x = uv.x * scaleInverted + (1.0 - scaleInverted) * 0.5;
		uv.y = uv.y * scaleInverted + (1.0 - scaleInverted) * 0.5;
	}

  return uv;
}

void main(void)
{
  if (scale < 0.000001)
	discard;

  vec2 uv = GetCoordinate();
  vec4 color;

  color = texture2D(tex1, uv);

	// Draw rotate origin preview
	if (previewRotateOrigin > 0.5)
	{
		float scaleInverted = 999999.0;
		if (scale > 0.000001) scaleInverted = 1.0 / scale;

		// Fix ascpect ratio
		float XPointSize = pointSize * (resY / resX) * scaleInverted;
		float YPointSize = pointSize * scaleInverted;

		float uvxr = uv.x - (rotateOriginX - XPointSize);
		float uvyr = uv.y - (rotateOriginY - YPointSize);

		if (uvxr >= 0.0 && uvxr < XPointSize * 2.0 && uvyr >= 0.0 && uvyr < YPointSize * 2.0)
		{
			color = vec4(1.0, 0.0, 0.0, 1.0);
		}
	}

	if (mirror < 0.001 && (uv.x < 0.0 || uv.y < 0.0 || uv.x > 1.0 || uv.y > 1.0))
       discard;

	gl_FragColor = color;
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="resX" VariableRef="ResX"/>
        <ShaderVariable VariableName="resY" VariableRef="ResY"/>
        <ShaderVariable VariableName="viewportX" Value="262" VariableRef="ViewportX"/>
        <ShaderVariable VariableName="viewportY" Value="262" VariableRef="ViewportY"/>
        <ShaderVariable VariableName="rotate" Value="0.5" VariableRef="Rotate"/>
        <ShaderVariable VariableName="rotateOriginX" ValuePropRef="Parameters[0]"/>
        <ShaderVariable VariableName="rotateOriginY" ValuePropRef="Parameters[1]"/>
        <ShaderVariable VariableName="previewRotateOrigin" ValuePropRef="Parameters[2]"/>
        <ShaderVariable VariableName="scale" Value="1" VariableRef="Scale"/>
        <ShaderVariable VariableName="mirror" ValuePropRef="Parameters[7]"/>
      </UniformVariables>
    </Shader>
    <Model Name="Canvas">
      <OnRender>
        <ZExpression>
          <Expression>
<![CDATA[float fillTypeParam = floor(Parameters[7] * 1000.0);

if (fillTypeParam == 1.0)
{
  FeedbackMaterialTexture.TextureWrapMode = 0;
  BitmapTexture.TextureWrapMode = 0;
}
else if (fillTypeParam == 2.0)
{
  FeedbackMaterialTexture.TextureWrapMode = 1;
  BitmapTexture.TextureWrapMode = 1;
}
else
{
  FeedbackMaterialTexture.TextureWrapMode = 2;
  BitmapTexture.TextureWrapMode = 2;
}

if (Parameters[4] > 0.501)
{
  float scaleParameter = MakeBipolar(Parameters[4], 2.0) * 0.995 + 0.0025;
  Scale = 1.0 + scaleParameter * 10.0;
}
else if (Parameters[4] < 0.499)
{
  float scaleParameter = MakeBipolar(Parameters[4], 1.0) * 0.995 + 0.0025;
  Scale = scaleParameter + 1.0;
}
else
{
  Scale = 1.0;
}

float angleParam = MakeBipolar(Parameters[3], 1.0);
float rotateParam = MakeBipolar(Parameters[5], 2.0) * 0.1;

if (rotateParam == 0.0)
{
  RotateAnimation = 0.0;
}
else
{
  RotateAnimation = RotateAnimation + rotateParam;
}

Rotate = RotateAnimation + angleParam;

float imageSourceParam = Parameters[6];

if (imageSourceParam < 0.5)
{
  ZgeVizSetEnablePostProcess(FLPluginHandle,LayerNr,1);
  UseMat.Material=CanvasMaterial1;
  ResX = App.ViewportWidth;
  ResY = App.ViewportHeight;
  ViewportX = App.ViewportX;
  ViewportY = App.ViewportY;
}
else
{
  ZgeVizSetEnablePostProcess(FLPluginHandle,LayerNr,0);
  UseMat.Material=CanvasMaterial2;
  float vw = App.ViewportWidth;
  float vh = App.ViewportHeight;
  float vx = App.ViewportX;
  float vy = App.ViewportY;
  float aspectRatio = (vh / vw);
  float bw = Bitmap1.Width / 6.0;
  float bh = Bitmap1.Height / 6.0;

  ResX = bw;
  ResY = bh;
  ViewportX = vx + (vw - bw) / 2.0;
  ViewportY = vy + (vh - bh) / 2.0;
}]]>
          </Expression>
        </ZExpression>
        <UseMaterial Name="UseMat" Material="CanvasMaterial1"/>
        <RenderSprite/>
      </OnRender>
    </Model>
    <Variable Name="Rotate"/>
    <Variable Name="RotateAnimation"/>
    <Variable Name="Scale"/>
    <Variable Name="ResX"/>
    <Variable Name="ResY"/>
    <Variable Name="ViewportX"/>
    <Variable Name="ViewportY"/>
    <Bitmap Name="Bitmap1" Width="256" Height="256">
      <Producers>
        <BitmapCells/>
        <BitmapBlur Radius="1"/>
      </Producers>
    </Bitmap>
  </Content>
</ZApplication>
