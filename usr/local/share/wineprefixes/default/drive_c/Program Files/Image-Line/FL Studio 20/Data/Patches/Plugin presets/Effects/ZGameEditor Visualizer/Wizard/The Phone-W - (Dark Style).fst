FLhd   0 * ` FLdt !  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��@r   ﻿[General]
GlWindowMode=1
LayerCount=21
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,7,1,5,6,4,2,8,3,9,16,11,10,12,13,14,17,21,18,15,20
WizardParams=995

[AppSet0]
App=Canvas effects\Stack Trace
FParamValues=0.54,0.508,1,0.416,0.218,0.224,0.6,0
ParamValues=540,508,1000,416,218,224,600,0
ParamValuesCanvas effects\Flaring=0,1000,1000,1000,1000,500,500,381,0,200,500
ParamValuesCanvas effects\Digital Brain=0,0,0,0,336,500,500,224,0,640,60,310,408,520,1000
ParamValuesCanvas effects\Electric=0,1000,112,956,540,500,500,152,100,1000,500
ParamValuesCanvas effects\Lava=0,148,924,872,780,500,500,520,676,752,500
ParamValuesCanvas effects\Flow Noise=0,880,952,788,0,500,500,184,100,1000,500
ParamValuesCanvas effects\OverlySatisfying=672,740,484,636,600,500,500,100,100,1000,500
Enabled=1
Collapsed=1
Name=Enemy FX 1

[AppSet7]
App=Feedback\WormHoleEclipse
FParamValues=0,0,0,1,0.12,1,0.5,0.5,0.5,0.5,0.5
ParamValues=0,0,0,1000,120,1000,500,500,500,500,500
ParamValuesFeedback\WarpBack=500,0,0,568,500,500,28
ParamValuesHUD\HUD Prefab=6,0,500,0,1000,500,500,734,1000,1000,444,0,500,1000,820,0,1000,1000
Enabled=1
Collapsed=1
ImageIndex=6
Name=Enemy FX 2

[AppSet1]
App=Postprocess\Edge Detect
FParamValues=0,0,0,0.664,0.384,1
ParamValues=0,0,0,664,384,1000
ParamValuesBackground\FourCornerGradient=400,1000,752,1000,1000,728,1000,1000,48,1000,1000,568,1000,1000
ParamValuesPostprocess\Ascii=0,136,176,600,700,200
Enabled=1
Collapsed=1
Name=Enemy FX 3

[AppSet5]
App=HUD\HUD Grid
AppVersion=1
FParamValues=0.352,0.5,0,1,0.5,0.5,1,1,0.844,4,0.5,1,0.5,0.82,1,1,0.648,0,0.888,0,1,1
ParamValues=352,500,0,1000,500,500,1000,1000,844,4,500,1000,500,820,1000,1000,648,0,888,0,1000,1
ParamValuesHUD\HUD Graph Radial=0,500,0,1000,500,500,202,444,500,0,1000,0,1000,0,250,728,200,0,0,0,500,1000
ParamValuesFeedback\BoxedIn=0,0,0,1000,1000,0,312,500,0,1000
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,900,708,336,28,784,500,500
ParamValuesHUD\HUD Callout Line=0,500,0,1000,500,500,700,400,0,200,0,200,100,450,100,1000
Enabled=1
Collapsed=1
Name=Enemy FX 4

[AppSet6]
App=HUD\HUD Prefab
FParamValues=27,0.504,0.5,0,1,0.5,0.5,0.248,1,1,4,0,0.5,1,0.884,0,1,1
ParamValues=27,504,500,0,1000,500,500,248,1000,1000,4,0,500,1,884,0,1000,1
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,1000,708,336,28,784,500,500
ParamValuesHUD\HUD Grid=0,500,0,1000,500,500,1000,1000,1000,444,500,1000,500,100,0,500,1000,516,1000,300,0,1000
Enabled=1
Collapsed=1
Name=Enemy FX 5
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0DF43273CA18863D000008D20A9C

[AppSet4]
App=Feedback\FeedMe
FParamValues=0,0,0,1,0.212,1,0
ParamValues=0,0,0,1000,212,1000,0
ParamValuesTerrain\CubesAndSpheres=0,0,0,1000,0,500,500,650,540,400,1000,1000,1000,1000,1000,1000,0,1000
ParamValuesPostprocess\Youlean Motion Blur=484,1000,732,764
ParamValuesFeedback\BoxedIn=0,0,0,552,1000,0,260,500,0,820
ParamValuesPostprocess\Youlean Color Correction=308,500,512,500,836,904
Enabled=1
Collapsed=1
Name=Enemy FX 6

[AppSet2]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.528,0.5,0.376,0.488,0.5
ParamValues=500,528,500,376,488,500
Enabled=1
Collapsed=1
Name=Enemy FX 7

[AppSet8]
App=Feedback\70sKaleido
FParamValues=0,0,0,1,0.752,0.56
ParamValues=0,0,0,1000,752,560
ParamValuesHUD\HUD Prefab=375,0,500,0,980,500,500,392,1000,1000,444,0,500,1000,920,0,940,1000
ParamValuesPostprocess\RGB Shift=560,0,0,600,700,200
ParamValuesPostprocess\Point Cloud High=0,730,594,500,568,448,444,503,226,549,156,0,0,0,0
ParamValuesPostprocess\ScanLines=0,200,0,0,0,0
ParamValuesPostprocess\Youlean Blur=500,1000,336
ParamValuesPostprocess\Youlean Motion Blur=656,1000,0,0
ParamValuesBackground\FourCornerGradient=9,688,680,1000,1000,250,1000,1000,500,1000,1000,750,1000,1000
ParamValuesFeedback\FeedMe=0,0,0,1000,212,1000,0
Enabled=1
Collapsed=1
Name=Enemy FX 8

[AppSet3]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.588,0,0.594,0.302,1,0,0,0
ParamValues=588,0,594,302,1000,0,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=Enemy Master

[AppSet9]
App=Background\SolidColor
FParamValues=0,0.78,0.052,0.948
ParamValues=0,780,52,948
ParamValuesHUD\HUD Prefab=71,0,500,0,1000,500,500,250,1000,1000,444,0,500,1000,548,0,1000,1000
ParamValuesPostprocess\Youlean Motion Blur=660,1000,464,212
ParamValuesPostprocess\Edge Detect=0,384,780,624,572,504
Enabled=1
Collapsed=1
Name=Background

[AppSet16]
App=HUD\HUD Prefab
FParamValues=1,0.156,0.5,0,0.768,0.5,0.5,0.318,1,1,4,0,0.5,0,0.524,0,1,1
ParamValues=1,156,500,0,768,500,500,318,1000,1000,4,0,500,0,524,0,1000,1
ParamValuesBackground\SolidColor=0,0,0,904
ParamValuesHUD\HUD Grid=572,500,128,280,500,500,1000,1000,1000,444,500,1000,500,0,0,500,1000,24,496,76,0,1000
Enabled=1
Name=Grid
LayerPrivateData=78014B4A4CCE4E2FCA2FCD4B89490232750D0C0CF53273CA18460A0000F04C0845

[AppSet11]
App=Image effects\Image
FParamValues=0,0,0,0.756,0.946,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,756,946,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=Phone Buttons

[AppSet10]
App=Image effects\Image
FParamValues=0,0,0,0,0.742,0.5,0.5,0.032,0,0.25,0,0,0,0
ParamValues=0,0,0,0,742,500,500,32,0,250,0,0,0,0
ParamValuesFeedback\BoxedIn=0,0,0,1000,1000,0,0,500,564,500
ParamValuesPostprocess\Youlean Color Correction=408,544,500,660,744,680
ParamValuesFeedback\70sKaleido=0,576,1000,456,504,700
ParamValuesPostprocess\Youlean Motion Blur=1000,1000,356,368
Enabled=1
Collapsed=1
ImageIndex=5
Name=LCD

[AppSet12]
App=Image effects\Image
FParamValues=0.952,0,0,0,0.944,0.5,0.5,0,0,0,0,0,0,0
ParamValues=952,0,0,0,944,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=2
Name=Phone Glass

[AppSet13]
App=Image effects\Image
FParamValues=0,0.524,0.18,0.512,0.944,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,524,180,512,944,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
Name=Phone Border

[AppSet14]
App=Image effects\Image
FParamValues=0,0.564,1,0,0.944,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,564,1000,0,944,500,500,0,0,0,1,0,0,0
Enabled=1
Collapsed=1
ImageIndex=3
Name=Phone Rim

[AppSet17]
App=Image effects\Image
FParamValues=0.096,0,0,0,0.792,0.5,0.42,0,0,0,0,0,0,0
ParamValues=96,0,0,0,792,500,420,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=4
Name=Phone FaceID

[AppSet21]
App=Text\TextTrueType
FParamValues=0,0,0,0,0,0.5,0.5,0,0,0,0.5
ParamValues=0,0,0,0,0,500,500,0,0,0,500
Enabled=1

[AppSet18]
App=Background\FourCornerGradient
FParamValues=7,0.432,0.508,1,1,0.754,1,1,0.136,1,1,0.75,1,1
ParamValues=7,432,508,1000,1000,754,1000,1000,136,1000,1000,750,1000,1000
ParamValuesImage effects\Image=208,0,0,0,792,500,420,0,0,0,0
Enabled=1
Collapsed=1
Name=Filter Color

[AppSet15]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fide in-out

[AppSet20]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""3"" y=""4""><p align=""left""><font face=""American-Captain"" size=""9"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""3"" y=""12""><p align=""left""><font face=""Chosence-Bold"" size=""6"" color=""#FFFFFF"">[title]</font></p></position>","<position y=""50.4""><p align=""center""><font face=""Chosence-Bold"" size=""2"" color=""#171717"">[comment]</font></p></position>"
Images="[presetpath]Wizard\ColoveContent\Others\COLOVE Phone\COLOVE-Phone-W-Border.svg","[presetpath]Wizard\ColoveContent\Others\COLOVE Phone\COLOVE-Phone-W-Button.svg","[presetpath]Wizard\ColoveContent\Others\COLOVE Phone\COLOVE-Phone-W-Gless.svg","[presetpath]Wizard\ColoveContent\Others\COLOVE Phone\COLOVE-Phone-W-Rim.svg",[presetpath]Wizard\ColoveContent\Devices\svg\iPhone-X-HD\iPhone-X-Face-id.svg
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

