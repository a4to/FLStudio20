<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="Cage" ClearColor="0 0 0 1" AmbientLightColor="0.7529 0.7529 0.7529 1" FrameRateStyle="1" ScreenMode="0" CameraPosition="0 0 30" LightPosition="200 200 200" FOV="70" ClipFar="1000" MouseVisible="255" FileVersion="2" AndroidPackageName="com.rado1.PhysicsDemo4">
  <Comment>
<![CDATA[Cage - Script Unique Parameters

Rotation speed - outer box rotation speed
Num. of boxes - number of inner boxes (0-150)
Gravity - amount o gravity applied to inner boxes]]>
  </Comment>
  <OnLoaded>
    <ZLibrary Comment="ZgeViz interface">
      <Source>
<![CDATA[// PARAMETERS

const string ParamHelpConst =
"Alpha\n" +
"Hue\n" +
"Saturation\n" +
"Lightness\n" +
"Size\n" +
"Position X\n" +
"Position Y\n"+
"Rotation speed\n" +
"Num. of boxes\n" +
"Gravity";

const int
  ALPHA = 0,
  HUE = 1,
  SATURATION = 2,
  LIGHTNESS = 3,
  SIZE = 4,
  POSITION_X = 5,
  POSITION_Y = 6,
  ROTATION_SPEED = 7,
  BOX_AMOUNT = 8,
  GRAVITY = 9;

// VARIABLES

float[32] SpecBandArray;
//float[0] AudioArray;
float SongPositionInBeats;
float[10] Parameters;
vec3 Color;

int LayerNr;
xptr FLPluginHandle;]]>
      </Source>
    </ZLibrary>
    <ZLibrary Comment="HSV convertion by Kjell">
      <Source>
<![CDATA[float angle(float X)
{
  if(X >= 0 && X < 360)return X;
  if(X > 360)return X-floor(X/360)* 360;
  if(X <   0)return X+floor(X/360)*-360;
}

void hsv(float H, float S, float V)
{
  float R,G,B,I,F,P,Q,T;
  
  H = angle(H);
  S = clamp(S,0,100);
  V = clamp(V,0,100);

  H /= 60;
  S /= 100;
  V /= 100;
  
  if(S == 0)
  {
    Color.R = V;
    Color.G = V;
    Color.B = V;
    return;
  }

  I = floor(H);
  F = H-I;

  P = V*(1-S);
  Q = V*(1-S*F);
  T = V*(1-S*(1-F));

  if(I == 0){R = V; G = T; B = P;}
  if(I == 1){R = Q; G = V; B = P;}
  if(I == 2){R = P; G = V; B = T;}
  if(I == 3){R = P; G = Q; B = V;}
  if(I == 4){R = T; G = P; B = V;}
  if(I == 5){R = V; G = P; B = Q;}
  
  Color.R = R;
  Color.G = G;
  Color.B = B;
}]]>
      </Source>
    </ZLibrary>
    <ZExternalLibrary Comment="Bullet 3D physics library" ModuleName="ZgeBullet" CallingConvention="1" BeforeInitExp="if(ANDROID) this.ModuleName = &quot;./libZgeBullet.so&quot;;" DefinitionsFile="zgebullet.txt"/>
    <ZLibrary Comment="Globals">
      <Source>
<![CDATA[// CONSTANTS

// boolean
const int FALSE = 0;
const int TRUE = 1;

// application
const float BAND_MIN = 0.1;
const int MAX_BOXES = 150;

// VARIABLES

xptr World;
xptr BoxShape;
xptr CageShape;

float BandMax;
int BandStep;
int LastSongPosition;
float LastGravity;
model [MAX_BOXES] Box;
int LastBoxAmount;
int BoxNumber;
float RotationPosition;]]>
      </Source>
    </ZLibrary>
    <ZExternalLibrary ModuleName="ZGameEditor Visualizer" Source="void ParamsNotifyChanged(xptr Handle,int Layer) { }"/>
    <ZExpression Comment="Init">
      <Expression>
<![CDATA[// init random seed
setRandomSeed(getSystemTime());

// set defaults
BoxNumber = 0;
BandMax = 0;
BandStep = SpecBandArray.SizeDim1 / 32;
RotationPosition = 0;
LastSongPosition = SongPositionInBeats;
LastGravity = 0.5;
LastBoxAmount = 0;
Parameters[ALPHA] = 0.6;
Parameters[HUE] = 0.2;
Parameters[SATURATION] = 0.3;
Parameters[SIZE] = 0.8;
Parameters[POSITION_X] = 0.5;
Parameters[POSITION_Y] = 0.5;
Parameters[ROTATION_SPEED] = 0.5;
Parameters[GRAVITY] = LastGravity;
Parameters[BOX_AMOUNT] = 0.5;
ParamsNotifyChanged(FLPluginHandle,LayerNr);

// init physical world
World = zbtCreateWorld();
zbtSetCurrentWorld(World);
zbtSetWorldGravity(0, LastGravity * -20, 0);

// create collision shapes
BoxShape = zbtCreateBoxShape(1, 1, 1);

xptr wall = zbtCreateBoxShape(2, 14, 14);
CageShape = zbtCreateCompoundShape();
zbtAddChildShape(CageShape, wall, 12, 0, 0, 0, 0, 0); //right
zbtAddChildShape(CageShape, wall, -12, 0, 0, 0, 0, 0); //left
zbtAddChildShape(CageShape, wall, 0, 12, 0, 0, 0, 0.25); //up
zbtAddChildShape(CageShape, wall, 0, -12, 0, 0, 0, 0.25); //down
zbtAddChildShape(CageShape, wall, 0, 0, 12, 0, 0.25, 0); //front
zbtAddChildShape(CageShape, wall, 0, 0, -12, 0, 0.25, 0); //back

// spawn cage
createModel(CageModel);]]>
      </Expression>
    </ZExpression>
  </OnLoaded>
  <OnUpdate>
    <ZExpression Comment="Update parameters, etc.">
      <Expression>
<![CDATA[// switch to the current world (used for simultaneous physics-based effects)
zbtSetCurrentWorld(World);

// compute band max
BandMax = 0;
for(int i=SpecBandArray.SizeDim1-1; i >= 0; i -= BandStep)
  if(BandMax < SpecBandArray[i]) BandMax = SpecBandArray[i];

// update number of boxes
int i = Parameters[BOX_AMOUNT] * MAX_BOXES;
LastBoxAmount = i;
if(BoxNumber > i){
  // delete remaining boxes
  for(int j = i; j < BoxNumber; j++)
    Box[j].Remove = TRUE;
  BoxNumber = i;
}

// spawn new box on a new beat
i = SongPositionInBeats;
if(/*BandMax > BAND_MIN &&*/ i != LastSongPosition){
  LastSongPosition = i;
  if(BoxNumber < LastBoxAmount){
    Box[BoxNumber] = createModel(BoxModel);
    BoxNumber++;
  }
}

// update camera position
App.CameraPosition.Z = 10 + (1-Parameters[SIZE]) * 100;
App.CameraPosition.X = (1-Parameters[POSITION_X]-0.5) * App.CameraPosition.Z;
App.CameraPosition.Y = (Parameters[POSITION_Y]-0.5) * (0-App.CameraPosition.Z);

// update gravity
float g = Parameters[GRAVITY];
if(g != LastGravity){
  LastGravity = g;
  zbtSetWorldGravity(0, g * -20, 0);
}

// physics simulation step
zbtStepSimulation(App.DeltaTime, 0, 0);]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <OnClose>
    <ZExpression Comment="Destroy physical world" Expression="zbtDestroyWorld(World);"/>
  </OnClose>
  <Content>
    <Group Comment="Artwork">
      <Children>
        <Group Comment="Meshes">
          <Children>
            <Mesh Name="BoxMesh">
              <Producers>
                <MeshBox XCount="1" YCount="1"/>
              </Producers>
            </Mesh>
          </Children>
        </Group>
        <Group Comment="Bitmaps and Materials">
          <Children>
            <Bitmap Name="EnvBitmap" Width="128" Height="128" Filter="2">
              <Producers>
                <BitmapExpression>
                  <Expression>
<![CDATA[Pixel=(abs(Y+.5)*sin(Y)*1.8)*(pow(X-.5,2)+pow(Y-.5,2))*2;
Pixel.A=.8;]]>
                  </Expression>
                </BitmapExpression>
              </Producers>
            </Bitmap>
            <Shader Name="EnvMapShader">
              <VertexShaderSource>
<![CDATA[varying vec4 col;

void main()
{ 
  gl_Position = ftransform();   
  
  gl_TexCoord[0] = gl_MultiTexCoord0;
  
  vec3 u = normalize( vec3(gl_ModelViewMatrix * gl_Vertex) );
  vec3 n = normalize( gl_NormalMatrix * gl_Normal );
  vec3 r = reflect( u, n );
  float m = 0.5 * sqrt( r.x*r.x + r.y*r.y + (r.z+1.0)*(r.z+1.0) );
  gl_TexCoord[1].s = r.x/m + 0.5;
  gl_TexCoord[1].t = r.y/m + 0.5;

  col = gl_Color;
}]]>
              </VertexShaderSource>
              <FragmentShaderSource>
<![CDATA[uniform sampler2D envMap;
varying vec4 col;

void main (void)
{
  vec4 env = texture2D( envMap, gl_TexCoord[1].st);
  gl_FragColor = col * env;
}]]>
              </FragmentShaderSource>
            </Shader>
            <Material Name="BoxMaterial" WireframeWidth="2" Shading="1" Light="0" Blend="2" ZBuffer="0" DrawBackFace="255" Shader="EnvMapShader">
              <Textures>
                <MaterialTexture Texture="EnvBitmap" Origin="0 0 0"/>
              </Textures>
            </Material>
          </Children>
        </Group>
      </Children>
    </Group>
    <Group Comment="Models">
      <Children>
        <Model Name="BoxModel" Category="1">
          <Definitions>
            <Variable Name="BoxBody" Type="9"/>
            <Variable Name="Remove" Type="1"/>
          </Definitions>
          <OnSpawn>
            <ZExpression Comment="Init">
              <Expression>
<![CDATA[// create rigid body
zbtSetCurrentWorld(World);
BoxBody = zbtCreateRigidBodyXYZ(0.1, BoxShape, 0, 0, 0, rnd(), rnd(), rnd());

// set physical properties
zbtSetAngularVelocity(BoxBody, rnd(), rnd(), rnd());
zbtSetLinearVelocity(BoxBody, random(0,10), random(0,10), random(0,10));
zbtSetDamping(BoxBody, 0.1, 0.5);
zbtSetActivationState(BoxBody, TRUE);
zbtSetSleepingThresholds(BoxBody, 0, 0);
zbtSetRestitution(BoxBody, 0.5);
zbtSetFriction(BoxBody, 0.2);

// visual
BoxColor.Color = rnd() + 0.1;

Remove = FALSE;]]>
              </Expression>
            </ZExpression>
          </OnSpawn>
          <OnUpdate>
            <ZExpression Comment="Update position and rotation">
              <Expression>
<![CDATA[zbtGetPosRot(BoxBody, CurrentModel.Position, CurrentModel.Rotation);

BoxColor.Color.A = (1-Parameters[ALPHA]) * 2;]]>
              </Expression>
            </ZExpression>
            <Condition Expression="return Remove;">
              <OnTrue>
                <RemoveModel/>
              </OnTrue>
            </Condition>
          </OnUpdate>
          <OnRender>
            <UseMaterial Material="BoxMaterial"/>
            <RenderSetColor Name="BoxColor" Color="1 1 1 1"/>
            <RenderMesh Mesh="BoxMesh"/>
          </OnRender>
          <OnRemove>
            <ZExpression Comment="Delete rigid body and shape">
              <Expression>
<![CDATA[zbtSetCurrentWorld(World);
zbtDeleteRigidBody(BoxBody);]]>
              </Expression>
            </ZExpression>
          </OnRemove>
        </Model>
        <Model Name="CageModel" Scale="10 10 10">
          <Definitions>
            <Variable Name="CageBody" Type="9"/>
          </Definitions>
          <OnSpawn>
            <ZExpression Comment="Init">
              <Expression>
<![CDATA[// create rigid body
zbtSetCurrentWorld(World);
CageBody = zbtCreateRigidBodyXYZ(0, CageShape, 0, 0, 0, 0, 0, 0);

// set physical properties
zbtSetCollisionFlags(CageBody, ZBT_CF_KINEMATIC_OBJECT);
zbtSetRestitution(CageBody, 0.9);
zbtSetFriction(CageBody, 0.1);]]>
              </Expression>
            </ZExpression>
          </OnSpawn>
          <OnUpdate>
            <ZExpression Comment="Update properties">
              <Expression>
<![CDATA[// update rotation
RotationPosition += BandMax * App.DeltaTime;
float s = BandMax * Parameters[ROTATION_SPEED] * App.DeltaTime * 8;
CurrentModel.Rotation.X += noise2(13, RotationPosition) * s;
CurrentModel.Rotation.Y += noise2(RotationPosition, 19) * s;
CurrentModel.Rotation.Z += noise2(7, RotationPosition) * s;
zbtSetRotation(CageBody, CurrentModel.Rotation);

// update color
hsv(Parameters[HUE]*360,Parameters[SATURATION]*100,(1-Parameters[LIGHTNESS])*100);
CageColor.Color = Color;
CageColor.Color.A = 1-Parameters[ALPHA];]]>
              </Expression>
            </ZExpression>
          </OnUpdate>
          <OnRender>
            <UseMaterial Material="BoxMaterial"/>
            <RenderSetColor Name="CageColor" Color="1 1 1 1"/>
            <RenderMesh Mesh="BoxMesh"/>
          </OnRender>
          <OnRemove>
            <ZExpression Comment="Delete rigid body and shape">
              <Expression>
<![CDATA[zbtSetCurrentWorld(World);
zbtDeleteRigidBody(CageBody);]]>
              </Expression>
            </ZExpression>
          </OnRemove>
        </Model>
      </Children>
    </Group>
    <Constant Name="AuthorInfo" Type="2" StringValue="Rado1"/>
  </Content>
</ZApplication>
