FLhd   0 * ` FLdt�  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ի*'  ﻿[General]
GlWindowMode=1
LayerCount=14
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=10,1,9,6,0,5,2,3,4,8,7,11,12,13
WizardParams=517,518,519,520,521,659

[AppSet10]
App=HUD\HUD Prefab
FParamValues=0,0,0.5,0,0,0.812,0.263,0.51,1,1,4,0,0.5,1,0.368,0.101,1,1
ParamValues=0,0,500,0,0,812,263,510,1000,1000,4,0,500,1,368,101,1000,1
Enabled=1
UseBufferOutput=1
Name=LOGO
LayerPrivateData=780173C8CBCF4BD52B2E4B671899000060F9036F

[AppSet1]
App=Background\SolidColor
FParamValues=0,0.668,0.752,0.872
ParamValues=0,668,752,872
Enabled=1
Name=Siri 1.0 BG

[AppSet9]
App=Postprocess\ScanLines
FParamValues=0,1,0.756,0.756,0
ParamValues=0,1,756,756,0
ParamValuesFeedback\WarpBack=500,0,0,0,500,500,88
ParamValuesHUD\HUD Prefab=1,944,500,0,0,500,500,286,1000,1000,444,0,500,0,368,0,1000,0
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,0,500,0,500,500,500,500,500
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,0,500,500,500,1000,500,500
ParamValuesHUD\HUD Grid=740,788,1000,604,500,500,1000,1000,1000,444,500,1000,500,100,0,500,1000,100,1000,0,0,1000
ParamValuesPostprocess\Youlean Blur=1000,1000,1000
ParamValuesPostprocess\FrameBlur=0,844,0,376,868,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesBackground\FourCornerGradient=0,100,840,1000,1000,610,1000,1000,528,1000,1000,750,1000,1000
Enabled=1
Collapsed=1
Name=Grid

[AppSet6]
App=Peak Effects\Linear
FParamValues=0.856,0.584,0.368,0.208,0.726,0.5,0.464,0.108,0,0.5,0,0.148,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0.472,1,0.152,0.2,1,0.072,0.016,0.654,0.47,0.244
ParamValues=856,584,368,208,726,500,464,108,0,500,0,148,500,1,0,1,0,500,500,500,500,472,1000,152,200,1000,72,16,654,470,244
Enabled=1
Collapsed=1
ImageIndex=1
Name=SIRI EQ 1

[AppSet0]
App=Peak Effects\Linear
FParamValues=0.244,0.584,0.716,0,0.669,0.5,0.464,0.564,0,0.5,0,0.08,0.501,1,0,1,0,0.5,0.5,0.5,0.5,0,1,0.024,0.136,0.544,0.048,0.432,0.71,0.25,0.596
ParamValues=244,584,716,0,669,500,464,564,0,500,0,80,501,1,0,1,0,500,500,500,500,0,1000,24,136,544,48,432,710,250,596
Enabled=1
Collapsed=1
ImageIndex=1
Name=SIRI EQ 2

[AppSet5]
App=Peak Effects\Linear
FParamValues=0.344,0.512,0.82,0,0.69,0.5,0.464,0.212,0,0.5,0,0.028,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0.66,1,0.152,0.244,0.848,0.332,0.412,0.434,0.25,0.1
ParamValues=344,512,820,0,690,500,464,212,0,500,0,28,500,1,0,1,0,500,500,500,500,660,1000,152,244,848,332,412,434,250,100
Enabled=1
Collapsed=1
ImageIndex=1
Name=SIRI EQ 3

[AppSet2]
App=Peak Effects\Linear
FParamValues=0.704,0.592,0.608,0,0.669,0.5,0.464,0.18,0.5,0.5,0,0.1,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,1,0.024,0.212,0.976,0.032,0.616,0.498,0.43,0.268
ParamValues=704,592,608,0,669,500,464,180,500,500,0,100,500,1,0,1,0,500,500,500,500,0,1000,24,212,976,32,616,498,430,268
Enabled=1
Collapsed=1
ImageIndex=1
Name=SIRI EQ 4

[AppSet3]
App=Peak Effects\Linear
FParamValues=0.556,0.752,0.54,0,0.73,0.5,0.464,0.204,0.5,0.5,0,0.063,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,1,0.152,0.2,0.504,0.016,0.412,0.434,0.25,0.1
ParamValues=556,752,540,0,730,500,464,204,500,500,0,63,500,1,0,1,0,500,500,500,500,0,1000,152,200,504,16,412,434,250,100
Enabled=1
Collapsed=1
ImageIndex=1
Name=SIRI EQ 5

[AppSet4]
App=Peak Effects\Linear
FParamValues=0.384,1,0,0,0.726,0.5,0.464,0.248,0.5,0.5,0,0.041,0.496,1,0,1,0,0.5,0.5,0.5,0.5,0,1,0.076,0.14,0.336,0.012,0.412,0.434,0.358,0.36
ParamValues=384,1000,0,0,726,500,464,248,500,500,0,41,496,1,0,1,0,500,500,500,500,0,1000,76,140,336,12,412,434,358,360
Enabled=1
Collapsed=1
ImageIndex=1
Name=SIRI EQ 6

[AppSet8]
App=Text\TextTrueType
FParamValues=0.056,0,0,0,0,0.5,0.5,0,0,0,0.5
ParamValues=56,0,0,0,0,500,500,0,0,0,500
ParamValuesPostprocess\Youlean Blur=76,0,0
ParamValuesPostprocess\Youlean Bloom=600,0,578,18
ParamValuesBackground\FourCornerGradient=467,628,0,492,1000,210,492,1000,528,492,1000,750,492,1000
ParamValuesPostprocess\Youlean Color Correction=500,500,500,500,500,424
ParamValuesBackground\SolidColor=0,644,688,808
Enabled=1
Collapsed=1
Name=Main Text

[AppSet7]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fade in-out

[AppSet11]
App=Image effects\Image
FParamValues=0,0,0,1,1,0.5,0.501,0,0,0,0,0,0,0
ParamValues=0,0,0,1000,1000,500,501,0,0,0,0,0,0,0
Enabled=1

[AppSet12]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1

[AppSet13]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""4"" y=""8""><p><font face=""American-Captain"" size=""5"" color=""#fff"">[author]</font></p></position>","<position x=""4"" y=""12""><p><font face=""Chosence-Bold"" size=""3"" color=""#f4f4f4"">[title]</font></p></position>","<position x=""4"" y=""19""><p> <font face=""Chosence-Bold"" size=""3"" color=""#fff"">[comment]</font></p></position>"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

