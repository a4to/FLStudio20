FLhd   0 * ` FLdt&  �20.6.9.1651 �s  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ՄK�%  ﻿[General]
GlWindowMode=1
LayerCount=27
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,5,3,1,2,4,8,9,6,12,13,15,14,16,20,10,24,25,26,7,17,27,18,19,22,23,28

[AppSet0]
App=Background\SolidColor
FParamValues=0,0.648,0.636,0.728
ParamValues=0,648,636,728
ParamValuesBackground\FourCornerGradient=0,1000,664,732,1000,898,736,1000,504,620,1000,150,812,1000
Enabled=1
Collapsed=1
Name=Background XSTry-Ray

[AppSet5]
App=Canvas effects\Flaring
FParamValues=0.524,1,1,0.092,0.952,0.5,0.5,0.653,0,2,0.112
ParamValues=524,1000,1000,92,952,500,500,653,0,2,112
Enabled=1
Collapsed=1
Name=Fatele Mod

[AppSet3]
App=Canvas effects\SkyOcean
FParamValues=0,0.96,1,0,0,0.5,0.5,0.708,0.34,0.736,1,1,0.576,0.68
ParamValues=0,960,1000,0,0,500,500,708,340,736,1000,1000,576,680
Enabled=1
Collapsed=1
ImageIndex=1
Name=React 1

[AppSet1]
App=Background\FogMachine
FParamValues=0.672,0.588,1,0.064,0.456,0.5,0.5,0.5,0.5,0.636,0,0.516
ParamValues=672,588,1000,64,456,500,500,500,500,636,0,516
ParamValuesBackground\Youlean Background MDL=1000,1000,1000,420,884,424,198,320,648,558,376,696,800,664,1000,0,500,4,0,500
Enabled=1
Collapsed=1
ImageIndex=1
Name=Liner Color

[AppSet2]
App=Canvas effects\SkyOcean
FParamValues=0,0.856,1,1,0,0.5,0.5,0,0.404,0.656,1,0.568,0.048,0.488
ParamValues=0,856,1000,1000,0,500,500,0,404,656,1000,568,48,488
ParamValuesBlend\VideoAlphaKey=0,636,1000,0,1000,656,344,0,0,0,474,92,340
Enabled=1
Collapsed=1
ImageIndex=1
Name=React 2

[AppSet4]
App=Canvas effects\SkyOcean
FParamValues=0,0.592,1,1,0.232,0.5,0.5,0.708,0.34,0.568,0.864,0.924,0,0
ParamValues=0,592,1000,1000,232,500,500,708,340,568,864,924,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=React 3

[AppSet8]
App=Peak Effects\Polar
FParamValues=0.916,0.024,0.836,0,0.524,0.5,0.5,1,0.5,0.348,0.708,0.804,0.48,0.5,0,0.608,0.216,1,1,1,0,0,1,1
ParamValues=916,24,836,0,524,500,500,1000,500,348,708,804,480,500,0,608,216,1000,1000,1,0,0,1,1
ParamValuesBackground\Youlean Background MDL=1000,72,1000,0,500,500,50,320,0,170,300,120,1000,1000,0,300,500,0,0,500
ParamValuesParticles\ReactiveFlow=0,125,500,250,636,0,500,100,500,500,500,500,500,0,0,500,200,500,125,500,500,500,1000,1000,100
ParamValuesPeak Effects\Fluidity=912,488,1000,0,0,264,8
ParamValuesPeak Effects\Linear=0,964,0,0,486,388,548,172,0,500,260,566,484,1000,0,0,0,500,500,500,500,828,500,0,20,192,0,700,206,398,660
ParamValuesPeak Effects\PeekMe=0,0,1000,1000,500,500,500,230,1000,0,0,0
ParamValuesPeak Effects\JoyDividers=0,0,0,1000,700,500,500,0,500,100,600,650,510
ParamValuesParticles\fLuids=0,0,0,0,884,492,476,0,0,0,392,628,500,780,708,766,500,608,468,728,500,500,168,208,500,0,0,250,248,0,0,0
ParamValuesCanvas effects\SkyOcean=0,592,1000,1000,232,500,500,708,340,568,864,924,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=Modliner X1 (EQ)

[AppSet9]
App=Canvas effects\Rain
FParamValues=0.896,1,1,0.648,0.216,0.5,0.5,0.5,0.5,0,0.736,1
ParamValues=896,1000,1000,648,216,500,500,500,500,0,736,1000
ParamValuesCanvas effects\OverlySatisfying=0,0,0,0,600,500,500,100,100,1000,500
Enabled=1
Collapsed=1
ImageIndex=1
Name=Modliner X2

[AppSet6]
App=Background\ItsFullOfStars
FParamValues=0,0,0.984,0,0.74,0.2,0.864,0.628,0.472,0,0
ParamValues=0,0,984,0,740,200,864,628,472,0,0
ParamValuesImage effects\Image=0,0,0,1000,884,500,500,0,0,0,0,0,1000,1000
Enabled=1
Collapsed=1
ImageIndex=1
Name=Start Riser 1

[AppSet12]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.924,0.184,0.304,0.608,0.472,0,0.268
ParamValues=0,0,0,0,924,184,304,608,472,0,268
Enabled=1
Collapsed=1
ImageIndex=1
Name=Start Riser 2

[AppSet13]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.932,0.496,0.312,0.496,0.472,0,0.72
ParamValues=0,0,0,0,932,496,312,496,472,0,720
Enabled=1
Collapsed=1
ImageIndex=1
Name=Start Riser 3

[AppSet15]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.932,0.344,0.312,0.496,0.472,0,0.72
ParamValues=0,0,0,0,932,344,312,496,472,0,720
Enabled=1
Collapsed=1
ImageIndex=1
Name=Start Riser 4

[AppSet14]
App=Background\ItsFullOfStars
FParamValues=0,0,0.968,0.244,0.492,0.5,0.5,0.5,0,0,0
ParamValues=0,0,968,244,492,500,500,500,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=Start Riser 5

[AppSet16]
App=Background\ItsFullOfStars
FParamValues=0,0,0,0,0.9,0.668,0.488,0.496,0.472,0,0
ParamValues=0,0,0,0,900,668,488,496,472,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=Start Riser 6

[AppSet20]
App=Feedback\FeedMeFract
FParamValues=0.404,0,0,1,0.332,0
ParamValues=404,0,0,1000,332,0
ParamValuesFeedback\BoxedIn=0,0,0,1000,1000,0,0,592,0,524
ParamValuesFeedback\70sKaleido=0,0,472,1000,248,88
ParamValuesPostprocess\Point Cloud High=0,638,446,500,500,448,516,503,94,857,148,0,0,0,333
ParamValuesFeedback\FeedMe=0,0,0,1000,500,296,956
Enabled=1
Collapsed=1
Name=Slow-Mo

[AppSet10]
App=Feedback\SphericalProjection
FParamValues=0.62,0,0,0,0.174,0.317,0.336,0.194,0.484,0.42,1,1,1,0.256,0.492,1
ParamValues=620,0,0,0,174,317,336,194,484,420,1,1,1,256,492,1000
ParamValuesFeedback\FeedMeFract=0,0,0,1000,0,676
ParamValuesFeedback\FeedMe=0,0,0,1000,0,700,404
Enabled=1
Collapsed=1
ImageIndex=1
Name=Hydra

[AppSet24]
App=Feedback\BoxedIn
FParamValues=0,0,0,1,0.268,0,0,0.656,0,0.712
ParamValues=0,0,0,1000,268,0,0,656,0,712
ParamValuesFeedback\SphericalProjection=60,0,0,256,1000,52,632,0,0,1000,0,333,530,1000,920,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesFeedback\70sKaleido=0,280,0,1000,96,500
Enabled=1
Collapsed=1
Name=Great Day

[AppSet25]
App=Feedback\SphericalProjection
FParamValues=0.06,0,0,0,0.782,0.425,0,0.746,0.12,0.524,0,1,1,0.116,0.968,0.76
ParamValues=60,0,0,0,782,425,0,746,120,524,0,1,1,116,968,760
ParamValuesFeedback\BoxedIn=0,0,0,1000,268,0,0,656,0,712
ParamValuesFeedback\FeedMeFract=0,0,1000,1000,368,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=3D

[AppSet26]
App=Postprocess\Youlean Audio Shake
FParamValues=0,0.854,0.04,0,0.236,0.072,0.124,0.2,0.5,0.5,0.5,0.44,0,0
ParamValues=0,854,40,0,236,72,124,200,500,500,500,440,0,0
ParamValuesFeedback\SphericalProjection=60,0,0,0,782,425,0,746,120,524,0,333,530,116,968,760,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
Name=SHAKER

[AppSet7]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=1,0,0.058,0.539,1,1,0,1
ParamValues=1000,0,58,539,1000,1,0,1
ParamValuesBlend\Youlean From Buffer=0,1000
Enabled=1
Collapsed=1
Name=Bloom Out End

[AppSet17]
App=Postprocess\Point Cloud High
FParamValues=0,0.17,0.49,0.592,0.5,0.448,0.62,0.503,0.116,0.661,0.24,1,0,0,0
ParamValues=0,170,490,592,500,448,620,503,116,661,240,1,0,0,0
ParamValuesPostprocess\Youlean Bloom=1000,0,546,455,1000,0,0,1000
Enabled=1
Collapsed=1
ImageIndex=1
Name=Effector Out

[AppSet27]
App=Postprocess\Youlean Kaleidoscope
FParamValues=0.5,0.5,0,0.856,0.828,0,0.5,0.5,0.5,0.324,0.112
ParamValues=500,500,0,856,828,0,500,500,500,324,112
ParamValuesPostprocess\Point Cloud High=0,170,490,592,500,448,620,503,116,661,240,1000,0,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=OUTKILLER

[AppSet18]
App=Background\SolidColor
FParamValues=0,0,0,1
ParamValues=0,0,0,1000
Enabled=1
Collapsed=1
Name=Controller Out

[AppSet19]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.494,0,0,0.5,0,0,0,0
ParamValues=0,0,0,0,1000,500,494,0,0,500,0,0,0,0
Enabled=1
Collapsed=1
Name=Output Top

[AppSet22]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.51,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,510,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
Name=Output Bottom

[AppSet23]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.22,0.088,0.194,0.455,0.9,1,0,1
ParamValues=220,88,194,455,900,1,0,1
Enabled=1
Collapsed=1
Name=Bloom The End

[AppSet28]
App=Background\FourCornerGradient
FParamValues=7,0.896,0.156,1,1,0,1,1,0.016,1,1,0.59,1,1
ParamValues=7,896,156,1000,1000,0,1000,1000,16,1000,1000,590,1000,1000
Enabled=1
Collapsed=1
Name=COLOR FILTER

[Video export]
VideoH=2160
VideoW=3840
VideoRenderFps=30
SampleRate=48000
VideoCodecName=
AudioCodecName=(default)
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=C:\Users\spyro\Desktop\LollieVox - Optimum Momentum_3.mp4
Bitrate=70000000
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""2"" y=""8""><p uppercase=""yes""><font face=""Montserrat-ExtraBold"" size=""7"" color=""#fff"">[author]</font></p></position>","<position x=""2"" y=""15""><p uppercase=""yes""><font face=""Montserrat-Medium"" size=""3"" color=""#fff"">[title]</font></p></position>","<position x=""2"" y=""19.3""><p><font face=""Chosence-bold"" size=""1.6"" color=""#EAEAEA"">version</font></p></position>","<position x=""2"" y=""21""><p><font face=""Chosence-bold"" size=""2.5"" color=""#fff"">[comment]</font></p></position>",,"<position y=""48""><p align=""center"" uppercase=""yes""><font face=""Antaris"" size=""5"" color=""#F9F9F9"">synthwave</font></p></position>","<position y=""52""><p align=""center"" uppercase=""yes""><font face=""Antaris"" size=""2"" color=""#E5E5E5"">flashback</font></p></position>",,"<position y=""91.7""><p align=""center""><font face=""Chosence-Bold"" size=""2"" color=""#fff"">COLOVE Products</font></p></position>","<position y=""94""><p align=""center""><font face=""Chosence-regular"" size=""1.4"" color=""#EAEAEA"">Exclusive Sounds</font></p></position>"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

