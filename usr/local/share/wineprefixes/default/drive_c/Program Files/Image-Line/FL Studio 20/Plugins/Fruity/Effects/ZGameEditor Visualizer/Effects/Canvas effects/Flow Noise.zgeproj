<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" FileVersion="2">
  <OnLoaded>
    <SpawnModel Model="ScreenModel"/>
    <ZLibrary Comment="HSV Library">
      <Source>
<![CDATA[vec3 hsv(float h, float s, float v)
{
  s = clamp(s/100, 0, 1);
  v = clamp(v/100, 0, 1);

  if(!s)return vector3(v, v, v);

  h = h < 0 ? frac(1-abs(frac(h/360)))*6 : frac(h/360)*6;

  float c, f, p, q, t;

  c = floor(h);
  f = h-c;

  p = v*(1-s);
  q = v*(1-s*f);
  t = v*(1-s*(1-f));

  switch(c)
  {
    case 0: return vector3(v, t, p);
    case 1: return vector3(q, v, p);
    case 2: return vector3(p, v, t);
    case 3: return vector3(p, q, v);
    case 4: return vector3(t, p, v);
    case 5: return vector3(v, p, q);
  }
}]]>
      </Source>
    </ZLibrary>
  </OnLoaded>
  <OnUpdate>
    <ZExpression>
      <Expression>
<![CDATA[const int
  ALPHA = 0,
  HUE = 1,
  SATURATION = 2,
  LIGHTNESS = 3;

vec3 c=hsv(Parameters[HUE]*360,Parameters[SATURATION]*100,(1-Parameters[LIGHTNESS])*100);

ShaderColor.r=c.r;
ShaderColor.g=c.g;
ShaderColor.b=c.b;
uSize=5.0-Parameters[4]*4.0;
uPos=vector2((1.0-Parameters[5])-.5,Parameters[6]-.5);
speed=Parameters[7]*10.0;
delta=app.DeltaTime*speed;
deltaTime+=delta;
uThreshold=Parameters[8]*3.00001;]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <Content>
    <Group>
      <Children>
        <Array Name="Parameters" SizeDim1="11" Persistent="255">
          <Values>
<![CDATA[78DA63608003FB378116F60C0C0DF62036089F3D73C6960109000080D305D3]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Alpha
Hue
Saturation
Lightness
Size
Position X
Position Y
Speed
Threshold]]>
          </StringValue>
        </Constant>
        <Constant Name="AuthorInfo" Type="2">
          <StringValue>
<![CDATA[nimitz 
Twitter: @stormoid
Adapted from https://www.shadertoy.com/view/MdlXRS]]>
          </StringValue>
        </Constant>
        <Variable Name="delta"/>
        <Variable Name="deltaTime"/>
        <Variable Name="Speed"/>
        <Variable Name="uPos" Type="6"/>
        <Variable Name="uSize"/>
        <Variable Name="uThreshold"/>
      </Children>
    </Group>
    <Material Name="ScreenMaterial" Shading="1" Light="0" Blend="1" ZBuffer="0" Shader="ScreenShader">
      <Textures>
        <MaterialTexture Texture="NoiseBitmap" TextureWrapMode="1" TexCoords="1"/>
      </Textures>
    </Material>
    <Shader Name="ScreenShader">
      <VertexShaderSource>
<![CDATA[varying vec2 position;

void main(){
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
  position=vec2(vertex.x,vertex.y);
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[//Flow by nimitz (twitter: @stormoid)
//Adapted from https://www.shadertoy.com/view/MdlXRS

uniform float iGlobalTime;
uniform float resX;
uniform float resY;
uniform float viewportX;
uniform float viewportY;
uniform vec2 pos;
uniform float size;
uniform float Alpha, threshold;

uniform sampler2D tex1;
#define iChannel0 tex1

vec2 iResolution = vec2(resX,resY);

//uniform float pSpeed1;
//uniform float pSpeed2;
//uniform float pOctScale;

uniform vec3 pColor;

//Noise animation - Flow
//by nimitz (stormoid.com) (twitter: @stormoid)


//Somewhat inspired by the concepts behind "flow noise"
//every octave of noise is modulated separately
//with displacement using a rotated vector field

//normalization is used to created "swirls"
//usually not a good idea, depending on the type of noise
//you are going for.

//Sinus ridged fbm is used for better effect.

#define time iGlobalTime*0.1
#define tau 6.2831853

mat2 makem2(in float theta){float c = cos(theta);float s = sin(theta);return mat2(c,-s,s,c);}
float noise( in vec2 x ){return texture2D(iChannel0, x*.01).x;}
mat2 m2 = mat2( 0.80,  0.60, -0.60,  0.80 );

float grid(vec2 p)
{
	float s = sin(p.x)*cos(p.y);
	return s;
}

float flow(in vec2 p)
{
	float z=2.;
	float rz = 0.;
	vec2 bp = p;
	for (float i= 1.;i < 7.;i++ )
	{
		bp += time*1.5;
		vec2 gr = vec2(grid(p*3.-time*2.),grid(p*3.+4.-time*2.))*0.4;
		gr = normalize(gr)*0.4;
		gr *= makem2((p.x+p.y)*.3+time*10.);
		p += gr*0.5;

		rz+= (sin(noise(p)*8.)*0.5+0.5) /z;

		p = mix(bp,p,.5);
		z *= 1.7;
		p *= 2.5;
		p*=m2;
		bp *= 2.5;
		bp*=m2;
	}
	return rz;
}

float spiral(vec2 p,float scl)
{
	float r = length(p);
	r = log(r);
	float a = atan(p.y, p.x);
	return abs(mod(scl*(r-2./scl*a),tau)-1.)*2.;
}

void main()
{
	vec2 p = (gl_FragCoord.xy-vec2(viewportX,viewportY)) / iResolution.xy-0.5;
  p=p*size+pos*size;
	p.x *= iResolution.x/iResolution.y;
	p*= 3.;
	float rz = flow(p);
	p /= exp(mod(time*3.,2.1));
	rz *= (6.-spiral(p,3.))*.9;
	vec3 col = pColor/rz;
	col=pow(abs(col),vec3(1.01));
  col=clamp(col,vec3(0.0),vec3(1.0));
	gl_FragColor = vec4(col,(col.r+col.g+col.b<threshold ? 0.0:1.0)*Alpha);
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="iGlobalTime" ValuePropRef="App.Time" VariableRef="deltaTime"/>
        <ShaderVariable VariableName="resX" Value="1163" ValuePropRef="App.ViewportWidth"/>
        <ShaderVariable VariableName="resY" Value="432" ValuePropRef="App.ViewportHeight"/>
        <ShaderVariable VariableName="viewportX" Value="262" ValuePropRef="App.ViewportX"/>
        <ShaderVariable VariableName="viewportY" Value="262" ValuePropRef="App.ViewportY"/>
        <ShaderVariable VariableName="Alpha" ValuePropRef="1-Parameters[0];"/>
        <ShaderVariable DesignDisable="1" VariableName="pSpeed1" ValuePropRef="Parameters[4];"/>
        <ShaderVariable DesignDisable="1" VariableName="pSpeed2" ValuePropRef="1.0 + Parameters[5]*3;"/>
        <ShaderVariable DesignDisable="1" VariableName="pOctScale" ValuePropRef="Parameters[6]*4;"/>
        <ShaderVariable VariableName="pColor" VariableRef="ShaderColor"/>
        <ShaderVariable VariableName="pos" VariableRef="uPos"/>
        <ShaderVariable VariableName="size" VariableRef="uSize"/>
        <ShaderVariable VariableName="threshold" VariableRef="uThreshold"/>
      </UniformVariables>
    </Shader>
    <Model Name="ScreenModel">
      <OnRender>
        <UseMaterial Material="ScreenMaterial"/>
        <RenderSprite/>
      </OnRender>
    </Model>
    <Bitmap Name="NoiseBitmap" Comment="NoCustom" Width="256" Height="256">
      <Producers>
        <BitmapExpression>
          <Expression>
<![CDATA[//X,Y : current coordinate (0..1)
//Pixel : current color (rgb)
//Sample expression: Pixel.R=abs(sin(X*16));
float s=1.0*rnd();
Pixel.r=s;
Pixel.g=s;
Pixel.b=s;]]>
          </Expression>
        </BitmapExpression>
      </Producers>
    </Bitmap>
    <Variable Name="ShaderColor" Type="7"/>
  </Content>
</ZApplication>
