FLhd   0 * ` FLdt�!  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��Bi!  ﻿[General]
GlWindowMode=1
LayerCount=16
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=6,16,8,9,0,20,14,15,1,10,11,18,3,2,12,13

[AppSet6]
App=Canvas effects\Lava
FParamValues=0,0.648,1,0.648,0,0.5,0.5,1,0.528,0.236,0.38
ParamValues=0,648,1000,648,0,500,500,1000,528,236,380
ParamValuesCanvas effects\Digital Brain=0,0,568,0,428,1000,508,4,892,28,0,264,340,292,1000,0
ParamValuesBackground\SolidColor=0,932,1000,0
Enabled=1
Name=EFX 1

[AppSet16]
App=Canvas effects\OverlySatisfying
FParamValues=0,0.74,0,0
ParamValues=0,740,0,0
Enabled=1
Name=EFX 2

[AppSet8]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.332,0.476,0.618,0.66,1,0,0,0
ParamValues=332,476,618,660,1000,0,0,0
ParamValuesBackground\FourCornerGradient=400,1000,700,1000,1000,726,1000,1000,8,972,1000,750,1000,984
ParamValuesPostprocess\Youlean Motion Blur=0,1000,420,776
ParamValuesPostprocess\Blur=644
Enabled=1
Name=Bloom

[AppSet9]
App=Feedback\WarpBack
FParamValues=0.248,0,0,0.052,0.468,0.5,0.2
ParamValues=248,0,0,52,468,500,200
ParamValuesHUD\HUD Graph Radial=0,500,0,712,500,500,82,444,500,0,1000,0,1000,64,250,500,200,312,12,0,500,1000
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,696,0,728,500,584,596,500,768
ParamValuesFeedback\70sKaleido=0,0,228,1000,860,324
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,1000,0,0,500,920,552,500
ParamValuesHUD\HUD Graph Linear=0,1000,1000,0,500,504,1000,584,810,444,500,1000,0,688,796,900,164,180,132,333,536,1000
ParamValuesPostprocess\Youlean Pixelate=392,333,0,0
ParamValuesFeedback\BoxedIn=0,0,0,0,1000,1000,748,500,0,0
ParamValuesFeedback\FeedMeFract=0,0,0,1000,564,0
ParamValuesPostprocess\Youlean Motion Blur=688,1000,408,672
ParamValuesFeedback\FeedMe=0,0,0,1000,500,680,312
Enabled=1
Name=OMG EFX

[AppSet0]
App=HUD\HUD Prefab
FParamValues=78,0,0.5,0,1,0.5,0.5,0.306,1,1,4,0,0.5,1,1,0,1,0
ParamValues=78,0,500,0,1000,500,500,306,1000,1000,4,0,500,1,1000,0,1000,0
ParamValuesBackground\FourCornerGradient=533,1000,0,1000,1000,434,1000,1000,500,1000,1000,750,1000,1000
ParamValuesBackground\SolidColor=0,932,1000,0
ParamValuesHUD\HUD Grid=0,500,0,1000,500,500,1000,1000,888,444,500,1000,500,428,0,500,1000,1000,648,0,0,1000
Enabled=1
Name=SWAP
LayerPrivateData=78014B2FCA4C89490712BA0606667A9939650C230B0000B8E805E5

[AppSet20]
App=Postprocess\Blooming
FParamValues=0,0,0,0.772,0.7,0.944,0.5,0.5,0
ParamValues=0,0,0,772,700,944,500,500,0
ParamValuesFeedback\WormHoleEclipse=0,180,1000,1000,1000,1000,0,976,764,500,500
Enabled=1
Name=Master LCD

[AppSet14]
App=Postprocess\Youlean Pixelate
FParamValues=0.432,0,0,0
ParamValues=432,0,0,0
ParamValuesHUD\HUD Prefab=6,0,500,0,1000,500,1000,514,1000,1000,444,0,660,0,368,372,736,1000
ParamValuesCanvas effects\Lava=0,664,1000,684,168,500,500,1000,528,500,224
ParamValuesFeedback\70sKaleido=0,0,320,180,908,152
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,1000,0,432,652,616,500,500
ParamValuesHUD\HUD Grid=0,500,0,1000,500,500,1000,1000,1000,444,500,1000,500,100,0,500,1000,800,904,300,0,1000
ParamValuesPostprocess\Youlean Motion Blur=336,1000,296,492
ParamValuesImage effects\Image=0,0,0,0,644,500,500,96,53,250,0,0,0,0
ParamValuesPostprocess\Youlean Bloom=1000,648,0,742
ParamValuesPostprocess\Youlean Blur=500,1000,600
ParamValuesPostprocess\Youlean Color Correction=500,488,500,972,800,620
Enabled=1
ImageIndex=3
Name=TOTAL Blur

[AppSet15]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=1,0,0.314,0.414,1,0,0,0
ParamValues=1000,0,314,414,1000,0,0,0
ParamValuesFeedback\FeedMeFract=0,0,0,0,296,0
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,708,348,544,500,260,500,500,560
Enabled=1
UseBufferOutput=1
Name=Cool Bloom

[AppSet1]
App=HUD\HUD Prefab
FParamValues=0,0,0.5,0,0,0.24,0.176,0.342,1,1,4,0,0.5,0,0.68,0.101,1,1
ParamValues=0,0,500,0,0,240,176,342,1000,1000,4,0,500,0,680,101,1000,1
ParamValuesBackground\SolidColor=0,676,0,956
Enabled=1
UseBufferOutput=1
Name=Grid BG 2
LayerPrivateData=780173C8CBCF4BD52B2E4B671899000060F9036F

[AppSet10]
App=Background\SolidColor
FParamValues=0,0,0,0.856
ParamValues=0,0,0,856
ParamValuesPostprocess\ScanLines=200,0,0,0,0,0
Enabled=1
Name=Background

[AppSet11]
App=HUD\HUD Prefab
FParamValues=90,0.652,0.5,0,0,0.5,0.5,0.274,1,1,4,0,0.5,0,0.68,0,1,0
ParamValues=90,652,500,0,0,500,500,274,1000,1000,4,0,500,0,680,0,1000,0
ParamValuesHUD\HUD Grid=0,500,0,1000,500,500,1000,1000,1000,444,500,1000,500,660,68,220,1000,0,704,328,0,1000
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,157,500,500,500,500,500
ParamValuesPostprocess\ScanLines=0,71,464,464,0,0
Enabled=1
Name=Grid BG 1
LayerPrivateData=78014B2FCA4C89490712BA0686167A9939650C230B0000BBC505E8

[AppSet18]
App=Image effects\Image
FParamValues=0,0.112,0.028,0.752,1,0.526,0.578,0.212,0.068,0.048,0,1,1,0.388
ParamValues=0,112,28,752,1000,526,578,212,68,48,0,1,1,388
ParamValuesCanvas effects\DarkSpark=500,0,500,500,252,500,500,500,500,500,0,0
ParamValuesPostprocess\RGB Shift=1000,0,0,600,700,200
ParamValuesPostprocess\Blooming=0,0,0,1000,1000,800,1000,1000,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesCanvas effects\SkyOcean=0,980,1000,0,248,500,500,772,836,500,464,936,732
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPostprocess\Point Cloud Default=0,1000,494,520,420,652,400,479,60,641,156,1000,184,0,333
ParamValuesPostprocess\AudioShake=360,0,333,500,100,900
ParamValuesPostprocess\Youlean Pixelate=568,0,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,1000,670,500,500,820,188,500,500,500,272
ParamValuesCanvas effects\StarTaser2=308,500,500,500,1000,1000,500,500,500,388,84,676,0
ParamValuesPostprocess\Ascii=0,0,572,600,700,200
ParamValuesHUD\HUD Prefab=83,652,500,0,0,500,500,274,1000,1000,444,0,500,0,680,0,1000,1000
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesCanvas effects\Digital Brain=0,0,0,0,1000,500,500,0,732,300,528,250,1000,0,1000,852
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,0,20,356,500,664,532,788
ParamValuesPostprocess\ColorCyclePalette=0,0,0,0,0,1000,320,280,844,0,528
ParamValuesHUD\HUD Grid=0,500,0,1000,500,500,1000,1000,704,444,500,1000,500,456,636,500,1000,552,1000,0,1000,1000
ParamValuesPostprocess\Edge Detect=728,768,676,820,660,592
ParamValuesCanvas effects\TaffyPulls=500,0,1000,0,0,500,500,584,500,500,656,560,0,500
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesFeedback\FeedMe=0,0,0,1000,500,1000,388
ParamValuesPostprocess\Dot Matrix=904,500,696,808,0,52,1000,1000
ParamValuesPostprocess\Vignette=0,0,0,600,808,184
ParamValuesCanvas effects\OverlySatisfying=0,740,0,0,600,500,500,100,100,1000,500
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,696,500,748,544,500,552,500,500
ParamValuesFeedback\70sKaleido=0,0,400,420,476,364
ParamValuesCanvas effects\Flaring=0,60,0,0,624,500,500,181,0,800,0
ParamValuesPostprocess\Blur=1000
ParamValuesPostprocess\Point Cloud Low=0,450,170,484,512,688,500,524,646,673,108,0,588,0,333
Enabled=1
ImageIndex=3
Name=Table

[AppSet3]
App=HUD\HUD 3D
FParamValues=0,0,0.654,0.446,0.496,0.492,0.48,0.464,0.508,0.43,0.523,0.37,0.481,0.24,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=0,0,654,446,496,492,480,464,508,430,523,370,481,240,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
ParamValuesImage effects\Image=588,0,0,120,1000,500,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=2
Name=LCD Module

[AppSet2]
App=Image effects\Image
FParamValues=0,0,0,0.704,0.944,0.608,0.492,0,0,0,0,0,0,0
ParamValues=0,0,0,704,944,608,492,0,0,0,0,0,0,0
Enabled=1
Name=Object 1

[AppSet12]
App=Background\FourCornerGradient
FParamValues=12,0.264,0.596,0.456,1,0.578,0.632,0.764,0.548,1,0.196,0.618,1,0.276
ParamValues=12,264,596,456,1000,578,632,764,548,1000,196,618,1000,276
Enabled=1
Name=Master Filter Color

[AppSet13]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Name=Fader in-out

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html=""
Images="[plugpath]Content\Bitmaps\Vector art\iMac.ilv"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

