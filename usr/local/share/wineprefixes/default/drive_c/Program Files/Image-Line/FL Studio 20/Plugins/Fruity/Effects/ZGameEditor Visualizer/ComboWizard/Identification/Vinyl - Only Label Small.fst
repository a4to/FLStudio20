FLhd   0 * ` FLdt�  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV Ն  ﻿[General]
GlWindowMode=1
LayerCount=7
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=3,4,5,0,6,7,2

[AppSet3]
App=HUD\HUD Text
FParamValues=0,0.5,0,0.944,0.5,0.324,0.5,0,0,0,0,37,0.328,0,1,3,1,0.5,0.5,0,1,0,0,0,0
ParamValues=0,500,0,944,500,324,500,0,0,0,0,37,328,0,1,3,1000,500,500,0,1000,0,0,0,0
Enabled=1
LayerPrivateData=7801732B4D4FACF2CF4BD50D4A4D2FCD492CD2AB4ACB67184100005339079C

[AppSet4]
App=HUD\HUD Text
FParamValues=0,0.5,0,0.944,0.5,0.688,0.5,0.54,0,0,0.946,30,0.304,0,1,3,1,0.5,0.5,0,1,0,0,0,0
ParamValues=0,500,0,944,500,688,500,540,0,0,946,30,304,0,1,3,1000,500,500,0,1000,0,0,0,0
Enabled=1
LayerPrivateData=780173C92FCE2CD6F54D4DC92CCDD52B29496318610000DF0D060D

[AppSet5]
App=HUD\HUD Text
FParamValues=0,0.5,0,0.944,0.5,0.744,0.5,1,0,0,0.73,61,0.24,0,1,3,1,0.5,0.5,0,1,0,0,0,0
ParamValues=0,500,0,944,500,744,500,1000,0,0,730,61,240,0,1,3,1000,500,500,0,1000,0,0,0,0
Enabled=1
UseBufferOutput=1
LayerPrivateData=7801F32F2E4FCC49D1F5C94CCF28D12B29496318610000DEB0060C

[AppSet0]
App=Image effects\Image
FParamValues=0,0,0,0,0.952,0.5,0.5,0,0,0.16,0,0,0,0
ParamValues=0,0,0,0,952,500,500,0,0,160,0,0,0,0
Enabled=1

[AppSet6]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0.155,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,155,0,0,0,0
Enabled=1
UseBufferOutput=1
ImageIndex=2

[AppSet7]
App=HUD\HUD Image
FParamValues=0,0,0.4816,0.5172,1,1,0.232,4,0.5,0,0,1,1,1,1
ParamValues=0,0,481,517,1000,1000,232,4,500,0,0,1000,1000,1,1
ParamValuesImage effects\Image=0,156,104,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
ImageIndex=3

[AppSet2]
App=Misc\Automator
FParamValues=1,7,10,3,1,0.038,0.25,0,5,11,3,1,0.038,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,7,10,3,1000,38,250,0,5,11,3,1000,38,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text=[title],[author],[comment]
Html="<position x=""0""><position y=""27""><p align=""center""><font face=""American-Captain"" size=""08"" color=""#000000"">[title]</font></p></position>","<position x=""0""><position y=""60""><p align=""center""><font face=""Khand-Regular"" size=""6"" color=""#000000"">[author]</font></p></position>","<position x=""0""><position y=""68""><p align=""center""><font face=""Chosence-Bold"" size=""3.5"" color=""#000000"">[comment]</font></p></position>"
Images=[plugpath]Content\Bitmaps\VinylLabel.png,[plugpath]Content\Bitmaps\VinylBack.jpg
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

