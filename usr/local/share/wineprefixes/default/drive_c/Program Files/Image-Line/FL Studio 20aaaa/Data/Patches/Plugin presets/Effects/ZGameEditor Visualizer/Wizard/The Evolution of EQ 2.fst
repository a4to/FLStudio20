FLhd   0 * ` FLdt�  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��>  ﻿[General]
GlWindowMode=1
LayerCount=6
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,1,3,2,11,12
WizardParams=81

[AppSet0]
App=Background\SolidColor
FParamValues=0,0.612,0.068,0.924
ParamValues=0,612,68,924
Enabled=1
Collapsed=1
Name=Background

[AppSet1]
App=Peak Effects\Linear
FParamValues=0,1,1,0,0.634,0.5,0.496,0.256,0,0.5,0.26,0.498,0.496,1,0,1,0,0.5,0.5,0.5,0.5,1,1,0.076,0.184,0.104,0.048,0.428,0.35,0.282,0.124
ParamValues=0,1000,1000,0,634,500,496,256,0,500,260,498,496,1,0,1,0,500,500,500,500,1000,1000,76,184,104,48,428,350,282,124
ParamValuesMisc\FruityIndustry=0,0,1000,0,0,500,500,500,500,0,500,500,500
ParamValuesCanvas effects\DarkSpark=500,24,500,500,500,500,500,500,500,500,0,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,838,500,500,500,500,500
ParamValuesTerrain\GoopFlow=424,1000,1000,0,1000,500,304,0,500,496,324,496,1000,300,168,92
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesCanvas effects\SkyOcean=0,0,500,0,500,604,500,0,500,1000,388,0,0
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesMisc\Automator=0,74,30,429,0,282,466,1000,185,61,286,508,526,254,1000,111,91,571,0,250,250,0,0,0,0,0,250,250,0,0,0,0
Enabled=1
Collapsed=1
Name=EQ 1

[AppSet3]
App=Peak Effects\Linear
FParamValues=0,0.248,0.068,0.788,0.634,0.5,0.496,0.256,0,0.5,0.26,0.47,0.496,1,0,1,0,0.5,0.5,0.5,0.5,1,0.3,0.052,0.164,0.104,0,0.248,0.234,0.282,0.124
ParamValues=0,248,68,788,634,500,496,256,0,500,260,470,496,1,0,1,0,500,500,500,500,1000,300,52,164,104,0,248,234,282,124
ParamValuesTerrain\GoopFlow=104,628,1000,168,1000,500,304,0,500,496,104,460,1000,300,308,36
Enabled=1
Collapsed=1
Name=EQ 2

[AppSet2]
App=Peak Effects\Linear
FParamValues=0,0,0,0,0.634,0.5,0.496,0.256,0,0.5,0.26,0.37,0.496,1,0,1,0,0.5,0.5,0.5,0.5,1,0.3,0.052,0.164,0.104,0,0.248,0.234,0.282,0.124
ParamValues=0,0,0,0,634,500,496,256,0,500,260,370,496,1,0,1,0,500,500,500,500,1000,300,52,164,104,0,248,234,282,124
ParamValuesPostprocess\Vignette=0,0,0,576,944,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesBackground\SolidColor=0,612,68,924
ParamValuesPostprocess\ScanLines=929,929,0,0,0,1000
ParamValuesTerrain\GoopFlow=512,1000,868,0,1000,500,304,0,500,496,324,460,1000,300,308,128
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPostprocess\Dot Matrix=1000,0,188,0,680,500,1000,1000
Enabled=1
Collapsed=1
Name=EQ 3

[AppSet11]
App=Text\TextTrueType
FParamValues=0.152,0,0,0,0,0.5,0.5,0,0,0,0.5
ParamValues=152,0,0,0,0,500,500,0,0,0,500
Enabled=1
Collapsed=1
Name=Main Text

[AppSet12]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fade in-out

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""4"" y=""5""><p><font face=""American-Captain"" size=""5"" color=""#F9F9F9"">[author]</font></p></position>","<position x=""4"" y=""9""><p><font face=""Chosence-Bold"" size=""3"" color=""#F9F9F9"">[title]</font></p></position>","<position x=""4"" y=""15.5""><p> <font face=""Chosence-Bold"" size=""3"" color=""#F9F9F9"">[comment]</font></p></position>"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

