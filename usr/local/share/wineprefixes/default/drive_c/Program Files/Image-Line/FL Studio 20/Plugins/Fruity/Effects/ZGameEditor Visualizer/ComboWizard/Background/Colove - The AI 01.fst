FLhd   0 * ` FLdt&  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ՜=�  ﻿[General]
GlWindowMode=1
LayerCount=17
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=8,6,2,7,1,3,5,4,11,0,15,14,16,9,10,12,19
WizardParams=565,566,567,568,569,947

[AppSet8]
App=Peak Effects\Linear
FParamValues=0.888,0.58,1,0,0.494,0.5,0.468,0.044,0,0.512,0,0.446,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,0.62,0.18,0.224,0.992,0.348,0.54,0.244,0.336,0.2
ParamValues=888,580,1000,0,494,500,468,44,0,512,0,446,500,1,0,1,0,500,500,500,500,0,620,180,224,992,348,540,244,336,200
Enabled=1
Collapsed=1
ImageIndex=3
Name=EQ LINE 1

[AppSet6]
App=Peak Effects\Linear
FParamValues=0.66,0.688,1,0,0.482,0.5,0.468,0.056,0.5,0.512,0,0.472,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0.132,0.564,0.16,0.16,1,0.304,0.54,0.244,0.336,0.2
ParamValues=660,688,1000,0,482,500,468,56,500,512,0,472,500,1,0,1,0,500,500,500,500,132,564,160,160,1000,304,540,244,336,200
Enabled=1
Collapsed=1
ImageIndex=3
Name=EQ LINE 2

[AppSet2]
App=Peak Effects\Linear
FParamValues=0.848,0.724,0.42,0,0.482,0.5,0.468,0.044,0,0.512,0,0.46,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0.56,0.604,0.18,0.224,1,0.304,0.54,0.244,0.336,0.2
ParamValues=848,724,420,0,482,500,468,44,0,512,0,460,500,1,0,1,0,500,500,500,500,560,604,180,224,1000,304,540,244,336,200
Enabled=1
Collapsed=1
ImageIndex=3
Name=EQ LINE 3

[AppSet7]
App=Peak Effects\Linear
FParamValues=0.832,0.78,1,0,0.482,0.5,0.468,0.056,0,0.512,0,0.5,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0.204,0.564,0.16,0.16,1,0.304,0.54,0.244,0.336,0.2
ParamValues=832,780,1000,0,482,500,468,56,0,512,0,500,500,1,0,1,0,500,500,500,500,204,564,160,160,1000,304,540,244,336,200
Enabled=1
Collapsed=1
ImageIndex=3
Name=EQ LINE 4

[AppSet1]
App=Peak Effects\Linear
FParamValues=0.188,1,0.199,0.193,0.482,0.5,0.468,0.044,0.5,0.512,0,0.28,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0.092,0.668,0.18,0.224,1,0.304,0.54,0.244,0.336,0.2
ParamValues=188,1000,199,193,482,500,468,44,500,512,0,280,500,1,0,1,0,500,500,500,500,92,668,180,224,1000,304,540,244,336,200
ParamValuesPostprocess\AudioShake=0,0,0,500,100,900
Enabled=1
Collapsed=1
ImageIndex=3
Name=EQ LINE 5

[AppSet3]
App=Peak Effects\Linear
FParamValues=0.916,0.548,1,0,0.482,0.5,0.468,0.044,0.5,0.512,0,0.46,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0.264,0.5,0.18,0.224,1,0.304,0.54,0.244,0.336,0.2
ParamValues=916,548,1000,0,482,500,468,44,500,512,0,460,500,1,0,1,0,500,500,500,500,264,500,180,224,1000,304,540,244,336,200
Enabled=1
Collapsed=1
ImageIndex=3
Name=EQ LINE 6

[AppSet5]
App=Peak Effects\Linear
FParamValues=0.484,1,0,0,0.482,0.5,0.468,0.056,0,0.512,0,0.36,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0.324,0.564,0.16,0.16,1,0.304,0.54,0.244,0.336,0.2
ParamValues=484,1000,0,0,482,500,468,56,0,512,0,360,500,1,0,1,0,500,500,500,500,324,564,160,160,1000,304,540,244,336,200
Enabled=1
Collapsed=1
ImageIndex=3
Name=EQ LINE 7

[AppSet4]
App=Peak Effects\Linear
FParamValues=0.512,1,0,0,0.482,0.5,0.468,0.056,0.5,0.512,0,0.34,0.5,1,0,1,0,0.5,0.5,0.5,0.5,0,0.564,0.16,0.16,1,0.304,0.54,0.244,0.336,0.2
ParamValues=512,1000,0,0,482,500,468,56,500,512,0,340,500,1,0,1,0,500,500,500,500,0,564,160,160,1000,304,540,244,336,200
Enabled=1
UseBufferOutput=1
BufferRenderQuality=6
Collapsed=1
ImageIndex=3
Name=EQ LINE 8

[AppSet11]
App=HUD\HUD Prefab
FParamValues=0,0,0.5,0,0,0.804,0.28,0.442,1,1,4,0,0.5,1,0.368,0.103,1,1
ParamValues=0,0,500,0,0,804,280,442,1000,1000,4,0,500,1,368,103,1000,1
ParamValuesFeedback\FeedMeFract=0,0,0,1000,0,8
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPostprocess\Blooming=0,0,0,1000,500,800,4,328,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesPostprocess\ColorCyclePalette=0,0,0,0,467,0,296,756,0,0,0
ParamValuesHUD\HUD Grid=0,500,0,0,500,500,1000,1000,1000,444,500,1000,500,100,0,500,1000,100,500,300,0,1000
ParamValuesPostprocess\Edge Detect=424,0,1000,472,588,604
ParamValuesPostprocess\ScanLines=0,71,1000,1000,0,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPostprocess\Point Cloud Default=0,742,434,500,500,448,408,487,330,625,156,0,0,0,0
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesFeedback\FeedMe=0,0,428,448,500,608,460
ParamValuesPostprocess\Luminosity=544
ParamValuesFeedback\70sKaleido=0,0,568,352,244,0
ParamValuesPeak Effects\SplinePeaks=0,20,950,1000,812,432,1000,0,0,0
ParamValuesPeak Effects\ReflectedPeeks=0,500,500,1000,500,500,500,500,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,423,500,500,500,500,500
ParamValuesBackground\FourCornerGradient=467,0,620,1000,484,634,704,656,268,1000,1000,690,1000,1000
ParamValuesPostprocess\Blur=1000
ParamValuesPostprocess\ParameterShake=512,784,20,31,332,0,20,94,0
ParamValuesPostprocess\FrameBlur=764,0,804,372,374,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
UseBufferOutput=1
Name=LOGO
LayerPrivateData=780173C8CBCF4BD52B2E4B671899000060F9036F

[AppSet0]
App=Background\SolidColor
FParamValues=0,0.62,0,0.908
ParamValues=0,620,0,908
ParamValuesCanvas effects\Digital Brain=0,0,0,0,1000,500,500,600,100,300,100,582,1000,484,1000,0
Enabled=1
Collapsed=1
Name=Siri BG

[AppSet15]
App=Image effects\Image
FParamValues=0,0,0,0.936,1,0.524,0.572,0.56,0.048,0.496,0,1,1,0.784
ParamValues=0,0,0,936,1000,524,572,560,48,496,0,1,1,784
Enabled=1
Collapsed=1
ImageIndex=3
Name=Table

[AppSet14]
App=Image effects\Image
FParamValues=0,0,0,0.324,0.824,0.5,0.608,0,0,0,0,0,0,0
ParamValues=0,0,0,324,824,500,608,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
Name=Object

[AppSet16]
App=Image effects\Image
FParamValues=0,0,0,0.976,0.061,0.504,0.847,0,0,0,0,0,1,1
ParamValues=0,0,0,976,61,504,847,0,0,0,0,0,1,1000
Enabled=1
Collapsed=1
ImageIndex=3
Name=Dot LCD

[AppSet9]
App=Image effects\Image
FParamValues=0,0,0,0,0.04,0.504,0.853,0.004,0,0,1,0,0,1
ParamValues=0,0,0,0,40,504,853,4,0,0,1,0,0,1000
ParamValuesPeak Effects\Linear=940,492,1000,0,538,500,468,60,500,512,0,110,500,1000,0,1000,0,500,500,500,500,0,1000,180,224,992,348,540,244,336,200
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,0,0,0,500,500,500,500
ParamValuesPostprocess\Youlean Pixelate=588,667,0,0
ParamValuesPostprocess\Youlean Blur=104,0,508
ParamValuesPostprocess\Youlean Bloom=500,120,698,1000
ParamValuesPostprocess\Youlean Color Correction=392,504,472,500,600,540
ParamValuesFeedback\FeedMe=0,0,0,944,0,692,732
Enabled=1
Collapsed=1
ImageIndex=1
Name=Siri EQ Out

[AppSet10]
App=Background\FourCornerGradient
FParamValues=7,0,0.248,0.732,0.484,0.862,0.704,0.656,0.22,0.448,0.164,0.69,0.148,0.372
ParamValues=7,0,248,732,484,862,704,656,220,448,164,690,148,372
Enabled=1
Collapsed=1
Name=Filter Color HUE

[AppSet12]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
ParamValuesPostprocess\Vignette=0,0,360,600,700,664
ParamValuesPostprocess\Youlean Motion Blur=372,1000,96,300
ParamValuesPostprocess\Youlean Bloom=328,764,238,224
ParamValuesBackground\FourCornerGradient=467,0,620,1000,484,634,704,656,268,1000,1000,690,1000,1000
ParamValuesPostprocess\TransitionEffects=500,500,500,500,500,500,500,500,500,500,0,0,0,1000
Enabled=1
Collapsed=1
Name=Fade in-out

[AppSet19]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Images="[plugpath]Content\Bitmaps\Vector art\Enhancer Marantz.ilv"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

