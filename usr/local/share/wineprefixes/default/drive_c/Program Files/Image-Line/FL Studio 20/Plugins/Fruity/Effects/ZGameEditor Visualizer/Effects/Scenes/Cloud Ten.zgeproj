<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" FileVersion="2">
  <OnLoaded>
    <SpawnModel Model="ScreenModel"/>
  </OnLoaded>
  <OnUpdate>
    <ZExpression>
      <Expression>
<![CDATA[//Alpha.Value=Parameters[0];
//Hue.Value=Parameters[1];
//Saturation.Value=Parameters[2];
//Lightness.Value=Parameters[3];
//Size.Value=Parameters[4];
//PositionX.Value=Parameters[5];
//PositionY.Value=Parameters[6];
//PointSize.Value=Parameters[7];
//SoundInfluence.Value=Parameters[8];

LocalTime += App.DeltaTime*Parameters[6];]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <Content>
    <Group>
      <Children>
        <Array Name="Parameters" SizeDim1="11" Persistent="255">
          <Values>
<![CDATA[78DA6360C0051AECD145000D5B00C0]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Alpha



Planet
TimeOfDay
Speed]]>
          </StringValue>
        </Constant>
        <Constant Name="AuthorInfo" Type="2">
          <StringValue>
<![CDATA[nimitz
@stormoid
Adapted from https://www.shadertoy.com/view/XtS3DD#]]>
          </StringValue>
        </Constant>
      </Children>
    </Group>
    <Material Name="ScreenMaterial" Shading="1" Light="0" Blend="1" ZBuffer="0" Shader="ScreenShader">
      <Textures>
        <MaterialTexture Texture="NoiseBitmap" TexCoords="1"/>
      </Textures>
    </Material>
    <Shader Name="ScreenShader">
      <VertexShaderSource>
<![CDATA[varying vec2 position;

void main(){
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
  position=vec2(vertex.x,vertex.y);
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[//Cloud Ten by nimitz (twitter: @stormoid)
//Adapted from https://www.shadertoy.com/view/XtS3DD#

uniform float iGlobalTime;
uniform float resX;
uniform float resY;
uniform float viewportX;
uniform float viewportY;

uniform float Alpha;

uniform sampler2D tex1;
#define iChannel0 tex1

vec2 iResolution = vec2(resX,resY);

uniform float pMouseX;
uniform float pMouseY;

vec2 iMouse = vec2(pMouseX,pMouseY);

#define time iGlobalTime
mat2 mm2(in float a){float c = cos(a), s = sin(a);return mat2(c,s,-s,c);}
float noise(float t){return texture2D(iChannel0,vec2(t,.0)/vec2(256.0,256.0)).x;}
float moy = 0.;

float hash( float n ) { return fract(sin(n)*753.5453123); }
float noise( in vec3 x )
{
    vec3 p = floor(x);
    vec3 f = fract(x);
    f = f*f*(3.0-2.0*f);
    float n = p.x + p.y*157.0 + 113.0*p.z;
    return mix(mix(mix( hash(n+  0.0), hash(n+  1.0),f.x),
                   mix( hash(n+157.0), hash(n+158.0),f.x),f.y),
               mix(mix( hash(n+113.0), hash(n+114.0),f.x),
                   mix( hash(n+270.0), hash(n+271.0),f.x),f.y),f.z);
}

float fbm(in vec3 x)
{
    float rz = 0.;
    float a = .35;
    for (int i = 0; i<2; i++)
    {
        rz += noise(x)*a;
        a*=.35;
        x*= 4.;
    }
    return rz;
}

float path(in float x){ return sin(x*0.01-3.1415)*28.+6.5; }
float map(vec3 p){
    return p.y*0.07 + (fbm(p*0.3)-0.1) + sin(p.x*0.24 + sin(p.z*.01)*7.)*0.22+0.15 + sin(p.z*0.08)*0.05;
}

float march(in vec3 ro, in vec3 rd)
{
    float precis = .3;
    float h= 1.;
    float d = 0.;
    for( int i=0; i<17; i++ )
    {
        if( abs(h)<precis || d>70. ) break;
        d += h;
        vec3 pos = ro+rd*d;
        pos.y += .5;
	    float res = map(pos)*7.;
        h = res;
    }
	return d;
}

vec3 lgt = vec3(0);
float mapV( vec3 p ){ return clamp(-map(p), 0., 1.);}
vec4 marchV(in vec3 ro, in vec3 rd, in float t, in vec3 bgc)
{
	vec4 rz = vec4( 0.0 );

	for( int i=0; i<150; i++ )
	{
		if(rz.a > 0.99 || t > 200.) break;

		vec3 pos = ro + t*rd;
        float den = mapV(pos);

        vec4 col = vec4(mix( vec3(.8,.75,.85), vec3(.0), den ),den);
        col.xyz *= mix(bgc*bgc*2.5,  mix(vec3(0.1,0.2,0.55),vec3(.8,.85,.9),moy*0.4), clamp( -(den*40.+0.)*pos.y*.03-moy*0.5, 0., 1. ) );
        col.rgb += clamp((1.-den*6.) + pos.y*0.13 +.55, 0., 1.)*0.35*mix(bgc,vec3(1),0.7); //Fringes
        col += clamp(den*pos.y*.15, -.02, .0); //Depth occlusion
        col *= smoothstep(0.2+moy*0.05,.0,mapV(pos+1.*lgt))*.85+0.15; //Shadows

		col.a *= .9;
		col.rgb *= col.a;
		rz = rz + col*(1.0 - rz.a);

        t += max(.4,(2.-den*30.)*t*0.011);
	}

	return clamp(rz, 0., 1.);
}

float pent(in vec2 p){
    vec2 q = abs(p);
    return max(max(q.x*1.176-p.y*0.385, q.x*0.727+p.y), -p.y*1.237)*1.;
}

vec3 flare(vec2 p, vec2 pos) //Inspired by mu6k's lens flare (https://www.shadertoy.com/view/4sX3Rs)
{
	vec2 q = p-pos;
    vec2 pds = p*(length(p))*0.75;
	float a = atan(q.x,q.y);

    float rz = .55*(pow(abs(fract(a*.8+.12)-0.5),3.)*(noise(a*15.)*0.9+.1)*exp2((-dot(q,q)*4.))); //Spokes

    rz += max(1.0/(1.0+32.0*pent(pds+0.8*pos)),.0)*00.2; //Projected ghost (main lens)
    vec2 p2 = mix(p,pds,-.5); //Reverse distort
	rz += max(0.01-pow(pent(p2 + 0.4*pos),2.2),.0)*3.0;
	rz += max(0.01-pow(pent(p2 + 0.2*pos),5.5),.0)*3.0;
	rz += max(0.01-pow(pent(p2 - 0.1*pos),1.6),.0)*4.0;
    rz += max(0.01-pow(pent(-(p2 + 1.*pos)),2.5),.0)*5.0;
    rz += max(0.01-pow(pent(-(p2 - .5*pos)),2.),.0)*4.0;
    rz += max(0.01-pow(pent(-(p2 + .7*pos)),5.),.0)*3.0;

    return vec3(clamp(rz,0.,1.));
}

mat3 rot_x(float a){float sa = sin(a); float ca = cos(a); return mat3(1.,.0,.0,    .0,ca,sa,   .0,-sa,ca);}
mat3 rot_y(float a){float sa = sin(a); float ca = cos(a); return mat3(ca,.0,sa,    .0,1.,.0,   -sa,.0,ca);}
mat3 rot_z(float a){float sa = sin(a); float ca = cos(a); return mat3(ca,sa,.0,    -sa,ca,.0,  .0,.0,1.);}

void main()
{
    vec2 q = (gl_FragCoord.xy-vec2(viewportX,viewportY)) / iResolution.xy;
    vec2 p = q - 0.5;
	float asp =iResolution.x/iResolution.y;
    p.x *= asp;
	vec2 mo = iMouse.xy;
	moy = mo.y;
    float st = sin(time*0.3-1.3)*0.2;
    vec3 ro = vec3(0.,-2.+sin(time*.3-1.)*2.,time*30.);
    ro.x = path(ro.z);
    vec3 ta = ro + vec3(0,0,1);
    vec3 fw = normalize( ta - ro);
    vec3 uu = normalize(cross( vec3(0.0,1.0,0.0), fw ));
    vec3 vv = normalize(cross(fw,uu));
    const float zoom = 1.;
    vec3 rd = normalize( p.x*uu + p.y*vv + -zoom*fw );

    float rox = sin(time*0.2)*0.8+2.9;
    rox += smoothstep(0.6,1.2,sin(time*0.25))*3.5;
   	float roy = sin(time*0.5)*0.2;
    mat3 rotation = rot_x(-roy)*rot_y(-rox+st*1.5)*rot_z(st);
	mat3 inv_rotation = rot_z(-st)*rot_y(rox-st*1.5)*rot_x(roy);
    rd *= rotation;
    rd.y -= dot(p,p)*0.06;
    rd = normalize(rd);

    vec3 col = vec3(0.);
    lgt = normalize(vec3(-0.3,mo.y+0.1,1.));
    float rdl = clamp(dot(rd, lgt),0.,1.);

    vec3 hor = mix( vec3(.9,.6,.7)*0.35, vec3(.5,0.05,0.05), rdl );
    hor = mix(hor, vec3(.5,.8,1),mo.y);
    col += mix( vec3(.2,.2,.6), hor, exp2(-(1.+ 3.*(1.-rdl))*max(abs(rd.y),0.)) )*.6;
    col += .8*vec3(1.,.9,.9)*exp2(rdl*650.-650.);
    col += .3*vec3(1.,1.,0.1)*exp2(rdl*100.-100.);
    col += .5*vec3(1.,.7,0.)*exp2(rdl*50.-50.);
    col += .4*vec3(1.,0.,0.05)*exp2(rdl*10.-10.);
    vec3 bgc = col;

    float rz = march(ro,rd);

    if (rz < 70.)
    {
        vec4 res = marchV(ro, rd, rz-5., bgc);
    	col = col*(1.0-res.w) + res.xyz;
    }

    vec3 projected_flare = (-lgt*inv_rotation);
    col += 1.4*vec3(0.7,0.7,0.4)*max(flare(p,-projected_flare.xy/projected_flare.z*zoom)*projected_flare.z,0.);

    float g = smoothstep(0.03,.97,mo.x);
    col = mix(mix(col,col.brg*vec3(1,0.75,1),clamp(g*2.,0.0,1.0)), col.bgr, clamp((g-0.5)*2.,0.0,1.));

	  col = clamp(col, 0., 1.);
    col = col*0.5 + 0.5*col*col*(3.0-2.0*col); //saturation
    col = pow(col, vec3(0.416667))*1.055 - 0.055; //sRGB
//	col *= pow( 16.0*q.x*q.y*(1.0-q.x)*(1.0-q.y), 0.12 ); //Vign

	gl_FragColor = vec4( col, Alpha );
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="iGlobalTime" ValuePropRef="LocalTime"/>
        <ShaderVariable VariableName="resX" Value="1163" ValuePropRef="App.ViewportWidth"/>
        <ShaderVariable VariableName="resY" Value="432" ValuePropRef="App.ViewportHeight"/>
        <ShaderVariable VariableName="viewportX" Value="262" ValuePropRef="App.ViewportX"/>
        <ShaderVariable VariableName="viewportY" Value="262" ValuePropRef="App.ViewportY"/>
        <ShaderVariable VariableName="Alpha" ValuePropRef="1-Parameters[0];"/>
        <ShaderVariable VariableName="pMouseX" ValuePropRef="Parameters[4];"/>
        <ShaderVariable VariableName="pMouseY" ValuePropRef="Parameters[5];"/>
      </UniformVariables>
    </Shader>
    <Model Name="ScreenModel">
      <OnRender>
        <UseMaterial Material="ScreenMaterial"/>
        <RenderSprite/>
      </OnRender>
    </Model>
    <Bitmap Name="NoiseBitmap" Comment="NoCustom" Width="256" Height="256" Filter="2">
      <Producers>
        <BitmapExpression>
          <Expression>
<![CDATA[//X,Y : current coordinate (0..1)
//Pixel : current color (rgb)
//Sample expression: Pixel.R=abs(sin(X*16));
const float s=1.0;
Pixel.r=rnd()*s;
Pixel.g=rnd()*s;
Pixel.b=rnd()*s;
Pixel.a=1;]]>
          </Expression>
        </BitmapExpression>
      </Producers>
    </Bitmap>
    <Variable Name="LocalTime"/>
  </Content>
</ZApplication>
