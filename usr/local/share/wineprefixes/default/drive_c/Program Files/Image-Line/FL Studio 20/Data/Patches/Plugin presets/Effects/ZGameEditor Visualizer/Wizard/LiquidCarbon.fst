FLhd   0  0 FLdt(  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   մ�  ﻿[General]
GlWindowMode=1
LayerCount=6
FPS=1
MidiPort=-1
Aspect=1
LayerOrder=0,1,2,3,4,5
WizardParams=33,34

[AppSet0]
App=Scenes\RhodiumLiquidCarbon
ParamValues=0,216,360,0,60,500,0,428,1000,980
Enabled=1
UseBufferOutput=1

[AppSet1]
App=Particles\ColorBlobs
ParamValues=176,628,376,320,756,500,500,114,1000,296
Enabled=1
UseBufferOutput=1

[AppSet2]
App=Blend\BufferBlender
ParamValues=0,1,5,1000,0,0,0,0,500,500,750,0
Enabled=1

[AppSet3]
App=Postprocess\AudioShake
ParamValues=196,0,0,600,700,200
Enabled=1

[AppSet4]
App=Text\TextTrueType
ParamValues=528,0,0,1000,0,494,498,0,0,0,500
Enabled=1

[AppSet5]
App=Text\TextTrueType
ParamValues=0,0,0,0,0,494,500,0,0,0,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0

[UserContent]
Text=""
Html="<position x=""3""><position y=""5""><p align=""left""><font face=""American-Captain"" size=""10"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""3""><position y=""13""><p align=""left""><font face=""Chosence-Bold"" size=""6"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""55""><position y=""90""><p align=""right""><font face=""Chosence-Bold"" size=""2"" color=""#FFFFFF"">[extra1]</font></p></position>","<position x=""67""><position y=""94""><p align=""right""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""2""><position y=""92""><p align=""center""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""2""><position y=""95""><p align=""center""><font face=""Chosence-regular"" size=""2"" color=""#FFFFF"">[extra3]</font></p></position>",ü
VideoUseSync=0
EnableMipmap=1

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

