<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="BallZ" ClearColor="0 0 0 1" AmbientLightColor="0.502 0.502 0.502 1" FullScreen="255" FrameRateStyle="1" ScreenMode="0" CameraPosition="37.1259 5.2838 37.5" CameraRotation="-0.125 0 -0.2725" ClipFar="10000" FileVersion="2" AndroidPackageName="com.rado1.BallZ">
  <Comment>
<![CDATA[BallZ - Script Unique Parameters

Shape - shape selector
Ball size - relative size of balls
Spin - rotation of scene
Sensitivity - sensitivity of movements to music intensity
Amount - amount of shape's movement
Variator - variation of shape

Note: allowed only for GPUs that support GLSL shaders]]>
  </Comment>
  <OnLoaded>
    <ZExternalLibrary Comment="OpenGL 4.0 graphics" ModuleName="opengl32" DefinitionsFile="opengl.txt">
      <BeforeInitExp>
<![CDATA[if(ANDROID) {
  if(App.GLBase==0)
    this.ModuleName="libGLESv1_CM.so";
  else
    this.ModuleName="libGLESv2.so";
}]]>
      </BeforeInitExp>
    </ZExternalLibrary>
    <ZLibrary Comment="ZgeViz interface">
      <Source>
<![CDATA[// PARAMETERS

const string ParamHelpConst =
"Alpha\n" +
"Hue\n" +
"Saturation\n" +
"Lightness\n" +
"Size\n" +
"Position X\n" +
"Position Y\n" +
"Shape @list: Wave, Plankton, Plop, Ctenophora, Coral, Starfish, Helix, Flower, Fountain, Bubbles, Worm\n" +
"Ball size\n" +
"Spin\n" +
"Sensitivity\n" +
"Amount\n" +
"Variator";

const int
  ALPHA = 0,
  HUE = 1,
  SATURATION = 2,
  LIGHTNESS = 3,
  SIZE = 4,
  POSITION_X = 5,
  POSITION_Y = 6,
  SHAPE = 7,
  BALL_SIZE = 8,
  SPIN = 9,
  SENSITIVITY = 10,
  AMOUNT = 11,
  VARIATOR = 12;

// VARIABLES

float[32] SpecBandArray;
float[0] AudioArray;
float SongPositionInBeats;
float[13] Parameters;

int LayerNr;
xptr FLPluginHandle;]]>
      </Source>
    </ZLibrary>
    <ZLibrary Comment="Globals">
      <Source>
<![CDATA[// CONSTANTS

// application-specific

const int AREA_SIZE = 32;
const int NUMBER_OF_BALLZ = AREA_SIZE * AREA_SIZE;
const int VBO_POINT_SIZE = 2;
const int VBO_SIZE = NUMBER_OF_BALLZ*VBO_POINT_SIZE*4; // 4 - size of float

// goniometry
const float PIx2 = PI * 2.0;


// VARIABLES

// VBO
int VertexBuffer;
float[AREA_SIZE, AREA_SIZE, VBO_POINT_SIZE] Balls;

// application
float BandMax;
int BandStep;

// OpenGL change callback -> init VBO + point sprites
void OnGLContextChange(){
  glGenBuffers(1, VertexBuffer);
  glBindBuffer(GL_ARRAY_BUFFER, VertexBuffer);
  glBufferData(GL_ARRAY_BUFFER, VBO_SIZE, Balls, GL_STATIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glEnable(GL_POINT_SPRITE);
  glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
}]]>
      </Source>
    </ZLibrary>
    <ZExternalLibrary ModuleName="ZGameEditor Visualizer" Source="void ParamsNotifyChanged(xptr Handle,int Layer) { }"/>
    <ZExpression Comment="Init">
      <Expression>
<![CDATA[// init positions of balls in VBO
for(int x = 0; x < AREA_SIZE; ++x)
  for(int y = 0; y < AREA_SIZE; ++y){
    Balls[x, y, 0] = x;
    Balls[x, y, 1] = y;
  }

// init OpenGL
OnGLContextChange();

// reset band max
BandMax = 0;

// set initial values of parameters
Parameters[ALPHA] = 0.0;
Parameters[HUE] = 0.0;
Parameters[SATURATION] = 1.0;
Parameters[LIGHTNESS] = 0.0;
Parameters[SIZE] = 0.75;
Parameters[POSITION_X] = 0.5;
Parameters[POSITION_Y] = 0.5;
Parameters[SPIN]  = 0.5;
Parameters[SENSITIVITY] = 0.5;
Parameters[AMOUNT] = 0.5;
Parameters[BALL_SIZE] = 0.5;
Parameters[VARIATOR] = 0.5;

// init scene
setRandomSeed(getSystemTime());
Parameters[SHAPE] = rnd();

ParamsNotifyChanged(FLPluginHandle,LayerNr);

// init band array iteration step
BandStep = SpecBandArray.SizeDim1 / 32;]]>
      </Expression>
    </ZExpression>
    <SpawnModel Model="BallsModel" SpawnStyle="1"/>
  </OnLoaded>
  <OnUpdate>
    <ZExpression Comment="Update scene">
      <Expression>
<![CDATA[// update camera position
App.CameraRotation.Z -= App.DeltaTime * (Parameters[SPIN]-0.5) * -0.3;
float
  a = App.CameraRotation.Z * -PIx2,
  s = sin(a), c = cos(a),
  k = (Parameters[POSITION_X]-0.5) * (4.0 - Parameters[SIZE]),
  d = 150.0 * (1.0 - Parameters[SIZE]);
App.CameraPosition.X = d * (s - c * k);
App.CameraPosition.Y = d * (c + s * k) * -1.0;
App.CameraPosition.Z = d - 4.0 * (Parameters[POSITION_Y]-0.5) * d;

// compute band max
float b = 0;
for(int i=SpecBandArray.SizeDim1-1; i >= 0; i -= BandStep)
  if(b < SpecBandArray[i]) b = SpecBandArray[i];

BandMax = BandMax + (b - BandMax) * pow(Parameters[SENSITIVITY], 4.0);]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <OnClose>
    <ZExpression Comment="Delete objects" Expression="glDeleteBuffers(1, VertexBuffer);"/>
  </OnClose>
  <Content>
    <Group Comment="GPU computation">
      <Children>
        <Shader Name="DrawShader">
          <VertexShaderSource>
<![CDATA[// constants
const float AREA_SIZE = 32.0;
const float AREA_MID = AREA_SIZE / 2.0;
const float PI = 3.14159265;
const float PIx2 = PI * 2.0;
const float PI3 = PIx2 / 3.0;
const float PI6 = PI3 * 2.0;

// inputs
uniform float screenSize;
uniform float time;
uniform float deltaTime;
uniform float scene;
uniform float alpha;
uniform float hue;
uniform float saturation;
uniform float lightness;
uniform float volume;
uniform float ballSize;
uniform float variator;

// results of computation
varying vec4 position;
varying float size;
varying vec3 color;
varying float realAlpha;

/// ----------------------------------------------
//
// Description : Array and textureless GLSL 2D simplex noise function.
//      Author : Ian McEwan, Ashima Arts.
//  Maintainer : ijm
//     Lastmod : 20110822 (ijm)
//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
//               Distributed under the MIT License. See LICENSE file.
//               https://github.com/ashima/webgl-noise
//

vec3 mod289(vec3 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec2 mod289(vec2 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec3 permute(vec3 x) {
  return mod289(((x*34.0)+1.0)*x);
}

float snoise(vec2 v) {
  const vec4 C = vec4(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
                      0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
                     -0.577350269189626,  // -1.0 + 2.0 * C.x
                      0.024390243902439); // 1.0 / 41.0
// First corner
  vec2 i  = floor(v + dot(v, C.yy) );
  vec2 x0 = v -   i + dot(i, C.xx);

// Other corners
  vec2 i1;
  //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0.0
  //i1.y = 1.0 - i1.x;
  i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
  // x0 = x0 - 0.0 + 0.0 * C.xx ;
  // x1 = x0 - i1 + 1.0 * C.xx ;
  // x2 = x0 - 1.0 + 2.0 * C.xx ;
  vec4 x12 = x0.xyxy + C.xxzz;
  x12.xy -= i1;

// Permutations
  i = mod289(i); // Avoid truncation effects in permutation
  vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))
		+ i.x + vec3(0.0, i1.x, 1.0 ));

  vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);
  m = m*m ;
  m = m*m ;

// Gradients: 41 points uniformly over a line, mapped onto a diamond.
// The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)

  vec3 x = 2.0 * fract(p * C.www) - 1.0;
  vec3 h = abs(x) - 0.5;
  vec3 ox = floor(x + 0.5);
  vec3 a0 = x - ox;

// Normalise gradients implicitly by scaling m
// Approximation of: m *= inversesqrt( a0*a0 + h*h );
  m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );

// Compute final noise value at P
  vec3 g;
  g.x  = a0.x  * x0.x  + h.x  * x0.y;
  g.yz = a0.yz * x12.xz + h.yz * x12.yw;
  return 130.0 * dot(m, g);
}

/// ----------------------------------------------
// http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl

vec3 rgb2hsv(vec3 c) {
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c) {
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

/// ----------------------------------------------

void computeShape(float x, float y) {

  float U, V, C, D, W, S, K;
  int intScene = int(scene);

  //switch(intScene){

  if(intScene==0) {
    //case 0: // wave

      V = time * (0.1 + variator * 3.9);
      U = snoise(vec2((x + V) / 20.0, (y + V) / 20.0)) * 5.0 * pow(volume, 1.5);

      position = vec4(
        x - AREA_MID,
        y - AREA_MID,
        U + 4.0,
        1.0);

      size = 0.5 + U * 0.04;

      color = vec3(
        U / 6.0,
        0.2 - U / 4.0,
        0.8 - U / 8.0);

      //break;
   } else if(intScene==1) {
    //case 1: // phytoplankton

      U = PIx2 * x / AREA_SIZE;
      V = volume * (y - AREA_MID) / AREA_SIZE;
      C = variator*26.0 + V * cos(U - time/3.0) * 13.0 * volume;

      position = vec4(
        cos(U) * C,
        sin(U) * C,
        sin(U) * V * 15.0,
        1.0);

      size = 0.4 + abs(V);

      color = vec3(
        abs(sin(U)),
        C / 15.0,
        0.2 + abs(cos(U)));

//      break;

   } else if(intScene==2) {
//    case 2: // plop

      W = x - AREA_MID;
      S = y - AREA_MID;
      D = length(vec2(W,S));
      U = volume * 25.0 * cos(D - time*2.0) / (D + 6.0);

      position = vec4(W, S, U, 1.0);

      size = 2.0 - D * 0.25 * variator;

      color = vec3(
        1.0 - U / 30.0,
        0.4 - U / 8.0,
        D * 0.1);

//      break;

   } else if(intScene==3) {
    //case 3: // ctenophora

      U = PI * x / AREA_MID;
      V = PI * (y - AREA_MID) / AREA_SIZE;
      D = volume*abs(sin(time + U*4.0*variator) * cos(time + V*6.0*variator) * 1.8);
      W = 11.0 + D;
      C = cos(V) * W;

      position = vec4(
        cos(U) * C,
        sin(U) * C,
        sin(V) * W - 2.0,
        1.0);

      size = 0.3 + D;

      color = vec3(
        D / 2.0,
        0.7 - D,
        D);

//      break;

   } else if(intScene==4) {
//    case 4: // coral

      // init
      K = (y * AREA_SIZE + x);
      S = 6.0;
      U = 0.0;
      V = 0.0;
      W = 0.0;
      C = 0.0;
      D = pow(volume, 0.3);
      float posX = 0.0;
      float posY = 0.0;
      float posZ = -6.0;

      // iterate
      while (K > 0.0) {
        K--;
        K = K / 3.0;
        if(fract(K) < 0.3) { // K == 0; position 0
          W += 2.09;
        } else {if(fract(K) < 0.6) { // K == 0.666...; position 1
          W -= 2.09;
        }}

        U += sin(W) * (0.5 + 2.0*variator*abs(sin(C * 1.8 + time / 3.0)) / 2.0);
        V += cos(W) * (0.5 + 2.0*variator*abs(cos(C * 1.8 + time / 2.0)) / 2.0);
        posX += sin(V) * S * 1.4 * D;
        posY += cos(V) * sin(U) * S * 1.4 * D;
        posZ += cos(V) * cos(U) * S * 1.4 * D;
        S *= 0.6 * D;
        K = floor(K);
        C++;
      }

      position = vec4(
        posX,
        posY,
        posZ,
        1.0);

      size = S;

      S /= 4.0;
      color = vec3(
        2.0 - S,
        1.0 - S,
        0.3 + S);

//      break;

   } else if(intScene==5) {
//    case 5: // sunflower starfish

      U = x * 0.196349; //2*PI/32
      V = pow(y, 0.8);
      W = V + 10.0*variator;

      position = vec4(
        cos(U) * W,
        sin(U) * W,
        2.0 + sin(time + y / 10.0 + x) * (y / 5.0 + 0.6) * volume,
        1.0);

      size = 1.0 - V / 18.0;

      color = vec3(
        y / 30.0,
        y / 10.0,
        0.8 - y / 40.0);

//      break;

   } else if(intScene==6) {
//    case 6: // helix

      U = y + PI * x / AREA_MID;
      V = x + time + PI * (y - AREA_MID) / 2.0;
      C = cos(V) * 5.0 * volume;
      W = C + 30.0 * variator;

      position = vec4(
        cos(U) * W,
        sin(U) * W,
        2.0 + sin(V) * 5.0*volume,
        1.0);

      size = 0.4 + abs(C)/15.0;

      color = clamp(vec3(
        sin(U*3.0) + 0.1,
        0.4 - C * 0.1 ,
        0.1 + C * 0.2), 0.0, 1.0) * 0.9;

//      break;

   } else if(intScene==7) {
//    case 7: // Mobius flower

      U = y + PI * x / AREA_MID;
      V = U*5.0 + time + PI * (y - AREA_MID) / AREA_SIZE;
      C = cos(V) * 5.0 * volume;
      W = C + 28.0 * variator;

      position = vec4(
        cos(U) * W,
        sin(U) * W,
        2.0 + sin(V)* 5.0 * volume,
        1.0);

      size = 0.5 + abs(C + sin(U*10.0)) / 30.0;

      color = clamp(vec3(
        C,
        sin(U*5.0)+1.0,
        0.5), 0.0, 1.0)*0.75;

//      break;

   } else if(intScene==8) {
//    case 8: // fountain

      U = PI * x / AREA_MID;
      V = U + PI * (y - AREA_MID) / AREA_SIZE - time;
      C = cos(V) * 7.0*volume;
      W = 2.0 + C + 20.0*variator;
      K = sin(U);

      position = vec4(
        cos(U) * W,
        K * W,
        2.0 + sin(V)*10.0*volume,
        1.0);

      size = 0.1 + abs(cos(V));

      color = vec3(
        y / AREA_MID,
        0.5 + K*0.2,
        0.5);

//      break;

   } else if(intScene==9) {
//    case 9: // bubbles

      C = 10.0 + time * 0.02 + volume * 0.018;
      U = snoise(vec2(x,y+C));
      V = snoise(vec2(y+C,x+C));
      W = snoise(vec2(y-C,x));

      color = vec3(
        0.6 + U*0.4,
        0.6 + V*0.4,
        0.6 + W*0.4);

      D = variator / length(vec3(U, V, W));
      if(D < 1.0){
        U *= D;
        V *= D;
        W *= D;
      }

      position = vec4(
        clamp(vec3(U, V, W) * AREA_SIZE, -AREA_MID, AREA_MID),
        1.0);

      size = 0.3 + abs(snoise(vec2(x + volume, y - volume)))* volume;

//      break;

   } else if(intScene==10) {
//    case 10: // worm

      U = x + y * AREA_SIZE;
      V = U * 0.005 * variator + time*0.2 + volume;

      position = vec4(
        snoise(vec2(V,13.0)) * AREA_MID,
        snoise(vec2(7.0,V)) * AREA_MID,
        snoise(vec2(V,V)) * AREA_MID,
        1.0);

      size = 0.2 + U/1024.0;

      color = hsv2rgb(vec3(0.1 + U/1137.78, 1.0, 1.0));

  } // end of switch by scene

  // compute final color
  color = rgb2hsv(color);
  color.r = fract(hue + color.r);
  color.g *= saturation;
  color.b *= lightness;
  color = clamp(hsv2rgb(color), 0.0, 1.0);
}

void main() {
  computeShape(gl_Vertex.x, gl_Vertex.y);

  // position
  gl_Position = gl_ModelViewProjectionMatrix * position;
  position = gl_ModelViewMatrix * position;

  // point size
  //http://stackoverflow.com/questions/8608844/resizing-point-sprites-based-on-distance-from-the-camera
  size *= ballSize;
  realAlpha = size > 0.0 ? alpha: 0.0;
  vec4 p = gl_ProjectionMatrix * vec4(size, size, position.z, position.w);
  gl_PointSize = screenSize * p.x / p.w;
}]]>
          </VertexShaderSource>
          <FragmentShaderSource>
<![CDATA[varying vec4 position;
varying float size;
varying vec3 color;
varying float realAlpha;
varying float pointSize;

void main()
{
#ifdef macos
  vec2 p = vec2(0.5,0.5) * 2.0 - vec2(1.0);
#else
  vec2 p = gl_PointCoord * 2.0 - vec2(1.0);
#endif
  float d = dot(p,p);

  if (d <= 1.0) {
    d = sqrt(1.0 - d);
    gl_FragColor = vec4(color * d, realAlpha);
    vec4 pos = position;
    pos.z += d * size;
    pos = gl_ProjectionMatrix * pos;
    gl_FragDepth = 0.5 + (pos.z / pos.w) * 0.5;
  } else {
    gl_FragColor = vec4(0.0);
    gl_FragDepth = 1000.0;
  }
}]]>
          </FragmentShaderSource>
          <UniformVariables>
            <ShaderVariable VariableName="screenSize" ValuePropRef="App.ViewportWidth"/>
            <ShaderVariable VariableName="time" ValuePropRef="SongPositionInBeats"/>
            <ShaderVariable VariableName="scene" ValuePropRef="clamp(round(Parameters[SHAPE]*11.0), 0, 10)"/>
            <ShaderVariable VariableName="alpha" ValuePropRef="1.0 - Parameters[ALPHA]"/>
            <ShaderVariable VariableName="hue" ValuePropRef="Parameters[HUE]"/>
            <ShaderVariable VariableName="saturation" ValuePropRef="Parameters[SATURATION]"/>
            <ShaderVariable VariableName="lightness" ValuePropRef="1.0-Parameters[LIGHTNESS]"/>
            <ShaderVariable VariableName="volume" ValuePropRef="BandMax * Parameters[AMOUNT] * 4.0"/>
            <ShaderVariable VariableName="ballSize" ValuePropRef="2.0 * Parameters[BALL_SIZE]"/>
            <ShaderVariable VariableName="variator" ValuePropRef="Parameters[VARIATOR]"/>
          </UniformVariables>
        </Shader>
        <Material Name="DrawMaterial" Shading="1" Light="0" Blend="1" Shader="DrawShader"/>
      </Children>
    </Group>
    <Model Name="BallsModel">
      <OnRender>
        <UseMaterial Material="DrawMaterial"/>
        <ZExpression Comment="Draw balls">
          <Expression>
<![CDATA[// draw balls
glBindBuffer(GL_ARRAY_BUFFER, VertexBuffer);
glVertexPointer(VBO_POINT_SIZE, GL_FLOAT, 0, null);
glEnableClientState(GL_VERTEX_ARRAY);

glDrawArrays(GL_POINTS, 0, NUMBER_OF_BALLZ);

glDisableClientState(GL_VERTEX_ARRAY);
glBindBuffer(GL_ARRAY_BUFFER, 0);]]>
          </Expression>
        </ZExpression>
      </OnRender>
    </Model>
    <Constant Name="AuthorInfo" Type="2" StringValue="Rado1"/>
  </Content>
</ZApplication>
