<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" CameraPosition="0 0 8" FileVersion="2">
  <OnLoaded>
    <SpawnModel Model="Canvas"/>
  </OnLoaded>
  <OnUpdate>
    <ZExpression>
      <Expression>
<![CDATA[float ViewportWidth = App.ViewportWidth;
float ViewportHeight = App.ViewportHeight;

if (Parameters[7] > 0.5) AmountHorizontal.Value = floor(Parameters[2] * ViewportHeight * 0.25);
else AmountHorizontal.Value = floor(Parameters[2] * 200);

if (Parameters[6] > 0.5) AmountHorizontal.Value *= 10;

AmountVertical.Value = AmountHorizontal.Value;
brightPassThreshold.Value = Parameters[1];

uRes=vector2(app.ViewportWidth,app.ViewportHeight);

Exposure.Value = Parameters[0]*10;
Gamma.Value = Parameters[3]*2.2;
Opacity.Value = Parameters[4];
Blend.Value = floor(Parameters[5] * 1000);]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <Content>
    <Group>
      <Children>
        <Array Name="Parameters" SizeDim1="8" Persistent="255">
          <Values>
<![CDATA[789C636060B0670082593325EDF477BDB063606800F321A0C11E005C080557]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Amount
Threshold
Radius
Gamma
Opacity
Blend @list1000: "Gama","Gama Alternative", "Additive"
10x Radius @checkbox
Uniform Radius @checkbox]]>
          </StringValue>
        </Constant>
        <Variable Name="uRes" Type="6"/>
      </Children>
    </Group>
    <Model Name="Canvas" Rotation="0 0 0.001">
      <OnRender>
        <ZExpression>
          <Expression>
<![CDATA[BrightPassTexture.RenderTarget=FeedbackMaterialTexture.RenderTarget;
orgTexture.RenderTarget=FeedbackMaterialTexture.RenderTarget;]]>
          </Expression>
        </ZExpression>
        <SetRenderTarget RenderTarget="TempRenderTarget1"/>
        <UseMaterial Material="BrightPassMaterial"/>
        <RenderSprite/>
        <ZExpression Expression="BrightPassTexture.RenderTarget=TempRenderTarget1;"/>
        <SetRenderTarget RenderTarget="TempRenderTarget2"/>
        <UseMaterial Material="BrightPassMaterial"/>
        <RenderSprite/>
        <SetRenderTarget RenderTarget="TempRenderTarget1"/>
        <UseMaterial Material="HorizontalMaterial"/>
        <RenderSprite/>
        <SetRenderTarget/>
        <UseMaterial Material="FeedbackMaterial"/>
        <RenderSprite/>
        <SetRenderTarget RenderTarget="TempRenderTarget2"/>
        <UseMaterial Material="VerticalMaterial"/>
        <RenderSprite/>
        <SetRenderTarget/>
        <UseMaterial Material="BlendMaterial"/>
        <RenderSprite/>
      </OnRender>
    </Model>
    <RenderTarget Name="TempRenderTarget1" Width="0" Height="0"/>
    <RenderTarget Name="TempRenderTarget2" Width="0" Height="0"/>
    <Material Name="HorizontalMaterial" Light="0" ZBuffer="0" Shader="HorizontalShader">
      <Textures>
        <MaterialTexture RenderTarget="TempRenderTarget2" TexCoords="1"/>
      </Textures>
    </Material>
    <Shader Name="HorizontalShader">
      <VertexShaderSource>
<![CDATA[void main()
{
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
  gl_TexCoord[0] = gl_MultiTexCoord0;
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[//http://callumhay.blogspot.rs/2010/09/gaussian-blur-shader-glsl.html

uniform vec2 iResolution;
uniform sampler2D tex1;
uniform float amount;
const int horizontalPass = 1; // 0 or 1 to indicate vertical or horizontal pass
float sigma = amount / 4.0 + 0.001;          // The sigma value for the gaussian function: higher value means more blur
                            // A good value for 9x9 is around 3 to 5
                            // A good value for 7x7 is around 2.5 to 4
                            // A good value for 5x5 is around 2 to 3.5
                            // ... play around with this based on what you need :)

const float PI = 3.14159265;

void main() {
  vec2 p = gl_TexCoord[0].xy;
  float numBlurPixelsPerSide = amount / 2.0;

  vec2 blurMultiplyVec = 0 < horizontalPass ? vec2(1.0, 0.0) : vec2(0.0, 1.0);

  // Incremental Gaussian Coefficent Calculation (See GPU Gems 3 pp. 877 - 889)
  vec3 incrementalGaussian;
  incrementalGaussian.x = 1.0 / (sqrt(2.0 * PI) * sigma);
  incrementalGaussian.y = exp(-0.5 / (sigma * sigma));
  incrementalGaussian.z = incrementalGaussian.y * incrementalGaussian.y;

  vec4 avgValue = vec4(0.0, 0.0, 0.0, 0.0);
  float coefficientSum = 0.0;

  // Take the central sample first...
  avgValue += texture2D(tex1, p) * incrementalGaussian.x;
  coefficientSum += incrementalGaussian.x;
  incrementalGaussian.xy *= incrementalGaussian.yz;

  // Go through the remaining 8 vertical samples (4 on each side of the center)
  for (float i = 1.0; i <= numBlurPixelsPerSide; i++)
   {
    avgValue += texture2D(tex1, p - i / iResolution * blurMultiplyVec) * incrementalGaussian.x;
    avgValue += texture2D(tex1, p + i / iResolution * blurMultiplyVec) * incrementalGaussian.x;
    coefficientSum += 2.0 * incrementalGaussian.x;
  incrementalGaussian.xy *= incrementalGaussian.yz;
  }

  gl_FragColor = avgValue / coefficientSum;
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable Name="AmountHorizontal" VariableName="amount"/>
        <ShaderVariable VariableName="iResolution" VariableRef="uRes"/>
      </UniformVariables>
    </Shader>
    <Material Name="VerticalMaterial" Light="0" ZBuffer="0" Shader="VerticalShader">
      <Textures>
        <MaterialTexture RenderTarget="TempRenderTarget1" TexCoords="1"/>
      </Textures>
    </Material>
    <Shader Name="VerticalShader">
      <VertexShaderSource>
<![CDATA[void main()
{
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
  gl_TexCoord[0] = gl_MultiTexCoord0;
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[//http://callumhay.blogspot.rs/2010/09/gaussian-blur-shader-glsl.html

uniform vec2 iResolution;
uniform sampler2D tex1;
uniform float amount;
const int horizontalPass = 0; // 0 or 1 to indicate vertical or horizontal pass
float sigma = amount / 4.0 + 0.001;        // The sigma value for the gaussian function: higher value means more blur
                            // A good value for 9x9 is around 3 to 5
                            // A good value for 7x7 is around 2.5 to 4
                            // A good value for 5x5 is around 2 to 3.5
                            // ... play around with this based on what you need :)

const float PI = 3.14159265;

void main() {
  vec2 p = gl_TexCoord[0].xy;
  float numBlurPixelsPerSide = amount / 2.0;

  vec2 blurMultiplyVec = 0 < horizontalPass ? vec2(1.0, 0.0) : vec2(0.0, 1.0);

  // Incremental Gaussian Coefficent Calculation (See GPU Gems 3 pp. 877 - 889)
  vec3 incrementalGaussian;
  incrementalGaussian.x = 1.0 / (sqrt(2.0 * PI) * sigma);
  incrementalGaussian.y = exp(-0.5 / (sigma * sigma));
  incrementalGaussian.z = incrementalGaussian.y * incrementalGaussian.y;

  vec4 avgValue = vec4(0.0, 0.0, 0.0, 0.0);
  float coefficientSum = 0.0;

  // Take the central sample first...
  avgValue += texture2D(tex1, p) * incrementalGaussian.x;
  coefficientSum += incrementalGaussian.x;
  incrementalGaussian.xy *= incrementalGaussian.yz;

  // Go through the remaining 8 vertical samples (4 on each side of the center)
  for (float i = 1.0; i <= numBlurPixelsPerSide; i++)
   {
    avgValue += texture2D(tex1, p - i / iResolution * blurMultiplyVec) * incrementalGaussian.x;
    avgValue += texture2D(tex1, p + i / iResolution * blurMultiplyVec) * incrementalGaussian.x;
    coefficientSum += 2.0 * incrementalGaussian.x;
  incrementalGaussian.xy *= incrementalGaussian.yz;
  }

  avgValue /= coefficientSum;

  gl_FragColor = avgValue;
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable Name="AmountVertical" VariableName="amount"/>
        <ShaderVariable VariableName="iResolution" VariableRef="uRes"/>
      </UniformVariables>
    </Shader>
    <Material Name="FeedbackMaterial" Light="0" Blend="1" ZBuffer="0" Shader="FeedbackShader">
      <Textures>
        <MaterialTexture Name="FeedbackMaterialTexture" TexCoords="1"/>
      </Textures>
    </Material>
    <Shader Name="FeedbackShader">
      <VertexShaderSource>
<![CDATA[void main()
{
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
  gl_TexCoord[0] = gl_MultiTexCoord0;
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[// http://www.curious-creature.com/2007/02/20/fast-image-processing-with-jogl/

uniform sampler2D tex1;

void main()
{

  vec4 color;
  vec2 pos = gl_TexCoord[0].xy;
  color = texture2D(tex1,vec2(pos.x, pos.y));
  gl_FragColor = color;
}]]>
      </FragmentShaderSource>
    </Shader>
    <Material Name="BrightPassMaterial" Light="0" Blend="1" ZBuffer="0" Shader="BrightPassShader">
      <Textures>
        <MaterialTexture Name="BrightPassTexture" TexCoords="1"/>
      </Textures>
    </Material>
    <Shader Name="BrightPassShader">
      <VertexShaderSource>
<![CDATA[varying vec4 vertColor;
attribute vec4 color;

void main()
{
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
  vertColor = color;
  gl_TexCoord[0] = gl_MultiTexCoord0;
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[// https://github.com/cansik/processing-bloom-filter

uniform sampler2D tex1;
varying vec4 vertColor;

uniform float brightPassThreshold;

void main()
{
    vec2 pos = gl_TexCoord[0].xy;
    vec4 c = texture2D(tex1, pos);
	  vec3 luminanceVector = vec3(0.2126, 0.7152, 0.0722);

    float luminance = dot(luminanceVector, c.rgb);
    luminance = max(0.0, luminance - brightPassThreshold);
    c.rgb *= sign(luminance);

    gl_FragColor = c;
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable Name="brightPassThreshold" VariableName="brightPassThreshold"/>
      </UniformVariables>
    </Shader>
    <Material Name="BlendMaterial" Light="0" ZBuffer="0" Shader="BlendShader">
      <Textures>
        <MaterialTexture Name="orgTexture" TexCoords="1"/>
        <MaterialTexture Name="blendTexture" RenderTarget="TempRenderTarget2" TexCoords="1"/>
      </Textures>
    </Material>
    <Shader Name="BlendShader">
      <VertexShaderSource>
<![CDATA[void main()
{
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
  gl_TexCoord[0] = gl_MultiTexCoord0;
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[uniform sampler2D tex1;
uniform sampler2D tex2;
uniform float exposure;
uniform float gamma;
uniform float blend;
uniform float opacity;

vec4 Blend(vec4 c1, vec4 c2)
{
  vec4 c;

  c.a = c1.a + c2.a * (1.0 - c1.a);
  c.r = (c1.r * c1.a + c2.r * c2.a * (1.0 - c1.a)) / c.a;
  c.g = (c1.g * c1.a + c2.g * c2.a * (1.0 - c1.a)) / c.a;
  c.b = (c1.b * c1.a + c2.b * c2.a * (1.0 - c1.a)) / c.a;

  return c;
}

vec4 BlendOneOneMinusSourceAlpha(vec4 c1, vec4 c2)
{
  vec4 c;

  c.r = c1.r + (c2.r * (1.0 - c1.a));
  c.g = c1.g + (c2.g * (1.0 - c1.a));
  c.b = c1.b + (c2.b * (1.0 - c1.a));
  c.a = c1.a + (c2.a * (1.0 - c1.a));

  return c;
}

vec4 BlendAlphaOneMinusSourceAlpha(vec4 c1, vec4 c2)
{
  vec4 c;

  c.r = (c1.r * c1.a) + (c2.r * (1.0 - c1.a));
  c.g = (c1.g * c1.a) + (c2.g * (1.0 - c1.a));
  c.b = (c1.b * c1.a) + (c2.b * (1.0 - c1.a));
  c.a = (c1.a * c1.a) + (c2.a * (1.0 - c1.a));

  return c;
}

vec4 BlendOneOne(vec4 c1, vec4 c2)
{
  vec4 c;
  c = c1 + c2;
  return c;
}

void main()
{
    vec2 pos = gl_TexCoord[0].xy;

    float alpha = texture2D(tex1, pos).a;
    float alphaBloom = texture2D(tex2, pos).a;

    vec3 hdrColor = texture2D(tex1, pos).rgb;
    vec3 bloomColor = texture2D(tex2, pos).rgb;

    if (blend == 0.0)
    {
    // additive blending
    hdrColor += bloomColor * opacity;

    // tone mapping
    vec3 result = vec3(1.0) - exp(-hdrColor * exposure);

    // also gamma correct while we're at it
    result = pow(result, vec3(1.0 / gamma));

    gl_FragColor = BlendOneOne(vec4(bloomColor, alpha), vec4(result, alphaBloom));
    }
    else if (blend == 1.0)
    {
    // additive blending
    hdrColor += bloomColor * exposure;

    // tone mapping
    vec3 result = vec3(1.0) - exp(-hdrColor * opacity);

    // also gamma correct while we're at it
    result = pow(result, vec3(1.0 / gamma)) * 1.5;

    gl_FragColor = BlendOneOne(vec4(bloomColor, alpha), vec4(result, alphaBloom));
    }
    else
    {
    // Set gamma
		float gammaCorrection = 1.0 / gamma;

		bloomColor.r = pow(bloomColor.r, gammaCorrection);
		bloomColor.g = pow(bloomColor.g, gammaCorrection);
		bloomColor.b = pow(bloomColor.b, gammaCorrection);

    vec3 c = (hdrColor + bloomColor * exposure) * opacity;

    gl_FragColor = BlendOneOne(vec4(bloomColor, alpha), vec4(c, alphaBloom));
    }
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable Name="Exposure" VariableName="exposure"/>
        <ShaderVariable Name="Gamma" VariableName="gamma"/>
        <ShaderVariable Name="Blend" VariableName="blend"/>
        <ShaderVariable Name="Opacity" VariableName="opacity"/>
      </UniformVariables>
    </Shader>
  </Content>
</ZApplication>
