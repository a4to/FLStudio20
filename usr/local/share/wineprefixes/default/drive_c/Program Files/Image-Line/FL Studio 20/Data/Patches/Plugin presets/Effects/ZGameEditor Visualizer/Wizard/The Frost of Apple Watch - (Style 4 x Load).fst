FLhd   0 * ` FLdt�  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV �ߚ[�  ﻿[General]
GlWindowMode=1
LayerCount=16
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=3,2,15,0,7,8,11,12,13,4,5,6,10,1,14,9

[AppSet3]
App=Scenes\RhodiumLiquidCarbon
FParamValues=0,0.041,1,0,0.284,0.5,0.268,1,0.252,1
ParamValues=0,41,1000,0,284,500,268,1000,252,1000
ParamValuesCanvas effects\DarkSpark=232,0,1000,388,424,500,500,500,500,460,0,468
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPeak Effects\PeekMe=0,0,1000,768,396,500,500,230,656,328,0,1000
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesCanvas effects\Rain=928,500,1000,500,1000,500,500,500,500,436,844,500
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesScenes\Postcard=0,0,0,0,572,216,0,0,0,0,0
ParamValuesPhysics\Columns=200,1000,0,984,500,484,1000,800,208,450,336,232,368,500
ParamValuesScenes\Lantern=0,572,292,500,122,200,250,500,228,38
ParamValuesPeak Effects\Linear=0,0,1000,0,684,500,500,1000,0,500,0,220,512,1000,0,1000,0,452,500,1000,350,0,300,1,32,220,288,0,330,426,100
ParamValuesScenes\Cloud Ten=0,0,0,0,408,472,412,0,0,0,0
ParamValuesScenes\Mandelbulb=0,412,484,948,304,292,0,868,0,1000,1000,125,0,0,0,0,0,0
ParamValuesObject Arrays\CubesGrasping=0,0,1000,0,872,500,500,500,500,500,500
ParamValuesPeak Effects\SplinePeaks=0,20,950,0,500,492,628,0,0,0
ParamValuesImage effects\Image=0,0,0,972,1000,500,500,0,0,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,499,500,500,500,500,500
ParamValuesScenes\Musicball=0,0,1000,0,0,500,500,192,1000,0,548,48,368
Enabled=1
Collapsed=1
Name=EQ

[AppSet2]
App=Background\FourCornerGradient
FParamValues=7,1,0.08,1,1,0.926,1,1,0.184,1,1,0.738,1,1
ParamValues=7,1000,80,1000,1000,926,1000,1000,184,1000,1000,738,1000,1000
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesScenes\RhodiumLiquidCarbon=0,41,0,1000,1000,500,500,1000,700,1000
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,0,1000,500,500,0,500,500
ParamValuesPeak Effects\Polar=948,16,936,0,548,500,532,884,652,916,320,868,0,500,0,500,0,1000,1000,0,0,0,0,1000
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesParticles\fLuids=0,0,0,0,772,500,500,0,0,0,0,1000,702,0,0,250,500,0,158,500,860,316,0,0,500,0,0,250,500,0,193,0
ParamValuesPostprocess\ColorCyclePalette=1000,0,0,0,0,696,0,1000,0,250,772
ParamValuesObject Arrays\WaclawGasket=596,0,0,1000,448,500,500,500,500,331,156,1000,212,328,144
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPostprocess\Point Cloud Default=0,610,394,484,500,440,472,503,1000,725,144,0,0,0,0
ParamValuesObject Arrays\Rings=0,0,0,0,916,216,500,500,500,0,0,500,0,0,0
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesBackground\ItsFullOfStars=0,724,1000,1000,504,500,500,500,0,72,644
ParamValuesCanvas effects\Lava=896,0,0,900,1000,1000,500,500,500,500,500
ParamValuesFeedback\70sKaleido=0,708,576,0,296,516
ParamValuesObject Arrays\Filaments=8,1000,940,0,548,500,444,548,620,500,4,0,0,872
ParamValuesTerrain\CubesAndSpheres=0,0,1000,0,0,500,500,0,404,400,540,940,316,556,684,456,0,1000
ParamValuesImage effects\ImageSphinkter=0,684,1000,0,0,500,500,500,1000,500,500,276,0,1000,0
ParamValuesObject Arrays\DiamondBit=500,500,1000,0,750,500,500,0,500,500,0,500,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,109,500,500,500,500,500
ParamValuesPostprocess\Blur=656
Enabled=1
Collapsed=1
Name=FILTER COLOR

[AppSet15]
App=HUD\HUD Grid
AppVersion=1
FParamValues=0,0.5,0,0,0.5,0.5,1,1,0.5,4,0.5,1,0.5,1,0,0.5,1,0.1,0.5,0.3,0,1
ParamValues=0,500,0,0,500,500,1000,1000,500,4,500,1000,500,1000,0,500,1000,100,500,300,0,1
Enabled=0
Collapsed=1

[AppSet0]
App=Image effects\Image
FParamValues=0,0,0,0.874,0.848,0.5,0.35,0.7,0,0,0,0,1,0
ParamValues=0,0,0,874,848,500,350,700,0,0,0,0,1,0
ParamValuesCanvas effects\DarkSpark=0,408,892,480,1000,500,500,500,500,0,0,0
ParamValuesPostprocess\RGB Shift=1000,0,0,600,700,200
ParamValuesPostprocess\Blooming=0,0,0,912,500,600,296,317,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesBackground\SolidColor=0,0,0,900
ParamValuesParticles\BugTails=0,636,1000,0,500,500,500,0,0,0,0,0
ParamValuesCanvas effects\SkyOcean=0,659,708,0,0,442,916,1000,650,734,1000,899,1000
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPostprocess\Point Cloud Default=0,678,506,500,500,448,444,519,1000,485,156,0,264,0,333
ParamValuesImage effects\ImageWarp=0,0,241,0,0,473,529,78
ParamValuesBackground\ItsFullOfStars=0,0,0,0,0,1000,500,500,0,0,22
ParamValuesTerrain\CubesAndSpheres=0,0,0,0,0,644,1000,538,540,645,1000,581,558,1000,555,939,520,127
ParamValuesPeak Effects\JoyDividers=0,0,0,0,700,500,500,726,500,1000,600,650,510
ParamValuesObject Arrays\DiamondBit=500,500,500,0,658,500,500,0,500,500,1000,500,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,437,500,500,500,500,500
ParamValuesPostprocess\FrameBlur=868,228,1000,116,666,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesParticles\StrangeAcid=0,0,0,0,508,36,80,86,464,60,804,560,8,1000,92,540,1000,1000,0,0
ParamValuesPostprocess\Ascii=0,428,0,600,700,200
ParamValuesImage effects\ImageSphereWarp=708,500,750,1000,250,400,0,500,500,500
ParamValuesCanvas effects\Digital Brain=0,0,0,0,584,500,500,600,100,300,100,250,1000,500,1000
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesHUD\HUD Grid=724,500,0,820,500,500,1000,1000,1000,444,500,1000,500,0,0,500,1000,104,1000,0,0,1000
ParamValuesBlend\VideoAlphaKey=0,0,1000,0,1000,320,300,0,0,1000,250,500,500
ParamValuesMisc\FruityDanceLine=0,0,0,0,772,500,500,532,788,588,0,492,60
ParamValuesParticles\fLuids=0,0,0,0,0,500,500,432,380,104,500,500,500,0,604,346,500,0,576,500,500,500,0,0,500,0,0,0,500,0,0,0
ParamValuesPeak Effects\Reactive Sphere=0,0,893,0,381,500,500,252,252,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesCanvas effects\N-gonFigure=0,200,900,0,807,500,494,480,0,400,48
ParamValuesCanvas effects\ShimeringCage=0,0,0,0,0,500,500,0,0,323,0,134,173,0
ParamValuesBlend\BufferBlender=0,0,0,1000,0,0,0,0,0,0,750,0
ParamValuesCanvas effects\TaffyPulls=900,0,0,0,0,500,500,100,500,500,0,0,0,500
ParamValuesObject Arrays\Rings=784,0,0,1000,940,500,500,500,488,56,0,112,0,532,296
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesCanvas effects\FreqRing=490,846,1000,0,885,500,500,1000,1000,856,245,251,554,0
ParamValuesFeedback\70sKaleido=0,0,266,1000,595,743
ParamValuesPostprocess\Point Cloud High=0,1000,330,500,500,448,444,503,1000,625,156,0,516,0,0
ParamValuesObject Arrays\CubicMatrix=0,0,0,0,228,500,500,500,500,0,0,0,0
ParamValuesImage effects\ImageSphinkter=0,0,0,0,853,500,500,0,411,730,843,210,538,0,378
ParamValuesBackground\FogMachine=0,491,0,422,901,529,510,1000,0,1000,137,402
ParamValuesPeak Effects\ReflectedPeeks=500,308,1000,112,478,500,500,0,576
ParamValuesPostprocess\Blur=1000
ParamValuesTerrain\GoopFlow=0,704,1000,92,744,864,504,0,444,1000,152,592,1000,1000,536,592
ParamValuesCanvas effects\Stack Trace=374,80,1000,983,348,714,602
Enabled=1
Collapsed=1
ImageIndex=2
Name=BLOCKATOR 1

[AppSet7]
App=Image effects\Image
FParamValues=0,0,0,0.874,0.848,0.5,0.65,0.7,0,0,0,0,1,0
ParamValues=0,0,0,874,848,500,650,700,0,0,0,0,1,0
ParamValuesPostprocess\Point Cloud Low=0,490,330,500,500,448,444,503,158,625,156,0,0,0,0
ParamValuesCanvas effects\DarkSpark=500,0,500,1000,500,500,500,500,500,500,1000,1000
ParamValuesParticles\ColorBlobs=0,924,1000,0,700,500,500,350,1000,1000
ParamValuesPostprocess\RGB Shift=200,380,0,600,700,200
ParamValuesPeak Effects\PeekMe=0,716,1000,0,528,556,248,0,0,1000,796,20
ParamValuesPeak Effects\VectorScope=452,733,1000,0,1000,1000,667,0
ParamValuesPhysics\Cage=0,700,1000,0,932,500,500,0,1000,0
ParamValuesPostprocess\Blooming=0,964,1000,0,500,800,500,0,88
ParamValuesParticles\BugTails=0,0,0,0,948,500,500,0,0,0,0,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesBackground\ItsFullOfStars=676,764,1000,0,500,500,500,500,0,0,0
ParamValuesObject Arrays\Pentaskelion=0,0,1000,0,500,500,500,0,500,500,500,584,392,680
ParamValuesPeak Effects\JoyDividers=0,0,1000,1000,700,500,500,0,500,100,600,650,510
ParamValuesTunnel\Oblivion=0,212,1000,572,736,500,500,100,100,1000,500
ParamValuesObject Arrays\CrystalCube=440,608,832,940,968,500,500,612
ParamValuesFeedback\SphericalProjection=60,532,296,288,424,425,172,538,236,0,0,500,530,584,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesCanvas effects\BitPadZ=0,0,440,336,972,500,0,360,0,740,584,0,784,520,240,336,0,424,240,528,1000
ParamValuesObject Arrays\BallZ=0,888,1000,868,894,500,500,364,568,400,436,200,672
ParamValuesCanvas effects\Electric=0,0,0,976,148,500,500,0,100,1000,500
ParamValuesParticles\StrangeAcid=0,712,1000,476,1000,500,500,146,500,500,912,564,0,116,0,0,400,766,600,567
ParamValuesPostprocess\Ascii=0,1000,0,600,700,200
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesScenes\RhodiumLiquidCarbon=480,0,0,0,0,0,500,1000,584,1000
ParamValuesPeak Effects\Polar=284,16,1000,600,324,500,500,484,976,312,524,624,0,360,0,500,0,1000,1000,0,0,0,0,1000
ParamValuesPostprocess\ColorCyclePalette=0,375,250,0,200,508,1000,500,192,500,1000
ParamValuesParticles\fLuids=0,692,1000,0,836,500,500,0,0,0,500,500,0,0,500,250,500,0,0,500,500,500,0,0,500,0,0,250,500,0,0,0
ParamValuesPostprocess\ScanLines=0,200,0,0,0,0
ParamValuesScenes\Postcard=0,468,1000,0,500,500,0,0,0,0,0
ParamValuesCanvas effects\N-gonFigure=0,846,900,252,500,500,500,500,0,400,0
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPostprocess\Dot Matrix=500,500,500,0,1000,500,1000,1000
ParamValuesScenes\Lantern=0,452,1000,500,0,200,250,500,228,998
ParamValuesTunnel\TorusJourney=0,0,0,0,0,500,500,0,828,452,500,40,390,1000,500,60,800,1000,500,1000
ParamValuesPeak Effects\Linear=872,1000,0,1000,714,506,260,464,0,500,368,300,496,1000,500,0,0,500,500,0,0,0,1000,1,600,60,32,212,330,250,100
ParamValuesFeedback\70sKaleido=968,700,1000,1000,420,1000
ParamValuesFeedback\WormHoleDarkn=1000,680,840,372,348,544,40,500,404,448,500,500
ParamValuesObject Arrays\Filaments=764,1000,1000,0,500,500,500,500,500,500,0,0,500,0
ParamValuesObject Arrays\CubesGrasping=0,0,1000,1000,0,500,0,500,500,0,0
ParamValuesObject Arrays\CubicMatrix=0,776,1000,0,852,500,500,500,500,0,0,0,0
ParamValuesPostprocess\Point Cloud High=0,922,966,412,724,708,444,503,898,465,436,0,0,0,0
ParamValuesTerrain\GoopFlow=0,900,1000,0,164,60,908,108,100,300,480,564,904,376,508,148
ParamValuesBackground\FogMachine=0,744,1000,1000,1000,492,452,480,480,448,0,0
ParamValuesCanvas effects\Flaring=375,712,1000,364,1000,500,500,181,0,0,500
ParamValuesPostprocess\Blur=1000
ParamValuesScenes\Cloud Ten=752,692,432,464,428,1000,300,100,100,1000,500
ParamValuesCanvas effects\Flow Noise=0,508,500,720,1000,500,500,100,100,1000,500
Enabled=1
Collapsed=1
ImageIndex=2
Name=BLOCKATOR 2

[AppSet8]
App=Image effects\Image
FParamValues=0,0,0,0.874,0.848,0.214,0.65,0.7,0,0.25,0,0,1,0
ParamValues=0,0,0,874,848,214,650,700,0,250,0,0,1,0
ParamValuesCanvas effects\DarkSpark=644,584,1000,548,1000,500,500,500,500,0,0,0
ParamValuesPeak Effects\PeekMe=0,584,1000,516,592,600,128,0,1000,1000,500,812
ParamValuesPeak Effects\VectorScope=744,572,1000,916,696,796,333,0
ParamValuesParticles\BugTails=0,0,0,1000,500,500,500,0,0,0,0,0
ParamValuesCanvas effects\SkyOcean=0,792,1000,0,0,500,500,0,500,500,0,0,0
ParamValuesMisc\PentUp=600,724,1000,480,500,500,500,0,0,1000,60,0
ParamValuesObject Arrays\Pentaskelion=0,0,1000,0,844,500,500,916,500,500,500,0,0,0
ParamValuesObject Arrays\CrystalCube=0,828,1000,524,0,500,0,1000
ParamValuesCanvas effects\Electric=768,740,0,936,1000,500,500,528,100,1000,500
ParamValuesParticles\ReactiveFlow=0,125,0,1000,1000,0,500,100,500,500,500,500,500,0,0,500,200,500,125,500,500,500,1000,1000,100
ParamValuesCanvas effects\Digital Brain=912,0,1000,0,1000,500,500,600,100,300,100,250,1000,500,1000
ParamValuesPeak Effects\StereoWaveForm=0,408,12,1000,188,664,1000,144,1000,1000,272,96,196,500,372,324,500,348,180,500,500,1000,448,0
ParamValuesPeak Effects\WaveSimple=632,556,1000,0,820,500,500,368,0
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,1000,556,336,500,756,0,724
ParamValuesCanvas effects\Rain=500,500,1000,0,500,500,500,500,1000,168,1000,1000
ParamValuesCanvas effects\ShimeringCage=0,888,1000,0,0,500,500,0,0,0,0,0,0,0
ParamValuesFeedback\FeedMe=0,0,0,1000,500,1000,276
ParamValuesObject Arrays\Rings=944,0,0,1000,1000,500,500,500,1000,0,0,0,664,1000,0
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesCanvas effects\Lava=820,556,1000,792,32,500,500,500,0,580,500
ParamValuesCanvas effects\FreqRing=500,0,1000,500,500,500,500,500,1000,312,123,500,500,500
ParamValuesFeedback\70sKaleido=0,0,0,1000,520,500
ParamValuesPostprocess\Point Cloud High=0,330,330,508,500,512,444,503,330,625,156,0,0,0,0
ParamValuesBackground\FogMachine=800,572,1000,936,500,500,500,500,500,500,0,0
ParamValuesBackground\Grid=650,0,8,0,1000,552,1000,0,0,0
ParamValuesParticles\ColorBlobs=660,0,1000,0,84,500,500,350,1000,0
ParamValuesPostprocess\Blooming=0,0,0,876,328,724,716,452,0
ParamValuesPhysics\Cage=600,676,300,0,800,500,500,500,500,500
ParamValuesObject Arrays\8x8x8_Eggs=0,1000,1000,0,0,500,500,200,500,500,0,0,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPostprocess\Point Cloud Default=0,38,330,500,500,32,444,503,330,625,680,0,0,0,0
ParamValuesPeak Effects\JoyDividers=0,0,0,0,0,1000,1000,104,500,100,680,626,0
ParamValuesFeedback\SphericalProjection=0,308,552,184,1000,1000,0,0,0,0,1000,333,0,204,276,240,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,909,500,500,500,500,500
ParamValuesFeedback\FeedMeFract=0,0,1000,1000,444,0
ParamValuesBackground\FourCornerGradient=400,112,812,1000,1000,834,1000,1000,588,1000,1000,642,1000,1000
ParamValuesPostprocess\Ascii=0,380,0,600,700,200
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPeak Effects\Polar=0,1000,1000,0,460,468,500,1000,0,1000,316,760,1000,584,0,500,0,1000,1000,0,0,0,0,1000
ParamValuesParticles\fLuids=0,636,1000,0,500,528,460,0,0,0,500,500,500,0,500,250,500,0,0,500,500,500,0,0,500,0,0,250,500,0,0,0
ParamValuesPeak Effects\Reactive Sphere=0,260,893,0,773,500,500,0,0,1000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesFeedback\BoxedIn=0,0,0,1000,500,468,296,500,648,500
ParamValuesCanvas effects\N-gonFigure=300,18,900,400,0,496,500,736,0,288,0
ParamValuesCanvas effects\OverlySatisfying=856,24,1000,0,600,500,500,100,100,1000,500
ParamValuesPostprocess\Dot Matrix=276,500,500,0,0,500,1000,1000
ParamValuesScenes\Lantern=932,572,1000,500,122,200,250,500,228,38
ParamValuesFeedback\WormHoleDarkn=1000,44,0,1000,60,500,500,500,500,500,500,500
ParamValuesPeak Effects\Linear=0,587,1000,0,106,502,344,896,0,500,0,724,496,0,0,456,0,500,500,1000,350,0,1000,1000,424,0
ParamValuesCanvas effects\Flaring=764,1000,1000,1000,1000,500,500,181,0,400,500
ParamValuesMisc\FruityIndustry=0,612,1000,0,820,500,500,0,956,468,460,500,316
ParamValuesPeak Effects\ReflectedPeeks=904,8,1000,584,1000,500,500,500,812
ParamValuesPostprocess\Blur=0
ParamValuesTerrain\GoopFlow=984,0,0,0,500,500,500,0,500,500,500,500,500,500,500,500
ParamValuesCanvas effects\Flow Noise=548,600,1000,0,1000,0,256,40,100,1000,500
Enabled=1
Collapsed=1
ImageIndex=2
Name=BLOCKATOR 3

[AppSet11]
App=Image effects\Image
FParamValues=0,0,0,0.874,0.848,0.788,0.65,0.7,0,0.25,0,0,1,0
ParamValues=0,0,0,874,848,788,650,700,0,250,0,0,1,0
ParamValuesFeedback\WarpBack=500,0,0,0,476,500,68
ParamValuesCanvas effects\DarkSpark=500,0,500,500,0,500,500,500,500,500,284,540
ParamValuesPeak Effects\PeekMe=0,0,1000,0,500,500,500,230,1000,260,356,676
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPostprocess\Point Cloud Default=0,958,498,500,500,500,512,503,418,625,156,0,0,0,0
ParamValuesBackground\ItsFullOfStars=320,0,1000,0,636,500,500,500,0,0,0
ParamValuesPeak Effects\JoyDividers=0,0,0,1000,700,0,500,1000,500,1000,508,650,750
ParamValuesFeedback\SphericalProjection=60,0,0,0,750,425,500,590,0,500,0,333,530,828,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,347,500,500,500,500,500
ParamValuesFeedback\FeedMeFract=0,0,0,1000,564,1000
ParamValuesParticles\StrangeAcid=0,0,0,0,412,527,516,50,500,500,580,200,452,200,0,436,212,690,624,327
ParamValuesBackground\FourCornerGradient=0,32,0,1000,1000,250,1000,1000,556,1000,1000,1000,1000,1000
ParamValuesParticles\ReactiveMob=0,868,1000,496,88,732,692,332,0,384,256,576,456,372,872,696,296,248,340,320,332,424,1000,1000,244,24,500,196,0,1000
ParamValuesPeak Effects\StereoWaveForm=136,776,1000,1000,708,1000,688,128,528,664,876,1000,500,408,160,500,500,1000,1000,0,1000,1000,0,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesFeedback\WormHoleEclipse=604,0,0,128,0,500,0,20,632,500,500
ParamValuesPeak Effects\Polar=0,0,0,0,596,500,500,752,1000,140,268,440,0,500,0,500,0,1000,1000,0,0,0,0,1000
ParamValuesPostprocess\Edge Detect=384,0,0,1000,176,480
ParamValuesPeak Effects\Reactive Sphere=0,0,1000,0,709,500,500,84,524,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesFeedback\BoxedIn=0,0,0,1000,688,468,180,0,0,708
ParamValuesObject Arrays\Rings=0,864,1000,296,500,500,428,604,500,0,0,500,0,0,0
ParamValuesFeedback\FeedMe=0,0,0,1000,848,1000,0
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPostprocess\Dot Matrix=1000,108,500,28,0,0,176,1000
ParamValuesFeedback\70sKaleido=0,332,1000,1000,0,500
ParamValuesFeedback\WormHoleDarkn=0,0,0,1000,1000,500,500,0,580,376,500,500
ParamValuesPostprocess\Point Cloud High=0,1000,888,536,500,636,428,783,474,625,156,1000,456,0,333
ParamValuesScenes\Cloud Ten=216,436,528,716,736,660,300,100,100,1000,500
ParamValuesPeak Effects\ReflectedPeeks=0,692,1000,0,200,500,500,300,684
ParamValuesBackground\Grid=0,616,1000,1000,524,192,756,0,0,0
ParamValuesPostprocess\Blur=1000
ParamValuesTerrain\GoopFlow=0,588,1000,0,416,372,692,68,468,632,500,156,637,492,588,624
Enabled=1
Collapsed=1
ImageIndex=2
Name=BLOCKATOR 4

[AppSet12]
App=Image effects\Image
FParamValues=0,0,0,0.874,0.04,0.786,0.5,0.72,0.084,0.25,0,0,1,0
ParamValues=0,0,0,874,40,786,500,720,84,250,0,0,1,0
ParamValuesPeak Effects\StereoWaveForm=32,548,1000,292,959,1000,956,720,0,1000,892,1000,500,756,500,500,1000,484,864,0,192,1000,0,0
ParamValuesParticles\ColorBlobs=0,0,0,0,860,500,500,350,1000,148
ParamValuesPeak Effects\PeekMe=0,0,1000,1000,500,500,500,230,0,0,0,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPostprocess\ScanLines=0,357,52,672,0,0
ParamValuesPeak Effects\Polar=0,0,1000,0,452,500,500,292,500,336,492,936,1000,500,0,656,0,1000,1000,0,0,0,0,1000
ParamValuesPeak Effects\WaveSimple=0,0,0,0,500,500,508,500,476
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPeak Effects\JoyDividers=0,576,1000,0,700,500,500,0,500,100,664,494,886
ParamValuesPeak Effects\Linear=0,964,648,0,1000,500,288,388,0,500,260,350,0,0,500,0,0,500,500,500,500,828,500,152,200,0,32,212,330,250,100
ParamValuesTerrain\GoopFlow=0,760,852,776,620,500,500,40,476,1000,256,520,328,908,500,80
ParamValuesBackground\FourCornerGradient=467,1000,836,640,644,0,612,424,944,1000,1000,48,704,1000
Enabled=1
Collapsed=1
ImageIndex=2
Name=BLOCKATOR 5

[AppSet13]
App=Image effects\Image
FParamValues=0,0,0,0.874,0.04,0.216,0.5,0.72,0.084,0.25,0,0,1,0
ParamValues=0,0,0,874,40,216,500,720,84,250,0,0,1,0
ParamValuesFeedback\WarpBack=500,0,0,0,0,500,200
ParamValuesHUD\HUD Graph Radial=0,500,0,1000,500,500,342,444,500,0,1000,536,812,0,234,568,628,364,172,500,40,1000
ParamValuesPostprocess\RGB Shift=180,208,0,600,700,200
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesBackground\SolidColor=1000,0,0,1000
ParamValuesImage effects\ImageSlices=0,0,0,1000,948,500,500,0,0,4,500,1000,1000,500,0,0,0
ParamValuesObject Arrays\8x8x8_Eggs=0,564,1000,0,380,596,444,200,388,500,1000,492,416
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesMisc\PentUp=684,603,0,0,268,508,220,0,656,340,1000,0
ParamValuesPostprocess\Point Cloud Default=576,970,814,496,492,448,444,503,330,625,144,0,0,0,0
ParamValuesPostprocess\AudioShake=164,0,333,544,100,900
ParamValuesImage effects\ImageWarp=0,868,1000,0,640,500,476,20
ParamValuesPeak Effects\JoyDividers=0,0,0,0,700,500,500,0,500,100,0,594,1000
ParamValuesPostprocess\Youlean Pixelate=512,0,0,0
ParamValuesObject Arrays\BallZ=784,0,1000,1000,408,500,268,182,1000,748,500,1000,500
ParamValuesPostprocess\FrameBlur=736,544,616,480,944,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesBackground\FourCornerGradient=0,64,740,1000,1000,698,1000,1000,568,1000,1000,750,1000,1000
ParamValuesPostprocess\Ascii=0,224,804,600,700,200
ParamValuesHUD\HUD Prefab=5,0,500,0,0,500,500,278,1000,1000,444,0,500,0,652,0,1000,1000
ParamValuesPeak Effects\StereoWaveForm=32,548,1000,292,959,1000,956,720,0,1000,892,1000,500,756,500,500,1000,484,864,0,500,1000,0,0
ParamValuesImage effects\ImageSphereWarp=616,500,750,1000,250,400,0,500,500,500
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesHUD\HUD Graph Linear=0,500,0,0,500,500,1000,1000,898,444,500,1000,0,1000,0,500,200,0,0,0,500,1000
ParamValuesPostprocess\ColorCyclePalette=1,375,188,333,933,444,876,1000,320,0,44
ParamValuesHUD\HUD Grid=0,500,0,0,500,500,1000,1000,1000,444,500,1000,500,100,0,500,1000,100,196,300,0,1000
ParamValuesPostprocess\ScanLines=2,143,0,0,0,0
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,0,688,188,500,440,1000,464
ParamValuesFeedback\BoxedIn=0,0,0,1000,0,580,352,500,1000,132
ParamValuesPeak Effects\Polar=624,0,992,708,368,500,500,200,1000,752,332,732,1000,516,0,500,0,1000,1000,0,0,0,0,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesInternal controllers\Peak Band Controller=212,1000,420,1000,0,676,448,572,0,624,0,652,1000,628,0,664,1000,0,0,0,0,0,0,0,0,0
ParamValuesFeedback\70sKaleido=0,0,0,1000,444,704
ParamValuesPeak Effects\Linear=0,964,648,0,606,500,288,388,0,500,260,350,0,0,500,0,0,500,500,500,500,0,500,152,200,0,32,212,330,250,100
ParamValuesPostprocess\Point Cloud High=0,662,462,484,488,224,928,535,584,601,0,0,568,0,0
ParamValuesBackground\Grid=434,0,0,1000,596,596,1000,0,0,0
ParamValuesPostprocess\Blur=1000
ParamValuesTerrain\GoopFlow=516,616,1000,608,252,452,440,0,500,500,500,364,500,500,500,500
Enabled=1
BufferRenderQuality=4
Collapsed=1
ImageIndex=2
Name=BLOCKATOR 6

[AppSet4]
App=Image effects\Image
FParamValues=0,0,0,0.78,0.804,0.458,0.5,0,0,0,0,1,0,0
ParamValues=0,0,0,780,804,458,500,0,0,0,0,1,0,0
ParamValuesFeedback\WarpBack=500,0,744,0,500,500,1000
ParamValuesParticles\ColorBlobs=808,1000,0,0,700,500,500,686,0,240
ParamValuesPostprocess\RGB Shift=1000,372,0,600,700,200
ParamValuesPeak Effects\PeekMe=0,0,1000,0,500,500,500,1000,1000,1000,0,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesPostprocess\Blooming=0,0,0,1000,500,840,504,928,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesMisc\PentUp=0,0,500,500,500,500,552,500,0,0,0,0
ParamValuesPostprocess\Point Cloud Default=0,602,290,496,504,448,504,503,1000,625,152,0,0,0,0
ParamValuesPostprocess\BufferBlender=0,0,267,756,750,0,0,0,500,500,750
ParamValuesPostprocess\AudioShake=36,0,0,600,700,200
ParamValuesObject Arrays\DiamondBit=500,500,500,0,750,500,236,0,500,500,0,500,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,490,500,500,500,500,500
ParamValuesBackground\FourCornerGradient=400,1000,0,1000,1000,250,1000,1000,500,1000,1000,750,1000,1000
ParamValuesPostprocess\Ascii=0,0,696,600,700,200
ParamValuesParticles\ReactiveMob=0,500,500,500,500,500,500,1000,500,500,500,500,500,500,500,500,200,50,125,0,500,500,1000,1000,180,100,500,0,0,0
ParamValuesImage effects\ImageSphereWarp=0,0,750,1000,126,400,0,500,500,500
ParamValuesPeak Effects\StereoWaveForm=0,72,1000,1000,1000,0,0,648,816,1000,900,1000,500,528,500,500,500,328,0,48,1000,1000,0,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPostprocess\ColorCyclePalette=500,0,0,0,0,732,0,500,0,0,0
ParamValuesPostprocess\ScanLines=400,0,0,0,0,0
ParamValuesObject Arrays\Rings=0,0,0,1000,540,500,436,500,500,0,0,0,0,0,0
ParamValuesCanvas effects\OverlySatisfying=836,740,1000,0,600,500,500,100,100,1000,500
ParamValuesFeedback\FeedMe=0,0,832,492,304,432,692
ParamValuesPhysics\Columns=200,300,0,1000,676,0,164,1000,1000,450,604,312,500,500
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,0,344,876,184,0,160,536,564
ParamValuesCanvas effects\Flaring=0,1000,1000,1000,844,500,500,181,0,400,500
ParamValuesTerrain\GoopFlow=0,80,1000,0,312,468,1000,0,524,484,512,440,1000,1000,288,500
ParamValuesPostprocess\Blur=1000
Enabled=1
Collapsed=1
Name=Watch 1

[AppSet5]
App=Image effects\Image
FParamValues=0,0,0,0.78,0.836,0.503,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,780,836,503,500,0,0,0,0,0,0,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesImage effects\ImageSlices=396,568,1000,0,660,500,500,0,0,0,0,333,340,500,0,0,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPostprocess\Point Cloud Default=0,330,330,500,500,448,444,503,1000,1000,156,0,0,0,0
ParamValuesTerrain\CubesAndSpheres=0,1000,0,0,0,500,500,650,540,400,1000,1000,1000,1000,1000,1000,0,1000
ParamValuesObject Arrays\DiamondBit=500,696,1000,0,702,500,500,480,0,152,624,8,304
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,89,500,500,500,500,500
ParamValuesCanvas effects\Electric=928,740,484,780,1000,500,500,100,100,1000,500
ParamValuesImage effects\ImageSphereWarp=0,1000,1000,980,6,400,0,304,668,500
ParamValuesCanvas effects\Digital Brain=920,0,0,0,1000,500,500,600,100,300,100,250,1000,500,1000
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesScenes\RhodiumLiquidCarbon=480,20,1000,0,0,0,788,1000,1000,1000
ParamValuesFeedback\WormHoleEclipse=0,0,612,1000,576,56,0,444,444,500,500
ParamValuesCanvas effects\N-gonFigure=0,654,900,1000,868,500,500,500,500,0,684
ParamValuesCanvas effects\ShimeringCage=0,0,0,0,0,500,500,0,0,0,0,1000,712,0
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPostprocess\Dot Matrix=500,0,500,320,652,1000,1000,1000
ParamValuesCanvas effects\Lava=884,0,1000,0,1000,500,500,0,0,0,500
ParamValuesCanvas effects\FreqRing=0,632,1000,0,932,500,500,48,92,192,579,100,0,0
ParamValuesFeedback\WormHoleDarkn=0,0,800,0,1000,500,500,1000,500,428,500,500
ParamValuesPostprocess\Point Cloud High=0,330,330,500,500,448,444,503,1000,1000,156,0,828,0,0
ParamValuesImage effects\ImageSphinkter=0,0,0,1000,200,500,500,500,500,500,500,500,500,0,0
ParamValuesCanvas effects\Flaring=784,0,0,1000,1000,500,500,181,0,0,500
ParamValuesTerrain\GoopFlow=0,0,0,0,0,500,492,0,540,512,500,468,500,0,500,500
ParamValuesCanvas effects\Flow Noise=364,628,1000,644,1000,500,500,32,100,1000,500
Enabled=1
Collapsed=1
Name=Watch 2

[AppSet6]
App=Image effects\Image
FParamValues=0,0,0,0.78,0.804,0.544,0.5,0,0,0,0,1,0,0
ParamValues=0,0,0,780,804,544,500,0,0,0,0,1,0,0
ParamValuesFeedback\WarpBack=500,628,0,0,500,384,40
ParamValuesCanvas effects\DarkSpark=500,0,500,500,176,500,500,500,500,500,0,0
ParamValuesPostprocess\RGB Shift=156,0,0,600,700,200
ParamValuesParticles\ColorBlobs=0,0,1000,1000,700,500,500,350,1000,148
ParamValuesPostprocess\Blooming=0,0,0,428,500,652,0,1000,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesObject Arrays\8x8x8_Eggs=0,0,1000,0,0,500,500,200,500,500,0,0,0
ParamValuesText\TextTrueType=292,0,0,0,0,500,500,0,0,0,500
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPostprocess\Point Cloud Default=0,770,370,836,480,0,444,455,106,625,0,0,96,0,0
ParamValuesPostprocess\AudioShake=20,0,0,600,700,200
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,758,500,500,500,500,500
ParamValuesImage effects\ImageMashup=0,200,0,0,0,460,0
ParamValuesBackground\FourCornerGradient=133,48,828,1000,792,634,1000,1000,0,632,1000,642,776,1000
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPeak Effects\WaveSimple=0,0,0,0,0,500,500,452,0
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,116,516,140,604,604,168,0
ParamValuesPeak Effects\Polar=0,0,1000,0,64,500,380,500,500,500,500,500,0,500,0,500,0,1000,1000,0,0,0,0,1000
ParamValuesPostprocess\ColorCyclePalette=1000,0,0,0,0,364,716,500,0,0,0
ParamValuesParticles\fLuids=0,596,1000,0,812,500,500,28,0,0,500,500,500,0,500,250,148,688,408,500,500,500,0,0,500,0,0,250,500,0,0,0
ParamValuesMisc\FruityDanceLine=0,584,1000,0,500,500,500,416,300,316,0,424,80
ParamValuesFeedback\BoxedIn=0,0,0,0,360,328,0,160,0,524
ParamValuesObject Arrays\WaclawGasket=0,0,0,1000,140,500,500,500,500,1000,504,500,500,500,500
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPostprocess\Dot Matrix=280,500,500,0,792,500,1000,1000
ParamValuesFeedback\WormHoleDarkn=92,0,896,1000,1000,180,0,1000,0,1000,500,500
ParamValuesPostprocess\Point Cloud High=0,1000,702,488,420,436,400,503,1000,593,244,1000,0,0,0
ParamValuesBackground\FogMachine=484,72,1000,0,500,500,500,500,500,952,368,524
ParamValuesCanvas effects\Flaring=604,188,1000,0,1000,500,500,181,0,400,500
Enabled=1
Collapsed=1
Name=Watch 3

[AppSet10]
App=Text\TextTrueType
FParamValues=0.536,0,0,1,0,0.5,0.499,0,0,0,0.5
ParamValues=536,0,0,1000,0,500,499,0,0,0,500
ParamValuesParticles\ColorBlobs=944,0,0,0,700,500,500,350,1000,148
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPostprocess\Blooming=0,0,0,412,500,632,500,60,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesPeak Effects\Polar=456,980,1000,0,356,500,500,200,1000,568,352,556,0,500,0,500,0,1000,1000,0,0,0,0,1000
ParamValuesPostprocess\ColorCyclePalette=1000,0,0,0,0,464,0,0,0,0,0
ParamValuesPostprocess\RGB Shift=324,512,0,600,700,200
ParamValuesPostprocess\ScanLines=0,600,0,0,0,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesObject Arrays\Rings=0,0,0,1000,500,500,500,500,500,0,0,1000,0,0,0
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPeak Effects\Linear=0,511,565,1000,390,392,440,1000,0,500,0,500,0,0,0,500,0,500,500,1000,350,0,1000,1000,500,0
ParamValuesPostprocess\Point Cloud High=0,318,286,500,464,448,444,503,330,625,156,0,0,0,0
ParamValuesBackground\FogMachine=484,72,1000,0,500,500,500,500,500,952,368,524
ParamValuesImage effects\Image=0,0,0,0,0,500,500,0,0,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,855,500,500,500,500,500
ParamValuesBackground\FourCornerGradient=0,32,140,812,1000,170,1000,1000,524,1000,1000,232,652,1000
ParamValuesPostprocess\Blur=1000
ParamValuesTerrain\GoopFlow=504,564,1000,0,500,500,500,36,500,500,500,0,500,500,500,500
Enabled=1
Collapsed=1
Name=TEXT MAIN

[AppSet1]
App=Text\TextTrueType
FParamValues=0.176,0,0,0,0,0.5,0.5,0,0,0,0.5
ParamValues=176,0,0,0,0,500,500,0,0,0,500
Enabled=1
Collapsed=1

[AppSet14]
App=Postprocess\Youlean Color Correction
FParamValues=0.52,0.5,0.506,0.5,0.5,0.456
ParamValues=520,500,506,500,500,456
ParamValuesHUD\HUD Graph Radial=0,20,1000,0,500,500,362,444,500,0,1000,536,812,388,234,568,376,0,0,0,0,1000
ParamValuesBackground\FourCornerGradient=800,64,480,508,776,590,720,656,644,628,684,558,800,696
ParamValuesPostprocess\Point Cloud Default=0,330,330,500,520,472,496,503,330,809,284,0,0,0,333
ParamValuesBackground\SolidColor=0,696,108,984
Enabled=1
Collapsed=1
Name=Filter Color

[AppSet9]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
ParamValuesFeedback\WarpBack=500,0,0,0,0,500,200
ParamValuesParticles\ColorBlobs=0,612,1000,1000,440,712,496,1000,16,140
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesImage effects\ImageSlices=0,0,0,1000,948,500,500,0,0,4,500,1000,1000,500,0,0,0
ParamValuesObject Arrays\8x8x8_Eggs=0,564,1000,0,380,596,444,200,388,500,1000,492,416
ParamValuesCanvas effects\SkyOcean=780,496,1000,0,92,432,272,1000,756,668,244,496,588
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesMisc\PentUp=684,603,0,0,268,508,220,0,656,340,1000,0
ParamValuesPostprocess\Point Cloud Default=576,970,814,496,492,448,444,503,330,625,144,0,0,0,0
ParamValuesImage effects\ImageWarp=0,868,1000,0,640,500,476,20
ParamValuesPeak Effects\JoyDividers=0,0,0,0,700,500,500,0,500,100,0,594,1000
ParamValuesImage effects\Image=1000,0,0,0,1000,500,500,0,0,0,0,0,0,0
ParamValuesObject Arrays\BallZ=240,0,0,772,722,412,504,818,292,568,560,940,148
ParamValuesPostprocess\FrameBlur=736,544,616,480,944,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesBackground\FourCornerGradient=0,64,740,1000,1000,698,1000,1000,568,1000,1000,750,1000,1000
ParamValuesImage effects\ImageSphereWarp=616,500,750,1000,250,400,0,500,500,500
ParamValuesParticles\ReactiveFlow=0,125,500,0,680,684,592,984,76,772,220,404,1000,0,712,500,200,500,573,500,688,492,1000,1000,144
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,0,688,188,500,440,1000,464
ParamValuesPeak Effects\Polar=624,0,992,708,368,500,500,200,1000,752,332,732,1000,516,0,500,0,1000,1000,0,0,0,0,1000
ParamValuesPostprocess\ColorCyclePalette=1000,0,0,0,0,1000,0,432,0,0,140
ParamValuesCanvas effects\Rain=0,500,500,1000,1000,792,844,776,628,496,500,500
ParamValuesObject Arrays\Rings=0,0,0,0,456,500,500,500,500,0,0,500,0,0,0
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesInternal controllers\Peak Band Controller=212,1000,420,1000,0,676,448,572,0,624,0,652,1000,628,0,664,1000,0,0,0,0,0,0,0,0,0
ParamValuesPeak Effects\Linear=0,207,1000,0,90,502,344,896,0,500,0,724,496,0,0,456,0,500,500,1000,350,0,1000,1000,424,0
ParamValuesPostprocess\Point Cloud High=0,662,462,484,488,224,928,535,584,601,0,0,568,0,0
ParamValuesObject Arrays\CubicMatrix=0,0,0,0,560,556,500,500,1000,352,496,620,836
ParamValuesBackground\Grid=434,0,0,1000,596,596,1000,0,0,0
ParamValuesPostprocess\Blur=1000
ParamValuesTerrain\GoopFlow=516,616,1000,608,252,452,440,0,500,500,500,364,500,500,500,500
Enabled=1
Collapsed=1
Name=Fade in-out

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""4"" y=""5""><p><font face=""American-Captain"" size=""8"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""4"" y=""11""><p><font face=""Chosence-Bold"" size=""6"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""4"" y=""20""><p> <font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[comment]</font></p></position>"
Images="[plugpath]Content\Bitmaps\Vector art\Apple Watch Black.ilv","[plugpath]Content\Bitmaps\Vector art\Apple Watch White.ilv"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

