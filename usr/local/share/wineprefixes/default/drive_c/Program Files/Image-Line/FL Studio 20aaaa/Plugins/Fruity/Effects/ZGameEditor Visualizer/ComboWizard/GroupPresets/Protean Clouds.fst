FLhd   0  0 FLdt$  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   հ�  ﻿[General]
GlWindowMode=1
LayerCount=8
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=2,6,5,4,3,0,1,7

[AppSet2]
App=Peak Effects\Youlean Waveform
FParamValues=0,0,0,0,0.186,0.5,0.148,0.5,0.276,0.25,0.5,0.138,0.5,0,0.1,0,1,1,16,0.088,0,0.5,0.1,0.1,0,0,0.282,0.5,0.5,1,0,1
ParamValues=0,0,0,0,186,500,148,500,276,250,500,138,500,0,100,0,1,1,16,88,0,500,100,100,0,0,282,500,500,1,0,1000
ParamValuesImage effects\Image=0,0,0,0,1000,500,500,0,0,0,1000,0,0,0
ParamValuesPeak Effects\SplinePeaks=940,0,0,0,900,500,360,0,0,0
ParamValuesPeak Effects\Stripe Peeks=0,0,0,0,0,0,74,500,220,0,250,500,700,0,500,150,300,200,200,300,1000,0,0
Enabled=1
ImageIndex=2
Name=Section2

[AppSet6]
App=HUD\HUD 3D
FParamValues=0,0,0.5059,0.4596,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,0
ParamValues=0,0,505,459,500,500,500,500,500,500,500,500,500,500,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,0
Enabled=1
ImageIndex=2
Name=ForegroundMod

[AppSet5]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
Name=TextFst

[AppSet4]
App=Text\TextTrueType
FParamValues=0,0.511,0.298,0,0,0.5,0.5,0,0,0,0.5
ParamValues=0,511,298,0,0,500,500,0,0,0,500
Enabled=0
Name=Text

[AppSet3]
App=Postprocess\Youlean Drop Shadow
FParamValues=0.5,0.2,0,1,0.02,0.875,0
ParamValues=500,200,0,1000,20,875,0
Enabled=1
UseBufferOutput=1

[AppSet0]
App=Scenes\Protean Clouds
FParamValues=0,0.5,0,0.5,0.75,1,0.5,0.5,1
ParamValues=0,500,0,500,750,1000,500,500,1000
ParamValuesImage effects\Image=0,0,0,0,1000,500,500,0,0,0,1000,0,0,0
Enabled=1
Name=Section0

[AppSet1]
Enabled=1
Name=Section1

[AppSet7]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
ImageIndex=1

[Video export]
VideoH=1350
VideoW=1080
VideoRenderFps=30
SampleRate=48000
VideoCodecName=
AudioCodecName=(default)
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=C:\Users\spyro\Documents\Image-Line\FL Studio\Projects\LollieVox - Optimum Momentum_3.mp4
Bitrate=13000000
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Images="@EmbeddedFst:""Name=Neon marquee.fst"",Data=7801C5944B6EDB301086F7067C07C3AB167052BD1F4118C096A2A6A8D3A471932CECA060AD49C24212058A4AE3B375D123F50AFD293FD22CBA48BB2804499C21E71FF203677E7EFF317F4B15295EDCF47B6F8B6B51E5F2DBA9CC89D9FDDE94AF4825B2AD3473FBBDEC7C669CA72217E75269B60763DCD4B4D4175C0BC9ECE020DEC49CA99C14B346F6C8E9F7FABDF9B8AE67A42DA4C0889D5CA60BBC834FF4A88D2C57BCBCE2454B0D42AC7D7F84AFF95B4E885F10C4CE931733BE0DD38B437891C0C5DBC5747118770FB6F65CD6B7D613101D4171B4B58D178290DBAA59586966D72B6C63EE348F2BFEA5A07CC7E65C8907AE29E59AB330B2ECCC4DD2244B264E96C44996BA56EA8DBD1456EAC54E923AE389374E264168475E0C61CBF126C7561884BF41B25F0AC90A70FE280EB7185CDB80318F6766DCC83140FF029435826A87E1992404B77AFF88EAD88B92C49BA4F69F31F96B4CC7C9D80A02DCC1DD5D725E86C98B63173062DF070C7353F6232FEA206D30396E68EC176382F008AA1DA6679210DCEAFD174C57222739A0C71AA50A569D79C26CD7B736C635B3AD686B5C50858ACDEA86B970CD78591784AA26E645D8FD2622415B587EE02531D4719B0BF9E47895D32D6F0BFD7AB3F463CB0BA1570CA1A7FCF13DAD32D438CD6ABE14D51D7218CD4C145419B5E46071D9906A164DBD527291CA655B52A59BC5BB92DFD1DE5454B4C8A68399363917E74A7E45CF691653591482AEE4E3606F70566B51B6E5E0549AC8B6FCECEE97B5D7EF4D8456E618B649D965ED36BE733BEBE35D564B59D68A9A06C56D00B435F6D35130266E9DD960222B0D75D0349D8BCDB5D005DD8CE6BCD5F752610011931E0B4E7459B0E1613D0086BB8A0D874BF8490D8747738DD80247BA397C531F0D37BC203F5B554B930C58B0D2605A674E49F3E53DE526ADAC991BE35853BAD53813DAEDB5C8F53D0B220C4F48DCDD6BE687EB766276AB808814222F289F3E14CCF10374794554EDAC09FAEECE98B6E56EDCEFFD02203CA758"
VideoUseSync=0
Filtering=0

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

[Wizard]
Section0Cb=Protean Clouds
Section1Cb=None
Section2Cb=Waveform Horiz 04
Section3Cb=Neon marquee

