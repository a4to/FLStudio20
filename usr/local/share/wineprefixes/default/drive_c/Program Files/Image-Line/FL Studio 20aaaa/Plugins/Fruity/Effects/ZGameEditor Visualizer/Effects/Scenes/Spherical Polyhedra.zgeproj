<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" FileVersion="2">
  <OnLoaded>
    <ZLibrary Comment="HSV Library">
      <Source>
<![CDATA[const int
  ALPHA = 0,
  HUE = 1,
  SATURATION = 2,
  VALUE = 3,
  HSV_MIX = 4,
  BACKGROUND = 5,
  SELECT = 6,
  ZOOM =7,
  ROTATION_X = 8,
  ROTATION_Y = 9,
  SPEED = 10,
  BLEND = 11;

vec3 hsv(float h, float s, float v)
{
  s = clamp(s/100, 0, 1);
  v = clamp(v/100, 0, 1);

  if(!s)return vector3(v, v, v);

  h = h < 0 ? frac(1-abs(frac(h/360)))*6 : frac(h/360)*6;

  float c, f, p, q, t;

  c = floor(h);
  f = h-c;

  p = v*(1-s);
  q = v*(1-s*f);
  t = v*(1-s*(1-f));

  switch(c)
  {
    case 0: return vector3(v, t, p);
    case 1: return vector3(q, v, p);
    case 2: return vector3(p, v, t);
    case 3: return vector3(p, q, v);
    case 4: return vector3(t, p, v);
    case 5: return vector3(v, p, q);
  }
}

float lerp(float target,float value){
  float mul=clamp(abs(target-value),.1,900.0);
    if(abs(target-value)>app.DeltaTime*mul)
      {
      if (value-target>0.0)value-=app.DeltaTime*mul*.5;
      if (value-target<0.0)value+=app.DeltaTime*mul*.5;
      }
  else value=target;
  return value;
}

float lerpMed(float target,float value){
  float mul=clamp(abs(target-value),.2,900.0);
    if(abs(target-value)>app.DeltaTime*mul*2.0)
      {
      if (value-target>0.0)value-=app.DeltaTime*mul*1.5;
      if (value-target<0.0)value+=app.DeltaTime*mul*1.5;
      }
  else value=target;
  return value;
}

float lerpSlow(float target,float value){
  float mul=clamp(abs(target-value),.4,900.0);
    if(abs(target-value)>app.DeltaTime*mul*parameters[7])
      {
      if (value-target>0.0)value-=app.DeltaTime*mul*.5*parameters[7];
      if (value-target<0.0)value+=app.DeltaTime*mul*.5*parameters[7];
      }
  else value=target;
  return value;
}]]>
      </Source>
    </ZLibrary>
    <ZExternalLibrary ModuleName="ZGameEditor Visualizer">
      <Source>
<![CDATA[void ParamsNotifyChanged(xptr Handle,int Layer) { }
void ParamsChangeName(xptr Handle,int Layer, int Parameters, string NewName) { }
void ParamsWriteValueForLayer(xptr Handle, int Layer,int Param, float NewValue) { }
void ParamsUpdateComboItems(xptr Handle, int Layer,int Param, string[] NewItems) { }]]>
      </Source>
    </ZExternalLibrary>
    <ZLibrary Comment="BPM Funtions">
      <Source>
<![CDATA[int getBeat(){
  int beats = SongPositionInBeats;
  return SongPositionInSeconds ? beats:0;
}

int getBar(){
  int bar = SongPositionInBeats*.25;
  return SongPositionInSeconds ? bar:0;
}]]>
      </Source>
    </ZLibrary>
  </OnLoaded>
  <OnUpdate>
    <ZExpression Comment="Parameters">
      <Expression>
<![CDATA[uMouse=vector4(0,0,0,0);
uResolution=vector2(app.ViewportWidth,app.ViewportHeight);
uViewport=vector2(app.ViewportX,app.ViewportY);
vec3 c=hsv(Parameters[HUE]*360,Parameters[SATURATION]*100,(1-Parameters[VALUE])*100);
uColor=vector4(c.x,c.y,c.z,1.0-Parameters[ALPHA]);
uHSV=Parameters[HSV_MIX];
if (uHSV){
  ParamsChangeName(FLPluginHandle,LayerNr, 1, "Hue");
  ParamsChangeName(FLPluginHandle,LayerNr, 2, "Saturation");
  ParamsChangeName(FLPluginHandle,LayerNr, 3, "Value");
}
else for(int i=1; i<4; i++)ParamsChangeName(FLPluginHandle,LayerNr, i, "Disabled");
uBackGround=Parameters[BACKGROUND];
float s=Parameters[SELECT];
int select=s*1000;
uZoom=Parameters[ZOOM]*14.0; //default 3.5
uRotX=(Parameters[ROTATION_X]-.5)*2.0; //default--> 2x-1=.25
uRotY=(Parameters[ROTATION_Y]-.5)*2.0; //default--> 2y-1=.27
float delta=app.DeltaTime*Parameters[SPEED]*2.0;
uTime+=delta;

//TriggerBPM event based on Tempo or manual selection
if (select<4)uSelect=Select;
if (select==4)uSelect=getBar()%4;  //mod bar count to cycle through shapes
if (select==5)uSelect=getBeat()%4; //mod beat count to cycle through shapes
mCanvas.Blend=floor(Parameters[BLEND]*1000);]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <OnRender>
    <UseMaterial Material="mCanvas"/>
    <RenderSprite/>
  </OnRender>
  <Content>
    <Shader Name="Spherical_Polyhedra">
      <VertexShaderSource>
<![CDATA[#version 120

void main(){
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[#version 120

uniform vec2 iResolution,iViewport;
uniform float iTime,iHSV,iBackground,iSelect,iZoom,iRotX,iRotY;
uniform vec4 iMouse,iColor;

//Spherical polyhedra by nimitz (twitter: @stormoid)

/*
	Follow up to my "Sphere mappings" shader (https://www.shadertoy.com/view/4sjXW1)

	I was thinking about a cheap way to do icosahedral mapping and realized
	I could just project on an axis and rotate the sphere for each projected
	"facet".

	Here I am showing only tilings of the regular polyhedra but this technique can
	be used for any tilings of the sphere, regular or not. (or even arbitrary projections)

	I omitted the tetraedron since the small number of projections
	results in heavy deformation.

	Perhaps there is a way to make that process cheaper? Let me know.
*/

#define time iTime

mat2 mm2(in float a){float c = cos(a), s = sin(a);return mat2(c,-s,s,c);}
vec3 rotx(vec3 p, float a){ float s = sin(a), c = cos(a);
    return vec3(p.x, c*p.y - s*p.z, s*p.y + c*p.z); }
vec3 roty(vec3 p, float a){ float s = sin(a), c = cos(a);
    return vec3(c*p.x + s*p.z, p.y, -s*p.x + c*p.z); }
vec3 rotz(vec3 p, float a){ float s = sin(a), c = cos(a);
    return vec3(c*p.x - s*p.y, s*p.x + c*p.y, p.z); }

//---------------------------Textures--------------------------------
//-------------------------------------------------------------------
vec3 texpent(in vec2 p, in float idx)
{
    float siz = iResolution.x *.007;
    vec2 q = abs(p);
    float rz = sin(clamp(max(max(q.x*1.176-p.y*0.385, q.x*0.727+p.y),
                             -p.y*1.237)*33.,0.,25.))*siz+siz;
    vec3 col = (sin(vec3(1,1.5,5)*idx)+2.)*(rz+0.25);
    col -= sin(dot(p,p)*10.+time*5.)*0.4;
	return col;
}

vec3 textri2(in vec2 p, in float idx)
{
    float siz = iResolution.x *.007;
    vec2 q = abs(p);
    float rz = sin(clamp(max(q.x*1.73205+p.y, -p.y*2.)*32.,0.,25.))*siz+siz;
    vec3 col = (sin(vec3(1,1.7,5)*idx)+2.)*(rz+0.25);
    col -= sin(p.x*20.+time*5.)*0.2;
    return col;
}

vec3 texcub(in vec2 p, in float idx)
{
    float siz = iResolution.x *.007;
    float rz = sin(clamp(max(abs(p.x),abs(p.y))*24.,0.,25.))*siz+siz;
    vec3 col = (sin(vec3(4,3.,5)*idx*.9)+2.)*(rz+0.25);
    float a= atan(p.y,p.x);
    col -= sin(a*15.+time*11.)*0.15-0.15;
    return col;
}

vec3 textri(in vec2 p, in float idx)
{
    float siz = iResolution.x *.001;
    p*=1.31;
    vec2 bp = p;
    p.x *= 1.732;
	vec2 f = fract(p)-0.5;
    float d = abs(f.x-f.y);
    d = min(abs(f.x+f.y),d);

    float f1 = fract((p.y-0.25)*2.);
    d = min(d,abs(f1-0.5));
    d = 1.-smoothstep(0.,.1/(siz+.7),d);

    vec2 q = abs(bp);
    p = bp;
    d -= smoothstep(1.,1.3,(max(q.x*1.73205+p.y, -p.y*2.)));
    vec3 col = (sin(vec3(1.,1.5,5)*idx)+2.)*((1.-d)+0.25);
    col -= sin(p.x*10.+time*8.)*0.15-0.1;
    return col;
}

//----------------------------------------------------------------------------
//----------------------------------Sphere Tilings----------------------------
//----------------------------------------------------------------------------

//All the rotation matrices can be precomputed for better performance.

//5 mirrored pentagons for the dodecahedron
vec3 dod(in vec3 p)
{
    vec3 col = vec3(1);
    vec2 uv = vec2(0);
    for (float i = 0.;i<=4.;i++)
    {
        p = roty(p,0.81);
        p = rotx(p,0.759);
        p = rotz(p,0.3915);
    	uv = vec2(p.z,p.y)/((p.x));
    	col = min(texpent(uv,i+1.),col);
    }
    p = roty(p,0.577);
    p = rotx(p,-0.266);
    p = rotz(p,-0.848);
    uv = vec2(p.z,-p.y)/((p.x));
   	col = min(texpent(uv,6.),col);

    return 1.-col;
}

//10 mirrored triangles for the icosahedron
vec3 ico(in vec3 p)
{
    vec3 col = vec3(1);
    vec2 uv = vec2(0);

    //center band
    const float n1 = .7297;
    const float n2 = 1.0472;
    for (float i = 0.;i<5.;i++)
    {
        if(mod(i,2.)==0.)
        {
            p = rotz(p,n1);
        	p = rotx(p,n2);
        }
		else
        {
            p = rotz(p,n1);
        	p = rotx(p,-n2);
        }
        uv = vec2(p.z,p.y)/((p.x));
    	col = min(textri(uv,i+1.),col);
    }
    p = roty(p,1.048);
    p = rotz(p,.8416);
    p = rotx(p,.7772);
    //top caps
    for (float i = 0.;i<5.;i++)
    {
        p = rotz(p,n1);
        p = rotx(p,n2);

    	uv = vec2(p.z,p.y)/((p.x));
    	col = min(textri(uv,i+6.),col);
    }

    return 1.-col;
}

//4 mirrored triangles for octahedron
vec3 octa(in vec3 p)
{
    vec3 col = vec3(1);
    vec2 uv = vec2(0);
    const float n1 = 1.231;
    const float n2 = 1.047;
    for (float i = 0.;i<4.;i++)
    {
       	p = rotz(p,n1);
       	p = rotx(p,n2);
    	uv = vec2(p.z,p.y)/((p.x));
    	col = min(textri2(uv*.54,i+1.),col);
    }

    return 1.-col;
}

//cube using the same technique for completeness
vec3 cub(in vec3 p)
{
    vec3 col = vec3(1);
    vec2 uv = vec2(p.z,p.y)/((p.x));
   	col = min(texcub(uv*1.01,15.),col);
    p = rotz(p,1.5708);
    uv = vec2(p.z,p.y)/((p.x));
   	col = min(texcub(uv*1.01,4.),col);
    p = roty(p,1.5708);
    uv = vec2(p.z,p.y)/((p.x));
    col = min(texcub(uv*1.01,5.),col);

    return 1.-col;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

vec2 iSphere2(in vec3 ro, in vec3 rd)
{
    vec3 oc = ro;
    float b = dot(oc, rd);
    float c = dot(oc,oc) - 1.;
    float h = b*b - c;
    if(h <0.0) return vec2(-1.);
    else return vec2((-b - sqrt(h)), (-b + sqrt(h)));
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
	vec2 p = fragCoord.xy/iResolution.xy-0.5;
    vec2 bp = p+0.5;
	p.x*=iResolution.x/iResolution.y;
	vec2 um = iMouse.xy / iResolution.xy-.5;
	um.x *= iResolution.x/iResolution.y;

    //camera
    //StevenM adding uniforms to camera matrix
	vec3 ro = vec3(0.,0.,iZoom);
    vec3 rd = normalize(vec3(p,-1.4));
    mat2 mx = mm2(time*iRotX+um.x*6.);
    mat2 my = mm2(time*iRotY+um.y*6.);
    ro.xz *= mx;rd.xz *= mx;
    ro.xy *= my;rd.xy *= my;

    float sel;
    //sel = mod(floor((time+10.)*0.2),4.); StevenM: Not needed in ZGE - sync with tempo
    sel=iSelect;

    vec2 t = iSphere2(ro,rd);
    vec3 col = vec3(0.);
    //float bg = clamp(dot(-rd,vec3(0.577))*0.3+.6,0.,1.); StevenM: disabled - unused code?

    //StevenM if(iBackground) then draw background.
    if(iBackground>.1){
      if (sel == 0.) col = dod(rd)*1.2;
      else if (sel == 1.) col = ico(rd)*1.2;
      else if (sel == 2.) col = cub(rd)*1.2;
      else if (sel == 3.) col = octa(rd)*1.2;
    }

    if (t.x > 0.)
    {
    	vec3 pos = ro+rd*t.x;
        vec3 pos2 = ro+rd*t.y;
        vec3 rf = reflect(rd,pos);
        if (sel == 0.)
        {
            vec3 col2 = max(dod(pos)*2.,dod(pos2)*.6);
            col = mix(max(col,col2),col2,0.6);
        }
        else if (sel == 1.)
        {
            vec3 col2  = max(ico(pos2)*0.6,ico(pos)*2.);
            col = mix(max(col,col2),col2,0.6);
        }
        else if (sel == 2.)
        {
            vec3 col2  = max(cub(pos2)*0.6,cub(pos)*2.);
            col = mix(max(col,col2),col2,0.6);
        }
        else if (sel == 3.)
        {
            vec3 col2  = max(octa(pos2)*0.6,octa(pos)*2.);
            col = mix(max(col,col2),col2,0.6);
        }
    }
  float colAve=(col.r+col.g+col.b)/3.0;
  vec3 iCol = mix(col,vec3(colAve,colAve,colAve),iHSV)*iColor.rgb;
	fragColor = vec4(mix(iCol,col,1.0-iHSV),colAve)*iColor.a; //Steven add alpha and Averaging HSV color shift variation.
}

void main(){
    //ZGE does not use mainImage( out vec4 fragColor, in vec2 fragCoord )
    //Rededefined fragCoord as gl_FragCoord.xy-iViewport(dynamic window)
    mainImage(gl_FragColor,gl_FragCoord.xy-iViewport);
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="iResolution" VariableRef="uResolution"/>
        <ShaderVariable VariableName="iViewport" VariableRef="uViewport"/>
        <ShaderVariable VariableName="iTime" VariableRef="uTime"/>
        <ShaderVariable VariableName="iMouse" VariableRef="uMouse"/>
        <ShaderVariable VariableName="iColor" VariableRef="uColor"/>
        <ShaderVariable VariableName="iHSV" VariableRef="uHSV"/>
        <ShaderVariable VariableName="iBackground" VariableRef="uBackground"/>
        <ShaderVariable VariableName="iSelect" VariableRef="uSelect"/>
        <ShaderVariable VariableName="iZoom" VariableRef="uZoom"/>
        <ShaderVariable VariableName="iRotX" VariableRef="uRotX"/>
        <ShaderVariable VariableName="iRotY" VariableRef="uRotY"/>
      </UniformVariables>
    </Shader>
    <Group Comment="Default ShaderToy Uniform Varible Inputs">
      <Children>
        <Variable Name="uResolution" Type="6"/>
        <Variable Name="uTime"/>
        <Variable Name="uTimeDelta"/>
        <Variable Name="uFrame" Type="1"/>
        <Variable Name="uFrameRate"/>
        <Variable Name="uMouse" Type="8"/>
        <Variable Name="uDate" Type="8"/>
      </Children>
    </Group>
    <Group Comment="FL Studio Varibles">
      <Children>
        <Array Name="Parameters" SizeDim1="12" Persistent="255">
          <Values>
<![CDATA[78DA63608081063B286D0F24ECF3859AAD21620AF631FD4A30312B00790D06E2]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Alpha
Hue 
Saturation
Value 
Toggle HSV @checkbox
Background @checkbox
Shape @list1000:  "Pentagon  Tiles", "Icosahedron", "Square Tiles","Triangle Tiles","Switch on Bar","Switch on Beat"
Zoom
Rotation X
Rotation Y
Speed
Blend @list1000: None,Alpha/OmeMinusSourceAlpha,Alpha/one,Color/OneMinusSourceColor,AlphaSaturate/One,OneMinusSourceAlpha/Alpha
]]>
          </StringValue>
        </Constant>
        <Constant Name="AuthorInfo" Type="2" StringValue="nimitz"/>
        <Variable Name="FLPluginHandle" Comment="Set by plugin" Type="9"/>
        <Variable Name="SongPositionInBeats"/>
        <Variable Name="SongPositionInSeconds"/>
        <Variable Name="isPlaying" Type="1"/>
        <Variable Name="LayerNr" Comment="Set by plugin" Type="1"/>
      </Children>
    </Group>
    <Group Comment="Unique Uniform Varible Inputs">
      <Children>
        <Variable Name="uViewport" Type="6"/>
        <Variable Name="uColor" Type="8"/>
        <Variable Name="uHSV"/>
        <Variable Name="uBackground"/>
        <Variable Name="uSelect"/>
        <Variable Name="uZoom"/>
        <Variable Name="uRotX"/>
        <Variable Name="uRotY"/>
      </Children>
    </Group>
    <Group Comment="Materials and Textures">
      <Children>
        <Material Name="mCanvas" Light="0" SpecularColor="0 0 0 1" EmissionColor="0 0 0 1" Blend="2" Shader="Spherical_Polyhedra"/>
      </Children>
    </Group>
  </Content>
</ZApplication>
