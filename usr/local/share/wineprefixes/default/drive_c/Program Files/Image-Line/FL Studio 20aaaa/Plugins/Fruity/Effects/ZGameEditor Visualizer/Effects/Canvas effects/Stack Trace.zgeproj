<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" FileVersion="2">
  <OnLoaded>
    <SpawnModel Model="ScreenModel"/>
    <ZLibrary Comment="HSV Library">
      <Source>
<![CDATA[vec3 hsv(float h, float s, float v)
{
  s = clamp(s/100, 0, 1);
  v = clamp(v/100, 0, 1);

  if(!s)return vector3(v, v, v);

  h = h < 0 ? frac(1-abs(frac(h/360)))*6 : frac(h/360)*6;

  float c, f, p, q, t;

  c = floor(h);
  f = h-c;

  p = v*(1-s);
  q = v*(1-s*f);
  t = v*(1-s*(1-f));

  switch(c)
  {
    case 0: return vector3(v, t, p);
    case 1: return vector3(q, v, p);
    case 2: return vector3(p, v, t);
    case 3: return vector3(p, q, v);
    case 4: return vector3(t, p, v);
    case 5: return vector3(v, p, q);
  }
}]]>
      </Source>
    </ZLibrary>
    <ZLibrary Comment="ZgeViz interface">
      <Source>
<![CDATA[const int
  ALPHA = 0,
  HUE = 1,
  SATURATION = 2,
  LIGHTNESS = 3;]]>
      </Source>
    </ZLibrary>
  </OnLoaded>
  <OnUpdate>
    <ZExpression>
      <Expression>
<![CDATA[vec3 c=hsv(Parameters[HUE]*360,Parameters[SATURATION]*100,(1-Parameters[LIGHTNESS])*100);

ShaderColor.r=1.0-c.r;
ShaderColor.g=1.0-c.g;
ShaderColor.b=1.0-c.b;

uThreshold=Parameters[7]*3.0001;]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <Content>
    <Group>
      <Children>
        <Array Name="Parameters" SizeDim1="8" Persistent="255">
          <Values>
<![CDATA[78DA636000827C217B0686062066B0DF21C76A07A2A19801003C4A0356]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Alpha
Hue
Saturation
Lightness
Bars
Aberrate
Shake
Threshold]]>
          </StringValue>
        </Constant>
        <Constant Name="AuthorInfo" Type="2">
          <StringValue>
<![CDATA[srtuss
Adapted from https://www.shadertoy.com/view/lt23WG]]>
          </StringValue>
        </Constant>
      </Children>
    </Group>
    <Material Name="ScreenMaterial" Shading="1" Light="0" Blend="1" ZBuffer="0" Shader="ScreenShader"/>
    <Shader Name="ScreenShader">
      <VertexShaderSource>
<![CDATA[varying vec2 position;

void main(){
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
  position=vec2(vertex.x,vertex.y);
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[//Stack Trace by "srtuss"
//Adapted from https://www.shadertoy.com/view/lt23WG

uniform float iGlobalTime;
uniform float resX;
uniform float resY;
uniform float viewportX;
uniform float viewportY;
uniform float threshold;

uniform float Alpha;

vec2 iResolution = vec2(resX,resY);

uniform float pBars; // def 0.13
uniform float pAberrate;
uniform float pShake;

//vec3 pColor = vec3(1.0, 0.8, 0.5) * 0.95;
uniform vec3 pColor;


#define TEST

/* srtuss, 2013

"Stack Trace"

some design ideas for my game project, but most of all a motion blur experiment.

** ok so people actually wanted to read the code so i added some comments. **

i would like to point out that all you see and hear is entirely based on my ideas. potential
similarities to movies, games or whatever are truely unintended.
*/

#define SCANLINES
#define LENS_DISTORT
//#define RED_VERSION

// delicious pie
#define pi2 6.283185307179586476925286766559

//float src_bands(float band)
//{
//    return texture2D(iChannel0, vec2(band / iChannelResolution[0].x, 0.5)).x;
//}

/////////////////////////////////////////////////////////////////////////////////////////////////
// basic utility functions //////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////

// rotate a 2d vector
vec2 rotate(vec2 p, float a)
{
	return vec2(p.x * cos(a) - p.y * sin(a), p.x * sin(a) + p.y * cos(a));
}

// a triangle wave with custom length, for safe reputition of distance fields
float tri(float x, float s)
{
    return abs(fract(x / s) * 2.0 - 1.0) * s * 0.5;
}

// same as above, but does also provide an id of the current reputition
float tri(float x, float s, out float id)
{
    x /= s;
    id = floor(x);
    return abs(fract(x) * 2.0 - 1.0) * 0.5 * s;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// noise utility functions //////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////

// get 1 random value for 1 input variable
float hash(float x)
{
    return fract(cos(x * 73935.289367) * 396.87);
}

// get 2 random values for 2 input variables
vec2 hash2(in vec2 p)
{
	return fract(1965.5786 * vec2(sin(p.x * 591.32 + p.y * 154.077), cos(p.x * 391.32 + p.y * 49.077)));
}

// "stepnoise" function. produces a smooth-rectangle-shaped series of values
float stpnse(float x)
{
    float fl = floor(x);
    float ss = 0.1;
    return mix(hash(fl), hash(fl + 1.0), smoothstep(0.5 - ss, 0.5 + ss, fract(x)));
}

// altered random function, that has only 4 possible results. this is used for the camera
// rotation which only locks to angles that are multiples of 90 degrees
float hashX(float x)
{
    return floor(fract(x * 1972.578672) * 4.0);
}

// altered stepnoise function (see function above)
float stpnseX(float x)
{
    float fl = floor(x);
    float ss = 0.1;
    return mix(hashX(fl), hashX(fl + 1.0), smoothstep(0.5 - ss, 0.5 + ss, fract(x)));
}




#define ITER 10

/////////////////////////////////////////////////////////////////////////////////////////////////
// random orthagonal space division fractal function, made by me. could need some optimisation.
/////////////////////////////////////////////////////////////////////////////////////////////////
vec2 circuit(vec2 p)
{
	p = fract(p);
	float r = 0.123;
	float v = 0.0, g = 0.0;
	float test = 0.0;
	r = fract(r * 9184.928);
	float cp, d;

	d = p.x;
	g += pow(clamp(1.0 - abs(d), 0.0, 1.0), 160.0);
	d = p.y;
	g += pow(clamp(1.0 - abs(d), 0.0, 1.0), 160.0);
	d = p.x - 1.0;
	g += pow(clamp(1.0 - abs(d), 0.0, 1.0), 160.0);
	d = p.y - 1.0;
	g += pow(clamp(1.0 - abs(d), 0.0, 1.0), 160.0);

	for(int i = 0; i < ITER; i ++)
	{
		cp = 0.5 + (r - 0.5) * 0.9;
		d = p.x - cp;
		g += pow(clamp(1.0 - abs(d), 0.0, 1.0), 160.0);
		if(d > 0.0)
		{
			r = fract(r * 4829.013);
			p.x = (p.x - cp) / (1.0 - cp);
			v += 1.0;
			test = r;
		}
		else
		{
			r = fract(r * 1239.528);
			p.x = p.x / cp;
			test = r;
		}
		p = p.yx;
	}
	v /= float(ITER);
	return vec2(v, g);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// scene code ///////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////

// distancefield of the fast-moving, glowing rectangles in the background
float qd(vec2 p, float id)
{
    return max(abs(p.y) - 0.05 * hash(id), abs(p.x) - pBars - hash(id + 11.11) * 0.1);
}

// get the brightness of a background (scene) pixel
float col(vec2 uv)
{
    float v;
    float ts = 0.2;
    float id;
    // repetition
    float tr = tri(uv.y, ts, id);
    // distancefield
    v = qd(vec2(uv.x, tr), id);
    float fo = 30.0;
    // glow
    float br = exp(-fo * qd(vec2(uv.x, tr), id)) + exp(-fo * qd(vec2(uv.x, tr - ts), id + 1.0)) + exp(-fo * qd(vec2(uv.x, tr + ts), id - 1.0));
    v = min(v, length(uv) - 2.0);
    v = smoothstep(0.015, 0.0, v) + br * 0.1;
    // circuit fractal in the background
    vec2 cc = circuit(uv * 0.3);
    v += cc.y * 0.05;
    return v;
}


// hectic camera movement on the function above to show off the nice motion blur effect
float pix2(vec2 p, float t)
{
    float tt = t * 0.3;
    p = rotate(p, (stpnseX(tt + 1.0) + stpnseX(tt * 2.0)) * pi2 * 0.25);
    p *= 0.3 + (stpnse(t) + stpnse(t * 2.0) * 0.5) * 0.8;
    p.y += cos(t) * 4.0 + t * 4.0;
    t *= 0.4;

    p.x += ((stpnse(t) + stpnse(t * 2.0) * 0.5) - 0.5) * 1.5;
    t += 11.111;

    return col(p);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// hud code /////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////

// distancefield of tiny bars with reputition
float fr2(vec2 p, float h)
{
    vec2 q = abs(vec2(tri(p.x, 0.09), p.y));
    return max(q.x, q.y - h) - 0.002;
}

// distancefield of the rotating circle thing in the screen center
float fr3(vec2 p, float h)
{
    vec2 q = abs(vec2(tri(p.x * 6.0, 0.09), p.y));
    float v = max(q.x, q.y - h) - 0.002;

    float ror = stpnse(iGlobalTime) + stpnse(iGlobalTime * 2.0) * 0.3;

    q = abs(vec2(tri(p.x + ror, 0.5), p.y));
    v = min(v, max(q.x, q.y) - 0.1);

    q = abs(vec2(tri(p.x + iGlobalTime * 0.1, 0.3), p.y + 0.1));
    v = min(v, max(q.x, q.y) - 0.1);

    return v;
}


// distancefield that looks like text (one line of it)
float fr4(vec2 p, float l, float seed)
{
    vec2 pp = p;

    p.x += seed;

    float v;
    float w = dot(p, normalize(vec2(1.5, 1.0)));
    w = min(min((tri(w, 0.4444444)), (tri(w, 0.555555)) - 0.02), (tri(w, 0.566666)) - 0.01);

    v = max(w, abs(p.y) - 0.09);


    w = dot(p, normalize(vec2(-1.0, 0.2)));
    w = min(min((tri(w, 0.24251)), (tri(w, 0.36548)) - 0.02), (tri(w, 0.14)) - 0.005);

    w = max(w, abs(p.y) - 0.12);

    v = min(v, w);


    w = min(min((tri(p.x, 0.9)), (tri(p.x, 1.2)) - 0.02), (tri(p.x, 2.4)) - 0.09);
    v = max(v, 0.08 - w);

    return max(max(v, pp.x - 5.5 * l), -pp.x);
}

// distancefield of something that looks like text
// this function assembles it into a chunk of liles, that look like getting printed
float fr5(vec2 p, float t)
{
    float id = floor(t / 3.0) * 10.0;
    t = mod(t, 3.0);
    t *= 5.0;

    float v = 1e38;
    float rr = 0.873;
    for(int i = 0; i < 8; i ++)
    {
        float l = t;
        rr = fract(rr * 10472.842);
        v = min(v, fr4(p, min(l, rr + 0.03), id + float(i)));
        t -= 1.0;
        p.y += 0.5;
    }
    return v;
}


// putting everything together to get the final brightness value of a pixel
float pix(vec2 p)
{
    // motion blur accumulation loop:
    float ite = 0.0;
    for(int i = 0; i < 10; i ++)
    {
        float tc = 0.05;
        ite += pix2(p, iGlobalTime + (hash2(p + float(i)) - 0.5).x * tc);
    }
    float v = ite / 10.0;

    // assemble the hud:
    float lc = length(p);
    float w = abs(lc - 0.5);

    // bars at the top and the bottom of the screen
    w = min(w, fr2(vec2(p.x, abs(p.y) - 0.95), 0.02));
    vec2 pol = vec2(atan(p.y, p.x) / pi2, lc);
    // center rings
    w = min(w, fr3(pol - vec2(0.0, 0.7), 0.02));
    // "crosshair" circle
    w = min(w, abs(lc - 0.1));
    // text
    w = min(w, fr5((p - vec2(1.0, 0.7)) * 6.0, iGlobalTime) / 6.0);

    // some more circles
    w = min(w, abs(length(p - vec2(-1.4, 0.7)) - 0.04));
    w = min(w, abs(length(p - vec2(-1.2, 0.7)) - 0.04));
    w = min(w, abs(length(p - vec2(-1.0, 0.7)) - 0.04));

    // horizontal bars
    vec2 q = p;
    q.x = -abs(q.x);
    q -= vec2(-0.2, 0.0);
    // this looks nice but doesn't fit the rest
    //w = min(w, max(max(length(q) - 0.9, -length(q - vec2(0.25, 0.0)) + 1.0) + 0.03, p.y));
    w = min(w, max(abs(q.y), q.x + 0.8));

    // blinking exclamation mark:
    if(fract(iGlobalTime / 6.0) > 0.7 && sin(iGlobalTime * 50.0) > 0.0)
    {
        q = p - vec2(1.3, -0.6);
        q.x = abs(q.x);
        float ww = max(dot(q, normalize(vec2(1.0, -0.2))), abs(q.y - 0.2) - 0.15);
        q.y += 0.05;
        ww = min(ww, length(q) - 0.04);
        w = min(w, abs(ww));
    }

    // finally, turn the hud distancefield into a brightness + glow
    v += smoothstep(0.01, 0.0, w) * 0.09 + exp(max(0.0, w) * -20.0) * 0.03;

#ifdef SCANLINES
    // add some scannlines to hide the lack of detail
    v *= sin(p.y * 300.0) * 0.15 + 0.9;
#endif

    return v;
}

void main() {
  	vec2 uv = (gl_FragCoord.xy-vec2(viewportX,viewportY)) / iResolution.xy;

    uv = 2.0 * uv - 1.0;
    uv.x *= iResolution.x / iResolution.y;

#ifdef LENS_DISTORT
    // pincushion distort the screen coordinates. gives some sort of visor-head-up-display feel.
    float ll = dot(uv, uv);
    uv *= (ll * ll * 0.01 + ll * ll * ll * 0.001 + ll * 0.04) * -0.3 + 1.01;
#endif

    // shake the screen slightly for an impression of speed and to distract
    // the viewer from seeing all the visual artifacts.
    uv += (vec2(hash(iGlobalTime), hash(iGlobalTime - 1.111)) - 0.5) * pShake * 0.04;

    // slight chroma aberration for a modern and blurry look
    vec2 ps = pAberrate / iResolution.xy;
    vec3 col = vec3(pix(uv - vec2(ps.x, 0.0)), pix(uv + vec2(0.0, ps.y * 0.3)), pix(uv + vec2(ps.x, 0.0)));

    // brighten the center, darken the screen border
    col *= exp(length(uv) * -2.0) * 3.0;

    // turn the brightness values into colors using some amazing color maps
    #ifdef RED_VERSION
    col = pow(col, vec3(0.5, 0.8, 1.0)) * 3.4;
    #else
    col = pow(col, pColor) * 4.4;
    #endif

    // last but not least, gamma correct
    col = pow(col, vec3(1.0 / 2.2));
    col=clamp(col,vec3(0.0),vec3(1.0));
	  gl_FragColor = vec4(col,(col.r+col.g+col.b<threshold ? 0.0:1.0)*Alpha);
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="iGlobalTime" ValuePropRef="App.Time"/>
        <ShaderVariable VariableName="resX" Value="1163" ValuePropRef="App.ViewportWidth"/>
        <ShaderVariable VariableName="resY" Value="432" ValuePropRef="App.ViewportHeight"/>
        <ShaderVariable VariableName="viewportX" Value="262" ValuePropRef="App.ViewportX"/>
        <ShaderVariable VariableName="viewportY" Value="262" ValuePropRef="App.ViewportY"/>
        <ShaderVariable VariableName="Alpha" ValuePropRef="1-Parameters[0];"/>
        <ShaderVariable VariableName="pBars" ValuePropRef="Parameters[4]-0.5;"/>
        <ShaderVariable VariableName="pAberrate" ValuePropRef="(Parameters[5]-0.5)*50;"/>
        <ShaderVariable VariableName="pShake" ValuePropRef="(Parameters[6]-0.5)*5;"/>
        <ShaderVariable VariableName="pColor" VariableRef="ShaderColor"/>
        <ShaderVariable VariableName="threshold" VariableRef="uThreshold"/>
      </UniformVariables>
    </Shader>
    <Model Name="ScreenModel">
      <OnRender>
        <UseMaterial Material="ScreenMaterial"/>
        <RenderSprite/>
      </OnRender>
    </Model>
    <Variable Name="ShaderColor" Type="7"/>
    <Variable Name="uThreshold"/>
  </Content>
</ZApplication>
