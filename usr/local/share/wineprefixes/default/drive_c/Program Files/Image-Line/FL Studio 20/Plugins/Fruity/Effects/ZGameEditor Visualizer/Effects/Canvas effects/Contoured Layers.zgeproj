<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" FileVersion="2">
  <OnLoaded>
    <ZLibrary Comment="HSV Library">
      <Source>
<![CDATA[vec3 hsv(float h, float s, float v)
{
  s = clamp(s/100, 0, 1);
  v = clamp(v/100, 0, 1);

  if(!s)return vector3(v, v, v);

  h = h < 0 ? frac(1-abs(frac(h/360)))*6 : frac(h/360)*6;

  float c, f, p, q, t;

  c = floor(h);
  f = h-c;

  p = v*(1-s);
  q = v*(1-s*f);
  t = v*(1-s*(1-f));

  switch(c)
  {
    case 0: return vector3(v, t, p);
    case 1: return vector3(q, v, p);
    case 2: return vector3(p, v, t);
    case 3: return vector3(p, q, v);
    case 4: return vector3(t, p, v);
    case 5: return vector3(v, p, q);
  }
}]]>
      </Source>
    </ZLibrary>
  </OnLoaded>
  <OnUpdate>
    <ZExpression>
      <Expression>
<![CDATA[uResolution=vector2(app.ViewportWidth,app.ViewportHeight);
uViewport=vector2(app.ViewportX,app.ViewportY);
uAlpha =1.0-Parameters[0];



vec3 col = hsv(Parameters[1]*360,Parameters[2]*100,(1-Parameters[3])*100);
uColor = vector3(col[0],col[1],col[2]);

float speed=(Parameters[4]-0.5)*4.0;
float delta=app.DeltaTime*Speed; //comment to use other time options
uTime+=delta;]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <OnRender>
    <UseMaterial Material="mCanvas"/>
    <RenderSprite/>
  </OnRender>
  <Content>
    <Shader Name="MainShader">
      <VertexShaderSource>
<![CDATA[#version 120

void main(){
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[#version 120

uniform vec2 iResolution,iViewport;
uniform float iTime,iPal,iHatch,iPaper;
uniform float iAlpha;


/*
	Contoured Layers
	----------------

	Constructing some concise contoured layers, then applying various edge and shading
	effects to produce some faux depth. Technically, that's what is happening here, but
	this example was mainly put together as a means to demonstrate various layering
    effects, like strokes, highlights, shadows, etc. No 3D was harmed during the making
	of this example. :)

	I love those contoured noise-based paper layer images that various graphic artists
	from places like Adobe distribute with their applications. Most consist of some
	antialiased noise layers rendered in a flat tone with drop shadows for each. The
	fancier ones sometimes include highlighted edges, etc, which is what I've put
	together here. None of it is difficult to produce, provided you're happy with
	concept of smoothing layers at a particular threshold with respect to the field
	derivative.

	I put in a few diferent aesthetic options to try, just to show how much something
	like a simple palette change, drop shadow, etc, can effect the overall feel. I've
    tuned most settings on by default, but turning them off can give a cleaner look.
	Turning everything off, except the shadows will give the minimal contoured images
	you see around, which I happen to like also.

	Anyway, feel free to play around with the defines below. At some stage, I might
    render some icons and allow the various options to be manipulated via the mouse,
	but for now, this will suffice.

	I also have a few raymarched 3D versions that I'll put up at a later date.


*/

// Dropping down a blurry dark layer to give a fake ambient occlusion effect.
// It's subtle, but gives things a bit more depth. However, turning it off gives
// a crisper look. I guess it depends what you're after.
#define FAKE_AO

// Contour strokes are great for that hand drawn look, or just to give some definition
// to geometry. This one is dark, but it can be any color.
#define STROKE

// Highlights, to give the impression that light is hitting the surface.
#define HILIGHT

// Shadows: There aren't too many times when I wouldn't want shadows, but I can think
// of a few. If expense if a problem, you can fake it with a thicker AO layer, but it's
// not quite the same.
#define SHADOW

// Including the metallic texture: I overuse this particular texture, but it's the only
// one on Shadertoy with a fine enough grade on it. I'm hoping more subtle textures
// will get added at some stage. :)
#define TEXTURED

// Running a cheap hatch-like algorithm over the layers for a bit of extra texture.
// Come to think of it, it's probably more of a stipple pattern... Either way, it's
// just a little extra texture.
//#define HATCH

// Very subtle paper grain. It's pretty simple... and I think it's based on one of
// Flockaroo's snippets. If you're interested in post processing effects, his examples
// are definitely worth looking at.
//#define PAPER_GRAIN

// Palette: It's amazing how something as simple as color choice can effect the feel
// of an image.
// Settings: Greyscale: 0, Red: 1, Blue: 2, Earth: 3, Pink and grey: 4,
// Grey and pink: 5, Green and white: 6.
//#define PALETTE iPal




// Standard 2D rotation formula.
mat2 rot2(in float a){ float c = cos(a), s = sin(a); return mat2(c, -s, s, c); }

// vec3 to float hash.
float hash21( vec2 p ){

    return fract(sin(dot(p, vec2(41, 289)))*45758.5453);

    //p.x = fract(sin(dot(p, vec2(1, 113)))*45758.5453);
    //return sin(p.x*6.2831853 + iTime);
}

// vec2 to vec2 hash.
vec2 hash22(vec2 p) {

    // Faster, but doesn't disperse things quite as nicely. However, when framerate
    // is an issue, and it often is, this is a good one to use. Basically, it's a tweaked
    // amalgamation I put together, based on a couple of other random algorithms I've
    // seen around... so use it with caution, because I make a tonne of mistakes. :)
    float n = sin(dot(p, vec2(1, 113)));
    //return fract(vec2(262144, 32768)*n)*2. - 1.;

    // Animated.
    p = sin(vec2(262144, 32768)*n);
    // Note the ".45," insted of ".5" that you'd expect to see. When edging, it can open
    // up the cells ever so slightly for a more even spread. In fact, lower numbers work
    // even better, but then the random movement would become too restricted. Zero would
    // give you square cells.
    return cos(p*6.2831853+iTime*2.0);

}



// Cheap and nasty 2D smooth noise function with inbuilt hash function -- based on IQ's
// original. Very trimmed down. In fact, I probably went a little overboard. I think it
// might also degrade with large time values, but that's not an issue here.
float n2D(vec2 p){

    // Setup.
    // Any random integers will work, but this particular
    // combination works well.
    const vec2 s = vec2(1.0, 113.0);
    // Unique cell ID and local coordinates.
    vec2 ip = floor(p); p -= ip;
    // Vertex IDs.
    vec4 h = vec4(0., s.x, s.y, s.x + s.y) + dot(ip, s);

    // Smoothing.
    p = p*p*(3. - 2.*p);
    //p *= p*p*(p*(p*6. - 15.) + 10.); // Smoother.

    // Random values for the square vertices.
    h = fract(sin(h)*43758.5453);

    // Interpolation.
    h.xy = mix(h.xy, h.zw, p.y);
    return mix(h.x, h.y, p.x); // Output: Range: [0, 1].
}


// Based on IQ's gradient noise formula.
float n2D3G( in vec2 p ){

    vec2 i = floor(p); p -= i;

    vec4 v;
    v.x = dot(hash22(i), p);
    v.y = dot(hash22(i + vec2(1, 0)), p - vec2(1, 0));
    v.z = dot(hash22(i + vec2(0, 1)), p - vec2(0, 1));
    v.w = dot(hash22(i + 1.), p - 1.);

#if 1
    // Quintic interpolation.
    p = p*p*p*(p*(p*6. - 15.) + 10.);
#else
    // Cubic interpolation.
    p = p*p*(3. - 2.*p);
#endif

    return mix(mix(v.x, v.y, p.x), mix(v.z, v.w, p.x), p.y);
    //return v.x + p.x*(v.y - v.x) + p.y*(v.z - v.x) + p.x*p.y*(v.x - v.y - v.z + v.w);
}



// The map function. Just two layers of gradient noise. Way more interesting
// functions are possible, but we're keeping things simple.
float map(vec3 p, float i){

    return n2D3G(p.xy*3.)*.66 + n2D3G(p.xy*6.)*.34 + i/10.*1. - .15;

}


// 2D derivative function.
vec3 getNormal(in vec3 p, float m, float i) {

    vec2 e = vec2(.001, 0);

    // Four extra samples. Slightly better, but not really needed here.
	//return (vec3(map(p + e.xyy, i) - map(p - e.xyy, i), map(p + e.yxy, i) - map(p - e.yxy, i),	0.))/e.x*.7071;

    // Three samples, but only two extra sample calculations.
    return (vec3(m - map(p - e.xyy, i), m - map(p - e.yxy, i),	0.))/e.x*1.4142;
}

// The map layer and its derivative. To produce constant width layer edges, the derivative
// is necessary, so the distance field value and the derivative at the point is returned.
vec4 mapLayer(in vec3 p, float i){

    vec4 d;

    d.x = map(p, i); // Distance field value.

    d.yzw = getNormal(p, d.x, i); // Derivative.

    return d;

}



// Layer color. Based on the shade, layer number and smoothing factor.
vec3 getCol(vec2 p, float sh, float fi, float sf){

    // Color.
    vec3 col;
    float d=1./255.;
    if (iPal == 0.){
        if(fi==5.) col = vec3(79.*d, 220.*d, 59.*d);
        if(fi==4.) col = vec3(32.*d, 80.*d, 22.*d);
        if(fi==3.) col = vec3(225.*d, 93.*d, 26.*d);
        if(fi==2.) col = vec3(205.*d, 65.*d, 10.*d);
        if(fi==1.) col = vec3(190.*d, 53.*d, 0.*d);
        if(fi==0.) col = vec3(30.*d, 30.*d, 25.*d);
    }

    if (iPal == 1.){
        if(fi==5.) col = vec3(60.*d, 59.*d, 109.*d);
        if(fi==4.) col = vec3(60.*d, 59.*d, 100.*d);
        if(fi==3.) col = vec3(255.*d, 255.*d, 255.*d);
        if(fi==2.) col = vec3(253.*d, 253.*d, 253.*d);
        if(fi==1.) col = vec3(178.*d, 33.*d, 51.*d);
        if(fi==0.) col = vec3(178.*d, 33.*d, 51.*d);
    }

    if (iPal == 2.){
        if(fi==5.) col = vec3(250.*d, 248.*d, 259.*d);
        if(fi==4.) col = vec3(155.*d, 157.*d, 146.*d);
        if(fi==3.) col = vec3(89.*d, 115.*d, 98.*d);
        if(fi==2.) col = vec3(84.*d, 110.*d, 146.*d);
        if(fi==1.) col = vec3(189.*d, 221.*d, 255.*d);
        if(fi==0.) col = vec3(33.*d, 57.*d, 117.*d);
    }

    if (iPal == 3.){
        if(fi==5.) col = vec3(250.*d, 210.*d, 232.*d);
        if(fi==4.) col = vec3(253.*d, 194.*d, 224.*d);
        if(fi==3.) col = vec3(249.*d, 156.*d, 203.*d);
        if(fi==2.) col = vec3(227.*d, 134.*d, 181.*d);
        if(fi==1.) col = vec3(207.*d, 114.*d, 161.*d);
        if(fi==0.) col = vec3(199.*d, 106.*d, 153.*d);
    }




    /*

    if (iPal == 5.){
      if(mod(fi, 2.)<.5) col = vec3(1, .15, .4)*.8;
    	else col = vec3(.25, .15, .2);
    }

    if (iPal == 6.){
    	if(mod(fi, 2.)>.5) col = vec3(.6);
    	else col = vec3(.6, 1, .15)*.6;
    }

    */

    //#ifdef TEXTURED
    //vec3 tx = iColor;//texture(iChannel0, p + hash21(vec2(sh, fi))).xyz; tx *= tx;
    //col = min(col*tx*3., 1.);
    //#endif


    return col;

}


// A hatch-like algorithm, or a stipple... or some kind of textured pattern.
float doHatch(vec2 p, float res){

    // If the "HATCH" define is on, produce the pattern.
    if(iHatch==1.0){

        // The pattern is physically based, so needs to factor in screen resolution.
        p *= res/16.;

        // Random looking diagonal hatch lines.
        float hatch = clamp(sin((p.x - p.y)*3.14159*200.)*2. + .5, 0., 1.); // Diagonal lines.

        // Slight randomization of the diagonal lines, but the trick is to do it with
        // tiny squares instead of pixels.
        float hRnd = hash21(floor(p*6.) + .73);
        if(hRnd>.66) hatch = hRnd;

        //#ifdef TEXTURED
        //hatch = hatch*.75 + .5; // Stronger hatching for the textured version.
        //#else
        hatch = hatch*.5 + .75;


        return hatch;

    }
    else return 1.;



}


void mainImage(out vec4 fragColor, in vec2 fragCoord){


    // Aspect correct screen coordinates. Setting a minumum resolution on the
    // fullscreen setting in an attempt to keep things relatively crisp.
    float res = iResolution.y;//min(iResolution.y, 700.);
	vec2 uv = (fragCoord - iResolution.xy*.5)/res;

    // Scaling and translation.
    vec2 p = uv;// + vec2(1, 0)*iTime;

    // Resolution based smoothing factor. Later, the contour derivative will
    // be factored in.
    float sf = 1./iResolution.y;

    // Initialize to the first layer color.
    vec3 col = getCol(p, 0., 0., sf);

    // Previous layer variable.
    float pL = 0.;


    // Random looking diagonal hatch lines.
    float hatch = doHatch(p, res);



    // Applying the cross hatch.
    col *= hatch;

    // Number of layers.
    int lNum = 5;
    float flNum = 5.;


    for(int i = 0; i<lNum; i++){


        float fi = float(i);

        // Repositioning the hatch lines on each layer to reduce the "shower screen effect,"
        // as per Fabrice Neyret's suggestion. :)
        hatch = doHatch(p + sin(vec2(41, 289)*(fi + 1.)), res);

        // The map layer value (just some gradient noise), and its derivative.
        vec4 c = mapLayer(vec3(p, 1.), fi);
        // Offset noise layer value with derivative. It's offset so as to coincide with
        // to the directional lighting.
        vec4 cSh = mapLayer(vec3(p - vec2(.03, -.03)*((flNum - fi)/flNum*.5 + .5), 1.), fi);

        // Shade.
        float sh = (fi + 1.)/(flNum);

        // Layer color.
        vec3 lCol = getCol(p, sh, fi + 1., sf);

        // Some standard direct lighting to apply to the edge layer. It's set in a
        // direction to compliment the shadow layer.
        vec3 ld = normalize(vec3(-1, 1, -.25));
        vec3 n = normalize(vec3(0, 0, -1) + c.yzw);
        float diff = max(dot(ld, n), 0.);
        #ifdef TEXTURED
        diff *= 2.5; // Add a bit more light to the edges for the textured version.
        #else
        diff *= 2.;
        #endif


        // Applying the diffuse lighting to the edge layer.
        vec3 eCol = lCol*(diff + 1.);

        // Apply the layers.

        // Smoothing factor, based on the distance field derivative.
    	float sfL = sf*length(c.yzx)*2.;
    	float sfLSh = sf*length(cSh.yzx)*6.;

        // Drop shadow.
        #ifdef SHADOW
        #ifdef TEXTURED
        const float shF = .5;
        #else
        const float shF = .35;
        #endif
        col = mix(col, vec3(0), (1. - smoothstep(0., sfLSh, max(cSh.x, pL)))*shF);
        #endif

        // Dark blurred layer.
        #ifdef FAKE_AO
		col = mix(col, vec3(0), (1. - smoothstep(0., sfL*3., c.x))*.25);
        #endif

        // Dark edge stroke.
        #ifdef STROKE
        col = mix(col, vec3(0), (1. - smoothstep(0., sfL, c.x))*.85);
        #endif

        // Hilight and color layer.
        #ifdef HILIGHT
        col = mix(col, eCol*hatch, (1. - smoothstep(0., sfL, c.x + length(c.yzx)*.003)));
        col = mix(col, lCol*hatch, (1. - smoothstep(0., sfL, c.x + length(c.yzx)*.006)));
        #else
        col = mix(col, lCol*hatch, (1. - smoothstep(0., sfL, c.x + length(c.yzx)*.0025)));
        #endif

        // Previous layer, to take away from the shadow.
        pL = c.x;

    }



    // Mixing in a little extra noisy color for the default greyscale textured setting.
    #ifdef TEXTURED
//    #if PALETTE == 0
	col *= mix(vec3(1.8, 1., .7).zyx, vec3(1.8, 1., .7).xzy, n2D(p*2.));
//    #endif
    #endif



    if(iPaper==1){
      vec3 rn3 = vec3(n2D((uv*iResolution.y/1. + 1.7)) - n2D(vec2(uv*iResolution.y/1. + 3.4)));
      col *= .93 + .07*rn3.xyz  + .07*dot(rn3, vec3(.299, .587, .114));
    }



    // Subtle vignette.
    uv = fragCoord/iResolution.xy;
    col *= pow(16.*uv.x*uv.y*(1. - uv.x)*(1. - uv.y) , .0625);
    // Colored variation.
    //col = mix(col*vec3(.3, 0, 1), col, pow(16.*uv.x*uv.y*(1. - uv.x)*(1. - uv.y) , .125));


    // Rough gamma correction, then output to the screen.
    fragColor = vec4(sqrt(max(col, 0.)), iAlpha);
}

void main(){
    //ZGE does not use mainImage( out vec4 fragColor, in vec2 fragCoord )
    //Rededefined fragCoord as gl_FragCoord.xy-iViewport(dynamic window)
    mainImage(gl_FragColor,gl_FragCoord.xy-iViewport);
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="iResolution" VariableRef="uResolution"/>
        <ShaderVariable VariableName="iViewport" VariableRef="uViewport"/>
        <ShaderVariable VariableName="iTime" VariableRef="uTime"/>
        <ShaderVariable VariableName="iAlpha" VariableRef="uAlpha"/>
        <ShaderVariable VariableName="iPal" ValuePropRef="round(Parameters[1]*1000)"/>
        <ShaderVariable VariableName="iHatch" ValuePropRef="Parameters[2]"/>
        <ShaderVariable VariableName="iPaper" ValuePropRef="Parameters[3]"/>
      </UniformVariables>
    </Shader>
    <Group Comment="Default ShaderToy Uniform Variable Inputs">
      <Children>
        <Variable Name="uResolution" Type="6"/>
        <Variable Name="uTime"/>
        <Variable Name="uViewport" Type="6"/>
      </Children>
    </Group>
    <Group Comment="FL Studio Variables">
      <Children>
        <Array Name="Parameters" SizeDim1="5" Persistent="255">
          <Values>
<![CDATA[789C63604005B3664ADA030004B8018C]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Alpha
Palette @list1000: Fruit,USA,Earth,"Bubble Gum"
Hatch @checkbox
Light Grain @checkbox
Speed]]>
          </StringValue>
        </Constant>
        <Constant Name="AuthorInfo" Type="2">
          <StringValue>
<![CDATA[Shane (converted by Youlean & StevenM)
https://www.shadertoy.com/view/3lj3zt]]>
          </StringValue>
        </Constant>
        <Array Name="SpecBandArray"/>
        <Variable Name="uMax123" Type="7"/>
        <Variable Name="uMax456" Type="7"/>
        <Variable Name="uCol1" Type="7"/>
        <Variable Name="uCol2" Type="7"/>
        <Variable Name="uCol3" Type="7"/>
        <Variable Name="uCol4" Type="7"/>
        <Variable Name="uCol5" Type="7"/>
        <Variable Name="uCol6" Type="7"/>
      </Children>
    </Group>
    <Group Comment="Unique Uniform Variable Inputs">
      <Children>
        <Variable Name="uAlpha"/>
        <Variable Name="uColor" Type="7"/>
      </Children>
    </Group>
    <Group Comment="Materials and Textures">
      <Children>
        <Material Name="mCanvas" Blend="1" Shader="MainShader"/>
      </Children>
    </Group>
  </Content>
</ZApplication>
