<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="HUD Line" ClearColor="0 0 0 1" AmbientLightColor="0.7529 0.7529 0.7529 1" ScreenMode="0" CameraPosition="0 0 30" LightPosition="200 200 200" ViewportRatio="2" FOV="70" ClipFar="1000" MouseVisible="255" UseStencilBuffer="255" FileVersion="2">
  <Comment>
<![CDATA[HUD Graph Linear
Linear sound frequency or signal pitch graph.

Mouse in ZGameEditor Visualizer - Preview:
RMB - displays the pivot point and effect boundaries
LMB on pivot point + mouse move - selects and moves graph

Parameters:
Alpha - transparency
Hue - color hue
Saturation - color saturation
Lightness - color lightness
Position X - X position
Position Y - Y position
Width - width of graph
Height - height of graph
Scale - scale of graph
Anchor - position of pivot point
Rotation - rotation around pivot point
Frequency - source of graph values; frequency or signal pitch
Start - start frequency; determines graph density
End - end frequency; determines graph density
Base - level of fill area or point lines
Min - minimal graph level
Velocity - speed of movement
Line width - graph line width
Point size - size of graph points
Point line width - width of point lines
Fill type - type of background fill
Fill alpha - transparency of background fill
Is selectable - allow to change position by LMB in Preview]]>
  </Comment>
  <OnLoaded>
    <ZExternalLibrary Comment="Vector graphics library" ModuleName="ZgeNano" CallingConvention="1">
      <Source>
<![CDATA[/*
  ZgeNano Library; vector graphics rendering library
  build on NanoVG: https://github.com/memononen/nanovg
  and NanoSVG: https://github.com/memononen/nanosvg

  Version: 1.4 (2018-09-10)

  Download Windows DLL and Android shared library from
  https://github.com/Rado-1/ZgeNano/releases

  Project home
  https://github.com/Rado-1/ZgeNano

  Copyright (c) 2016-2018 Radovan Cervenka
*/

// init flags
const int
  NVG_ANTIALIAS = 1<<0,
	NVG_STENCIL_STROKES = 1<<1,
	NVG_DEBUG = 1<<2;

// winding (direction of arcs)
const int
  NVG_CCW = 1, // counter-clockwise
  NVG_CW = 2; // clockwise

// solidity
const int
  NVG_SOLID = 1,
	NVG_HOLE = 2;

// line caps
const int
	NVG_BUTT = 0,
	NVG_ROUND = 1,
	NVG_SQUARE = 2,
	NVG_BEVEL = 3,
	NVG_MITER = 4;

// align
const int
	// Horizontal align
	NVG_ALIGN_LEFT 		= 1<<0,	// Default, align text horizontally to left.
	NVG_ALIGN_CENTER 	= 1<<1,	// Align text horizontally to center.
	NVG_ALIGN_RIGHT 	= 1<<2,	// Align text horizontally to right.
	// Vertical align
	NVG_ALIGN_TOP 		= 1<<3,	// Align text vertically to top.
	NVG_ALIGN_MIDDLE	= 1<<4,	// Align text vertically to middle.
	NVG_ALIGN_BOTTOM	= 1<<5,	// Align text vertically to bottom.
	NVG_ALIGN_BASELINE	= 1<<6; // Default, align text vertically to baseline.

// blend factor
const int
	NVG_ZERO = 1<<0,
	NVG_ONE = 1<<1,
	NVG_SRC_COLOR = 1<<2,
	NVG_ONE_MINUS_SRC_COLOR = 1<<3,
	NVG_DST_COLOR = 1<<4,
	NVG_ONE_MINUS_DST_COLOR = 1<<5,
	NVG_SRC_ALPHA = 1<<6,
	NVG_ONE_MINUS_SRC_ALPHA = 1<<7,
	NVG_DST_ALPHA = 1<<8,
	NVG_ONE_MINUS_DST_ALPHA = 1<<9,
	NVG_SRC_ALPHA_SATURATE = 1<<10;

// composite operation
const int
	NVG_SOURCE_OVER = 0,
	NVG_SOURCE_IN = 1,
	NVG_SOURCE_OUT = 2,
	NVG_ATOP = 3,
	NVG_DESTINATION_OVER = 4,
	NVG_DESTINATION_IN = 5,
	NVG_DESTINATION_OUT = 6,
	NVG_DESTINATION_ATOP = 7,
	NVG_LIGHTER = 8,
	NVG_COPY = 9,
	NVG_XOR = 10;

// image flags
const int
  NVG_IMAGE_GENERATE_MIPMAPS	= 1<<0,     // Generate mipmaps during creation of the image.
	NVG_IMAGE_REPEATX			= 1<<1,		// Repeat image in X direction.
	NVG_IMAGE_REPEATY			= 1<<2,		// Repeat image in Y direction.
	NVG_IMAGE_FLIPY				= 1<<3,		// Flips (inverses) image in Y direction when rendered.
	NVG_IMAGE_PREMULTIPLIED		= 1<<4;		// Image data has premultiplied alpha.

// Init
xptr nvg_Init(int flags) {} // Returns NanoVG context used in other functions.
void nvg_SetContext(xptr context) {}
void nvg_Finish(xptr context) {}
int nvg_SetViewport(xptr context) {} // Returns 1 if changed viewport, 0 otherwise.

// Drawing
void nvg_BeginFrame() {}
void nvg_CancelFrame() {}
void nvg_EndFrame() {}

// Global composite operation
void nvg_GlobalCompositeOperation(int op) {}
void nvg_GlobalCompositeBlendFunc(int sfactor, int dfactor) {}
void nvg_GlobalCompositeBlendFuncSeparate(int srcRGB, int dstRGB, int srcAlpha, int dstAlpha) {}

// State handling
void nvg_Save() {}
void nvg_Restore() {}
void nvg_Reset() {}

// Render styles
void nvg_StrokeColor(float r, float g, float b, float a) {}
void nvg_StrokePaint(xptr paint) {} // paint - result of Paints functions
void nvg_FillColor(float r, float g, float b, float a) {}
void nvg_FillPaint(xptr paint) {} // paint - result of Paints functions
void nvg_MiterLimit(float limit) {}
void nvg_StrokeWidth(float size) {}
void nvg_LineCap(int cap) {}
void nvg_LineJoin(int join) {}
void nvg_GlobalAlpha(float alpha) {}

// Transformations
void nvg_ResetTransform() {}
void nvg_Transform(float a, float b, float c, float d, float e, float f) {}
void nvg_Translate(float x, float y) {}
void nvg_Rotate(float angle) {}
void nvg_SkewX(float angle) {}
void nvg_SkewY(float angle) {}
void nvg_Scale(float x, float y) {}

// Images
int nvg_CreateImage(string filename, int imageFlags) {}
int nvg_CreateImageMem(int imageFlags, xptr data, int ndata) {}
int nvg_CreateImageRGBA(int w, int h, int imageFlags, xptr data) {}
void nvg_UpdateImage(int image, xptr data) {}
void nvg_ImageSize(int image, ref int w, ref int h) {}
void nvg_DeleteImage(int image) {}

// Paints
xptr nvg_LinearGradient(float sx, float sy, float ex, float ey,
	float ir, float ig, float ib, float ia,
	float or, float og, float ob, float oa) {}
xptr nvg_BoxGradient(float x, float y, float w, float h, float r, float f,
	float ir, float ig, float ib, float ia,
	float or, float og, float ob, float oa) {}
xptr nvg_RadialGradient(float cx, float cy, float inr, float outr,
	float ir, float ig, float ib, float ia,
	float or, float og, float ob, float oa) {}
xptr nvg_ImagePattern(float ox, float oy, float ex, float ey,
	float angle, int image, float alpha) {}
void nvg_FreePaint(xptr paint) {}

// Scissoring
void nvg_Scissor(float x, float y, float w, float h) {}
void nvg_IntersectScissor(float x, float y, float w, float h) {}
void nvg_ResetScissor() {}

// Paths
void nvg_BeginPath() {}
void nvg_MoveTo(float x, float y) {}
void nvg_LineTo(float x, float y) {}
void nvg_BezierTo(float c1x, float c1y, float c2x, float c2y, float x, float y) {}
void nvg_QuadTo(float cx, float cy, float x, float y) {}
void nvg_ArcTo(float x1, float y1, float x2, float y2, float radius) {}
void nvg_ClosePath() {}
void nvg_PathWinding(int dir) {}
void nvg_Arc(float cx, float cy, float r, float a0, float a1, int dir) {}
void nvg_Rect(float x, float y, float w, float h) {}
void nvg_RoundedRect(float x, float y, float w, float h, float r) {}
void nvg_RoundedRectVarying(float x, float y, float w, float h,
	float radTopLeft, float radTopRight, float radBottomRight, float radBottomLeft) {}
void nvg_Ellipse(float cx, float cy, float rx, float ry) {}
void nvg_Circle(float cx, float cy, float r) {}
void nvg_Fill() {}
void nvg_Stroke() {}
void nvg_StrokeNoScale() {}

// Text
int nvg_CreateFont(string name, string filename) {}
int nvg_CreateFontMem(string name, xptr data, int ndata, int freeData) {}
int nvg_FindFont(string name) {}
int nvg_AddFallbackFontId(int baseFont, int fallbackFont) {}
int nvg_AddFallbackFont(string baseFont, string fallbackFont) {}
void nvg_FontSize(float size) {}
void nvg_FontBlur(float blur) {}
void nvg_TextLetterSpacing(float spacing) {}
void nvg_TextLineHeight(float lineHeight) {}
void nvg_TextAlign(int align) {}
void nvg_FontFaceId(int font) {}
void nvg_FontFace(string font) {}
void nvg_Text(float x, float y, string str, string end) {}
void nvg_TextBox(float x, float y, float breakRowWidth, string str, string end) {}
float nvg_TextBounds(float x, float y, string str, string end, vec4 bounds) {}
void nvg_TextBoxBounds(float x, float y, float breakRowWidth, string str, string end, vec4 bounds) {}
void nvg_TextMetrics(ref float ascender, ref float descender, ref float lineh) {}

// SVG support
xptr nsvg_ParseFromFile(string filename, string units, float dpi) {}
xptr nsvg_ParseMem(string data, int ndata, string units, float dpi) {}
void nsvg_ImageSize(xptr image, ref float width, ref float height) {}
int nsvg_ImageShapeCount(xptr image, string shapeIdPrefix) {}
void nsvg_Draw(xptr image, string shapeIdPrefix,
  int strokeWidthScaling, float strokeWidthFactor, float buildUpFromFactor, float buildUpToFactor, xptr color) {}
void nsvg_Rasterize(xptr image, float tx, float ty, float scale, xptr dst, int w, int h) {}
void nsvg_Delete(xptr image) {}]]>
      </Source>
    </ZExternalLibrary>
    <ZExternalLibrary ModuleName="ZGameEditor Visualizer">
      <Source>
<![CDATA[// ZGameEditor Visualizer built-in functions

void ParamsWriteValueForLayer(xptr Handle, int Layer,int Param, float NewValue) { }]]>
      </Source>
    </ZExternalLibrary>
    <ZLibrary Comment="HSV conversion by Kjell">
      <Source>
<![CDATA[float angle(float X)
{
  if(X >= 0 && X < 360)return X;
  if(X > 360)return X-floor(X/360)* 360;
  if(X <   0)return X+floor(X/360)*-360;
}

void hsv(float H, float S, float V, ref vec4 c)
{
  float R,G,B,I,F,P,Q,T;

  H = angle(H);
  S = clamp(S,0,100);
  V = clamp(V,0,100);

  H /= 60;
  S /= 100;
  V /= 100;

  if(S == 0)
  {
    c.R = V;
    c.G = V;
    c.B = V;
    return;
  }

  I = floor(H);
  F = H-I;

  P = V*(1-S);
  Q = V*(1-S*F);
  T = V*(1-S*(1-F));

  if(I == 0){R = V; G = T; B = P;}
  if(I == 1){R = Q; G = V; B = P;}
  if(I == 2){R = P; G = V; B = T;}
  if(I == 3){R = P; G = Q; B = V;}
  if(I == 4){R = T; G = P; B = V;}
  if(I == 5){R = V; G = P; B = Q;}

  c.R = R;
  c.G = G;
  c.B = B;
}]]>
      </Source>
    </ZLibrary>
    <ZLibrary Comment="ZgeViz interface">
      <Source>
<![CDATA[const string AuthorInfo = "Rado1";

// PARAMETERS

const string ParamHelpConst =
"Alpha\n" +
"Hue\n" +
"Saturation\n" +
"Lightness\n" +
"Position X\n" +
"Position Y\n" +
"Width\n" +
"Height\n" +
"Scale\n" +
"Anchor @list: \"Top left\", \"Top middle\", \"Top right\"," +
  "\"Middle left\", Center, \"Middle right\"," +
  "\"Bottom left\", \"Bottom middle\", \"Bottom right\"\n" +
"Rotation\n" +
"Frequency @checkbox\n" +
"Start\n" +
"End\n" +
"Base\n" +
"Velocity\n" +
"Line width\n" +
"Point size\n" +
"Point line width\n" +
"BG type @list: None, Solid, \"Top-down\", \"Bottom-up\"," +
  "\"Left-right\", \"Right-left\"\n" +
"BG Alpha\n" +
"Is selectable @checkbox";

const int
  P_ALPHA = 0,
  P_HUE = 1,
  P_SATURATION = 2,
  P_LIGHTNESS = 3,
  P_POSITION_X = 4,
  P_POSITION_Y = 5,
  P_WIDTH = 6,
  P_HEIGHT = 7,
  P_SCALE = 8,
  P_ANCHOR = 9,
  P_ROTATION = 10,
  P_FREQUENCY = 11,
  P_START = 12,
  P_END = 13,
  P_BASE = 14,
  P_VELOCITY = 15,
  P_LINE_WIDTH = 16,
  P_POINT_SIZE = 17,
  P_POINT_LINE_WIDTH = 18,
  P_BG_TYPE = 19,
  P_BG_ALPHA = 20,
  P_IS_SELECTABLE = 21,
  NUM_OF_PARAMS = 22;

const int
  NUM_OF_ANCHORS = 9,
  NUM_OF_BG = 6;

// VARIABLES

xptr FLPluginHandle;
int LayerNr;

float[] SpecBandArray;
float[] AudioArray;
//float SongPositionInBeats;

//float[NUM_OF_PARAMS] Parameters;
float[NUM_OF_PARAMS] ParamOld;
int[NUM_OF_PARAMS] ParamChanged;
vec4 Color;]]>
      </Source>
    </ZLibrary>
    <ZLibrary Comment="Globals">
      <Source>
<![CDATA[// CONSTANTS

// determines processing mode flag for OnHostMessage
private const int FPD_ProcessMode = 1;

// boolean
const int FALSE = 0;
const int TRUE = 1;

const float PI2 = PI * 2.0;

// varia
const float SELECTION_OFFSET = 0.01;
const float SELECTION_RADIUS = 15.0;

// VARIABLES

xptr NvgContext;
float ViewportWidth, ViewportHeight, AspectRatio, SelectionRadius;
int IsLMB, IsRMB, WasLMB, IsMoved, IsNotExported;
float RefX, RefY, OrigX, OrigY;
int BgType;
xptr BgGradient;
float[] SignalArray;
vec2[] Points;

float
  PosX, // position  X
  PosY, // position Y
  Width, // width
  Height, // height
  AnchorX, // horizontal position of anchor; 0.0, 0.5, or 1.0
  AnchorY, // vertical position of anchor; 0.0, 0.5, or 1.0
  Rotation, // rotation in radians
  Base, // vertical position of base
  LineWidth, // line width in px; 0 - no line
  PointSize, // size of point in px; 0 - no point
  PointLineWidth, // width of point line; 0 - no line
  Velocity; // speed of moving the line
int Start, End, From, To; // beginning and end of displayed interval

// FUNCTIONS

inline float dist(float x1, float y1, float x2, float y2) {
  float dx = (x1-x2)*AspectRatio, dy = y1-y2;
  return sqrt(dx*dx+dy*dy);
}

inline float max(float a, float b) {
  return a<b ? b : a;
}

// Callback on hanged OpenGL context
void OnGLContextChange() {
  // reset NanoVG
  @CallComponent(Component: InitNanoVG);
}

// Callback to handle external messages
void OnHostMessage(int id, int index, int value) {
  if(id == FPD_ProcessMode)
    IsNotExported = !(value & 16);
}]]>
      </Source>
    </ZLibrary>
    <ZExpression Comment="Init">
      <Expression>
<![CDATA[// init variables
IsMoved = FALSE;
IsNotExported = TRUE;

// set size of band array buffer
SignalArray.SizeDim1 = SpecBandArray.SizeDim1;
Points.SizeDim1 = SignalArray.SizeDim1;

// recompute all parameters
for(int i = 0; i < NUM_OF_PARAMS; ++i)
  ParamOld[i] = -1;]]>
      </Expression>
    </ZExpression>
    <ZExpression Name="InitNanoVG">
      <Expression>
<![CDATA[// init NanoVG
NvgContext = nvg_Init(NVG_STENCIL_STROKES);
if (NvgContext == null) {
  trace("Error to init NanoVG.");
  quit();
}
BgGradient = null;]]>
      </Expression>
    </ZExpression>
    <SetAppState State="MainAppState"/>
  </OnLoaded>
  <States>
    <AppState Name="MainAppState" CollisionsEnabled="0">
      <OnUpdate>
        <Condition Expression="return App.ViewportWidth != 0;">
          <OnTrue>
            <ZExpression Comment="Reset input">
              <Expression>
<![CDATA[IsLMB = FALSE;
IsRMB = FALSE;]]>
              </Expression>
            </ZExpression>
            <KeyPress Comment="LMB" Keys="{">
              <OnPressed>
                <ZExpression Expression="IsLMB = TRUE;"/>
              </OnPressed>
            </KeyPress>
            <KeyPress Comment="RMB" Keys="}">
              <OnPressed>
                <ZExpression Expression="IsRMB = TRUE;"/>
              </OnPressed>
            </KeyPress>
            <ZExpression Comment="Compute shape parameters">
              <Expression>
<![CDATA[// flag of changing size
int w = FALSE, h = FALSE, r = FALSE, d = FALSE, c = FALSE;

// reload BG gradient if NVG context has changed
if (BgType >=2) {
  if(ViewportWidth == -1) {
    BgGradient = null;
    c = TRUE;
  }
}

// resize if viewport has changed
if (nvg_SetViewport(NvgContext)) {
  ViewportWidth = App.ViewportWidth;
  ViewportHeight = App.ViewportHeight;
  AspectRatio = ViewportWidth / ViewportHeight;
  SelectionRadius = SELECTION_RADIUS * 2.0 / ViewportWidth;
  w = TRUE;
  h = TRUE;
}

// update parameter change
for(int i = 0; i < NUM_OF_PARAMS; ++i) {
  ParamChanged[i] = Parameters[i] != ParamOld[i];
  if (ParamChanged[i])
    ParamOld[i] = Parameters[i];
}

// check changed parameters

// update color
if (ParamChanged[P_HUE] || ParamChanged[P_SATURATION] || ParamChanged[P_LIGHTNESS] ||
  ParamChanged[P_ALPHA]) {

  hsv(Parameters[P_HUE]*360,Parameters[P_SATURATION]*100,(1-Parameters[P_LIGHTNESS])*100, Color);
  Color.A = 1 - Parameters[P_ALPHA];

  c = TRUE;
}

// width
if (ParamChanged[P_WIDTH] || ParamChanged[P_SCALE] || w) {
  Width = Parameters[P_WIDTH] * Parameters[P_SCALE] * ViewportWidth;
  w = TRUE;
}

// height
if (ParamChanged[P_HEIGHT] || ParamChanged[P_SCALE] || h) {
  Height = Parameters[P_HEIGHT] * Parameters[P_SCALE] * ViewportHeight;
  h = TRUE;
}

// anchor position
if (ParamChanged[P_ANCHOR] || w || h) {
  int k = round(Parameters[P_ANCHOR] * NUM_OF_ANCHORS);
  AnchorX = (k % 3) * Width / -2.0;
  AnchorY = (k / 3) * Height / -2.0;
}

// position X
if (ParamChanged[P_POSITION_X] || w)
  PosX = Parameters[P_POSITION_X] * ViewportWidth;

// position Y
if (ParamChanged[P_POSITION_Y] || h)
  PosY = Parameters[P_POSITION_Y] * ViewportHeight;

// rotation
if (ParamChanged[P_ROTATION]) {
  Rotation = (Parameters[P_ROTATION] - 0.5) * PI2;
  r = TRUE;
}

// start index
if (ParamChanged[P_START]) {
  Start = Parameters[P_START] * (SpecBandArray.SizeDim1 - 1);
  From = Start < End ? Start : End;
  To = Start < End ? End : Start;
  d = TRUE;
}

// end index
if (ParamChanged[P_END]) {
  End = Parameters[P_END] * (SpecBandArray.SizeDim1 - 1);
  From = Start < End ? Start : End;
  To = Start < End ? End : Start;
  d = TRUE;
}

// base
if (ParamChanged[P_BASE] || h)
  Base = (1.0 - Parameters[P_BASE]) * Height;

// velocity
if (ParamChanged[P_VELOCITY])
  Velocity = pow(Parameters[P_VELOCITY], 3);

// line width
if (ParamChanged[P_LINE_WIDTH] || w)
  LineWidth = Width * Parameters[P_LINE_WIDTH]  * 0.02;

// point size
if (ParamChanged[P_POINT_SIZE] || w)
  PointSize = Width * pow(Parameters[P_POINT_SIZE], 2) * 0.05;

// point line width
if (ParamChanged[P_POINT_LINE_WIDTH] || w)
  PointLineWidth = Width * pow(Parameters[P_POINT_LINE_WIDTH], 2) * 0.1;

// BG fill type or BG fill alpha
if (ParamChanged[P_BG_TYPE] || ParamChanged[P_BG_ALPHA] || c ||
  (BgType >=2 && (w || h))) {

  BgType = round(Parameters[P_BG_TYPE] * NUM_OF_BG);

  switch (BgType) {
    case 0: // none
    case 1: // solid fill
      break;

    case 2: // top-down
      nvg_FreePaint(BgGradient);
      BgGradient = nvg_LinearGradient(0, 0, 0, Height,
        Color.R, Color.G, Color.B, Color.A * (1 - Parameters[P_BG_ALPHA]),
        Color.R, Color.G, Color.B, 0.0);
      break;

    case 3: // bottom-up
      nvg_FreePaint(BgGradient);
      BgGradient = nvg_LinearGradient(0, 0, 0, Height,
        Color.R, Color.G, Color.B, 0.0,
        Color.R, Color.G, Color.B, Color.A * (1 - Parameters[P_BG_ALPHA]));
      break;

    case 4: // left-right
      nvg_FreePaint(BgGradient);
      BgGradient = nvg_LinearGradient(0, 0, Width, 0,
        Color.R, Color.G, Color.B, Color.A * (1 - Parameters[P_BG_ALPHA]),
                Color.R, Color.G, Color.B, 0.0);
      break;

    case 5: // right-left
      nvg_FreePaint(BgGradient);
      BgGradient = nvg_LinearGradient(0, 0, Width, 0,
        Color.R, Color.G, Color.B, 0.0,
        Color.R, Color.G, Color.B, Color.A * (1 - Parameters[P_BG_ALPHA]));
      break;
  }
}

// prepare points

// compute x axes
if (w || d) {
  float dx = From == To ? Width : Width / (To - From);
  float cx = 0;

  for (int i = From; i <= To; ++i) {
    Points[i].X = cx;
    cx += dx;
  }
}

// compute y axes
if (Parameters[P_FREQUENCY])
  for (int i = From; i <= To; ++i) {
    SignalArray[i] =  SignalArray[i] +
      (SpecBandArray[i] - SignalArray[i]) * Velocity;
    Points[i].Y = (1.0 - SignalArray[i]) * Height;
  }
else
  for (int i = From; i <= To; ++i) {
    SignalArray[i] =  SignalArray[i] +
      (AudioArray[i] + 0.5 - SignalArray[i]) * Velocity;
    Points[i].Y = (1.0 - SignalArray[i]) * Height;
  }

// handle LMB

if (Parameters[P_IS_SELECTABLE] && IsLMB) {

  float mx = App.MousePosition.X / 2.0 + 0.5;
  float my = App.MousePosition.Y / -2.0 + 0.5;

  if (IsMoved) {

    // move
    ParamsWriteValueForLayer(FLPluginHandle, LayerNr, P_POSITION_X, OrigX + mx - RefX);
    ParamsWriteValueForLayer(FLPluginHandle, LayerNr, P_POSITION_Y, OrigY + my - RefY);

  } else {
    if (!WasLMB && dist(
      Parameters[P_POSITION_X], Parameters[P_POSITION_Y],
      mx, my) < SelectionRadius) {

      // start moving
      RefX = mx;
      RefY = my;
      OrigX = Parameters[P_POSITION_X];
      OrigY = Parameters[P_POSITION_Y];
      IsMoved = TRUE;
    }
  }
} else // IsLMB
  IsMoved = FALSE;

WasLMB = IsLMB;]]>
              </Expression>
            </ZExpression>
          </OnTrue>
        </Condition>
      </OnUpdate>
      <OnRender>
        <ZExpression>
          <Expression>
<![CDATA[nvg_SetContext(NvgContext);
nvg_BeginFrame();

// transform
nvg_Translate(PosX, PosY);
nvg_Rotate(Rotation);
nvg_Translate(AnchorX, AnchorY);

// fill BG
if (BgType) {

  // create path
  nvg_BeginPath();
  nvg_MoveTo(0, Points[From].y);
  for (int i = From+1; i <= To; ++i)
    nvg_LineTo(Points[i].X, Points[i].Y);

  nvg_LineTo(Width, Base);
  nvg_LineTo(0, Base);
  nvg_ClosePath();

  // set BG type
  switch (BgType) {

    case 1: // solid color
      nvg_FillColor(Color.R, Color.G, Color.B, Color.A * (1 - Parameters[P_BG_ALPHA]));
      break;

    default: // gradient
      nvg_FillPaint(BgGradient);
      break;
  }

  nvg_Fill();
}

// draw common properties
nvg_StrokeColor(Color.R, Color.G, Color.B, Color.A);
nvg_LineJoin(NVG_ROUND);

// draw point lines
if (PointLineWidth > 0.01) {
  nvg_StrokeWidth(PointLineWidth);
  nvg_LineCap(NVG_BUTT);

  for (int i = From; i <= To; ++i) {
    nvg_BeginPath();
    nvg_MoveTo(Points[i].X, Base);
    nvg_LineTo(Points[i].X, Points[i].Y);
    nvg_Stroke();
  }
}

// draw line
if (LineWidth > 0.01) {
  nvg_StrokeWidth(LineWidth);
  nvg_LineCap(NVG_ROUND);

  nvg_BeginPath();
  nvg_MoveTo(0, Points[From].Y);
  for (int i = From+1; i <= To; ++i)
    nvg_LineTo(Points[i].X, Points[i].Y);

  nvg_Stroke();
}

// draw points
if (PointSize > 0.01) {
  nvg_FillColor(Color.R, Color.G, Color.B, Color.A);

  for (int i = From; i <= To; ++i) {
    nvg_BeginPath();
    nvg_Circle(Points[i].X, Points[i].Y, PointSize);
    nvg_Fill();
  }
}

// draw selection area
if (Parameters[P_IS_SELECTABLE] && (IsRMB || IsMoved) && IsNotExported) {
  nvg_FillColor(1.0, 0.2, 0.2, 0.5);
  nvg_StrokeColor(1.0, 0.2, 0.2, 0.5);
  nvg_StrokeWidth(1.0);

  nvg_BeginPath();
  nvg_Rect(0, 0, Width, Height);
  nvg_Stroke();

  nvg_BeginPath();
  nvg_MoveTo(0,0);
  nvg_LineTo(Width, Height);
  nvg_Stroke();

  nvg_BeginPath();
  nvg_MoveTo(0,Height);
  nvg_LineTo(Width, 0);
  nvg_Stroke();

  nvg_BeginPath();
  nvg_Circle(0-AnchorX, 0-AnchorY, SELECTION_RADIUS);
  nvg_Fill();
}

nvg_EndFrame();]]>
          </Expression>
        </ZExpression>
      </OnRender>
    </AppState>
  </States>
  <OnClose>
    <ZExpression Name="FinishNanoVG">
      <Expression>
<![CDATA[nvg_SetContext(NvgContext);
nvg_FreePaint(BgGradient);
nvg_Finish(NvgContext);]]>
      </Expression>
    </ZExpression>
  </OnClose>
  <Content>
    <Array Name="Parameters" SizeDim1="22" Persistent="255">
      <Values>
<![CDATA[789C636000037B0604B087E00618B64BEB786C8710030118CD607FF68C8F1D032A00AB03009B480A26]]>
      </Values>
    </Array>
  </Content>
</ZApplication>
