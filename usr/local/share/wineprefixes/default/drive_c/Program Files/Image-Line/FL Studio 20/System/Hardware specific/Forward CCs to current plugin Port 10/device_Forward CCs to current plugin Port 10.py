# name=Forward CCs to current plugin Port 10
# url=https://forum.image-line.com/viewtopic.php?f=1994&t=242187
# receiveFrom=Forward CCs to current plugin Port 10

import transport
import mixer
import ui
import midi
import time
import math
import device
import playlist
import channels
import patterns
import utils
import general

class TSimple():

    def OnInit(self):
        print(general.getVersion())
        
    def OnMidiIn(self, event):                
        if (event.status == midi.MIDI_CONTROLCHANGE):
            msg = event.status + (event.data1 << 8) + (event.data2 << 16) + (10 << 24)
            device.forwardMIDICC(msg, 2)                    
            event.handled = True        
        else:
            event.handled = False           

    def OnMidiMsg(self, event):
        event.handled = False  

Simple = TSimple()

def OnInit():
    Simple.OnInit()

def OnMidiMsg(event):
    Simple.OnMidiMsg(event)

def OnMidiIn(event):
    Simple.OnMidiIn(event)

