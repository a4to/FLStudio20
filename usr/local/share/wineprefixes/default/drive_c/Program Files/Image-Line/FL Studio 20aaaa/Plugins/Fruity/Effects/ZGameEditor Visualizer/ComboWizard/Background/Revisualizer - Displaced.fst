FLhd   0  ` FLdt�  �20.6 �    %�.Z G a m e E d i t o r   V i s u a l i z e r   �4    	          A                     �   �  B  �    �HQV ՙ,  ﻿[General]
GlWindowMode=1
LayerCount=22
FPS=1
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,1,10,19,20,8,18,17,11,14,13,2,3,4,5,6,7,12,21,9,15,16

[AppSet0]
App=HUD\HUD 3D
FParamValues=0,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.599,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=0,0,500,500,500,500,500,500,500,500,500,500,500,599,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet1]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet10]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=2

[AppSet19]
App=Peak Effects\Polar
FParamValues=0,0.455,0,0,0.126,0.5,0.471,0.08,0.5,0.5,0.173,0.487,0,0.528,0,0.5,0,0.464,1,0,0,0,0,1
ParamValues=0,455,0,0,126,500,471,80,500,500,173,487,0,528,0,500,0,464,1000,0,0,0,0,1
Enabled=1
Collapsed=1
ImageIndex=6

[AppSet20]
App=Peak Effects\Polar
FParamValues=0,0.461,1,0,0.126,0.5,0.471,0.08,0.5,0.5,0.173,0.487,0,0.528,0,0.5,0,0.464,1,0,0,0,0,1
ParamValues=0,461,1000,0,126,500,471,80,500,500,173,487,0,528,0,500,0,464,1000,0,0,0,0,1
Enabled=1
Collapsed=1
ImageIndex=6

[AppSet8]
App=Postprocess\Youlean Color Correction
FParamValues=0.055,0.112,0.5,0.5,0.5,0.5
ParamValues=55,112,500,500,500,500
ParamValuesPostprocess\Youlean Bloom=276,0,150,312,1000,0,0,0
Enabled=1
Collapsed=1

[AppSet18]
App=Misc\Automator
FParamValues=1,9,1,6,0.055,0,0.523,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,9,1,6,55,0,523,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1
Collapsed=1

[AppSet17]
App=Postprocess\Youlean Bloom
AppVersion=2
FParamValues=0.5,0,0.15,0.4545,1,0,0,1
ParamValues=500,0,150,454,1000,0,0,1
Enabled=1
Collapsed=1

[AppSet11]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet14]
App=Blend\BufferBlender
FParamValues=0,2,2,1,0,0,0,0,0.5,0.5,0.75,0
ParamValues=0,2,2,1000,0,0,0,0,500,500,750,0
Enabled=1
Collapsed=1
ImageIndex=2

[AppSet13]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet2]
App=HUD\HUD 3D
FParamValues=0,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.776,0.5,0.578,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,1
ParamValues=0,0,500,500,500,500,500,500,500,500,500,776,500,578,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
Collapsed=1

[AppSet3]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet4]
App=Blend\BufferBlender
FParamValues=0,4,0,0,1,4,0.059,0,0.5,0.5,0.75,0
ParamValues=0,4,0,0,1,4,59,0,500,500,750,0
Enabled=1
Collapsed=1
ImageIndex=5

[AppSet5]
App=Misc\Automator
FParamValues=1,5,7,2,0.249,0.194,0.611,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,5,7,2,249,194,611,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1
Collapsed=1

[AppSet6]
App=HUD\HUD 3D
FParamValues=0,0,0.506,0.503,0.5,0.5039,0.4968,0.684,0.5,0.5,0.5024,0.5,0.5,0.5,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,0
ParamValues=0,0,506,503,500,503,496,684,500,500,502,500,500,500,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,0
Enabled=1
Collapsed=1
ImageIndex=6

[AppSet7]
App=Misc\Automator
FParamValues=1,7,6,2,0.496,0.538,0.509,1,7,7,2,0.498,1,0.501,1,7,11,2,0.501,0.405,0.501,0,0,0,0,0,0.25,0.25
ParamValues=1,7,6,2,496,538,509,1,7,7,2,498,1000,501,1,7,11,2,501,405,501,0,0,0,0,0,250,250
Enabled=1
Collapsed=1

[AppSet12]
App=Postprocess\Youlean Motion Blur
FParamValues=0,0,0,0,1,0,1
ParamValues=0,0,0,0,1,0,1
Enabled=1
ImageIndex=3

[AppSet21]
App=Misc\Automator
FParamValues=1,13,1,6,0,0.25,0.546,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,13,1,6,0,250,546,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1
Collapsed=1

[AppSet9]
App=Postprocess\Youlean Audio Shake
FParamValues=2,0.08,0,0,0.2,0,0.1,0.2,0.5,0.5,0.5,0.508,0,0
ParamValues=2,80,0,0,200,0,100,200,500,500,500,508,0,0
ParamValuesMisc\Automator=1000,69,121,857,500,250,500,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0
Enabled=1
Collapsed=1

[AppSet15]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.464,0.5,0.5,0.5,0.5
ParamValues=500,464,500,500,500,500
Enabled=1
Collapsed=1

[AppSet16]
App=Misc\Automator
FParamValues=1,16,2,6,0.464,0.25,0.502,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,16,2,6,464,250,502,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1
Collapsed=1

[Video export]
VideoH=2160
VideoW=3840
VideoRenderFps=60
SampleRate=44100
VideoCodecName=
AudioCodecName=(default)
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=C:\Users\ugots\Desktop\ZGameEditor - Displaced.mp4
Bitrate=100000000
AudioBitrate=384000
Uncompressed=0
Supersample=0

[UserContent]
Text=Author
Html="<p align=""center"">[textline]</p>"
Images="[plugpath]ComboWizard\Assets Revisualizer\gradient-square.png","[plugpath]ComboWizard\Assets Revisualizer\bg displaced.jpg"
VideoUseSync=0
Filtering=0

[Detached]
Top=186
Left=438
Width=1236
Height=648

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

