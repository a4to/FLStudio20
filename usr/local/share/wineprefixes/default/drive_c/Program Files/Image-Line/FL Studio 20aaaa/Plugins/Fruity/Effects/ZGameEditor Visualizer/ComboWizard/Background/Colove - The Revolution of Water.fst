FLhd   0 + ` FLdt�  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��!�  ﻿[General]
GlWindowMode=1
LayerCount=11
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,1,2,3,4,5,6,8,13,10,12
WizardParams=515

[AppSet0]
App=Background\SolidColor
FParamValues=0,0.612,0.764,0.924
ParamValues=0,612,764,924
Enabled=1
Collapsed=1
Name=WORLD

[AppSet1]
App=Terrain\GoopFlow
FParamValues=0.424,0.568,1,0,1,0.5,0.304,0,0.5,0.496,0.324,0.496,1,0.3,0.168,0.092
ParamValues=424,568,1000,0,1000,500,304,0,500,496,324,496,1000,300,168,92
ParamValuesCanvas effects\DarkSpark=500,24,500,500,500,500,500,500,500,500,0,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesCanvas effects\SkyOcean=0,0,500,0,500,604,500,0,500,1000,388,0,0
ParamValuesMisc\FruityIndustry=0,0,1000,0,0,500,500,500,500,0,500,500,500
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,838,500,500,500,500,500
ParamValuesMisc\Automator=0,74,30,429,0,282,466,1000,185,61,286,508,526,254,1000,111,91,571,0,250,250,0,0,0,0,0,250,250,0,0,0,0
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
Enabled=1
Collapsed=1
Name=Water 1

[AppSet2]
App=Terrain\GoopFlow
FParamValues=0.512,0.68,0.868,0,1,0.5,0.304,0,0.5,0.496,0.324,0.46,1,0.3,0.308,0.128
ParamValues=512,680,868,0,1000,500,304,0,500,496,324,460,1000,300,308,128
Enabled=1
Collapsed=1
Name=Water 2

[AppSet3]
App=Terrain\GoopFlow
FParamValues=0.496,0.636,0.828,0.168,1,0.5,0.304,0,0.5,0.496,0.104,0.46,1,0.3,0.308,0.036
ParamValues=496,636,828,168,1000,500,304,0,500,496,104,460,1000,300,308,36
Enabled=1
Collapsed=1
Name=Water 3

[AppSet4]
App=Terrain\GoopFlow
FParamValues=0.476,0.596,1,0.168,1,0.5,0.304,0,0.496,0.508,0.748,0.46,0.516,0.548,0.308,0.036
ParamValues=476,596,1000,168,1000,500,304,0,496,508,748,460,516,548,308,36
Enabled=1
Collapsed=1
Name=Water 4

[AppSet5]
App=Terrain\GoopFlow
FParamValues=0.516,0.576,1,0.168,0.836,0.5,0.304,0,0.496,0.484,0.748,0.46,0.516,0.548,0.308,0.036
ParamValues=516,576,1000,168,836,500,304,0,496,484,748,460,516,548,308,36
Enabled=1
Collapsed=1
Name=Water 5

[AppSet6]
App=Postprocess\Blooming
FParamValues=0,0,0,0.712,0.58,0.8,0.128,0.26,0
ParamValues=0,0,0,712,580,800,128,260,0
Enabled=1
Collapsed=1
Name=Water 6

[AppSet8]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.208,0.192,0.53,0.258,1,0,0,0
ParamValues=208,192,530,258,1000,0,0,0
ParamValuesPostprocess\Vignette=0,0,0,1000,676,596
ParamValuesPostprocess\RGB Shift=424,448,0,600,700,200
ParamValuesPostprocess\Point Cloud High=0,1000,482,500,512,448,496,495,570,657,184,0,376,0,667
ParamValuesPostprocess\ScanLines=400,400,0,0,1000,1000
ParamValuesPostprocess\Point Cloud Low=0,1000,482,500,500,448,496,503,570,473,156,0,0,0,0
Enabled=1
Collapsed=1
Name=Bloom

[AppSet13]
App=Postprocess\Youlean Motion Blur
FParamValues=0.5,0,0.872,0.568,0,0,0
ParamValues=500,0,872,568,0,0,0
ParamValuesPostprocess\Blur=1000
ParamValuesBackground\FourCornerGradient=667,1000,944,712,1000,638,748,1000,96,1000,1000,0,1000,1000
ParamValuesPostprocess\ColorCyclePalette=0,250,500,333,267,528,1000,1000,1000,250,168
ParamValuesPostprocess\ScanLines=0,214,1000,1000,0,272
Enabled=1
Collapsed=1
Name=Reflector

[AppSet10]
App=Postprocess\Youlean Color Correction
FParamValues=0.508,0.568,0.504,0.508,0.508,0.484
ParamValues=508,568,504,508,508,484
Enabled=1
Name=Filter Color

[AppSet12]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fade in-out

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="<position y=""86""><p align=""center""><font face=""Chosence-Bold"" size=""2"" color=""#fff"">Image-Line Software</font></p></position>","<position y=""88  ""><p align=""center""><font face=""Chosence-regular"" size=""2"" color=""#CCCCC1"">Exclusive Sounds</font></p></position>"
Html=,,
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

