<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" FileVersion="2">
  <OnLoaded>
    <ZLibrary Comment="Robert Penner&apos;s easing equations by Kjell">
      <Source>
<![CDATA[// Sine

float easeInSine(float T)
{
  return sin((T-1)*PI*0.5)+1;
}

float easeOutSine(float T)
{
  return sin(T*PI*0.5);
}

float easeInOutSine(float T)
{
  return 0.5*(1-cos(T*PI));
}

// Quadratic

float easeInQuad(float T)
{
  return T*T;
}

float easeOutQuad(float T)
{
  return 0-T*(T-2);
}

float easeInOutQuad(float T)
{
  if(T < 0.5)
  {
    return 2*T*T;
  }
  else
  {
    return -2*T*T+4*T-1;
  }
}

// Cubic

float easeInCubic(float T)
{
  return T*T*T;
}

float easeOutCubic(float T)
{
  float F = T-1;
  return F*F*F+1;
}

float easeInOutCubic(float T)
{
  if(T < 0.5)
  {
    return 4*T*T*T;
  }
  else
  {
    float F = 2*T-2;
    return 0.5*F*F*F+1;
  }
}

// Quartic

float easeInQuart(float T)
{
  return T*T*T*T;
}

float easeOutQuart(float T)
{
  float F = T-1;
  return F*F*F*(1-T)+1;
}

float easeInOutQuart(float T)
{
  if(T < 0.5)
  {
    return 8*T*T*T*T;
  }
  else
  {
    float F = T-1;
    return -8*F*F*F*F+1;
  }
}

// Quintic

float easeInQuint(float T)
{
  return T*T*T*T*T;
}

float easeOutQuint(float T)
{
  float F = T-1;
  return F*F*F*F*F+1;
}

float easeInOutQuint(float T)
{
  if(T < 0.5)
  {
    return 16*T*T*T*T*T;
  }
  else
  {
    float F = 2*T-2;
    return  0.5*F*F*F*F*F+1;
  }
}

// Exponential

float easeInExpo(float T)
{
  return T ? pow(2,10*(T-1)) : T;
}

float easeOutExpo(float T)
{
  return T == 1 ? T : 1-pow(2,-10*T);
}

float easeInOutExpo(float T)
{
  if(T == 0 || T == 1)return T;

  if(T < 0.5)
  {
    return 0.5*pow(2,(20*T)-10);
  }
  else
  {
    return -0.5*pow(2,(-20*T)+10)+1;
  }
}

// Circular

float easeInCirc(float T)
{
  return 1-sqrt(1-T*T);
}

float easeOutCirc(float T)
{
  return sqrt((2-T)*T);
}

float easeInOutCirc(float T)
{
  if(T < 0.5)
  {
    return 0.5*(1-sqrt(1-4*T*T));
  }
  else
  {
    return 0.5*(sqrt(0-(2*T-3)*(2*T-1))+1);
  }
}

// Back

float easeInBack(float T)
{
  return T*T*T-T*sin(T*PI);
}

float easeOutBack(float T)
{
  float F = 1-T;
  return 1-(F*F*F-F*sin(F*PI));
}

float easeInOutBack(float T)
{
  float F;

  if(T < 0.5)
  {
    F = 2*T;
    return 0.5*(F*F*F-F*sin(F*PI));
  }
  else
  {
    F = 1-(2*T-1);
    return 0.5*(1-(F*F*F-F*sin(F*PI)))+0.5;
  }
}

// Elastic

float easeInElastic(float T)
{
  return sin(13*PI*0.5*T)*pow(2,10*(T-1));
}

float easeOutElastic(float T)
{
  return sin(-13*PI*0.5*(T+1))*pow(2,-10*T)+1;
}

float easeInOutElastic(float T)
{
  if(T < 0.5)
  {
    return 0.5*sin(13*PI*T)*pow(2,10*(2*T-1));
  }
  else
  {
    return 0.5*(sin(-13*PI*T)*pow(2,-10*(2*T-1))+2);
  }
}]]>
      </Source>
    </ZLibrary>
    <ZExternalLibrary ModuleName="ZGameEditor Visualizer">
      <Source>
<![CDATA[void ParamsNotifyChanged(xptr Handle,int Layer) { }
void ParamsChangeName(xptr Handle,int Layer, int Parameters, string NewName) { }
void ParamsWriteValueForLayer(xptr Handle, int Layer,int Param, float NewValue) { }
void ParamsUpdateComboItems(xptr Handle, int Layer,int Param, string[] NewItems) { }]]>
      </Source>
    </ZExternalLibrary>
    <ZLibrary>
      <Source>
<![CDATA[void getTempo(){
  int tSelect1,tSelect2;

  tSelect1=Parameters[12]*1000;
  tSelect2=Parameters[13]*1000;
  if(tSelect1==0)tempo1=frac(SongPositionInBeats*4.0);
  if(tSelect1==1)tempo1=frac(SongPositionInBeats*2.0);
  if(tSelect1==2)tempo1=frac(SongPositionInBeats);
  if(tSelect1==3)tempo1=frac(SongPositionInBeats*.5);
  if(tSelect1==4)tempo1=frac(SongPositionInBeats*.25);
  if(tSelect1==5)tempo1=frac(SongPositionInBeats*.125);
  if(tSelect1==6)tempo1=frac(SongPositionInBeats*.0625);
  if(tSelect1==7)tempo1=frac(SongPositionInBeats*.03125);
  if(tSelect1==8)tempo1=frac(SongPositionInBeats*.03125*.5);
  if(tSelect1==9)tempo1=frac(SongPositionInBeats*.03125*.25);

  if(tSelect2==0)tempo2=frac(SongPositionInBeats*4.0);
  if(tSelect2==1)tempo2=frac(SongPositionInBeats*2.0);
  if(tSelect2==2)tempo2=frac(SongPositionInBeats);
  if(tSelect2==3)tempo2=frac(SongPositionInBeats*.5);
  if(tSelect2==4)tempo2=frac(SongPositionInBeats*.25);
  if(tSelect2==5)tempo2=frac(SongPositionInBeats*.125);
  if(tSelect2==6)tempo2=frac(SongPositionInBeats*.0625);
  if(tSelect2==7)tempo2=frac(SongPositionInBeats*.03125);
  if(tSelect2==8)tempo2=frac(SongPositionInBeats*.03125*.5);
  if(tSelect2==9)tempo2=frac(SongPositionInBeats*.03125*.25);
  //t1=SongPositionInBeats;
}

void setBaseLabel(){
  int p1=1;
  int p2=5;
  for(int i=0; i<2; i++){
    switch (Parameters[8+i]*1000){
      case 0: {ParamsChangeName(FLPluginHandle,LayerNr, p1, "Base Level"); break;}
      case 1: {ParamsChangeName(FLPluginHandle,LayerNr, p1, "Base Level"); break;}
      case 2: {ParamsChangeName(FLPluginHandle,LayerNr, p1, "Base Level"); break;}
      case 3: {ParamsChangeName(FLPluginHandle,LayerNr, p1, "Phase Shift"); break;}
      case 4: {ParamsChangeName(FLPluginHandle,LayerNr, p1, "Phase Shift"); break;}
      case 5: {ParamsChangeName(FLPluginHandle,LayerNr, p1, "Phase Shift"); break;}
      case 6: {ParamsChangeName(FLPluginHandle,LayerNr, p1, "disabled"); break;}
      case 7: {ParamsChangeName(FLPluginHandle,LayerNr, p1, "disabled"); break;}
      case 8: {ParamsChangeName(FLPluginHandle,LayerNr, p1, "Phase Shift"); break;}
    }
    p1=p2;
  }
}

//if mapping formula is not tempo based, change the label
void setTempoPamamName(){
for(int i=0; i<2; i++){
  if(Parameters[8+i]*1000==0)ParamsChangeName(FLPluginHandle,LayerNr, 12+i, "disabled");
  if(Parameters[8+i]*1000==1)ParamsChangeName(FLPluginHandle,LayerNr, 12+i, "disabled");
  if(Parameters[8+i]*1000==2)ParamsChangeName(FLPluginHandle,LayerNr, 12+i, "disabled");
  if(Parameters[8+i]*1000==3)ParamsChangeName(FLPluginHandle,LayerNr, 12+i, "Tempo Base");
  if(Parameters[8+i]*1000==4)ParamsChangeName(FLPluginHandle,LayerNr, 12+i, "Tempo Base");
  if(Parameters[8+i]*1000==5)ParamsChangeName(FLPluginHandle,LayerNr, 12+i, "Tempo Base");
  if(Parameters[8+i]*1000==6)ParamsChangeName(FLPluginHandle,LayerNr, 12+i, "disabled");
  if(Parameters[8+i]*1000==7)ParamsChangeName(FLPluginHandle,LayerNr, 12+i, "Tempo Base");
  if(Parameters[8+i]*1000==8)ParamsChangeName(FLPluginHandle,LayerNr, 12+i, "Tempo Base");
  }
}]]>
      </Source>
    </ZLibrary>
    <ZLibrary Comment="Mapping fomulas">
      <Source>
<![CDATA[float defaultMap(int shakeNum){
  float AvLevel=0;
  for (int b=0; b<32; b++) AvLevel+=SpecBandArray[b*FFTmulti];
  //ParamsChangeName(FLPluginHandle,LayerNr, 1, "Base Laevel");
  if(shakeNum==1){
    if (AvLevelSmooth01<AvLevel) AvLevelSmooth01+=(AvLevel-AvLevelSmooth01)*1f/32f;
    AvLevelSmooth01=(AvLevelSmooth01*.66*Parameters[0])+(Parameters[1]);
    return AvLevelSmooth01;
  }
  if(shakeNum==2){
    if (AvLevelSmooth02<AvLevel) AvLevelSmooth02+=(AvLevel-AvLevelSmooth02)*1f/32f;
    AvLevelSmooth02=(AvLevelSmooth02*.66*Parameters[4])+(Parameters[5]);
    return AvLevelSmooth02;
  }
}

float inverted(int shakeNum){
  float AvLevel=0;
  for (int b=0; b<32; b++) AvLevel+=SpecBandArray[b*FFTmulti];
  //ParamsChangeName(FLPluginHandle,LayerNr, 1, "Base Laevel");
  if(shakeNum==1){
    if (AvLevelSmooth01<AvLevel) AvLevelSmooth01+=(AvLevel-AvLevelSmooth01)*1f/32f;
    AvLevelSmooth01=(AvLevelSmooth01*.66*Parameters[0])+(Parameters[1]);
    return 1f-AvLevelSmooth01;
  }
  if(shakeNum==2){
    if (AvLevelSmooth02<AvLevel) AvLevelSmooth02+=(AvLevel-AvLevelSmooth02)*1f/32f;
    AvLevelSmooth02=(AvLevelSmooth02*.66*Parameters[4])+(Parameters[5]);
    return 1f-AvLevelSmooth02;
  }
}

float maxBand(int shakeNum){
  float mb=0;
  for(int i=0; i<SpecBandArray.SizeDim1; i++)mb = mb < SpecBandArray[i] ? SpecBandArray[i]:mb;
  if(shakeNum==1)return mb*Parameters[0]+Parameters[1];
  if(shakeNum==2)return mb*Parameters[4]+Parameters[5];
}

float bpm(int shakeNum){
  if(shakeNum==1)return (tempo1*Parameters[0]+Parameters[1])*isPlaying;
  if(shakeNum==2)return (tempo2*Parameters[4]+Parameters[5])*isPlaying;
}

float invbpm(int shakeNum){
  if(shakeNum==1)return (1.0+Parameters[1])-(tempo1*Parameters[0]);
  if(shakeNum==2)return (1.0+Parameters[5])-(tempo2*Parameters[4]);
}

float circ(int shakeNum){
  float mb=0;
  for(int i=0; i<SpecBandArray.SizeDim1; i++)mb = mb < SpecBandArray[i] ? SpecBandArray[i]:mb;
  if(shakeNum==1)return cos(tempo1*2.0*PI*Parameters[0])*mb+.5+Parameters[1];
  if(shakeNum==2)return sin(tempo2*2.0*PI*Parameters[4])*mb+.5+Parameters[5];
}


float switchOn(int shakeNum){
  if(shakeNum==1)return Parameters[0];
  if(shakeNum==2)return Parameters[4];
}

float squareWave(int shakeNum){
  bpm(shakeNum==1);
  if(shakeNum==1){
    if(tempo1>.5)return Parameters[0];
    else return 1.0-Parameters[0];
  }

  if(shakeNum==2){
  if(tempo2>.5)return Parameters[4];
  else return 1.0-Parameters[4];
  }
}

float invBPM_invPhase(int shakeNum){
  if(shakeNum==1)return (1.0-Parameters[1])-(tempo1*Parameters[0]);
  if(shakeNum==2)return (1.0-Parameters[5])-(tempo2*Parameters[4]);
}]]>
      </Source>
    </ZLibrary>
    <ZExpression>
      <Expression>
<![CDATA[FFTmulti=SpecBandArray.SizeDim1/32;
AvLevelSmooth01=0;
AvLevelSmooth02=0;]]>
      </Expression>
    </ZExpression>
  </OnLoaded>
  <OnUpdate>
    <ZExpression>
      <Expression>
<![CDATA[//Bar Count
int beats=floor(SongPositionInBeats);
if(isPlaying)bars=beats/4;
else bars=0;

//Shake Timer Start and end
Shake1_Start=Parameters[16]*1000;
Shake1_End=Parameters[17]*1000;
Shake2_Start=Parameters[18]*1000;
Shake2_End=Parameters[19]*1000;

//if end is none make it a big number
if(Shake1_End==0)Shake1_End=100000;
if(Shake2_End==0)Shake2_End=100000;

//if the end time is less than the start time make it greater
if(Shake1_End<=Shake1_Start)ParamsWriteValueForLayer(FLPluginHandle, LayerNr,17, Parameters[17]+.00125);
if(Shake2_End<=Shake2_Start)ParamsWriteValueForLayer(FLPluginHandle, LayerNr,19, Parameters[18]+.00125);

//Set the base timing to note length
getTempo();
setTempoPamamName();

//Check if playing
isPlaying=SongPositionInSeconds ? 1:0;


//Write to parameters
int layer1,layer2,parameter1,parameter2;
layer1=Parameters[2]*1000;
parameter1=Parameters[3]*1000;
layer2=Parameters[6]*1000;
parameter2=Parameters[7]*1000;

//Values to determine if the current bar count is withing the start and end ranges.
int go1,go2;
go1=(bars>=Shake1_Start)&&(bars<Shake1_End);
go2=(bars>=Shake2_Start)&&(bars<Shake2_End);

//set base label acording to what mapping formula is selected
setBaseLabel();


//only write values if all condition are valid
if(Parameters[0]>0.0&&!Parameters[10]&&go1){
  ParamsChangeName(FLPluginHandle,LayerNr, 0, "Shake1 Level");
  float v1;
  switch (Parameters[8]*1000){
    case 0: {v1=defaultMap(1); break;}
    case 1: {v1=inverted(1); break;}
    case 2: {v1=maxBand(1); break;}
    case 3: {v1=bpm(1); break;}
    case 4: {v1=invbpm(1); break;}
    case 5: {v1=circ(1); break;}
    case 6: {v1=switchOn(1); break;}
    case 7: {v1=squareWave(1); break;}
    case 8: {v1=invBPM_invPhase(1); break;}
  }
  //if the value of the parmeter is >1||<0 and wrap is on roll it over.
  if (Parameters[14])v1=frac(abs(v1));
  v1*=isPlaying;
  ParamsWriteValueForLayer(FLPluginHandle, layer1, parameter1, v1);
}
else {
  if(Parameters[10])ParamsChangeName(FLPluginHandle,LayerNr, 0, "Shake1 Stop");
  else ParamsChangeName(FLPluginHandle,LayerNr, 0, "Shake1 off");
  //ParamsChangeName(FLPluginHandle,LayerNr, 1, "Not Active");
}


//shake2
if(Parameters[4]>0.0&&!Parameters[11]&&go2){
  ParamsChangeName(FLPluginHandle,LayerNr, 4, "Shake2 Level");
  float v2;
  switch (Parameters[9]*1000){
    case 0: {v2=defaultMap(2); break;}
    case 1: {v2=inverted(2); break;}
    case 2: {v2=maxBand(2); break;}
    case 3: {v2=bpm(2); break;}
    case 4: {v2=invbpm(2); break;}
    case 5: {v2=circ(2); break;}
    case 6: {v2=switchOn(2); break;}
    case 7: {v2=squareWave(2); break;}
    case 8: {v2=invBPM_invPhase(2); break;}
  }
  //if the value of the parmeter is >1||<0 and wrap is on roll it over.
  if (Parameters[15])v2=frac(abs(v2));
  v2*=isPlaying;
  ParamsWriteValueForLayer(FLPluginHandle, layer2, parameter2, v2);
}
else {
  if(Parameters[11])ParamsChangeName(FLPluginHandle,LayerNr, 4, "Shake1 Stop");
  else ParamsChangeName(FLPluginHandle,LayerNr, 4, "Shake2 off");
  //ParamsChangeName(FLPluginHandle,LayerNr, 5, "Not Active");
}

//init parmeter values
//set init time - safty net to prevent accidentally setting wrong parameter.

if(Parameters[22]){
  if(setInit1==-1){
    setInit1=1;
    holdInit1[0]=layer1;
    holdInit1[1]=parameter1;
  }
  //Turn off init if user chages the parmeter ord layer value to prevent accidental parameter changes.
  if(setInit1==1)if(holdInit1[0]!=layer1||holdInit1[1]!=Parameter1)ParamsWriteValueForLayer(FLPluginHandle, LayerNr, 22, 0);
  //init parameter on start
  if(layer1!=LayerNr&&!isPlaying&&Parameters[22])ParamsWriteValueForLayer(FLPluginHandle, layer1, parameter1, Parameters[20]);
  //init parameter after end if hold is not enable
  if(!Parameters[24])if(layer1!=LayerNr&&isPlaying&&bars>Shake1_End&&Parameters[22])ParamsWriteValueForLayer(FLPluginHandle, layer1, parameter1, Parameters[20]);
}
else setInit1=-1;


//init for shake2
if(Parameters[23]){
  if(setInit2==-1){
    setInit2=1;
    holdInit2[0]=layer2;
    holdInit2[1]=parameter2;
  }

  if(setInit2==1)if(holdInit2[0]!=layer2||holdInit2[1]!=Parameter2)ParamsWriteValueForLayer(FLPluginHandle, LayerNr, 23, 0);


  if(layer2!=LayerNr&&!isPlaying&&Parameters[23])ParamsWriteValueForLayer(FLPluginHandle, layer2, parameter2, Parameters[21]);
  if(!Parameters[25])if(layer2!=LayerNr&&isPlaying&&bars>Shake2_End&&Parameters[23])ParamsWriteValueForLayer(FLPluginHandle, layer2, parameter2, Parameters[21]);
}
else setInit2=-1;]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <Content>
    <Group>
      <Children>
        <Array Name="Parameters" SizeDim1="28" Persistent="255">
          <Values>
<![CDATA[78DA6360200D2C9BED620DC2F8D45CF099C974443814AB1C001FBA0664]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Shake1  Level
Base Level
Layer @list1000: A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,L26,L27,L28,L29,L30,L31,L32,L33,L34,L35,L36,L37,L38,L39,L40,L41,L42,L43,L44,L45,L46,L47,L48,L49,L50,L51,L52,L53,L54,L55,L56,L57,L58,L59,L60,L61,L62,L63,L64,L65,L66,L67,L68,L69,L70,L71,L72,L73,L74,L75,L76,L77,L78,L79,L80,L81,L82,L83,L84,L85,L86,L87,L88,L89,L90,L91,L92,L93,L94,L95,L96,L97,L98,L99
Parameter @list1000: 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32
Shake2  Level
Base Level
Layer @list1000: A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,L26,L27,L28,L29,L30,L31,L32,L33,L34,L35,L36,L37,L38,L39,L40,L41,L42,L43,L44,L45,L46,L47,L48,L49,L50,L51,L52,L53,L54,L55,L56,L57,L58,L59,L60,L61,L62,L63,L64,L65,L66,L67,L68,L69,L70,L71,L72,L73,L74,L75,L76,L77,L78,L79,L80,L81,L82,L83,L84,L85,L86,L87,L88,L89,L90,L91,L92,L93,L94,L95,L96,L97,L98,L99
Parameter @list1000: 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32
Shake1 map @list1000: Peak,InvPeak,"Max Band",BPM,InvBPM,CircX,switch,"Square Wave","InvBPM-P"
Shake2 map @list1000: Peak,InvPeak,"Max Band",BPM,InvBPM,CircY,switch,"Square Wave","InvBPM-P"
Stop Shake1 @checkbox
Stop Shake2 @checkbox
Tempo Shake1 @list1000: 16th,8th,1/4,1/2,Bar,"2 Bars","4 bars","8 bars","16 bars","32 bars"
Tempo Shake2 @list1000: 16th,8th,1/4,1/2,Bar,"2 Bars","4 bars","8 bars","16 bars","32 bars"
Shake1 Wrap @checkbox
Shake2 Wrap @checkbox
Shake1 Start @list1000: "Bar 1",2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,62,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,190,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240
Shake1 End @list1000: none,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,62,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,190,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240
Shake2 Start @list1000: "Bar 1",2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,62,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,190,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240
Shake2 End @list1000: none,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,62,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,190,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240
Shake1 Init
Shake2 Init
Set Init1 @checkbox
Set Init2 @checkbox
Skake1 Hold @checkbox
Skake2 Hold @checkbox ]]>
          </StringValue>
        </Constant>
        <Array Name="SpecBandArray" SizeDim1="32"/>
        <Variable Name="AvLevel"/>
        <Variable Name="AvLevelSmooth01"/>
        <Variable Name="AvLevelSmooth02"/>
        <Variable Name="FFTmulti" Type="1"/>
        <Variable Name="FLPluginHandle" Comment="Set by plugin" Type="9"/>
        <Variable Name="LayerNr" Comment="Set by plugin" Type="1"/>
        <Constant Name="AuthorInfo" Type="2">
          <StringValue>
<![CDATA[StevenM
]]>
          </StringValue>
        </Constant>
        <Variable Name="SongPositionInBeats"/>
        <Variable Name="SongPositionInSeconds"/>
        <Variable Name="isPlaying"/>
        <Variable Name="tempo1"/>
        <Variable Name="tempo2"/>
        <Variable Name="Shake1_Wrap" Type="1"/>
        <Variable Name="Shake2_Wrap"/>
        <Variable Name="Shake1_Start"/>
        <Variable Name="Shake2_Start"/>
        <Variable Name="Shake1_End"/>
        <Variable Name="Shake2_End"/>
        <Variable Name="bars"/>
        <Variable Name="holdInit1" Type="6"/>
        <Variable Name="holdInit2" Type="6"/>
        <Variable Name="setInit1"/>
        <Variable Name="setInit2"/>
      </Children>
    </Group>
  </Content>
</ZApplication>
