FLhd   0  ` FLdt�  �20.8.0.2091 �+  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4               A                  :   d   j    �    �HQV ��;�  ﻿[General]
GlWindowMode=1
LayerCount=19
FPS=1
AspectRatio=16:9
LayerOrder=0,1,7,9,6,2,3,11,12,5,8,10,13,17,14,15,16,18,4

[AppSet0]
App=HUD\HUD Graph Polar
FParamValues=0.207,0.54,1,0,0.516,0.482,0.25,4,0.5,0,1,0.378,0.695,0,0.25,0.5,0,0,0,3,0.478,0
ParamValues=207,540,1000,0,516,482,250,4,500,0,1,378,695,0,250,500,0,0,0,3,478,0
Enabled=1
Collapsed=1

[AppSet1]
App=HUD\HUD 3D
FParamValues=0.063,1,0.5,0.5,0.5,0.5,0.5,0.5,0.443,0.4165,0.5,0.5,0.5,0.9,0.299,0.5,0.5,0.514,0.5,0.5,0.23,0.225,0.448,1,1,0,0,1,1,0
ParamValues=63,1,500,500,500,500,500,500,443,416,500,500,500,900,299,500,500,514,500,500,230,225,448,1,1,0,0,1000,1000,0
Enabled=1
UseBufferOutput=1
Collapsed=1
ImageIndex=1

[AppSet7]
App=Image effects\Image
FParamValues=0,0,0,0,0.687,0.106,0.338,0,0,0,1,0,0,0
ParamValues=0,0,0,0,687,106,338,0,0,0,1,0,0,0
Enabled=1
Collapsed=1

[AppSet9]
App=Postprocess\Edge Detect
FParamValues=1,0.376,1,0.654,0.351,0.109
ParamValues=1000,376,1000,654,351,109
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPostprocess\Blooming=0,605,431,599,541,800,147,930,863
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,574,500,500,500,500,500
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPostprocess\Blur=170
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPostprocess\Dot Matrix=1000,633,571,0,517,500,1000,1000
Enabled=1
Collapsed=1

[AppSet6]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
Collapsed=1

[AppSet2]
App=HUD\HUD Text
FParamValues=0.5,0.54,0.256,0,0.639,0.08,0.5,0,0,0,0,69,0.194,0,2,3,0.277,0.5,0.5,0,0.2519,0,0,1,0
ParamValues=500,540,256,0,639,80,500,0,0,0,0,69,194,0,2,3,277,500,500,0,251,0,0,1,0
ParamValuesText\TextTrueType=0,0,0,0,0,500,500,0,0,1000,440
Enabled=1
LayerPrivateData=78010B4ACC492D4FACD40D4A4D2FCD492CD22B294963184900000EFA0751

[AppSet3]
App=HUD\HUD Text
FParamValues=0.5,0.54,0.256,0,0.93,0.837,0.75,0.25,0,0,0.032,69,0.156,0,2,3,1,0.5,0.5,0,1,0,0,2,0
ParamValues=500,540,256,0,930,837,750,250,0,0,32,69,156,0,2,3,1000,500,500,0,1000,0,0,2,0
ParamValuesHUD\HUD 3D=0,1000,500,500,500,500,500,500,443,400,500,500,500,900,299,500,500,514,500,500,230,225,448,1000,1000,0,0,1000,1000,500
Enabled=1
LayerPrivateData=78010B4ACC492D4FACD40D4A4D2FCD492CD22B294963184900000EFA0751

[AppSet11]
App=HUD\HUD Text
FParamValues=0.5,0.54,0.256,0,0.93,0.046,0.75,0.493,0,0,0,70,0.363,0,0,3,1,0.5,0.5,0,1,0,0,2,0
ParamValues=500,540,256,0,930,46,750,493,0,0,0,70,363,0,0,3,1000,500,500,0,1000,0,0,2,0
Enabled=1
LayerPrivateData=78010B4ACC492D4FACD40DC9C8CCD32B29496318610000E4970612

[AppSet12]
App=HUD\HUD Text
FParamValues=0.5,0.54,0.256,0,0.93,0.286,0.75,0.691,0,0,0,70,0.363,0,0,3,1,0.5,0.5,0,1,0,0,2,0
ParamValues=500,540,256,0,930,286,750,691,0,0,0,70,363,0,0,3,1000,500,500,0,1000,0,0,2,0
Enabled=1
LayerPrivateData=78010B4ACC492D4FACD40DC9C8CCD32B29496318610000E4970612

[AppSet5]
App=Background\FourCornerGradient
FParamValues=2,0.311,0.68,1,1,0.817,1,1,0.496,1,1,0.564,1,0.67
ParamValues=2,311,680,1000,1000,817,1000,1000,496,1000,1000,564,1000,670
Enabled=1
Collapsed=1

[AppSet8]
App=HUD\HUD Prefab
FParamValues=455,0,0.54,1,0,0.179,0.267,0.4,1,0.839,4,0,0.5,1,0.34,0,1,0
ParamValues=455,0,540,1000,0,179,267,400,1000,839,4,0,500,1,340,0,1000,0
Enabled=1
Collapsed=1
LayerPrivateData=78012BCE4DCCC95148CD49CD4DCD2B298E292E2C2D4A05918945A9BA0626C67A9939650CC319000075170D4D

[AppSet10]
App=HUD\HUD Prefab
FParamValues=54,0,0.54,1,0,0.183,0.438,0.267,1,0.357,4,2,0.5,1,0.292,0,0.4216,0
ParamValues=54,0,540,1000,0,183,438,267,1000,357,4,2,500,1,292,0,421,0
Enabled=1
Collapsed=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D0C8DF43273CA184600000028C90AAF

[AppSet13]
App=HUD\HUD Text
FParamValues=0,0.54,1,0,0.044,0.06,0.5,0.869,0,0,0,54,0.239,0,0,0,0.259,0.5,0.5,0,1,0.147,0.738,0,0
ParamValues=0,540,1000,0,44,60,500,869,0,0,0,54,239,0,0,0,259,500,500,0,1000,147,738,0,0
Enabled=1
LayerPrivateData=7801F3CB2F4BF4CDCFCBD72B29496318810000905404AA

[AppSet17]
App=HUD\HUD Text
FParamValues=0.032,0.54,1,0,0.038,0.488,0.5,0.972,0,0,0,54,0.308,0,0,0,0.284,0.415,0.5,0,1,0.147,0.681,0,0
ParamValues=32,540,1000,0,38,488,500,972,0,0,0,54,308,0,0,0,284,415,500,0,1000,147,681,0,0
Enabled=1
LayerPrivateData=7801F3CB2F4BF4CDCFCBD72B29496318810000905404AA

[AppSet14]
App=HUD\HUD Graph Linear
FParamValues=0.9,0.54,0.926,0,0.032,0.737,0.477,0.687,0.555,7,0.75,1,0.322,0.445,0,0.25,0,0,0.5,0,0.827,0
ParamValues=900,540,926,0,32,737,477,687,555,7,750,1,322,445,0,250,0,0,500,0,827,0
Enabled=1
Collapsed=1

[AppSet15]
App=HUD\HUD Graph Linear
FParamValues=0.8,0.54,0.926,0,0.032,0.737,0.477,0.687,0.555,7,0.75,1,0.322,0.445,0,0.5,0,0,0.4,0,0.827,0
ParamValues=800,540,926,0,32,737,477,687,555,7,750,1,322,445,0,500,0,0,400,0,827,0
Enabled=1
Collapsed=1

[AppSet16]
App=HUD\HUD Graph Linear
FParamValues=0.7,0.54,0.926,0,0.032,0.737,0.477,0.687,0.555,7,0.75,1,0.322,0.445,0,0.75,0,0,0.3,0,0.827,0
ParamValues=700,540,926,0,32,737,477,687,555,7,750,1,322,445,0,750,0,0,300,0,827,0
Enabled=1
Collapsed=1

[AppSet18]
App=HUD\HUD Free Line
FParamValues=0,0.54,0.97,0,0.333,0.585,0.449,0.024,7,0.304,5,0.062,1,1,1,0.28,0.75
ParamValues=0,540,970,0,333,585,449,24,7,304,5,62,1,1000,1,280,750
ParamValuesHUD\HUD Callout Line=0,540,1000,0,150,240,26,534,909,151,364,158,58,500,128,1000
Enabled=1
Collapsed=1
LayerPrivateData=78015BBD6A951DC79A39760C0C0DF6AB5769D94BEBC7DA02D976AB81E29A31FD4071077B207F3F031C8CB22141311A0EF8C20100A4D8468A

[AppSet4]
App=Misc\Automator
FParamValues=1,3,21,4,0,0.01,0.4,1,11,17,3,0.19,0.01,0.654,1,10,2,3,0,0.01,0.75,1,2,10,2,0.45,0,0.52
ParamValues=1,3,21,4,0,10,400,1,11,17,3,190,10,654,1,10,2,3,0,10,750,1,2,10,2,450,0,520
ParamValuesHUD\HUD Text=0,500,0,0,756,943,500,874,0,0,0,3,186,0,667,750,217,500,500,0,1000,0,0,0,1000
Enabled=1
Collapsed=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=256000
Uncompressed=0
Supersample=0

[UserContent]
Text="Soft tantalizing petals                             Of the perfect flower                               Giving peace and calmness                           Over life's ongoing agony                           Hope brings a mind's eye                            Visions of love's heart                             Talks of simple past                                Greater than perfection                             Living an unpredictable life                        Not knowing where you'll be                         Showing unpredictable feelings                      Hoping that you'll soon see                         The moon's penetrating rays                         Lighting the cool night air                         Filling the heart's emptiness                       With something that cares                           Love is a needful thing                             Holding fast and true                               Forever will it sing                                A tune greater than blue...                         Blue Flowers","Russell Sivey",Blue,Flowers,[title],[author]
Html="<p align=""center"">[textline]</p>"
VideoUseSync=0
Filtering=0

[Detached]
Top=256
Left=1250
Width=444
Height=394

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

