FLhd   0 * ` FLdt�  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV գ"  ﻿[General]
GlWindowMode=1
LayerCount=12
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,1,3,6,7,8,9,10,2,4,5,11
WizardParams=563

[AppSet0]
App=Background\SolidColor
FParamValues=0,0.624,0.756,0.908
ParamValues=0,624,756,908
Enabled=1
Collapsed=1
Name=Background

[AppSet1]
App=Peak Effects\Polar
FParamValues=0.972,0,0,0.476,0.18,0.5,0.5,1,0.5,0.324,0.468,1,1,0.456,0,0.504,1,1,1,1,0.308,0.32,0,1
ParamValues=972,0,0,476,180,500,500,1000,500,324,468,1000,1000,456,0,504,1000,1000,1000,1,308,320,0,1
Enabled=1
Collapsed=1
Name=EQ 1

[AppSet3]
App=Peak Effects\Polar
FParamValues=0.972,0.708,0,0.58,0.284,0.5,0.5,1,0.5,0.324,0.468,1,1,0.012,0,0.504,1,1,1,1,0.308,0.32,0,1
ParamValues=972,708,0,580,284,500,500,1000,500,324,468,1000,1000,12,0,504,1000,1000,1000,1,308,320,0,1
Enabled=1
Collapsed=1
Name=EQ 2

[AppSet6]
App=Peak Effects\Polar
FParamValues=0.968,0.64,0.764,0,0.284,0.5,0.5,1,1,0.324,0.544,1,1,0.012,0,0.504,1,1,1,1,0.308,0.32,0,1
ParamValues=968,640,764,0,284,500,500,1000,1000,324,544,1000,1000,12,0,504,1000,1000,1000,1,308,320,0,1
Enabled=1
Collapsed=1
Name=EQ 3

[AppSet7]
App=Peak Effects\Polar
FParamValues=0.964,1,1,0,0.152,0.5,0.5,1,1,0.324,0.544,1,1,0.308,0,0.2,1,0.968,0.672,1,0.308,0.32,0,1
ParamValues=964,1000,1000,0,152,500,500,1000,1000,324,544,1000,1000,308,0,200,1000,968,672,1,308,320,0,1
Enabled=1
Collapsed=1
Name=EQ 4

[AppSet8]
App=Peak Effects\Polar
FParamValues=0.968,0.612,0.672,0.368,0.152,0.5,0.5,1,1,0.324,0.544,1,1,0.66,0,0.2,1,0.968,0.672,1,0.308,0.32,0,1
ParamValues=968,612,672,368,152,500,500,1000,1000,324,544,1000,1000,660,0,200,1000,968,672,1,308,320,0,1
Enabled=1
Collapsed=1
Name=EQ 5

[AppSet9]
App=Postprocess\AudioShake
FParamValues=0.232,0,1,0.5,0.1,0.9
ParamValues=232,0,1,500,100,900
ParamValuesPostprocess\Point Cloud High=0,330,330,500,500,320,444,527,234,625,156,0,0,0,0
ParamValuesPostprocess\Point Cloud Low=0,712,422,500,536,448,508,503,706,625,156,0,676,0,667
Enabled=1
Collapsed=1
Name=Boom EFX

[AppSet10]
App=Postprocess\Youlean Motion Blur
FParamValues=0.175,1,0.492,0.168,0,0,0
ParamValues=175,1,492,168,0,0,0
ParamValuesPostprocess\RGB Shift=664,472,0,600,700,200
ParamValuesPostprocess\Blur=1000
ParamValuesPostprocess\FrameBlur=60,1000,1000,552,0,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesPostprocess\Youlean Bloom=324,80,458,262
ParamValuesPostprocess\ScanLines=0,71,1000,1000,0,0
ParamValuesPostprocess\AudioShake=232,0,333,500,100,900
Enabled=1
Collapsed=1
Name=Master Out

[AppSet2]
App=Text\TextTrueType
FParamValues=0.248,0,0,0,0,0.5,0.5,0,0,0,0.5
ParamValues=248,0,0,0,0,500,500,0,0,0,500
Enabled=1
Collapsed=1
Name=Main Text

[AppSet4]
App=Background\FourCornerGradient
FParamValues=7,0,0.392,1,1,0.666,0.74,0.784,0.144,0.816,0.804,0.866,1,1
ParamValues=7,0,392,1000,1000,666,740,784,144,816,804,866,1000,1000
ParamValuesHUD\HUD Meter Radial=180,952,880,8,140,116,0,0,500,500,268,242,1,162,600,0,0,100,508,1000
ParamValuesHUD\HUD Graph Polar=0,500,0,0,500,436,286,444,500,0,1000,0,1000,0,250,500,200,0,0,0,500,1000
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,648,500,0,500,500,500,500
ParamValuesBackground\SolidColor=0,624,756,908
Enabled=1
Collapsed=1
Name=Filter Color HUE

[AppSet5]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fade in-out

[AppSet11]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""4"" y=""5""><p><font face=""American-Captain"" size=""4"" color=""#fff"">[author]</font></p></position>","<position x=""4"" y=""8""><p><font face=""Chosence-Bold"" size=""3"" color=""#f4f4f4"">[title]</font></p></position>","<position x=""4"" y=""14""><p> <font face=""Chosence-Bold"" size=""3"" color=""#fff"">[comment]</font></p></position>"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

