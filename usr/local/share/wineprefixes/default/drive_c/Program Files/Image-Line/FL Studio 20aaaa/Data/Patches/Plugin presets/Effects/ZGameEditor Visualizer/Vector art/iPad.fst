FLhd   0  ` FLdt�  �20.0.2 �    %�.Z G a m e E d i t o r   V i s u a l i z e r   �4               A                  9   Z   �  B  �    �HQV պ(6  ﻿[General]
GlWindowMode=1
LayerCount=10
FPS=1
DmxOutput=0
DmxDevice=0
OutputControllers=0
MidiPort=0
Aspect=1
CameraMode=0
CameraZoom=0
CameraRotate=0
LayerOrder=3,0,1,5,7,2,4,8,9,10
Info=

[AppSet3]
App=Peak Effects\SplinePeaks
ParamValues=520,833,0,496,500,500,1000
ParamValuesTerrain\GoopFlow=0,0,0,844,500,500,500,0,500,500,500,500,500,500,500,500
ParamValuesScenes\Postcard=0,1000,484,1000,500,500,0,0,0,0,0
ParamValuesText\MeshText=0,0,0,616,502,500,500,500,500,500,500,500,500,500,500,500,100,0,0,0
ParamValuesPostprocess\ColorCyclePalette=500,0,812,0,800,1000,0,500,40,500,0
ParamValuesPostprocess\ScanLines=0,800,0,0,0,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
Collapsed=1
ImageIndex=2
MeshIndex=0

[AppSet0]
App=Background\ItsFullOfStars
ParamValues=0,0,0,500,496,500,500,500,0,0,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPeak Effects\VectorScope=0,0,0,0,1000,165,667,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesPeak Effects\Polar=912,508,1000,0,800,500,500,280,1000,344,192,632,0,500,1000
ParamValuesParticles\fLuids=0,0,0,0,876,500,500,0,0,0,500,500,500,0,500,250,500,0,0,500,500,500,0,0,500,0,0,250,500,0,0,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPeak Effects\Linear=0,0,1000,1000,750,500,84,1000,0,500,0,308,0,1000,0,1000,0,0,1000,1000,0,0,1000,1000,1000
ParamValuesCanvas effects\Flaring=652,0,0,1000,1000,1000,0,64,0,200,500
ParamValuesImage effects\Image=0,0,0,0,0,500,500,0,0,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,557,500,500,500,500,500
ParamValuesBackground\FourCornerGradient=0,1000,1000,752,1000,0,1000,1000,524,984,1000,750,1000,1000
ParamValuesPostprocess\Blur=0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
Collapsed=1
ImageIndex=2
MeshIndex=0

[AppSet1]
App=Particles\ColorBlobs
ParamValues=0,0,28,0,200,500,500,1000,0,80
ParamValuesParticles\BugTails=0,0,640,0,852,500,500,1000,1000,1000,1000,1000
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,41,500,500,500,500,500
ParamValuesMisc\CoreDump=0,0,0,648,1000,500,500,0,0,236,0,0,0,0,44,584,1000
ParamValuesObject Arrays\CubesGrasping=0,492,1000,0,500,500,500,760,1000,0,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
Collapsed=1
ImageIndex=2
MeshIndex=0

[AppSet5]
App=Postprocess\ColorCyclePalette
ParamValues=1,1,6,0,13,1000,0,0,152,0,0
ParamValuesImage effects\Image=0,0,0,0,1000,500,500,0,0,0,0
ParamValuesPeak Effects\Polar=0,476,904,712,636,500,500,500,1000,156,336,480,0,500,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
Collapsed=0
ImageIndex=2
MeshIndex=0

[AppSet7]
App=Postprocess\Projection
ParamValues=130,388,384,0,377,32,147,501,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
Collapsed=0
ImageIndex=2
MeshIndex=0

[AppSet2]
App=Image effects\Image
ParamValues=0,0,0,0,1000,500,500,0,0,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
Collapsed=0
ImageIndex=0
MeshIndex=0

[AppSet4]
App=Text\TextTrueType
ParamValues=0,0,0,0,0,500,828,0,0,0,500
ParamValuesParticles\PlasmaFlys=500,836,500,692,500,204,560,192,80,100,0,0,0,0
ParamValuesImage effects\Image=1000,0,1000,1000,1000,0,500,0,0,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,799,500,500,500,500,500
ParamValuesPeak Effects\JoyDividers=1000,348,180,664,0,0,0,1000,436,100,696,650,510
ParamValuesParticles\ReactiveMob=0,0,424,52,500,64,0,692,0,1000,0,64,1000,284,472,0,0,1000,36,0,0,284,1000,0,0,100,0,0,0,0
Input=0
Enabled=1
UseBufferOutput=1
BufferRenderQuality=0
Collapsed=0
ImageIndex=2
MeshIndex=0

[AppSet8]
App=Background\SolidColor
ParamValues=0,833,0,685
ParamValuesPostprocess\Vignette=0,0,0,600,961,200
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
Collapsed=0
ImageIndex=2
MeshIndex=0

[AppSet9]
App=Image effects\Image
ParamValues=0,0,0,0,1000,500,500,0,0,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
Collapsed=0
ImageIndex=1
MeshIndex=0

[AppSet10]
App=Postprocess\Vignette
ParamValues=0,0,0,600,927,339
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
Collapsed=0
ImageIndex=2
MeshIndex=0

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0

[UserContent]
Text=
Html="<position x=""4"" y=""72""><p align=""left""><font face=""RobotoCondensed-Light"" size=""6"">[author]</font></position>","<position x=""4"" y=""76""><p align=""left""><font face=""RobotoCondensed-Light"" size=""10"">[title]</font></p></position>"
VideoCues=
Meshes=
MeshAutoScale=0
MeshWithColors=0
Images="[plugpath]Content\Bitmaps\Vector art\iPad2.ilv"
VideoUseSync=0
EnableMipmap=1

[Detached]
Top=238
Left=1012
Width=689
Height=577

[Controller]
RedBase=0
GreenBase=0
BlueBase=0
LumBase=0
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

