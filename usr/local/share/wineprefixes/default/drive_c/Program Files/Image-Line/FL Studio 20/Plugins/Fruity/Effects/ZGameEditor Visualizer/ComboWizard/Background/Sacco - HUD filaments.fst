FLhd   0 * ` FLdt%  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ՆI�$  ﻿[General]
GlWindowMode=1
LayerCount=20
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=4,3,24,5,16,6,15,7,14,8,12,9,10,11,13,17,2,0,1,27
WizardParams=178,322,370,418,610,706,754,802

[AppSet4]
App=Background\SolidColor
FParamValues=0.156,0.568,1,1
ParamValues=156,568,1000,1000
ParamValuesF=0.156,0.568,1,1
Enabled=1

[AppSet3]
App=HUD\HUD Prefab
FParamValues=3,0,0.5,0.752,0,0.264,0.5,0.8909,1,1,4,0,0.5,1,0.2179,0,1,0
ParamValues=3,0,500,752,0,264,500,890,1000,1000,4,0,500,1,217,0,1000,0
ParamValuesF=3,0,0.5,0.752,0,0.264,0.5,0,1,1,4,0,0.5,1,0,0,1,1
Enabled=1
LayerPrivateData=78014B4A4CCE4E2FCA2FCD4B89490232750D0C8CF53273CA18460A0000F2280847

[AppSet24]
App=Postprocess\ParameterShake
FParamValues=0.156,0.12,3,14,0.5,0,3,7,2,8,0,0,3,5,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=156,120,3,14,500,0,3,7,2,8,0,0,3,5,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=0.156,0.12,3,14,0.5,0,3,7,2,8,0,0,3,5,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet5]
App=HUD\HUD Prefab
FParamValues=48,0,0.5,0.756,0,0.344,0.88,1,1,1,4,0,0.5,1,0.24,0.917,0.687,0
ParamValues=48,0,500,756,0,344,880,1000,1000,1000,4,0,500,1,240,917,687,0
ParamValuesF=41,0,0.5,0.756,0,0.344,0.88,1,1,1,4,0,0.5,1,0.24,0.0352,0.3518,1
Enabled=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D0CCCF43273CA18460000002B830AB2

[AppSet16]
App=HUD\HUD Prefab
FParamValues=62,0,0.5,0.756,0,0.344,0.252,1,1,1,4,0,0.5,1,0.24,0.9932,0.0899,0
ParamValues=62,0,500,756,0,344,252,1000,1000,1000,4,0,500,1,240,993,89,0
ParamValuesF=55,0,0.5,0.756,0,0.344,0.252,1,1,1,4,0,0.5,1,0.24,0.9834,0.0618,1
Enabled=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D8C0CF43273CA184600000027E10AAE

[AppSet6]
App=HUD\HUD Prefab
FParamValues=49,0,0.852,0.756,0,0.376,0.88,1,1,1,4,0,0.5,1,0.212,0.9743,0.8768,0
ParamValues=49,0,852,756,0,376,880,1000,1000,1000,4,0,500,1,212,974,876,0
ParamValuesF=42,0,0.852,0.756,0,0.376,0.88,1,1,1,4,0,0.5,1,0.212,0.9858,0,1
Enabled=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D0CCCF53273CA18460000002C6C0AB3

[AppSet15]
App=HUD\HUD Prefab
FParamValues=52,0,0.852,0.756,0,0.684,0.684,1,1,1,4,0,0.5,1,0.212,0.687,0.8826,0
ParamValues=52,0,852,756,0,684,684,1000,1000,1000,4,0,500,1,212,687,882,0
ParamValuesF=45,0,0.852,0.756,0,0.684,0.684,1,1,1,4,0,0.5,1,0.212,0.3518,0.1421,1
Enabled=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D0C0DF43273CA184600000026F70AAD

[AppSet7]
App=HUD\HUD Prefab
FParamValues=56,0,0.5,0.756,0,1,0.544,1,1,1,4,0,0.5,1,0.356,0.4913,0.0811,0
ParamValues=56,0,500,756,0,1000,544,1000,1000,1000,4,0,500,1,356,491,81,0
ParamValuesF=49,0,0.5,0.756,0,1,0.544,1,1,1,4,0,0.5,1,0.356,0.4641,0.8985,1
Enabled=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D0C4DF43273CA18460000002A9B0AB1

[AppSet14]
App=HUD\HUD Prefab
FParamValues=51,0,0.5,0.756,0,1,0.432,1,1,1,4,0,0.5,1,0.356,0.01,0.4024,0
ParamValues=51,0,500,756,0,1000,432,1000,1000,1000,4,0,500,1,356,10,402,0
ParamValuesF=44,0,0.5,0.756,0,1,0.432,1,1,1,4,0,0.5,1,0.356,0.0033,0.4326,1
Enabled=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D0C2CF53273CA18460000002E3E0AB5

[AppSet8]
App=HUD\HUD Prefab
FParamValues=59,0,0.852,0.756,0,0.404,0.184,1,1,1,4,0,0.5,1,0.388,0.8448,0.4344,0
ParamValues=59,0,852,756,0,404,184,1000,1000,1000,4,0,500,1,388,844,434,0
ParamValuesF=52,0,0.852,0.756,0,0.404,0.184,1,1,1,4,0,0.5,1,0.388,0.8707,0.4054,1
Enabled=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D0CCDF53273CA18460000002D560AB4

[AppSet12]
App=HUD\HUD Prefab
FParamValues=60,0,0.852,0.756,0,0.404,0.184,1,1,1,4,0,0.5,1,0.388,0.5464,0.6815,0
ParamValues=60,0,852,756,0,404,184,1000,1000,1000,4,0,500,1,388,546,681,0
ParamValuesF=53,0,0.852,0.756,0,0.404,0.184,1,1,1,4,0,0.5,1,0.388,0.5174,0.127,1
Enabled=1
LayerPrivateData=78014BCECF2DC8CF4BCD2B298E498631750D0C2DF43273CA18460000002E3F0AB5

[AppSet9]
App=Misc\Automator
FParamValues=1,13,16,2,0.5,0,0.25,1,13,17,2,0.388,0.034,0.25,1,6,16,2,0.468,0.018,0.25,1,6,17,2,0.5,0.042,0.25
ParamValues=1,13,16,2,500,0,250,1,13,17,2,388,34,250,1,6,16,2,468,18,250,1,6,17,2,500,42,250
ParamValuesF=1,13,16,2,0.5,0,0.25,1,13,17,2,0.388,0.034,0.25,1,6,16,2,0.468,0.018,0.25,1,6,17,2,0.5,0.042,0.25
Enabled=1

[AppSet10]
App=Misc\Automator
FParamValues=1,7,16,2,0.5,0.044,0.25,1,7,17,2,0.388,0.01,0.25,1,8,16,2,0.5,0.054,0.25,1,8,17,2,0.5,0.022,0.25
ParamValues=1,7,16,2,500,44,250,1,7,17,2,388,10,250,1,8,16,2,500,54,250,1,8,17,2,500,22,250
ParamValuesF=1,7,16,2,0.5,0.044,0.25,1,7,17,2,0.388,0.01,0.25,1,8,16,2,0.5,0.054,0.25,1,8,17,2,0.5,0.022,0.25
Enabled=1

[AppSet11]
App=Misc\Automator
FParamValues=1,9,16,2,0.5,0.028,0.25,1,9,17,2,0.388,0,0.25,0,8,16,2,0.5,0.018,0.25,0,8,17,2,0.5,0.018,0.25
ParamValues=1,9,16,2,500,28,250,1,9,17,2,388,0,250,0,8,16,2,500,18,250,0,8,17,2,500,18,250
ParamValuesF=1,9,16,2,0.5,0.028,0.25,1,9,17,2,0.388,0,0.25,0,8,16,2,0.5,0.018,0.25,0,8,17,2,0.5,0.018,0.25
Enabled=1

[AppSet13]
App=Misc\Automator
FParamValues=1,15,16,2,0.5,0.048,0.25,1,15,17,2,0.388,0.004,0.25,1,16,16,2,0.5,0.042,0.25,1,16,17,2,0.5,0.026,0.25
ParamValues=1,15,16,2,500,48,250,1,15,17,2,388,4,250,1,16,16,2,500,42,250,1,16,17,2,500,26,250
ParamValuesF=1,15,16,2,0.5,0.048,0.25,1,15,17,2,0.388,0.004,0.25,1,16,16,2,0.5,0.042,0.25,1,16,17,2,0.5,0.026,0.25
Enabled=1

[AppSet17]
App=Misc\Automator
FParamValues=1,17,16,2,0.5,0.06,0.25,1,17,17,2,0.388,0.024,0.25,0,16,16,2,0.5,0.018,0.25,0,16,17,2,0.5,0.018,0.25
ParamValues=1,17,16,2,500,60,250,1,17,17,2,388,24,250,0,16,16,2,500,18,250,0,16,17,2,500,18,250
ParamValuesF=1,17,16,2,0.5,0.06,0.25,1,17,17,2,0.388,0.024,0.25,0,16,16,2,0.5,0.018,0.25,0,16,17,2,0.5,0.018,0.25
Enabled=1

[AppSet2]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.808,0,1,0.455,1,0,0,0
ParamValues=808,0,1000,455,1000,0,0,0
ParamValuesF=0.808,0,0.426,0.455,1,0,0,0
Enabled=1
UseBufferOutput=1

[AppSet0]
App=Background\SolidColor
FParamValues=0,0,0,0.696
ParamValues=0,0,0,696
ParamValuesF=0,0,0,0.696
Enabled=1

[AppSet1]
App=Object Arrays\Filaments
FParamValues=0,0,0,0,0.868,0.5,0.5,0.5,0.431,0,0.592,0,1,0
ParamValues=0,0,0,0,868,500,500,500,431,0,592,0,1000,0
ParamValuesF=0,0,0,0,0.868,0.5,0.5,0.5,0.431,0,0.592,0,1,0
Enabled=1
ImageIndex=11

[AppSet27]
App=Postprocess\ParameterShake
FParamValues=1,0,26,12,0,0,0,0,3,0,0,0,6,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=1000,0,26,12,0,0,0,0,3,0,0,0,6,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=1,0,26,12,0,0,0,0,3,0,0,0,6,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""0""><position y=""4""><p align=""center""><font face=""04b03_COMP_MAT-Regular"" size=""9"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""0""><position y=""12""><p align=""center""><font face=""04b03_COMP_MAT-Regular"" size=""6"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""0""><position y=""33""><p align=""center""><font face=""04b03_COMP_MAT-Regular"" size=""3"" color=""#FFFFFF"">[extra1]</font></p></position>","<position x=""0""><position y=""78""><p align=""center""><font face=""04b03_COMP_MAT-Regular"" size=""4"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""0""><position y=""62""><p align=""center""><font face=""04b03_COMP_MAT-Regular"" size=""3"" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""0""><position y=""65""><p align=""center""><font face=""04b03_COMP_MAT-Regular"" size=""3"" color=""#FFFFF"">[extra3]</font></p></position>"
Meshes=[plugpath]Content\Meshes\Brain.zgeMesh,[plugpath]Content\Meshes\Car.zgeMesh,[plugpath]Content\Meshes\CircularPlane.zgeMesh,[plugpath]Content\Meshes\Cone(Textue).zgeMesh,[plugpath]Content\Meshes\Cone.zgeMesh,[plugpath]Content\Meshes\Cube.zgeMesh,[plugpath]Content\Meshes\Cylinder(Texture).zgeMesh,[plugpath]Content\Meshes\Cylinder.zgeMesh,[plugpath]Content\Meshes\Drone.zgeMesh,[plugpath]Content\Meshes\Hero.zgeMesh,[plugpath]Content\Meshes\Heroine.zgeMesh,[plugpath]Content\Meshes\Monkey.zgeMesh,[plugpath]Content\Meshes\Plane.zgeMesh,[plugpath]Content\Meshes\Skull.zgeMesh,[plugpath]Content\Meshes\Sphere(Ico).zgeMesh,[plugpath]Content\Meshes\Sphere(Texture).zgeMesh,[plugpath]Content\Meshes\Sphere.zgeMesh,[plugpath]Content\Meshes\Sphere.zgeMesh,"[plugpath]Content\Meshes\Spheres 1.zgeMesh",[plugpath]Content\Meshes\Torus.zgeMesh,"[plugpath]Content\Meshes\World map.zgeMesh"
Images="[presetpath]Wizard\Assets\Sacco\Panel 01 stroke.svg","[presetpath]Wizard\Assets\Sacco\Panel 01.svg","[presetpath]Wizard\Assets\Sacco\Panel 01b black.svg","[presetpath]Wizard\Assets\Sacco\Panel 01b stroke.svg","[presetpath]Wizard\Assets\Sacco\Panel 01b.svg","[presetpath]Wizard\Assets\Sacco\Panel 02.svg","[presetpath]Wizard\Assets\Sacco\Panel 03.svg","[presetpath]Wizard\Assets\Sacco\Panel 03b.svg","[presetpath]Wizard\Assets\Sacco\Panel 04.svg","[presetpath]Wizard\Assets\Sacco\Panel 05.svg",[plugpath]Content\Bitmaps\Particles\earth1.png
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

