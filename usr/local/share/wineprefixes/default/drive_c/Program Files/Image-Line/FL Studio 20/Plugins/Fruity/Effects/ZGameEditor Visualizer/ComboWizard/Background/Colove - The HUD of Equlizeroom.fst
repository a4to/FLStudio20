FLhd   0 + ` FLdtv  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��=�  ﻿[General]
GlWindowMode=1
LayerCount=15
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=1,2,0,3,4,15,6,16,13,7,9,5,12,10,11
WizardParams=563

[AppSet1]
App=Peak Effects\Stripe Peeks
FParamValues=0,0,0.808,1,0,0,0.074,0.5,0.52,0,0.786,0.196,0.296,0,0.276,0.518,0.108,0.308,0.224,1,1,1,0
ParamValues=0,0,808,1000,0,0,74,500,520,0,786,196,296,0,276,518,108,308,224,1,1,1000,0
ParamValuesImage effects\Image=0,0,0,864,947,500,500,0,0,0,0
ParamValuesPeak Effects\JoyDividers=0,312,616,0,204,0,628,44,724,252,604,722,0
ParamValuesPeak Effects\PeekMe=0,1000,1000,0,204,528,384,0,0,216,60,180
ParamValuesHUD\HUD Graph Linear=0,0,1000,0,796,363,1000,1000,258,444,500,1000,404,224,0,864,0,280,0,333,1000,1000
ParamValuesHUD\HUD Free Line=0,500,0,0,656,552,168,304,182,92,182,168,1000,312,0,280,500
Enabled=1
Collapsed=1
Name=EG BG 1
LayerPrivateData=780113BFE967CFC0D000C430006223F361E20C0C965B4ED81DFEBAC20E59BEFF902958FDAC993381E20C0CD2FA776D7537CDB5734A48B54B787A012CB6ED73ADED9E92C97647150CECA252ACEDCF9EF1018BA7A53DB383B167CD94B45F369BC5618DCC2DFB83A714A16E68B00700CC4B29CE

[AppSet2]
App=Peak Effects\Stripe Peeks
FParamValues=0,0,0.704,1,0,0,0.074,0.5,0.52,0,0.018,0.448,0.432,1,0.524,0.518,0.344,0.216,0.536,1,1,1,0
ParamValues=0,0,704,1000,0,0,74,500,520,0,18,448,432,1,524,518,344,216,536,1,1,1000,0
ParamValuesImage effects\Image=0,0,0,872,947,500,500,0,0,0,0
Enabled=1
Collapsed=1
Name=EG BG 2
LayerPrivateData=78018BB034B167606800621800B191F930710606CB2D27EC0E7F5D61872CDF7FC814AC7ED6CC9940710686A30A1BECA4F563ED7CCC39ED139E5E008B6DFB5C6BBBA764B2DD510503BBA8146BFBB3677CC0E26969CFEC60EC593325ED97CD6671582373CBFEE02945A81B1AEC0144EA2875

[AppSet0]
App=Postprocess\Blooming
FParamValues=0,0,0,0.104,0.34,1,0.404,0.956,0.816
ParamValues=0,0,0,104,340,1000,404,956,816
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPostprocess\FrameBlur=60,760,984,1000,0,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPeak Effects\Stripe Peeks=0,0,16,1000,1000,0,74,736,520,0,786,32,296,1000,276,518,108,308,224,1000,1000,1000,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,362,500,500,500,500,500
ParamValuesPostprocess\Youlean Blur=392,0,248
ParamValuesPostprocess\Youlean Motion Blur=596,0,352,452
ParamValuesHUD\HUD 3D=12,0,184,500,500,500,500,500,500,500,500,332,1000,500,1000,1000,0,264,1000,1000,0
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesBackground\FourCornerGradient=667,1000,0,1000,1000,250,1000,1000,500,1000,1000,750,1000,1000
Enabled=1
Collapsed=1
Name=Blooming EQ 1

[AppSet3]
App=Postprocess\Youlean Blur
FParamValues=0.5,0,0,0,0,0
ParamValues=500,0,0,0,0,0
ParamValuesPostprocess\Blooming=0,0,0,104,340,1000,404,956,816
ParamValuesImage effects\Image=0,0,0,940,972,500,514,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=EQ Blur

[AppSet4]
App=Postprocess\Youlean Motion Blur
FParamValues=0.26,1,0.952,1,0,0,0
ParamValues=260,1,952,1000,0,0,0
ParamValuesPostprocess\Youlean Blur=256,0,432
ParamValuesPostprocess\Blur=1000
ParamValuesPeak Effects\Stripe Peeks=0,800,0,0,0,0,34,688,456,0,786,644,0,1000,768,250,200,272,0,0,1000,1000,0
ParamValuesHUD\HUD Text=0,0,0,0,660,686,500,552,1000,1000,324,41,240,0,0,750,1000,536,504,1000
ParamValuesPostprocess\ScanLines=800,800,0,0,0,0
ParamValuesPostprocess\Dot Matrix=1000,648,500,120,0,500,1000,1000
Enabled=1
Collapsed=1
ImageIndex=1
Name=Blooming EQ 2

[AppSet15]
App=Particles\StrangeAcid
FParamValues=0.232,0,0,0.684,0.576,1,0,0.086,0.464,0.06,0.804,0.56,0.28,1,0.092,0.54,1,1,0,0
ParamValues=232,0,0,684,576,1000,0,86,464,60,804,560,280,1000,92,540,1000,1000,0,0
ParamValuesBackground\ItsFullOfStars=0,0,124,120,884,504,0,536,40,0,560
Enabled=1
Collapsed=1
ImageIndex=1
Name=STARS 1 LEFT

[AppSet6]
App=Peak Effects\Stripe Peeks
FParamValues=0.528,1,0.988,0,0.928,0,0.054,0.5,0.52,0,0.434,0.144,0.296,1,0.368,0.466,0.46,0.072,0.062,0,1,0.68,0
ParamValues=528,1,988,0,928,0,54,500,520,0,434,144,296,1,368,466,460,72,62,0,1,680,0
ParamValuesHUD\HUD Text=0,0,0,0,660,686,500,552,1000,1000,324,41,240,0,0,750,1000,536,504,1000
Enabled=1
Collapsed=1
Name=EG BG 3 END
LayerPrivateData=780163608081067B106BE2DB1A309D3131174CE73EFF6DC7C000926BB0B7DC7202C88601887A180FA28681415AFFAEADEEA6B9764E09A976F2ADAFEDF6944CB6DDF6B9D6565A3FD64EA8D9C12EA6FF90DDD9333E76A671BBECD2D29ED9F51F2A059B396BA6A4FDB2D92C0EBDD3DFD9C7EDE274689C7AD8DE6D9BB103000F912C7B

[AppSet16]
App=Particles\StrangeAcid
FParamValues=0.424,0,0,0.104,0.612,0.036,0.08,0.086,0.464,0.06,0.804,0.56,0.008,1,0.092,0.54,1,1,0,0
ParamValues=424,0,0,104,612,36,80,86,464,60,804,560,8,1000,92,540,1000,1000,0,0
ParamValuesTerrain\GoopFlow=576,0,0,592,484,472,664,0,0,488,316,480,888,1000,460,456
ParamValuesBackground\SolidColor=0,105,1000,842
ParamValuesImage effects\Image=1000,0,0,0,1000,500,500,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=STARS 2 RIGHT

[AppSet13]
App=Postprocess\Blooming
FParamValues=0,0,0,1,0,0.688,0,0.464,0
ParamValues=0,0,0,1000,0,688,0,464,0
ParamValuesPostprocess\Vignette=0,0,0,664,676,656
ParamValuesCanvas effects\Rain=1000,764,1000,936,1000,492,500,500,500,0,1000,1000
ParamValuesPostprocess\Youlean Bloom=532,0,0,1000
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=Cool Bloom

[AppSet7]
App=Background\SolidColor
FParamValues=0,0.66,0.704,0.94
ParamValues=0,660,704,940
ParamValuesPeak Effects\Stripe Peeks=528,200,988,0,928,0,54,736,520,0,434,144,296,1000,368,466,460,72,62,0,1000,680,0
ParamValuesPostprocess\Youlean Bloom=960,724,512,168
Enabled=1
Collapsed=1
Name=Background

[AppSet9]
App=Canvas effects\Rain
FParamValues=0.916,0.772,1,0.864,1,0.5,0.5,0.5,0.5,0,0.428,0.704
ParamValues=916,772,1000,864,1000,500,500,500,500,0,428,704
ParamValuesHUD\HUD Prefab=241,816,788,1000,0,500,500,686,1000,1000,444,0,500,1000,20,0,1000,1000
ParamValuesBackground\SolidColor=0,0,0,940
ParamValuesHUD\HUD Grid=0,500,0,1000,500,500,1000,1000,952,444,500,1000,500,472,208,1000,1000,0,500,0,0,1000
Enabled=1
Collapsed=1
ImageIndex=1
Name=SkyLine 1

[AppSet5]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.506,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,506,0,0,0,0,0,0,0
ParamValuesBackground\SolidColor=0,492,140,924
ParamValuesPeak Effects\Stripe Peeks=528,200,988,0,928,0,54,736,520,0,434,144,296,1000,368,466,460,72,62,0,1000,680,0
Enabled=1
Collapsed=1

[AppSet12]
App=Canvas effects\Rain
FParamValues=0.148,0.7,1,0.949,0.436,0.492,0.5,0.5,0.5,0,0.176,0.9
ParamValues=148,700,1000,949,436,492,500,500,500,0,176,900
Enabled=1
Collapsed=1
ImageIndex=1
Name=SkyLine 2

[AppSet10]
App=Background\FourCornerGradient
FParamValues=5,0.192,0.084,0.316,0.856,0.084,0.316,0.856,0.084,0.316,0.856,0.084,0.316,0.856
ParamValues=5,192,84,316,856,84,316,856,84,316,856,84,316,856
ParamValuesPostprocess\ColorCyclePalette=0,0,0,0,0,264,560,288,396,500,336
ParamValuesHUD\HUD Meter Linear=0,208,884,1000,0,0,0,824,128,753,224,68,0,11,500,0,584,560,404,364,568,1000
Enabled=1
Name=Filter Color

[AppSet11]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
ParamValuesBackground\SolidColor=1000,0,0,1000
Enabled=1
Name=Fade-in and out

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html=," ",,,
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

