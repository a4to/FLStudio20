FLhd   0  ` FLdt�  �20.7.3.1981 ��  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4               A                        o  �  �    �HQV լ6(  ﻿[General]
GlWindowMode=1
LayerCount=30
FPS=1
AspectRatio=16:9
LayerOrder=0,1,11,25,27,26,3,10,7,6,8,16,28,15,14,20,22,23,4,5,13,2,9,12,24,21,18,17,19,29

[AppSet0]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
UseBufferOutput=1

[AppSet1]
App=Canvas effects\Cellular Tiling
FParamValues=0,0,0,0,0.56,2
ParamValues=0,0,0,0,560,2
ParamValuesYoulean new shaders\Canvas effects\Bumped Sinusoidal Warp=0,0,0,892,600
Enabled=1

[AppSet11]
App=Canvas effects\Flow Noise
FParamValues=0.36,0,0.5,0.72,0.548,0,0.54,0.108,0
ParamValues=360,0,500,720,548,0,540,108,0
Enabled=1

[AppSet25]
App=Canvas effects\Flow Noise
FParamValues=0.428,0,0.5,0.72,0.512,0.096,0.46,0.132,0
ParamValues=428,0,500,720,512,96,460,132,0
Enabled=1

[AppSet27]
App=Canvas effects\Flow Noise
FParamValues=0.484,0,0.5,0.72,0,0,0.332,0.18,0
ParamValues=484,0,500,720,0,0,332,180,0
Enabled=1

[AppSet26]
App=Canvas effects\Flow Noise
FParamValues=0.412,0,0.5,0.72,0,0.096,0.46,0.12,0
ParamValues=412,0,500,720,0,96,460,120,0
Enabled=1

[AppSet3]
App=Postprocess\Vignette
FParamValues=0,0,0,0.6,0.7,0
ParamValues=0,0,0,600,700,0
Enabled=1

[AppSet10]
App=Image effects\Image
FParamValues=1,0,0,1,1,0.5,0.5,1,0,0,0,0,1,0
ParamValues=1000,0,0,1000,1000,500,500,1000,0,0,0,0,1,0
Enabled=1
UseBufferOutput=1
ImageIndex=12

[AppSet7]
App=Canvas effects\Cellular Tiling
FParamValues=0,0,0,0,0.48,2
ParamValues=0,0,0,0,480,2
Enabled=1

[AppSet6]
App=Postprocess\Vignette
FParamValues=0,0,0,0.6,0.7,0.252
ParamValues=0,0,0,600,700,252
Enabled=1

[AppSet8]
App=Image effects\Image
FParamValues=0,0,0,1,1,0.5,0.295,1,0,0,0,0,1,0
ParamValues=0,0,0,1000,1000,500,295,1000,0,0,0,0,1,0
Enabled=1
UseBufferOutput=1
ImageIndex=12

[AppSet16]
App=Canvas effects\Flaring
FParamValues=0.464,1,1,1,0,0.444,0.5,0.009,0,0,0
ParamValues=464,1000,1000,1000,0,444,500,9,0,0,0
ParamValuesYoulean new shaders\Canvas effects\Cellular Tiling=0,0,1000,0,480
ParamValuesCanvas effects\TaffyPulls=672,0,0,0,0,500,1000,8,500,500,0,0,0,644
ParamValuesYoulean new shaders\Canvas effects\One Tweet Cellular Pattern=0,500,0,500,1000
ParamValuesYoulean new shaders\Canvas effects\Quasi Infinite Zoom Voronoi=296,0,0,0,564
Enabled=1

[AppSet28]
App=Canvas effects\Electric
FParamValues=0.316,0.74,0,0.78,1,0.5,0.5,0.006,0
ParamValues=316,740,0,780,1000,500,500,6,0
ParamValuesBackground\ItsFullOfStars=0,0,0,0,964,528,500,500,1000,0,0
ParamValuesCanvas effects\DarkSpark=0,0,500,500,500,500,500,500,500,500,0,0
ParamValuesCanvas effects\TaffyPulls=0,0,0,119,0,569,1000,14,478,508,0,0,0,562
Enabled=1

[AppSet15]
App=Postprocess\Vignette
FParamValues=0,0,0,0.6,0.18,0
ParamValues=0,0,0,600,180,0
Enabled=1

[AppSet14]
App=Image effects\Image
FParamValues=1,0,0,1,1,0.5,0.5,1,0,0,0,0,1,0
ParamValues=1000,0,0,1000,1000,500,500,1000,0,0,0,0,1,0
Enabled=1
UseBufferOutput=1
ImageIndex=12

[AppSet20]
App=Patterns\Overlap Tiling
FParamValues=0.228,0,1,0,0.492,3,0.5,0
ParamValues=228,0,1000,0,492,3,500,0
ParamValuesYoulean new shaders\Patterns\Faux Layered Extrusion=0,500,0,500,148
ParamValuesYoulean new shaders\Patterns\Arbitrary Weave=148,500,0,500,388
ParamValuesBackground\FogMachine=0,0,0,500,180,500,1000,500,0,500,0,0
ParamValuesYoulean new shaders\Patterns\Hexagonal Maze Flow=344,0,0,0,464
ParamValuesYoulean new shaders\Patterns\Impossible Geometric Lattice=0,0,1000,0,464
ParamValuesYoulean new shaders\Patterns\Algorithmic Vector Art=856,0,1000,0,600
ParamValuesYoulean new shaders\Patterns\Minimal Jigsaw=336,0,1000,0,472
Enabled=1

[AppSet22]
App=Postprocess\Vignette
FParamValues=0,0,0,0.6,0.62,0
ParamValues=0,0,0,600,620,0
Enabled=1

[AppSet23]
App=HUD\HUD Image
FParamValues=0,0,0.5,0.632,1,0.384,0.872,4,0.5,0,0,1,1,0,1
ParamValues=0,0,500,632,1000,384,872,4,500,0,0,1000,1000,0,1
Enabled=1
ImageIndex=1

[AppSet4]
App=HUD\HUD Image
FParamValues=0,0,0.5,0.08,1,1,0.88,4,0.5,0,0,1,1,1,0
ParamValues=0,0,500,80,1000,1000,880,4,500,0,0,1000,1000,1,0
ParamValuesImage effects\Image=0,0,0,0,872,500,500,0,0,0,0,0,0,0
Enabled=1
UseBufferOutput=1
ImageIndex=7

[AppSet5]
App=HUD\HUD Image
FParamValues=0,0,0.5,0.5,1,0.16,0.608,4,0.5,0,0,1,1,1,0
ParamValues=0,0,500,500,1000,160,608,4,500,0,0,1000,1000,1,0
Enabled=1
UseBufferOutput=1
ImageIndex=5

[AppSet13]
App=HUD\HUD Image
FParamValues=0,0,0.224,0.608,1,0.38,1,4,0.5,0,0,1,1,1,0
ParamValues=0,0,224,608,1000,380,1000,4,500,0,0,1000,1000,1,0
Enabled=1
UseBufferOutput=1
ImageIndex=10

[AppSet2]
App=Blend\BufferBlender
FParamValues=9,0,0,1,1,2,0.092,0,0.5,0.5,0.75,0
ParamValues=9,0,0,1000,1,2,92,0,500,500,750,0
Enabled=1
UseBufferOutput=1
ImageIndex=1

[AppSet9]
App=Blend\BufferBlender
FParamValues=9,0,0,1,1,2,0.024,0,0.5,0.5,0.75,0
ParamValues=9,0,0,1000,1,2,24,0,500,500,750,0
Enabled=1
UseBufferOutput=1
ImageIndex=2

[AppSet12]
App=Blend\BufferBlender
FParamValues=9,0,0,1,1,3,0.38,0,0.5,0.5,0.75,0
ParamValues=9,0,0,1000,1,3,380,0,500,500,750,0
Enabled=1
UseBufferOutput=1
ImageIndex=6

[AppSet24]
App=Background\FourCornerGradient
FParamValues=0,0.332,0,0,0,0.25,0,0,0.5,0,1,0.75,0,1
ParamValues=0,332,0,0,0,250,0,0,500,0,1000,750,0,1000
Enabled=1

[AppSet21]
App=HUD\HUD Image
FParamValues=0,0,0.5,0.295,0.74,1,0.5,4,0.497,0,0.008,1,1,0,1
ParamValues=0,0,500,295,740,1000,500,4,497,0,8,1000,1000,0,1
Enabled=1
ImageIndex=12

[AppSet18]
App=Postprocess\Vignette
FParamValues=0,0,0,0.6,0,0.368
ParamValues=0,0,0,600,0,368
ParamValuesBackground\SolidColor=0,0,0,0
Enabled=1
UseBufferOutput=1

[AppSet17]
App=Blend\BufferBlender
FParamValues=9,0,0,1,1,3,0.744,0,0.5,0.5,0.83,0
ParamValues=9,0,0,1000,1,3,744,0,500,500,830,0
Enabled=1
ImageIndex=8

[AppSet19]
App=Misc\Automator
FParamValues=1,18,7,2,0.5,0,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,18,7,2,500,0,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1

[AppSet29]
App=Postprocess\Youlean Color Correction
FParamValues=0.52,0.5,0.5,0.5,0.592,0.508
ParamValues=520,500,500,500,592,508
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=256000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<p align=""center"">[textline]</p>"
Images=@Stream:https://images.pexels.com/photos/3476312/pexels-photo-3476312.jpeg
VideoUseSync=0
Filtering=0

[Detached]
Top=-864
Left=0
Width=1536
Height=864

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

[Wizard]
Section0Cb=Displacement - Mountain View
Section1Cb=None
Section2Cb=Waveform Horiz 02
Section3Cb=Handwritten sweet
Macro0=Song Title
Macro1=Author
Macro2=Comment

