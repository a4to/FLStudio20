FLhd   0 * ` FLdt�  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��"S  ﻿[General]
GlWindowMode=1
LayerCount=9
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=12,15,16,11,13,6,7,10,18

[AppSet12]
App=Background\SolidColor
FParamValues=0,0,0.652,0.144
ParamValues=0,0,652,144
Enabled=1
Collapsed=1
Name=Background Main

[AppSet15]
App=HUD\HUD Prefab
FParamValues=3,0.904,0.5,0,0.996,0.5,0.5,0.636,1,1,4,0,0.5,1,0.292,0,1,1
ParamValues=3,904,500,0,996,500,500,636,1000,1000,4,0,500,1,292,0,1000,1
Enabled=1
Collapsed=1
Name=Border BG 1
LayerPrivateData=78014B4A4CCE4E2FCA2FCD4B89490232750D0C8CF53273CA18460A0000F2280847

[AppSet16]
App=HUD\HUD Prefab
FParamValues=22,0.864,0.5,0,1,0.5,0.484,1,1,1,4,0,0.5,0,0.412,0.504,1,1
ParamValues=22,864,500,0,1000,500,484,1000,1000,1000,4,0,500,0,412,504,1000,1
Enabled=1
Collapsed=1
Name=Border BG 2
LayerPrivateData=78014BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C4CF53273CA18863D00000C6D0AA0

[AppSet11]
App=HUD\HUD Graph Polar
FParamValues=0,1,0,0.912,0.5,0.484,0.426,4,0.5,0,1,0.508,1,0,0,0.904,0.112,0.308,0.184,2,0.992,1
ParamValues=0,1000,0,912,500,484,426,4,500,0,1,508,1000,0,0,904,112,308,184,2,992,1
ParamValuesHUD\HUD Graph Linear=140,56,1000,1000,500,357,1000,284,1000,444,500,1000,284,644,0,784,76,244,312,333,0,1000
ParamValuesHUD\HUD Free Line=0,500,0,0,656,552,168,304,182,92,182,168,1000,312,0,280,500
Enabled=1
Collapsed=1
Name=EQ CENTER

[AppSet13]
App=HUD\HUD Graph Polar
FParamValues=0,0,0,0.912,0.5,0.484,0.37,4,0.5,0,1,0.508,1,0,0,0.98,0.008,0.316,0.532,2,0.992,1
ParamValues=0,0,0,912,500,484,370,4,500,0,1,508,1000,0,0,980,8,316,532,2,992,1
ParamValuesImage effects\ImageWall=0,0,0,0,0,0,0
ParamValuesImage effects\Image=0,0,0,0,711,501,502,42,0,250,0
ParamValuesHUD\HUD Image=0,0,500,500,1000,1000,500,444,500,0,0,1000,1000,500,1000
ParamValuesBackground\SolidColor=0,892,1000,124
ParamValuesImage effects\ImageSlices=0,0,0,0,500,496,488,0,0,0,500,269,1000,500,0,0,0
Enabled=1
Collapsed=1
Name=EQ BLOW

[AppSet6]
App=HUD\HUD Graph Polar
FParamValues=0,0.96,1,0,0.5,0.484,0.298,4,0.5,0,1,0.48,1,0,0,0.924,0.344,0,0,1,0.4,1
ParamValues=0,960,1000,0,500,484,298,4,500,0,1,480,1000,0,0,924,344,0,0,1,400,1
ParamValuesHUD\HUD Graph Linear=140,536,1000,1000,500,641,1000,284,1000,444,1000,1000,284,644,0,784,76,244,312,333,0,1000
ParamValuesHUD\HUD Text=60,500,0,1000,660,686,500,552,1000,1000,324,1,240,0,0,750,1000,536,504,1000
Enabled=1
Collapsed=1
Name=GLOW

[AppSet7]
App=Text\TextTrueType
FParamValues=0.08,0,0,0,0,0.5,0.5,0,0,0,0.5
ParamValues=80,0,0,0,0,500,500,0,0,0,500
Enabled=1
Collapsed=1
Name=Main Text

[AppSet10]
App=Background\FourCornerGradient
FParamValues=7,0.416,1,1,1,0.036,1,1,0.856,1,1,0.834,0.604,0.76
ParamValues=7,416,1000,1000,1000,36,1000,1000,856,1000,1000,834,604,760
ParamValuesHUD\HUD Prefab=0,92,500,0,36,502,790,250,1000,1000,444,0,500,1000,368,140,1000,0
ParamValuesHUD\HUD Graph Radial=0,500,0,0,200,500,250,444,500,0,1000,0,1000,0,250,500,200,0,0,0,500,1000
ParamValuesHUD\HUD Meter Radial=0,248,1000,0,0,0,0,992,492,708,156,34,1000,0,374,0,1000,1000,500,1000
ParamValuesHUD\HUD Free Line=0,500,0,0,656,552,168,304,182,92,182,168,1000,312,0,280,500
ParamValuesHUD\HUD Graph Linear=0,500,0,0,816,468,1000,1000,250,444,500,1000,212,1000,0,500,200,0,0,0,500,1000
ParamValuesHUD\HUD Meter Linear=0,500,0,0,0,0,750,0,800,664,300,100,0,125,500,1,238,0,0,1000,612,1000
Enabled=1
Collapsed=1
Name=Filter Color

[AppSet18]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
ParamValuesBackground\SolidColor=1000,0,0,1000
Enabled=1
Name=Fade-in and out

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""4"" y=""5""><p><font face=""American-Captain"" size=""4"" color=""#fff"">[author]</font></p></position>","<position x=""4"" y=""8""><p><font face=""Chosence-Bold"" size=""3"" color=""#f4f4f4"">[title]</font></p></position>","<position x=""4"" y=""14""><p> <font face=""Chosence-Bold"" size=""3"" color=""#fff"">[comment]</font></p></position>"," ",
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

