<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" FileVersion="2">
  <OnUpdate>
    <ZExpression>
      <Expression>
<![CDATA[uRes=vector2(app.ViewportWidth,app.ViewportHeight);
uViewport=vector2(app.ViewportX,app.ViewportY);
float Speed=Parameters[3]*4.0;
delta=app.DeltaTime*Speed;
deltaTimer+=delta;

uTex1Resolution=vector2(app.ViewportWidth,app.ViewportHeight);
uTex2Resolution=vector2(Bitmap1.Width,Bitmap1.Height);
uDetail=floor(Parameters[4]*8f+1f);]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <OnRender>
    <UseMaterial Material="canvas"/>
    <RenderSprite/>
  </OnRender>
  <Content>
    <Bitmap Name="Bitmap1" Comment="NoCustom" Width="256" Height="256" Filter="2">
      <Producers>
        <BitmapExpression>
          <Expression>
<![CDATA[//X,Y : current coordinate (0..1)
//Pixel : current color (rgb)
//Sample expression: Pixel.R=abs(sin(X*16));
Pixel=vector4(rnd(),rnd(),rnd(),1);]]>
          </Expression>
        </BitmapExpression>
      </Producers>
    </Bitmap>
    <Material Name="canvas" Blend="1" Shader="Notebook_Drawings">
      <Textures>
        <MaterialTexture Name="FeedbackMaterialTexture" TexCoords="1"/>
        <MaterialTexture Name="noise256" Texture="Bitmap1"/>
      </Textures>
    </Material>
    <Shader Name="Notebook_Drawings">
      <VertexShaderSource>
<![CDATA[// created by florian berger (flockaroo) - 2016
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.

// trying to resemle some hand drawing style

// #version 130

void main(){
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[// created by florian berger (flockaroo) - 2016
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.

// trying to resemle some hand drawing style

// ---- begin of ZGE wrapper (before shader) ----

//#version 130 //we may get rid of for 100% copatibility for sersion 120

uniform vec2 iResolution,iViewport,tex1Resolution,tex2Resolution;
uniform float iTime;
uniform sampler2D tex1;
uniform sampler2D tex2;

//ZGameEditor does not have Shadertoy iChannel0 redefined below
#define iChannel0 tex1
#define iChannel1 tex2

//ZGameEditor does not have Shadertoy iChannelResolution veriables
//iChannelResolution[0] has been changed to uniform tex1Resolution
//iChannelResolution[1] has been changed to uniform tex2Resolution

// ---- end of ZGE wrapper (before shader) ----

// ----------------------
// ---- shader begin ----
// ----------------------

#ifdef SHADEROO
#define Res0 vec2(textureSize(iChannel0,0))
#define Res1 vec2(textureSize(iChannel1,0))
#else
#define Res0 tex1Resolution.xy
#define Res1 tex2Resolution.xy
#endif
#define Res  iResolution.xy
#define randSamp iChannel1
#define colorSamp iChannel0

uniform float Alpha;
uniform float PencilSize;
uniform float ColorIntensity;
uniform float ColorSpread;
uniform float detail;
uniform float PaperMode;
uniform float PaperElementSize;
uniform float StrokeAngle;
uniform float StrokeBend;
uniform float StrokeOffs;
uniform float StrokeSpread;
uniform float StrokeSmear;
//uniform float DrawMode;  changed noise function
//uniform float NoiseSize;


vec4 getRandX(vec2 uv)
{
    uv *= 15.718281828459045;
    vec4 seeds = vec4(0.123, 0.456, 0.789, 0.);
    seeds = fract((uv.x + 0.5718281828459045 + seeds) * ((seeds + mod(uv.x, 0.141592653589793)) * 27.61803398875 + 4.718281828459045));
    seeds = fract((uv.y + 0.5718281828459045 + seeds) * ((seeds + mod(uv.y, 0.141592653589793)) * 27.61803398875 + 4.718281828459045));
    seeds = fract((.62 + 0.5718281828459045 + seeds) * ((seeds + mod(.48, 0.141592653589793)) * 27.61803398875 + 4.718281828459045));
    return vec4(seeds);
}

vec4 getRand(vec2 pos)
{
    vec2 uv;
    // i highly recommend using the texture - its faster in most cases
    //uv = pos/Res1*1.3;                  // use this if noise should look the same on all resolutions
    uv = pos/Res1/iResolution.y*1080.; // use this if final image should look similar on all resolutions
    return texture2D(iChannel1,uv,0.);
}

vec4 getCol(vec2 pos)
{
    // take aspect ratio into account - min: src image always fills screen, max: src image is always fully visible
    vec2 uv = (pos-Res*.5)*min(Res0.x/Res.x,Res0.y/Res.y)/Res0.xy+.5;
    // "texture" changed to texture2D for opengl 120
    vec4 c1=texture2D(iChannel0,uv);
    return c1;
}

vec4 getColHT(vec2 pos)
{
  return smoothstep(ColorIntensity-.05,ColorIntensity+.05,getCol(pos)+getRand(pos*.7));
}

float getVal(vec2 pos)
{
    vec4 c=getCol(pos);
  return pow(dot(c.xyz,vec3(PencilSize)),1.0)*1.;
}

vec2 getGrad(vec2 pos, float eps)
{
    pos+=(getRand(pos*.1).xy-.5)*StrokeSpread*Res.x/20.;
    vec2 d=vec2(eps,0);
    return vec2(
        getVal(pos+d.xy)-getVal(pos-d.xy),
        getVal(pos+d.yx)-getVal(pos-d.yx)
    )/eps/2.;
}

#define AngleNum detail //changed to unifrom variable to allow for style variation

#define SampNum 24
#define PI2 6.28318530717959

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    // took Alpha into bounce so that original image and effect image fit positionally
    // maybe should be called "Fade" then...!?
    vec2 pos = fragCoord+4.0*sin(iTime*1.*vec2(1,1.7))*iResolution.y/400.*Alpha;
    vec2 pos0 = pos;
  vec3 col = vec3(0);
  vec3 col2 = vec3(0);
  float sum=0.;
  //implicit cast fixed
  for(int i=0;i<int(AngleNum);i++){
    float ang=StrokeAngle+PI2/float(AngleNum)*(float(i)+.8);
    vec2 v=vec2(cos(ang),sin(ang));
    for(int j=0;j<SampNum;j++){
      vec2 dpos  = v.yx*vec2(1,-1)*float(j)/float(SampNum)*16.*iResolution.y/400.;
      vec2 dpos2 = v.xy*(
          float(j*j)/float(SampNum)/float(SampNum)*16.*.5*iResolution.y/400.*StrokeBend
          +float(j)/float(SampNum)*30.5*iResolution.y/400.*StrokeSmear
          +StrokeOffs*2./**(rnd.x-.5)*/*iResolution.y/400.*10.);
      vec2 g;
      float fact;
      float fact2;

      for(float s=-1.;s<=1.;s+=2.){
        vec2 pos2=pos+s*dpos+dpos2;
        vec2 pos3=pos+(s*dpos+dpos2).yx*vec2(1,-1)*(.1+ColorSpread*2.);
        g=getGrad(pos2,.4);
        fact=dot(g,v)-.5*abs(dot(g,v.yx*vec2(1,-1)))/**(1.-getVal(pos2))*/;
        fact2=dot(normalize(g+vec2(.0001)),v.yx*vec2(1,-1));

        fact=clamp(fact,0.,.05);
        fact2=abs(fact2);

        fact*=1.-float(j)/float(SampNum);
        col += fact;
        col2 += fact2*getColHT(pos3).xyz;
        sum+=fact2;
      }
    }
  }
  // implicit cast fixed
  col/=float(SampNum*int(AngleNum))*.75/sqrt(iResolution.y);
  col2/=sum;
  col.x*=(.6+.8*getRand(pos*.7).x);
  col.x=1.-col.x;
  col.x*=col.x*col.x;

  vec2 s=sin(pos.xy*.1/sqrt(iResolution.y * PaperElementSize));
  vec3 karo=vec3(1);

  if (PaperMode == 1.0) karo-=.5*vec3(.25,.1,.1)*dot(exp(-s*s*80.),vec2(1));
  if (PaperMode == 2.0) karo-=.5*vec3(.25,.1,.1)*dot(exp(-s*s*80.),vec2(0, 1));
  if (PaperMode == 3.0) karo-=.5*vec3(.25,.1,.1)*dot(exp(-s*s*80.),vec2(1, 0));

  float r=length(pos-iResolution.xy*.5)/iResolution.x;
  float vign=1.-r*r*r;
  //out vec4 fragColor has been changed to gl_FragColor

  vec4 outColor = vec4(vec3(col.x*col2*karo*vign),1);
  //vec4 orgColor = texture2D(colorSamp,fragCoord / iResolution.xy);
  vec4 orgColor = getCol(pos0);

  fragColor = mix(orgColor,outColor,Alpha);
  //gl_FragColor = vec4(vec3(col.x*col2*karo*vign),1);
  //fragColor=getCol(fragCoord);
}

// --------------------
// ---- shader end ----
// --------------------

// ---- begin of ZGE wrapper (after shader) ----

void main(){
    //ZGE does not use mainImage( out vec4 fragColor, in vec2 fragCoord )
    //Rededefined fragCoord as gl_FragCoord.xy-iViewport(dynamic window)
    mainImage(gl_FragColor,gl_FragCoord.xy-iViewport);
}

// ---- end of ZGE wrapper (after shader) ----
        ]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="iResolution" VariableRef="uRes"/>
        <ShaderVariable VariableName="iViewport" VariableRef="uViewport"/>
        <ShaderVariable VariableName="iTime" VariableRef="deltaTimer"/>
        <ShaderVariable VariableName="tex1Resolution" VariableRef="uTex1Resolution"/>
        <ShaderVariable VariableName="tex2Resolution" VariableRef="uTex2Resolution"/>
        <ShaderVariable VariableName="Alpha" ValuePropRef="1.0 - Parameters[0]"/>
        <ShaderVariable VariableName="PencilSize" ValuePropRef="Parameters[1] + 0.001"/>
        <ShaderVariable VariableName="ColorIntensity" Value="0.5" ValuePropRef="Parameters[2]"/>
        <ShaderVariable VariableName="ColorSpread" Value="0.5" ValuePropRef="Parameters[11]"/>
        <ShaderVariable VariableName="detail" VariableRef="uDetail"/>
        <ShaderVariable VariableName="PaperMode" ValuePropRef="floor(Parameters[5] * 5)"/>
        <ShaderVariable VariableName="PaperElementSize" ValuePropRef="Pow(Parameters[6] + 0.2, 4) * 0.2 + 0.0000001"/>
        <ShaderVariable DesignDisable="255" VariableName="DrawMode" ValuePropRef="floor(Parameters[7] * 5)"/>
        <ShaderVariable DesignDisable="255" VariableName="NoiseSize" ValuePropRef="Pow(1.0-Parameters[8], 5) * 1079.0 + 1.0"/>
        <ShaderVariable VariableName="StrokeAngle" ValuePropRef="Parameters[7]*6.3"/>
        <ShaderVariable VariableName="StrokeBend" Value="-1" ValuePropRef="Parameters[8]*3.0-1.5"/>
        <ShaderVariable VariableName="StrokeOffs" ValuePropRef="Parameters[9]*10.0-5.0"/>
        <ShaderVariable VariableName="StrokeSpread" ValuePropRef="Parameters[10]"/>
        <ShaderVariable VariableName="StrokeSmear" ValuePropRef="Parameters[12]"/>
      </UniformVariables>
    </Shader>
    <Group Comment="Shader: Uniform variables ">
      <Children>
        <Variable Name="uRes" Type="6"/>
        <Variable Name="uViewport" Type="6"/>
        <Variable Name="uSpeed"/>
        <Variable Name="delta"/>
        <Variable Name="deltaTimer"/>
        <Variable Name="uTex1Resolution" Type="6"/>
        <Variable Name="uTex2Resolution" Type="6"/>
        <Variable Name="uDetail"/>
      </Children>
    </Group>
    <Array Name="Parameters" SizeDim1="13" Persistent="255">
      <Values>
<![CDATA[78DA63606060387BC6C70E48D9333034D8C130548C01C207C98101980600050807FC]]>
      </Values>
    </Array>
    <Constant Name="ParamHelpConst" Type="2">
      <StringValue>
<![CDATA[Alpha
Pencil Size
Color Intensity
Bounce Speed
Detail @list: Low,Medium,High,Ultra
Paper Type @list: Off, "Squares", "Horizontal Lines", "Vertical Lines"
Paper Size
Stroke Angle
Stroke Bend
Stroke Offs
Stroke Spread
Color Spread
Stroke Smear
]]>
      </StringValue>
    </Constant>
    <Constant/>
    <Constant Name="AuthorInfo" Type="2" StringValue="Florian Berger - shaderoo.org"/>
  </Content>
</ZApplication>
