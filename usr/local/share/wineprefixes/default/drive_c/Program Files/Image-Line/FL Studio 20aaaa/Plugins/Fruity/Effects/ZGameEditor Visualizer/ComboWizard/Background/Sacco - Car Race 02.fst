FLhd   0 * ` FLdt4  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV Ւg�3  ﻿[General]
GlWindowMode=1
LayerCount=36
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=11,12,13,10,3,41,40,33,2,8,5,42,34,7,35,4,36,37,15,0,38,39,16,17,14,9,31,32,18,29,30,1,19,43,6,26

[AppSet11]
App=Background\SolidColor
FParamValues=0,0,0,0.968
ParamValues=0,0,0,968
ParamValuesF=0,0,0,0.968
Enabled=1

[AppSet12]
App=HUD\HUD Prefab
FParamValues=73,0,0.5,0,0,0.656,0.5,0.386,1,1,4,0,0.5,1,0.368,0,1,0
ParamValues=73,0,500,0,0,656,500,386,1000,1000,4,0,500,1,368,0,1000,0
ParamValuesF=66,0,0.5,0,0,0.062,0.5,0.386,1,1,4,0,0.5,1,0.368,0,1,1
Enabled=1
LayerPrivateData=78014B2FCA4C89490712BA0606867A9939650C230B0000B42405E0

[AppSet13]
App=Misc\Automator
FParamValues=1,13,6,3,1,0.016,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,13,6,3,1000,16,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
ParamValuesF=1,13,6,3,1,0.016,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
Enabled=1

[AppSet10]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.5,0,0.15,0.45,1,0,0,0
ParamValues=500,0,150,450,1000,0,0,0
ParamValuesF=0.5,0,0.15,0.45,1,0,0,0
ParamValuesHUD\HUD Prefab=2,0,500,0,0,500,500,428,1000,1000,444,0,500,1000,368,0,1000,1000
Enabled=1
UseBufferOutput=1

[AppSet3]
App=Background\SolidColor
FParamValues=0,0,0,0.968
ParamValues=0,0,0,968
ParamValuesF=0,0,0,0.968
Enabled=1

[AppSet41]
App=HUD\HUD Prefab
FParamValues=2,0.476,0,0.604,0.872,0.447,0.628,0.626,0.268,1,4,0,0.5,1,0.395,0,1,0
ParamValues=2,476,0,604,872,447,628,626,268,1000,4,0,500,1,395,0,1000,0
ParamValuesF=2,0.476,0,0.604,0.872,0.566,0.628,0.626,0.268,1,4,0,0.5,1,0.395,0,1,1
Enabled=1
LayerPrivateData=78014B4A4CCE4E2FCA2FCD4B89490232750D0C8CF43273CA18460A0000F13A0846

[AppSet40]
App=HUD\HUD Prefab
FParamValues=3,0.488,0.796,0.604,0.872,0.82,0.3348,0.626,0.268,1,4,0,0.5,1,0.395,0,1,0
ParamValues=3,488,796,604,872,820,334,626,268,1000,4,0,500,1,395,0,1000,0
ParamValuesF=3,0.488,0.796,0.604,0.872,0.078,0.4479,0.626,0.268,1,4,0,0.5,1,0.395,0,1,1
Enabled=1
LayerPrivateData=78014B4A4CCE4E2FCA2FCD4B89490232750D0C8CF53273CA18460A0000F2280847

[AppSet33]
App=HUD\HUD Prefab
FParamValues=4,0.452,0.5,0.604,0.872,0.042,0.4771,0.626,0.276,1,4,0,0.5,1,0.4225,0,1,0
ParamValues=4,452,500,604,872,42,477,626,276,1000,4,0,500,1,422,0,1000,0
ParamValuesF=4,0.452,0.5,0.604,0.872,0.239,0.504,0.626,0.276,1,4,0,0.5,1,0.2869,0,1,1
Enabled=1
LayerPrivateData=78014B4A4CCE4E2FCA2FCD4B89490232750D0C4CF43273CA18460A0000F3160848

[AppSet2]
App=HUD\HUD Prefab
FParamValues=4,0.444,0.5,0.604,0.872,0.566,0.2527,0.626,0.264,1,4,0,0.5,1,0.4225,0,1,0
ParamValues=4,444,500,604,872,566,252,626,264,1000,4,0,500,1,422,0,1000,0
ParamValuesF=4,0.444,0.5,0.604,0.872,0.101,0.483,0.626,0.264,1,4,0,0.5,1,0.2869,0,1,1
Enabled=1
LayerPrivateData=78014B4A4CCE4E2FCA2FCD4B89490232750D0C4CF43273CA18460A0000F3160848

[AppSet8]
App=HUD\HUD Prefab
FParamValues=1,0.508,0.5,0.604,0.872,0.4016,0.4096,0.542,0.304,1,4,0,0.5,1,0.4225,0,1,0
ParamValues=1,508,500,604,872,401,409,542,304,1000,4,0,500,1,422,0,1000,0
ParamValuesF=1,0.508,0.5,0.604,0.872,0.3914,0.4547,0.542,0.304,1,4,0,0.5,1,0.2869,0,1,1
Enabled=1
LayerPrivateData=78014B4A4CCE4E2FCA2FCD4B89490232750D0C0CF53273CA18460A0000F04C0845

[AppSet5]
App=Misc\Automator
FParamValues=1,42,6,3,1,0.017,0.25,1,42,7,3,0.628,0,0.386,1,41,6,3,1,0.02,0.25,1,41,7,3,0.628,0.007,0.386
ParamValues=1,42,6,3,1000,17,250,1,42,7,3,628,0,386,1,41,6,3,1000,20,250,1,41,7,3,628,7,386
ParamValuesF=1,42,6,3,1,0.017,0.25,1,42,7,3,0.628,0,0.386,1,41,6,3,1,0.02,0.25,1,41,7,3,0.628,0.007,0.386
Enabled=1

[AppSet42]
App=Misc\Automator
FParamValues=1,34,6,3,1,0.062,0.25,1,34,7,3,0.628,0.059,0.386,1,41,6,3,1,0.02,0.25,1,41,7,3,0.628,0.027,0.386
ParamValues=1,34,6,3,1000,62,250,1,34,7,3,628,59,386,1,41,6,3,1000,20,250,1,41,7,3,628,27,386
ParamValuesF=1,34,6,3,1,0.062,0.25,1,34,7,3,0.628,0.059,0.386,1,41,6,3,1,0.02,0.25,1,41,7,3,0.628,0.027,0.386
Enabled=1

[AppSet34]
App=Misc\Automator
FParamValues=1,3,6,3,1,0.026,0.25,1,3,7,3,0.628,0.047,0.386,1,9,6,3,0.872,0.007,0.246,1,9,7,3,0.628,0.031,0.386
ParamValues=1,3,6,3,1000,26,250,1,3,7,3,628,47,386,1,9,6,3,872,7,246,1,9,7,3,628,31,386
ParamValuesF=1,3,6,3,1,0.026,0.25,1,3,7,3,0.628,0.047,0.386,1,9,6,3,0.872,0.007,0.246,1,9,7,3,0.628,0.031,0.386
Enabled=1

[AppSet7]
App=Postprocess\ParameterShake
FParamValues=0.604,0.108,33,14,0.604,0.108,8,14,0,0,0,1,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=604,108,33,14,604,108,8,14,0,0,0,1,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=0.604,0.108,33,14,0.604,0.108,8,14,0,0,0,1,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet35]
App=Postprocess\ParameterShake
FParamValues=0.604,0.108,2,14,0.604,0.108,8,14,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValues=604,108,2,14,604,108,8,14,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesF=0.604,0.108,2,14,0.604,0.108,8,14,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet4]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.5,0,0.15,0.45,1,0,0,0
ParamValues=500,0,150,450,1000,0,0,0
ParamValuesF=0.5,0,0.15,0.45,1,0,0,0
ParamValuesHUD\HUD Prefab=2,0,500,0,0,500,500,428,1000,1000,444,0,500,1000,368,0,1000,1000
Enabled=1
UseBufferOutput=1

[AppSet36]
App=HUD\HUD 3D
FParamValues=0,0,0,0.48,0,0,0.14,0.5,0.5,0.365,0.5,1,0.5,0.188,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0.363,0,0.336,1,1
ParamValues=0,0,0,480,0,0,140,500,500,365,500,1000,500,188,0,500,500,500,500,500,500,500,500,1,1,363,0,336,1000,1
ParamValuesF=0,0,0,0.48,0,0,0.14,0.5,0.5,0.365,0.5,1,0.5,0.188,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0.363,0,0.336,1,1
Enabled=1
UseBufferOutput=1

[AppSet37]
App=HUD\HUD 3D
FParamValues=0,0,0.134,0.552,1,0,0.14,0.501,0.271,0.415,0.505,0.243,0.5,0.597,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0.363,0,0.336,1,1
ParamValues=0,0,134,552,1000,0,140,501,271,415,505,243,500,597,0,500,500,500,500,500,500,500,500,1,1,363,0,336,1000,1
ParamValuesF=0,0,0.134,0.552,1,0,0.14,0.501,0.271,0.415,0.505,0.243,0.5,0.597,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0.363,0,0.336,1,1
Enabled=1
UseBufferOutput=1
ImageIndex=1

[AppSet15]
App=HUD\HUD 3D
FParamValues=0,0,0.067,0.567,0.764,0,0.14,0.5,0.271,0.35,0.509,0.243,0.5,0.886,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0.363,0,0.336,1,1
ParamValues=0,0,67,567,764,0,140,500,271,350,509,243,500,886,0,500,500,500,500,500,500,500,500,1,1,363,0,336,1000,1
ParamValuesF=0,0,0.067,0.567,0.764,0,0.14,0.5,0.271,0.35,0.509,0.243,0.5,0.886,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0.363,0,0.336,1,1
Enabled=1
UseBufferOutput=1
ImageIndex=1

[AppSet0]
App=Background\SolidColor
FParamValues=0,0,0.22,0.798
ParamValues=0,0,220,798
ParamValuesF=0,0,0.22,0.798
Enabled=1

[AppSet38]
App=Image effects\Image
FParamValues=0,0,0,0,0.884,0.346,0.501,0.065,0,0,0,0,0,0
ParamValues=0,0,0,0,884,346,501,65,0,0,0,0,0,0
ParamValuesF=0,0,0,0,0.884,0.346,0.501,0.065,0,0,0,0,0,0
Enabled=1
ImageIndex=3

[AppSet39]
App=Image effects\Image
FParamValues=0,0,0,0,0.972,0.452,0.501,0,0,0,0,0,0,0
ParamValues=0,0,0,0,972,452,501,0,0,0,0,0,0,0
ParamValuesF=0,0,0,0,0.972,0.452,0.501,0,0,0,0,0,0,0
Enabled=1
ImageIndex=3

[AppSet16]
App=Image effects\Image
FParamValues=0,0,0,0,0.972,0.463,0.503,0,0,0,0,0,0,0
ParamValues=0,0,0,0,972,463,503,0,0,0,0,0,0,0
ParamValuesF=0,0,0,0,0.972,0.463,0.503,0,0,0,0,0,0,0
Enabled=1
ImageIndex=4

[AppSet17]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.47,0.503,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,470,503,0,0,0,0,0,0,0
ParamValuesF=0,0,0,0,1,0.47,0.503,0,0,0,0,0,0,0
Enabled=1
ImageIndex=2

[AppSet14]
App=HUD\HUD 3D
FParamValues=0,0,0.5,0.583,0.5,0.5,0.5,0.5,0.272,0.336,0.512,0.5,0.5,0.874,0,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0.363,0,0.336,1,1
ParamValues=0,0,500,583,500,500,500,500,272,336,512,500,500,874,0,0,500,500,500,500,500,500,500,1,1,363,0,336,1000,1
ParamValuesF=0,0,0.5,0.583,0.5,0.5,0.5,0.5,0.272,0.336,0.512,0.5,0.5,0.874,0,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0.363,0,0.336,1,1
Enabled=1
ImageIndex=1

[AppSet9]
App=HUD\HUD 3D
FParamValues=0,0,0.031,0.48,0,0.554,0.14,0.5,0.5,0.352,0.5,1,0.5,0.234,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0.363,0,0.336,1,1
ParamValues=0,0,31,480,0,554,140,500,500,352,500,1000,500,234,0,500,500,500,500,500,500,500,500,1,1,363,0,336,1000,1
ParamValuesF=0,0,0.031,0.48,0,0.554,0.14,0.5,0.5,0.352,0.5,1,0.5,0.234,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0.363,0,0.336,1,1
Enabled=1

[AppSet31]
App=HUD\HUD 3D
FParamValues=0,0,0.579,0.492,0,0,0.14,0.5,0.5,0.332,0.5,1,0.5,1,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0.363,0,0.336,1,1
ParamValues=0,0,579,492,0,0,140,500,500,332,500,1000,500,1000,0,500,500,500,500,500,500,500,500,1,1,363,0,336,1000,1
ParamValuesF=0,0,0.579,0.492,0,0,0.14,0.5,0.5,0.332,0.5,1,0.5,1,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0.363,0,0.336,1,1
Enabled=1

[AppSet32]
App=HUD\HUD 3D
FParamValues=0,0,0.613,0.5,0.5,1,0.14,0,0.5,0.332,0.5,1,0.5,1,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,0.405,1,1
ParamValues=0,0,613,500,500,1000,140,0,500,332,500,1000,500,1000,0,500,500,500,500,500,500,500,500,1,1,0,0,405,1000,1
ParamValuesF=0,0,0.613,0.5,0.5,1,0.14,0,0.5,0.332,0.5,1,0.5,1,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,0.405,1,1
Enabled=1

[AppSet18]
App=Background\FogMachine
FParamValues=0.5,0.38,0,0.712,0.5,0,1,0.284,0.5,0.684,1,1
ParamValues=500,380,0,712,500,0,1000,284,500,684,1000,1000
ParamValuesF=0.5,0.38,0,0.712,0.5,0,1,0.284,0.5,0.684,1,1
Enabled=1
ImageIndex=5

[AppSet29]
App=Misc\MatCap
FParamValues=0,0,0,0.298,0.2655,0.314,0.935,0.474,0.419,0.005,0.08,1,1,1,0.572,1
ParamValues=0,0,0,298,265,314,935,474,419,5,80,1000,1000,1000,572,1000
ParamValuesF=0,0,0,0.298,0.234,0.314,0.935,0.474,0.419,0.005,0.08,1,1,1,0.572,1
Enabled=1
MeshIndex=1

[AppSet30]
App=Misc\MatCap
FParamValues=0,0,1,0.298,0.2655,0.453,0.936,0.521,0.584,0.509,0,0.5,1,1,0,0.3
ParamValues=0,0,1,298,265,453,936,521,584,509,0,500,1000,1000,0,300
ParamValuesF=0,0,1,0.298,0.234,0.453,0.936,0.521,0.584,0.509,0,0.5,1,1,0,0.3
Enabled=1
MeshIndex=1

[AppSet1]
App=Misc\MatCap
FParamValues=0,0,0,0.298,0.2655,0.489,0.936,0.521,0.584,0.509,0.5,0.0741,1,0.576,0,1
ParamValues=0,0,0,298,265,489,936,521,584,509,500,74,1000,576,0,1000
ParamValuesF=0,0,0,0.298,0.234,0.489,0.936,0.521,0.584,0.509,0.5,0.4318,1,0.576,0,1
ParamValuesParticles\PlasmaFlys=500,500,500,0,148,500,500,500,0,500,500,500,1000,1000
ParamValuesObject Arrays\Rings=0,0,108,0,636,500,495,492,517,836,0,1000,0,1000,420
Enabled=1
MeshIndex=1

[AppSet19]
App=Misc\Automator
FParamValues=1,2,5,2,0.228,0.072,0.522,1,2,12,2,0.5,0.072,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,2,5,2,228,72,522,1,2,12,2,500,72,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
ParamValuesF=1,2,5,2,0.228,0.072,0.522,1,2,12,2,0.5,0.072,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
Enabled=1

[AppSet43]
App=Misc\Automator
FParamValues=1,31,5,2,0.228,0.072,0.522,1,30,5,2,0.228,0.072,0.522,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,31,5,2,228,72,522,1,30,5,2,228,72,522,0,0,0,0,0,250,250,0,0,0,0,0,250,250
ParamValuesF=1,31,5,2,0.228,0.072,0.522,1,30,5,2,0.228,0.072,0.522,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
Enabled=1

[AppSet6]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.632,0.5,0.5,0.5
ParamValues=500,500,632,500,500,500
ParamValuesF=0.5,0.5,0.632,0.5,0.5,0.5
Enabled=1

[AppSet26]
App=Misc\Automator
FParamValues=1,23,13,2,0.5,0.029,0.25,1,24,13,2,0.5,0.012,0.25,1,25,13,2,0.5,0.025,0.25,1,26,13,2,0.5,0.02,0.25
ParamValues=1,23,13,2,500,29,250,1,24,13,2,500,12,250,1,25,13,2,500,25,250,1,26,13,2,500,20,250
ParamValuesF=1,23,13,2,0.5,0.029,0.25,1,24,13,2,0.5,0.012,0.25,1,25,13,2,0.5,0.025,0.25,1,26,13,2,0.5,0.02,0.25
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""6""><position y=""5""><p align=""left""><font face=""American-Captain"" size=""7"" color=""#333333"">[author]</font></p></position>","<position x=""6""><position y=""15.5""><p align=""left""><font face=""Chosence-Bold"" size=""6"" color=""#333333"">[title]</font></p></position>","<position x=""14.2""><position y=""13.2""><p align=""center""><font face=""Chosence-Bold"" size=""3"" color=""#333333"">[extra1]</font></p></position>","<position x=""0""><position y=""94.5""><p align=""right""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""0""><position y=""75""><p align=""right""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""0""><position y=""78""><p align=""right""><font face=""Chosence-regular"" size=""2.5"" color=""#FFFFF"">[extra3]</font></p></position>","  "
Meshes=[plugpath]Content\Meshes\Cube.zgeMesh,[plugpath]Content\Meshes\Car.zgeMesh
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

