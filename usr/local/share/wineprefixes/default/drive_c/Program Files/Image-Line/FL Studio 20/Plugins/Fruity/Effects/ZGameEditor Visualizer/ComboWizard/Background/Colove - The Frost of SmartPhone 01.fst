FLhd   0 * ` FLdt�  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��.c  ﻿[General]
GlWindowMode=1
LayerCount=17
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=6,16,8,14,0,9,15,10,11,1,7,4,2,3,5,12,13

[AppSet6]
App=Canvas effects\Lava
FParamValues=0,0.648,1,0.592,0,0.5,0.5,1,0.528,0.236,0.468
ParamValues=0,648,1000,592,0,500,500,1000,528,236,468
ParamValuesCanvas effects\Digital Brain=0,0,568,0,428,1000,508,4,892,28,0,264,340,292,1000,0
ParamValuesBackground\SolidColor=0,932,1000,0
Enabled=1
Collapsed=1
Name=EFX 1

[AppSet16]
App=Canvas effects\OverlySatisfying
FParamValues=0,0.74,0,0
ParamValues=0,740,0,0
Enabled=1
Collapsed=1
Name=EFX 2

[AppSet8]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.724,0.476,0.618,0.66,1,0,0,0
ParamValues=724,476,618,660,1000,0,0,0
ParamValuesBackground\FourCornerGradient=400,1000,700,1000,1000,726,1000,1000,8,972,1000,750,1000,984
ParamValuesPostprocess\Youlean Motion Blur=0,1000,420,776
ParamValuesPostprocess\Blur=644
Enabled=1
Collapsed=1
Name=Bloom

[AppSet14]
App=HUD\HUD Prefab
FParamValues=74,0,0,0,1,0.5,0.5,0.294,1,1,4,0,0.5,0,1,0,1,1
ParamValues=74,0,0,0,1000,500,500,294,1000,1000,4,0,500,0,1000,0,1000,1
ParamValuesCanvas effects\Lava=0,664,1000,684,168,500,500,1000,528,500,224
ParamValuesImage effects\Image=0,0,0,0,644,500,500,96,53,250,0,0,0,0
ParamValuesHUD\HUD Grid=0,500,0,1000,500,500,1000,1000,1000,444,500,1000,500,100,0,500,1000,800,904,300,0,1000
Enabled=1
Collapsed=1
Name=GRIDFECTOR
LayerPrivateData=78014B2FCA4C89490712BA0606467A9939650C230B0000B51805E1

[AppSet0]
App=Background\FourCornerGradient
FParamValues=8,1,0,1,1,0.434,1,1,0.5,1,1,0.75,1,1
ParamValues=8,1000,0,1000,1000,434,1000,1000,500,1000,1000,750,1000,1000
ParamValuesBackground\SolidColor=0,932,1000,0
Enabled=1
Collapsed=1
Name=Filter Color

[AppSet9]
App=Feedback\SphericalProjection
FParamValues=0,0,0,0,0,0.177,0.384,0.59,0.548,0.488,0,1,1,0.86,0.52,0.432
ParamValues=0,0,0,0,0,177,384,590,548,488,0,1,1,860,520,432
ParamValuesFeedback\WarpBack=1000,140,0,0,500,500,924
ParamValuesHUD\HUD Graph Radial=0,500,0,712,500,500,82,444,500,0,1000,0,1000,64,250,500,200,312,12,0,500,1000
ParamValuesFeedback\70sKaleido=0,0,228,1000,860,324
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,696,0,728,500,584,596,500,768
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,1000,0,0,500,920,552,500
ParamValuesHUD\HUD Graph Linear=0,1000,1000,0,500,504,1000,584,810,444,500,1000,0,688,796,900,164,180,132,333,536,1000
ParamValuesPostprocess\Youlean Pixelate=392,333,0,0
ParamValuesFeedback\BoxedIn=0,0,0,0,1000,1000,748,500,0,0
ParamValuesFeedback\FeedMeFract=0,0,0,1000,564,0
ParamValuesPostprocess\Youlean Motion Blur=688,1000,408,672
ParamValuesFeedback\FeedMe=0,0,0,1000,500,680,312
Enabled=1
Collapsed=1
ImageIndex=6
Name=OMG EFX

[AppSet15]
App=Feedback\FeedMe
FParamValues=0,0,0,0.44,0.884,0.44,0
ParamValues=0,0,0,440,884,440,0
ParamValuesFeedback\WormHoleDarkn=1000,0,0,1000,708,348,544,500,260,500,500,560
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=Master LCD

[AppSet10]
App=Background\SolidColor
FParamValues=0,0.656,0.196,0.824
ParamValues=0,656,196,824
ParamValuesPostprocess\ScanLines=200,0,0,0,0,0
Enabled=1
Collapsed=1
Name=Background

[AppSet11]
App=Postprocess\ScanLines
FParamValues=0,1,1,1,0
ParamValues=0,1,1000,1000,0
ParamValuesHUD\HUD Prefab=18,0,0,0,884,488,496,588,1000,1000,444,0,324,0,0,0,1000,1000
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,157,500,500,500,500,500
ParamValuesHUD\HUD Grid=68,0,0,700,500,500,1000,1000,1000,444,500,1000,500,416,0,536,596,56,760,240,0,1000
Enabled=1
Collapsed=1
Name=Grid BG

[AppSet1]
App=Image effects\Image
FParamValues=0,0,0,0,0.997,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,997,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
Name=SM Buttons

[AppSet7]
App=Image effects\Image
FParamValues=0,0,0,0,0.644,0.5,0.5,0.102,0.053,0.25,0,0,0,0.148
ParamValues=0,0,0,0,644,500,500,102,53,250,0,0,0,148
Enabled=1
Collapsed=1
ImageIndex=5
Name=LCD Module

[AppSet4]
App=Image effects\Image
FParamValues=0.956,0,0,0.324,1,0.5,0.5,0.002,0.008,0,0,0,0,0
ParamValues=956,0,0,324,1000,500,500,2,8,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=2
Name=SM Glass

[AppSet2]
App=Image effects\Image
FParamValues=0,0,0,0.952,0.96,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,952,960,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=4
Name=SM Border

[AppSet3]
App=Image effects\Image
FParamValues=0.588,0,0,0.12,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=588,0,0,120,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=SM Camera

[AppSet5]
App=Image effects\Image
FParamValues=0.696,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=696,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=3
Name=SM Frame

[AppSet12]
App=Background\FourCornerGradient
FParamValues=8,0,0,1,1,0.25,1,1,0.5,1,1,0.75,1,1
ParamValues=8,0,0,1000,1000,250,1000,1000,500,1000,1000,750,1000,1000
Enabled=1
Collapsed=1
Name=Master Filter Color

[AppSet13]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fader in-out

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text=Author,"Song Title"
Html=," ",,,
Images=[presetpath]Wizard\ColoveContent\Devices\svg\Samsung\Samsung-S-8-Buttons.svg,[presetpath]Wizard\ColoveContent\Devices\svg\Samsung\Samsung-S-8-Camera.svg,[presetpath]Wizard\ColoveContent\Devices\svg\Samsung\Samsung-S-8-Glass.svg,[presetpath]Wizard\ColoveContent\Devices\png\Display\Samsung-S-8-Frame.png,[presetpath]Wizard\ColoveContent\Devices\svg\Samsung\Samsung-S-8-Border-RQ.svg
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

