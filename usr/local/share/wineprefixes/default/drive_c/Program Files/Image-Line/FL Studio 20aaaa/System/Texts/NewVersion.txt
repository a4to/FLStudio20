﻿A newer version of |FL Studio| is available for download.

After installation, update your license key. Open the |Help > About...| window, enter your account email, password and click |Unlock with account|.

Click Download to open the download page. Click Cancel to continue without downloading the installer.