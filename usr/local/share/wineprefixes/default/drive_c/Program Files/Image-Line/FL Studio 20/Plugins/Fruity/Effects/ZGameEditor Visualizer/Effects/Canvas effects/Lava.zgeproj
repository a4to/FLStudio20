<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" FileVersion="2">
  <OnLoaded>
    <SpawnModel Model="ScreenModel"/>
    <ZLibrary Comment="HSV Library">
      <Source>
<![CDATA[vec3 hsv(float h, float s, float v)
{
  s = clamp(s/100, 0, 1);
  v = clamp(v/100, 0, 1);

  if(!s)return vector3(v, v, v);

  h = h < 0 ? frac(1-abs(frac(h/360)))*6 : frac(h/360)*6;

  float c, f, p, q, t;

  c = floor(h);
  f = h-c;

  p = v*(1-s);
  q = v*(1-s*f);
  t = v*(1-s*(1-f));

  switch(c)
  {
    case 0: return vector3(v, t, p);
    case 1: return vector3(q, v, p);
    case 2: return vector3(p, v, t);
    case 3: return vector3(p, q, v);
    case 4: return vector3(t, p, v);
    case 5: return vector3(v, p, q);
  }
}]]>
      </Source>
    </ZLibrary>
  </OnLoaded>
  <OnUpdate>
    <ZExpression>
      <Expression>
<![CDATA[const int
  ALPHA = 0,
  HUE = 1,
  SATURATION = 2,
  LIGHTNESS = 3;

vec3 c=hsv(Parameters[HUE]*360,Parameters[SATURATION]*100,(1-Parameters[LIGHTNESS])*100);

ShaderColor.r=c.r;
ShaderColor.g=c.g;
ShaderColor.b=c.b;

float delta=app.DeltaTime;

uDynamicTime+=delta;

uSize=5.0-Parameters[4]*4.0;
uPos=vector2((1.0-Parameters[5])-.5,Parameters[6]-.5);

float speed1Delta=delta*(Parameters[7]-.5);
uSpeed1+=speed1Delta;

float speed2Delta=delta*(1.0-Parameters[8]*2.0);
uSpeed2+=speed2Delta;
uThreshold=Parameters[10]*3.0001;]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <Content>
    <Group>
      <Children>
        <Array Name="Parameters" SizeDim1="11" Persistent="255">
          <Values>
<![CDATA[78DA63608003FB378116F60C0C0DF6203616CC0000607303EE]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Alpha
Hue
Saturation
Lightness
Size
Positon X
Position Y
PrimarySpeed
ScndrySpeed
OctScale
Threshold
]]>
          </StringValue>
        </Constant>
        <Variable Name="uSize"/>
        <Variable Name="uPos" Type="6"/>
        <Variable Name="uSpeed1"/>
        <Variable Name="uSpeed2"/>
        <Variable Name="uDynamicTime"/>
        <Variable Name="uThreshold"/>
      </Children>
    </Group>
    <Material Name="ScreenMaterial" Shading="1" Light="0" Blend="1" ZBuffer="0" Shader="ScreenShader">
      <Textures>
        <MaterialTexture Texture="NoiseBitmap" TextureWrapMode="1" TexCoords="1"/>
      </Textures>
    </Material>
    <Shader Name="ScreenShader">
      <VertexShaderSource>
<![CDATA[varying vec2 position;

void main(){
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
  position=vec2(vertex.x,vertex.y);
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[//Lava by nimitz (twitter: @stormoid)
//Adapted from https://www.shadertoy.com/view/lslXRS

uniform float iGlobalTime;
uniform float resX;
uniform float resY;
uniform float viewportX;
uniform float viewportY;
uniform vec2 pos;
uniform float size;
uniform float threshold;

uniform float Alpha;

uniform sampler2D tex1;
#define iChannel0 tex1

vec2 iResolution = vec2(resX,resY);

uniform float pSpeed1;
uniform float pSpeed2;
uniform float pOctScale;

uniform vec3 pColor;

//Noise animation - Lava
//by nimitz (stormoid.com) (twitter: @stormoid)


//Somewhat inspired by the concepts behind "flow noise"
//every octave of noise is modulated separately
//with displacement using a rotated vector field

//This is a more standard use of the flow noise
//unlike my normalized vector field version (https://www.shadertoy.com/view/MdlXRS)
//the noise octaves are actually displaced to create a directional flow

//Sinus ridged fbm is used for better effect.

#define time iGlobalTime*0.1

float hash21(in vec2 n){ return fract(sin(dot(n, vec2(12.9898, 4.1414))) * 43758.5453); }
mat2 makem2(in float theta){float c = cos(theta);float s = sin(theta);return mat2(c,-s,s,c);}
float noise( in vec2 x ){return texture2D(iChannel0, x*.01).x;}

vec2 gradn(vec2 p)
{
	float ep = .09;
	float gradx = noise(vec2(p.x+ep,p.y))-noise(vec2(p.x-ep,p.y));
	float grady = noise(vec2(p.x,p.y+ep))-noise(vec2(p.x,p.y-ep));
	return vec2(gradx,grady);
}

float flow(in vec2 p)
{
	float z=2.;
	float rz = 0.;
	vec2 bp = p;
	for (float i= 1.;i < 7.;i++ )
	{
		//primary flow speed
    p += pSpeed1;

		//secondary flow speed (speed of the perceived flow)
    bp +=pSpeed2;

		//displacement field (try changing time multiplier)
		vec2 gr = gradn(i*p*.34+time*1.);

		//rotation of the displacement field
		gr*=makem2(time*6.-(0.05*p.x+0.03*p.y)*40.);

		//displace the system
		p += gr*.5;

		//add noise octave
		rz+= (sin(noise(p)*7.)*0.5+0.5)/z;

		//blend factor (blending displaced system with base system)
		//you could call this advection factor (.5 being low, .95 being high)
		p = mix(bp,p,.77);

		//intensity scaling
		z *= 1.4;
		//octave scaling
		p *= pOctScale;
		bp *= 1.9;
	}
	return rz;
}

void main()
{
	vec2 p = (gl_FragCoord.xy-vec2(viewportX,viewportY)) / iResolution.xy-0.5;
  p=p*size+pos*size;
	p.x *= iResolution.x/iResolution.y;
	p*= 3.;
	float rz = flow(p);

//	vec3 col = vec3(.2,0.07,0.01)/rz;
	vec3 col = pColor/rz;
	col=pow(col,vec3(1.4));
  col=clamp(col,vec3(0.0),vec3(1.0));
	gl_FragColor = vec4(col,(col.r+col.g+col.b<threshold ? 0.0:1.0)*Alpha);
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="iGlobalTime" VariableRef="uDynamicTime"/>
        <ShaderVariable VariableName="resX" Value="1163" ValuePropRef="App.ViewportWidth"/>
        <ShaderVariable VariableName="resY" Value="432" ValuePropRef="App.ViewportHeight"/>
        <ShaderVariable VariableName="viewportX" Value="262" ValuePropRef="App.ViewportX"/>
        <ShaderVariable VariableName="viewportY" Value="262" ValuePropRef="App.ViewportY"/>
        <ShaderVariable VariableName="Alpha" ValuePropRef="1-Parameters[0];"/>
        <ShaderVariable VariableName="pSpeed1" VariableRef="uSpeed1"/>
        <ShaderVariable VariableName="pSpeed2" VariableRef="uSpeed2"/>
        <ShaderVariable VariableName="pOctScale" ValuePropRef="Parameters[9]*3.5+.5;"/>
        <ShaderVariable VariableName="pColor" VariableRef="ShaderColor"/>
        <ShaderVariable VariableName="pos" VariableRef="uPos"/>
        <ShaderVariable VariableName="size" VariableRef="uSize"/>
        <ShaderVariable VariableName="threshold" VariableRef="uThreshold"/>
      </UniformVariables>
    </Shader>
    <Model Name="ScreenModel">
      <OnRender>
        <UseMaterial Material="ScreenMaterial"/>
        <RenderSprite/>
      </OnRender>
    </Model>
    <Bitmap Name="NoiseBitmap" Comment="NoCustom" Width="256" Height="256">
      <Producers>
        <BitmapExpression>
          <Expression>
<![CDATA[//X,Y : current coordinate (0..1)
//Pixel : current color (rgb)
//Sample expression: Pixel.R=abs(sin(X*16));
float s=1.0*rnd();
Pixel.r=s;
Pixel.g=s;
Pixel.b=s;]]>
          </Expression>
        </BitmapExpression>
      </Producers>
    </Bitmap>
    <Variable Name="ShaderColor" Type="7"/>
    <Constant Name="AuthorInfo" Type="2">
      <StringValue>
<![CDATA[nimitz 
Twitter: @stormoid
Adapted from https://www.shadertoy.com/view/lslXRS]]>
      </StringValue>
    </Constant>
  </Content>
</ZApplication>
