FLhd   0  ` FLdt%  �	12.2.0.3 �.Z G a m e E d i t o r   V i s u a l i z e r   �4               A                  �  '   �  ~  �    �HQV ե3�  [General]
GlWindowMode=1
LayerCount=7
FPS=1
DmxOutput=0
DmxDevice=0
MidiPort=-1
Aspect=1
LayerOrder=0,1,2,3,4,5,6
Info=

[AppSet0]
App=Particles\ColorBlobs
ParamValues=704,0,0,0,629,500,500,87,477,200
ParamValuesBackground\FogMachine=500,0,0,500,218,500,500,500,500,1000,550,0
ParamValuesBackground\ItsFullOfStars=0,636,591,0,723,500,500,297,465,0,0
ParamValuesCanvas effects\FreqRing=500,500,500,500,218,500,500,500,0,312,123,500,500,500
ParamValuesImage effects\ImageWarp=0,0,0,0,0,500,500,19
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet1]
App=Feedback\FeedMe
ParamValues=474,0,0,807,541,823,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet2]
App=Postprocess\ColorCyclePalette
ParamValues=1,5,6,0,8,861,674,500,116,2,0
ParamValuesFeedback\70sKaleido=0,0,0,1000,824,828
ParamValuesFeedback\FeedMe=0,0,1000,1000,846,1000,0
ParamValuesFeedback\SphericalProjection=60,0,0,0,750,722,500,763,632,282,1000,500,500,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesImage effects\ImageWarp=0,0,0,0,0,0,812,234
Input=0
Enabled=1
UseBufferOutput=1
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet3]
App=Canvas effects\Electric
ParamValues=0,740,484,780,752,500,500,100
ParamValuesCanvas effects\Digital Brain=0,0,0,0,1000,500,500,600,100,300,100,133,1000,500,1000
ParamValuesCanvas effects\Flaring=0,1000,1000,1000,1000,500,500,61,0,0,500
ParamValuesCanvas effects\Lava=0,0,500,720,1000,500,500,576,357,500,500
ParamValuesCanvas effects\OverlySatisfying=0,563,484,780,600,500,500,100,100,1000,500
ParamValuesImage effects\Image=0,0,0,0,0,500,500,0,0,0
ParamValuesMisc\CoreDump=0,0,0,0,56,500,500,519,241,0,181,0,0,0,1000,922,102
ParamValuesObject Arrays\8x8x8_Eggs=0,0,0,0,591,481,684,309,666,244,501,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,321,500,500,818,347,500,500,782,639
ParamValuesPostprocess\ColorCyclePalette=500,500,375,667,0,1000,0,500,204,500,0
Input=0
Enabled=1
UseBufferOutput=1
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet4]
App=Blend\BufferBlender
ParamValues=0,1,11,1000,2,0,838,0,500,500,932,0
ParamValuesCanvas effects\Digital Brain=519,489,0,0,1000,500,500,600,100,300,100,250,1000,500,1000
ParamValuesCanvas effects\Flaring=0,804,669,1000,1000,526,500,132,0,600,500
ParamValuesCanvas effects\Flow Noise=0,0,500,720,1000,500,500,213,100,1000,500
ParamValuesCanvas effects\FreqRing=500,500,500,500,722,500,500,500,0,312,123,500,500,500
Input=0
Enabled=1
UseBufferOutput=1
BufferRenderQuality=0
ImageIndex=0
MeshIndex=0

[AppSet5]
App=Feedback\70sKaleido
ParamValues=0,0,0,1000,0,500
ParamValuesBackground\FourCornerGradient=67,1000,0,1000,1000,250,1000,1000,500,1000,1000,750,1000,1000
ParamValuesCanvas effects\Digital Brain=0,0,0,0,108,500,500,600,100,300,100,250,0,0,1000
ParamValuesCanvas effects\Electric=0,740,484,780,1000,500,500,0,100,1000,500
ParamValuesCanvas effects\Flow Noise=0,0,500,720,405,500,500,382,100,1000,500
ParamValuesPostprocess\BufferBlender=0,111,200,1000,0,0,0,0,500,500,750
Input=0
Enabled=1
UseBufferOutput=1
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet6]
App=Blend\BufferBlender
ParamValues=0,1,8,1000,2,1,809,0,500,500,1000,0
ParamValuesFeedback\70sKaleido=0,0,0,364,1000,1000
ParamValuesFeedback\FeedMe=0,0,0,1000,888,951,0
ParamValuesFeedback\SphericalProjection=60,0,0,0,154,425,500,1000,500,500,0,333,530,352,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesFeedback\WarpBack=500,0,0,0,500,500,405
ParamValuesFeedback\WormHoleEclipse=0,0,0,593,949,116,481,793,755,624,500
ParamValuesImage effects\Image=0,0,0,0,0,500,500,0,0,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=2
MeshIndex=0

[AppSet7]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet8]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet9]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet10]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet11]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet12]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet13]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet14]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet15]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet16]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet17]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet18]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet19]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet20]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet21]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet22]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet23]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet24]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=44100
VideoCodec=5
AudioCodec=3
AudioCodecFormat=6
VideoQuality=0
MaxKeyFrameSpacing=6000
Filename=E:\PrimordialSoup_preview.wmv
Bitrate=30000000
Uncompressed=0

[UserContent]
Text="This is the default text."
Html=
VideoCues=
Meshes=
MeshAutoScale=0
MeshWithColors=0
Images=
VideoUseSync=0

[Detached]
Top=67
Left=807
Width=412
Height=280

