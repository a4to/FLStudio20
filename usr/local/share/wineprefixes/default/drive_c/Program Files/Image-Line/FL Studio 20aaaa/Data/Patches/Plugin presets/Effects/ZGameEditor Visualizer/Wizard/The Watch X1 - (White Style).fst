FLhd   0 * ` FLdtg  �20.6.2.1597 �=  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ��'�  ﻿[General]
GlWindowMode=1
LayerCount=18
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=4,7,5,6,0,1,2,16,11,10,3,9,8,12,14,13,15,17
WizardParams=851

[AppSet4]
App=Image effects\Image
FParamValues=0,0,0,0,0.744,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,744,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1
Name=Watch - Buttons

[AppSet7]
App=Image effects\Image
FParamValues=0,0,0,0.888,0.848,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,888,848,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=3
Name=Watch - Strap

[AppSet5]
App=Image effects\Image
FParamValues=0,0,0,0,0.848,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,848,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
Name=Watch - Border

[AppSet6]
App=Image effects\Image
FParamValues=0,0,0,0,0.744,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,744,500,500,0,0,0,0,0,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1
ImageIndex=2
Name=Watch - Rim

[AppSet0]
App=Canvas effects\Digital Brain
FParamValues=0,0,0,0,1,0.5,0.5,0.6,0.1,0.3,0.1,0.25,1,0.5,1,0
ParamValues=0,0,0,0,1000,500,500,600,100,300,100,250,1000,500,1,0
ParamValuesBackground\SolidColor=0,616,1000,224
Enabled=1
Collapsed=1
Name=Effects BG 1

[AppSet1]
App=Postprocess\FrameBlur
FParamValues=0.184,0.48,0,0.412,0.386
ParamValues=184,480,0,412,386
ParamValuesCanvas effects\TaffyPulls=576,604,1000,0,0,500,500,100,500,500,0,0,0,500
ParamValuesCanvas effects\ShimeringCage=0,0,0,0,72,500,500,0,716,0,84,212,0,200
Enabled=1
UseBufferOutput=1
Collapsed=1
Name=Effects BG 2

[AppSet2]
App=Background\SolidColor
FParamValues=0,0,0,0.336
ParamValues=0,0,0,336
Enabled=1
Collapsed=1
Name=Background

[AppSet16]
App=HUD\HUD Grid
AppVersion=1
FParamValues=0,0.5,0,0.4,0.5,0.5,1,1,1,4,0.5,1,0.5,0.404,0.076,0.5,1,0.092,0.676,0.3,0,1
ParamValues=0,500,0,400,500,500,1000,1000,1000,4,500,1000,500,404,76,500,1000,92,676,300,0,1
ParamValuesBackground\SolidColor=0,0,0,336
Enabled=1
Collapsed=1
Name=Grids

[AppSet11]
App=Image effects\Image
FParamValues=0,0,0,0,0.272,0.404,0.5,0,0.115,0.25,0,1,0,0.524
ParamValues=0,0,0,0,272,404,500,0,115,250,0,1,0,524
Enabled=1
Collapsed=1
ImageIndex=4
Name=LCD - 1

[AppSet10]
App=Image effects\Image
FParamValues=0,0,0,0,0.272,0.5,0.5,0,0.115,0.25,0,1,0,0.524
ParamValues=0,0,0,0,272,500,500,0,115,250,0,1,0,524
Enabled=1
Collapsed=1
ImageIndex=4
Name=LCD - 2

[AppSet3]
App=Image effects\Image
FParamValues=0,0,0,0,0.272,0.596,0.5,0,0.115,0.25,0,1,0,0.524
ParamValues=0,0,0,0,272,596,500,0,115,250,0,1,0,524
Enabled=1
Collapsed=1
ImageIndex=4
Name=LCD - 3

[AppSet9]
App=Image effects\Image
FParamValues=0,0,0,0.148,1,0.437,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,148,1000,437,500,0,0,0,1,0,0,0
Enabled=1
Collapsed=1
ImageIndex=5
Name=Main Watch 1

[AppSet8]
App=Image effects\Image
FParamValues=0,0,0,0.148,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,148,1000,500,500,0,0,0,1,0,0,0
Enabled=1
Collapsed=1
ImageIndex=5
Name=Main Watch 2

[AppSet12]
App=Image effects\Image
FParamValues=0,0,0,0.148,1,0.563,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,148,1000,563,500,0,0,0,1,0,0,0
Enabled=1
Collapsed=1
ImageIndex=5
Name=Main Watch 3

[AppSet14]
App=Text\TextTrueType
FParamValues=0,0,0,0,0,0.5,0.5,0,0,0,0.5
ParamValues=0,0,0,0,0,500,500,0,0,0,500
Enabled=1
Collapsed=1
Name=Main Text

[AppSet13]
App=Background\FourCornerGradient
FParamValues=13,0.192,0.496,0.696,1,0.67,0.68,1,0.76,0.656,1,0.842,0.8,0.816
ParamValues=13,192,496,696,1000,670,680,1000,760,656,1000,842,800,816
Enabled=1
Collapsed=1
Name=Filter Color

[AppSet15]
App=Background\SolidColor
FParamValues=1,0,0,1
ParamValues=1000,0,0,1000
Enabled=1
Collapsed=1
Name=Fide in-out

[AppSet17]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position y=""5""><p align=""center""><font face=""American-Captain"" size=""5.5"" color=""#212121"">[author]</font></p></position>","<position y=""9""><p align=""center""><font face=""Chosence-Bold"" size=""3.2"" color=""#212121"">[title]</font></p></position>","<position y=""16""><p align=""center""><font face=""Chosence-Bold"" size=""3"" color=""#212121"">[comment]</font></p></position>"
Images="[presetpath]Wizard\ColoveContent\Others\COLOVE Watch\The Watch X1 by COLOVE - Border.svg","[presetpath]Wizard\ColoveContent\Others\COLOVE Watch\The Watch X1 by COLOVE - Buttons.svg","[presetpath]Wizard\ColoveContent\Devices\png\Display\The Watch X1 by COLOVE - Rim.png","[presetpath]Wizard\ColoveContent\Others\COLOVE Watch\The Watch X1 by COLOVE - Strap.svg"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

