<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" NoSound="1" FileVersion="2">
  <OnLoaded>
    <SpawnModel Model="ScreenModel"/>
    <ZLibrary Comment="HSV Library">
      <Source>
<![CDATA[vec3 hsv(float h, float s, float v)
{
  s = clamp(s/100, 0, 1);
  v = clamp(v/100, 0, 1);

  if(!s)return vector3(v, v, v);

  h = h < 0 ? frac(1-abs(frac(h/360)))*6 : frac(h/360)*6;

  float c, f, p, q, t;

  c = floor(h);
  f = h-c;

  p = v*(1-s);
  q = v*(1-s*f);
  t = v*(1-s*(1-f));

  switch(c)
  {
    case 0: return vector3(v, t, p);
    case 1: return vector3(q, v, p);
    case 2: return vector3(p, v, t);
    case 3: return vector3(p, q, v);
    case 4: return vector3(t, p, v);
    case 5: return vector3(v, p, q);
  }
}]]>
      </Source>
    </ZLibrary>
  </OnLoaded>
  <OnUpdate>
    <ZExpression DesignDisable="1">
      <Expression>
<![CDATA[const int
  ALPHA = 0,
  HUE = 1,
  SATURATION = 2,
  LIGHTNESS = 3;

vec3 c=hsv(Parameters[HUE]*360,Parameters[SATURATION]*100,(1-Parameters[LIGHTNESS])*100);

ShaderColor.r=c.r;
ShaderColor.g=c.g;
ShaderColor.b=c.b;]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <Content>
    <Group>
      <Children>
        <Array Name="Parameters" SizeDim1="11" Persistent="255">
          <Values>
<![CDATA[78DA63600082425B7B8673DFED18D6B9DBCF9A29690F1401E3B367CED88230034303980F00081A0C70]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Alpha
Hue
Saturation
Lightness]]>
          </StringValue>
        </Constant>
        <Constant Name="AuthorInfo" Type="2">
          <StringValue>
<![CDATA[nimitz 
Twitter: @stormoid
Adapted from https://www.shadertoy.com/view/Mts3zM]]>
          </StringValue>
        </Constant>
      </Children>
    </Group>
    <Material Name="ScreenMaterial" Shading="1" Light="0" Blend="1" ZBuffer="0" Shader="ScreenShader"/>
    <Shader Name="ScreenShader">
      <VertexShaderSource>
<![CDATA[varying vec2 position;

void main(){
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
  position=vec2(vertex.x,vertex.y);
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[//by nimitz (stormoid.com) (twitter: @stormoid)
//Adapted from https://www.shadertoy.com/view/Mts3zM

uniform float iGlobalTime;
uniform float resX;
uniform float resY;
uniform float viewportX;
uniform float viewportY;

uniform float Alpha;

vec2 iResolution = vec2(resX,resY);

//uniform float pCurvature;

//uniform vec3 pColor;

//Overly satisfying by nimitz (twitter: @stormoid)

//This might look like a lot of code but the base implementation of the gif itself is ~10loc

#define time iGlobalTime*2.6
#define pi 3.14159265

#define NUM 20.
#define PALETTE vec3(.0, 1.4, 2.)+1.5

#define COLORED
#define MIRROR
//#define ROTATE
#define ROT_OFST
#define TRIANGLE_NOISE

//#define SHOW_TRIANGLE_NOISE_ONLY

float aspect = iResolution.x/iResolution.y;
float w = 50./sqrt(iResolution.x*aspect+iResolution.y);

mat2 mm2(in float a){float c = cos(a), s = sin(a);return mat2(c,-s,s,c);}
float tri(in float x){return abs(fract(x)-.5);}
vec2 tri2(in vec2 p){return vec2(tri(p.x+tri(p.y*2.)),tri(p.y+tri(p.x*2.)));}
mat2 m2 = mat2( 0.970,  0.242, -0.242,  0.970 );

//Animated triangle noise, cheap and pretty decent looking.
float triangleNoise(in vec2 p)
{
    float z=1.5;
    float z2=1.5;
	float rz = 0.;
    vec2 bp = p;
	for (float i=0.; i<=3.; i++ )
	{
        vec2 dg = tri2(bp*2.)*.8;
        dg *= mm2(time*.3);
        p += dg/z2;

        bp *= 1.6;
        z2 *= .6;
		z *= 1.8;
		p *= 1.2;
        p*= m2;

        rz+= (tri(p.x+tri(p.y)))/z;
	}
	return rz;
}

void main()
{
    vec2 p = (gl_FragCoord.xy-vec2(viewportX,viewportY)) / iResolution.xy*2.-1.;
	p.x *= aspect;
    p*= 1.05;
    vec2 bp = p;

    #ifdef ROTATE
    p *= mm2(time*.25);
    #endif

    float lp = length(p);
    float id = floor(lp*NUM+.5)/NUM;

    #ifdef ROT_OFST
    p *= mm2(id*11.);
    #endif

    #ifdef MIRROR
    p.y = abs(p.y);
    #endif

    //polar coords
    vec2 plr = vec2(lp, atan(p.y, p.x));

    //Draw concentric circles
    float rz = 1.-pow(abs(sin(plr.x*pi*NUM))*1.25/pow(w,0.25),2.5);

    //get the current arc length for a given id
    float enp = plr.y+sin((time+id*5.5))*1.52-1.5;
    rz *= smoothstep(0., 0.05, enp);

    //smooth out both sides of the arcs (and clamp the number)
    rz *= smoothstep(0.,.022*w/plr.x, enp)*step(id,1.);
    #ifndef MIRROR
    rz *= smoothstep(-0.01,.02*w/plr.x,pi-plr.y);
    #endif

    #ifdef TRIANGLE_NOISE
    rz *= (triangleNoise(p/(w*w))*0.9+0.4);
    vec3 col = (sin(PALETTE+id*5.+time)*0.5+0.5)*rz;
    col += smoothstep(.4,1.,rz)*0.15;
    col *= smoothstep(.2,1.,rz)+1.;

    #else
    vec3 col = (sin(PALETTE+id*5.+time)*0.5+0.5)*rz;
    col *= smoothstep(.8,1.15,rz)*.7+.8;
    #endif

    #ifndef COLORED
    col = vec3(dot(col,vec3(.7)));
    #endif

    #ifdef SHOW_TRIANGLE_NOISE_ONLY
    col = vec3(triangleNoise(bp));
    #endif

  //Ville: make rings transparent
  float gcGrey=(col.x+col.y+col.z)/1.0;

	gl_FragColor = vec4(col,Alpha*gcGrey);
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="iGlobalTime" ValuePropRef="App.Time"/>
        <ShaderVariable VariableName="resX" Value="1163" ValuePropRef="App.ViewportWidth"/>
        <ShaderVariable VariableName="resY" Value="432" ValuePropRef="App.ViewportHeight"/>
        <ShaderVariable VariableName="viewportX" Value="262" ValuePropRef="App.ViewportX"/>
        <ShaderVariable VariableName="viewportY" Value="262" ValuePropRef="App.ViewportY"/>
        <ShaderVariable VariableName="Alpha" ValuePropRef="1-Parameters[0];"/>
        <ShaderVariable DesignDisable="1" VariableName="pCurvature" ValuePropRef="Parameters[4]*200;"/>
        <ShaderVariable DesignDisable="1" VariableName="pColor" VariableRef="ShaderColor"/>
      </UniformVariables>
    </Shader>
    <Model Name="ScreenModel">
      <OnRender>
        <UseMaterial Material="ScreenMaterial"/>
        <RenderSprite/>
      </OnRender>
    </Model>
    <Variable Name="ShaderColor" Type="7"/>
  </Content>
</ZApplication>
