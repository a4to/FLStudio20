FLhd   0  ` FLdtZ  �20.6.0.1423 ��  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  Z   �   }  �  �    �HQV ���  ﻿[General]
GlWindowMode=1
LayerCount=2
FPS=1
MidiPort=-1
AspectRatio=16:9
LayerOrder=1,0

[AppSet1]
App=Background\FourCornerGradient
FParamValues=0,1,0.688,0.304,1,0.25,0.264,1,0.5,0.248,1,0.582,0.256,1
ParamValues=0,1000,688,304,1000,250,264,1000,500,248,1000,582,256,1000
Enabled=1

[AppSet0]
App=Object Arrays\Filaments
FParamValues=0,0,0,1,0.5,0.5,0.772,0.5,0.292,0.296,0.212,0,0.052,0
ParamValues=0,0,0,1000,500,500,772,500,292,296,212,0,52,0
ParamValuesImage effects\ImageSphereWarp=0,236,750,944,0,1000,0,500,500,500
ParamValuesCanvas effects\Digital Brain=0,0,0,0,640,500,500,600,100,300,100,125,1000,500,1000,0
ParamValuesFeedback\WormHoleEclipse=0,0,0,1000,584,668,780,824,500,500,500
ParamValuesImage effects\ImageTileSprite=4,4,0,0,0,0,4,250,250,500,500,500,500,0,0,0,0
ParamValuesMisc\FruityDanceLine=0,0,0,0,1000,500,500,0,300,100,0,0,0
ParamValuesHUD\HUD Image=0,0,500,500,1000,1000,500,444,500,0,0,1000,1000,0,1000
ParamValuesTerrain\CubesAndSpheres=0,0,0,0,112,500,116,650,140,400,860,1000,1000,1000,1000,1000,0,0
ParamValuesCanvas effects\Lava=0,0,500,512,1000,500,500,500,672,500,0
ParamValuesScenes\Cloud Ten=0,0,0,0,272,1000,531,0,0,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,942,500,500,818,500,644,0,696,80
ParamValuesCanvas effects\Electric=0,333,611,842,120,212,500,73,0,1000,500
ParamValuesMisc\CoreDump=0,208,1000,0,500,500,500,0,0,0,0,0,1000,456,292,408,0
ParamValuesScenes\Alps=0,71,362,0,250,348,304,0,0
ParamValuesHUD\HUD 3D=0,0,500,500,500,500,500,500,500,500,500,500,500,500,0,500,500,500,500,500,500,500,500,1000,1000,0,0,1000,1000,0
ParamValuesCanvas effects\Flow Noise=0,0,0,720,608,756,332,100,0,0,0
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""3""><position y=""3""><p align=""left""><font face=""American-Captain"" size=""10"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""3""><position y=""26""><p align=""left""><font face=""Chosence-Bold"" size=""8"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""0""><position y=""10""><p align=""right""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[extra1]</font></p></position>","<position x=""0""><position y=""3""><p align=""right""><font face=""Chosence-Bold"" size=""4"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""0""><position y=""20""><p align=""right""><font face=""Chosence-Bold"" size=""4 "" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""0""><position y=""25""><p align=""right""><font face=""Chosence-regular"" size=""3"" color=""#FFFFF"">[extra3]</font></p></position>",
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

