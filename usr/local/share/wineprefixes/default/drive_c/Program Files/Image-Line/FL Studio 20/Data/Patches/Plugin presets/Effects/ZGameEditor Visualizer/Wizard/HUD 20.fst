FLhd   0  0 FLdt6N  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   ����M  ﻿[General]
GlWindowMode=1
LayerCount=97
FPS=2
MidiPort=-1
Aspect=1
LayerOrder=92,70,94,96,95,71,80,81,82,83,88,89,77,79,78,76,56,2,90,23,54,51,49,46,43,40,37,57,63,65,66,73,91,24,28,29,26,31,9,13,12,11,14,8,0,3,15,19,17,61,60,72,1,16,34,10,7,4,6,69,68,18,21,5,20,27,22,32,25,30,33,36,84,87,86,85,75,39,42,41,45,48,50,53,55,62,38,44,47,52,64,67,74,58,59,35,93
WizardParams=3393,3394,3395,3396,3397,3398,3399,3400,3401,3447

[AppSet92]
App=Background\SolidColor
ParamValues=0,0,0,1000
Enabled=1
Collapsed=1

[AppSet70]
App=HUD\HUD Prefab
ParamValues=0,0,500,0,0,558,563,333,1000,1000,4,0,500,1,368,108,1000,1
ParamValuesImage effects\Image=0,0,0,0,664,570,548,0,0,0,0,0,0,0
Enabled=1
Name=LOGO
LayerPrivateData=78DA73C8CBCF4BD52B2E4B671899000060F9036F

[AppSet94]
App=HUD\HUD Prefab
ParamValues=15,0,500,0,0,558,575,110,1000,1000,4,0,167,1,368,386,430,1
Enabled=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C4CF53273CA18863D00000C6D0AA0

[AppSet96]
App=HUD\HUD Prefab
ParamValues=13,828,500,0,0,558,575,160,1000,1000,4,0,666,1,368,256,387,1
Enabled=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C8CF53273CA18863D00000A9F0A9E

[AppSet95]
App=Misc\Automator
ParamValues=1,95,13,3,0,2,750,1,97,13,3,1000,4,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1

[AppSet71]
App=Postprocess\Youlean Bloom
ParamValues=60,0,118,450,1000,0,0,0
ParamValuesPostprocess\Blooming=0,0,0,0,400,1000,248,0,0
ParamValuesPostprocess\Youlean Motion Blur=1000,1000,0,0
Enabled=1
UseBufferOutput=1

[AppSet80]
App=HUD\HUD Prefab
ParamValues=42,0,128,1000,0,62,764,146,899,1000,4,0,500,1,368,693,90,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0CCCF53273CA18460000002C6C0AB3

[AppSet81]
App=HUD\HUD Prefab
ParamValues=45,0,500,1000,0,171,764,146,1000,1000,4,0,500,1,368,7,269,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C0DF43273CA184600000026F70AAD

[AppSet82]
App=HUD\HUD Prefab
ParamValues=44,0,128,1000,0,171,897,212,1000,1000,4,0,500,1,368,693,90,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C2CF53273CA18460000002E3E0AB5

[AppSet83]
App=HUD\HUD Prefab
ParamValues=41,0,500,1000,0,62,901,170,1000,821,4,0,500,1,368,7,269,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0CCCF43273CA18460000002B830AB2

[AppSet88]
App=Misc\Automator
ParamValues=1,81,16,2,500,558,250,1,81,17,2,500,786,250,1,82,16,2,500,766,250,1,82,17,2,500,526,250
Enabled=1
Collapsed=1

[AppSet89]
App=Misc\Automator
ParamValues=1,83,16,2,500,558,250,1,83,17,2,500,786,250,1,84,16,2,500,766,250,1,84,17,2,500,526,250
Enabled=1
Collapsed=1

[AppSet77]
App=HUD\HUD Prefab
ParamValues=11,164,500,0,0,325,830,132,1000,1000,4,0,915,1,368,447,539,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0CF53273CA18863D000008D10A9C

[AppSet79]
App=HUD\HUD Prefab
ParamValues=11,330,500,0,0,325,830,96,1000,1000,4,0,249,1,368,542,641,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0CF53273CA18863D000008D10A9C

[AppSet78]
App=Misc\Automator
ParamValues=1,78,13,3,1000,7,250,1,80,13,3,1000,3,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1
Collapsed=1

[AppSet76]
App=HUD\HUD Mesh
ParamValues=120,80,948,1000,174,341,295,3,0,150,500,500,500,500,348,1
Enabled=1
Collapsed=1
MeshIndex=2

[AppSet56]
App=HUD\HUD Prefab
ParamValues=187,276,500,0,0,558,866,253,486,1000,4,0,250,1,400,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B48CC4BCD298E290051BA0606867A9939650C23080000E80D0727

[AppSet2]
App=HUD\HUD Prefab
ParamValues=191,204,500,1000,0,821,624,306,1000,1000,4,0,500,1,436,0,476,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B48CC4BCD298E290051BA0606A67A9939650C23080000EBD1072B

[AppSet90]
App=HUD\HUD Prefab
ParamValues=191,204,500,0,0,821,624,306,1000,1000,4,0,500,1,436,520,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B48CC4BCD298E290051BA0606A67A9939650C23080000EBD1072B

[AppSet23]
App=HUD\HUD Prefab
ParamValues=193,0,204,972,0,241,328,250,808,1000,4,0,500,1,416,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B48CC4BCD298E290051BA0606E67A9939650C23080000EDB3072D

[AppSet54]
App=HUD\HUD Prefab
ParamValues=52,72,128,1000,0,779,110,296,1000,1000,4,0,500,1,328,534,95,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0CCDF53273CA18460000002D560AB4

[AppSet51]
App=HUD\HUD Prefab
ParamValues=53,52,500,1000,0,603,147,196,696,1000,4,0,500,1,328,769,381,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C2DF43273CA18460000002E3F0AB5

[AppSet49]
App=HUD\HUD Prefab
ParamValues=54,0,128,1000,0,489,147,170,1000,1000,4,0,500,1,328,534,95,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C2DF53273CA18460000002F280AB6

[AppSet46]
App=HUD\HUD Prefab
ParamValues=56,0,128,1000,0,393,627,170,1000,1000,4,0,500,1,368,18,316,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D8C0CF53273CA184600000028CA0AAF

[AppSet43]
App=HUD\HUD Prefab
ParamValues=63,0,500,1000,0,275,627,170,1000,1000,4,0,500,1,368,602,71,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D8C2CF43273CA18460000002F290AB6

[AppSet40]
App=HUD\HUD Prefab
ParamValues=64,0,128,1000,0,121,627,170,1000,1000,4,0,500,1,328,509,457,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D8C2CF53273CA184600000030120AB7

[AppSet37]
App=HUD\HUD Prefab
ParamValues=65,0,128,1000,0,111,500,154,1000,1000,4,0,500,1,328,100,855,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D8C0DF43273CA184600000028CB0AAF

[AppSet57]
App=HUD\HUD Prefab
ParamValues=47,0,500,964,0,562,862,240,929,1000,4,0,500,1,328,0,775,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C8DF43273CA184600000028C90AAF

[AppSet63]
App=HUD\HUD Prefab
ParamValues=43,0,148,928,0,554,349,247,929,1000,4,0,500,1,328,0,90,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C2CF43273CA18460000002D550AB4

[AppSet65]
App=HUD\HUD Prefab
ParamValues=218,0,500,508,0,32,403,91,1000,1000,4,2,250,1,368,0,0,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0606867A9939650C23030000A1AD0906

[AppSet66]
App=HUD\HUD Prefab
ParamValues=218,0,500,704,0,49,403,91,1000,1000,4,2,250,1,368,0,0,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0606867A9939650C23030000A1AD0906

[AppSet73]
App=HUD\HUD Prefab
ParamValues=219,0,500,676,0,977,496,295,1000,1000,4,2,250,1,368,0,0,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0606467A9939650C23030000A2990907

[AppSet91]
App=HUD\HUD Prefab
ParamValues=219,0,500,824,0,977,496,295,1000,1000,4,2,250,1,368,0,0,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0606467A9939650C23030000A2990907

[AppSet24]
App=HUD\HUD Callout Line
ParamValues=0,500,1,0,252,305,420,207,1,0,0,200,212,450,100,1
Enabled=1
Collapsed=1

[AppSet28]
App=HUD\HUD Callout Line
ParamValues=0,500,0,0,817,409,682,274,1,242,0,200,212,549,100,1
Enabled=1
Collapsed=1

[AppSet29]
App=HUD\HUD Callout Line
ParamValues=0,500,151,0,682,273,418,207,3,0,0,200,212,619,100,1
Enabled=1
Collapsed=1

[AppSet26]
App=HUD\HUD Callout Line
ParamValues=0,128,224,0,69,266,247,400,1,83,4,200,212,450,100,1
Enabled=1
Collapsed=1

[AppSet31]
App=Postprocess\Youlean Bloom
ParamValues=1000,0,122,450,1000,0,0,0
ParamValuesPostprocess\Youlean Motion Blur=1000,1000,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet9]
App=HUD\HUD Free Line
ParamValues=0,152,1000,0,730,890,700,100,0,100,0,100,1,0,0,297,250
Enabled=1
Collapsed=1
LayerPrivateData=78DA6360606048129861C7C0D000C2FB19E060943DCA269F0D00A4F03F16

[AppSet13]
App=HUD\HUD Free Line
ParamValues=0,152,1000,0,712,381,279,100,0,100,0,100,1,0,0,0,500
Enabled=1
Collapsed=1
LayerPrivateData=78DA636080037B068686FD08EE287B944D3E1B00865B3D4F

[AppSet12]
App=HUD\HUD Free Line
ParamValues=0,152,1000,0,712,858,279,100,0,100,0,100,1,0,0,220,500
Enabled=1
Collapsed=1
LayerPrivateData=78DA6360606058E79E6807A4EC19181AF633C0C1287B944D3E1B0031643EE3

[AppSet11]
App=HUD\HUD Free Line
ParamValues=0,152,1000,0,712,399,393,100,0,100,0,100,1,0,0,220,500
Enabled=1
Collapsed=1
LayerPrivateData=78DA6360606058E79E6807A4EC19181AF633C0C1287B944D3E1B0031643EE3

[AppSet14]
App=Misc\Automator
ParamValues=1,10,5,2,818,250,456,1,14,6,2,627,250,377,1,0,6,2,627,150,377,1,12,6,2,627,122,377
Enabled=1
Collapsed=1

[AppSet8]
App=Postprocess\Blooming
ParamValues=0,0,0,1000,500,800,500,728,1000
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet0]
App=HUD\HUD Prefab
ParamValues=1,640,0,0,0,500,500,284,1000,567,4,0,500,1,368,0,1000,1
Enabled=1
UseBufferOutput=1
Collapsed=1
LayerPrivateData=78DA4B4A4CCE4E2FCA2FCD4B89490232750D0C0CF53273CA18460A0000F04C0845

[AppSet3]
App=HUD\HUD Mesh
ParamValues=0,500,496,1000,172,792,476,3,0,150,500,0,500,90,700,1
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet15]
App=Misc\CoreDump
ParamValues=0,0,0,0,500,500,500,684,0,0,0,0,0,0,0,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1
ImageIndex=9

[AppSet19]
App=HUD\HUD Prefab
ParamValues=13,280,500,1000,0,500,500,58,1000,1000,4,0,812,1,648,0,427,1
Enabled=1
UseBufferOutput=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C8CF53273CA18863D00000A9F0A9E

[AppSet17]
App=HUD\HUD Prefab
ParamValues=11,0,128,0,0,500,500,63,1000,1000,4,0,829,1,0,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCA4C5748CE2C4ACE492D8E49CA4CD785B0750D0C0CF53273CA18863D000008D10A9C

[AppSet61]
App=Postprocess\ParameterShake
ParamValues=452,712,17,14,0,0,0,0,4,0,0,0,2,3,1,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet60]
App=Postprocess\Blooming
ParamValues=0,0,0,1000,500,800,500,992,0
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet72]
App=Image effects\Image
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=8

[AppSet1]
App=Image effects\ImageSlices
ParamValues=0,0,0,0,942,500,500,0,0,684,500,333,0,500,0,0,0
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet16]
App=Image effects\Image
ParamValues=0,0,0,0,548,976,612,0,322,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=4

[AppSet34]
App=Image effects\Image
ParamValues=0,0,0,0,548,32,367,0,259,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=4

[AppSet10]
App=Image effects\Image
ParamValues=787,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=3

[AppSet7]
App=HUD\HUD Prefab
ParamValues=75,0,160,224,0,820,627,306,221,462,4,0,500,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4B2FCA4C89490712BA0686067A9939650C230B0000B42505E0

[AppSet4]
App=Misc\Automator
ParamValues=1,4,14,2,500,54,250,1,8,4,2,500,1000,250,1,18,13,3,1000,14,250,1,18,4,2,500,250,250
ParamValuesPostprocess\ParameterShake=800,0,3,13,0,0,0,0,3,0,0,0,6,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet6]
App=Image effects\Image
ParamValues=484,0,0,1000,1000,495,492,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=2

[AppSet69]
App=HUD\HUD Prefab
ParamValues=218,680,500,0,0,32,403,91,1000,1000,4,2,250,1,368,0,883,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0606867A9939650C23030000A1AD0906

[AppSet68]
App=HUD\HUD Prefab
ParamValues=218,680,500,0,0,49,403,91,1000,1000,4,2,250,1,368,0,954,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0606867A9939650C23030000A1AD0906

[AppSet18]
App=HUD\HUD 3D
ParamValues=428,0,748,636,500,500,500,491,265,500,480,500,500,500,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,1
Enabled=1
Collapsed=1
ImageIndex=7
MeshIndex=3

[AppSet21]
App=Misc\Automator
ParamValues=1,21,4,2,535,126,476,1,20,13,3,1000,62,250,1,23,13,2,500,36,430,1,23,4,2,500,250,250
Enabled=1
Collapsed=1

[AppSet5]
App=Image effects\Image
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=2

[AppSet20]
App=HUD\HUD 3D
ParamValues=588,0,653,534,500,500,500,491,305,579,480,500,500,500,0,500,500,500,500,500,500,500,500,1,0,0,0,1000,1000,1
Enabled=1
Collapsed=1
ImageIndex=5
MeshIndex=3

[AppSet27]
App=HUD\HUD Prefab
ParamValues=3,736,500,1000,0,241,328,122,808,1000,4,0,500,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4B4A4CCE4E2FCA2FCD4B89490232750D0C8CF53273CA18460A0000F2280847

[AppSet22]
App=HUD\HUD Mesh
ParamValues=628,500,496,0,225,267,588,3,0,150,500,500,372,500,588,1
Enabled=1
Collapsed=1
MeshIndex=1

[AppSet32]
App=Image effects\Image
ParamValues=468,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=6

[AppSet25]
App=Postprocess\ParameterShake
ParamValues=1000,0,24,9,86,0,29,9,4,3,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet30]
App=Misc\Automator
ParamValues=1,30,3,3,604,340,250,1,29,3,3,604,423,250,1,25,3,3,604,294,250,1,27,3,2,500,1000,250
Enabled=1
Collapsed=1

[AppSet33]
App=Postprocess\Youlean Color Correction
ParamValues=500,500,500,500,500,604
Enabled=1
Collapsed=1

[AppSet36]
App=HUD\HUD Prefab
ParamValues=65,292,500,0,0,111,500,154,1000,1000,4,0,500,1,328,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D8C0DF43273CA184600000028CB0AAF

[AppSet84]
App=HUD\HUD Prefab
ParamValues=41,356,500,0,0,62,901,170,1000,821,4,0,500,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0CCCF43273CA18460000002B830AB2

[AppSet87]
App=HUD\HUD Prefab
ParamValues=42,356,500,0,0,62,764,146,899,1000,4,0,500,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0CCCF53273CA18460000002C6C0AB3

[AppSet86]
App=HUD\HUD Prefab
ParamValues=45,356,500,0,0,171,764,146,1000,1000,4,0,500,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C0DF43273CA184600000026F70AAD

[AppSet85]
App=HUD\HUD Prefab
ParamValues=44,356,500,0,0,171,897,212,1000,1000,4,0,500,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C2CF53273CA18460000002E3E0AB5

[AppSet75]
App=HUD\HUD Prefab
ParamValues=219,664,500,0,0,983,496,295,888,1000,4,2,250,1,368,0,901,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B28CA4F2F4A2D2E56484A2C2A8E0112BA0606467A9939650C23030000A2990907

[AppSet39]
App=HUD\HUD Prefab
ParamValues=64,444,500,0,0,121,627,170,1000,1000,4,0,500,1,328,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D8C2CF53273CA184600000030120AB7

[AppSet42]
App=HUD\HUD Prefab
ParamValues=189,0,500,0,0,303,627,225,245,1000,4,0,250,1,300,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA2B48CC4BCD298E290051BA0606C67A9939650C23080000E9EF0729

[AppSet41]
App=HUD\HUD Prefab
ParamValues=63,292,500,0,0,275,627,170,1000,1000,4,0,500,1,328,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D8C2CF43273CA18460000002F290AB6

[AppSet45]
App=HUD\HUD Prefab
ParamValues=56,276,500,0,0,393,627,170,1000,1000,4,0,500,1,328,0,412,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D8C0CF53273CA184600000028CA0AAF

[AppSet48]
App=HUD\HUD Prefab
ParamValues=54,276,500,0,0,489,147,170,1000,1000,4,0,500,1,328,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C2DF53273CA18460000002F280AB6

[AppSet50]
App=HUD\HUD Prefab
ParamValues=53,276,500,0,0,603,147,196,696,1000,4,0,500,1,328,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C2DF43273CA18460000002E3F0AB5

[AppSet53]
App=HUD\HUD Prefab
ParamValues=52,368,500,0,0,779,110,296,1000,1000,4,0,500,1,328,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0CCDF53273CA18460000002D560AB4

[AppSet55]
App=HUD\HUD Prefab
ParamValues=47,732,500,0,0,562,862,240,929,1000,4,0,500,1,328,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C8DF43273CA184600000028C90AAF

[AppSet62]
App=HUD\HUD Prefab
ParamValues=43,570,500,0,0,554,349,247,929,1000,4,0,500,1,328,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C2CF43273CA18460000002D550AB4

[AppSet38]
App=Misc\Automator
ParamValues=1,38,16,2,500,506,250,1,38,17,2,500,414,250,1,41,16,2,500,406,250,1,41,17,2,500,298,250
Enabled=1
Collapsed=1

[AppSet44]
App=Misc\Automator
ParamValues=1,44,16,2,500,782,250,1,44,17,2,500,614,250,1,47,16,2,223,810,397,1,47,17,2,251,666,432
Enabled=1
Collapsed=1

[AppSet47]
App=Misc\Automator
ParamValues=1,50,16,2,500,438,250,1,50,17,2,500,618,250,1,52,16,2,500,494,250,1,52,17,2,500,762,250
Enabled=1
Collapsed=1

[AppSet52]
App=Misc\Automator
ParamValues=1,55,16,2,500,438,250,1,55,17,2,500,618,250,1,58,16,2,500,702,250,1,58,17,2,500,630,250
Enabled=1
Collapsed=1

[AppSet64]
App=Misc\Automator
ParamValues=1,64,16,2,500,702,250,1,64,17,2,500,786,250,1,58,16,2,500,702,250,1,58,17,2,500,630,250
Enabled=1
Collapsed=1

[AppSet67]
App=Postprocess\ParameterShake
ParamValues=1000,73,65,16,1000,73,66,16,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet74]
App=Postprocess\ParameterShake
ParamValues=1000,73,73,16,1000,73,91,16,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet58]
App=HUD\HUD Prefab
ParamValues=167,0,500,0,0,284,53,250,1000,1000,4,2,500,1,368,0,564,1
Enabled=1
Collapsed=1
LayerPrivateData=78DACBC9CC4B2D8E294E2D482C4A2CC92F4262EA1A1819EA65E694310C6B000056200D22

[AppSet59]
App=HUD\HUD Prefab
ParamValues=167,0,500,0,0,19,523,250,1000,1000,4,2,250,1,368,538,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DACBC9CC4B2D8E294E2D482C4A2CC92F4262EA1A1819EA65E694310C6B000056200D22

[AppSet35]
App=Text\TextTrueType
ParamValues=0,0,0,0,0,493,500,0,0,0,500
Enabled=1
Collapsed=1

[AppSet93]
App=Peak Effects\Linear
ParamValues=0,120,648,0,134,704,753,112,0,500,740,242,1000,0,1,0,0,500,500,500,500,0,500,152,200,0,32,212,330,250,100
Enabled=1
Collapsed=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0

[UserContent]
Text=" "
Html="<position x=""45""><position y=""22""><p align=""left""><font face=""American-Captain"" size=""5.5"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""45""><position y=""27""><p align=""left""><font face=""Chosence-Bold"" size=""4"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""33""><position y=""50""><p align=""left""><font face=""Chosence-Bold"" size=""2"" color=""#FFFFFF"">[extra1]</font></p></position>","<position x=""0""><position y=""95""><p align=""right""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""35""><position y=""13""><p align=""left""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""33""><position y=""74""><p align=""center""><font face=""Chosence-regular"" size=""2.5"" color=""#FFFFF"">[extra3]</font></p></position>","  "
Meshes=[plugpath]Content\Meshes\Hero.zgeMesh,[plugpath]Content\Meshes\Brain.zgeMesh,[plugpath]Content\Meshes\Skull.zgeMesh
Images=[plugpath]Effects\HUD\prefabs\components\component-002.ilv
VideoUseSync=0
EnableMipmap=1

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

