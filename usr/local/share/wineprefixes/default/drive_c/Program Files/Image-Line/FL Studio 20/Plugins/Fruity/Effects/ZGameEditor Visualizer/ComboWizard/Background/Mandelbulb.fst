FLhd   0  ` FLdtB  �20.5.1.1193 ��  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4               A                        �  �  �    �HQV ո�  ﻿[General]
GlWindowMode=1
LayerCount=2
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=0,5

[AppSet0]
App=Scenes\Mandelbulb
ParamValues=300,322,500,476,500,500,863,500,120
ParamValuesPeak Effects\Fluidity=0,488,1000,0,472,508,512
ParamValuesScenes\Spherical Polyhedra=0,484,676,0,0,500,5,298,625,635,500,1
ParamValuesScenes\Xyptonjtroz=0,252,500,182,1000
ParamValuesPeak Effects\PeekMe=0,792,1000,328,500,500,552,150,520,0,892,0
ParamValuesScenes\Frozen Wasteland=0,0,0,0,364,1,0,0,0,0,0
ParamValuesPeak Effects\Reactive Sphere=0,1000,833,0,900,500,500,252,252,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesScenes\Space Jewels=0,500,0,180,232,1,500,181,0,0,0
ParamValuesTunnel\TorusJourney=0,0,0,0,0,500,500,532,992,1000,780,812,1000,1000,976,784,800,1000,64,0
ParamValuesBackground\Youlean Background MDL=1000,892,0,0,500,500,50,320,0,170,300,764,744,1000,0,300,500,0,0,500
ParamValuesTerrain\CubesAndSpheres=0,0,0,0,112,500,116,650,140,400,860,1000,1000,1000,1000,1000,0,0
ParamValuesTunnel\Oblivion=0,312,804,212,352,500,500,100,100,1000,500
ParamValuesScenes\Alien Thorns=0,500,542,724,4,832,468,808
ParamValuesScenes\Boaty Goes Caving=0,250,0
ParamValuesPeak Effects\Linear=0,760,648,0,574,500,160,1000,0,672,260,350,0,0,500,0,0,500,500,500,500,1000,0,0,1000,0,0,212,330,250,100
ParamValuesScenes\Neptune Racing=0,500,250,916,1000,848,932,0,118,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,948,500,500,500,500,500
ParamValuesScenes\Alps=0,167,206,0,250,628,628,0,0
Enabled=1

[AppSet5]
App=Misc\Automator
ParamValues=1,1,2,2,500,0,250,1,1,7,2,500,54,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="<position x=""3""><position y=""3""><p align=""left""><font face=""American-Captain"" size=""10"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""3""><position y=""26""><p align=""left""><font face=""Chosence-Bold"" size=""8"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""0""><position y=""10""><p align=""right""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[extra1]</font></p></position>","<position x=""0""><position y=""3""><p align=""right""><font face=""Chosence-Bold"" size=""4"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""0""><position y=""20""><p align=""right""><font face=""Chosence-Bold"" size=""4 "" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""0""><position y=""25""><p align=""right""><font face=""Chosence-regular"" size=""3"" color=""#FFFFF"">[extra3]</font></p></position>",
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

