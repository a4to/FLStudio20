FLhd   0 $ ` FLdt�  �	12.2.0.3 �.Z G a m e E d i t o r   V i s u a l i z e r   �4               A                  1  j   �  ~  �    �HQV ն22  [General]
GlWindowMode=1
LayerCount=9
FPS=2
DmxOutput=0
DmxDevice=0
MidiPort=0
Aspect=0
LayerOrder=0,1,2,3,4,5,6,7,8
Info=

[AppSet0]
App=Background\FogMachine
ParamValues=0,166,354,222,500,500,500,500,500,500,0,0
ParamValuesCanvas effects\N-gonFigure=0,599,654,0,632,494,500,157,0,170,0
ParamValuesCanvas effects\ShimeringCage=0,0,0,0,132,500,500,1000,473,0,0,0,0,0
ParamValuesFishEyeLens=0,142,442,868,1000,1000,0,211,111,0,0,1000,222,480
Input=0
Enabled=1
UseBufferOutput=1
BufferRenderQuality=0
ImageIndex=9
MeshIndex=0

[AppSet1]
App=Background\FogMachine
ParamValues=0,653,907,656,743,500,500,500,500,500,1000,89
ParamValuesBackground\ItsFullOfStars=0,0,0,0,90,500,500,654,0,0,0
ParamValuesFeedback\FeedMe=0,0,0,747,137,0,579
ParamValuesFeedback\SphericalProjection=0,0,0,0,544,425,382,731,901,369,0,500,500,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
Input=0
Enabled=1
UseBufferOutput=1
BufferRenderQuality=0
ImageIndex=9
MeshIndex=0

[AppSet2]
App=Blend\BufferBlender
ParamValues=0,1,2,1000,0,1,1000,0,500,500,750,1
ParamValuesBackground\SolidColor=0,833,0,1000
ParamValuesFishEyeLens=0,548,475,603,1000,1000,222,0,111,485,0,136,111,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=2
MeshIndex=0

[AppSet3]
App=Feedback\WarpBack
ParamValues=500,0,0,0,500,500,89
ParamValuesBlend\BufferBlender=0,0,133,1000,250,222,1000,0,500,500,750,1000
ParamValuesFeedback\SphericalProjection=60,0,0,0,1000,447,500,747,341,834,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesFishEyeLens=0,679,569,810,1000,1000,222,0,111,509,889,80,111,0
Input=0
Enabled=1
UseBufferOutput=1
BufferRenderQuality=0
ImageIndex=9
MeshIndex=0

[AppSet4]
App=Blend\BufferBlender
ParamValues=2,2,13,1000,2,2,1000,0,500,500,750,1
ParamValuesBackground\FogMachine=0,313,0,282,711,500,500,500,500,357,0,0
ParamValuesFishEyeLens=0,346,358,922,987,1000,222,0,111,453,889,521,111,0
Input=0
Enabled=1
UseBufferOutput=1
BufferRenderQuality=0
ImageIndex=2
MeshIndex=0

[AppSet5]
App=Blend\BufferBlender
ParamValues=9,7,2,1000,1,2,1000,0,500,500,779,1
ParamValuesPeak Effects\JoyDividers=0,699,507,956,774,500,0,48,350,682,638,650,510
Input=0
Enabled=1
UseBufferOutput=1
BufferRenderQuality=0
ImageIndex=2
MeshIndex=0

[AppSet6]
App=Blend\BufferBlender
ParamValues=2,0,13,1000,1,3,1000,0,500,500,750,0
ParamValuesBlend\VideoAlphaKey=500,1000,0,0,1000,0,0,0,0,0,1000,500,513
ParamValuesPeak Effects\JoyDividers=0,699,507,956,800,500,0,544,350,682,638,650,510
Input=0
Enabled=1
UseBufferOutput=1
BufferRenderQuality=0
ImageIndex=2
MeshIndex=0

[AppSet7]
App=Particles\ReactiveFlow
ParamValues=579,0,0,0,698,1000,500,0,500,0,500,385,500,608,294,0,11,129,125,500,500,500,1000,1000,100
ParamValuesBackground\FourCornerGradient=867,1000,585,1000,469,692,1000,741,685,1000,610,750,1000,1000
ParamValuesPostprocess\Blooming=0,0,0,0,458,709,0,1000,0
Input=0
Enabled=1
UseBufferOutput=1
BufferRenderQuality=0
ImageIndex=0
MeshIndex=0

[AppSet8]
App=Blend\BufferBlender
ParamValues=5,6,5,1000,3,5,1000,2,500,500,875,1
ParamValuesMisc\Automator2=1000,74,152,600,606,20,0,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0
ParamValuesPostprocess\Blooming=0,0,0,1000,500,579,1000,500,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=7
MeshIndex=0

[AppSet9]
App=(none)
ParamValues=
ParamValuesBackground\ItsFullOfStars=0,0,0,0,762,410,500,1000,0,241,0
ParamValuesBlend\BufferBlender=900,222,800,1000,250,0,1000,667,500,500,1000,1000
ParamValuesPeak Effects\WaveSimple=0,0,0,0,548,593,247,99,518
Input=0
Enabled=0
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=1
MeshIndex=0

[AppSet10]
App=(none)
ParamValues=
ParamValuesNew\FrameBlur=432,0,0,132,981,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=9
MeshIndex=0

[AppSet11]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=9
MeshIndex=0

[AppSet12]
App=(none)
ParamValues=
ParamValuesMisc\Automator2=1000,370,303,200,29,515,643,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=9
MeshIndex=0

[AppSet13]
App=(none)
ParamValues=
ParamValuesCanvas effects\DarkSpark=500,0,500,500,0,500,500,500,500,500,1000,589
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=9
MeshIndex=0

[AppSet14]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=9
MeshIndex=0

[AppSet15]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=9
MeshIndex=0

[AppSet16]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=9
MeshIndex=0

[AppSet17]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=9
MeshIndex=0

[AppSet18]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=9
MeshIndex=0

[AppSet19]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=9
MeshIndex=0

[AppSet20]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=9
MeshIndex=0

[AppSet21]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=9
MeshIndex=0

[AppSet22]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=9
MeshIndex=0

[AppSet23]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=9
MeshIndex=0

[AppSet24]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=9
MeshIndex=0

[Video export]
VideoH=480
VideoW=640
VideoRenderFps=30
SampleRate=0
VideoCodec=-1
AudioCodec=-1
AudioCodecFormat=-1
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=0
Uncompressed=0

[UserContent]
Text=
Html=
VideoCues=
Meshes=
MeshAutoScale=0
MeshWithColors=0
Images=[plugpath]Content\Bitmaps\Particles\furball.png,[plugpath]Content\Bitmaps\Particles\flare.png
VideoUseSync=0

[Detached]
Top=394
Left=139
Width=784
Height=459

