FLhd   0  0 FLdt�  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   ո*4  ﻿[General]
GlWindowMode=1
LayerCount=8
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=2,6,5,4,3,0,1,7

[AppSet2]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
ParamValuesPeak Effects\SplinePeaks=940,0,0,0,1000,500,1000,0,0,0
Enabled=1
ImageIndex=1
Name=Section2

[AppSet6]
App=HUD\HUD 3D
FParamValues=0,1,0.6758,0.3379,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.3047,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,0
ParamValues=0,1,675,337,500,500,500,500,500,500,500,500,500,304,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,0
Enabled=1
ImageIndex=3
Name=ForegroundMod

[AppSet5]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
Name=TextFst

[AppSet4]
App=Text\TextTrueType
FParamValues=0,0.8333,0,1,0,0.5,0.5,0,0,0,0.5
ParamValues=0,833,0,1000,0,500,500,0,0,0,500
Enabled=0
Name=Text

[AppSet3]
App=Postprocess\Youlean Drop Shadow
FParamValues=0.5,0.2,0,1,0.02,0.875,0
ParamValues=500,200,0,1000,20,875,0
Enabled=1
UseBufferOutput=1

[AppSet0]
App=Youlean new shaders\Patterns\Raymarched Hexagonal Truchet
FParamValues=0,0,1,0,0.6
ParamValues=0,0,1000,0,600
Enabled=1
Name=Section0

[AppSet1]
App=Postprocess\Vignette
FParamValues=0,0,0,0.6,0.9079,0
ParamValues=0,0,0,600,907,0
ParamValuesPostprocess\Raindrops=404,0
ParamValuesPostprocess\RGB Shift=128,0,0,600,700,200
ParamValuesPostprocess\Youlean Color Correction=226,500,539,500,500,500
Enabled=1
Name=Section1

[AppSet7]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
ImageIndex=2

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Images="@EmbeddedFst:""Name=Handwritten savage.fst"",Data=7801B554CD6EDB300CBE07C83B1439679D24CB3F1AAA02FE891B60C9D6356B7BC87AD062AE11E0D886A274CDB3EDB047DA2B8C527EDA1E036C10129194F891FC4CEACFAFDFF32B68C0A8FAA1DFBBAAEF7553B53FA76D0592F67B13B50593B79BC6CAA0DF2BAF67CE38D595BE6E8D95EF5049D71D2CEC8DB2BA9534FA20F63E9F4D054692211DB27EAFDF9BA75D37034B30044A727C5B7CC3DFD95778B60E5619B5BA53F506D6E842CEC321FEE34E9808704B18172F563C61CE1AB218ADB8020CE27DBC1FCA7E616A6F6143B23B60628880C383EA8C888768DE0DC1085E74A7BB1B5E3D428E1AF5BD86EA48CDB5D14FCA42A1AC927142681CE4A3BCE422A4314149E4392F58CE4511A6194FF32C8A6912394892D124206151BE62879EC84E90385A44C0D9BEFE80A2E41773261605C7A24E6128488608EAEB7F838878FF8CA384957C94173CE3A32224054F91A71275C1F282B18C092EA28026DC731566B8C5297FC5153B91AB4470C755E44A707D729EF0E4C0956B1F1A895DD79DD84DF80910D473F50611F1FE0F577198A7AEC30AFAC252285C4731928E4814E29CE2BCDDE90ADA3378EE704E912AAF8E252509D92BF7920A76506EA0C1712DBBB50CD03453ABAE061C6990870B39BE078B4F6A0512077853E9F6B5C1837FD9A85ADBADF398AAE78FB02D71A661D6A9856E1E1196E041A96B68762099B6C605A031A7388E78E8618F6696788FDB66D1AE3A03EB350E1D5E9A6D3A306B9F9F53B1D0DBB57BA01A0B8DABD33D28726EB5ADE16138571BBB6C0D0A08B2DA5D18DB552D0717DD1966FBD8C8C16081763083C1E5DCA26FAD1B78B878DF5D0EF63421FC6CDB2C5C30CC1E6FBA6A76910BB06AB184CA856D3B1908ECCF09FCB09206F80ADEEBCA2E6594A03806FDB8B4328C639FB1CBD6B4750D063D6FA09A3CD59285113EBE06A0396A193E874765B2591DE57EEF2F7FE680AD","@EmbeddedFst:""Name=Song Position vertical.fst"",Data=78018D54DB6ED340107D8F947F88F21C8277EDF882BA9572A5122D84A6B4426D1F9678DAAC706CCBDE40FB6D3CF049FC0267D67594883EA0386BCF78E6CCCCF1D9FDF3EBF7ED7BCAA9D2D97DB7F33EBB31795AFCBC285252A2DB39D7CF544D8B5D6E95DFED2C962B765E98D42C8BCAAA3730C675496B7BA9AD299408DF252F399FAA942AE50DC440763BDDCEEDB82C57643D94C093BA325BEAA190596B5B54F5DDD7629791CE7BCE3FD11597D295DE5EEB6C473560BCE1280C067CF764CC56123A4BFA214AB03F8CE0198A0051C338E0982061C7088BC74B8405BF261A8D1F1768E081DD203B5CCFE304066658060D12008CBC01AE08FF164F78DE11DE92F4F7DEFCE101C4D477CB22D3CCC44B782491CD28F1C0E73B17710BCA049283425F341E8E3B8CD86320A7DB99E7FA5B46297F903DBFE27FF85D59BD2D5F63B86971180BE63064F69832A65232A1DC0BD3C75EF7C65139381E1D83366D23113821086BEC787000C243B3FB604237DCD1544E7BCBCAFCD09666DA6A15C59E88FCE97C1ACB45309FCE8249309F8DA2D1743C5D044928441C2540F626E370EC05B3E88018F942CCD197393739BDAAB5A4D11AC614825515853ED6464F92C9612D391A24BFF69926470D93D4D2D3DE5F3C6224F1843F56CF77CF82579FA125E3FDA3C9B60D6E825B70EA437D961E3327436FE03B19B6C55B4EDB7B13C7A5A5D312EA4A54F57D2423D1E9F65847D726A5A2474F25F6373873E699125E8CCFEC8C1B2512D91A9794639B2FCA5AF970ADA0AB8C7014906A03A63847D61FF5961436FE2E35C5A1C3E17DDEE9CCD867CEB8D04F1FE879817D4FAB52AF4DFE085896FAC26494372013632B2E20A240C432C04B07BB77CBD8657CC9D7C5B6ACA8AEB14310B4DA9554D5AE3F36B161BED47CB0E596729EF38A9EACEA5F6D4CDDC36537D44BE941EF32DBB37833EC773B67769BA9FE49D943BB8FB9EAF7D7C8A4AADF3FBDE5900C5ABA3F795B9E22D4CD05FCD573BEE66A681F913C4E537A4656AF379472DDA2547E12E0D0A407AB848FE3F3C6A476A3C2188F67641E37568DA246CADC6E55641955C8BCA4F4FC47A6E428C4A95D11E57B6B8233736F9CEFB6FBE76EE72F22229111"
VideoUseSync=0
Filtering=0

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

[Wizard]
Section0Cb=Raymarched Hexagonal Truchet
Section1Cb=Vignette
Section2Cb=Song Position vertical
Section3Cb=Handwritten savage
Macro0=Song Title
Macro1=Song Author
Macro2=Song made with love and time

