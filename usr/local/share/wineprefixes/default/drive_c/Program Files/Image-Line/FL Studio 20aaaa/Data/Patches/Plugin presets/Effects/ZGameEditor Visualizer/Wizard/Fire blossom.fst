FLhd   0  0 FLdtO  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   ��!�  ﻿[General]
GlWindowMode=1
LayerCount=14
FPS=1
MidiPort=-1
Aspect=1
LayerOrder=0,1,2,3,4,5,6,7,8,10,11,9,13,12

[AppSet0]
App=Canvas effects\FreqRing
ParamValues=500,500,500,500,354,500,500,500,0,312,123,500,828,500
Enabled=1
Collapsed=1
ImageIndex=6

[AppSet1]
App=Feedback\FeedMe
ParamValues=0,0,0,1000,752,864,0
Enabled=1
Collapsed=1

[AppSet2]
App=Feedback\WormHoleEclipse
ParamValues=0,0,0,504,384,596,672,684,748,500,500
Enabled=1
Collapsed=1
ImageIndex=6

[AppSet3]
App=Postprocess\ColorCyclePalette
ParamValues=1,4,12,2,3,1000,92,500,136,2,0
Enabled=1
UseBufferOutput=1
Collapsed=1
ImageIndex=6

[AppSet4]
App=Canvas effects\FreqRing
ParamValues=500,500,500,500,722,500,500,500,0,648,183,492,568,932
Enabled=1
UseBufferOutput=1
Collapsed=1
ImageIndex=6

[AppSet5]
App=Blend\BufferBlender
ParamValues=0,1,1,1000,1,0,1000,0,500,500,750,0
Enabled=1
Collapsed=1

[AppSet6]
App=Feedback\WormHoleEclipse
ParamValues=0,0,0,593,949,116,481,793,755,624,500
Enabled=1
UseBufferOutput=1
Collapsed=1
ImageIndex=6

[AppSet7]
App=Peak Effects\Polar
ParamValues=0,212,752,0,692,500,500,1000,1000,0,540,820,468,500,0,500,0,1000,1000,0,0,0,0,1
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet8]
App=Blend\BufferBlender
ParamValues=0,1,2,456,1,1,1000,0,500,500,750,0
Enabled=1
UseBufferOutput=1
Collapsed=1
ImageIndex=2

[AppSet10]
App=Canvas effects\StarTaser2
ParamValues=500,500,500,500,500,500,500,796,500,500,500,500,0
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet11]
App=Blend\BufferBlender
ParamValues=0,1,2,1000,2,1,1000,1,500,500,750,0
Enabled=1
Collapsed=1
ImageIndex=4

[AppSet9]
App=Text\TextTrueType
ParamValues=460,0,0,1000,0,494,498,0,0,0,500
Enabled=1

[AppSet13]
App=Text\TextTrueType
ParamValues=0,0,0,0,0,494,500,0,0,0,500
Enabled=1

[AppSet12]
App=Misc\Automator
ParamValues=1,11,8,5,728,204,472,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1
Collapsed=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0

[UserContent]
Text="Soft tantalizing petals                             Of the perfect flower                               Giving peace and calmness                           Over life's ongoing agony                           Hope brings a mind's eye                            Visions of love's heart                             Talks of simple past                                Greater than perfection                             Living an unpredictable life                        Not knowing where you'll be                         Showing unpredictable feelings                      Hoping that you'll soon see                         The moon's penetrating rays                         Lighting the cool night air                         Filling the heart's emptiness                       With something that cares                           Love is a needful thing                             Holding fast and true                               Forever will it sing                                A tune greater than blue...                         Blue Flowers","Russell Sivey",Blue,Flowers,
Html="<position x=""3""><position y=""5""><p align=""left""><font face=""American-Captain"" size=""10"" color=""#FFFFFF"">[author]</font></p></position>","<position x=""3""><position y=""13""><p align=""left""><font face=""Chosence-Bold"" size=""6"" color=""#FFFFFF"">[title]</font></p></position>","<position x=""55""><position y=""90""><p align=""right""><font face=""Chosence-Bold"" size=""2"" color=""#FFFFFF"">[extra1]</font></p></position>","<position x=""67""><position y=""94""><p align=""right""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[comment]</font></p></position>",,"<position x=""2""><position y=""92""><p align=""center""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[extra2]</font></p></position>","<position x=""2""><position y=""95""><p align=""center""><font face=""Chosence-regular"" size=""2"" color=""#FFFFF"">[extra3]</font></p></position>"
VideoUseSync=0
EnableMipmap=1

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

