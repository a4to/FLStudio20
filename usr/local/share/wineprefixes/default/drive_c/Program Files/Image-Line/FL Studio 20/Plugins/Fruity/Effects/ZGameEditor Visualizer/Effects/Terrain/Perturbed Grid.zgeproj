<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" FileVersion="2">
  <OnLoaded>
    <ZLibrary Comment="HSV Library">
      <Source>
<![CDATA[vec3 hsv(float h, float s, float v)
{
  s = clamp(s/100, 0, 1);
  v = clamp(v/100, 0, 1);

  if(!s)return vector3(v, v, v);

  h = h < 0 ? frac(1-abs(frac(h/360)))*6 : frac(h/360)*6;

  float c, f, p, q, t;

  c = floor(h);
  f = h-c;

  p = v*(1-s);
  q = v*(1-s*f);
  t = v*(1-s*(1-f));

  switch(c)
  {
    case 0: return vector3(v, t, p);
    case 1: return vector3(q, v, p);
    case 2: return vector3(p, v, t);
    case 3: return vector3(p, q, v);
    case 4: return vector3(t, p, v);
    case 5: return vector3(v, p, q);
  }
}]]>
      </Source>
    </ZLibrary>
  </OnLoaded>
  <OnUpdate>
    <ZExpression>
      <Expression>
<![CDATA[uResolution=vector2(app.ViewportWidth,app.ViewportHeight);
uViewport=vector2(app.ViewportX,app.ViewportY);
uAlpha =1.0-Parameters[0];

float speed=(Parameters[4]-0.5)*4.0;
float delta=app.DeltaTime*Speed; //comment to use other time options
uTime+=delta;

vec3 col = hsv(Parameters[1]*360,Parameters[2]*100,(1-Parameters[3])*100);
uColor = vector3(col[0],col[1],col[2]);]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <OnRender>
    <UseMaterial Material="mCanvas"/>
    <RenderSprite/>
  </OnRender>
  <Content>
    <Shader Name="MainShader">
      <VertexShaderSource>
<![CDATA[#version 120

void main(){
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[#version 120

uniform vec2 iResolution,iViewport;
uniform float iTime;

uniform float iAlpha;
uniform vec3 iColor;
/*
float Truncate(float val) { return clamp(val,0.0,1.0); }

vec3 TransformHSV(vec3 c, float H, float S, float V) {
  float M_PI = 3.1415926;
	float VSU = V * S * cos(H * M_PI / 180.0);
	float VSW = V * S * sin(H * M_PI / 180.0);
  vec3 ret;
	ret.r = Truncate((0.299 * V + 0.701 * VSU + 0.168 * VSW) * c.r
		+ (0.587 * V - 0.587*VSU + 0.330 * VSW) * c.g
		+ (0.114 * V - 0.114*VSU - 0.497*VSW) * c.b);
	ret.g = Truncate((0.299 * V - .299 * VSU - 0.328 * VSW) * c.r
		+ (0.587 * V + 0.413 * VSU + 0.035 * VSW) * c.g
		+ (0.114 * V - 0.114 * VSU + 0.292 * VSW) * c.b);
	ret.b = Truncate((0.299 * V - 0.3 * VSU + 1.25 * VSW) * c.r
		+ (0.587 * V - 0.588 * VSU - 1.05 * VSW) * c.g
		+ (0.114 * V + 0.886 * VSU - 0.203 * VSW) * c.b);
	return ret;
} // col = TransformHSV(clamp(col, 0., 1.), iColorHSV[0], iColorHSV[1], iColorHSV[2]);
*/

/*

    Polygon Terrain Grid
    --------------------

	I made this a while back when constructing my "Terrain Lattice" example. I thought the
    rendering style was interesting, but overall, found it a little bland. Anyway, I
	tweaked it a little and put in some options... and it's still a little boring, but I
	figured I'd put it up anyway. :) As you can see, it's a standard triangulated perturbed
    grid, or to be more specific, a raymarched heightmap subdivided into grid squares,
    which are each subdivided into two triangles to produce a flat shaded look.

    Normally, when you see one of these, the square grid cells are subdivided into triangles
    via a diagonal partitioning with fixed orientation. However, it's possible to partition
	the cells in all kinds of ways, like patterns, randomness and slope -- Playing around
    with the "SHOW_DIAGONAL" define will illustrate that. The default is a partitioning
	based on the slope -- Basically, it favors a partitioning that cuts the higher slopes
	in half. I believe it gives the underlying height map just a touch more definition.

    There are two ways to render flat grid squares. One is to linearly interpolate between
	the height values of all four vertices to produce a quad that looks flat on account of
    its straight edge joins. The other is to split the quad into two triangles and linearly
	interpolate between the three verticies of each of those. I tried both methods, but
	liked the look of the genuinely flat-planed triangles more. For the smooth quad look,
	uncomment the "SMOOTH_QUAD" define. By the way, there are a heap of compiler directives
	in there for anyone who wants to play around with different looks before making one
	of these.

    A flat shaded triangle render usually requires a barycentric approach, but since the
	grid triangles are essentially half squares, it's possible to use, vector
	perpendicularity, symmetry, etc, to cut down on the calculations considerably.


	Other examples:

	// Simple, and really nicely lit.
    Triangulator - nimitz
	https://www.shadertoy.com/view/lllGRr

	// Nice example that takes an intuitive vectorized approach.
	Ray Marched Mesh Terrain - Flyguy
	https://www.shadertoy.com/view/ltjSRD

	Terrain Lattice - Shane
	https://www.shadertoy.com/view/XslyRH

*/



// OPTIONAL COMPILER DIRECTIVES

// The point of the exercise was to polygonize the terrain and give it a flat shaded triangulated
// appearance, but if you'd prefer to see smooth quads, just uncomment the following:
//#define SMOOTH_QUAD

// Animate the surface: The default setting, "DIAGONAL_MODE 4," will flip diagonals according to
// changine slope, so this would look better with a fixed diagonal orientation, like
// "DIAGONAL_MODE 3" for instance.
//#define ANIMATE_SURFACE

// Turn the blinking squares on or off.
#define BLINKING_LIGHTS

// Diagonal orientation (0 - 4) -- Not applicable when the SMOOTH_QUAD directive is on.
//
// The orientation of the diagonal can effect the look of the surface. The default is a diagonal
// orientation based on the slope. The idea being that you favor cutting the largest quad slope
// in half, which means running a diagonal line between diagonal points with the smallest slope.
// It's a matter of opinion, but I think it enhances the terrain shape a little more. In order to
// more clearly see the joins, set the SHOW_DIAGONAL directive to something other than zero.
//
// Left: 0, Right: 1, Random: 2, Pattern: 3, Slope: 4.
#define DIAGONAL_MODE 4

// Horizontal, vertical and diagonal lines. I like it with the horizonal lines only, but wanted
// to show the square grid by default to emulate a lot of the online imagery I see.
#define SHOW_HORIZONTAL
#define SHOW_VERTICAL
// I find the diagonal joins too busy, so have left it off by default. Also, it's not
// applicable when the SMOOTH_QUAD directive is on.
// No lines: 0, Metalic: 1, Timber Grooves: 2.
#define SHOW_DIAGONAL 0

// Ball joins. The grid looks cleaner without the ball bearing joins, but less interesting.
#define SHOW_BALL_JOINS


/////////


// Max ray distance.
#define FAR 40.

// Scene object ID container to separate the ball joint mesh object from the terrain.
vec3 vObjID; // Terrain: 0, Join: 1, Ball, 2.

// 2x2 matrix rotation. Note the absence of "cos." It's there, but in disguise, and comes courtesy
// of Fabrice Neyret's "ouside the box" thinking. :)
mat2 r2(float th){ vec2 a = sin(vec2(1.5707963, 0) + th); return mat2(a, -a.y, a.x); }


// vec3 to float hash.
float hash21(vec2 p){

    float n = dot(p, vec2(13.163, 157.247));
    return fract(sin(n)*43758.5453);
}


/*
// A cheap orthonormal basis vector function - Taken from Nimitz's "Cheap Orthonormal Basis" example, then
// modified slightly.
//
//Cheap orthonormal basis by nimitz
//http://orbit.dtu.dk/fedora/objects/orbit:113874/datastreams/file_75b66578-222e-4c7d-abdf-f7e255100209/content
//via: http://psgraphics.blogspot.pt/2014/11/making-orthonormal-basis-from-unit.html
mat3 basis(in vec3 n){

    float a = 1./(1. + n.z);
    float b = -n.x*n.y*a;
    return mat3(1. - n.x*n.x*a, b, n.x, b, 1. - n.y*n.y*a, n.y, -n.x, -n.y, n.z);

}


// A line segment formula that orients via an orthanormal basis. It'd be faster to use
// IQ's 3D line segment formula, but this one allows for more interesting cross sections,
// like hexagons and so forth.
float sdCapsule( vec3 p, vec3 a, vec3 b, float r, float lf){ // Length factor on the end.


    b -= a;
    float l = length(b);

    p = basis(normalize(b))*(p - a - b*.5);

    p = abs(p);
    //p.x = abs(p.x - .03);
    //return max(length(p.xy) - r, p.z - l*lf);
    //return max((p.x + p.y)*.7071 - r, p.z - l*lf);
    //return max(max(p.x, p.y) - r, p.z - l*lf);
    //return max(max(max(p.x, p.y), (p.y + p.x)*.7071) - r, p.z - l*lf);
    return max(max(p.x*.866025 + p.y*.5, p.y) - r, p.z - l*lf);
}
*/


// IQ's 3D line segment formula. Simpler and cheaper, but doesn't orient carved cross-sections.
float sdCapsule(vec3 p, vec3 a, vec3 b){

    vec3 pa = p - a, ba = b - a;
    float h = clamp( dot(pa,ba)/dot(ba,ba), 0.0, 1.0 );
    pa = abs(pa - ba*h);
    return length( pa );
}



// Height map values. Just a couple of animated sinusoidal layers, but you could put anything
// here... so long as it's cheap. :)
float hm(in vec2 p){

    #ifdef ANIMATE_SURFACE
    // Scaling, plus some movement.
    p = p/4. + iTime/8.;
    #else
    // Scaling only.
    p /= 4.;
    #endif

    // Layer one.
    float n = dot(sin(p.xy*3.14159 - cos(p.yx*3.14159)*3.14159/2.), vec2(0.25) + .5)*.66;
    p = p*1.5;  // Increase frequency.

    p.xy = mat2(.866025, .5, -.5, .866025)*p.xy; // Rotate.

    // Add another layer.
    n += dot(sin(p.xy*3.14159 - cos(p.yx*3.14159)*3.14159/2.), vec2(0.25) + .5)*.34;

    return n; // Range [0, 1]... hopefully. :)

}

// Used to scale the grid without having to move the camera.
#define scale 1.5
vec3 hVal; // Global variable to hold the three height values for reuse.

float diag;

// The terrain - tesselated in a flat-grid triangle-pair fashion... Needs rewording. :D
float triTerrain(vec2 p){



    vec2 ip = floor(p); // Integer value. Used for the unique corner height values.
    p -= ip; // Fractional grid value.

    // The height values -- One for each grid vertex. If it were not for the slope
    // comparisons, only three values would be required. It's a cheap distance field,
    // so it shouldn't really matter anyway.
    float h00 = hm(ip);
    float h01 = hm(ip + vec2(0, 1));
    float h10 = hm(ip + vec2(1, 0));
    float h11 = hm(ip + 1.);

    #ifdef SMOOTH_QUAD
    	// Smoothing: Mainly here for reference. To use it with joins, you need to smoothen
        // the line joins themselves, which doesn't look that great.
    	//p *= p*(3. - 2.*p);
    	//p *= p*p*(p*(p*6. - 15.) + 10.);
    	diag = 0.;
    #else
        #if DIAGONAL_MODE == 0 // Left diagonal.
        diag = 0.;
        #elif DIAGONAL_MODE == 1 // Right diagonal.
        diag = 1.;
        #elif DIAGONAL_MODE == 2 // Random diagonal orientation.
        diag = hash21(ip)<.5? 0. : 1.;
        #elif DIAGONAL_MODE == 3 // Pattern diagonal
        diag = mod(ip.x + ip.y, 2.)>.5? 0. : 1.;
        #else
        float l1 = abs(h00 - h11);
        float l2 = abs(h01 - h10);
        diag = l1>l2? 0. : 1.;
    #endif

    #endif

    // The barycentric coordinates, so to speak, and the corresponding height value.
    // For those of you familiar with the process, you may note that there are far
    // fewer operations than usual.
    float s;

    if(diag<.5){
        s = step(1., p.x + p.y); // Determines which side of the diagonal we're on.

        // Storing the heights at the three triangle vertices. Normally, it wouldn't be
        // necessary, but we're reusing them to render the mesh.
        //hVal = vec3(hm(ip + s), hm(ip + vec2(1, 0)), hm(ip + vec2(0, 1)));
        float hs = s<.5? h00 : h11;
        hVal = vec3(hs, h10, h01);
    }
    else {
        // Triangulating across the other diagonal. Handy, if you want to make patterns.
        s = step(p.x, p.y);
        //hVal = vec3(hm(ip), hm(ip + vec2(1. - s, s)), hm(ip + 1.));
        float hs = s<.5? h10 : h01;
        hVal = vec3(h00, hs, h11);
    }


    #ifdef SMOOTH_QUAD
        // A simple, interpolated quad. It's not really flat, but the edge-joins are straight,
        // so it looks that way. Because the mesh is set up on triangle logic, there two
        // extra height values. Normally, you'd only need one extra.
        return mix(mix(hm(ip), hVal.y, p.x), mix(hVal.z, hm(ip+1.), p.x), p.y);
    #else



    // Barycentric setup: This is a very trimmed down version of the generalized barycentric
    // calculations that involve cross-products, and so forth. Without going into detail, I'm
    // sure you could imagine that three points in space can be used to generate a plane
    // equation via cross products and such, and the fractional grid points could be used in
    // unison with the vertice coordinates to determine the exact coordinate on the plane, or
    // the height value at that coordinate.
    //
    // Anyway, the grid triangles are shaped in such a way that a lot of the operations cancel
    // out, and the lines below are the result. You could just use them. However, if you require
    // more information, look up a few barycentric coordinate examples.
    //
    if(diag<.5){
        vec3 b = abs(vec3(1.0 - p.x - p.y, p.x - (p.x - p.y + 1.)*s, p.y - (p.y - p.x + 1.)*s));

        // The linearly interpolated triangle height.
        return dot(b, hVal);
    }
    else {
        //return mix(mix(hVal.x, hm(ip+vec2(1, 0)), f.x), mix(hm(ip+vec2(0, 1)), hVal.z, f.x),f.y);
        vec3 b = abs(vec3(1. - (1. - s)*p.x - p.y*s, (1. - 2.*s)*(p.x - p.y), p.x*s + p.y*(1. - s)));
        return dot(b, hVal);
    }

    #endif




}

float tObjID;

// The flat shaded terrain and the mesh.
float map(vec3 p){

    // The terrain. By the way, when you scale coordinates, you have to scale back
    // the distance value to keep things in check. I often forget this.
    float ter = triTerrain(p.xz*scale)/scale; // The terrain.

    const float hPert = .4; // Terrain height perturbation.
    float fl = p.y  + (.5 - ter)*hPert;//*.25; // Adding it to a flat plane.


    hVal = (hVal - .5*scale)*hPert + .04;///scale; // Rescaling the height values to match the terrain perturbation.

    // The grid cell boundaries. As usual, the code looks more complicated than it is, but it's
    // just grid vextex to grid vertex tubes.
    vec3 q = p*scale;
    q.xz = fract(q.xz); // Break space into squares along the XZ plane.

    // Grid line thickness.
    const float lw = .04;

    float ln = 1e5;
    float ln2 = 1e5;

      if(diag<.5){

        #ifdef SHOW_HORIZONTAL
        ln = min(ln, sdCapsule(q, vec3(0, hVal.x, 0), vec3(1, hVal.y, 0)));
        ln = min(ln, sdCapsule(q, vec3(0, hVal.z, 1), vec3(1, hVal.x, 1)));
        #endif
        #ifdef SHOW_VERTICAL
        ln =  min(ln, sdCapsule(q, vec3(0, hVal.x, 0), vec3(0, hVal.z, 1)));
        ln = min(ln, sdCapsule(q, vec3(1, hVal.y, 0), vec3(1, hVal.x, 1)));
        #endif
        ln -= lw; // Line thickness.
        #if SHOW_DIAGONAL > 0
        // Diagonal.
        ln2 = min(ln2, sdCapsule(q, vec3(0, hVal.z, 1), vec3(1, hVal.y, 0)) - lw);
        #endif

    }
    else {
        #ifdef SHOW_HORIZONTAL
        ln = min(ln, sdCapsule(q, vec3(0, hVal.x, 0), vec3(1, hVal.y, 0)));
        ln = min(ln, sdCapsule(q, vec3(0, hVal.y, 1), vec3(1, hVal.z , 1)));
        #endif
        #ifdef SHOW_VERTICAL
    	ln = min(ln, sdCapsule(q, vec3(0, hVal.x, 0), vec3(0, hVal.y, 1)));
        ln = min(ln, sdCapsule(q, vec3(1, hVal.y, 0), vec3(1, hVal.z, 1)));
      	#endif
        ln -= lw; // Line thickness.
        #if SHOW_DIAGONAL > 0
        // Diagonal.
        ln2 = min(ln2, sdCapsule(q, vec3(0, hVal.x, 0), vec3(1, hVal.z, 1)) - lw);
        #endif
    }



    #ifdef SHOW_BALL_JOINS
    // Ball joins. We've calculated another height value offset by half the grid in order to
    // draw just one each - instead of four. It's a little hard to explain why but it has to
    // do with repetitive cell boundaries.
    float hgt = (hm(floor(p.xz*scale + .5)) - .5*scale)*hPert + .04;
    vec2 offXZ = fract(p.xz*scale + .5) - .5;
    // The metallic balls.
    float sp = length(vec3(offXZ.x, abs(q.y - hgt), offXZ.y)) - .09; // Ball join size.
    #else
    float sp = 1e5;
    #endif

    // Scaling the variables back.
    ln /= scale;
    ln2 /= scale;
    sp /= scale;


    #ifndef SMOOTH_QUAD
    #if SHOW_DIAGONAL == 1
    ln = min(ln, ln2); // Thinner.
    //ln = min(ln, ln2 + .01); // Thinner.
    #elif SHOW_DIAGONAL == 2
    fl = max(fl, -ln2 + .004); // Innner grooves.
    //fl = min(fl, ln2 + .01); // Outer join.
    #endif
    #endif


    // Save the individual object IDs: With larger object numbers, Identifying inside the raymarching
    // equation can add a lot of extra calls, so it's best to store them, then identify them outside.
    // It's possible to do the vec2(dist, objID), but that can complicate things also.
    vObjID = vec3(fl, ln, sp); // terrain, joiner lines, ball joints.


    // Combining the mesh with the terrain.
    return min(min(fl, ln), sp)*.75; //smin(fl, ln, .025);

}



// Standard raymarching routine.
float trace(vec3 ro, vec3 rd){

    float t = 0.; //fract(sin(dot(rd, vec3(57, 111, 27)))*45758.5453)*.1; // Jitter.

    for (int i=0; i<96; i++){

        float d = map(ro + rd*t);

        if(abs(d)<.001*(t*.125 + 1.) || t>FAR) break;

        t += d;  // Using more accuracy, in the first pass.
    }

    return min(t, FAR);
}


/*
// Tetrahedral normal - courtesy of IQ. I'm in saving mode, so the two "map" calls saved make
// a difference. Also because of the random nature of the scene, the tetrahedral normal has the
// same aesthetic effect as the regular - but more expensive - one, so it's an easy decision.
vec3 getNormal(in vec3 p, float t)
{
    vec2 e = vec2(-1., 1.)*0.001*min(1. + t, 5.);
	return normalize(e.yxx*map(p + e.yxx) + e.xxy*map(p + e.xxy) +
					 e.xyx*map(p + e.xyx) + e.yyy*map(p + e.yyy) );
}
*/

// Standard normal function. It's not as fast as the tetrahedral calculation, but more symmetrical.
vec3 getNormal(in vec3 p, float t) {
	const vec2 e = vec2(.002, 0); //vec2(0.002*min(1. + t*.5, 2.), 0);
	return normalize(vec3(map(p + e.xyy) - map(p - e.xyy), map(p + e.yxy) - map(p - e.yxy),	map(p + e.yyx) - map(p - e.yyx)));
}



// Basic soft shadows.
float getShad(in vec3 ro, in vec3 n, in vec3 lp){

    const float eps = .001;

	float t = 0., sh = 1., dt;

    ro += n*eps*1.1;

    vec3 ld = (lp - ro);
    float lDist = length(ld);
    ld /= lDist;

    //t += hash31(ro + ld)*.005;

	for(int i=0; i<16; i++){

    	dt = map(ro + ld*t);

        sh = min(sh, 12.*dt/t);

 		t += clamp(dt, .01, .5);
        if(dt<0. || t>lDist){ break; }
	}

    return min(max(sh, 0.) + 0.2, 1.0);
}




// I keep a collection of occlusion routines... OK, that sounded really nerdy. :)
// Anyway, I like this one. I'm assuming it's based on IQ's original.
float cAO(in vec3 pos, in vec3 nor)
{
	float sca = 2.0, occ = 0.0;
    for( int i=0; i<5; i++ ){

        float hr = 0.01 + float(i)*0.5/4.0;
        float dd = map(nor * hr + pos);
        occ += (hr - dd)*sca;
        sca *= 0.7;
    }
    return clamp( 1.0 - occ, 0.0, 1.0 );
}



/*
// Tri-Planar blending function. Based on an old Nvidia writeup:
// GPU Gems 3 - Ryan Geiss: http://http.developer.nvidia.com/GPUGems3/gpugems3_ch01.html
vec3 tex3D(sampler2D channel, vec3 p, vec3 n){

    n = max(abs(n) - .2, 0.001);
    n /= dot(n, vec3(1));
	vec3 tx = texture(channel, p.zy).xyz;
    vec3 ty = texture(channel, p.xz).xyz;
    vec3 tz = texture(channel, p.xy).xyz;

    // Textures are stored in sRGB (I think), so you have to convert them to linear space
    // (squaring is a rough approximation) prior to working with them... or something like that. :)
    // Once the final color value is gamma corrected, you should see correct looking colors.
    return tx*tx*n.x + ty*ty*n.y + tz*tz*n.z;
}
*/
// Texture bump mapping. Four tri-planar lookups, or 12 texture lookups in total. I tried to
// make it as concise as possible. Whether that translates to speed, or not, I couldn't say.
vec3 texBump( sampler2D tx, in vec3 p, in vec3 n, float bf){


    const vec2 e = vec2(0.001, 0);

    // Three gradient vectors rolled into a matrix, constructed with offset greyscale texture values.
    mat3 m = mat3(iColor, iColor, iColor);

    vec3 g = vec3(0.299, 0.587, 0.114)*m; // Converting to greyscale.
    g = (g - dot(iColor, vec3(0.299, 0.587, 0.114)) )/e.x; g -= n*dot(n, g);

    return normalize( n + g*bf ); // Bumped normal. "bf" - bump factor.

}



// Compact, self-contained version of IQ's 3D value noise function. I have a transparent noise
// example that explains it, if you require it.
float n3D(in vec3 p){

	const vec3 s = vec3(57, 113, 27);
	vec3 ip = floor(p); p -= ip;
    vec4 h = vec4(0., s.yz, s.y + s.z) + dot(ip, s);
    p = p*p*(3. - 2.*p); //p *= p*p*(p*(p * 6. - 15.) + 10.);
    h = mix(fract(sin(h)*43758.5453), fract(sin(h + s.x)*43758.5453), p.x);
    h.xy = mix(h.xz, h.yw, p.y);
    return mix(h.x, h.y, p.z); // Range: [0, 1].
}



// Very basic pseudo environment mapping... and by that, I mean it's fake. :) However, it
// does give the impression that the surface is reflecting the surrounds in some way.
//
// More sophisticated environment mapping:
// UI easy to integrate - XT95
// https://www.shadertoy.com/view/ldKSDm
vec3 envMap(vec3 p){

    p *= 2.;
    p.xz += iTime*.5;

    float n3D2 = n3D(p*2.);

    // A bit of fBm.
    float c = n3D(p)*.57 + n3D2*.28 + n3D(p*4.)*.15;
    c = smoothstep(0.5, 1., c); // Putting in some dark space.

    p = vec3(c*.8, c*.9, c);//vec3(c*c, c*sqrt(c), c); // Bluish tinge.

    return mix(p.zxy, p, n3D2*.34 + .665); // Mixing in a bit of purple.

}

// Simple sinusoidal path, based on the z-distance.
vec2 path(in float z){ float s = sin(z/36.)*cos(z/18.); return vec2(s*16., 0.); }

// Recreating part of the distance function to obtain the segment IDs, which in turn is used
// to create the blink effect.
float lightBlink(vec2 p){

    // Unique identifier for the grid cell.
    float rnd = hash21(floor(p));

    // Blink at random.
    return smoothstep(.9, 1., sin(rnd*6.283 + iTime*2.)*.5 + .5);
}

void mainImage( out vec4 fragColor, in vec2 fragCoord ){


    // Screen coordinates.
	vec2 uv = (fragCoord - iResolution.xy*.5)/iResolution.y;//min(iResolution.y, 800.);

	// Camera Setup.
	vec3 lk = vec3(0, 0, iTime*1.5);  // "Look At" position.
	vec3 ro = lk + vec3(0, 2.5, -2); // Camera position, doubling as the ray origin.

    // Light positioning. One is just in front of the camera, and the other is in front of that.
 	vec3 lp = ro + vec3(0, 0, 3);// Put it a bit in front of the camera.

	// Sending the camera, "look at," and two light vectors across the plain. The "path" function is
	// synchronized with the distance function.
	lk.xy += path(lk.z);
	ro.xy += path(ro.z);
	lp.xy += path(lp.z);

    // Using the above to produce the unit ray-direction vector.
    float FOV = 3.14159/3.; // FOV - Field of view.
    vec3 fwd = normalize(lk - ro);
    vec3 rgt = normalize(vec3(fwd.z, 0, -fwd.x));
    // "right" and "forward" are perpendicular, due to the dot product being zero. Therefore, I'm
    // assuming no normalization is necessary? The only reason I ask is that lots of people do
    // normalize, so perhaps I'm overlooking something?
    vec3 up = cross(fwd, rgt);

    // rd - Ray direction.
    vec3 rd = normalize(fwd + (uv.x*rgt + uv.y*up)*FOV);

    // Swiveling the camera about the XY-plane (from left to right) when turning corners.
    // Naturally, it's synchronized with the path in some kind of way.
	rd.xy *= r2( path(lk.z).x/128.);

    // Raymarch to the scene.
    float t = trace(ro, rd);

    //float svObjID = vObjID.x<vObjID.y && vObjID.x<vObjID.z? 0. : vObjID.y<vObjID.z? 1. : 2.;
    float svObjID = vObjID.x < vObjID.y ? (vObjID.x < vObjID.z ? 0. : 2.) : (vObjID.y < vObjID.z ? 1. : 2.);

    // Initiate the scene color to black.
	vec3 sceneCol = vec3(0);

	// The ray has effectively hit the surface, so light it up.
	if(t < FAR){


        // Texture scale factor.
        float tSize0 = 1.;

    	// Surface position and surface normal.
	    vec3 sp = ro + rd*t;
	    //vec3 sn = getNormal(sp, edge, crv, ef, t);
        vec3 sn = getNormal(sp, t);


        // Texture-based bump mapping. I've left it out for this.
        //if(svObjID>.5) sn = texBump(iChannel0, sp*tSize0, sn, .005);


    	// Light direction vector.
	    vec3 ld = lp - sp;

        // Distance from respective light to the surface point.
	    float lDist = max(length(ld), .001);

    	// Normalize the light direction vector.
	    ld /= lDist;


        // Shadows and ambient self shadowing.
    	float sh = getShad(sp, sn, lp);
    	float ao = cAO(sp, sn); // Ambient occlusion.

	    // Light attenuation, based on the distances above.
	    float atten = 1.2/(1. + lDist*lDist*0.05);


    	// Diffuse lighting.
	    float diff = max( dot(sn, ld), 0.0);
        diff = pow(diff, 4.)*1.5; // Ramping up the diffuse.

    	// Specular lighting.
	    float spec = pow(max( dot( reflect(-ld, sn), -rd ), 0.0 ), 8.);

	    // Fresnel term. Good for giving a surface a bit of a reflective glow.
        float fre = pow( clamp(dot(sn, rd) + 1., .0, 1.), 4.);


        // I got a reminder looking at XT95's "UI" shader that there are cheaper ways
        // to produce a hint of reflectivity than an actual reflective pass. :)
        vec3 ref = reflect(rd, sn);
        vec3 env = envMap(ref);
        vec3 specCol = vec3(1, .95, .8);

        // Obtaining the texel color.
	    vec3 texCol;


        if(svObjID<.5) { // Terrain texturing.

            // Tri-planar texturing - A little better than the 2D method below.
            texCol = iColor;//tex3D(iChannel0, sp*tSize0, sn);
            //texCol = mix(texCol, tex3D(iChannel0, sp*tSize0*2. + .5, sn), .25);
            // Just 2D XZ texturing. It works, but lacks a touch of depth.
            //texCol = texture(iChannel0, sp.xz*tSize0).xyz; texCol *= texCol;
            // Ramping up the saturation (hue intensity) a bit.
            texCol = smoothstep(0., .7, texCol);

            /*
            // A quick attempt to emulate the timber texture. Some of the nice
            // patterns and hues are missing, so it needs more work, but it
            // could definitely be done.
            vec3 tsp = sp*vec3(1, 1, 6);
            float ns = n3D(tsp*6.)*.57 + n3D(tsp*12.)*.28 + n3D(tsp*24.)*.15;
            float ns2 = n3D(tsp*6.)*.66 + n3D(tsp*12.)*.34;
            ns2 = mix(ns2, smoothstep(0., 1., ns2), .5);
            float ns3 = clamp(sin(ns*6.2831) + .5, 0., 1.);
            texCol = mix(vec3(1.3, .175, .0)/1.3, vec3(0), 1. - ns);
            texCol = mix(texCol, vec3(.5, .05, .02), mix(ns, ns3, .5));
            tsp = (sp + .5)*vec3(1, 1, 4);
            float ns4 = n3D(tsp*6.)*.66 + n3D(tsp*18.)*.34;
            texCol *= mix(ns4, smoothstep(0., .25, ns4 - .35), .5)*.7 + .65;
            texCol *= n3D(sp*128.)*.8 + .6;//*.4 + .8;
            //texCol = max(texCol, 0.);
            */


            //texCol *= max(triTerrain(sp.xz*scale)*.25 + .75, 0.); // Fake terrain shadowing.
            //texCol = mix(texCol.xzy, texCol, max(triTerrain(sp.xz*scale)*.3 + .7, 0.)); // Hue shadowing.


            #ifdef BLINKING_LIGHTS
            texCol = mix(texCol, mix(texCol, texCol.yyz, .35)*3.5, lightBlink(sp.xz*scale));
            #endif

            // Lowering the terrain settings a bit.

            //specCol *= .5;
            // Add some very subtle, almost imperceptible noise to the specular color. I wanted the
            // surface to have a laminated sheen, but not completely smooth. I guess you could very
            // subtly bump the surface, but this is much easier and more suited to the example.
            specCol *= (n3D(sp*96.)*.66 + n3D(sp*192.)*.34)*.3 + .35;

            // Toning down the fake environment lighting on the terrain.
            env *= texCol/1.5;

            // Subtle reflective hint of cherry.
            texCol = mix(texCol, texCol.xzy, dot(ref, vec3(.15)));

        }
        else { // The chrome lattice.

            // Tri-planar texturing
            texCol = iColor;//tex3D(iChannel0, sp*tSize0*2., sn);
            // Taking out a lot of the red and darkening. Shiny chrome looking stuff tends
            // to require less diffuse but higher reflectivity.
            texCol = mix(texCol, texCol.xxx, .75)*.35;

            // Add some golden reflectivity to the ball joins.
            if(svObjID==2.) env *= vec3(2.2, 1.7, .5);

            // Alternative gold lattice -- A bit gaudy. :)
            //texCol *= vec3(8, 6, 4)/4.;
            //env *= vec3(8, 6, 2)/4.;


        }


        // Combining the above terms to procude the final color.
        sceneCol += (texCol*(diff +  ao*.3 + vec3(1, .9, .7)*fre) + env*1.5 + specCol*spec*2.);


        // Shading. The shadows have minimal effect in this example, but I've cut down on
        // shadow iterations and left them in.
        sceneCol *= ao*atten*sh;



	}

    // Simple dark fog. It's almost black, but I left a speck of color in there to account for
    // the reflective glow... Although, it still doesn't explain where it's coming from. :)
    vec3 bg = mix(vec3(1, .5, .6), vec3(.1, .05, .025), clamp(rd.y + .75, 0., 1.));
    bg = mix(bg, bg.xzy, rd.y*.5 + .5);
    sceneCol = mix(sceneCol, bg, smoothstep(0., .95, t/FAR));


    // Rought gamma correction.
	fragColor = vec4(sqrt(clamp(sceneCol, 0., 1.)), iAlpha);

}

void main(){
    //ZGE does not use mainImage( out vec4 fragColor, in vec2 fragCoord )
    //Rededefined fragCoord as gl_FragCoord.xy-iViewport(dynamic window)
    mainImage(gl_FragColor,gl_FragCoord.xy-iViewport);
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="iResolution" VariableRef="uResolution"/>
        <ShaderVariable VariableName="iViewport" VariableRef="uViewport"/>
        <ShaderVariable VariableName="iTime" VariableRef="uTime"/>
        <ShaderVariable VariableName="iAlpha" VariableRef="uAlpha"/>
        <ShaderVariable VariableName="iColor" VariableRef="uColor"/>
      </UniformVariables>
    </Shader>
    <Group Comment="Default ShaderToy Uniform Variable Inputs">
      <Children>
        <Variable Name="uResolution" Type="6"/>
        <Variable Name="uTime"/>
        <Variable Name="uViewport" Type="6"/>
      </Children>
    </Group>
    <Group Comment="FL Studio Variables">
      <Children>
        <Array Name="Parameters" SizeDim1="5" Persistent="255">
          <Values>
<![CDATA[789C63606060387BC6C776D6CC99764026C3AC9992F6003AB005B8]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Alpha
Hue
Saturation
Lightness
Speed]]>
          </StringValue>
        </Constant>
        <Constant Name="AuthorInfo" Type="2">
          <StringValue>
<![CDATA[Shane (converted by Youlean & StevenM)
https://www.shadertoy.com/view/XtycR1
]]>
          </StringValue>
        </Constant>
      </Children>
    </Group>
    <Group Comment="Unique Uniform Variable Inputs">
      <Children>
        <Variable Name="uAlpha"/>
        <Variable Name="uColor" Type="7"/>
      </Children>
    </Group>
    <Group Comment="Materials and Textures">
      <Children>
        <Material Name="mCanvas" Blend="1" Shader="MainShader"/>
      </Children>
    </Group>
  </Content>
</ZApplication>
