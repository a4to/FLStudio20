FLhd   0  ` FLdt�  �	12.2.0.3 �.Z G a m e E d i t o r   V i s u a l i z e r   �4               I                  �  u   �  �  �    �HQV ��8L  [General]
GlWindowMode=1
LayerCount=6
FPS=2
DmxOutput=0
DmxDevice=0
MidiPort=0
Aspect=0
LayerOrder=0,1,2,3,4,5

[AppSet0]
App=Terrain\GoopFlow
ParamValues=150,0,0,347,405,905,717,0,26,500,0,500,1000,478,374,698
ParamValuesBackground\FogMachine=0,690,717,0,882,401,273,334,440,1000,1000,314
ParamValuesMisc\Automator=1000,185,273,400,608,161,498,1000,148,182,400,576,112,538,1000,111,61,400,627,112,512,1000,185,303,400,378,129,342,0,0,0,0
ParamValuesPhysics\Cage=600,200,300,0,800,500,500,500,500,500
ParamValuesPhysics\Columns=200,300,0,600,500,600,1000,1000,0,450,500,500,500,500
ParamValuesPhysics\Heightfield=200,300,0,800,500,500,1000,1000,0,550,500,500,500,500
ParamValuesPhysics\Ragdoll=0,0,500,500,500,600,500,1000
ParamValuesSaved\EffectsOld\backup3\backup\Clear\08. WormHoleEclipse=561,895,807,158,223,158,386,320,500,500,500
ParamValuesSaved\EffectsOld\backup3\backup\Clear\09. SolidColor=439,877,614,965
ParamValuesSaved\EffectsOld\backup3\backup\Clear\12. WormHoleDarkn=105,737,912,0,0,579,263,719,421,439,1000,500
ParamValuesSaved\EffectsOld\backup3\backup\Clear\14. Blooming=684,807,1000,912,719,789,947,632,351
ParamValuesTerrain\CubesAndSpheres=0,0,0,0,106,445,0,352,265,799,1000,242,1000,1000,97,939,1000,127
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet1]
App=Feedback\SphericalProjection
ParamValues=60,0,0,0,364,425,686,718,775,1000,0,1,1,731,500,500
ParamValuesBackground\FogMachine=0,166,866,0,1000,586,500,500,500,500,1000,387
ParamValuesFeedback\70sKaleido=0,0,0,1000,0,62
ParamValuesFeedback\BoxedIn=0,0,0,1000,500,0,0,500,0,0
ParamValuesFeedback\FeedMe=0,0,0,1000,1000,1000,358
ParamValuesObject Arrays\8x8x8_Eggs=0,448,381,0,154,500,500,357,574,644,0,221,221
ParamValuesParticles\fLuids=439,930,965,754,754,737,211,1000,754,1000,632,1000,632,877,386,175,158,0,1000,0,825,263,281,684,500,88,70,1000,1000,4,175,211
ParamValuesSaved\EffectsOld\backup3\backup\FgAndBg\43. fLuids=333,474,825,825,263,500,500,737,53,88,754,772,396,930,456,175,158,123,396,500,1000,579,982,544,500,53,1000,298,702,18,1000,0
Input=0
Enabled=1
UseBufferOutput=1
BufferRenderQuality=0
ImageIndex=0
MeshIndex=0

[AppSet2]
App=Background\FogMachine
ParamValues=0,0,0,880,500,500,222,500,500,0,0,0
ParamValuesBackground\FourCornerGradient=0,883,547,917,325,560,440,677,500,536,578,529,565,139
ParamValuesBlend\BufferBlender=0,111,867,862,250,111,1000,0,500,500,750
ParamValuesImage effects\ImageWarp=0,0,0,297,0,669,803,424
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,860,500,500,500,500,500
ParamValuesParticles\fLuids=0,0,0,0,305,500,500,0,0,0,500,500,500,0,500,250,500,0,0,500,500,500,0,0,500,0,0,250,500,0,0,0
ParamValuesSaved\EffectsOld\backup3\backup\FgAndBg\43. fLuids=544,107,526,351,614,500,500,105,228,228,439,439,1000,439,278,544,105,123,559,500,439,316,298,404,500,965,211,175,1000,0,193,35
ParamValuesTerrain\GoopFlow=0,0,0,0,596,500,500,0,500,497,241,628,500,1000,420,462
Input=0
Enabled=1
UseBufferOutput=1
BufferRenderQuality=0
ImageIndex=0
MeshIndex=0

[AppSet3]
App=Background\FourCornerGradient
ParamValues=2,1000,542,1000,1000,504,237,502,901,127,1000,827,1000,686
ParamValuesBackground\FogMachine=470,0,0,0,0,500,500,500,500,500,0,0
ParamValuesBackground\ItsFullOfStars=595,0,0,0,536,845,292,350,230,0,0
ParamValuesBlend\BufferBlender=0,222,333,59,250,222,1000,0,498,0,797
ParamValuesPostprocess\Blooming=0,253,1000,377,1000,738,1000,623,208
Input=0
Enabled=1
UseBufferOutput=1
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet4]
App=Blend\BufferBlender
ParamValues=0,2,12,1000,1,2,0,0,500,500,750
ParamValuesMisc\Automator=1000,111,182,400,448,23,586,1000,111,212,400,531,26,548,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0
ParamValuesPostprocess\Blooming=0,163,688,1000,465,960,199,372,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=1
MeshIndex=0

[AppSet5]
App=Misc\Automator
ParamValues=1,1,6,2,729,93,680,1,1,7,2,534,87,369,1,1,5,2,665,231,539,0,2,5,2,550,96,327
ParamValuesPostprocess\Blooming=0,0,0,1000,500,800,1000,245,0
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet6]
App=(none)
ParamValues=
ParamValuesFeedback\70sKaleido=525,163,858,1000,1000,263
ParamValuesFeedback\WarpBack=500,0,0,0,500,500,66
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet7]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet8]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet9]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet10]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet11]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet12]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet13]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet14]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet15]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet16]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet17]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet18]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet19]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet20]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet21]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet22]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet23]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[AppSet24]
App=(none)
ParamValues=
Input=0
Enabled=1
UseBufferOutput=0
BufferRenderQuality=0
ImageIndex=4
MeshIndex=0

[Video export]
VideoH=480
VideoW=640
VideoRenderFps=30
SampleRate=44100
VideoCodec=5
AudioCodec=3
AudioCodecFormat=1
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=C:\Users\chris\Desktop\TalesOfJulia.wmv
Bitrate=15000000
Uncompressed=0

[UserContent]
Text=
Html=
VideoCues=
Meshes=
MeshAutoScale=0
MeshWithColors=0
Images=[plugpath]Content\Bitmaps\Particles\furball.png
VideoUseSync=0

[Detached]
Top=0
Left=1920
Width=1920
Height=1040

