FLhd   0 * ` FLdt�  �20.6.2.1585 �1  %�.Z G a m e E d i t o r   V i s u a l i z e r   �4              I                  �     }  �  �    �HQV ՜$  ﻿[General]
GlWindowMode=1
LayerCount=21
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=1,2,9,8,22,20,23,25,24,27,19,10,0,21,3,4,12,13,5,6,26

[AppSet1]
App=Background\SolidColor
FParamValues=0,0,0,0
ParamValues=0,0,0,0
Enabled=1

[AppSet2]
App=Postprocess\Vignette
FParamValues=0,0,0,0.6,0.068,0.655
ParamValues=0,0,0,600,68,655
Enabled=1

[AppSet9]
App=Background\FourCornerGradient
FParamValues=3,1,0,1,1,0.25,1,1,0.5,1,1,0.75,1,1
ParamValues=3,1000,0,1000,1000,250,1000,1000,500,1000,1000,750,1000,1000
Enabled=1

[AppSet8]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1

[AppSet22]
App=Background\SolidColor
FParamValues=0.086,0,0,1
ParamValues=86,0,0,1000
Enabled=1

[AppSet20]
App=Image effects\Image
FParamValues=0,0,1,0,0.949,0.5,0.5,0.291,0,0,0,0,1,0
ParamValues=0,0,1000,0,949,500,500,291,0,0,0,0,1,0
ParamValuesHUD\HUD Prefab=67,0,449,1000,0,500,500,251,1000,791,444,0,500,1000,1000,0,1000,1000
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,0,546,662,500,500,500
Enabled=1
ImageIndex=4

[AppSet23]
App=Peak Effects\Linear
FParamValues=0.293,0,0.648,1,0.66,0.506,0.235,0.126,0,0.5,0.19,0.528,0,0,1,0,0,0.5,0.5,0.5,0.5,0,0.5,0.152,0.2,0,0.032,0.212,0.33,0.25,0.1
ParamValues=293,0,648,1000,660,506,235,126,0,500,190,528,0,0,1,0,0,500,500,500,500,0,500,152,200,0,32,212,330,250,100
Enabled=1
ImageIndex=4

[AppSet25]
App=HUD\HUD Prefab
FParamValues=207,0,0,1,0,0.5,0.5,0.605,0.841,0.96,4,0,0.25,1,0.563,0,1,1
ParamValues=207,0,0,1000,0,500,500,605,841,960,4,0,250,1,563,0,1000,1
Enabled=1
LayerPrivateData=78012B48CC4BCD298E290051BA0686267A9939650C23080000EBD2072B

[AppSet24]
App=Scenes\Spherical Polyhedra
FParamValues=0.677,0,0.25,0,0,1,3,0,0.625,0.635,0,1
ParamValues=677,0,250,0,0,1,3,0,625,635,0,1
Enabled=1

[AppSet27]
App=Misc\Automator
FParamValues=1,25,11,6,0,0.25,0.533,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,25,11,6,0,250,533,0,0,0,0,0,250,250,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1

[AppSet19]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.319,0.5
ParamValues=500,500,500,500,319,500
Enabled=1
UseBufferOutput=1

[AppSet10]
App=Image effects\Image
FParamValues=0,0,0,0,0.585,0.5,0.5,0.127,0,0,0,0,0,0
ParamValues=0,0,0,0,585,500,500,127,0,0,0,0,0,0
ParamValuesImage effects\ImageBox=0,0,0,0,0,500,500,500,500,500,0,0,0,34,4,0,500,500,500
Enabled=1
ImageIndex=3

[AppSet0]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,0,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1

[AppSet21]
App=Postprocess\Youlean Handheld
FParamValues=0.2,0.5,0.85,1,0.017,2
ParamValues=200,500,850,1000,17,2
Enabled=1

[AppSet3]
App=Postprocess\Youlean Bloom
AppVersion=1
FParamValues=0.076,0,0.15,0.455,1,0,0,0
ParamValues=76,0,150,455,1000,0,0,0
Enabled=1

[AppSet4]
App=Postprocess\Youlean Blur
FParamValues=0.253,1,0,1,0,0
ParamValues=253,1,0,1,0,0
Enabled=1
ImageIndex=1

[AppSet12]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.5,0.5,0.5,0.5,0.5
ParamValues=500,500,500,500,500,500
Enabled=1
UseBufferOutput=1

[AppSet13]
App=Blend\BufferBlender
FParamValues=0,0,2,1,0,0,0,0,0.5,0.5,0.75,0
ParamValues=0,0,2,1000,0,0,0,0,500,500,750,0
Enabled=1
ImageIndex=2

[AppSet5]
App=Postprocess\Youlean Color Correction
FParamValues=0.5,0.376,0.5,0.5,0.5,0.5
ParamValues=500,376,500,500,500,500
ParamValuesPostprocess\FrameBlur=0,0,0,0,750,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1

[AppSet6]
App=Misc\Automator
FParamValues=1,6,2,6,0.376,0.25,0.504,1,6,4,6,0.5,0.25,0.501,0,0,0,0,0,0.25,0.25,0,0,0,0,0,0.25,0.25
ParamValues=1,6,2,6,376,250,504,1,6,4,6,500,250,501,0,0,0,0,0,250,250,0,0,0,0,0,250,250
Enabled=1

[AppSet26]
App=Postprocess\Youlean Handheld
FParamValues=0.2,0.5,0.85,1,0.011,1
ParamValues=200,500,850,1000,11,1
Enabled=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text=Hyperspeed,Eveningland
Html="<p align=""center"">[textline]</p>"
Meshes=[plugpath]Content\Meshes\Sphere(Texture).3ds
Images="[plugpath]ComboWizard\Assets Revisualizer\Hyperspeed.jpg"
VideoUseSync=0
Filtering=0

[Detached]
Top=-1080
Left=0
Width=1920
Height=1080

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

