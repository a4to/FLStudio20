FLhd   0  0 FLdt�  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   ղ0.  ﻿[General]
GlWindowMode=1
LayerCount=29
FPS=2
MidiPort=-1
Aspect=1
LayerOrder=0,6,1,16,17,18,9,2,29,3,4,7,11,10,22,26,23,27,28,24,5,8,15,12,14,19,25,30,20
WizardParams=129,1425,1475,1476,177,225,465,949,950,952,953

[AppSet0]
App=Background\SolidColor
ParamValues=0,0,0,868
Enabled=1
Collapsed=1

[AppSet6]
App=HUD\HUD Grid
ParamValues=128,500,0,0,500,500,1000,1000,1000,4,500,1000,1000,748,0,500,1000,0,412,0,0,1
Enabled=1
Collapsed=1

[AppSet1]
App=HUD\HUD Prefab
ParamValues=66,0,500,0,0,500,500,90,1000,1000,4,0,500,1,368,0,1000,1
ParamValuesCanvas effects\FreqRing=0,500,1000,500,500,500,500,1000,0,0,0,4,500,16
ParamValuesCanvas effects\SkyOcean=472,328,500,0,0,500,500,0,500,260,0,0,0
ParamValuesCanvas effects\ImageTileSprite=5,5,0,0,0,0,4,250,250,500,500,0,492,0,0,0,0
Enabled=1
Collapsed=1
LayerPrivateData=78DA4B2FCA4C89490712BA0606867A9939650C230B0000B42405E0

[AppSet16]
App=HUD\HUD Prefab
ParamValues=66,0,500,0,0,288,500,90,1000,1000,4,0,500,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4B2FCA4C89490712BA0606867A9939650C230B0000B42405E0

[AppSet17]
App=HUD\HUD Prefab
ParamValues=66,0,500,0,0,288,865,90,1000,1000,4,0,500,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4B2FCA4C89490712BA0606867A9939650C230B0000B42405E0

[AppSet18]
App=HUD\HUD Prefab
ParamValues=66,0,500,0,0,500,865,90,1000,1000,4,0,500,1,368,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4B2FCA4C89490712BA0606867A9939650C230B0000B42405E0

[AppSet9]
App=Canvas effects\SkyOcean
ParamValues=0,840,500,0,0,500,500,0,500,260,0,0,0
Enabled=1
Collapsed=1

[AppSet2]
App=Canvas effects\SkyOcean
ParamValues=0,500,500,0,0,500,552,13,500,260,0,0,0
Enabled=1
Collapsed=1

[AppSet29]
App=Canvas effects\SkyOcean
ParamValues=0,500,500,0,0,500,552,191,500,260,0,0,0
Enabled=1
Collapsed=1

[AppSet3]
App=Canvas effects\SkyOcean
ParamValues=0,840,500,0,0,500,584,31,500,260,0,0,0
Enabled=1
Collapsed=1

[AppSet4]
App=Canvas effects\SkyOcean
ParamValues=0,500,500,0,0,500,708,46,500,260,0,0,0
Enabled=1
Collapsed=1

[AppSet7]
App=Postprocess\Youlean Bloom
ParamValues=712,0,150,30,1000,0,0,0
ParamValuesPostprocess\FrameBlur=0,0,0,0,862,425,500,590,500,500,0,333,530,1000,500,500,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet11]
App=HUD\HUD Prefab
ParamValues=0,0,500,0,0,500,484,0,1000,1000,4,0,500,1,368,43,1000,1
Enabled=0
Collapsed=1
LayerPrivateData=78DA73C8CBCF4BD52B2E4B671899000060F9036F

[AppSet10]
App=Postprocess\Blooming
ParamValues=0,0,0,1000,500,1000,396,616,0
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet22]
App=HUD\HUD Prefab
ParamValues=63,747,500,0,0,199,102,455,1000,1000,4,0,500,1,546,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D8C2CF43273CA18460000002F290AB6

[AppSet26]
App=HUD\HUD Prefab
ParamValues=63,747,500,1000,0,199,102,455,1000,1000,4,0,500,1,546,131,28,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D8C2CF43273CA18460000002F290AB6

[AppSet23]
App=HUD\HUD Prefab
ParamValues=53,786,500,0,0,812,897,455,1000,1000,4,0,500,1,546,0,1000,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C2DF43273CA18460000002E3F0AB5

[AppSet27]
App=HUD\HUD Prefab
ParamValues=53,786,840,1000,0,812,897,455,1000,1000,4,0,500,1,546,341,33,1
Enabled=1
Collapsed=1
LayerPrivateData=78DA4BCECF2DC8CF4BCD2B298E498631750D0C2DF43273CA18460000002E3F0AB5

[AppSet28]
App=Misc\Automator
ParamValues=1,27,16,2,500,786,250,1,27,17,2,500,362,250,1,28,16,2,500,426,250,1,28,17,2,500,678,250
Enabled=1
Collapsed=1

[AppSet24]
App=Postprocess\Blooming
ParamValues=0,0,0,1000,500,800,500,133,0
ParamValuesPostprocess\Youlean Motion Blur=852,0,0,0
Enabled=1
UseBufferOutput=1
Collapsed=1

[AppSet5]
App=Image effects\ImageSphereWarp
ParamValues=0,0,451,1000,0,400,0,500,500
Enabled=1
Collapsed=1
ImageIndex=1

[AppSet8]
App=Postprocess\ParameterShake
ParamValues=420,292,5,1,420,292,14,7,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet15]
App=Postprocess\ParameterShake
ParamValues=420,292,19,7,420,292,11,7,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0
Enabled=1
Collapsed=1

[AppSet12]
App=Image effects\Image
ParamValues=920,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=2

[AppSet14]
App=HUD\HUD Prefab
ParamValues=0,156,500,0,1000,500,493,0,1000,1000,4,0,500,1,368,111,1000,1
Enabled=0
Collapsed=1
LayerPrivateData=78DA73C8CBCF4BD52B2E4B671899000060F9036F

[AppSet19]
App=HUD\HUD Prefab
ParamValues=0,156,500,0,0,500,484,0,1000,1000,4,0,500,1,368,111,1000,1
Enabled=1
Name=LOGO
LayerPrivateData=78DA73C8CBCF4BD52B2E4B671899000060F9036F

[AppSet25]
App=Image effects\Image
ParamValues=0,0,0,0,1000,500,500,0,0,0,0,0,0,0
Enabled=1
Collapsed=1
ImageIndex=3

[AppSet30]
App=Postprocess\Youlean Color Correction
ParamValues=500,544,500,500,1000,500
Enabled=1
Collapsed=1

[AppSet20]
App=Text\TextTrueType
ParamValues=0,0,0,0,0,500,500,0,0,0,500
Enabled=1
Collapsed=1

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0

[UserContent]
Text="This is the default text."
Html="<position x=""4""><position y=""8""><p align=""left""><font face=""American-Captain"" size=""9"" color=""#ffffff"">[author]</font></p></position>","<position x=""55""><position y=""88""><p align=""center""><font face=""American-Captain"" size=""9"" color=""#ffffff"">[title]</font></p></position>","<position x=""0""><position y=""90""><p align=""center""><font face=""Chosence-Bold"" size=""3"" color=""#ffffff"">[extra1]</font></p></position>",,"<position x=""0""><position y=""95""><p align=""center""><font face=""Chosence-Bold"" size=""3"" color=""#FFFFFF"">[comment]</font></p></position>","  "
Images="[plugpath]Effects\HUD\prefabs\small elements\triangles\triangle-007.ilv"
VideoUseSync=0
EnableMipmap=1

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

