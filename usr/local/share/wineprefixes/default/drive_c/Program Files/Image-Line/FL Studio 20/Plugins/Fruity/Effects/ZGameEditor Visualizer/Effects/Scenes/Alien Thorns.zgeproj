<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" FileVersion="2">
  <OnLoaded>
    <ZLibrary Comment="HSV Library">
      <Source>
<![CDATA[vec3 hsv(float h, float s, float v)
{
  s = clamp(s/100, 0, 1);
  v = clamp(v/100, 0, 1);

  if(!s)return vector3(v, v, v);

  h = h < 0 ? frac(1-abs(frac(h/360)))*6 : frac(h/360)*6;

  float c, f, p, q, t;

  c = floor(h);
  f = h-c;

  p = v*(1-s);
  q = v*(1-s*f);
  t = v*(1-s*(1-f));

  switch(c)
  {
    case 0: return vector3(v, t, p);
    case 1: return vector3(q, v, p);
    case 2: return vector3(p, v, t);
    case 3: return vector3(p, q, v);
    case 4: return vector3(t, p, v);
    case 5: return vector3(v, p, q);
  }
}

float lerp(float target,float value){
  float mul=clamp(abs(target-value),.1,900.0);
    if(abs(target-value)>app.DeltaTime*mul)
      {
      if (value-target>0.0)value-=app.DeltaTime*mul*.5;
      if (value-target<0.0)value+=app.DeltaTime*mul*.5;
      }
  else value=target;
  return value;
}

float lerpMed(float target,float value){
  float mul=clamp(abs(target-value),.2,900.0);
    if(abs(target-value)>app.DeltaTime*mul*2.0)
      {
      if (value-target>0.0)value-=app.DeltaTime*mul*1.5;
      if (value-target<0.0)value+=app.DeltaTime*mul*1.5;
      }
  else value=target;
  return value;
}

float lerpSlow(float target,float value){
  float mul=clamp(abs(target-value),.4,900.0);
    if(abs(target-value)>app.DeltaTime*mul*parameters[7])
      {
      if (value-target>0.0)value-=app.DeltaTime*mul*.5*parameters[7];
      if (value-target<0.0)value+=app.DeltaTime*mul*.5*parameters[7];
      }
  else value=target;
  return value;
}]]>
      </Source>
    </ZLibrary>
    <ZLibrary Comment="Constants">
      <Source>
<![CDATA[const int
  ALPHA = 0,
  SKY_HUE =1,
  SKY_SAT =2,
  SKY_VAL =3,
  BLEND = 4,
  SPEED = 5,
  CAM_Y = 6,
  ROTATION = 7;]]>
      </Source>
    </ZLibrary>
  </OnLoaded>
  <OnUpdate>
    <ZExpression>
      <Expression>
<![CDATA[uResolution=vector2(app.ViewportWidth,app.ViewportHeight);
uViewport=vector2(app.ViewportX,app.ViewportY);

uMouse=vector4(0.0,0.0,0.0,0.0);
uAlpha=1.0-Parameters[ALPHA];

uSkyCol=hsv(Parameters[SKY_HUE]*360,Parameters[SKY_SAT]*100,(1-Parameters[SKY_VAL])*100);
mCanvas.Blend=floor(Parameters[BLEND]*1000);
float speed=Parameters[SPEED]*2.0;
uCamY=Parameters[CAM_Y];
uRot=Parameters[ROTATION];

float delta=app.DeltaTime*Speed;
uTime+=delta;]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <OnRender>
    <UseMaterial Material="mCanvas"/>
    <RenderSprite/>
  </OnRender>
  <Content>
    <Shader Name="Alien_Thorns">
      <VertexShaderSource>
<![CDATA[#version 120

void main(){
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[#version 120

uniform vec2 iResolution,iViewport;
uniform float iTime,iAlpha,iCamY,iRot,iTest;
uniform sampler2D tex1,tex2,tex3,tex4;
uniform vec4 iMouse;
uniform vec3 iSkyCol;


#define iChannel0 tex1
#define iChannel1 tex2
#define iChannel2 tex3
#define iChannel3 tex4
// Alien Thorns
// Dave Hoskins
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.

#define PRECISION 0.02

#define MOD3 vec3(.0631,.07369,.08787)

vec3 sunDir = normalize(vec3(-.3, 0.6, .8));

float time;

//--------------------------------------------------------------------------------------------------
vec3 TexCube(in vec3 p, in vec3 n )
{
    p *= .5;
	vec3 x = texture2D( iChannel0, p.yz, 0.0 ).xyz;
	vec3 y = texture2D( iChannel1, p.zx, 0.0 ).xyz;
	vec3 z = texture2D( iChannel2, p.xy, 0.0 ).xyz;
	return x*abs(n.x) + y*abs(n.y) + z*abs(n.z);
}

//--------------------------------------------------------------------------------------------------
vec4 ThornVoronoi( vec3 p, out float which)
{

    vec2 f = fract(p.xz);
    p.xz = floor(p.xz);
	float d = 1.0e10;
    vec3 id = vec3(0.0);

	for (int xo = -1; xo <= 1; xo++)
	{
		for (int yo = -1; yo <= 1; yo++)
		{
            vec2 g = vec2(xo, yo);
            vec2 n = texture2D(iChannel3,(p.xz + g+.5)/256.0, 0.0).xy;
            n = n*n*(3.0-2.0*n);

			vec2 tp = g + .5 + sin(p.y + 1.2831 * (n * time*.5)) - f;
            float d2 = dot(tp, tp);
			if (d2 < d)
            {
                // 'id' is the colour code for each thorn
                d = d2;
                which = n.x+n.y*3.0;
                id = vec3(tp.x, p.y, tp.y);
            }
		}
	}

    return vec4(id, 1.35-pow(d, .17));
}


//--------------------------------------------------------------------------------------------------
float MapThorns( in vec3 pos)
{
    float which;
	return pos.y * .21 - ThornVoronoi(pos, which).w  - max(pos.y-5.0, 0.0) * .5 + max(pos.y-5.5, 0.0) * .8;
}

//--------------------------------------------------------------------------------------------------
vec4 MapThornsID( in vec3 pos, out float which)
{
    vec4 ret = ThornVoronoi(pos, which);
	return vec4(ret.xyz, pos.y * .21 - ret.w - max(pos.y-5.0, 0.0) * .5 + max(pos.y-5.5, 0.0) * .8);
}

//--------------------------------------------------------------------------------------------------
float Hash12(vec2 p)
{
	vec3 p3  = fract(vec3(p.xyx) * MOD3);
    p3 += dot(p3, p3.yzx + 19.19);
    return fract(p3.x * p3.y * p3.z);
}

//--------------------------------------------------------------------------------------------------
vec4 Raymarch( in vec3 ro, in vec3 rd, in vec2 uv, in vec2 fragCoord, out float which)
{
	float maxd = 40.0;

    vec4 h = vec4(1.0);
    float t = 0.+ Hash12(fragCoord.xy)*.2;
    vec3 p;
    for (int i = 0; i < 200; i++)
    {
        p = ro + rd * t;
        if(h.w < PRECISION || t > maxd || p.y > 12.0 ) break;
	    h = MapThornsID(p, which);
        t += h.w * .5 + min(t*.002, .03);
    }

    if (t > maxd || p.y > 8.0)	t = -1.0;

    return vec4(h.xyz, t);
}

//--------------------------------------------------------------------------------------------------
vec3 Normal( in vec3 pos )
{
    vec2 eps = vec2(PRECISION, 0.0);
	return normalize( vec3(
           MapThorns(pos+eps.xyy) - MapThorns(pos-eps.xyy),
           MapThorns(pos+eps.yxy) - MapThorns(pos-eps.yxy),
           MapThorns(pos+eps.yyx) - MapThorns(pos-eps.yyx) ) );

}

//--------------------------------------------------------------------------
float FractalNoise(in vec2 xy)
{
	float w = 1.5;
	float f = .5;
    xy *= .08;

	for (int i = 0; i < 5; i++)
	{
		f += texture2D(iChannel2, .5+xy * w, -99.0).x / w;
		w += w;
	}
	return f*.8;
}

//--------------------------------------------------------------------------
vec3 GetClouds(in vec3 sky, in vec3 cameraPos, in vec3 rd)
{
    //if (rd.y < 0.0) return vec3(0);
	// Uses the ray's y component for horizon fade of fixed colour clouds...
	float v = (70.0-cameraPos.y)/rd.y;
	rd.xz = (rd.xz * v + cameraPos.xz+vec2(0.0,0.0)) * 0.004;
	float f = (FractalNoise(rd.xz) -.5);
	vec3 cloud = mix(sky, vec3(.4, .2, .2), max(f, 0.0));
   	return cloud;
}

//
//--------------------------------------------------------------------------------------------------
float Shadow( in vec3 ro, in vec3 rd, float mint)
{
    float res = 1.0;
    float t = .15;
    for( int i=0; i < 15; i++ )
    {
        float h = MapThorns(ro + rd*t);
		h = max( h, 0.0 );
        res = min( res, 4.0*h/t );
        t+= clamp( h*.6, 0.05, .1);
		if(h < .001) break;
    }
    return clamp(res,0.05,1.0);
}

//--------------------------------------------------------------------------------------------------
vec3 Path( float time )
{
   //SteveM: CameraY, Rotate parameters added
  return vec3(1.3+ ((iRot-.5)*90.0)*cos(0.2-0.5*.33*time*.75), iCamY*6.0+2.0, 7.0 - 16.2*sin(0.5*0.11*time*.75) );
}

//--------------------------------------------------------------------------------------------------
void mainImage( out vec4 fragColor, in vec2 fragCoord )
{

    vec2 q = fragCoord.xy / iResolution.xy;
	vec2 p = (-1.0 + 2.0*q)*vec2(iResolution.x / iResolution.y, 1.0);

    // Camera...
	float off = iMouse.x*1.0*iMouse.x/iResolution.x;
	time =173.0+iTime + off;
	vec3 ro = Path( time+0.0 );

	vec3 ta = Path( time+5.2 );
    float add = (sin(time*.3)+1.0)*2.0;
    ro.y+= add;
    ta.y -= add;
	ta.y *= 1.0+sin(3.0+0.12*time) * .5;
	float roll = 0.3*sin(0.07*time);

	vec3 cw = normalize(ta-ro);
	vec3 cp = vec3(sin(roll), cos(roll),0.0);
	vec3 cu = normalize(cross(cw,cp));
	vec3 cv = (cross(cu,cw));

	float r2 = p.x*p.x*0.32 + p.y*p.y;
    p *= (7.0-sqrt(37.5-11.5*r2))/(r2+1.0);

	vec3 rd = normalize( p.x*cu + p.y*cv + 2.1*cw );
  //StevenM Using sky color(iSkyCol) for Viz HSV
	vec3 col 		= mix(iSkyCol, GetClouds(vec3(0.), ro, rd),	pow(abs(rd.y), .5));
    vec3 background = mix(vec3(.3, .3, .5), vec3(.0), 						pow(abs(rd.y), .5));

	float sun = clamp( dot(rd, sunDir), 0.0, 1.0 );
	float which;
	vec4 ret = Raymarch(ro, rd, q, fragCoord, which);

    if(ret.w > 0.0)
	{
		vec3 pos = ro + ret.w*rd;
		vec3 nor = Normal(pos);
		vec3 ref = reflect(rd, nor);

		float s = clamp( dot( nor, sunDir ), 0.0, 1.0 );

        float sha = 0.0; if( s>0.01) sha = Shadow(pos, sunDir, 0.05);
		vec3 lin = s*vec3(1.0,.9,.8) * sha;
		lin += background*(max(nor.y, 0.0)*.2);

		col = TexCube(ret.xyz, nor);
        vec3 wormCol =  clamp(abs(fract(which * 1.5 + vec3(1.0, 2.0 / 3.0, 1.0 / 3.0)) * 6.0 - 3.0) -1.0, 0.0, 1.0);

		col = lin * col * (.7 + wormCol * .6);
        col += vec3(1.0, .6, 1.0)*pow(clamp( dot( ref, sunDir ), 0.0, 1.0 ), 10.0) * sha;

		col = mix( col, background, 1.0-exp(-0.002*ret.w*ret.w) );
	}

    col += vec3(.4,.25,.25)*pow( sun, 20.0 )*4.0*clamp( (rd.y+0.4) / .2,0.0,1.0);
    // Gamma & colour adjust...
	col = pow(col, vec3(.45, .45, .5));
    // Border shading...
    col *= 0.5 + 0.5*pow( 52.0*q.x*q.y*(1.0-q.x)*(1.0-q.y), 0.2 );

	fragColor = vec4( col, iAlpha );
}

void main(){
    mainImage(gl_FragColor,gl_FragCoord.xy-iViewport);
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="iResolution" VariableRef="uResolution"/>
        <ShaderVariable VariableName="iViewport" VariableRef="uViewport"/>
        <ShaderVariable VariableName="iTime" VariableRef="uTime"/>
        <ShaderVariable VariableName="iMouse" VariableRef="uMouse"/>
        <ShaderVariable VariableName="iAlpha" VariableRef="uAlpha"/>
        <ShaderVariable DesignDisable="255" VariableName="iTest" VariableRef="uTest"/>
        <ShaderVariable VariableName="iCamY" VariableRef="uCamY"/>
        <ShaderVariable VariableName="iRot" VariableRef="uRot"/>
        <ShaderVariable VariableName="iSkyCol" VariableRef="uSkyCol"/>
      </UniformVariables>
    </Shader>
    <Group Comment="Default ShaderToy Uniform Varible Inputs">
      <Children>
        <Variable Name="uResolution" Type="6"/>
        <Variable Name="uTime"/>
        <Variable Name="uMouse" Type="8"/>
      </Children>
    </Group>
    <Group Comment="FL Studio Varibles">
      <Children>
        <Array Name="Parameters" SizeDim1="8" Persistent="255">
          <Values>
<![CDATA[78DA636000037B0686063B109D2FD46C05E183310300301802F9]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Alpha
Sky Hue
Sky Sat
SKy Val
Blend @list1000: None,Alpha/OneMinusSourceAlpha,Alpha/one,Color/OneMinusSourceColor,AlphaSaturate/One,OneMinusSourceAlpha/Alpha
Speed
Cam Y
Rotate
]]>
          </StringValue>
        </Constant>
        <Constant Name="AuthorInfo" Type="2" StringValue="Dave Hoskins"/>
      </Children>
    </Group>
    <Group Comment="Unique Uniform Varible Inputs">
      <Children>
        <Variable Name="uViewport" Type="6"/>
        <Variable Name="uAlpha"/>
        <Variable Name="uTest" DesignDisable="255"/>
        <Variable Name="uCamY"/>
        <Variable Name="uRot"/>
        <Variable Name="uSkyCol" Type="7"/>
      </Children>
    </Group>
    <Group Comment="Materials and Textures">
      <Children>
        <Material Name="mCanvas" Shader="Alien_Thorns">
          <Textures>
            <MaterialTexture Texture="Bitmap1"/>
            <MaterialTexture Texture="Bitmap2"/>
            <MaterialTexture Texture="Bitmap3"/>
            <MaterialTexture Texture="Bitmap4"/>
          </Textures>
        </Material>
        <Bitmap Name="Bitmap1" Comment="NoCustom" Width="1024" Height="1024" Filter="2">
          <Producers>
            <BitmapCells RandomSeed="67" BorderPixels="0"/>
            <BitmapExpression>
              <Expression>
<![CDATA[//X,Y : current coordinate (0..1)
//Pixel : current color (rgb)
//Sample expression: Pixel.R=abs(sin(X*16));

Pixel.r*=(Pixel.g+Pixel.b)*rnd()*.9;
Pixel.g*=(Pixel.r+Pixel.b)*rnd()*.6;
Pixel.b*=(Pixel.g+Pixel.r)*rnd()*.3;]]>
              </Expression>
            </BitmapExpression>
          </Producers>
        </Bitmap>
        <Bitmap Name="Bitmap2" Comment="NoCustom" Width="512" Height="512" Filter="2">
          <Producers>
            <BitmapExpression>
              <Expression>
<![CDATA[//X,Y : current coordinate (0..1)
//Pixel : current color (rgb)
//Sample expression: Pixel.R=abs(sin(X*16));

Pixel.r+=rnd()*.3;]]>
              </Expression>
            </BitmapExpression>
          </Producers>
        </Bitmap>
        <Bitmap Name="Bitmap3" Comment="NoCustom" Width="129" Height="129" Filter="2">
          <Producers>
            <BitmapNoise Octaves="9" Persistence="2.1" ZHeight="82" Tile="255"/>
            <BitmapExpression>
              <Expression>
<![CDATA[//X,Y : current coordinate (0..1)
//Pixel : current color (rgb)
//Sample expression: Pixel.R=abs(sin(X*16));

Pixel.r*=rnd();
Pixel.b*=rnd();
Pixel.g=(Pixel.r+Pixel.b)*.5;]]>
              </Expression>
            </BitmapExpression>
          </Producers>
        </Bitmap>
        <Bitmap Name="Bitmap4" Comment="NoCustom" Width="256" Height="256">
          <Producers>
            <BitmapExpression>
              <Expression>
<![CDATA[//X,Y : current coordinate (0..1)
//Pixel : current color (rgb)
//Sample expression: Pixel.R=abs(sin(X*16));

Pixel.r = rnd();
Pixel.b = rnd();
Pixel.g = rnd();]]>
              </Expression>
            </BitmapExpression>
          </Producers>
        </Bitmap>
      </Children>
    </Group>
  </Content>
</ZApplication>
