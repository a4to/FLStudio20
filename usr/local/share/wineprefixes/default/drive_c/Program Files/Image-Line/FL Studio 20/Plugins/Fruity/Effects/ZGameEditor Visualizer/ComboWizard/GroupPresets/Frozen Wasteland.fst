FLhd   0  0 FLdt�  �11.5.5 �.Z G a m e E d i t o r   V i s u a l i z e r   �4                                                   ՜2  ﻿[General]
GlWindowMode=1
LayerCount=8
FPS=2
MidiPort=-1
AspectRatio=16:9
LayerOrder=2,6,5,4,3,0,1,7

[AppSet2]
App=Image effects\Image
FParamValues=0,0.8333,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,833,0,0,1000,500,500,0,0,0,1,0,0,0
ParamValuesPeak Effects\Youlean Waveform=0,0,0,0,186,500,148,500,228,250,500,102,712,0,100,0,0,1000,12,120,0,500,100,100,0,0,282,500,500,1000,0,1000
ParamValuesPeak Effects\Stripe Peeks=0,0,0,0,0,0,74,500,220,0,250,500,700,0,500,150,300,200,200,300,1000,0,0
ParamValuesPeak Effects\Linear=0,964,0,0,178,765,488,388,250,500,568,350,500,1000,500,0,0,500,500,500,468,0,500,152,200,0,32,212,330,250,100
ParamValuesPeak Effects\Polar=0,0,0,0,188,500,152,1000,556,500,220,596,0,1000,0,500,1000,1000,968,1000,1000,0,1000,1000
ParamValuesPeak Effects\SplinePeaks=940,833,0,1000,900,500,360,0,0,0
Enabled=1
ImageIndex=1
Name=Section2

[AppSet6]
App=HUD\HUD 3D
FParamValues=0,1,0.7944,0.5032,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,0,0,1,1,0
ParamValues=0,1,794,503,500,500,500,500,500,500,500,500,500,500,0,500,500,500,500,500,500,500,500,1,1,0,0,1000,1000,0
Enabled=1
ImageIndex=3
Name=ForegroundMod

[AppSet5]
App=Image effects\Image
FParamValues=0,0.5229,0.2,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,522,200,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
Name=TextFst

[AppSet4]
App=Text\TextTrueType
FParamValues=0,0.5229,0.2,0,0,0.5,0.5,0,0,0,0.5
ParamValues=0,522,200,0,0,500,500,0,0,0,500
Enabled=0
Name=Text

[AppSet3]
App=Postprocess\Youlean Drop Shadow
FParamValues=0.5,0.2,0,1,0.02,0.875,0
ParamValues=500,200,0,1000,20,875,0
Enabled=1
UseBufferOutput=1

[AppSet0]
App=Scenes\Frozen Wasteland
FParamValues=0,0,0,0,0.364,1
ParamValues=0,0,0,0,364,1
ParamValuesPeak Effects\Fluidity=0,488,1000,0,472,508,512
ParamValuesScenes\Boaty Goes Caving=0,250,0
ParamValuesScenes\Alien Thorns=0,500,542,724,4,832,468,808
ParamValuesPeak Effects\Linear=0,760,648,0,574,500,160,1000,0,672,260,350,0,0,500,0,0,500,500,500,500,1000,0,0,1000,0,0,212,330,250,100
ParamValuesObject Arrays\Filaments=0,0,0,0,500,500,792,500,292,296,212,0,500,0
ParamValuesPeak Effects\PeekMe=0,792,1000,328,500,500,552,150,520,0,892,0
ParamValuesPeak Effects\Reactive Sphere=0,1000,833,0,900,500,500,252,252,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ParamValuesObject Arrays\BallZ=0,0,1000,0,750,500,500,948,500,500,500,500,500
ParamValuesScenes\Alps=0,167,206,0,250,628,628,0,0
Enabled=1
Name=Section0

[AppSet1]
App=Postprocess\Youlean Color Correction
FParamValues=0.4024,0.5,0.5,0.5,0.5,0.5
ParamValues=402,500,500,500,500,500
Enabled=1
Name=Section1

[AppSet7]
App=Image effects\Image
FParamValues=0,0,0,0,1,0.5,0.5,0,0,0,1,0,0,0
ParamValues=0,0,0,0,1000,500,500,0,0,0,1,0,0,0
Enabled=1
ImageIndex=2

[Video export]
VideoH=1080
VideoW=1920
VideoRenderFps=30
SampleRate=0
VideoCodecName=
AudioCodecName=
VideoQuality=0
MaxKeyFrameSpacing=3000
Filename=
Bitrate=17418240
AudioBitrate=128000
Uncompressed=0
Supersample=0

[UserContent]
Text="This is the default text."
Html="This is the default text."
Images="@EmbeddedFst:""Name=Dirty blockbuster.fst"",Data=7801AD544B6EDB3010DD0BF01D02AF5597A4487D8A30802D5935D0A44DE32659B859B0D23426204B024DA7F1D9BAE8917A850EE94F935591B6106C727E6F661E38F3F3FB8FC55B68C1A8E66E10BC6D6E755B77DF2EBA1A241D04E76A0B26EF36AD95D120282FE74E79A16B7DD9192B5FA1305EF750D92B65752769FC26DBC77C30351849421AB241300816E3BE9F832598026F72765D7CC6DFC92778B40E5619B5BA51CD06D618424622C47F3C0915191E3423F16F2D5A9C52C424F16E11E6F0213E0CEFFEC3CA9EA30AB2378810F1C283E894598860072C827ECEB873A04EDC070E8269ABBE34501F89B934FA41592894553249094DA284255CD08490928C4B81725288F1848FF3499CD0347170686311E1AC78C20B7D292F1C7B1FB12C7107B2358A28DB97C9B0DA5114D31D872F25878708EA7B7F868878FF879F7C9AA7ACE4D3BCE0133E2D4422F2715EF2ACA06CC2329EC5114D45E64862643C25B1C057777C3DECA52CC5A9E34270E4C6B3947254F8CFB3C478161DDADA7B38D6FEFC84E210413D4BCF1011EF00870DFCC32BFA4B966E740DDD093CF6389B48951767929294EC855B49337610AEA0C5112DFBB58C503557ABBE011C639007871C7740F55EAD40E2D06E6ADD3D5578F08F1BD568BB751117EAF11D6C4B9C6398F7AAD2ED3DC2123494BA81760732D1D6B80434E134651C8D1EF6A866A98FB86EAB6ED51B58AF71D4D069BEE9C1AC7D7D4EC4E770BD764BA9B5D0BA3EDD12910BAB6D0377E1426DECB233784190D5CE6166578D1C9EF62758ED7D2B87C30AF56086C3B385C5D846B77077FABA3F1BEE6942F8F9B6AD5C32AC1E3D5D37BBCC0558552DA17669BB5E4619C77D075FADA4116EBE5B5DDBA58C53BCCE40DF2FAD1449E22B76D59AAE69C060E415D4E70F8D6422C6856B00DAA334C1157814CE37ABE37D10FC02DE277D4B","@EmbeddedFst:""Name=Song Position horizontal.fst"",Data=78018D544B6EDB3010DD0BD01D04AF5D97A4FE4518C0DF06A8D3BA713E28922C58691213952541A2D3F86C5DF448BD426728DBB0D12C8A381467C8796FE671C83FBF7EDF7F84121A553CBACEC7E24E9779F5F3B2CA4172D799AB2D34E36A531AE9BBCE6CB124E7A5CEF5A26A8C7C87C6B0AD213357CAE84AF2E843BA8BF9D2E4D048D6E77DE13AAE733FACEB251886143893D77A0D1E12E94C99AA691FBE559B0254E959FF483544A51AB5BE55C5065A846183300AFAF46522212B8DAC25FC0829C81FC5E819F000770D429A0941234D190EF6AFDB89499F8277D088DBA15A4CC62884400932448B0079C8707E84C6193B415B80FAE14D9F9E5092F66151158A34D891C7A2C30993BE4F78446107240984A5F379E721BEE31D070C8C719D69A9BE1790D3511C94E5FFA3ECD2A875FD96B65D8A83204A51AED48A469AC5688900F5A6644867AB2CAD5825FBA7B563A55DDEAC4F4004D3D971FF0884AA26F75189B6BA93B26CDB2D1AFDA20C4C9451324E188FFDF1749C8859301D4F8251309D8471381E8E67411A719EC42922B3D1301AB260121F292376CA9C1CCD5C97F0669BA55D9B61A95C504351FF08665B49740D683DB4E47742ECA4D84BB3FFEE627948B1F88F23F3ED9CD3E8FBE426AC7FDA719F0225402A113DB7E4D68A58DFB73D684F0481F76AEEBF9D8788856D236415C8E9FB088581B6654F5BE856E75079F05AE3A546B5AC7921394BF080AD7127792AF6C6159478B767752B7D742DB1A50AC0FB0F72BF618C8F47F659AD41E26DDFE4BA3A7658BCAF1B5568B3A5884BF5FA09B633BCECB0AC55A6CB6784A52E9FE902CA0E64A44D43043C0E7822025CB4B007B7486CC44D9955EBBA81B6C5CB819B969B1A9AD6E64726DE959B965EB3D24049755EC3AB91BDEB956E3DFC991578393CA94D613C832B839EEB5C9875217B67B587E93E97B2D7CB30129A5EEFFC9EB614D8458F67EFEB73DC6AEB42FCE5B6CC880DD3C79D544E473D01A3B215E4C45BD5D24F037C29E1C948EEE39B79A773B3925182D30BD0CF2B23C3B86B624AB7A98A021A8CBC827CFE52481146F8543700E5C11AE1437930E69BF561EE3A7F017A538E8B"
VideoUseSync=0
Filtering=0

[Controller]
RedLvl=256
GreenLvl=256
BlueLvl=256
LumLvl=256

[Wizard]
Section0Cb=Frozen Wasteland
Section1Cb=Brightness-Gamma-Contrast
Section2Cb=Song Position horizontal
Section3Cb=Dirty blockbuster

