<?xml version="1.0" encoding="iso-8859-1" ?>
<ZApplication Name="App" Caption="ZGameEditor application" FileVersion="2">
  <OnLoaded>
    <ZExpression/>
    <ZExternalLibrary ModuleName="ZGameEditor Visualizer">
      <Source>
<![CDATA[void ParamsNotifyChanged(xptr Handle,int Layer) { }
void ParamsChangeName(xptr Handle,int Layer, int Parameters, string NewName) { }]]>
      </Source>
    </ZExternalLibrary>
  </OnLoaded>
  <OnUpdate>
    <ZExpression>
      <Expression>
<![CDATA[uMode=round(1.0 + Parameters[0] * 2.0);
uAlpha=Parameters[1];
uMaskR=Parameters[2];
uMaskG=Parameters[3];
uMaskB=Parameters[4];
uTolerance=Parameters[5] * 0.5;
uBlackTolerance=Parameters[6] * 0.5;
uSmoothing=round(1.0 + Parameters[7] * 2.0);
uEdgeTest=round(1.0 + Parameters[8] * 2.0);
uZoom = 1.0 + ((Parameters[10] - 0.25) * 10.0);
if (uZoom < 0.000000001) uZoom = 0.000000001;
uOffsetX = (Parameters[11] - 0.5) * 2.0;
uOffsetY = (Parameters[12] - 0.5) * 2.0;
uEdgeAmount=Parameters[9];


switch(uMode) {
 case 1 :
   ParamsChangeName(FLPluginHandle,LayerNr, 7, "Anti-Aliasing");
   ParamsChangeName(FLPluginHandle,LayerNr, 8, "Smooth Edges");
   ParamsChangeName(FLPluginHandle,LayerNr, 9, "Smooth Amt");
 break;
  case 2 :
   Parameters[7] = 0.0;
   Parameters[8] = 0.0;
   Parameters[9] = 0.0;
   ParamsChangeName(FLPluginHandle,LayerNr, 7, "");
   ParamsChangeName(FLPluginHandle,LayerNr, 8, "");
   ParamsChangeName(FLPluginHandle,LayerNr, 9, "");
   ParamsNotifyChanged(FLPluginHandle,LayerNr);
 break;
}]]>
      </Expression>
    </ZExpression>
  </OnUpdate>
  <OnRender>
    <UseMaterial Material="ScreenMaterial"/>
    <RenderSprite/>
  </OnRender>
  <Content>
    <Material Name="ScreenMaterial" Light="0" Blend="1" Shader="ScreenShader">
      <Textures>
        <MaterialTexture Texture="VideoTextureBitmap"/>
        <MaterialTexture Name="FeedbackMaterialTexture" Comment="NoCustom"/>
      </Textures>
    </Material>
    <Shader Name="ScreenShader">
      <VertexShaderSource>
<![CDATA[varying vec2 position;

void main(){
  vec4 vertex = gl_Vertex;
  vertex.xy *= 2.0;
  gl_Position = vertex;
  position=vertex.xy;
  gl_TexCoord[0] = gl_MultiTexCoord0;
}]]>
      </VertexShaderSource>
      <FragmentShaderSource>
<![CDATA[varying vec2 position;
uniform sampler2D tex1;
uniform sampler2D tex2;
uniform float uMode;
uniform float time;
uniform float resX;
uniform float resY;
uniform float viewportX;
uniform float viewportY;
vec2 iResolution = vec2(resX,resY);

uniform float uAlpha;
uniform float uMaskR;
uniform float uMaskG;
uniform float uMaskB;
uniform float uTolerance;
uniform float uBlackTolerance;
uniform float uSmoothing;
uniform float uEdgeTest;
uniform float uEdgeAmount;
uniform float uZoom;
uniform float uOffsetX;
uniform float uOffsetY;

vec3 rgb2hsv(vec4 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 rgb2hsv2(vec3 rgb)
{
	float Cmax = max(rgb.r, max(rgb.g, rgb.b));
	float Cmin = min(rgb.r, min(rgb.g, rgb.b));
    float delta = Cmax - Cmin;

	vec3 hsv = vec3(0., 0., Cmax);

	if (Cmax > Cmin)
	{
		hsv.y = delta / Cmax;

		if (rgb.r == Cmax)
			hsv.x = (rgb.g - rgb.b) / delta;
		else
		{
			if (rgb.g == Cmax)
				hsv.x = 2. + (rgb.b - rgb.r) / delta;
			else
				hsv.x = 4. + (rgb.r - rgb.g) / delta;
		}
		hsv.x = fract(hsv.x / 6.);
	}
	return hsv;
}

vec4 pixelMask (vec2 uv, int testing) {

  vec4 maskColor = vec4(uMaskR,uMaskG,uMaskB,1.0);

  float deltaX = 1.0 / iResolution.x * 2.0;
  float deltaY = 1.0 / iResolution.y * 2.0;
  vec4 color = texture2D(tex1,uv);
  vec4 blendcolor = color;
  vec4 returnColor = color;
  if (uSmoothing == 2.0 || testing == 1) {
  blendcolor += texture2D(tex1,uv + vec2(deltaX,0.0));
  blendcolor += texture2D(tex1,uv - vec2(deltaX,0.0));
  blendcolor += texture2D(tex1,uv + vec2(0.0,deltaY));
  blendcolor += texture2D(tex1,uv - vec2(0.0,deltaY));
  blendcolor /= vec4 (5.0,5.0,5.0,5.0);
  }
  vec3 hsv1 = rgb2hsv (blendcolor);
  vec3 hsv2 = rgb2hsv (maskColor);

  if (abs(hsv2.x - hsv1.x) <= uTolerance) {
  returnColor.r -= maskColor.r;
  returnColor.g -= maskColor.g;
  returnColor.b -= maskColor.b;
  returnColor.a = abs(hsv2.x - hsv1.x);
  if (color.r < uBlackTolerance && color.g < uBlackTolerance && color.b < uBlackTolerance)
    {
      returnColor=color;
      returnColor.a = 1.0;
    }
  } else {
     returnColor = color;
     returnColor.a =  1.0;
  }
   return (returnColor);
}

float chromaKey(vec3 color)
{
	vec3 backgroundColor = vec3(uMaskR, uMaskG, uMaskB);
	vec3 weights = vec3(uTolerance * 5.0 + uBlackTolerance,uTolerance * 5.0 + uBlackTolerance, uTolerance * 5.0 + uBlackTolerance);

	vec3 hsv = rgb2hsv2(color);
	vec3 target = rgb2hsv2(backgroundColor);
	float dist = length(weights * (target - hsv));
	return 1. - clamp(3. * dist - 1.5, 0., 1.);
}

vec3 changeSaturation(vec3 color, float saturation)
{
	float luma = dot(vec3(0.213, 0.715, 0.072) * color, vec3(1.));
	return mix(vec3(luma), color, saturation);
}

void main() {

    vec2 uv = (gl_FragCoord.xy-vec2(viewportX,viewportY)) / iResolution.xy;
    vec2 ouv = uv;
    // zoom & Offset
    uv -= vec2 (0.5,0.5);
    uv /= vec2(uZoom,uZoom);
    uv += vec2 (0.5,0.5);
    uv -= vec2 (uOffsetX,uOffsetY);
    uv.x = clamp (uv.x,0.0,1.0);
    uv.y = clamp (uv.y,0.0,1.0);

    if (uMode == 1.0) {
    gl_FragColor = pixelMask(uv,0);



    // Edge Testing
      if (uEdgeTest == 2.0) {
      float deltaX = 1.0 / iResolution.x * 2.0;
      float deltaY = 1.0 / iResolution.y * 2.0;
      vec4 edge1,edge2,edge3,edge4;
      edge1 = pixelMask(uv + vec2(deltaX,0.0),1);
      edge2 = pixelMask(uv - vec2(deltaX,0.0),1);
      edge3 = pixelMask(uv + vec2(0.0,deltaY),1);
      edge4 = pixelMask(uv - vec2(0.0,deltaY),1);
      float newAlpha = (gl_FragColor.a + edge1.a + edge2.a + edge3.a + edge4.a) / 5.0;
      gl_FragColor.a = (gl_FragColor.a * (1.0-uEdgeAmount)) + (newAlpha * (uEdgeAmount));
      }
      float alphaT = gl_FragColor.a * (1.0-uAlpha);

      vec4 c2 = texture2D(tex2,ouv);
      vec3 newCol = (vec3(1.0-alphaT)*c2.rgb) + (vec3(alphaT)*gl_FragColor.rgb);
      gl_FragColor.rgb = newCol.rgb;
      gl_FragColor.a = 1.0;
      }

     if (uMode == 2.0) {
      vec3 color = texture2D(tex1, uv).rgb;
	    float incrustation = chromaKey(color);
	    color = changeSaturation(color, 1.1);
      vec4 c2 = texture2D(tex2,ouv);
	    color = mix(color, c2.rgb, incrustation);
	    gl_FragColor = vec4(color, 1.0);
      vec3 newCol = (vec3(uAlpha)*c2.rgb) + (vec3(1.0-uAlpha)*gl_FragColor.rgb);
      gl_FragColor.rgb = newCol.rgb;
            gl_FragColor.a = 1.0;
     }
}]]>
      </FragmentShaderSource>
      <UniformVariables>
        <ShaderVariable VariableName="time" ValuePropRef="App.Time"/>
        <ShaderVariable VariableName="resX" ValuePropRef="App.ViewportWidth"/>
        <ShaderVariable VariableName="resY" ValuePropRef="App.ViewportHeight"/>
        <ShaderVariable VariableName="viewportX" ValuePropRef="App.ViewportX"/>
        <ShaderVariable VariableName="viewportY" ValuePropRef="App.ViewportY"/>
        <ShaderVariable VariableName="uAlpha" ValuePropRef="uAlpha"/>
        <ShaderVariable VariableName="uMaskR" ValuePropRef="uMaskR"/>
        <ShaderVariable VariableName="uMaskG" ValuePropRef="uMaskG"/>
        <ShaderVariable VariableName="uMaskB" ValuePropRef="uMaskB"/>
        <ShaderVariable VariableName="uTolerance" ValuePropRef="uTolerance"/>
        <ShaderVariable VariableName="uBlackTolerance" ValuePropRef="uBlackTolerance"/>
        <ShaderVariable VariableName="uSmoothing" ValuePropRef="uSmoothing"/>
        <ShaderVariable VariableName="uEdgeTest" ValuePropRef="uEdgeTest"/>
        <ShaderVariable VariableName="uZoom" ValuePropRef="uZoom"/>
        <ShaderVariable VariableName="uOffsetX" ValuePropRef="uOffsetX"/>
        <ShaderVariable VariableName="uOffsetY" ValuePropRef="uOffsetY"/>
        <ShaderVariable VariableName="uEdgeAmount" ValuePropRef="uEdgeAmount"/>
        <ShaderVariable VariableName="uMode" ValuePropRef="uMode"/>
      </UniformVariables>
    </Shader>
    <Bitmap Name="VideoTextureBitmap"/>
    <Group Comment="FLStudio">
      <Children>
        <Array Name="Parameters" SizeDim1="13" Persistent="255">
          <Values>
<![CDATA[78DA63608081067B183D6BE64C3B106640916B00F1416AEC01C093078E]]>
          </Values>
        </Array>
        <Constant Name="ParamHelpConst" Type="2">
          <StringValue>
<![CDATA[Key Mode @list: "RGB Key","Chroma Key"
Alpha
Mask Red
Mask Green
Mask Blue
Tolerance
Fine Tolerance
Anti-Aliasing @list: "Off","On"
Smooth Edges @list:"Off","On"
Smooth Amt
Zoom
Offset X
Offset Y]]>
          </StringValue>
        </Constant>
        <Constant Name="AuthorInfo" Type="2" StringValue="Cynex"/>
        <Variable Name="FLPluginHandle" Comment="Set by plugin" Type="9"/>
        <Variable Name="LayerNr" Comment="Set by plugin" Type="1"/>
      </Children>
    </Group>
    <Group Name="Uniforms">
      <Children>
        <Variable Name="uAlpha"/>
        <Variable Name="uMaskR"/>
        <Variable Name="uMaskG"/>
        <Variable Name="uMaskB"/>
        <Variable Name="uTolerance"/>
        <Variable Name="uBlackTolerance"/>
        <Variable Name="uSmoothing"/>
        <Variable Name="uEdgeTest"/>
        <Variable Name="uZoom"/>
        <Variable Name="uOffsetX"/>
        <Variable Name="uOffsetY"/>
        <Variable Name="uEdgeAmount"/>
        <Variable Name="uMode"/>
      </Children>
    </Group> <!-- Uniforms -->

  </Content>
</ZApplication>
